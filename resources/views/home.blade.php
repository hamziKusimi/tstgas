
@if (Route::has('login'))
@auth
@extends('master')

@section('content')

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-3">

                    <div class="info-box">
                        <!-- Apply any bg-* class to to the icon to color it -->
                        <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
                        <div class="info-box-content">
                            <span class="">Total Purchase</span>
                            <span class="info-box-number">RM {{ number_format($totalpurchase, 2) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">       
                    
                    <div class="info-box">
                        <!-- Apply any bg-* class to to the icon to color it -->
                        <span class="info-box-icon bg-orange"><i class="fa fa-star-o"></i></span>
                        <div class="info-box-content">
                            <span class="">Total Invoice</span>
                            <span class="info-box-number">RM {{ number_format($totalinvoice, 2) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">      
                        
                    <div class="info-box">
                        <!-- Apply any bg-* class to to the icon to color it -->
                        <span class="info-box-icon bg-blue"><i class="fa fa-star-o"></i></span>
                        <div class="info-box-content">
                            <span class="">Total Cash Bill</span>
                            <span class="info-box-number">RM {{ number_format($totalcashbill, 2) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">      
                        
                    <div class="info-box">
                        <!-- Apply any bg-* class to to the icon to color it -->
                        <span class="info-box-icon bg-green"><i class="fa fa-star-o"></i></span>
                        <div class="info-box-content">
                            <span class="">Total Delivery Order</span>
                            <span class="info-box-number">RM {{ number_format($totaldo, 2) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div> 
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6">
                <h4><i class="fa fa-line-chart" style="font-size:20px"> </i>Sales Weekly</h4>
            </div>
            <div class="col-md-6">
                <h4><i class="fa fa-bar-chart" style="font-size:20px"> </i>Sales Monthly</h4>
            </div>
        </div>
        <div class="row">
            <div class="card-body col-md-6">
                <canvas id="myChart" width="12%" height="12%" class="line-chart" data-get-weekly="{{ route("home.get.sales") }}"></canvas>
            </div>
            <div class="card-body col-md-6">
                <canvas id="myChart2" width="12%" height="12%" class="bar-chart" data-get-monthly="{{ route("home.get.sales.monthly") }}"></canvas>
            </div>
        </div>
@endsection

@push('scripts')
<script src="{{ asset('js/chart.min.js') }}"></script>
<script src="{{ asset('js/chart.bundle.min.js') }}"></script>
    <script>
    $(function () {

        let getRoute = $('.line-chart').data('get-weekly')
        let getRoute2 = $('.bar-chart').data('get-monthly')

            //Get Sales Weekly
            // var d = new Date().toLocale  DateString()
            var day1 = new Date()
            day1.setDate(day1.getDate() - 1)
            var day2 = new Date()
            day2.setDate(day2.getDate() - 2)
            var day3 = new Date()
            day3.setDate(day3.getDate() - 3)
            var day4 = new Date()
            day4.setDate(day4.getDate() - 4)
            var day5 = new Date()
            day5.setDate(day5.getDate() - 5)
            var day6 = new Date()
            day6.setDate(day6.getDate() - 6)
            var day7 = new Date()
            day7.setDate(day7.getDate() - 7)
            console.log(day1.toLocaleDateString())
            $.ajax({
                url: getRoute,
                method: 'GET',
            }).done(function (response) { console.log(response) 

                var ctx = document.getElementById("myChart").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: [day1.toLocaleDateString(), day2.toLocaleDateString(), day3.toLocaleDateString(), day4.toLocaleDateString(), day5.toLocaleDateString(), day6.toLocaleDateString(), day7.toLocaleDateString()],
                        datasets: [{
                            label: '# of Sales',
                            data: response,
                            backgroundColor: [
                            //     'rgba(255, 99, 132, 0.2)',
                            //     'rgba(54, 162, 235, 0.2)',
                            //     'rgba(255, 206, 86, 0.2)',
                            //     'rgba(75, 192, 192, 0.2)',
                            //     'rgba(153, 102, 255, 0.2)',
                            //     'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'blue'
                            //     'rgba(255,99,132,1)',
                            //     'rgba(54, 162, 235, 1)',
                            //     'rgba(255, 206, 86, 1)',
                            //     'rgba(75, 192, 192, 1)',
                            //     'rgba(153, 102, 255, 1)',
                            //     'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    drawOnChartArea: false
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    drawOnChartArea: false
                                },
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            })


            
            //Get Sales Monthly
            
            $.ajax({
                url: getRoute2,
                method: 'GET',
            }).done(function (response2) { console.log(response2) 

                var ctx2 = document.getElementById("myChart2").getContext('2d');
                var myChart2 = new Chart(ctx2, {
                    type: 'bar',
                    data: {
                        labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        datasets: [{
                            label: '# of Sales',
                            data: response2,
                            backgroundColor: [
                            //     'rgba(255, 99, 132, 0.2)',
                            //     'rgba(54, 162, 235, 0.2)',
                            //     'rgba(255, 206, 86, 0.2)',
                            //     'rgba(75, 192, 192, 0.2)',
                            //     'rgba(153, 102, 255, 0.2)',
                            //     'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'blue'
                            //     'rgba(255,99,132,1)',
                            //     'rgba(54, 162, 235, 1)',
                            //     'rgba(255, 206, 86, 1)',
                            //     'rgba(75, 192, 192, 1)',
                            //     'rgba(153, 102, 255, 1)',
                            //     'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    drawOnChartArea: false
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    drawOnChartArea: false
                                },
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            })
        });
    </script>
@endpush
@else
        <script>window.location = "/login";</script>
@endauth
@endif
