@extends('master')

@section('content')

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

@php ($i = 0)

<div class="row">
    <div class="col-4">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success" href="{{ route('suppliers.search') }}"> Search </a>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">

        <div class="bg-white">
            <div class="table-responsive">
                <table id="" class="table table-sm">
                    <thead class="bg-dark-blue text-white text-sm">
                        <tr>
                            <th width="5%">SN</th>
                            <th width="9%">Doc No</th>
                            <th width="8%">Date</th>
                            <th width="9%">Creditor</th>
                            <th width="9%">Stock Code</th>
                            <th width="12%">Description</th>
                            <th width="8%">Qty</th>
                            <th width="8%">U.O.M</th>
                            <th width="8%">Rate</th>
                            <th width="8%">UCost</th>
                            <th width="8%">Discount</th>
                            <th width="8%">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($suppliers as $supplier)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{ $supplier->doc_no }}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($supplier->updated_at)->format('d/m/Y') }}
                            </td>
                            <td>
                                {{ $supplier->account_code }}
                            </td>
                            <td>
                                {{ $supplier->item_code }}
                            </td>
                            <td>
                                {{ $supplier->subject }}
                            </td>
                            <td>
                                {{ $supplier->qty }}
                            </td>
                            <td>
                                {{ $supplier->uom }}
                            </td>
                            <td>
                                {{ $supplier->rate }}
                            </td>
                            <td>
                                {{ $supplier->ucost }}
                            </td>
                            <td>
                                {{ $supplier->discount }}
                            </td>
                            <td>
                                {{ $supplier->amount }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if(isset($suppliers))
                {{ $suppliers->links() }}
            @endif
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script>        
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endpush