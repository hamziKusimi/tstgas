
@extends('master')

@section('content')


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <form action ="{{ route('suppliers.query') }}" method="POST">
                @csrf
                    @include('enquiry.suppliers.form')
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
    $(".myselect").select2();
</script>
<script type="text/javascript">
    $('.docno').on('click', function () {
        $('.docnofrm').prop('disabled', false)
        $('.docnoto').prop('disabled', false)
    });
    $('.docnofrm').on('change', function (){
        $docnofrm = $(this).val()
        $('.docnoto').val($docnofrm)
    });
    
    $('.creditor').on('click', function () {
        $('.creditorfrm').prop('disabled', false)
        $('.creditorto').prop('disabled', false)
    });
    $('.creditorfrm').on('change', function (){
        $creditorfrm = $(this).val()
        $('.creditorto').val($creditorfrm)
    });
    
    $('.stockcode').on('click', function () {
        $('.stockcodefrm').prop('disabled', false)
        $('.stockcodeto').prop('disabled', false)
    });
    $('.stockcodefrm').on('change', function (){
        $stockcodefrm = $(this).val()
        $('.stockcodeto').val($stockcodefrm)
    });
    
    $('.category').on('click', function () {
        $('.categoryfrm').prop('disabled', false)
        $('.categoryto').prop('disabled', false)
    });
    $('.categoryfrm').on('change', function (){
        $categoryfrm = $(this).val()
        $('.categoryto').val($categoryfrm)
    });

    $('.product').on('click', function () {
        $('.productfrm').prop('disabled', false)
        $('.productto').prop('disabled', false)
    });
    $('.productfrm').on('change', function (){
        $productfrm = $(this).val()
        $('.productto').val($productfrm)
    });

    $('.brand').on('click', function () {
        $('.brandfrm').prop('disabled', false)
        $('.brandto').prop('disabled', false)
    });
    $('.brandfrm').on('change', function (){
        $brandfrm = $(this).val()
        $('.brandto').val($brandfrm)
    });

    $('.location').on('click', function () {
        $('.locationfrm').prop('disabled', false)
        $('.locationto').prop('disabled', false)
    });
    $('.locationfrm').on('change', function (){
        $locationfrm = $(this).val()
        $('.locationto').val($locationfrm)
    });
    
    $('.uom').on('click', function () {
        $('.uomfrm').prop('disabled', false)
        $('.uomto').prop('disabled', false)
    });
    $('.uomfrm').on('change', function (){
        $uomfrm = $(this).val()
        $('.uomto').val($uomfrm)
    });

</script>
@endpush