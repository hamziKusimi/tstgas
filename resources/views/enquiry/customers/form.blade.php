  
<div class="row">
    <div class="col-md-6">
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="docno" value="1" class="docno"> Document No
            </div>
            <div class="col-md-4">
                <input name="docnofrm" type="text" maxlength="100" maxlength="30" class="form-control docnofrm" disabled>
            </div>
            <div class="col-md-4">
                <input name="docnoto" type="text" maxlength="100" maxlength="30" class="form-control docnoto" disabled>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="debtor" value="1" class="debtor"> Debtor
            </div>
            <div class="col-md-4">
                <select class="myselect form-control debtorfrm" style="width:100%" name="debtorfrm" id="debtorfrm" disabled> 
                    <option value=""></option>
                    @foreach($debtors as $debtor)
                        <option value="{{ $debtor }}">{{ $debtor }}</option>
                    @endforeach
                </select>
                {{-- <input name="debtorfrm" type="text" maxlength="100" maxlength="30" class="form-control debtorfrm" disabled> --}}
            </div>
            <div class="col-md-4">
                <select class="myselect form-control debtorto" style="width:100%" name="debtorto" id="debtorto" disabled> 
                    <option value=""></option>
                    @foreach($debtors as $debtor)
                        <option value="{{ $debtor }}">{{ $debtor }}</option>
                    @endforeach
                </select>
                {{-- <input name="debtorto" type="text" maxlength="100" maxlength="30" class="form-control debtorto" disabled> --}}
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="stockcode" value="1" class="stockcode"> Stockcode
            </div>
            <div class="col-md-4">
                <select class="myselect form-control stockcodefrm" style="width:100%" name="stockcodefrm" id="stockcodefrm" disabled> 
                    <option value=""></option>
                    @foreach($stockcodes as $stockcode)
                        <option value="{{ $stockcode }}">{{ $stockcode }}</option>
                    @endforeach
                </select>
                {{-- <input name="stockcodefrm" type="text" maxlength="100" maxlength="30" class="form-control stockcodefrm" disabled> --}}
            </div>
            <div class="col-md-4">
                <select class="myselect form-control stockcodeto" style="width:100%" name="stockcodeto" id="stockcodeto" disabled> 
                    <option value=""></option>
                    @foreach($stockcodes as $stockcode)
                        <option value="{{ $stockcode }}">{{ $stockcode }}</option>
                    @endforeach
                </select>
                {{-- <input name="stockcodeto" type="text" maxlength="100" maxlength="30" class="form-control stockcodeto" disabled> --}}
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="category" value="1" class="category"> Category
            </div>
            <div class="col-md-4">
                <select class="myselect form-control categoryfrm" style="width:100%" name="categoryfrm" id="categoryfrm" disabled> 
                    <option value=""></option>
                    @foreach($categories as $category)
                        <option value="{{ $category }}">{{ $category }}</option>
                    @endforeach
                </select>
                {{-- <input name="categoryfrm" type="text" maxlength="100" maxlength="30" class="form-control categoryfrm" disabled> --}}
            </div>
            <div class="col-md-4">
                <select class="myselect form-control categoryto" style="width:100%" name="categoryto" id="categoryto" disabled> 
                    <option value=""></option>
                    @foreach($categories as $category)
                        <option value="{{ $category }}">{{ $category }}</option>
                    @endforeach
                </select>
                {{-- <input name="categoryto" type="text" maxlength="100" maxlength="30" class="form-control categoryto" disabled> --}}
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="product" value="1" class="product"> Product
            </div>
            <div class="col-md-4">
                <select class="myselect form-control productfrm" style="width:100%" name="productfrm" id="productfrm" disabled> 
                    <option value=""></option>
                    @foreach($products as $product)
                        <option value="{{ $product }}">{{ $product }}</option>
                    @endforeach
                </select>
                {{-- <input name="productfrm" type="text" maxlength="100" maxlength="30" class="form-control productfrm" disabled> --}}
            </div>
            <div class="col-md-4">
                <select class="myselect form-control productto" style="width:100%" name="productto" id="productto" disabled> 
                    <option value=""></option>
                    @foreach($products as $product)
                        <option value="{{ $product }}">{{ $product }}</option>
                    @endforeach
                </select>
                {{-- <input name="productto" type="text" maxlength="100" maxlength="30" class="form-control productto" disabled> --}}
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="brand" value="1" class="brand"> Brand
            </div>
            <div class="col-md-4">
                <select class="myselect form-control brandfrm" style="width:100%" name="brandfrm" id="brandfrm" disabled> 
                    <option value=""></option>
                    @foreach($brands as $brand)
                        <option value="{{ $brand }}">{{ $brand }}</option>
                    @endforeach
                </select>
                {{-- <input name="brandfrm" type="text" maxlength="100" maxlength="30" class="form-control brandfrm" disabled> --}}
            </div>
            <div class="col-md-4">
                <select class="myselect form-control brandto" style="width:100%" name="brandto" id="brandto" disabled> 
                    <option value=""></option>
                    @foreach($brands as $brand)
                        <option value="{{ $brand }}">{{ $brand }}</option>
                    @endforeach
                </select>
                {{-- <input name="brandto" type="text" maxlength="100" maxlength="30" class="form-control brandto" disabled> --}}
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="location" value="1" class="location"> Location
            </div>
            <div class="col-md-4">
                <select class="myselect form-control locationfrm" style="width:100%" name="locationfrm" id="locationfrm" disabled> 
                    <option value=""></option>
                    @foreach($locations as $location)
                        <option value="{{ $location }}">{{ $location }}</option>
                    @endforeach
                </select>
                {{-- <input name="locationfrm" type="text" maxlength="100" maxlength="30" class="form-control locationfrm" disabled> --}}
            </div>
            <div class="col-md-4">
                <select class="myselect form-control locationto" style="width:100%" name="locationto" id="locationto" disabled> 
                    <option value=""></option>
                    @foreach($locations as $location)
                        <option value="{{ $location }}">{{ $location }}</option>
                    @endforeach
                </select>
                {{-- <input name="locationto" type="text" maxlength="100" maxlength="30" class="form-control locationto" disabled> --}}
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-4">
                <input type="checkbox" name="uom" value="1" class="uom"> U.O.M
            </div>
            <div class="col-md-4">
                <select class="myselect form-control uomfrm" style="width:100%" name="uomfrm" id="uomfrm" disabled> 
                    <option value=""></option>
                    @foreach($uoms as $uom)
                        <option value="{{ $uom }}">{{ $uom }}</option>
                    @endforeach
                </select>
                {{-- <input name="uomfrm" type="text" maxlength="100" maxlength="30" class="form-control uomfrm" disabled> --}}
            </div>
            <div class="col-md-4">
                <select class="myselect form-control uomto" style="width:100%" name="uomto" id="uomto" disabled> 
                    <option value=""></option>
                    @foreach($uoms as $uom)
                        <option value="{{ $uom }}">{{ $uom }}</option>
                    @endforeach
                </select>
                {{-- <input name="uomto" type="text" maxlength="100" maxlength="30" class="form-control uomto" disabled> --}}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-group row narrow-padding-global">
            
            <label for="" class="col-md-4">Left Text(1)</label>
            <div class="col-md-4">
                <select class="myselect form-control lefttextname1" name="lefttextname1" id="lefttextname1"> 
                    <option value=""></option>
                    <option value="doc_no">Doc No</option>
                    <option value="account_code">debtor</option>
                    <option value="name">Name</option>
                    <option value="item_code">Stockcode</option>
                    <option value="subject">Description</option>
                    <option value="ref1">Reference 1</option>
                    <option value="ref2">Reference 2</option>
                    <option value="model">Model</option>
                    <option value="category">Category</option>
                    <option value="product">Product</option>
                    <option value="brand">Brand</option>
                    <option value="location">Location</option>
                    <option value="uom">U.O.M</option>
                    <option value="type">Type</option>
                    <option value="weight">Weight</option>
                </select>
                {{-- <input name="lefttextname1" type="text" maxlength="100" maxlength="30" class="form-control lefttextname1" disabled> --}}
            </div>
            <div class="col-md-4">
                <input name="lefttextval1" type="text" maxlength="100" maxlength="30" class="form-control lefttextval1">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-4">Context Text</label>
            <div class="col-md-4">
                <select class="myselect form-control ctxtextname1" name="ctxtextname1" id="ctxtextname1"> 
                    <option value=""></option>
                    <option value="doc_no">Doc No</option>
                    <option value="account_code">debtor</option>
                    <option value="name">Name</option>
                    <option value="item_code">Stockcode</option>
                    <option value="subject">Description</option>
                    <option value="ref1">Reference 1</option>
                    <option value="ref2">Reference 2</option>
                    <option value="model">Model</option>
                    <option value="category">Category</option>
                    <option value="product">Product</option>
                    <option value="brand">Brand</option>
                    <option value="location">Location</option>
                    <option value="uom">U.O.M</option>
                    <option value="type">Type</option>
                    <option value="weight">Weight</option>
                </select>
                {{-- <input name="ctxtextname1" type="text" maxlength="100" maxlength="30" class="form-control ctxtextname1" disabled> --}}
            </div>
            <div class="col-md-4">
                <input name="ctxtextval1" type="text" maxlength="100" maxlength="30" class="form-control ctxtextval1">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            
            <label for="" class="col-md-4">Left Text(2)</label>
            <div class="col-md-4">
                <select class="myselect form-control lefttextname2" name="lefttextname2" id="lefttextname2"> 
                    <option value=""></option>
                    <option value="doc_no">Doc No</option>
                    <option value="account_code">debtor</option>
                    <option value="name">Name</option>
                    <option value="item_code">Stockcode</option>
                    <option value="subject">Description</option>
                    <option value="ref1">Reference 1</option>
                    <option value="ref2">Reference 2</option>
                    <option value="model">Model</option>
                    <option value="category">Category</option>
                    <option value="product">Product</option>
                    <option value="brand">Brand</option>
                    <option value="location">Location</option>
                    <option value="uom">U.O.M</option>
                    <option value="type">Type</option>
                    <option value="weight">Weight</option>
                </select>
                {{-- <input name="lefttextname2" type="text" maxlength="100" maxlength="30" class="form-control lefttextname2" disabled> --}}
            </div>
            <div class="col-md-4">
                <input name="lefttextval2" type="text" maxlength="100" maxlength="30" class="form-control lefttextval2">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-4">Context Text</label>
            <div class="col-md-4">
                <select class="myselect form-control ctxtextname2" name="ctxtextname2" id="ctxtextname2"> 
                    <option value=""></option>
                    <option value="doc_no">Doc No</option>
                    <option value="account_code">debtor</option>
                    <option value="name">Name</option>
                    <option value="item_code">Stockcode</option>
                    <option value="subject">Description</option>
                    <option value="ref1">Reference 1</option>
                    <option value="ref2">Reference 2</option>
                    <option value="model">Model</option>
                    <option value="category">Category</option>
                    <option value="product">Product</option>
                    <option value="brand">Brand</option>
                    <option value="location">Location</option>
                    <option value="uom">U.O.M</option>
                    <option value="type">Type</option>
                    <option value="weight">Weight</option>
                </select>
                {{-- <input name="ctxtextname2" type="text" maxlength="100" maxlength="30" class="form-control ctxtextname2" disabled> --}}
            </div>
            <div class="col-md-4">
                <input name="ctxtextval2" type="text" maxlength="100" maxlength="30" class="form-control ctxtextval2">
            </div>
        </div>
        <br>
        {{-- <div class="input-group row narrow-padding-global">
            <div class="col-md-1">
            </div>
            <div class="col-md-3">
                <input type="checkbox" name="active" value="1" checked> Active<br>
            </div>
            <div class="col-md-3">
                <input type="checkbox" name="stock" value="1" checked> Stock Item<br>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="nonstock" value="1" checked > None Stock Item<br>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-1">
            </div>
            <div class="col-md-3">
                <input type="checkbox" name="inactive" value="1" > inActive<br>
            </div>
            <div class="col-md-3">
                <input type="checkbox" name="service" value="1" checked> Service<br>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="inactivestock" value="1" checked > inActive Stock Item<br>
            </div>
        </div> --}}
    </div>
</div>

<br>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Search</button>
</div>
