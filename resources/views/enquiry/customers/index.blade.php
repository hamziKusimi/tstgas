@extends('master')

@section('content')

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

@php ($i = 0)

<div class="row">
    <div class="col-4">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success" href="{{ route('customers.search') }}"> Search </a>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">

        <div class="bg-white">
            <div class="table-responsive">
                <table id="example" class="table table-sm">
                    <thead class="bg-dark-blue text-white text-sm">
                        <tr>
                            <th width="5%">SN</th>
                            <th width="9%">Doc No</th>
                            <th width="8%">Date</th>
                            <th width="9%">Creditor</th>
                            <th width="9%">Stock Code</th>
                            <th width="12%">Description</th>
                            <th width="8%">Qty</th>
                            <th width="8%">U.O.M</th>
                            <th width="8%">Rate</th>
                            <th width="8%">UCost</th>
                            <th width="8%">Discount</th>
                            <th width="8%">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($customers as $customer)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{ $customer->doc_no }}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($customer->updated_at)->format('d/m/Y') }}
                            </td>
                            <td>
                                {{ $customer->account_code }}
                            </td>
                            <td>
                                {{ $customer->item_code }}
                            </td>
                            <td>
                                {{ $customer->subject }}
                            </td>
                            <td>
                                {{ $customer->qty }}
                            </td>
                            <td>
                                {{ $customer->uom }}
                            </td>
                            <td>
                                {{ $customer->rate }}
                            </td>
                            <td>
                                {{ $customer->ucost }}
                            </td>
                            <td>
                                {{ $customer->discount }}
                            </td>
                            <td>
                                {{ $customer->amount }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script>        
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endpush