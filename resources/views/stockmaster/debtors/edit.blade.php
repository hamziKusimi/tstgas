@extends('master')
@section('content')

<div class="card">
    <div class="card-body">
        <form action ="{{ route('debtors.update', $debtor->id )}}" method="POST">
            <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('stockmaster/debtors/form')
            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit" data-dismiss="modal">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
