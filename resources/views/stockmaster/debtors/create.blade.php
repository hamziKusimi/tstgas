@extends('master')
@section('content')

<div class="card">
    <div class="card-body">
        <form action="{{ route('debtors.store') }}" method="POST">
            @csrf
            @include('stockmaster/debtors/form')
            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit" data-dismiss="modal">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('modals')
    @include('dailypro.components.account-modal', [
        'removeDebtorFinder' => true
    ])
@endsection
@push('scripts')
<script>
(function() {

    let validated = false
    let exist = false
    $('.acode-input').keyup(function(e) {
        let acode = $(this).val()
        let existingAccounts = $(this).data('existing-accounts')

        // validate string to be in A0001-00, otherwise show error
        let regex = new RegExp('^([A-Z0-9]{5}-[0-9]{2})$');
        if (acode.match(regex)) {
            if ($.inArray(acode, existingAccounts) !== -1) {
                $('.d-acode-message').text('This code is already used in another account')
                $('.acode-input').prop('disabled', true)
                exist = true
            } else {
                $('.d-acode-message').text('')
                $('.acode-input').prop('disabled', false)
                exist = false
            }
            validated = true
        } else {
            // if the validation fail
            validated = false
            exist = false
            acode !== '' ?
                $('.d-acode-message').text('Error, please follow the following format: XXXXX-CC') :
                $('.d-acode-message').text('')

            // check again if in array
            if ($.inArray(acode, existingAccounts) !== -1) {
                $('.d-acode-message').text('This code is already used in another account')
                $('.acode-input').prop('disabled', true)
                exist = true
            }
        }

        // if the account code is validated, then run the AJAX to find if there's another account exist with this code
        if (validated) {
            let url = $(this).data('url')
            let data = { 'acode-input': $('.acode-input').val() }
            $.ajax({
                'url': url,
                'method': 'POST',
                'data': data,
            }).done(function(account) {
                // if found account, populate the fields, and change the route of the form action
                // because we want to allow users to create new debtor here as well as update the existing details
                if (account) {
                    $('.submit').hasClass('d-none') ? '' : $('.submit').addClass('d-none')
                    $('#D_ACODE').val(account.D_ACODE)
                    $('#D_NAME').val(account.D_NAME)

                    if (account.D_TYPE == 'T') {
                        $('#D_TYPE_T').attr('checked', true)
                        $('#D_TYPE_O').attr('checked', false)
                    } else {
                        $('#D_TYPE_T').attr('checked', false)
                        $('#D_TYPE_O').attr('checked', true)
                    }

                    $('#D_ADDRESS').val(account.address)
                    $('#D_PHONE').val(account.D_TEL)
                    $('#D_HP').val(account.D_HP)
                    $('#D_FAX').val(account.D_FAX)
                    $('#D_EMAIL').val(account.D_EMAIL)
                    $('#D_CTERM').val(account.D_CTERM)
                    $('#D_CLIMIT').val(account.D_CLIMIT)
                    $('#D_GSTNO').val(account.d_gstno)
                    $('#D_BRN').val(account.d_brn)
                    $('#D_MEMO').val(account.D_MEMO)

                    // show reset button
                    $('#reset-button').hasClass('d-none') ? $('#reset-button').removeClass('d-none') : ''
                    exist = true

                    // re-enable the account input button
                    $('.acode-input').prop('disabled', false)
                }
            })
        }

        // disable save button if matches with existing acode
        if (exist || !validated) {
            $('.submit').prop('disabled', true)
        } else {
            $('.submit').prop('disabled', false)
        }

        // check if not exist, clear the form
        if (!exist) clearForm()

    })

    // clear everything including the acode
    $('#reset-button').on('click', function() {
        $('#D_ACODE').val('')
        $('.d-acode-message').text('')
        clearForm()
    })

    let clearForm = function() {
        exist = false
        $('.form-data').val('')
        $('#reset-button').addClass('d-none')
        $('.submit').hasClass('d-none') ? $('.submit').removeClass('d-none') : ''
    }
}) ()
</script>
@endpush
