@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('IV_MD_DEBTOR_CR'))
    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('debtors.create') }}"> Add Debtor </a>
        </div>
    </div>
@endif

    {{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif --}}

    
    @if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>

    @endif  

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="example">
                    <thead class="">
                        <th width="25%">A/C Code</th>
                        <th width="30%">Name</th>
                        <th width="10%">Tel</th>
                        <th width="10%">Fax</th>
                        <th width="10%">Email</th>
                        <th width="15%"></th>
                    </thead>
                    @if(sizeof($debtors) > 0)        
                    <tbody>
                        @foreach ($debtors as $debtor)
                        <tr>
                            @if(Auth::user()->hasPermissionTo('IV_MD_DEBTOR_UP'))
                            <td><a href="{{ route('debtors.edit',$debtor->id) }}">{{ $debtor->accountcode }}</a></td>
                            @else
                            <td>{{ $debtor->accountcode }}</td>
                            @endif
                            <td>{{ $debtor->name }}</td>
                            <td>{{ $debtor->tel }}</td>
                            <td>{{ $debtor->fax }}</td>
                            <td>{{ $debtor->email }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_DEBTOR_DL'))
                            <td>
                                <a href="{{ route('debtors.destroy', $debtor->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                <i class="fa fa-trash"></i></a>  
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                     @endif
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endpush

