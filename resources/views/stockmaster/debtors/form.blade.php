<div class="form-group">
    {!! Form::label('D_ACODE', 'Account Code', ['class' => 'col-form-label']) !!} <span class="text-danger">*</span>
    <small class="text-danger">
        <span class="d-acode-message"></span>
    </small>
    <div class="input-group">
        {!! Form::text('D_ACODE', isset($debtor)?$debtor->accountcode:null, [ 'class' => 'form-control acode-input',
        'id' => 'D_ACODE',
        'placeholder' => 'Please create a new account code',
        'required',
        isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
        // 'data-url' => route('debtors.api.find.acode'),
        'data-existing-accounts' => null,
        ]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('D_NAME', 'Name', ['class' => 'col-form-label']) !!} <span class="text-danger">*</span>
    {!! Form::text('D_NAME', isset($debtor)?$debtor->name:null, ['class' => 'form-control form-data', 'required', 'id'
    => 'D_NAME',
    'placeholder' => 'Name']) !!}
</div>
<div class="row">
    <div class="col-3">
        <div class="form-group">
            {!! Form::label('def_P', 'Default Price', ['class' => 'col-form-label']) !!}
            {!! Form::select('def_price',
            ['price1'=>$price1,
            'minprice'=>'Min Price',
            'latestprice'=>'Latest Price',
            'price2'=>$price2,
            'price3'=>$price3,
            'price4'=>$price4,
            'price5'=>$price5,
            'price6'=>$price6
            ],
            isset($debtor)?$debtor->def_price:'unitprice', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-3">
        <div class="form-group">
            {!! Form::label('salesman', 'Salesman', ['class' => 'col-form-label']) !!}
            {!! Form::select('salesman', $salesmans, isset($debtor) ? $debtor->salesman : null, [
            'class' => 'form-control myselect salesman',
            'placeholder' => ''
            ]) !!}
        </div>
    </div>
</div>
<br>
<div class="form-group row">		
    <div class="col-md-12">
        <div class="radio-list">
            <label class="radio-inline">
            <input type="radio" name="active" value="1" {{ isset($debtor)?($debtor->active=='1')? 'checked' : '': 'checked' }}/> Active </font></label>
            <label class="radio-inline">
            <input type="radio" name="active" value="0" {{ isset($debtor)?($debtor->active=='0')? 'checked' : '': '' }}/> Non-Active </font></label>
        </div>
    </div>
</div>
<br>
<div class="form-group">
    {!! Form::label('D_ADDRESS', 'Address', ['class' => 'col-form-label']) !!}
    {!! Form::textarea('D_ADDRESS', isset($debtor)?$debtor->address:null, ['class' => 'form-control form-data', 'rows'
    => '4', 'id' => 'D_ADDRESS']) !!}
</div>
<div class="form-group">
    {!! Form::label('contact', 'Contact', ['class' => 'col-form-label']) !!}
    <div class="row">
        <div class="col">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-phone"></i></div>
                </div>
                {!! Form::text('D_PHONE', isset($debtor)?$debtor->tel:null, ['class' => 'form-control form-data', 'id'
                => 'D_PHONE', 'placeholder' => 'Phone No']) !!}
            </div>
        </div>
        <div class="col">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-phone"></i></div>
                </div>
                {!! Form::text('D_HP', isset($debtor)?$debtor->hp:null, ['class' => 'form-control form-data', 'id' =>
                'D_HP', 'placeholder' => 'Handphone No']) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-fax"></i></div>
                </div>
                {!! Form::text('D_FAX', isset($debtor)?$debtor->fax:null, ['class' => 'form-control form-data', 'id' =>
                'D_FAX', 'placeholder' => 'Fax']) !!}
            </div>
        </div>
        <div class="col">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-envelope"></i></div>
                </div>
                {!! Form::text('D_EMAIL', isset($debtor)?$debtor->email:null, ['class' => 'form-control form-data', 'id'
                => 'D_EMAIL', 'placeholder' => 'Email']) !!}
            </div>
        </div>
    </div>
</div>

<a href="#" data-toggle="collapse" data-target="#account-create-more-details">Click to show more</a>
<div id="account-create-more-details" class="collapse">
    <div class="form-group">
        <div class="row">
            <div class="col">
                {!! Form::label('D_CTERM', 'Credit Term', ['class' => 'col-form-label']) !!}
                {!! Form::text('D_CTERM', isset($debtor)?$debtor->cterm:null, ['class' => 'form-control form-data', 'id'
                => 'D_CTERM']) !!}
            </div>
            <div class="col">
                {!! Form::label('D_CLIMIT', 'Credit Limit', ['class' => 'col-form-label']) !!}
                {!! Form::text('D_CLIMIT', isset($debtor)?$debtor->climit:null, ['class' => 'form-control form-data
                money-default', 'placeholder' => '0.00', 'id' => 'D_CLIMIT']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::label('d_gstno', 'GST No.', ['class' => 'col-form-label']) !!}
                {!! Form::text('d_gstno', isset($debtor)?$debtor->gstno:null, ['class' => 'form-control form-data', 'id'
                => 'D_GSTNO']) !!}
            </div>
            <div class="col">
                {!! Form::label('d_brn', 'BRN No.', ['class' => 'col-form-label']) !!}
                {!! Form::text('d_brn', isset($debtor)?$debtor->brnno:null, ['class' => 'form-control form-data', 'id'
                => 'D_BRN']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('D_MEMO', 'Memo', ['class' => 'col-form-label']) !!}
        {!! Form::textarea('D_MEMO', isset($debtor)?$debtor->memo:null, ['class' => 'form-control form-data', 'rows' =>
        '3', 'id' => 'D_MEMO']) !!}
    </div>

    <br>
    <h4>Custom Field</h4>
    <br>

    <div class="form-group">
        <div class="row">
            <div class="col">
                <div class="input-group mb-2">
                    {!! Form::label('custom1', $custom1, ['class' => 'col-form-label']) !!}
                    {!! Form::hidden('custom1', isset($custom1)?$custom1:null, ['class' => 'form-control', 'id' =>
                    'custom1', 'placeholder'=>'Name']) !!}
                </div>
            </div>
            <div class="col">
                <div class="input-group mb-2">
                    {!! Form::text('customval1', isset($debtor)?$debtor->customval1:null, ['class' => 'form-control',
                    'id' => 'customval1', 'placeholder'=>'Value']) !!}
                </div>
            </div>
        </div>
    </div>

    @if($custom2)
    <div class="form-group">
        <div class="row">
            <div class="col">
                <div class="input-group mb-2">
                    {!! Form::label('custom2', $custom2, ['class' => 'col-form-label']) !!}
                    {!! Form::hidden('custom2', isset($custom2)?$custom2:null, ['class' => 'form-control', 'id' =>
                    'custom2', 'placeholder'=>'Name']) !!}
                </div>
            </div>
            <div class="col">
                <div class="input-group mb-2">
                    @if ($custom2_type == 'text')
                    {!! Form::text('customval2', isset($debtor)?$debtor->customval2:null, ['class' => 'form-control',
                    'id' => 'customval2', 'placeholder'=>'Value']) !!}
                    @elseif($custom2_type == 'checkbox' )
                    {!! Form::checkbox('customval2', null,isset($debtor)?$debtor->customval2:null) !!}
                    @endif

                </div>
            </div>
        </div>
    </div>
    @endif
    @if($custom3)
    <div class="form-group">
        <div class="row">
            <div class="col">
                <div class="input-group mb-2">
                    {!! Form::label('custom3', $custom3, ['class' => 'col-form-label']) !!}
                    {!! Form::hidden('custom3', isset($custom3)?$custom3:null, ['class' => 'form-control', 'id' =>
                    'custom3', 'placeholder'=>'Name']) !!}
                </div>
            </div>
            <div class="col">
                @if ($custom3_type == 'text')
                {!! Form::text('customval3', isset($debtor)?$debtor->customval3:null, ['class' => 'form-control',
                'id' => 'customval3', 'placeholder'=>'Value']) !!}
                @elseif($custom3_type == 'checkbox' )
                {!! Form::checkbox('customval3', null,isset($debtor)?$debtor->customval3:null) !!}
                @endif
            </div>
        </div>
    </div>
    @endif
    @if($custom4)
    <div class="form-group">
        <div class="row">
            <div class="col">
                <div class="input-group mb-2">
                    {!! Form::label('custom4', $custom4, ['class' => 'col-form-label']) !!}
                    {!! Form::hidden('custom4', isset($custom4)?$custom4:null, ['class' => 'form-control', 'id' =>
                    'custom4', 'placeholder'=>'Name']) !!}
                </div>
            </div>
            <div class="col">
                @if ($custom4_type == 'text')
                {!! Form::text('customval3', isset($debtor)?$debtor->customval3:null, ['class' => 'form-control',
                'id' => 'customval3', 'placeholder'=>'Value']) !!}
                @elseif($custom4_type == 'checkbox' )
                {!! Form::checkbox('customval3', null,isset($debtor)?$debtor->customval3:null) !!}
                @endif
            </div>
        </div>
    </div>
    @endif
    @if($custom5)
    <div class="form-group">
        <div class="row">
            <div class="col">
                <div class="input-group mb-2">
                    {!! Form::label('custom5', $custom5, ['class' => 'col-form-label']) !!}
                    {!! Form::hidden('custom5', isset($custom5)?$custom5:null, ['class' => 'form-control', 'id' =>
                    'custom5', 'placeholder'=>'Name']) !!}
                </div>
            </div>
            <div class="col">
                @if ($custom5_type == 'text')
                {!! Form::text('customval5', isset($debtor)?$debtor->customval5:null, ['class' => 'form-control',
                'id' => 'customval5', 'placeholder'=>'Value']) !!}
                @elseif($custom5_type == 'checkbox' )
                {!! Form::checkbox('customval5', null,isset($debtor)?$debtor->customval5:null) !!}
                @endif
            </div>
        </div>
    </div>
    @endif
</div>