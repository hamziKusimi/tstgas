@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('stockcodes.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @include('stockmaster/stockcodes/form')

        </form>
    </div>
</div>

@endsection
@push('scripts')

<script type="text/javascript">
    $(".myselect1").select2({
        
    //   theme: 'bootstrap4',
    //   width: 'style',
    });
</script>
<script type="text/javascript">
 @include('js.stockcode-calculate-js')
 @include('js.select-Itemmaster-js')
</script>

@endpush

