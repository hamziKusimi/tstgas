@extends('master')

@section('content')


@if(Auth::user()->hasPermissionTo('IV_MD_STOCK_CR'))
    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('stockcodes.create') }}">Add Stock</a>
        </div>
    </div>
@endif

    {{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif --}}

    
@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)
<div class="card">
    <div class="card-body">
        <div class="bg-white">        
            <div class="table-responsive">
                <table id="stkCode" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Category</th>
                            <th>Product</th>
                            <th>Brand</th>
                            <th>Location</th>
                            <th width="280px">More</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                @if(sizeof($stockcodes) > 0)        
                    <tbody>
                        @foreach ($stockcodes as $stockcode)
                        <tr>
                        
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_STOCK_UP'))
                                <td><a href="{{ route('stockcodes.edit',$stockcode->id) }}">{{ $stockcode->code }}</a></td>
                            @else
                                <td>
                                {{ $stockcode->code }}
                                </td>
                            @endif
                            {{-- <td>{{ $stockcode->code}}</td> --}}
                            <td>{{ $stockcode->category->code }}</td>
                            <td>{{ $stockcode->product->code }}</td>
                            <td>{{ $stockcode->brand->code }}</td>
                            <td>{{ $stockcode->location->code }}</td>
                            <td>{{ $stockcode->descr }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_STOCK_DL'))
                                <td>
                                    <a href="{{ route('stockcodes.destroy', $stockcode->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>  
                                </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
            @endif
                </table>
                
            </div>
        </div>
    </div>
</div>
     {{-- {!! $stockcodes->links() !!} --}}

@endsection
@push('scripts')
    <script>        
        $(document).ready(function() {
            $('#stkCode').DataTable();
        } );
    </script>
@endpush