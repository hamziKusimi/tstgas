@extends('master')
@section('content')

<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('stockcodes.update', $stockcode->id )}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('stockmaster/stockcodes/form')

        </form>
    </div>
</div>


@endsection
@push('scripts')

<script type="text/javascript">
    @include('js.stockcode-calculate-js')
    @include('js.select-Itemmaster-js')
</script>

@endpush

