<div class="row">
    <div class="col-md-8">
        <div class="input-group row narrow-padding-global">
            <label for="code" class="col-md-2">Stock Code<span class="text-danger">*</span></label>
            <div class="col-md-6">
                <input name="code" type="text" maxlength="100" maxlength="30" class="form-control"
                    value="{{ isset($stockcode)?$stockcode->code:'' }}" required>
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="edit" value="1"
                    {{ isset($stockcode)?($stockcode->edit=='1')? 'checked' : '': ''}}> Allow to edit Description at
                Daily Processing
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="descr" class="col-md-2">Description<span class="text-danger">*</span></label>
            <div class="col-md-6">
                <input name="descr" type="text" maxlength="50" maxlength="100" class="form-control"
                    value="{{ isset($stockcode)?$stockcode->descr:'' }}" required>
            </div>

        </div>

        <div class="input-group row narrow-padding-global">
            <label for="ref1" class="col-md-2">Reference 1</label>
            <div class="col-md-6">
                <input name="ref1" type="text" maxlength="100" class="form-control"
                    value="{{ isset($stockcode)?$stockcode->ref1:'' }}">
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Reference 2</label>
            <div class="col-md-6">
                <input name="ref2" type="text" maxlength="100" class="form-control"
                    value="{{ isset($stockcode)?$stockcode->ref2:'' }}">
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Model</label>
            <div class="col-md-6">
                <input name="model" type="text" maxlength="100" class="form-control"
                    value="{{ isset($stockcode)?$stockcode->model:'' }}">
            </div>
        </div>
    </div>
    <div class="col-md-4 row canvas">
        <div class="input-group row narrow-padding-global">
            <div class="col-md-12" style="width:285px;height:190px;border:1px black">
                <input type="file" name="image" id="image" accept="image/*" class="form-control" value="">

                @if (isset($stockcode) && !empty($stockcode->image))
                {{-- {{ dd($stockcode->image) }} --}}
                <img src="{{ asset($stockcode->image) }}" alt="" style="width:285px;height:190px;border:1px black">
                @endif
            </div>
        </div>
    </div>
</div>
{{-- {{ dd($stockcode) }} --}}
<div class="row">
    <div class="col-md-8">
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Category<span class="text-danger">*</span></label>
            <div class="col-md-4">
                {!! Form::select('category', $categories, isset($stockcode)?$stockcode->cat_id:null, [
                'class' => 'form-control category calculate-field2',
                'placeholder' => '',
                'required',
                'style' => 'width:100%; height:100%'
                ]) !!}
                {{-- <select class="form-control myselect" name="category" id="category" required>
                    <option value=""></option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}"
                {{ isset($stockcode)?$stockcode->cat_id == $category->id  ? 'selected' : '' : ''}}>{{ $category->descr}}
                </option> --}}
                {{-- <option value="{{$category->id}}">{{$category->code}}</option> --}}
                {{-- @endforeach --}}
                {{-- </select> --}}
                {{-- <input name="category" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->cat_id:'' }}"
                > --}}
            </div>
            <label for="" class="col-md-2">Type<span class="text-danger">*</span></label>
            <div class="col-md-4">

                <select class="myselect form-control" name="type" id="type" required>
                    <option value=""></option>
                    <option value="Stock Item"
                        {{ isset($stockcode)?$stockcode->type == "Stock Item"  ? 'selected' : '' : ''}}>Stock Item
                    </option>
                    <option value="inActive Stock Item"
                        {{ isset($stockcode)?$stockcode->type == "inActive Stock Item"  ? 'selected' : '' : ''}}>
                        inActive Stock Item</option>
                    <option value="None Stock Item"
                        {{ isset($stockcode)?$stockcode->type == "None Stock Item"  ? 'selected' : '' : ''}}>None Stock
                        Item</option>
                    <option value="Service"
                        {{ isset($stockcode)?$stockcode->type == "Service"  ? 'selected' : '' : ''}}>Service</option>
                </select>
                {{-- <input name="type" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->type:'' }}"
                > --}}
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Product<span class="text-danger">*</span></label>
            <div class="col-md-4">

                {!! Form::select('product', $products, isset($stockcode)?$stockcode->prod_id:null, [
                'class' => 'form-control product calculate-field2 myselect',
                'placeholder' => '',
                'required',
                'style' => 'width:100%; height:100%',
                // 'data-prod' => $products,
                ]) !!}
                {{-- <select class="myselect form-control" name="product" id="product" required>
                    <option value=""></option>
                    @foreach($products as $product)
                        <option value="{{$product->id}}"
                {{isset($stockcode)?$stockcode->prod_id == $product->id  ? 'selected' : '' : ''}}>{{ $product->descr}}
                </option> --}}
                {{-- <option value="{{$product->id}}">{{$product->code}}</option> --}}
                {{-- @endforeach
                </select> --}}
                {{-- <input name="product" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->prod_id:'' }}"
                > --}}
            </div>

            <label for="" class="col-md-2">Weight</label>
            <div class="col-md-4">
                <input name="weight" type="text" maxlength="100" class="form-control"
                    value="{{ isset($stockcode)?$stockcode->weight:'' }}">
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Brand<span class="text-danger">*</span></label>
            <div class="col-md-4">
                {!! Form::select('brand', $brands, isset($stockcode)?$stockcode->brand_id:null, [
                'class' => 'form-control brand calculate-field2 myselect',
                'placeholder' => '',
                'required',
                'style' => 'width:100%; height:100%',
                // 'data-brand' => $brands,
                ]) !!}
                {{-- <select class="myselect form-control" name="brand" id="brand" required>  --}}
                {{-- <option value=""></option> --}}
                {{-- @foreach($brands as $brand) --}}
                {{-- <option value="{{$brand->id}}"
                {{isset($stockcode)?$stockcode->brand_id == $brand->id  ? 'selected' : '' : ''}}>{{  $brand->descr}}
                </option> --}}
                {{-- <option value="{{$brand->id}}">{{$brand->code}}</option> --}}
                {{-- @endforeach --}}
                {{-- </select> --}}
                {{-- <input name="brand" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->brand_id:'' }}"
                > --}}
            </div>

            <label for="" class="col-md-2">Min Stk Qty</label>
            <div class="col-md-4">
                <input name="minstkqty" id="minstkqty" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->minstkqty)?number_format($stockcode->minstkqty, 2):'0.00' }}">
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Location<span class="text-danger">*</span></label>
            <div class="col-md-4">
                <select class="myselect form-control" name="location" id="location" required>
                    <option value=""></option>
                    @foreach($locations as $location)
                    <option value="{{$location->id}}"
                        {{isset($stockcode)?$stockcode->loc_id == $location->id  ? 'selected' : '' : ''}}>
                        {{ $location->code}} {{ $location->descr}}</option>
                    {{-- <option value="{{$location->id}}">{{$location->code}}</option> --}}
                    @endforeach
                </select>
                {{-- <input name="location" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->loc_id:'' }}"
                > --}}
            </div>

            <label for="" class="col-md-2">Max Stk Qty</label>
            <div class="col-md-4">
                <input name="maxstkqty" id="maxstkqty" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->maxstkqty)?number_format($stockcode->maxstkqty, 2):'0.00' }}">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">U.O.M. (1)<span class="text-danger">*</span></label>
            <div class="col-md-4">
                <select class="myselect form-control" name="uom1" id="uom1" required>
                    <option value=""></option>
                    @foreach($uoms as $uom)
                    <option value="{{$uom->id}}"
                        {{isset($stockcode)?$stockcode->uom_id1 == $uom->id  ? 'selected' : '' : ''}}>{{$uom->code}} {{$uom->descr}}
                    </option>
                    {{-- <option value="{{$uom->id}}">{{$uom->code}}</option> --}}
                    @endforeach
                </select>
                {{-- <input name="uom1" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->uom_id1:'' }}"
                > --}}
            </div>

            <label for="" class="col-md-2">Volume</label>
            <div class="col-md-4">
                <input name="volume" id="volume" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->volume)?number_format($stockcode->volume, 4):'1.0000' }}">
            </div>
        </div>
        @if(isset($systemsetup) && ($systemsetup->uom2 == "1"))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">U.O.M. (2)<span class="text-danger">*</span></label>
            <div class="col-md-4">
                <select class="myselect form-control" name="uom2" id="uom2" required>
                    <option value=""></option>
                    @foreach($uoms as $uom)
                    <option value="{{$uom->id}}"
                        {{isset($stockcode)?$stockcode->uom_id2 == $uom->id  ? 'selected' : '' : ''}}>{{$uom->code}}
                    </option>
                    @endforeach
                </select>
                {{-- <input name="uom2" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->uom_id2:'' }}"
                > --}}
            </div>

            <label for="" class="col-md-2">U.O.M. (2) Rate</label>
            <div class="col-md-4">
                <input name="uomrate2" id="uomrate2" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->uomrate2)?$stockcode->uomrate2:'1' }}">
            </div>
        </div>
        @endif
        @if(isset($systemsetup) && ($systemsetup->uom3 == "1"))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">U.O.M. (3)<span class="text-danger">*</span></label>
            <div class="col-md-4">
                <select class="myselect form-control" name="uom3" id="uom3" required>
                    <option value=""></option>
                    @foreach($uoms as $uom)
                    <option value="{{$uom->id}}"
                        {{isset($stockcode)?$stockcode->uom_id3 == $uom->id  ? 'selected' : '' : ''}}>{{$uom->code}}
                    </option>
                    @endforeach
                </select>
                {{-- <input name="uom3" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode)?$stockcode->uom_id3:'' }}"
                > --}}
            </div>

            <label for="" class="col-md-2">U.O.M. (3) Rate</label>
            <div class="col-md-4">
                <input name="uomrate3" id="uomrate3" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->uomrate3)?$stockcode->uomrate3:'1' }}">
            </div>
        </div>
        @endif
        <div class="input-group row narrow-padding-global">
            <label for="code" class="col-md-2"> Unit Cost </label>
            <div class="col-md-4">
                <input name="unitcost" id="unitcost" type="float" maxlength="100" class="form-control unitcost"
                    value="{{ isset($stockcode->unitcost)?number_format($stockcode->unitcost, 4):'0.0000' }}">
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Prev Cost</label>
            <div class="col-md-4">
                <input name="prevcost" id="prevcost" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->prevcost)?number_format($stockcode->prevcost, 4):'0.0000' }}">
            </div>
            {{-- <div class="col-md-5">
                    <div class="input-group row narrow-padding-global"> --}}
            {{-- <label for="" class="col-md-2">Stock Bal</label>
            <div class="col-md-4">
                <input name="stkbal" id="stkbal" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->stkbal)?number_format($stockcode->stkbal, 2):'0.00' }}">
            </div> --}}
            {{-- </div>
                </div> --}}
            {{-- <label for="" class="col-md-2">Currency</label>
            <div class="col-md-4">
                <input name="currency" id="currency" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode->currency)?$stockcode->currency:'' }}"
            >
        </div> --}}
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="" class="col-md-2">Ave Cost</label>
        <div class="col-md-4">
            <input name="avecost" id="avecost" type="float" maxlength="100" class="form-control"
                value="{{ isset($stockcode->avecost)?number_format($stockcode->avecost, 4):'0.0000' }}">
        </div>

        {{-- <div class="col-md-5">
                    <div class="input-group row narrow-padding-global"> --}}
        <label for="" class="col-md-2">Last Stck Check</label>
        <div class="col-md-4">
            <input name="laststkch" type="text" maxlength="100" class="form-control"
                value="{{ isset($stockcode)?$stockcode->laststkch:'' }}">
        </div>
        {{-- </div>
                </div> --}}
        {{-- <label for="" class="col-md-2">Currency Cost</label>
            <div class="col-md-4">
                <input name="curcost" type="text" maxlength="100" class="form-control" value="{{ isset($stockcode->avecost)?$stockcode->curcost:'' }}"
        >
    </div> --}}
</div>
</div>
<div class="col-md-4">
    <div class="input-group row narrow-padding-global">
        <label for="" class="col-md-4">Remark</label>
        <div class="col-md-12">
            <textarea name="remark" rows="7"
                class="form-control">{{ isset($stockcode)?$stockcode->remark:'' }}</textarea>
        </div>
    </div>
    <div class="input-group row narrow-padding-global">
        <label for="" class="col-md-4">Memo</label>
        <div class="col-md-12">
            <textarea name="memo" rows="7" class="form-control">{{ isset($stockcode)?$stockcode->memo:'' }}</textarea>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Min Price</label>
            <div class="col-md-4">
                <input name="minprice" id="minprice" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->minprice)?$stockcode->minprice:'0.00' }}">
            </div>
            <label for=""> .= </label>
            <div class="col-md-4">
                <input name="unitcost1" id="unitcost1" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->unitcost1)?number_format($stockcode->unitcost1, 2):'0.00' }}">
            </div>
            <label for=""> % * Unit Cost</label>
        </div>
        @if(($systemsetup->price1))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">{{ $systemsetup->price1 }}</label>
            <div class="col-md-4">
                <input name="price1" id="price1" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->price1)?number_format($stockcode->price1, 2):'0.00' }}">
            </div>
            <label for=""> .= </label>
            <div class="col-md-4">
                <input name="unitcost2" id="unitcost2" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->unitcost2)?number_format($stockcode->unitcost2, 2):'0.00' }}">
            </div>
            <label for=""> % * Unit Cost</label>
        </div>
        @endif
        @if(($systemsetup->price2))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">{{ $systemsetup->price2 }}</label>
            <div class="col-md-4">
                <input name="price2" id="price2" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->price2)?number_format($stockcode->price2, 2):'0.00' }}">
            </div>
            <label for=""> .= </label>
            <div class="col-md-4">
                <input name="unitcost3" id="unitcost3" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->unitcost3)?number_format($stockcode->unitcost3, 2):'0.00' }}">
            </div>
            <label for=""> % * Unit Cost</label>
        </div>
        @endif
        @if(($systemsetup->price3))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">{{ $systemsetup->price3 }}</label>
            <div class="col-md-4">
                <input name="price3" id="price3" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->price3)?number_format($stockcode->price3, 2):'0.00' }}">
            </div>
            <label for=""> .= </label>
            <div class="col-md-4">
                <input name="unitcost4" id="unitcost4" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->unitcost4)?number_format($stockcode->unitcost4, 2):'0.00' }}">
            </div>
            <label for=""> % * Unit Cost</label>
        </div>
        @endif
        @if(($systemsetup->price4))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">{{ $systemsetup->price4 }}</label>
            <div class="col-md-4">
                <input name="price4" id="price4" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->price4)?number_format($stockcode->price4, 2):'0.00' }}">
            </div>
            <label for=""> .= </label>
            <div class="col-md-4">
                <input name="unitcost5" id="unitcost5" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->unitcost5)?number_format($stockcode->unitcost5, 2):'0.00' }}">
            </div>
            <label for=""> % * Unit Cost</label>
        </div>
        @endif
        @if(($systemsetup->price5))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">{{ $systemsetup->price5 }}</label>
            <div class="col-md-4">
                <input name="price5" id="price5" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->price5)?number_format($stockcode->price5, 2):'0.00' }}">
            </div>
            <label for=""> .= </label>
            <div class="col-md-4">
                <input name="unitcost6" id="unitcost6" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->unitcost6)?number_format($stockcode->unitcost6, 2):'0.00' }}">
            </div>
            <label for=""> % * Unit Cost</label>
        </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        @if(($systemsetup->price6))
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">{{ $systemsetup->price6 }}</label>
            <div class="col-md-4">
                <input name="price6" id="price6" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->price6)?number_format($stockcode->price6, 2):'0.00' }}">
            </div>
            <label for=""> .= </label>
            <div class="col-md-4">
                <input name="unitcost7" id="unitcost7" type="float" maxlength="100" class="form-control"
                    value="{{ isset($stockcode->unitcost7)?number_format($stockcode->unitcost7, 2):'0.00' }}">
            </div>
            <label for=""> % * Unit Cost</label>

        </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <div class="col-md-6">
                <input type="checkbox" name="inactive" value="1"
                    {{ isset($stockcode)?($stockcode->inactive=='1')? 'checked' : '': ''}}> Inactive<br>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <div class="col-md-6">
                <a class="btn btn-info" id="testButton">ReCalculate Price</a>
            </div>
        </div>
    </div>
</div>