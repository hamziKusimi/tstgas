@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('reasons.store') }}" method="POST">
            @csrf
            @include('stockmaster/reasons/form')

        </form>
    </div>
</div>


@endsection
