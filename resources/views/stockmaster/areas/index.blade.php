@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('IV_MD_AREA_CR'))
<div class="row">
    <div class="col-4">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('areas.create') }}"> Add Area</a>
        </div>
    </div>
    {{-- <div class="col-4">
    </div>
    <div class="col-2" style="left: 50px;">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" style="width: 100%;" target="_blank" href="{{ route('area.print') }}"> Print (pdf)</a>
        </div>
    </div>
    <div class="col-2">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" style="width: 100%;" target="_blank" href="{{ route('area.download') }}"> Download
                (csv)</a>
        </div>
    </div> --}}
</div>
@endif

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif

@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="areaTab" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Description</th>
                            <th width="280px">More</th>
                        </tr>
                    </thead>
                    @if(sizeof($areas) > 0)
                    <tbody>
                        @foreach ($areas as $area)
                        <tr>

                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_AREA_UP'))
                                <td><a href="{{ route('areas.edit', $area->id) }}">{{ $area->code }}</a></td>
                            @else
                                <td>
                                    {{ $area->code }}
                                </td>
                            @endif
                            {{-- <td>{{ $area->code}}</td> --}}
                            <td>{{ $area->descr }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_AREA_DL'))
                                <td>
                                    <a href="{{ route('areas.destroy', $area->id) }}" data-method="delete"
                                        data-confirm="Confirm delete this account?">
                                        <i class="fa fa-trash"></i></a>
                                </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
                @if(Auth::user()->hasPermissionTo('IV_MD_AREA_PR') && $print->isEmpty())
                    <div id="test" class="exist"></div>
                    
                @elseif(Auth::user()->hasPermissionTo('IV_MD_AREA_PRY'))
                    <div id="test" class="exist"></div>
                @endif
            </div>
        </div>
    </div>
</div>

{{-- {!! $areas->links() !!} --}}

@endsection

@push('scripts')
<script>
$(document).ready(function() {
    let test = $('.card-body').find('.exist')
    if((test.length>0)){  
        $('#areaTab').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    }
      
} );
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
@endpush