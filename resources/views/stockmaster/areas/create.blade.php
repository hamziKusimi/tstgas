@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('areas.store') }}" method="POST">
            @csrf
            @include('stockmaster/areas/form')

        </form>
    </div>
</div>


@endsection
