@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('adjustmentits.store') }}" method="POST">
            @csrf
            @include('stockmaster/adjustmentits/form')

        </form>
    </div>
</div>

@endsection
