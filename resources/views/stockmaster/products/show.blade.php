@extends('master')
@section('content')

<div class="col-md-12">
    <div class="box box-solid box-default">
        <div class="box-header with-border">Product</div>

        <div class="box-body with-border">
            <div class="form-group row">
                <label for="code" class="col-md-2"> Code : </label>
                <label for="code" class="col-md-10"> {{  $product->code }}</label>
                
            </div>
            
            <div class="form-group row">
                <label for="description" class="col-md-2">Description : </label>
                <label for="description" class="col-md-10"> {{  $product->descr }}</label>
            </div>
            
            <div class="form-group row">
                <label for="descr" class="col-md-2">Active : </label>
                <label for="descr" class="col-md-10">{{  $product->active }}</label>
            </div>
        </div>
    </div>
</div>


@endsection
