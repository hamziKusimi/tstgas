@extends('master')
@section('content')



<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('locations.update', $location->id) }}" method="POST">
            <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('stockmaster/locations/form')

        </form>
    </div>
</div>

@endsection
