@extends('master')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="box box-solid box-default">
                <div class="box-header with-border">Edit equipment</div>

                <div class="box-body with-border">
                    <form action ="{{ route('equipments.update', $equipment->id) }}" method="POST">
                        <input type="hidden" name="_method" value="PUT"/>
                        @csrf
                        @include('stockmaster/equipments/form')

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
