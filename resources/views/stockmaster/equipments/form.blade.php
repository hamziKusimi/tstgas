
<div class="row ">
    <div class="col-md-12">    
        <div class="row">
            <div class=col-md-12>
                <div class="form-group">
                    <label for="code" class="col-md-2"> Code </label>
                    <div class="col-md-10">
                        <input name="code" type="text" class="form-control" value="" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="name" class="col-md-2">Name</label>
                    <div class="col-md-10">
                        <input name="name" type="text" class="form-control" value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="srlno" class="col-md-2"> Serial No </label>
                    <div class="col-md-10">
                        <input name="srlno" type="text" class="form-control" value="" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="egsrlno" class="col-md-2">Engine Serial No</label>
                    <div class="col-md-10">
                        <input name="egsrlno" type="text" class="form-control" value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="cpty" class="col-md-2"> Capacity </label>
                    <div class="col-md-10">
                        <input name="cpty" type="text" class="form-control" value="" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="product" class="col-md-2">Product</label>
                    <div class="col-md-10">
                        <input name="product" type="text" class="form-control" value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="category" class="col-md-2"> Category </label>
                    <div class="col-md-10">
                        <input name="category" type="text" class="form-control" value="" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="brand" class="col-md-2">Brand</label>
                    <div class="col-md-10">
                        <input name="brand" type="text" class="form-control" value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="location" class="col-md-2"> Location </label>
                    <div class="col-md-10">
                        <input name="location" type="text" class="form-control" value="" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="owner" class="col-md-2">Owner</label>
                    <div class="col-md-10">
                        <input name="owner" type="text" class="form-control" value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="accessories" class="col-md-2">Accessories </label>
                    <div class="col-md-10">
                        <input name="accessories" type="text" class="form-control" value="" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="remark" class="col-md-2">Remark</label>
                    <div class="col-md-10">
                        <input name="remark" type="text" class="form-control" value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-md-2"> Status </label>
                    <div class="col-md-10">
                        <input name="status" type="text" class="form-control" value="" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="dailyrental" class="col-md-2">Daily Rental</label>
                    <div class="col-md-10">
                        <input name="dailyrental" type="text" class="form-control" value="" >
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>