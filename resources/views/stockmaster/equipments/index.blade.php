@extends('master')

@section('content')

<div class="row">
    <div class="col-4">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('equipments.create') }}"> Add Equipment</a>
        </div>

    </div>
    {{-- <div class="col-4">
    </div>
    <div class="col-2" style="left: 50px;">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" style="width: 100%;" href="{{ route('adjustmentit.print') }}"> Print (pdf)</a>
        </div>
    </div>
    <div class="col-2">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" style="width: 100%;" href="{{ route('adjustmentit.download') }}"> Download
                (csv)</a>
        </div>
    </div> --}}
</div>

{{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
</div>
@endif --}}


@if (Session::has('Success'))
<div class="alert alert-success alert-dismissible mt-2 mb-0">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

    <h5><i class="icon fa fa-ban"></i> Success </h5>

    <p>{{ Session::get('Success') }}</p>
</div>
@endif
@php ($i = 0)
@if(sizeof($equipment) > 0)

<div class="table-responsive">
    <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Code</th>
                <th>Description</th>
                <th width="280px">More</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($equipment as $eqp)
            <tr>

                <td>{{ ++$i }}</td>
                <td><a href="{{ route('equipments.edit',$eqp->id) }}">{{ $eqp->code }}</a></td>
                {{-- <td>{{ $eqp->code}}</td> --}}
                <td>{{ $eqp->descr }}</td>
                <td>
                    <form action="{{ route('equipments.destroy',$eqp->id) }}" method="POST">

                        {{-- <a class="btn btn-info" href="{{ route('equipments.show',$eqp->id) }}">Show</a> --}}
                        {{-- <a class="btn btn-primary" href="{{ route('equipments.edit',$eqp->id) }}">Edit</a> --}}

                        @csrf
                        @method('DELETE')

                        <button onclick="return confirm('Confirm to Delete?')" type="submit"><i
                                class="fa fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
<div class="alert alert-alert">Start Adding to the Database.</div>
@endif


{{-- {!! $equipment->links() !!} --}}

@endsection
{{-- @push('script') --}}
@section('js')

<script>
    // $(document).ready(function() {
    //     $('#example').DataTable();
    // } );
</script>
<script>
    $(document).ready(function() {
            $('#example').DataTable({
            });
        } );
</script>
{{-- @endpush --}}
@endsection