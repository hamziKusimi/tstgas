@extends('master')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="box box-solid box-default">
                <div class="box-header with-border">Add equipment</div>

                <div class="box-body with-border">
                    <form action ="{{ route('equipments.store') }}" method="POST">
                        @csrf
                        @include('stockmaster/equipments/form')

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
