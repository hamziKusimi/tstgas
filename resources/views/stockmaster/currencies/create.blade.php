@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('currencies.store') }}" method="POST">
            @csrf
            @include('stockmaster/currencies/form')

        </form>
    </div>
</div>


@endsection
