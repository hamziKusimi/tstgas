@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('IV_MD_ADJUSTMENT_O_CR'))
<div class="row">
    <div class="col-4">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('adjustmentots.create') }}"> Add Adjustment Out Type</a>
        </div>
    </div>
    {{-- <div class="col-4">
    </div>
    <div class="col-2" style="left: 50px;">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" style="width: 100%;" target="_blank" href="{{ route('adjustmentot.print') }}"> Print (pdf)</a>
        </div>
    </div>
    <div class="col-2">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" style="width: 100%;" target="_blank" href="{{ route('adjustmentot.download') }}"> Download
                (csv)</a>
        </div>
    </div> --}}
</div>
@endif

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif

@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="adjTab" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Description</th>
                            <th width="280px">More</th>
                        </tr>
                    </thead>
                    @if(sizeof($adjustmentots) > 0)
                    <tbody>
                        @foreach ($adjustmentots as $adjustmentot)
                        <tr>

                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_ADJUSTMENT_O_UP'))
                            <td><a
                                    href="{{ route('adjustmentots.edit', $adjustmentot->id) }}">{{ $adjustmentot->code }}</a>
                            </td>
                            @else
                                <td>{{ $adjustmentot->code }}</td>
                            @endif
                            {{-- <td>{{ $adjustmentot->code}}</td> --}}
                            <td>{{ $adjustmentot->descr }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_ADJUSTMENT_O_DL'))
                                <td>
                                    <a href="{{ route('adjustmentots.destroy', $adjustmentot->id) }}" data-method="delete"
                                        data-confirm="Confirm delete this account?">
                                        <i class="fa fa-trash"></i></a>
                                </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
{{-- {!! $adjustmentots->links() !!} --}}

@endsection

@push('scripts')
<script>
$(document).ready(function() {

            $('#adjTab').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        } );
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
@endpush