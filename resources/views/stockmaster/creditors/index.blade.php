@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('IV_MD_CREDITOR_CR'))
    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('creditors.create') }}"> Add Creditor </a>
        </div>
    </div>
@endif

    @if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>

    @endif

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="example">
                    <thead class="">
                        <th width="25%">A/C Code</th>
                        <th width="30%">Name</th>
                        <th width="10%">Tel</th>
                        <th width="10%">Fax</th>
                        <th width="10%">Email</th>
                        <th width="15%"></th>
                    </thead>
                    @if(sizeof($creditors) > 0)        
                    <tbody>
                        @foreach ($creditors as $creditor)
                        <tr>
                            @if(Auth::user()->hasPermissionTo('IV_MD_CREDITOR_UP'))
                            <td><a href="{{ route('creditors.edit',$creditor->id) }}">{{ $creditor->accountcode }}</a></td>
                            @else
                            <td>{{ $creditor->accountcode }}</td>
                            @endif
                            <td>{{ $creditor->name }}</td>
                            <td>{{ $creditor->tel }}</td>
                            <td>{{ $creditor->fax }}</td>
                            <td>{{ $creditor->email }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_CREDITOR_DL'))
                                <td>
                                    <a href="{{ route('creditors.destroy', $creditor->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>  
                                </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                     @endif
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endpush
