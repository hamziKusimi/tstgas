<div class="form-group">
        {!! Form::label('C_ACODE', 'Account Code', ['class' => 'col-form-label']) !!} <span class="text-danger">*</span>
        <small class="text-danger">
            <span class="d-acode-message"></span>
        </small>
        <div class="input-group">
            {!! Form::text('C_ACODE', isset($creditor)?$creditor->accountcode:null, [ 'class' => 'form-control acode-input',
                'id' => 'C_ACODE',
                'placeholder' => 'Please create a new account code',
                'required',
                isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                // 'data-url' => route('debtors.api.find.acode'),
                'data-existing-accounts' => null,
            ]) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('C_NAME', 'Name', ['class' => 'col-form-label']) !!} <span class="text-danger">*</span>
        {!! Form::text('C_NAME', isset($creditor)?$creditor->name:null, ['class' => 'form-control form-data', 'required', 'id' => 'C_NAME',
        'placeholder' => 'Name']) !!}
    </div>
    
    <br>
    <div class="form-group row">		
        <div class="col-md-12">
            <div class="radio-list">
                <label class="radio-inline">
                <input type="radio" name="active" value="1" {{ isset($creditor)?($creditor->active=='1')? 'checked' : '': 'checked' }}/> Active </font></label>
                <label class="radio-inline">
                <input type="radio" name="active" value="0" {{ isset($creditor)?($creditor->active=='0')? 'checked' : '': '' }}/> Non-Active </font></label>
            </div>
        </div>
    </div>
    <br>
    <div class="form-group">
        {!! Form::label('C_ADDRESS', 'Address', ['class' => 'col-form-label']) !!}
        {!! Form::textarea('C_ADDRESS', isset($creditor)?$creditor->address1:null, ['class' => 'form-control form-data', 'rows' => '4', 'id' => 'C_ADDRESS']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('contact', 'Contact', ['class' => 'col-form-label']) !!}
        <div class="row">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><div class="input-group-text"><i class="fa fa-phone"></i></div></div>
                    {!! Form::text('C_PHONE', isset($creditor)?$creditor->tel:null, ['class' => 'form-control form-data', 'id' => 'C_PHONE',
                    'placeholder' => 'Phone No']) !!}
                </div>
            </div>
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><div class="input-group-text"><i class="fa fa-phone"></i></div></div>
                    {!! Form::text('C_HP', isset($creditor)?$creditor->hp:null, ['class' => 'form-control form-data', 'id' => 'C_HP',
                    'placeholder' => 'Handphone No']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><div class="input-group-text"><i class="fa fa-fax"></i></div></div>
                    {!! Form::text('C_FAX', isset($creditor)?$creditor->fax:null, ['class' => 'form-control form-data', 'id' => 'C_FAX',
                    'placeholder' => 'Fax']) !!}
                </div>
            </div>
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><div class="input-group-text"><i class="fa fa-envelope"></i></div></div>
                    {!! Form::text('C_EMAIL', isset($creditor)?$creditor->email:null, ['class' => 'form-control form-data', 'id' => 'C_EMAIL',
                    'placeholder' => 'Email']) !!}
                </div>
            </div>
        </div>
    </div>

    <a href="#" data-toggle="collapse" data-target="#account-create-more-details">Click to show more</a>
    <div id="account-create-more-details" class="collapse">
        <div class="form-group">
            <div class="row">
                <div class="col">
                    {!! Form::label('C_CTERM', 'Credit Term', ['class' => 'col-form-label']) !!}
                    {!! Form::text('C_CTERM', isset($creditor)?$creditor->cterm:null, ['class' => 'form-control form-data', 'id' => 'C_CTERM']) !!}
                </div>
                <div class="col">
                    {!! Form::label('C_CLIMIT', 'Credit Limit', ['class' => 'col-form-label']) !!}
                    {!! Form::text('C_CLIMIT', isset($creditor)?$creditor->climit:null, ['class' => 'form-control form-data money-default', 'placeholder' => '0.00', 'id' => 'C_CLIMIT']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::label('d_gstno', 'GST No.', ['class' => 'col-form-label']) !!}
                    {!! Form::text('d_gstno', isset($creditor)?$creditor->gstno:null, ['class' => 'form-control form-data', 'id' => 'C_GSTNO']) !!}
                </div>
                <div class="col">
                    {!! Form::label('d_brn', 'BRN No.', ['class' => 'col-form-label']) !!}
                    {!! Form::text('d_brn', isset($creditor)?$creditor->brnno:null, ['class' => 'form-control form-data', 'id' => 'C_BRN']) !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('C_MEMO', 'Memo', ['class' => 'col-form-label']) !!}
            {!! Form::textarea('C_MEMO', isset($creditor)?$creditor->memo:null, ['class' => 'form-control form-data', 'rows' => '3', 'id' => 'C_MEMO']) !!}
        </div>
    </div>
