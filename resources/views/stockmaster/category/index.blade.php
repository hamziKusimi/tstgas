@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('IV_MD_CATEGORY_CR'))
<div class="row">
    <div class="col-4">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('category.create') }}"> Add Category</a>
        </div>
    </div>
    {{-- <div class="col-4">
    </div>
    <div class="col-2" style="left: 50px;">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" target="_blank" style="width: 100%;" href="{{ route('category.print') }}"> Print
                (pdf)</a>
        </div>
    </div>
    <div class="col-2">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-primary" target="_blank" style="width: 100%;" href="{{ route('category.download') }}">
                Download (csv)</a>
        </div>
    </div> --}}
</div>
@endif


{{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
</div>
@endif --}}


@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>

    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="categoryTab" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Description</th>

                            @if (($systemSetup->set_category1) == "true")
                            <th>DR Cash Purchase</th>
                            @endif

                            @if (($systemSetup->set_category2) == "true")
                            <th>DR Credit Purchase</th>
                            @endif

                            @if (($systemSetup->set_category3) == "true")
                            <th>CR Purchase Return</th>
                            @endif

                            @if (($systemSetup->set_category5) == "true")
                            <th>CR Cash Sales</th>
                            @endif

                            @if (($systemSetup->set_category6) == "true")
                            <th>DR Cash Sales Return</th>
                            @endif

                            @if (($systemSetup->set_category7) == "true")
                            <th>CR Invoice Sales</th>
                            @endif

                            @if (($systemSetup->set_category8) == "true")
                            <th>DR Credit Sales Return</th>
                            @endif
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    @if(sizeof($category) > 0)
                    <tbody>
                        @foreach ($category as $cat)
                        <tr>
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('IV_MD_CATEGORY_UP'))
                                <td><a href="{{ route('category.edit',$cat->id) }}">{{ $cat->code }}</a></td>
                            @else
                                <td>{{ $cat->code }}</td>
                            @endif
                            <td>{{ $cat->descr }}</td>

                            @if (($systemSetup->set_category1) == "true")
                            <td>{{ $cat->dr_cashpurchase_acc }}</td>
                            @endif

                            @if (($systemSetup->set_category2) == "true")
                            <td>{{ $cat->dr_creditpurcase_acc }}</td>
                            @endif

                            @if (($systemSetup->set_category3) == "true")
                            <td>{{ $cat->cr_purchasereturn_acc }}</td>
                            @endif

                            @if (($systemSetup->set_category5) == "true")
                            <td>{{ $cat->cr_cashsales_acc }}</td>
                            @endif

                            @if (($systemSetup->set_category6) == "true")
                            <td>{{ $cat->dr_cashsales_return_acc }}</td>
                            @endif

                            @if (($systemSetup->set_category7) == "true")
                            <td>{{ $cat->cr_invoicesales_acc }}</td>
                            @endif

                            @if (($systemSetup->set_category8) == "true")
                            <td>{{ $cat->dr_creditsales_return_acc }}</td>
                            @endif

                            @if(Auth::user()->hasPermissionTo('IV_MD_CATEGORY_DL'))
                                <td class="text-center">
                                    @if(!$cat->isUsedInStockcode())
                                    <a href="{{ route('category.destroy', $cat->id) }}" data-method="delete"
                                        data-confirm="Confirm delete this account?">
                                        <i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                    {{-- @else --}}
                    {{-- <div class="alert alert-alert">Start Adding to the Database.</div> --}}
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>


{{-- {!! $category->links() !!} --}}

@endsection

@push('scripts')
<script>
$(document).ready(function() {

            $('#categoryTab').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        } );
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
@endpush

