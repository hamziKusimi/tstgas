@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('category.store') }}" method="POST" id="create-form">
            @csrf
            @include('stockmaster/category/form')

        </form>
    </div>
</div>


@endsection
@push('scripts')
    <script type="text/javascript">
        $(function () {
            $("#create-form").submit(function () {
                $("#btnSubmit").attr("disabled", true);
                return true;
            });
        });
    </script>
@endpush
