
<div class="form-group row">
        <label for="code" class="col-md-2"> Code <span class="text-danger">*</span></label>
        <div class="col-md-10">
            <input name="code" maxlength="50" type="text" class="form-control" value="{{ isset($category)?$category->code:'' }}" required>
        </div>
    </div>

    <div class="form-group row">
        <label for="description" class="col-md-2">Description <span class="text-danger">*</span></label>
        <div class="col-md-10">
            <input name="descr" maxlength="100" type="text" class="form-control" value="{{ isset($category)?$category->descr:'' }}" required>
        </div>
    </div><br>


    @if (($systemSetup->set_category1) == "true")
    <div class="form-group row">
        <label for="CashPurchaseAcc" class="col-md-2">DR Cash Purchase Account</label>
        <div class="col-md-10">
            <input name="dr_cashpurchase_acc" maxlength="100" type="text" class="form-control"
               data-inputmask="'mask': '*****-**'"
            value="{{ isset($category)?$category->dr_cashpurchase_acc:'' }}">
        </div>
    </div>
    @endif

    @if (($systemSetup->set_category2) == "true")
    <div class="form-group row">
        <label for="CreditPurchaseAcc" class="col-md-2">DR Credit Purchase Account</label>
        <div class="col-md-10">
            <input name="dr_creditpurchase_acc" maxlength="100" type="text" class="form-control"
               data-inputmask="'mask': '*****-**'"
            value="{{ isset($category)?$category->dr_creditpurcase_acc:'' }}">
        </div>
    </div>
    @endif

    @if (($systemSetup->set_category3) == "true")
    <div class="form-group row">
        <label for="PurchaseReturnAcc" class="col-md-2">CR Purchase Return Account</label>
        <div class="col-md-10">
            <input name="cr_purchasereturn_acc" maxlength="100" type="text" class="form-control"
               data-inputmask="'mask': '*****-**'"
            value="{{ isset($category)?$category->cr_purchasereturn_acc:'' }}">
        </div>
    </div>
    @endif

    @if (($systemSetup->set_category5) == "true")
    <div class="form-group row">
        <label for="CashSalesAcc" class="col-md-2">CR Cash Sales Account</label>
        <div class="col-md-10">
            <input name="cr_cashsales_acc" maxlength="100" type="text" class="form-control"
               data-inputmask="'mask': '*****-**'"
            value="{{ isset($category)?$category->cr_cashsales_acc:'' }}">
        </div>
    </div>
    @endif

    @if (($systemSetup->set_category6) == "true")
    <div class="form-group row">
        <label for="SalesReturnAcc" class="col-md-2">DR Cash Sales Return Account</label>
        <div class="col-md-10">
            <input name="dr_cashsales_return_acc" maxlength="100" type="text" class="form-control"
               data-inputmask="'mask': '*****-**'"
            value="{{ isset($category)?$category->dr_cashsales_return_acc:'' }}">
        </div>
    </div>
    @endif

    @if (($systemSetup->set_category7) == "true")
    <div class="form-group row">
        <label for="InvoiceSalesAcc" class="col-md-2">CR Invoice Sales Account</label>
        <div class="col-md-10">
            <input name="cr_invoicesales_acc" maxlength="100" type="text" class="form-control"
               data-inputmask="'mask': '*****-**'"
            value="{{ isset($category)?$category->cr_invoicesales_acc:'' }}">
        </div>
    </div>
    @endif

    @if (($systemSetup->set_category8) == "true")
    <div class="form-group row">
        <label for="CreditSalesReturnAcc" class="col-md-2">DR Credit Sales Return Account</label>
        <div class="col-md-10">
            <input name="dr_creditsales_return_acc" maxlength="100" type="text" class="form-control"
               data-inputmask="'mask': '*****-**'"
            value="{{ isset($category)?$category->dr_creditsales_return_acc:'' }}">
        </div>
    </div>
    @endif

    <div class="form-group row">
        <label class="control-label col-md-2">Active</label>
        <div class="col-md-10">
            <div class="radio-list">
                <label class="radio-inline">
                <input type="radio" name="active" value="1" {{ isset($category)?($category->active=='1')? 'checked' : '': 'checked' }}/> Active </font></label>
                <label class="radio-inline">
                <input type="radio" name="active" value="0" {{ isset($category)?($category->active=='0')? 'checked' : '': '' }}/> Non-Active </font></label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
    </div>