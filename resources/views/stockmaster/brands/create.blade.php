@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('brands.store') }}" method="POST">
            @csrf
            @include('stockmaster/brands/form')

        </form>
    </div>
</div>


@endsection
