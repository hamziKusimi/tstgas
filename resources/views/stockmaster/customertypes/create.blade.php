@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <form action ="{{ route('customertypes.store') }}" method="POST">
            @csrf
            @include('stockmaster/customertypes/form')

        </form>
    </div>
</div>


@endsection
