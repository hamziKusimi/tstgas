@extends('master')
@section('content')

<div class="alert red-alert text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist"></p>
</div>

<div class="alert red-alert2 text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist2"></p>
</div>

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('cylindercategory.store') }}" method="POST">
            @csrf
            @include('cylindermaster/cylindercategory/form')

        </form>
    </div>
</div>


@endsection

@push('scripts')
    <script type="text/javascript">
        $('#code').on('change', function(){
            let data = $(this).val()
            data = data.trim()
            let originalCode = $('#codecoy').val()
            originalCode = originalCode.trim()
            let broute = "{{ route('cylindercategory.get.code', 'CODE') }}"
            let route = broute.replace('CODE', data)
            
            $.ajax({
                    url: route,
                    method: "GET",
                    data: '',
                }).done(function (response) {
                                
                    if(response.length){
                        let selectedEmail = response[0]
                        
                        if(data.toLowerCase() == selectedEmail.code.trim().toLowerCase() && data.toLowerCase() != originalCode.toLowerCase()){
                        //    alert('Code already exist!\nPlease Create a new Code')
                            $('.alert-exist').text('Code already exist! Please Create a new Code')
                            $(".red-alert").removeClass('d-none')
                        //    $('#code').val('')
                            $('.btn-primary').prop('disabled',true)
                            console.log('here')
                        }else{
                            $(".red-alert").addClass('d-none')
                            $('.btn-primary').prop('disabled',false)
                            console.log('there')

                        }
                    }else{
                        $(".red-alert").addClass('d-none')
                        $('.btn-primary').prop('disabled',false)
                            console.log('new')
                    }
                }).fail(function (response) {
                    console.log('heree')
                })

        })
    </script>
@endpush    