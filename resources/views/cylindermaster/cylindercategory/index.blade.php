@extends('master')

@section('content')

    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('cylindercategory.create') }}"> Add Category</a>
        </div>
    </div>

    
    @if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>

    @endif

@php ($i = 0)
       
<div class="card">
    <div class="card-body">
        <div class="bg-white">        
            <div class="table-responsive">
                <table id="catTab" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Description</th>
                            <th width="280px">More</th>
                        </tr>
                    </thead>
                @if(sizeof($cylindercategory) > 0)    
                    <tbody>
                        @foreach ($cylindercategory as $cylindercat)
                        <tr>
                        
                            <td>{{ ++$i }}</td>
                            <td><a href="{{ route('cylindercategory.edit',$cylindercat->id) }}">{{ $cylindercat->code }}</a></td>
                            {{-- <td>{{ $cylindercat->code}}</td> --}}
                            <td>{{ $cylindercat->descr }}</td>
                            <td>
                                <a href="{{ route('cylindercategory.destroy', $cylindercat->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                <i class="fa fa-trash"></i></a>  
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                @endif
                </table>
            </div>
        </div>
    </div>
</div>


    {{-- {!! $cylindercategory->links() !!} --}}

@endsection

@push('scripts')
<script>
$(document).ready(function() {

            $('#catTab').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        } );
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
@endpush

