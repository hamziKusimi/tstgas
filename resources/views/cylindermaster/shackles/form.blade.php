
<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <label for="barcode" class="col-md-2">Barcode</label>
            <div class="col-md-10">
                <input name="barcode" type="text" maxlength="12" class="form-control barcode" value="{{ isset($shackle)?$shackle->barcode:'' }}" data-values="{{ $shackles }}">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="serial" class="col-md-2"> Serial No <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input name="serial" type="text" maxlength="12" class="form-control serial" value="{{ isset($shackle)?$shackle->serial:'' }}" data-items="{{ $shackles }}" required>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="mfr" class="col-md-2">Name of Manufacturer</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <select class="myselect form-control mfr" style="width:100%" name="mfr" id="mfr" data-values="{{ $manufacturers }}"> 
                        <option value=""></option>
                        @foreach($manufacturers as $manufacturer)
                        <option value="{{ $manufacturer->id }}" {{ isset($shackle)?$shackle->mfr == $manufacturer->id  ? 'selected' : '' : ''}}>{{ $manufacturer->code }}, {{ $manufacturer->descr }}</option>
                        @endforeach
                    </select>
                    {{-- <input name="mfr" type="hidden" maxlength="100" class="form-control mfr" value="{{ isset($shackle)?$shackle->mfr:'' }}" > --}}
                </div>
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="owner" class="col-md-2"> Name of Owner </label>
            <div class="col-md-10">
                <input name="owner" type="text" maxlength="100" class="form-control" value="{{ isset($shackle)?$shackle->owner:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="loadlimit" class="col-md-2"> Working Load Limit </label>
            <div class="col-md-10">
                    <input name="loadlimit" type="text" maxlength="100" class="form-control" value="{{ isset($shackle)?$shackle->loadlimit:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="type" class="col-md-2">Type of Shackle</label>
            <div class="col-md-10">
                <select class="myselect form-control" name="type" id="type"> 
                    <option value=""></option>
                    @foreach($shackletypes as $shackletype)
                    <option value="{{$shackletype->id}}" {{ isset($shackle)?$shackle->type == $shackletype->id  ? 'selected' : '' : ''}}>{{$shackletype->code}}, {{ $shackletype->descr }}</option>
                    @endforeach
                </select>
                {{-- <input name="type" type="text" maxlength="100" class="form-control" value="{{ isset($shackle)?$shackle->type:'' }}" > --}}
            </div>
        </div>


        <div class="input-group row narrow-padding-global">
            <label for="size" class="col-md-2">Shackle Size</label>
            <div class="col-md-10">
                <input name="size" type="text" maxlength="100" class="form-control" value="{{ isset($shackle)?$shackle->size:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="descr" class="col-md-2">Description of Gear</label>
            <div class="col-md-10">
                <input name="descr" type="text" maxlength="100" class="form-control" value="{{ isset($shackle)?$shackle->descr:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="coc" class="col-md-2">COC</label>
            <div class="col-md-10">
                <input name="coc" type="text" maxlength="100" class="form-control" value="{{ isset($shackle)?$shackle->coc:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="testdate" class="col-md-2">Test Date</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-calendar"></i></small>
                    </div>
                    <input type="text" maxlength="100" name="testdate" id="datepicker" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($shackle)?date('d-m-Y', strtotime($shackle->testdate)):'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-form-submit">Submit</button>
            </div>
        </div>
    </div>
</div>