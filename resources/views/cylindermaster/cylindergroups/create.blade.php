@extends('master')
@section('content')

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('cylindergroups.store') }}" method="POST">
            @csrf
            @include('cylindermaster/cylindergroups/form')

        </form>
    </div>
</div>


@endsection
