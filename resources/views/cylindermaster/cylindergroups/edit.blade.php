@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('cylindergroups.update', $cylindergroup->id )}}" method="POST">
        <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('cylindermaster/cylindergroups/form')

        </form>
    </div>
</div>


@endsection
