
<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <label for="barcode" class="col-md-2">Barcode</label>
            <div class="col-md-10">
                <input name="barcode" type="text" maxlength="12" class="form-control barcode" value="{{ isset($sling)?$sling->barcode:'' }}" data-values="{{ $slings }}">
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="serial" class="col-md-2"> Serial No <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input name="serial" type="text" maxlength="12" class="form-control serial" value="{{ isset($sling)?$sling->serial:'' }}" data-items="{{ $slings }}" required>
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
                <label for="mfr" class="col-md-2">Name of Manufacturer</label>
                <div class="col-md-10">
                    <div class="input-group-prepend">
                        <select class="myselect form-control mfr" style="width:100%" name="mfr" id="mfr" data-values="{{ $manufacturers }}"> 
                            <option value=""></option>
                            @foreach($manufacturers as $manufacturer)
                            <option value="{{ $manufacturer->id }}" {{ isset($sling)?$sling->mfr == $manufacturer->id  ? 'selected' : '' : ''}}>{{ $manufacturer->code }}, {{ $manufacturer->descr }}</option>
                            @endforeach
                        </select>
                        {{-- <input name="mfr" type="hidden" maxlength="100" class="form-control mfr" value="{{ isset($sling)?$sling->mfr:'' }}" > --}}
                    </div>
                </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="owner" class="col-md-2"> Name of Owner </label>
            <div class="col-md-10">
                <input name="owner" type="text" maxlength="100" class="form-control" value="{{ isset($sling)?$sling->owner:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="type" class="col-md-2">Type of Sling Set (xxx)</label>
            <div class="col-md-10">
                <select class="myselect form-control" name="type" id="type"> 
                    <option value=""></option>
                    @foreach($slingtypes as $slingtype)
                    <option value="{{$slingtype->id}}" {{ isset($sling)?$sling->type == $slingtype->id  ? 'selected' : '' : ''}}>{{$slingtype->code}}, {{ $slingtype->descr }}</option>
                    @endforeach
                </select>
                {{-- <input name="type" type="text" maxlength="100" class="form-control" value="{{ isset($sling)?$sling->type:'' }}" > --}}
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="loadlimit" class="col-md-2"> Working Load Limit </label>
            <div class="col-md-10">
                    <input name="loadlimit" type="text" maxlength="100" class="form-control" value="{{ isset($sling)?$sling->loadlimit:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="dimension" class="col-md-2">Dimension</label>
            <div class="col-md-10">
                <input name="dimension" type="text" maxlength="100" class="form-control" value="{{ isset($sling)?$sling->dimension:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="legged" class="col-md-2">Legged</label>
            <div class="col-md-10">
                <input name="legged" type="text" maxlength="100" class="form-control" value="{{ isset($sling)?$sling->legged:'' }}" >
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="certno" class="col-md-2"> Certificate No </label>
            <div class="col-md-10">
                <input name="certno" type="text" maxlength="100" class="form-control" value="{{ isset($sling)?$sling->certno:'' }}" >
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="testdate" class="col-md-2">Test Date</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-calendar"></i></small>
                    </div>
                    <input type="text" maxlength="100" name="testdate" id="datepicker" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($sling->testdate)?date('d-m-Y', strtotime($sling->testdate)):'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-form-submit">Submit</button>
            </div>
        </div>
    </div>
</div>