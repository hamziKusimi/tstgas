@extends('master')
@section('content')

<div class="alert red-alert text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist"></p>
</div>

<div class="alert red-alert2 text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist2"></p>
</div>

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
            <form action ="{{ route('manyfor.update', $manyfor->id )}}" method="POST">
        <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('cylindermaster/manyfor/form')

        </form>
    </div>
</div>


@endsection
@push('scripts')
    <script type="text/javascript">
        $(".myselect").select2();
    </script>
    <script>
        $(function () {
            $('#datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
            $('#datepicker1').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>
    <script type="text/javascript">
       @include('js.barcode-validation-js');
       @include('js.serialno-validation-js');
    </script>
@endpush
