
<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <label for="barcode" class="col-md-2">Barcode</label>
            <div class="col-md-10">
                <input name="barcode" type="text" maxlength="12" class="form-control barcode" value="">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="serial" class="col-md-2"> Serial No <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input name="serial" type="text" maxlength="12" class="form-control serial" value="">
            </div>
        </div>
    
        <br>
        <!-- Table for Cylinder Many For -->
        @include('cylindermaster.manyfor.table.table')

        <div class="input-group row narrow-padding-global">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-form-submit">Submit</button>
            </div>
        </div>
    </div>
</div>