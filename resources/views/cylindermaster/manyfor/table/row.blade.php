@if (isset($grcontents))
    @foreach ($grcontents as $i => $grcontent)
        <tr class="narrow-padding sortable-row existing-data2" data-grcontent-id="{{ $grcontent->id }}">
            <td class="details-control toggle-inner-table-button-exist" data-second-row-id2="row-id-{{ $i+1 }}">
            </td>
            <td>
                {!! Form::hidden('content_id[]', $grcontent->id, ['class' => 'grcontent-id']) !!}
                {!! Form::text('grsequence_nodt[]', $grcontent->sequence_no, ['class' => 'form-control grsequence_nodt', 'disabled']) !!}
                {!! Form::hidden('grsequence_no[]', $grcontent->sequence_no, ['class' => 'form-control grsequence_no']) !!}
            </td>
            <td> 
                {!! Form::text('typedt[]', $grcontent->type, [ 'class' => 'form-control typedt', 'disabled' ]) !!}
                {!! Form::hidden('type[]', $grcontent->type, [ 'class' => 'form-control type']) !!}
            </td>
            <td>
                {!! Form::number('quantitydt[]', $grcontent->qty, ['class' => 'form-control quantitydt', 'min' => '1', 'step' => '0.01', 'disabled']) !!}
                {!! Form::hidden('quantity[]', $grcontent->qty, ['class' => 'form-control quantity', 'min' => '1', 'step' => '0.01']) !!}
            </td>
            <td>
                {!! Form::text('pricedt[]', $grcontent->price, ['class' => 'form-control pricedt text-right', 'disabled']) !!}
                {!! Form::hidden('price[]', $grcontent->price, ['class' => 'form-control price text-right']) !!}
            </td>
            <td>
                <button type="button" class="btn btn-danger remove-row" data-delete-route="{{  route('deliverynotes.data.destroy', $grcontent->id)  }}">
                    <i class="fa fa-times"></i>
                </button>
            </td>
        </tr>
        <tr class="inner-table d-none row-id-{{ $i+1 }}" data-row-id2="0">
            <td colspan="9">
                <table class="table table-sm narrow-padding gasrack-table">
                    <thead>
                        <tr class="text-sm">
                            <th class="text-center">Barcode</th>
                            <th class="text-center">Serial</th>
                            <th class="text-center">Driver</th>
                            <th class="text-center">Loading</th>
                            <th class="text-center">Unloading</th>
                            <th></th>
                        </tr>
                    </thead>
                    @if (isset($grcontent->gasracks))
                        <tbody>
                        @foreach ($grcontent->gasracks as $gasrack)
                            <tr class="new-row">
                                <td class="text-center">{{ $gasrack->barcode }}</td>
                                <td class="text-center">{{ $gasrack->serial }}</td>
                                <td class="text-center">{{ $gasrack->driver }}</td>
                                <td class="text-center">{{ date('d/m/Y', strtotime($gasrack->loading_date)) }}</td>
                                <td class="text-center">{{ date('d/m/Y', strtotime($gasrack->unloading_date)) }}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-danger remove-row-child" 
                                    data-dnno="{{ $gasrack->dn_no }}"
                                    data-sqndt="{{ $gasrack->dt_sqn }}"
                                    data-sqnlist="{{ $gasrack->list_sqn }}"
                                    data-delete-routelist="{{ route('deliverynotes.data.destroylist') }}">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                </table>
            </td>
        </tr>
    @endforeach
@endif