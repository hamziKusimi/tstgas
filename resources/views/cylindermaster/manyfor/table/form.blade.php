
<tr class="narrow-padding toggle-details main-table-row-template2 d-none" data-row-id2="0">
    <td class="details-control toggle-inner-table-button">
    </td>
    <td>
        {!! Form::hidden('gritem_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('grsequence_no[]', null, ['class' => 'form-control grsequence_no exist-val-gr']) !!}
    </td>
    <td>
        {!! Form::select('grtype[]', [], null, [
            'class' => 'form-control grtype exist-val-gr',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            // 'data-gstype' => $gasracktype,
        ]) !!}
      
    </td>
    <td>
        {!! Form::number('grquantity[]', null, ['class' => 'form-control validate-barcode grquantity exist-val-gr', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('grprice[]', null, ['class' => 'form-control grprice exist-val-gr']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id2="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="inner-table second-row-template2 d-none" data-row-id2="0">
    <td colspan="9">
        <table class="table table-sm narrow-padding gasrack-table">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="gasrack"></tbody>
        </table>
    </td>
</tr>
