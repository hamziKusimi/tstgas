<div class="tax-container"></div>
<div class="table-responsive">
    <table class="table table-sm" id="manyfordt">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="4.5%"></th>
                <th width="6%" class="text-center">No</th>
                <th width="14%" class="text-center">Barcode</th>
                <th width="14%" class="text-center">Serial</th>
                <th width="14%" class="text-center">Category</th>
                <th width="14%" class="text-center">Product</th>
                <th width="14%" class="text-center">Capacity</th>
                <th width="14%" class="text-center">Pressure</th>
                <th width="14%" class="text-center">Qty</th>
                <th width="1%"></th>
            </tr>
        </thead>
        <tbody id="table-form-tbody-manyfor" class="manyfor">
            @include('cylindermaster.manyfor.table.form')
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1">    
                    <button type="button" class="form-control btn bg-dark-green text-white" style="width:70px" id="add-new2">
                        Add 
                    </button>
                </td>
                <td colspan="8"></td>
            </tr> 
        </tfoot>
    </table>
</div>

