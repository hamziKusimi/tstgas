@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('cylinderhandwheeltypes.store') }}" method="POST">
            @csrf
            @include('cylindermaster/cylinderhandwheeltypes/form')

        </form>
    </div>
</div>


@endsection
