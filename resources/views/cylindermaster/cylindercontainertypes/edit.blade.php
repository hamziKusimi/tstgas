@extends('master')
@section('content')



<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('cylindercontainertypes.update', $cylindercontainertype->id )}}" method="POST">
        <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('cylindermaster/cylindercontainertypes/form')

        </form>
    </div>
</div>


@endsection
