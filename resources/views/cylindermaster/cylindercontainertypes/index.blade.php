@extends('master')

@section('content')

    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('cylindercontainertypes.create') }}"> Add Container Type</a>
        </div>
    </div>

    
    @if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>

    @endif
    
@php ($i = 0)
    
<div class="card">
    <div class="card-body">
        <div class="bg-white">        
            <div class="table-responsive">
                <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Description</th>
                            <th width="280px">More</th>
                        </tr>
                    </thead>
                @if(sizeof($cylindercontainertypes) > 0)   
                    <tbody>
                        @foreach ($cylindercontainertypes as $cylindercontainertype)
                        <tr>
                        
                            <td>{{ ++$i }}</td>
                            <td><a href="{{ route('cylindercontainertypes.edit',$cylindercontainertype->id) }}">{{ $cylindercontainertype->code }}</a></td>
                            {{-- <td>{{ $cylindercontainertype->code}}</td> --}}
                            <td>{{ $cylindercontainertype->descr }}</td>
                            <td>
                                <a href="{{ route('cylindercontainertypes.destroy', $cylindercontainertype->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                <i class="fa fa-trash"></i></a>  
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                @endif
                </table>
            </div>
        </div>
    </div>
</div>
    {{-- {!! $cylindercontainertypes->links() !!} --}}

@endsection
{{-- @push('script') --}}
@section('js')

    <script>
    // $(document).ready(function() {
    //     $('#example').DataTable();
    // } );
    </script>
    <script>        
        $(document).ready(function() {
            $('#example').DataTable({
            });
        } );
    </script>
{{-- @endpush --}}
@endsection

