@extends('master')
@section('content')



<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('ownerships.update', $ownership->id )}}" method="POST">
        <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('cylindermaster/ownerships/form')

        </form>
    </div>
</div>


@endsection
