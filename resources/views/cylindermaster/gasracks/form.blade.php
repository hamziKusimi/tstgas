<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <label for="barcode" class="col-md-2">Barcode</label>
            <div class="col-md-6">
                <input name="barcode" type="text" maxlength="12" class="form-control barcode"
                    value="{{ isset($gasrack)?$gasrack->barcode:'' }}" data-values="{{ $gasracks }}">
            </div>
        </div>

        <div class="input-group row narrow-padding-global">
            <label for="serial" class="col-md-2"> Serial No <span class="text-danger">*</span></label>
            <div class="col-md-6">
                <input name="serial" id="serial" type="text" maxlength="12" class="form-control serial"
                    value="{{ isset($gasrack)?$gasrack->serial:'' }}" data-items="{{ $gasracks }}" required>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="description" class="col-md-2">Description</label>
            <div class="col-md-6">
                <input name="description" type="text" maxlength="100" class="form-control"
                    value="{{ isset($gasrack)?$gasrack->description:'' }}">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="mfr" class="col-md-2">Name of Manufacturer</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <select class="myselect form-control mfr" style="width:100%" name="mfr" id="mfr"
                        data-values="{{ $manufacturers }}">
                        <option value=""></option>
                        @foreach($manufacturers as $manufacturer)
                        <option value="{{ $manufacturer->id }}"
                            {{ isset($gasrack)?$gasrack->mfr == $manufacturer->id  ? 'selected' : '' : ''}}>
                            {{ $manufacturer->code }}, {{ $manufacturer->descr }}</option>
                        @endforeach
                    </select>
                    {{-- <input name="mfr" type="hidden" maxlength="100" class="form-control mfr" value="{{ isset($gasrack)?$gasrack->mfr:'' }}"
                    > --}}
                </div>
            </div>
            {{-- <div class="col-md-6">
                <input name="mfr" type="text" maxlength="100" class="form-control" value="{{ isset($gasrack)?$gasrack->mfr:'' }}"
            >
        </div> --}}
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="mfgterm" class="col-md-2">Month/Year Manufacture</label>
        <div class="col-md-6">
            <input name="mfgterm" type="text" maxlength="100" class="form-control"
                value="{{ isset($gasrack)?$gasrack->mfgterm:'' }}">
        </div>
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="owner" class="col-md-2"> Name of Owner </label>
        <div class="col-md-6">
            <input name="owner" type="text" maxlength="100" class="form-control"
                value="{{ isset($gasrack)?$gasrack->owner:'' }}">
        </div>
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="type" class="col-md-2">Type of Gas Rack (xxxxx)</label>
        <div class="col-md-6">
            <select class="myselect form-control" name="type" id="type">
                <option value=""></option>
                @foreach($gasracktypes as $gasracktype)
                <option value="{{$gasracktype->id}}"
                    {{ isset($gasrack)?$gasrack->type == $gasracktype->id  ? 'selected' : '' : ''}}>
                    {{$gasracktype->code}}, {{ $gasracktype->descr }}</option>
                @endforeach
            </select>
            {{-- <input name="type" type="text" maxlength="100" class="form-control" value="{{ isset($gasrack)?$gasrack->type:'' }}"
            > --}}
        </div>
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="maxgmass" class="col-md-2"> Maximum Gross Mass </label>
        <div class="col-md-6">
            <input name="maxgmass" type="text" maxlength="100" class="form-control"
                value="{{ isset($gasrack)?$gasrack->maxgmass:'' }}">
        </div>
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="taremass" class="col-md-2">Tare Mass</label>
        <div class="col-md-6">
            <input name="taremass" type="text" maxlength="100" class="form-control"
                value="{{ isset($gasrack)?$gasrack->taremass:'' }}">
        </div>
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="payload" class="col-md-2">Payload</label>
        <div class="col-md-6">
            <input name="payload" type="text" maxlength="100" class="form-control"
                value="{{ isset($gasrack)?$gasrack->payload:'' }}">
        </div>
    </div>

    <div class="input-group row narrow-padding-global">
        <label for="certno" class="col-md-2"> Certificate No </label>
        <div class="col-md-6">
            <input name="certno" type="text" maxlength="100" class="form-control"
                value="{{ isset($gasrack)?$gasrack->certno:'' }}">
        </div>
    </div>
    <div class="input-group row narrow-padding-global">
        <label for="testdate" class="col-md-2">Test Date</label>
        <div class="col-md-6">
            <div class="input-group-prepend">
                <div class="input-group-text bg-dark-blue text-white">
                    <small><i class="fa fa-calendar"></i></small>
                </div>
                <input type="text" maxlength="100" name="testdate" id="datepicker" autocomplete="off"
                    class="form-control" placeholder="dd-mm-yyyy"
                    value="{{ isset($gasrack->testdate)?date('d-m-Y', strtotime($gasrack->testdate)):'' }}">
            </div>
        </div>
    </div>
    <div class="input-group row narrow-padding-global">
        <label for="" class="col-md-2">Status</label>
        @php
        $isChecked = isset($gasrack)?$gasrack->status:'active'
        @endphp
        <div class="col-md-5">
            <div class="form-check form-check-inline">
                {!! Form::radio('Status', 'active', ($isChecked == 'active'), ['class' => 'my-class check-status'])!!}
                {!! Form::label('Active', '&nbsp;Active', ['class' => 'col-form-label']) !!}
            </div>
            <div class="form-check form-check-inline">
                {!! Form::radio('Status', 'inactive', ($isChecked == 'inactive'), ['class' => 'my-class check-status'])!!}
                {!! Form::label('Inactive', '&nbsp;Inactive', ['class' => 'col-form-label']) !!}
            </div>
            <div class="form-check form-check-inline">
                {!! Form::radio('Status', 'sold', ($isChecked == 'sold'), ['class' => 'my-class check-status'])!!}
                {!! Form::label('Sold', '&nbsp;Sold', ['class' => 'col-form-label']) !!}
            </div>
            <div class="form-check form-check-inline">
                {!! Form::radio('Status', 'disposed', ($isChecked == 'disposed'), ['class' => 'my-class check-status'])!!}
                {!! Form::label('Disposed', '&nbsp;Disposed', ['class' => 'col-form-label']) !!}
            </div>
        </div>
    </div>
    <div class="input-group row narrow-padding-global cylindergasrack-status">
        @if(isset($gasrack) && isset($gasrack->reasons))
        <label for="" class="col-md-2">Reasons</label>
        <div class="col-md-5">
            <input name="reasons" type="text" maxlength="100" class="form-control reasons" disabled
                value="{{ $gasrack->reasons }}">
        </div> 
        @endif
    </div> 
    @if(($systemsetup->updoc_gasrack == "1"))
        <div class="input-group row narrow-padding-global">
            <label for="certno" class="col-md-2"> Attachment File </label>
            <div class="col-md-6">
                <input name="attachment" type="file">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="certno" class="col-md-2"> </label>
            <div class="col-md-6">
                <label>Uploaded File :</label>

                <div class="remove-attachment">
                    @if(isset($gasrack))
                    <a href='{{URL::asset("public/attachment/$gasrack->attachment")}}'>
                        {{ $gasrack->attachment }}
                    </a>
                    @if(isset($gasrack->attachment))
                    <button type="button" class="btn btn-sm btn-danger" id="remove" data-id="{{$gasrack->id}}"
                        data-file="{{$gasrack->attachment}}">
                        remove
                    </button>
                    @endif
                    @endif
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin" style="font-size: large;
                        top: 90%;
                        left: 40%;"></i>
                    </div>
                </div>

            </div>
        </div>
    @endif
    @php ($i = 0)
    <div class="input-group row narrow-padding-global">
        <label for="cylinder" class="col-md-2">Add Cylinders</label>
        <div class="col-md-6">
            <div class="tax-container2"></div>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr class="bg-dark-blue text-white text-sm">
                            <th class="text-center">No</th>
                            <th class="text-center">Barcode</th>
                            <th class="text-center">Serial</th>
                            <th class="text-center">Category</th>
                            <th class="text-center">Product</th>
                            <th class="text-center">Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="narrow-padding">
                            <td></td>
                            <td> {!! Form::text('cybarcode', null, ['class' => 'form-control cybarcode']) !!}</td>
                            <td> {!! Form::text('cyserial', null, ['class' => 'form-control cyserial']) !!}</td>
                            <td> {!! Form::text('cycat', null, ['class' => 'form-control cycat']) !!}</td>
                            <td> {!! Form::text('cyprod', null, ['class' => 'form-control cyprod']) !!}</td>
                            <td> {!! Form::text('cydescr', null, ['class' => 'form-control cydescr']) !!}
                                {!! Form::hidden('cypressure', null, ['class' => 'form-control cypressure']) !!}
                                {!! Form::hidden('cycapacity', null, ['class' => 'form-control cycapacity']) !!}</td>
                            <td>
                                <div class="input-group row narrow-padding-global">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-secondary btn-form-submit add">Add</button>
                                        <button type="submit" class="btn btn-secondary btn-form-submit d-none save">Add</button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @if(isset($gasrackcylinders))
                        @foreach($gasrackcylinders as $grcy)
                            <tr class="narrow-padding">
                                <td>{{ ++$i }}</td>
                                <td> {{ $grcy->cy_barcode }} </td>
                                <td> {{ $grcy->cy_serial }} </td>
                                <td> {{ $grcy->cy_category }} </td>
                                <td> {{ $grcy->cy_product }} </td>
                                <td> {{ $grcy->cy_descr }} </td>
                                <td>
                                    <a href="{{ route('gasracks.destroycylinder', $grcy->id) }}" data-method="delete" data-confirm="Confirm delete this cylinder?">
                                    <i class="fa fa-trash"></i></a>  
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @php ($y = 0)
    <div class="input-group row narrow-padding-global">
        <label for="cylinder" class="col-md-2">Add Sling</label>
        <div class="col-md-6">
            <div class="tax-container2"></div>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr class="bg-dark-blue text-white text-sm">
                            <th class="text-center">No</th>
                            <th class="text-center">Barcode</th>
                            <th class="text-center">Serial</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!isset($gasrack->sg_serial))
                        <tr class="narrow-padding">
                            <td></td>
                            <td> {!! Form::text('sgbarcode', null, ['class' => 'form-control sgbarcode']) !!}</td>
                            <td> {!! Form::text('sgserial', null, ['class' => 'form-control sgserial']) !!}</td>
                            <td>
                                <div class="input-group row narrow-padding-global">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-secondary btn-form-submit add2">Add</button>
                                        <button type="submit" class="btn btn-secondary btn-form-submit d-none save2">Add</button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @else
                        <tr class="narrow-padding">
                            <td>{{ ++$y }}</td>
                            <td> {{ $gasrack->sg_barcode }} 
                                {!! Form::hidden('sgbarcode', $gasrack->sg_barcode, ['class' => 'form-control sgbarcode']) !!}</td>
                            <td> {{ $gasrack->sg_serial }} 
                                {!! Form::hidden('sgserial', $gasrack->sg_serial, ['class' => 'form-control sgserial']) !!}</td>
                            <td>
                                <a href="{{ route('gasracks.destroysling', $gasrack->sg_serial) }}" data-method="delete" data-confirm="Confirm delete this cylinder?">
                                <i class="fa fa-trash"></i></a>  
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="input-group row narrow-padding-global">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary btn-form-submit">Submit</button>
        </div>
    </div>
</div>
</div>