@extends('master')

@section('content')

    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('gasracks.create') }}"> Add Gas Rack</a>
        </div>
    </div>

    @if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>

    @endif

@php ($i = 0)
    
<div class="card">
    <div class="card-body">
        <div class="bg-white">        
            <div class="table-responsive">
                <table id="gasTab" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Serial</th>
                            <th>Barcode</th>
                            <th>Description</th>
                            <th>Gas Rack</th>
                            <th width="280px">More</th>
                        </tr>
                    </thead>
                @if(sizeof($gasracks) > 0)    
                    <tbody>
                        @foreach ($gasracks as $gasrack)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td><a href="{{ route('gasracks.edit',$gasrack->id) }}">{{ $gasrack->serial }}</a></td>
                            <td>{{ $gasrack->barcode }}</td>
                            <td>{{ $gasrack->description }}</td>
                            <td>{{ $gasrack->gstype['code'] }}</td>
                            <td>
                                <a href="{{ route('gasracks.destroy', $gasrack->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                <i class="fa fa-trash"></i></a>  
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                @endif   
                </table>
            </div>
        </div>
    </div>
</div>

    {{-- {!! $gasracks->links() !!} --}}

@endsection

@push('scripts')
<script>
$(document).ready(function() {

            $('#gasTab').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        } );
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
@endpush

