@extends('master')
@section('content')

<div class="row">
    
    <div class="col-md-2">
        <a href="{{ route('gasracks.index') }}" style="width: 100%;" class="btn btn-danger"><i class="fa"></i>Back</a>
    </div>
</div>
<br>
<div class="alert red-alert text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist"></p>
</div>

<div class="alert red-alert2 text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist2"></p>
</div>
    
<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('gasracks.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @include('cylindermaster/gasracks/form')

        </form>
    </div>
</div>

@endsection
@push('scripts')
    <script type="text/javascript">
        $(".myselect").select2();
    </script>
    <script>
        $(function () {
            $('#datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
            $('#datepicker1').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
    </script>
    
    <script type="text/javascript">
        @include('js.barcode-validation-js');
        @include('js.serialno-validation-js');
        @include('js.cylinder-validation-js');
        @include('js.sling-validation-js');
        @include('js.check-reasons-status-js');
        $(function () {
            $('.overlay').hide();
        })
     </script>
@endpush
