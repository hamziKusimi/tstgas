@extends('master')

@section('content')

<div classs="row">
    <div class="col-md-2">
        <a href="{{ route('cylinders.cylinderactivity') }}" style="width: 100%;" class="btn btn-danger"><i class="fa"></i>Back</a>
    </div>
</div>
<br>
@php ($i = 0)
<div class="card">
    <div class="card-body">
        <div class="bg-white">  
            <br>
            <br>
            
            <div class="col-md-12">   
                <div class="row">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Doc. Type</th>
                                    <th>Barcode</th>
                                    <th>Serial</th>
                                    <th>Action</th>
                                    <th>Asset Type</th>
                                    <th>Driver</th>
                                    <th>Date Time</th>
                                    <th>Doc No.</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                </tr>
                            </thead>
                        {{-- @if(sizeof($cylinderactivities) > 0)    
                            <tbody>
                                @foreach ($cylinderactivities as $cylinder)
                                <tr>
                                
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $cylinder->model }}</td>
                                    <td>{{ $cylinder->barcode }}</td>
                                    <td>{{ $cylinder->serial }}</td>
                                    <td>{{ $cylinder->action }}</td>
                                    <td>{{ $cylinder->description }}</td>
                                    <td>{{ $cylinder->datetime }}</td>
                                    <td>{{ $cylinder->dn_no }}</td>
                                    <td>{{ $cylinder->TYPE }}</td>
                                    <td>{{ $cylinder->created_at }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        @endif     --}}
                        </table>
                    </div>
                </div>
            </div>
            {{-- {{ $cylinderactivities->links() }} --}}

        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script>        
        // $(document).ready(function() {
        //     $('#example').DataTable({
        //     });
        // } );
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#LPO_Chkbx_1").prop("checked") == true){
        $("#LPO_frm_1").prop('disabled', false);
        $("#LPO_to_1").prop('disabled', false);
    }else{
        $("#LPO_frm_1").prop('disabled', true);
        $("#LPO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#LPO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#LPO_frm_1").prop('disabled', false);
            $("#LPO_to_1").prop('disabled', false);
        }else{
            $("#LPO_frm_1").prop('disabled', true);
            $("#LPO_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });
    
    let docno_frm = $('#docno_frm').val()
    let docno_to = $('#docno_to').val()
    let docno_Chkbx = $('#docno_Chkbx').val()

    let data = {
            'docno_frm': docno_frm,
            'docno_to': docno_to,
            'docno_Chkbx': docno_Chkbx
        }
    

    $.ajax({
            url: '{!! route('cylindertracking.datatable.post') !!}',
            method: 'POST',
            data: data
        }).done(function (response) {
            console.log("test")
            console.log(response)
        })

    $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:'{!! route('cylindertracking.datatable.post') !!}',
            type:'POST',
            data: data
        },
        columns: [
            { data: 'model', name: 'model' },
            { data: 'barcode', name: 'barcode' },
            { data: 'serial', name: 'serial' },
            { data: 'action', name: 'action' },
            { data: 'TYPE', name: 'TYPE'},
            { data: 'description', name: 'description' },
            { data: 'datetime', name: 'datetime'},
            { data: 'dn_no', name: 'dn_no'},
            { data: 'created_at', name: 'created_at'},
            { data: 'updated_at', name: 'updated_at'},
        ],
    });
</script>
@endpush

