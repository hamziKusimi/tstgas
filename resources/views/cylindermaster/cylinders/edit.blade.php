@extends('master')
@section('content')
<div class="row">
    {{-- <div class="col-md-2"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4"></div> --}}
    <div class="col-md-2">
        <a href="{{ route('cylinders.index') }}" style="width: 100%;" class="btn btn-danger"><i class="fa"></i>Back</a>
    </div>
</div>
<br>
@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif
<div class="alert red-alert text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist"></p>
</div>

<div class="alert red-alert2 text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist2"></p>
</div>

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action="{{ route('cylinders.update', $cylinder->id )}}" method="POST">
            <input type="hidden" name="_method" value="PUT" />
            @csrf
            @include('cylindermaster/cylinders/form')

        </form>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    $(".myselect").select2();
</script>
<script>
    $(function () {
            $('#datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
            $('#datepicker1').datepicker({
                format: 'dd-mm-yyyy'
            });
        });
</script>
<script type="text/javascript">
    @include('common.billing.js.cylinder-js');
       @include('js.barcode-validation-js');
       @include('js.serialno-validation-js');
        @include('js.check-reasons-status-js');

       $(function() {
            $('#mfr').change(function() {
                let contents = $(this).data('values')
                console.log(contents);
                let manufacturer = $(this).val()  
        
                let selectedManufacturer = contents.filter(content => {
                    return content.id == manufacturer
                }) [0]
        
                // console.log(selectedManufacturer)
                $('.valvemfr').val(selectedManufacturer.val)
                $('.valvemfr2').val(selectedManufacturer.val)
            })  
        });
</script>
<script type='text/javascript'>
    $('.editBtn').on('click', function () {
            if ($('.barcode').val() == '') {
                console.log('test')
                //Check to see if there is any text entered
                // If there is no text within the input ten disable the button
                $('.barcode').prop('disabled', true);
            } else {
                console.log('yes')
                //If there is text in the input, then enable the button
                $('.barcode').prop('disabled', false);
            }
        });
</script>
@endpush