@extends('master')

@section('content')
@php ($i = 0)
<div class="card">
    <div class="card-body">
        <div class="bg-white">   
            <div classs="row">
                <h6><b>Filter By: </b></h6>
            </div>
            <div class="col-md-12">                
                <form action="{{ route('cylinders.cylinderactivity.listing') }}" method="GET" target="_blank">

                    <div class="row">

                        <div class="col-3">
    
                            {!! Form::checkbox('dates_Chkbx',null,null, [ 'id' =>'dates_Chkbx'])!!}
    
                            {!! Form::label('Date', 'Date', ['class' => 'col-form-label']) !!}
    
                        </div>
    
    
    
                        <label class="col-form-label">:</label>
    
                        <div class="col-4">
    
                            <div class="form-group">
    
                                <div class="input-group date">
    
                                    {!! Form::text('dates', null, ['class' => 'form-control form-data',
    
                                    'autocomplete'=>'off', 'id' =>'dates'
    
                                    ])!!}
    
                                </div>
    
                            </div>
    
                        </div>
    
                    </div>
    
                    <div class="row">
    
                        <div class="col-3">
    
                            {!! Form::checkbox('docno_Chkbx',null,null, [ 'id' =>'docno_Chkbx'])!!}
    
                            {!! Form::label('docno', 'Document No.', ['class' => 'col-form-label']) !!}
    
                        </div>
    
                        <label class="col-form-label">:</label>
    
                        <div class="col-3">
    
                            <div class="form-group">
    
                                <div class="input-group-append">
    
                                    {!! Form::text('docno_frm', null, ['class' => 'form-control docno_frm', 'id'
    
                                    =>'docno_frm'
    
                                    ,'disabled' =>'disabled'])!!}
    
                                    {{-- {!! Form::select('docno_frm',  $docno_select, null, ['class' => 'form-control select',
    
                                    'id'
    
                                    =>'docno_frm'
    
                                    ,'disabled' =>'disabled'])!!} --}}
    
                                    <div class="input-group-text bg-dark-blue text-white pointer" data-toggle="modal" data-target=".reportdocno-modal" style="text-decoration: underline;"">
    
                                        <small>
    
                                                <i class="fa fa-search"></i>
    
                                        </small>
    
                                    </div>
    
                                </div>
    
                            </div>
    
                        </div>
                        <div class="col-3">
    
                            <div class="form-group">
    
                                <div class="input-group-append">
    
                                    {!! Form::text('docno_to', null, ['class' => 'form-control docno_to', 'id'
    
                                    =>'docno_to'
    
                                    ,'disabled' =>'disabled'])!!}
    
                                    {{-- {!! Form::select('docno_to',  $docno_select, null, ['class' => 'form-control select',
    
                                    'id'
    
                                    =>'docno_to'
    
                                    ,'disabled' =>'disabled'])!!} --}}
    
                                    <div class="input-group-text bg-dark-blue text-white pointer docnoto" data-toggle="modal" data-target=".reportdocnoto-modal" style="text-decoration: underline;"">
    
                                        <small>
    
                                                <i class="fa fa-search"></i>
    
                                        </small>
    
                                    </div>
    
                                </div>
    
                            </div>
    
                        </div>
    
                    </div>
                    
                <div class="row">

                    <div class="col-3">

                        {!! Form::checkbox('debCode_Chkbx',null,null, [ 'id' =>'debCode_Chkbx'])!!}

                        {!! Form::label('debCode', 'Debtor Code', ['class' => 'col-form-label']) !!}

                    </div>

                    <label class="col-form-label">:</label>

                    <div class="col-3">

                        <div class="form-group">

                            {!! Form::select('debCode_frm', [], null, ['class' => 'form-control

                            select', 'id'

                            =>'debCode_frm'

                            ,'disabled' =>'disabled'])!!}

                        </div>

                    </div>

                    <div class="col-3">

                        <div class="form-group">

                            {!! Form::select('debCode_to', [], null, ['class' => 'form-control

                            select', 'id'

                            =>'debCode_to'

                            ,'disabled' =>'disabled'])!!}

                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary form-control submit">Submit</button>
                    </div>
                </div>
                </form>
            </div>
            <br>
            <br>
            
            {{-- <div class="col-md-12">   
                <div class="row">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Doc. Type</th>
                                    <th>Barcode</th>
                                    <th>Serial</th>
                                    <th>Action</th>
                                    <th>Driver</th>
                                    <th>Date Time</th>
                                    <th>Doc No.</th>
                                    <th>Asset Type</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                </tr>
                            </thead> --}}
                        {{-- @if(sizeof($cylinderactivities) > 0)    
                            <tbody>
                                @foreach ($cylinderactivities as $cylinder)
                                <tr>
                                
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $cylinder->model }}</td>
                                    <td>{{ $cylinder->barcode }}</td>
                                    <td>{{ $cylinder->serial }}</td>
                                    <td>{{ $cylinder->action }}</td>
                                    <td>{{ $cylinder->description }}</td>
                                    <td>{{ $cylinder->datetime }}</td>
                                    <td>{{ $cylinder->dn_no }}</td>
                                    <td>{{ $cylinder->TYPE }}</td>
                                    <td>{{ $cylinder->created_at }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        @endif     --}}
                        {{-- </table>
                    </div>
                </div>
            </div> --}}
            {{-- {{ $cylinderactivities->links() }} --}}

        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script>        
        // $(document).ready(function() {
        //     $('#example').DataTable({
        //     });
        // } );
        $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            console.log('sn')
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
        console.log('here')
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });

    let docno_frm = $('#docno_frm').val()
    let docno_to = $('#docno_to').val()
    let docno_Chkbx = $('#docno_Chkbx').val()
    
    let data = {
            'docno_frm': docno_frm,
            'docno_to': docno_to,
            'docno_Chkbx': docno_Chkbx
        }

    $.ajax({
            url: '{!! route('cylindertracking.datatable.post') !!}',
            method: 'POST',
            data: data
            }).done(function (response) {
                console.log(response)
            })
    var dataTable = $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:'{!! route('cylindertracking.datatable.post') !!}',
            type:'POST',
            data: function(data){
                // Read values
                var docnofrm = $('#docno_frm').val()
                var docnoto = $('#docno_to').val()

                // Append to data
                data.docno_frm = docnofrm
                data.docno_to = docnoto
            }
        },
        columns: [
            { data: 'model', name: 'model' },
            { data: 'barcode', name: 'barcode' },
            { data: 'serial', name: 'serial' },
            { data: 'action', name: 'action' },
            { data: 'description', name: 'description' },
            { data: 'datetime', name: 'datetime'},
            { data: 'dn_no', name: 'dn_no'},
            { data: 'TYPE', name: 'TYPE'},
            { data: 'created_at', name: 'created_at'},
            { data: 'updated_at', name: 'updated_at'},
        ],
    });
    
    $('#docno_frm').keyup(function(){
        dataTable.draw();
    });

    $('#docno_to').change(function(){
        dataTable.draw();
    });
</script>
@endpush

