<div class="row">
        <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">{{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        <p style="text-align:center; font-size:22px; font-weight:bold;margin-top: 0px;">Cylinder Master Listing</p>
        <br>
</div>
<div class="card">
        <div class="card-body">
            <div class="bg-white">
                <div class="row">

                        <table border="1" cellspacing="0" style="width:100%">
                                <thead >
                                    <tr>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">No.</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Barcode</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Serial</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Category</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Group</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Type</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Product</th>
                                        <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Detail</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Owner</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Handwheel</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Manufacture</th>
                                        <th style="text-align:center;border-top:2px solid;border-bottom:2px solid;">Material</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @php
                                        $no=1;
                                    @endphp
                                    @foreach ($CylinderJSON as $cylinder)
                                    <tr>
                                        @php
                                        $detail = $cylinder->descr . "<br> <b>Guard: </b>" . ($cylinder->valveguard ? "Yes" : "No");
                                        if ($request->OriOwn_Checkbox == "on") {
                                            $detail = $detail . "<br><b>Ori. Owner:</b><br>" . $cylinder->oriowner;
                                        }
                                        if ($request->OriWeight_Checkbox == "on") {
                                            $detail = $detail . "<br><b>Ori. TareWeight:</b><br>" . $cylinder->oritareweight;
                                        }
                                        if ($request->VOut_checkBox == "on") {
                                            $detail = $detail . "<br><b>Valve Outlet:</b><br>" . $cylinder->valveoutlet;
                                        }
                                        if ($request->TestPress_Checkbox == "on") {
                                            $detail = $detail . "<br><b>Test Pressure:</b><br>" . $cylinder->testpressure;
                                        }
                                        if ($request->VType_Checkbox == "on") {
                                            $detail = $detail . "<br><b>Valve Type:</b><br>" . $cylinder->vtype;
                                        }
                                        if ($request->ValManu_Checkbox == "on") {
                                            $detail = $detail . "<br><b>Value Manufact:</b><br>" . $cylinder->valvemfr;
                                        }
                                        if ($request->TestDate_Checkbox == "on") {
                                            $detail = $detail . "<br><b>Test Date:</b>" . ($cylinder->testdate ==""?"": date("d/m/Y", strtotime($cylinder->testdate)));
                                        }
                                        if ($request->ManuDate_Checkbox == "on") {
                                            $detail = $detail . "<br><b>Manufac. Date:</b>" . ($cylinder->mfgdate ==""?"":date("d/m/Y", strtotime($cylinder->mfgdate)));
                                        }
                                        @endphp
                                        <td>{{ $no }}</td>
                                        <td>{!! $cylinder->barcode !!} </td>
                                        <td>{{ $cylinder->serial }}</td>
                                        <td>{{ $cylinder->category }}</td>
                                        <td>{{  $cylinder->cgroup }}</td>
                                        <td>{{  $cylinder->ctype }}</td>
                                        <td>{{  $cylinder->product }}</td>
                                        <td> {!! $detail!!}</td>
                                        <td>{{  $cylinder->own }}</td>
                                        <td>{{ $cylinder->htype }}</td>
                                        <td>{{  $cylinder->manu }}</td>
                                        <td>{{  $cylinder->material }}</td>
                                    </tr>      
                                    @php
                                        $no++;
                                    @endphp
                                    @endforeach
                                </tbody>


                        </table>
                </div>
            </div>
        </div>
</div>