<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Barcode</label>
            <div class="col-md-5">
                <div class="input-group-prepend">
                    <input name="barcodecoy" id="barcodecoy" type="text" maxlength="12" class="form-control barcodecoy"
                        data-values="{{ $barcodes }}" value="{{ isset($cylinder)?$cylinder->barcode:'' }}"
                        {{ isset($cylinder->barcode)?'disabled':'' }}>
                    <input name="barcode" id="barcode" type="hidden" maxlength="12" class="form-control barcode"
                        value="{{ isset($cylinder)?$cylinder->barcode:'' }}">
                    <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editBarcode"
                        value="Edit" />
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2"> Serial No <span class="text-danger">*</span></label>
            <div class="col-md-5">
                <div class="input-group-prepend">
                    <input name="serialcoy" id="serialcoy" type="text" maxlength="12" class="form-control serialcoy"
                        data-items="{{ $serials }}" value="{{ isset($cylinder)?$cylinder->serial:'' }}"
                        {{ isset($cylinder->serial)?'disabled':'' }}>
                    <input name="serial" id="serial" type="hidden" maxlength="12" class="form-control serial"
                        value="{{ isset($cylinder)?$cylinder->serial:'' }}">
                    <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editSerial"
                        value="Edit" />
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Type</label>
            <div class="col-md-5">
                <select class="myselect form-control" name="type_id" id="type_id">
                    <option value=""></option>
                    @foreach($types as $type)
                    <option value="{{ $type->id }}"
                        {{ isset($cylinder)?$cylinder->type_id == $type->id  ? 'selected' : '' : ''}}>{{ $type->code }},
                        {{ $type->descr }}</option>
                    @endforeach
                </select>
                {{-- <input name="type_id" type="text" maxlength="100" class="form-control" value="{{ isset($cylinder)?$cylinder->type_id:'' }}"
                > --}}
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2"> Category <span class="text-danger">*</span></label>
            <div class="col-md-5">
                <div class="input-group-prepend">
                    <select class="myselect form-control categorycoy" style="width:96%" name="categorycoy"
                        id="categorycoy" required {{ isset($cylinder->cat_id)?'disabled':'' }}>
                        <option value=""></option>
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}"
                            {{ isset($cylinder)?$cylinder->cat_id == $category->id  ? 'selected' : '' : ''}}>
                            {{ $category->code }}, {{ $category->descr  }}</option>
                        {{-- <option value="{{$category->id}}">{{$category->code}}</option> --}}
                        @endforeach
                    </select>
                    <input name="category" type="hidden" maxlength="100" class="form-control category"
                        value="{{ isset($cylinder)?$cylinder->cat_id:'' }}">
                    <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editCategory"
                        value="Edit" />
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2"> Group <span class="text-danger">*</span></label>
            <div class="col-md-5">
                <div class="input-group-prepend">
                    <select class="myselect form-control groupcoy" style="width:96%" name="groupcoy" id="groupcoy"
                        required {{ isset($cylinder->group_id)?'disabled':'' }}>
                        <option value=""></option>
                        {{-- @foreach($groups as $id => $group)
                        <option value="{{ $id }}"
                        {{ isset($cylinder)?$cylinder->group_id == $id  ? 'selected' : '' : '' }}>{{ $group }}</option>
                        --}}
                        {{-- <option value="{{$category->id}}">{{$category->code}}</option> --}}
                        {{-- @endforeach --}}
                        @foreach($groups as $group)
                        <option value="{{ $group->id }}"
                            {{ isset($cylinder)?$cylinder->group_id == $group->id  ? 'selected' : '' : '' }}>
                            {{ $group->code }}, {{ $group->descr }}</option>
                        {{-- <option value="{{$category->id}}">{{$category->code}}</option> --}}
                        @endforeach
                    </select>
                    <input name="group" type="hidden" maxlength="100" class="form-control group"
                        value="{{ isset($cylinder)?$cylinder->group_id:'' }}">
                    <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editGroup"
                        value="Edit" />
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2"> Product <span class="text-danger">*</span></label>
            <div class="col-md-5">
                <div class="input-group-prepend">
                    <select class="myselect form-control productcoy" style="width:96%" name="productcoy" id="productcoy"
                        required {{ isset($cylinder->prod_id)?'disabled':'' }}>
                        <option value=""></option>
                        @foreach($products as $product)
                        <option value="{{ $product->id }}"
                            {{ isset($cylinder)?$cylinder->prod_id == $product->id  ? 'selected' : '' : '' }}>
                            {{ $product->code }}, {{ $product->descr }}</option>
                        {{-- <option value="{{$category->id}}">{{$category->code}}</option> --}}
                        @endforeach
                    </select>
                    <input name="product" type="hidden" maxlength="100" class="form-control product"
                        value="{{ isset($cylinder)?$cylinder->prod_id:'' }}">
                    <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editProduct"
                        value="Edit" />
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2"> Description <span class="text-danger">*</span></label>
            <div class="col-md-5">
                <input name="descr" type="text" maxlength="100" class="form-control"
                    value="{{ isset($cylinder)?$cylinder->descr:'' }}">
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2"> Ownership </label>
            <div class="col-md-5">
                <div class="input-group-prepend">
                    <select class="myselect form-control ownercoy" style="width:96%" name="ownercoy" id="ownercoy"
                        {{ isset($cylinder->owner)?'disabled':'' }}>
                        <option value=""></option>
                        @foreach($owners as $owner)
                        <option value="{{ $owner->id }}"
                            {{ isset($cylinder)?$cylinder->owner == $owner->id  ? 'selected' : '' : ''}}>
                            {{ $owner->code }}, {{ $owner->descr }}</option>
                        @endforeach
                    </select>
                    <input name="owner" type="hidden" maxlength="100" class="form-control owner"
                        value="{{ isset($cylinder)?$cylinder->owner:'' }}">
                    <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editOwnership"
                        value="Edit" />
                </div>
            </div>
        </div>
        {{-- <div class="input-group row narrow-padding-global">
            <label for="mass" class="col-md-2"> Acetylene Mass </label>
            <div class="col-md-5">
                <input name="mass" type="text" maxlength="100" class="form-control" value="{{ isset($cylinder)?$cylinder->mass:'' }}"
        >
    </div>
</div> --}}
{{-- <div class="input-group row narrow-padding-global">
            <label for="approval" class="col-md-2"> Approval Code </label>
            <div class="col-md-5">
                <input name="approval" type="text" maxlength="100" class="form-control" value="{{ isset($cylinder)?$cylinder->approval:'' }}"
>
</div>
</div> --}}
{{-- <div class="input-group row narrow-padding-global">		
            <label for="approved" class="col-md-2"></label>
            <div class="col-md-5">
                <div class="radio-list">
                    <label class="radio-inline">
                    <input type="radio" name="approved" value="1" {{ isset($cylinder)?($cylinder->approved=='1')? 'checked' : '': 'checked' }}/>Yes
</font></label>
<label class="radio-inline">
    <input type="radio" name="approved" value="0"
        {{ isset($cylinder)?($cylinder->approved=='0')? 'checked' : '': '' }} />No</font></label>
</div>
</div>
</div> --}}
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2"> Capacity </label>
    <div class="col-md-5">
        <input name="capacity" type="text" maxlength="100" class="form-control"
            value="{{ isset($cylinder)?$cylinder->capacity:'' }}">
    </div>
</div>
{{-- <div class="input-group row narrow-padding-global">
            <label for="conttype" class="col-md-2"> Container Type </label>
            <div class="col-md-5">
                <select class="myselect form-control" name="conttype" id="conttype"> 
                    <option value=""></option>
                    @foreach($containertypes as $containertype)
                    <option value="{{$containertype->id}}"
{{ isset($cylinder)?$cylinder->conttype == $containertype->id  ? 'selected' : '' : ''}}>{{$containertype->code}}
</option>
@endforeach
</select>
</div>
</div> --}}
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2"> Handwheel Type </label>
    <div class="col-md-5">
        <div class="input-group-prepend">
            <select class="myselect form-control hwtypecoy" style="width:96%" name="hwtypecoy" id="hwtypecoy"
                {{ isset($cylinder->hwtype)?'disabled':'' }}>
                <option value=""></option>
                @foreach($handwheeltypes as $handwheeltype)
                <option value="{{ $handwheeltype->id }}"
                    {{ isset($cylinder)?$cylinder->hwtype == $handwheeltype->id  ? 'selected' : '' : ''}}>
                    {{ $handwheeltype->code }}, {{ $handwheeltype->descr }}</option>
                @endforeach
            </select>
            <input name="hwtype" type="hidden" maxlength="100" class="form-control hwtype"
                value="{{ isset($cylinder)?$cylinder->hwtype:'' }}">
            <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editHwtype"
                value="Edit" />
        </div>
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Last Test Date</label>
    <div class="col-md-5">
        <div class="input-group-prepend">
            <div class="input-group-text bg-dark-blue text-white">
                <small><i class="fa fa-calendar"></i></small>
            </div>
            <input type="text" maxlength="100" name="testdate" id="datepicker" autocomplete="off" class="form-control"
                placeholder="dd-mm-yyyy"
                value="{{ isset($cylinder->testdate)?date('d-m-Y', strtotime($cylinder->testdate)):'' }}">
        </div>
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2"> Manufacturing Date</label>
    <div class="col-md-5">
        <div class="input-group-prepend">
            <div class="input-group-text bg-dark-blue text-white">
                <small><i class="fa fa-calendar"></i></small>
            </div>
            <input type="text" maxlength="100" name="mfgdate" id="datepicker1" autocomplete="off" class="form-control"
                placeholder="dd-mm-yyyy"
                value="{{ isset($cylinder->mfgdate)?date('d-m-Y', strtotime($cylinder->mfgdate)):'' }}">
        </div>
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Manufacturer</label>
    <div class="col-md-5">
        <div class="input-group-prepend">
            <select class="myselect form-control mfrcoy" style="width:96%" name="mfrcoy" id="mfrcoy"
                data-values="{{ $manufacturers }}" {{ isset($cylinder->mfr)?'disabled':'' }}>
                <option value=""></option>
                @foreach($manufacturers as $manufacturer)
                <option value="{{ $manufacturer->id }}"
                    {{ isset($cylinder)?$cylinder->mfr == $manufacturer->id  ? 'selected' : '' : ''}}>
                    {{ $manufacturer->code }}, {{ $manufacturer->descr }}</option>
                @endforeach
            </select>
            <input name="mfr" type="hidden" maxlength="100" class="form-control mfr"
                value="{{ isset($cylinder)?$cylinder->mfr:'' }}">
            <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editMfr" value="Edit" />
        </div>
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Material</label>
    <div class="col-md-5">
        <input name="material" type="text" maxlength="100" class="form-control"
            value="{{ isset($cylinder)?$cylinder->material:'' }}">
    </div>
</div>
{{-- <div class="input-group row narrow-padding-global">
            <label for="neckcode" class="col-md-2">Neck Code</label>
            <div class="col-md-5">
                <input name="neckcode" type="text" maxlength="100" class="form-control" value="{{ isset($cylinder)?$cylinder->neckcode:'' }}"
>
</div>
</div> --}}
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Original Owner</label>
    <div class="col-md-5">
        <input name="oriowner" type="text" maxlength="100" class="form-control"
            value="{{ isset($cylinder)?$cylinder->oriowner:'' }}">
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Original Tare Weight</label>
    <div class="col-md-5">
        <input name="oritareweight" type="text" maxlength="100" class="form-control"
            value="{{ isset($cylinder)?$cylinder->oritareweight:'' }}">
    </div>
</div>
{{-- <div class="input-group row narrow-padding-global">
            <label for="ramplot" class="col-md-2">Ramp Lot</label>
            <div class="col-md-5">
                <input name="ramplot" type="text" maxlength="100" class="form-control" value="{{ isset($cylinder)?$cylinder->ramplot:'' }}"
>
</div>
</div> --}}
{{-- <div class="input-group row narrow-padding-global">
            <label for="solvent" class="col-md-2">Solvent Code</label>
            <div class="col-md-5">
                <input name="solvent" type="text" maxlength="100" class="form-control" value="{{ isset($cylinder)?$cylinder->solvent:'' }}"
>
</div>
</div> --}}
<div class="input-group row narrow-padding-global">
    <label class="col-md-2">Valve Guard</label>
    <div class="col-md-5">
        <div class="radio-list">
            <label class="radio-inline">
                <input type="radio" name="valveguard" value="1"
                    {{ isset($cylinder)?($cylinder->valveguard=='1')? 'checked' : '': 'checked' }} /> Yes </font>
            </label>
            <label class="radio-inline">
                <input type="radio" name="valveguard" value="0"
                    {{ isset($cylinder)?($cylinder->valveguard=='0')? 'checked' : '': '' }} /> No </font></label>
        </div>
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Valve Outlet</label>
    <div class="col-md-5">
        <input name="valveoutlet" type="text" maxlength="100" class="form-control"
            value="{{ isset($cylinder)?$cylinder->valveoutlet:'' }}">
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Test Pressure</label>
    <div class="col-md-5">
        <input name="testpressure" type="text" maxlength="100" class="form-control"
            value="{{ isset($cylinder)?$cylinder->testpressure:'' }}">
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Working Pressure</label>
    <div class="col-md-5">
        <input name="workingpressure" type="text" maxlength="100" class="form-control"
            value="{{ isset($cylinder)?$cylinder->workingpressure:'' }}">
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Valve Type</label>
    <div class="col-md-5">
        <div class="input-group-prepend">
            <select class="myselect form-control valvetypecoy" style="width:96%" name="valvetypecoy" id="valvetypecoy"
                {{ isset($cylinder->valvetype)?'disabled':'' }}>
                <option value=""></option>
                @foreach($valvetypes as $valvetype)
                <option value="{{ $valvetype->id }}"
                    {{ isset($cylinder)?$cylinder->valvetype == $valvetype->id  ? 'selected' : '' : ''}}>
                    {{ $valvetype->code }}, {{ $valvetype->descr }}</option>
                @endforeach
            </select>
            <input name="valvetype" type="hidden" maxlength="100" class="form-control valvetype"
                value="{{ isset($cylinder)?$cylinder->valvetype:'' }}">
            <input type="button" name="editBtn" id="editBtn" class="btn btn-default pale-green editValve"
                value="Edit" />
        </div>
    </div>
</div>

<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2"> Value Manufacturer </label>
    <div class="col-md-5">
        <input name="valvemfr2" type="text" maxlength="100" class="form-control valvemfr2"
            value="{{ isset($cylinder)?$cylinder->valvemfr:'' }}" disabled>
        <input name="valvemfr" type="hidden" maxlength="100" class="form-control valvemfr"
            value="{{ isset($cylinder)?$cylinder->valvemfr:'' }}">
    </div>
</div>
<div class="input-group row narrow-padding-global">
    <label for="" class="col-md-2">Status</label>
    @php
        $isChecked = isset($cylinder)?$cylinder->status:'active'
    @endphp
    <div class="col-md-5">
        <div class="form-check form-check-inline">
            {!! Form::radio('Status', 'active', ($isChecked == 'active'), ['class' => 'my-class check-status'])!!}
            {!! Form::label('Active', '&nbsp;Active', ['class' => 'col-form-label']) !!}
        </div>
        <div class="form-check form-check-inline">
            {!! Form::radio('Status', 'inactive',  ($isChecked == 'inactive'), ['class' => 'my-class check-status'])!!}
            {!! Form::label('Inactive', '&nbsp;Inactive', ['class' => 'col-form-label']) !!}
        </div>
        <div class="form-check form-check-inline">
            {!! Form::radio('Status', 'sold',  ($isChecked == 'sold'), ['class' => 'my-class check-status'])!!}
            {!! Form::label('Sold', '&nbsp;Sold', ['class' => 'col-form-label']) !!}
        </div>
        <div class="form-check form-check-inline">
            {!! Form::radio('Status', 'disposed',  ($isChecked == 'disposed'), ['class' => 'my-class check-status'])!!}
            {!! Form::label('Disposed', '&nbsp;Disposed', ['class' => 'col-form-label']) !!}
        </div>
    </div>
   
</div> 
<div class="input-group row narrow-padding-global cylindergasrack-status">
    @if(isset($cylinder) && isset($cylinder->reasons))
    <label for="" class="col-md-2">Reasons</label>
    <div class="col-md-5">
        <input name="reasons" type="text" maxlength="100" class="form-control reasons" disabled
            value="{{ $cylinder->reasons }}">
    </div> 
    @endif
</div> 
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary btn-form-submit">Submit</button>
    </div>
</div>
</div>
</div>