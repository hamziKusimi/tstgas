@extends('master')

@section('content')

<div class="row">
    <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
        <a class="btn btn-success " href="{{ route('cylinders.create') }}"> Add Cylinder</a>
    </div>
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)


<div class="card">

    <div class="card-body">
        <div class="bg-white">
            <div class="row">
                <div class="col-md-2">
                    @if(isset($cylindersearch))
                    <a href="{{ route('cylinders.index') }}" style="width: 100%;" class="btn btn-danger"><i
                            class="fa"></i>Back</a>
                    @endif
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-4">
                    <form action="{{ route('cylinders.searchindex') }}" method="POST">
                        @csrf
                        <div class="input-group mb-3">
                            <input name="search" type="text" id="search" class="form-control"
                                placeholder="Search Cylinder">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Serial</th>
                            <th>Barcode</th>
                            <th>Type</th>
                            <th>Category</th>
                            <th>Product</th>
                            <th>Description</th>
                            <th width="280px">More</th>
                        </tr>
                    </thead>
                    {{-- @if(sizeof($cylinders) > 0) --}}
                    <tbody>
                        @foreach ($cylinders as $cylinder)
                        <tr>
                            <td width="7.5%">{{ ++$i }}</td>
                            <td width="12%"><a
                                    href="{{ route('cylinders.edit',$cylinder->id) }}">{{ $cylinder->serial }}</a></td>
                            <td width="12%">{{ $cylinder->barcode }}</td>
                            <td width="12%">{{ $cylinder->Cytype['code'] }}</td>
                            <td width="12%">{{ $cylinder->Category['code'] }}</td>
                            <td width="12%">{{ $cylinder->Product['code']}}</td>
                            <td width="22%">{{ $cylinder->descr }}</td>
                            <td width="8%">
                                <a href="{{ route('cylinders.destroy', $cylinder->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    {{-- @endif --}}
                </table>
            </div>

            {{ $cylinders->links() }}
        </div>
    </div>
</div>

{{-- {!! $cylinders->links() !!} --}}

@endsection
@push('scripts')
<script>
    $(document).ready(function() {
            // $('#example').DataTable({
            //     "bPaginate": false
            // });
        } );
</script>
@endpush