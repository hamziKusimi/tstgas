@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('drivers.store') }}" method="POST">
            @csrf
            @include('cylindermaster/drivers/form')

        </form>
    </div>
</div>


@endsection

@section('js')

<script type="text/javascript">
    @include('js.barcode-validation-js');
</script>

@endsection