<div class="form-group row">
    <label for="code" class="col-md-2"> User Code <span class="text-danger">*</span></label>
    <div class="col-md-5">
        {{-- <input name="code" type="select" maxlength="50" class="form-control"
            value="{{ isset($driver)?$driver->code:'' }}" required> --}}
            {!! Form::select('code', $usercode, isset($driver) ?$driver->code: null, [
                'class' => 'form-control select',
                'placeholder' => '',
                'required' => 'required'
            ]) !!}
    </div>
</div>

<div class="form-group row">
    <label for="description" class="col-md-2">Description <span class="text-danger">*</span></label>
    <div class="col-md-5">
        <input name="descr" type="text" maxlength="100" class="form-control"
            value="{{ isset($driver)?$driver->descr:'' }}" required>
    </div>
</div>

<div class="form-group row">
    <label class="control-label col-md-2">Active</label>
    <div class="col-md-10">
        <div class="radio-list">
            <label class="radio-inline">
                <input type="radio" name="active" value="1"
                    {{ isset($driver)?($driver->active=='1')? 'checked' : '': 'checked' }} /> Active </font></label>
            <label class="radio-inline">
                <input type="radio" name="active" value="0"
                    {{ isset($driver)?($driver->active=='0')? 'checked' : '': '' }} /> Non-Active </font></label>
        </div>
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>

@push('scripts')
<script>
    $(document).ready(function() {
        $('.select').select2();
    });
</script>
@endpush