@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('shackletypes.store') }}" method="POST">
            @csrf
            @include('cylindermaster/shackletypes/form')

        </form>
    </div>
</div>


@endsection
