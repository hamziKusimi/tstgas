@extends('master')
@section('content')


<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('cylinderproducts.store') }}" method="POST">
            @csrf
            @include('cylindermaster/cylinderproducts/form')

        </form>
    </div>
</div>


@endsection
