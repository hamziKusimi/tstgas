<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-param" content="_token" />

    <title>{{ $page_title ?? "AdminLTE Dashboard" }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('css/adminlte-skin-blue.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datatables.responsive.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datatables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap4-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">


</head>

<body class="skin-blue">

    <div class="wrapper">

        <!-- Header -->
        @include('header')
        <!-- Sidebar -->
        @include('sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            @yield('page-form-open')
            <section class="content-header">
                @yield('header')
                <h1>
                    {{ $page_title ?? "Page Title" }}
                    <small>{{ $page_description ?? null }}</small>
                </h1>
                <!-- You can dynamically generate breadcrumbs here -->
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    @if (isset($bclvl1))<li class="active"><a href="{{$bclvl1_url ?? NULL}}">{{ $bclvl1 ?? NULL}}</a>
                    </li>@endif
                    @if (isset($bclvl2))<li class="active"><a href="{{$bclvl2_url ?? NULL}}">{{ $bclvl2 ?? NULL}}</a>
                    </li>@endif
                    @if (isset($bclvl3))<li class="active"><a href="{{$bclvl3_url ?? NULL}}">{{ $bclvl3 ?? NULL}}</a>
                    </li>@endif
                </ol>
            </section>


            <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->
                @yield('content')
            </section><!-- /.content -->
            @yield('page-form-close')
        </div><!-- /.content-wrapper -->
        @yield('modals')
        {{-- TADA~ --}}
        <!-- Footer -->
        @include('footer')

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    {{-- <script src="{{ asset('js/bootstrap4-toggle.min.js') }}"></script> --}}
    <script>
        var url = window.location;
        // for sidebar menu but not for treeview submenu
        $('ul.sidebar-menu a').filter(function() {
            return this.href == url;
        }).parent().siblings().removeClass('active').end().addClass('active');
        // for treeview which is like a submenu
        $('li.treeview a').filter(function() {
            return this.href == url;
        }).parentsUntil(".treeview > .treeview-child").siblings().removeClass('active menu-open').end().addClass('active menu-open');
    </script>
    <script>
        (function() {
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>

    // DataTable setup for switching tabs
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc()
    })

    // prevent form submit on press enter, and instead, trigger the next input if have
    $('form').find('input').keydown(function(event){
        if (event.keyCode == 13 ){
            event.preventDefault();
        }
    })

    // AJAX setup for CSRF-TOKEN
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
}) ()
    </script>
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-inputmask.js') }}"></script>
    <script src="{{ asset('js/jquery-inputmask-bindings.js') }}"></script>
    <script src="{{ asset('js/jquery-inputmask-date-ext.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        (function() {
    $('.dropdown-toggle').dropdown({
        display: 'static'
    })

    $('[data-toggle="popover"]').popover()

}) ()
    </script>
    @stack('scripts')
</body>

</html>