@extends('master')
@section('content')

<div class="card">

    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('cylinderRental.print') }}" method="GET" target="_blank">
            <div class="row">
                <div class="col-7">
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                </div>
                {{-- <div class="col-4">
                    <div class="form-group">
                        <p><b>Filter by Barcode:</b></p>
                    </div>
                </div> --}}
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('Date ', 'Date', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('dates', null, ['class' => 'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'dates','readonly'=>'readonly'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {{-- {!! Form::checkbox('Debtor_Chkbx',null,null, [ 'id' =>'Debtor_Chkbx'])!!} --}}
                    {!! Form::label('Debtor', 'Debtor', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Debtor_frm', $Debtor, null, ['class' => 'form-control select', 'id' =>'Debtor_frm'
                        ])!!}
                    </div>
                </div>
                {{-- <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Debtor_to', $Debtor, null, ['class' => 'form-control select', 'id' =>'Debtor_to'
                        ])!!}
                    </div>
                </div> --}}
                <div class="col-1">
                </div>
                {{-- <div class="col-2">
                    {!! Form::radio('BarcodeFilter','all',null, [ 'id' =>'BarcodeFilter' ,'checked' =>'checked'] )!!}
                    {!! Form::label('BarcodeFilter', 'All', ['class' => 'col-form-label']) !!}
                </div> --}}
            </div>
 

            <hr>

            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit">Print</button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {
    // if($("#Debtor_Chkbx").prop("checked") == true){
    //     $("#Debtor_frm").prop('disabled', false);
    //     $("#Debtor_to").prop('disabled', false);
    // }else{
    //     $("#Debtor_frm").prop('disabled', true);
    //     $("#Debtor_to").prop('disabled', true);
    // }

    // $("#Debtor_Chkbx").change(function() {
    //     if(this.checked) {
    //         $("#Debtor_frm").prop('disabled', false);
    //         $("#Debtor_to").prop('disabled', false);
    //     }else{
    //         $("#Debtor_frm").prop('disabled', true);
    //         $("#Debtor_to").prop('disabled', true);
    //     }
    // });

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('.select').select2();
}) ()
</script>
@endpush