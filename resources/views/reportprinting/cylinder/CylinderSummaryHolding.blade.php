@extends('master')
@section('content')

<div class="card">

    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('CylinderSummaryHolding.print') }}" method="GET" target="_blank">
            <div class="row">
                <div class="col-7">
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Debtor_Chkbx',null,null, [ 'id' =>'Debtor_Chkbx'])!!}
                    {!! Form::label('Debtor', 'Debtor', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Debtor_frm', $Debtor, null, ['class' => 'form-control select', 'id'
                        =>'Debtor_frm', 'disabled' =>'disabled'
                        ])!!}
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Debtor_to', $Debtor, null, ['class' => 'form-control select', 'id'
                        =>'Debtor_to', 'disabled' =>'disabled'
                        ])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('DeliveryNote_Chkbx',null,null, [ 'id' =>'DeliveryNote_Chkbx'])!!}
                    {!! Form::label('DeliveryNote', 'Delivery No.', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('DeliveryNote_frm', $DeliveryNote, null, ['class' => 'form-control select',
                        'id'
                        =>'DeliveryNote_frm', 'disabled' =>'disabled'
                        ])!!}
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('DeliveryNote_to', $DeliveryNote, null, ['class' => 'form-control select', 'id'
                        =>'DeliveryNote_to', 'disabled' =>'disabled'
                        ])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Product_Chkbx',null,null, [ 'id' =>'Product_Chkbx'])!!}
                    {!! Form::label('Product', 'Product', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Product_frm', $Product, null, ['class' => 'form-control select', 'id'
                        =>'Product_frm','disabled' =>'disabled'
                        ])!!}
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Product_to', $Product, null, ['class' => 'form-control select', 'id'
                        =>'Product_to', 'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('LPO_Chkbx',null,null, [ 'id' =>'LPO_Chkbx'])!!}
                    {!! Form::label('LPO', 'LPO No', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::text('LPO_frm', null, ['class' => 'form-control ', 'id'
                        =>'LPO_frm', 'disabled' =>'disabled'
                        ])!!}
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::text('LPO_to', null, ['class' => 'form-control ', 'id'
                        =>'LPO_to', 'disabled' =>'disabled'
                        ])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::radio('date_radio','date_picker',null, [ 'id' =>'date_picker', 'checked'=>'checked'])!!}
                    {!! Form::label('Date ', 'As at Date', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('date', null, ['class' => 'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'datepicker', 'disabled' =>'disabled'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::radio('date_radio','date_range',null, [ 'id' =>'date_range'])!!}
                    {!! Form::label('Date ', 'Date', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('dates', null, ['class' => 'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'dates', 'disabled' =>'disabled'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit">Print</button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {

    if($("#Product_Chkbx").prop("checked") == true){
        $("#Product").prop('disabled', false);
    }else{
        $("#Product").prop('disabled', true);
    }

    $("#Product_Chkbx").change(function() {
        if(this.checked) {
            $("#Product_frm").prop('disabled', false);
            $("#Product_to").prop('disabled', false);
        }else{
            $("#Product_frm").prop('disabled', true);
            $("#Product_to").prop('disabled', true);
        }
    });

    if($("#LPO_Chkbx").prop("checked") == true){
        $("#LPO").prop('disabled', false);
    }else{
        $("#LPO").prop('disabled', true);
    }

    $("#LPO_Chkbx").change(function() {
        if(this.checked) {
            $("#LPO_frm").prop('disabled', false);
            $("#LPO_to").prop('disabled', false);
        }else{
            $("#LPO_frm").prop('disabled', true);
            $("#LPO_to").prop('disabled', true);
        }
    });

    if($("#DeliveryNote_Chkbx").prop("checked") == true){
        $("#DeliveryNote").prop('disabled', false);
    }else{
        $("#DeliveryNote").prop('disabled', true);
    }

    $("#DeliveryNote_Chkbx").change(function() {
        if(this.checked) {
            $("#DeliveryNote_frm").prop('disabled', false);
            $("#DeliveryNote_to").prop('disabled', false);
        }else{
            $("#DeliveryNote_frm").prop('disabled', true);
            $("#DeliveryNote_to").prop('disabled', true);
        }
    });

    if($("#Debtor_Chkbx").prop("checked") == true){
        $("#Debtor_frm").prop('disabled', false);
            $("#Debtor_to").prop('disabled', false);
    }else{
        $("#Debtor_frm").prop('disabled', true);
        $("#Debtor_to").prop('disabled', true);
    }

    $("#Debtor_Chkbx").change(function() {
        if(this.checked) {
            $("#Debtor_frm").prop('disabled', false);
            $("#Debtor_to").prop('disabled', false);
        }else{
            $("#Debtor_frm").prop('disabled', true);
            $("#Debtor_to").prop('disabled', true);
        }
    });

    $("#date_range").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
            $("#datepicker").prop('disabled', true);
        }else{
            $("#dates").prop('disabled', true);
            $("#datepicker").prop('disabled', false);
        }
    });

    $("#date_picker").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', true);
            $("#datepicker").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', false);
            $("#datepicker").prop('disabled', true);
        }
    });

    if($("#date_picker").prop("checked") == true) {
            $("#dates").prop('disabled', true);
            $("#datepicker").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', false);
            $("#datepicker").prop('disabled', true);
        }

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $('#datepicker').datepicker({
      autoclose: true,
      format: "dd/mm/yyyy"
    }).datepicker("setDate", new Date());
    $('.select').select2();
}) ()
</script>
@endpush