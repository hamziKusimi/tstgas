@extends('master')
@section('content')

<div class="card">

    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('cylinderledger.print') }}" method="GET" target="_blank">
            <div class="row">
                <div class="col-7">
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                </div>
                {{-- <div class="col-4">
                    <div class="form-group">
                        <p><b>Filter by Barcode:</b></p>
                    </div>
                </div> --}}
            </div>
            <div class="row">
                <div class="col-1">
                    {!! Form::label('Date ', 'Date :', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('dates', null, ['class' => 'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'dates','readonly'=>'readonly'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Type_Chkbx',null,null, [ 'id' =>'Type_Chkbx'])!!}
                    {!! Form::label('Type', 'Type', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Type_frm', $Type, null, ['class' => 'form-control select', 'id' =>'Type_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Type_to', $Type, null, ['class' => 'form-control select', 'id' =>'Type_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
                {{-- <div class="col-2">
                    {!! Form::radio('BarcodeFilter','all',null, [ 'id' =>'BarcodeFilter' ,'checked' =>'checked'] )!!}
                    {!! Form::label('BarcodeFilter', 'All', ['class' => 'col-form-label']) !!}
                </div> --}}
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Category_Chkbx',null,null, [ 'id' =>'Category_Chkbx'])!!}
                    {!! Form::label('Category', 'Category', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Category_frm', $Category, null, ['class' => 'form-control select', 'id'
                        =>'Category_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Category_to', $Category, null, ['class' => 'form-control select', 'id'
                        =>'Category_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
                {{-- <div class="col-2">
                    {!! Form::radio('BarcodeFilter','withoutBc',null, [ 'id' =>'BarcodeFilter'] )!!}
                    {!! Form::label('BarcodeFilter', 'Without Barcode', ['class' => 'col-form-label']) !!}
                </div> --}}
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Product_Chkbx',null,null, [ 'id' =>'Product_Chkbx'])!!}
                    {!! Form::label('Product', 'Product', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Product_frm', $Product, null, ['class' => 'form-control select', 'id'
                        =>'Product_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Product_to', $Product, null, ['class' => 'form-control select', 'id'
                        =>'Product_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
                {{-- <div class="col-2">
                    {!! Form::radio('BarcodeFilter','withBc',null, [ 'id' =>'BarcodeFilter'] )!!}
                    {!! Form::label('BarcodeFilter', 'With Barcode', ['class' => 'col-form-label']) !!}
                </div> --}}
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Group_Chkbx',null,null, [ 'id' =>'Group_Chkbx'])!!}
                    {!! Form::label('Group', 'Group', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Group_frm', $Group, null, ['class' => 'form-control select', 'id'
                        =>'Group_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Group_to', $Group, null, ['class' => 'form-control select', 'id' =>'Group_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit">Print</button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {
    if($("#Type_Chkbx").prop("checked") == true){
        $("#Type_frm").prop('disabled', false);
        $("#Type_to").prop('disabled', false);
    }else{
        $("#Type_frm").prop('disabled', true);
        $("#Type_to").prop('disabled', true);
    }

    if($("#Category_Chkbx").prop("checked") == true){
        $("#Category_frm").prop('disabled', false);
        $("#Category_to").prop('disabled', false);
    }else{
        $("#Category_frm").prop('disabled', true);
        $("#Category_to").prop('disabled', true);
    }

    if($("#Product_Chkbx").prop("checked") == true){
        $("#Product_frm").prop('disabled', false);
        $("#Product_to").prop('disabled', false);
    }else{
        $("#Product_frm").prop('disabled', true);
        $("#Product_to").prop('disabled', true);
    }

    if($("#Group_Chkbx").prop("checked") == true){
        $("#Group_frm").prop('disabled', false);
        $("#Group_to").prop('disabled', false);
    }else{
        $("#Group_frm").prop('disabled', true);
        $("#Group_to").prop('disabled', true);
    }
    if($("#HWtype_Chkbx").prop("checked") == true){
        $("#HWtype_frm").prop('disabled', false);
        $("#HWtype_to").prop('disabled', false);
    }else{
        $("#HWtype_frm").prop('disabled', true);
        $("#HWtype_to").prop('disabled', true);
    }
    if($("#Capacity_Chkbx").prop("checked") == true){
        $("#Capacity_frm").prop('disabled', false);
        $("#Capacity_to").prop('disabled', false);
    }else{
        $("#Capacity_frm").prop('disabled', true);
        $("#Capacity_to").prop('disabled', true);
    }
    if($("#Manufacturer_Chkbx").prop("checked") == true){
        $("#Manufacturer_frm").prop('disabled', false);
        $("#Manufacturer_to").prop('disabled', false);
    }else{
        $("#Manufacturer_frm").prop('disabled', true);
        $("#Manufacturer_to").prop('disabled', true);
    }
    if($("#ValveType_Chkbx").prop("checked") == true){
        $("#ValveType_frm").prop('disabled', false);
        $("#ValveType_to").prop('disabled', false);
    }else{
        $("#ValveType_frm").prop('disabled', true);
        $("#ValveType_to").prop('disabled', true);
    }

    $("#Type_Chkbx").change(function() {
        if(this.checked) {
            $("#Type_frm").prop('disabled', false);
            $("#Type_to").prop('disabled', false);
        }else{
            $("#Type_frm").prop('disabled', true);
            $("#Type_to").prop('disabled', true);
        }
    });
        $("#Product_Chkbx").change(function() {
        if(this.checked) {
            $("#Product_frm").prop('disabled', false);
            $("#Product_to").prop('disabled', false);
        }else{
            $("#Product_frm").prop('disabled', true);
            $("#Product_to").prop('disabled', true);
        }
    });
        $("#Category_Chkbx").change(function() {
        if(this.checked) {
            $("#Category_frm").prop('disabled', false);
            $("#Category_to").prop('disabled', false);
        }else{
            $("#Category_frm").prop('disabled', true);
            $("#Category_to").prop('disabled', true);
        }
    });
        $("#Group_Chkbx").change(function() {
        if(this.checked) {
            $("#Group_frm").prop('disabled', false);
            $("#Group_to").prop('disabled', false);
        }else{
            $("#Group_frm").prop('disabled', true);
            $("#Group_to").prop('disabled', true);
        }
    });
        $("#HWtype_Chkbx").change(function() {
        if(this.checked) {
            $("#HWtype_frm").prop('disabled', false);
            $("#HWtype_to").prop('disabled', false);
        }else{
            $("#HWtype_frm").prop('disabled', true);
            $("#HWtype_to").prop('disabled', true);
        }
    });
        $("#Capacity_Chkbx").change(function() {
        if(this.checked) {
            $("#Capacity_frm").prop('disabled', false);
            $("#Capacity_to").prop('disabled', false);
        }else{
            $("#Capacity_frm").prop('disabled', true);
            $("#Capacity_to").prop('disabled', true);
        }
    });
        $("#Manufacturer_Chkbx").change(function() {
        if(this.checked) {
            $("#Manufacturer_frm").prop('disabled', false);
            $("#Manufacturer_to").prop('disabled', false);
        }else{
            $("#Manufacturer_frm").prop('disabled', true);
            $("#Manufacturer_to").prop('disabled', true);
        }
    });
        $("#ValveType_Chkbx").change(function() {
        if(this.checked) {
            $("#ValveType_frm").prop('disabled', false);
            $("#ValveType_to").prop('disabled', false);
        }else{
            $("#ValveType_frm").prop('disabled', true);
            $("#ValveType_to").prop('disabled', true);
        }
    });

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('.select').select2();
}) ()
</script>
@endpush