@extends('master')
@section('content')

<div class="card">

    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('cylinderlocation.print') }}" method="GET" target="_blank">
            <div class="row">
                <div class="col-7">
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                </div>
                {{-- <div class="col-4">
                    <div class="form-group">
                        <p><b>Filter by Barcode:</b></p>
                    </div>
                </div> --}}
            </div>
           
            <div class="row">
                <div class="col-2">
                    {!! Form::label('Debtor', 'Debtor', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Debtor_frm', $Debtor, null, ['class' => 'form-control select', 'id'
                        =>'Debtor_frm',
                        ])!!}
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Debtor_to', $Debtor, null, ['class' => 'form-control select', 'id'
                        =>'Debtor_to',
                        ])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('Date ', 'As At', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('date', null, ['class' => 'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'datepicker','readonly'=>'readonly'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit">Print</button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {

    $('#datepicker').datepicker({
      autoclose: true,
      dateFormat: 'dd-mm-yy'
    }).datepicker("setDate", new Date());
    $('.select').select2();
}) ()
</script>
@endpush