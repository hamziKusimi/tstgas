<div class="row">
        <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">
            {{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:16px;margin-top: 0px;">{{ config('config.company.company_no') }}</p>
        <p style="text-align:center; font-size:22px; font-weight:bold;margin-top: 0px;">
                Stock Code Purchase Listing Report {{$date}}
        </p>
        <br>
    </div>
    
    <div class="card">
        <div class="card-body">
            <div class="bg-white">
                <div class="row">
                    <table class="table" cellspacing="0" style="width:100%">
                        <thead class="" border="1">
                            <tr>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">SN</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Stock Code</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Description</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">UOM</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Quantity</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Total</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Discount</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                                $totQty = 0;
                                $totTot = 0;
                                $totGross = 0;
                            @endphp
                            @foreach ($purchaseArray as $purchase)
                            <tr>
                                <td style="text-align:left;">{{ $i}}</td>
                                <td style="text-align:left;">{{ $purchase['item_code']}}</td>
                                <td style="text-align:left;">{{ $purchase['subject']}}</td>
                                <td style="text-align:left;">{{ $purchase['uom']}}</td>
                                {{-- <td style="text-align:right;">{{ number_format((float)$purchase['quantity'], 2, '.', '')}}</td> --}}
                                <td style="text-align:right;">{{ number_format((float)$purchase['quantity'])}}</td>
                                <td style="text-align:right;">{{ number_format((float)$purchase['amount'], 2, '.', '')}}</td>
                                <td style="text-align:right;">{{ "-"}}</td>
                                <td style="text-align:right;">{{ number_format((float)$purchase['t_amount'], 2, '.', '')}}</td>
                            </tr>
                            @php
                            $i++;
                            $totQty =$totQty + $purchase['quantity'];
                            $totTot =$totTot + $purchase['amount'];
                            $totGross = $totGross + $purchase['t_amount'];
                        @endphp
                            @endforeach
                            <tr>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:right;"><b>Grand Total :</b></td>
                                    {{-- <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totQty, 2, '.', '')}}</td> --}}
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totQty)}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totTot, 2, '.', '')}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{ "-"}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totGross, 2, '.', '')}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>