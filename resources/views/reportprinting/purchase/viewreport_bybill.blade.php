<div class="row">
        <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">
            {{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:16px;margin-top: 0px;">{{ config('config.company.company_no') }}</p>
        <p style="text-align:center; font-size:22px; font-weight:bold;margin-top: 0px;">
            Purchase Listing Report {{$date}}
        </p>
        <br>
    </div>
    
    <div class="card">
        <div class="card-body">
            <div class="bg-white">
                <div class="row">
                    <table class="table" cellspacing="0" style="width:100%">
                        <thead class="" border="1">
                            <tr>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">SN</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Doc. No</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Date</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Code</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Name</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Total</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Disc</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Chrg</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Gross</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totTot = 0;
                                $totGross = 0;
                            @endphp
                            @foreach ($purchaseArray as $purchase)
                            <tr>
                                <td style="text-align:left;">{{ $purchase['s_n']}}</td>
                                <td style="text-align:left;">{{ $purchase['doc_no']}}</td>
                                <td style="text-align:left;">{{ $purchase['date']}}</td>
                                <td style="text-align:left;">{{ $purchase['code']}}</td>
                                <td style="text-align:left;">{{ $purchase['name']}}</td>
                                <td style="text-align:right;">{{ $purchase['total']}}</td>
                                <td style="text-align:right;">{{ "-"}}</td>
                                <td style="text-align:right;">{{ "-"}}</td>
                                <td style="text-align:right;">{{ $purchase['gross']}}</td>
                            </tr>
                            @php
                            $totTot =$totTot + $purchase['total'];
                            $totGross = $totGross + $purchase['gross'];
                        @endphp
                            @endforeach
                            <tr>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totTot, 2, '.', '')}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{ "-"}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{ "-"}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totGross, 2, '.', '')}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>