<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;margin-bottom: 0px;">
            {{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:14px;margin-top: 0px;">{{ config('config.company.show_company_no') == 'true' ? config('config.company.company_no') :'' }}</p>
        <p style="text-align:center; font-size:16px; font-weight:bold;margin-top: 0px;">
                Stock Code Sales Listing Report {{$date}}
        </p>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="bg-white">
                <div class="row">
                    <table class="table" cellspacing="0" style="width:100%">
                        <thead class="" border="1">
                            <tr>
                                <th width="2%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">SN</p></th>
                                <th width="14%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Stock Code</p></th>
                                <th width="26%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Description</p></th>
                                <th width="7%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">UOM</p></th>
                                <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Quantity</p></th>
                                <th width="10%" style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Total</p></th>
                                <th width="7%" style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Discount</p></th>
                                <th width="10%" style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Total Excl. Tax</p></th>
                                <th width="7%" style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Tax Amt</p></th>
                                <th width="10%" style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"><p style="margin-top: 4px;">Total Incl. Tax</p></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                                $totQty = 0;
                                $totTot = 0;
                                $totGross = 0;
                                $totDiscount = 0;
                                $totTax = 0;
                                $totTaxAmount = 0;
                            @endphp
                            @foreach ($SalesArray as $sale)
                            @php
                            
                                if(is_numeric($sale['uom'])){
                                    $uoms = DB::select(DB::raw("
                                            SELECT code
                                            FROM uoms
                                            WHERE id = :code_id
                                    "), [
                                        'code_id'=>$sale['uom'],
                                    ])[0];
                                    $uom = $uoms->code;
                                }else{
                                    $uom = $sale['uom'];
                                }
                            @endphp
                            <tr>
                                <td style="text-align:left;font-size:12px;">{{ $i}}</td>
                                <td style="text-align:left;font-size:12px;">{{ $sale['item_code']}}</td>
                                <td style="text-align:left;font-size:12px;">{{ $sale['subject']}}</td>
                                <td style="text-align:left;font-size:12px;">{{ $uom}}</td>
                                @if($systemsetup == '1')
                                <td style="text-align:left;font-size:12px;"><p style="margin-top: 2px;">{{ number_format((float)$sale['quantity'], 2, '.', '')}}</p></td>
                                @else
                                <td style="text-align:left;font-size:12px;">
                                    <p style="margin-top: 2px;">{{ number_format((float)$sale['quantity'])}}</p></td>
                                @endif
                                <td style="text-align:right;font-size:12px;"><p style="margin-top: 2px;">{{ number_format((float)$sale['totalprice'], 2, '.', '')}}</p></td>
                                <td style="text-align:right;font-size:12px;"><p style="margin-top: 2px;">{{ number_format((float)$sale['discount'], 2, '.', '')}}</p></td>
                                <td style="text-align:right;font-size:12px;"><p style="margin-top: 2px;">{{ number_format((float)$sale['amount'], 2, '.', '')}}</p></td>
                                <td style="text-align:right;font-size:12px;"><p style="margin-top: 2px;">{{ number_format((float)$sale['taxamount'], 2, '.', '')}}</p></td>
                                <td style="text-align:right;font-size:12px;"><p style="margin-top: 2px;">{{ number_format((float)$sale['taxed_amount'], 2, '.', '')}}</p></td>
                            </tr>
                            @php
                            $i++;
                            $totQty =$totQty + $sale['quantity'];
                            $totTot =$totTot + $sale['totalprice'];
                            $totGross = $totGross + $sale['amount'];
                            $totDiscount = $totDiscount + $sale['discount'];
                            $totTax = $totTax + $sale['taxamount'];
                            $totTaxAmount = $totTaxAmount + $sale['taxed_amount'];
                        @endphp
                            @endforeach
                            <tr>
                                    <td style="text-align:left;font-size:12px;"></td>
                                    <td style="text-align:left;font-size:12px;"></td>
                                    <td style="text-align:left;font-size:12px;"></td>
                                    <td style="text-align:center;font-size:12px;"><b>Grand Total</b></td>
                                    @if($systemsetup == '1')
                                    <td  style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;"> <p style="margin-top: 2px;">{{  number_format((float)$totQty, 2, '.', '')}}</p></td>
                                    @else
                                    <td  style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;"> <p style="margin-top: 2px;">{{  number_format((float)$totQty)}}</p></td>
                                    @endif
                                    <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"> <p style="margin-top: 2px;">{{  number_format((float)$totTot, 2, '.', '')}}</p></td>
                                    <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"> <p style="margin-top: 2px;">{{ number_format((float)$totDiscount, 2, '.', '')}}</p></td>
                                    <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"> <p style="margin-top: 2px;">{{  number_format((float)$totGross, 2, '.', '')}}</p></td>
                                    <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"> <p style="margin-top: 2px;">{{  number_format((float)$totTax, 2, '.', '')}}</p></td>
                                    <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;"> <p style="margin-top: 2px;">{{  number_format((float)$totTaxAmount, 2, '.', '')}}</p></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>