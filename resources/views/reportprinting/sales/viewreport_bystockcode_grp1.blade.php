<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;margin-bottom: 0px;">
            {{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:14px;margin-top: 0px;">{{ config('config.company.show_company_no') == 'true' ? config('config.company.company_no') :'' }}</p>
        <p style="text-align:center; font-size:16px; font-weight:bold;margin-top: 0px;">
                Stock Code Sales Listing Report {{$date}}
        </p>
        <br>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="bg-white">
                <div class="row">
                    <table class="table" cellspacing="0" style="width:100%;border-spacing:10px 10px 10px 10px;" >
                        <thead class="" border="1">
                            <tr>
                                    <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">SN</p></th>
                                    <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">Stock Code</p></th>
                                    <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">Description</p></th>
                                    <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">UOM</p></th>
                                    <th style="text-align:center;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">Quantity</p></th>
                                    <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">Total</p></th>
                                    <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">Discount</p></th>
                                    <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;border-spacing:2px;"><p style="margin-top: 4px;">Amount</p></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $totQty = 0;
                            $i = 1;
                            $totTot = 0;
                            $totGross = 0;
                            $totDiscount = 0;
                            @endphp
                            @foreach ($SalesArray as $key=>$Sales)
                            <thead>
                                <tr>
                                    <th colspan="9" style="text-align:left;font-size:12px;">{{($key)}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @php
                                    $totQty_g = 0;
                                    $totTot_g = 0;
                                    $totGross_g = 0;
                                    $totDiscount_g = 0;
                                    @endphp
                                @foreach ($Sales as $sale)
                                <tr>
                                        <td style="text-align:left;font-size:12px;"> <p style="margin-top: 2px;">{{ $i}}</p></td>
                                        <td style="text-align:left;font-size:12px;"> <p style="margin-top: 2px;">{{ $sale['item_code']}}</p></td>
                                        <td style="text-align:left;font-size:12px;"> <p style="margin-top: 2px;">{{ $sale['subject']}}</p></td>
                                        <td style="text-align:left;font-size:12px;"> <p style="margin-top: 2px;">{{ $sale['uom']}}</p></td>
                                        @if($systemsetup == '1')
                                        <td style="text-align:center;font-size:12px;"> <p style="margin-top: 2px;">{{ number_format((float)$sale['quantity'], 2, '.', '')}}</p></td>
                                        @else
                                        <td style="text-align:center;font-size:12px;"> <p style="margin-top: 2px;">{{ number_format((float)$sale['quantity'])}}</p></td>
                                        @endif
                                        <td style="text-align:right;font-size:12px;"> <p style="margin-top: 2px;">{{ number_format((float)$sale['amount'], 2, '.', '')}}</p></td>
                                        {{-- <td style="text-align:right;font-size:12px;"> <p style="margin-top: 2px;">{{ "-"}}</td> --}}
                                        <td style="text-align:right;font-size:12px;"> <p style="margin-top: 2px;">{{ number_format((float)$sale['discount'], 2, '.', '')}}</p></td>
                                        <td style="text-align:right;font-size:12px;"> <p style="margin-top: 2px;">{{ number_format((float)$sale['amount'], 2, '.', '')}}</p></td>
                                </tr>
                                @php
                                $i++;
                                $totQty_g =$totQty_g + $sale['quantity'];
                                $totTot_g = $totTot_g + $sale['amount'];
                                $totDiscount_g = $totDiscount_g + $sale['discount'];
                                $totGross_g =  $totGross_g + $sale['amount'];
                                @endphp
                                @endforeach
                                <tr>
                                    <td colspan="4" style="text-align:right;font-size:12px;" ><b>{{"Sub Total by " . ucfirst($group_1) . ":"}}</p></b></td>

                                    @if($systemsetup == '1')
                                    <td style="text-align:center;border-top:1px solid;font-size:12px;">{{  number_format((float)$totQty_g, 2, '.', '')}}</td>
                                    @else
                                    <td style="text-align:center;border-top:1px solid;font-size:12px;">{{  number_format((float)$totQty_g)}}</td>
                                    @endif
                                    <td style="text-align:right;border-top:1px solid;font-size:12px;">{{ number_format((float)$totTot_g, 2, '.', '')}}</td>
                                    <td style="text-align:right;border-top:1px solid;font-size:12px;">{{ number_format((float)$totDiscount_g, 2, '.', '')}}</td>
                                    <td style="text-align:right;border-top:1px solid;font-size:12px;">{{  number_format((float)$totGross_g, 2, '.', '')}}</td>
                                </tr>
                            </tbody>
                            @php
                            $totQty = $totQty + $totQty_g;
                            $totTot = $totTot + $totTot_g;
                            $totGross =  $totGross + $totGross_g ;
                            $totDiscount = $totDiscount + $totDiscount_g;
                            @endphp
                            @endforeach
                            <tr>
                                <td colspan="4" style="text-align:right;font-size:12px;" ></td>

                                @if($systemsetup == '1')
                                <td style="text-align:center;border-top:1px solid;border-bottom:1px solid;font-size:12px;">{{  number_format((float)$totQty, 2, '.', '')}}</td>
                                @else
                                <td style="text-align:center;border-top:1px solid;border-bottom:1px solid;font-size:12px;">{{  number_format((float)$totQty)}}</td>
                                @endif
                                <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;">{{ number_format((float)$totTot, 2, '.', '')}}</td>
                                <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;">{{  number_format((float)$totDiscount, 2, '.', '')}}</td>
                                <td style="text-align:right;border-top:1px solid;border-bottom:1px solid;font-size:12px;">{{  number_format((float)$totGross, 2, '.', '')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>