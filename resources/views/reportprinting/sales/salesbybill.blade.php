@extends('master')
@section('content')
@if (Session::has('Error'))
<div class="alert alert-danger " role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Error')}}</p>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h5>Conditions {{Session('ac_chk')}}</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('salesreport.processbybill') }}" method="GET">
            <div class="form-group">
                <p><b>Filter by :</b></p>
            </div>
            <div class="row">
                @php
                if(Session::has('checkbox')){
                    $isInv = in_array("INV" ,Session('checkbox'));
                    $isCB = in_array("CB" ,Session('checkbox'));
                    $isDO = in_array("DO" ,Session('checkbox'));
                    $isSR = in_array("SR" ,Session('checkbox'));
                }else{
                    $isInv = true;
                    $isCB = true;
                    $isDO = true;
                    $isSR = true;
                }
                @endphp
                <div class="col-2">
                    {!! Form::checkbox('sales[]','INV',$isInv == false ?false:true,
                    [ 'id' =>'sales'])!!}
                    {!! Form::label('DocNo', 'Invoice Sales', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::checkbox('sales[]','CB', $isCB == false ?false:true,
                    [ 'id' =>'sales'])!!}
                    {!! Form::label('DocNo', 'Cash Bill', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::checkbox('sales[]','DO', $isDO == false ?false:true,
                    [ 'id' =>'sales'])!!}
                    {!! Form::label('DocNo', 'Delivery Order', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::checkbox('sales[]','SR', $isSR == false ?false:true,
                    [ 'id' =>'sales'])!!}
                    {!! Form::label('DocNo', 'Sales Return', ['class' => 'col-form-label']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('Date ', 'Date', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('dates', Session::has('date')?Session('date'):null, ['class' => 'form-control
                            form-data',
                            'autocomplete'=>'off', 'id' =>'dates'])!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('DocNo_Chkbx',null,(Session('docno_chk')=='on'?true:false),
                    [ 'id' =>'DocNo_Chkbx'])!!}
                    {!! Form::label('DocNo', 'Document No. :', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-3">
                    <div class="form-group">
                        {!! Form::text('DocNo_frm', Session::has('docno_from')?Session('docno_from'):null,
                        ['class' => 'form-control form-data',
                        'id' =>'DocNo_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {!! Form::text('DocNo_to', Session::has('docno_to')?Session('docno_to'):null,
                        ['class' => 'form-control form-data',
                        'id' =>'DocNo_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">

                    {!! Form::checkbox('AC_Code_Chkbx',null,(Session('ac_chk')=='on'?true:false),
                    [ 'id' =>'AC_Code_Chkbx'])!!}
                    {!! Form::label('AC_Code', 'Debtor :', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('AC_Code_frm', $Debtor,
                        Session::has('ac_from')?Session('ac_from'):null,
                        ['class' => 'form-control form-data select',
                        'id' =>'AC_Code_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('AC_Code_to', $Debtor,
                        Session::has('ac_to')?Session('ac_to'):null,
                        ['class' => 'form-control form-data select',
                        'id' =>'AC_Code_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="form-group">
                <p><b>Group By :</b></p>
            </div>
            <div class="row">
                <div class="col-2">
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            {!! Form::label('grp1', '1.&nbsp;', ['class' => 'col-form-label']) !!}
                            {!! Form::checkbox('grp[]', 'grp1',null, [ 'id' =>'grp_1_Checkbox',
                            'style'=>'margin-top: 13px;'])!!}
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::select('grp_1', $grouping ,0, ['class' => 'form-control form-data ','disabled'
                        =>'disabled', 'id' =>'grp_1'])!!}
                    </div>
                </div>

                <div class="col-2">
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            {!! Form::label('grp2', '2.&nbsp;', ['class' => 'col-form-label']) !!}
                            {!! Form::checkbox('grp[]', 'grp2',null, [ 'id' =>'grp_2_Checkbox',
                            'style'=>'margin-top: 13px;'])!!}
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::select('grp_2', $grouping ,0, ['class' => 'form-control form-data ','disabled'
                        =>'disabled', 'id' =>'grp_2'])!!}
                    </div>
                </div>

                <div class="col-2">
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            {!! Form::label('grp3', '3.&nbsp;', ['class' => 'col-form-label']) !!}
                            {!! Form::checkbox('grp[]', 'grp3',null, [ 'id' =>'grp_3_Checkbox',
                            'style'=>'margin-top: 13px;'])!!}
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::select('grp_3', $grouping ,0, ['class' => 'form-control form-data ','disabled'
                        =>'disabled', 'id' =>'grp_3'])!!}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-2" style="right: -20px;">
                    <button type="submit" name="Btn" value="search"
                        class="btn btn-primary form-control submit">Search</button>
                </div>
                <div class="col-md-2">
                    <button type="submit" name="Btn" value="print" class="btn btn-primary form-control submit"
                        {{Session::has('Sales')?null:'disabled'}}>Print</button>
                </div>
            </div>
            <hr>
        </form>

        <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body" id="target">
                <table id="table" class="table table-bordered table-striped" cellspacing="0">
                    <thead class="">
                        <tr>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Creditor</th>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Gross</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(Session::has('Sales'))
                        @php
                        $Sales = Session::get('Sales');
                        session(['SalesReport' => $Sales]);
                        $isSearch = 1;
                        @endphp
                        @foreach ($Sales as $sale)
                        <tr>
                            <td>{{$sale['docno']}}</td>
                            <td>{{ date("d/m/Y", strtotime($sale['DATE']))}}</td>
                            <td>{{$sale['account_code']}}</td>
                            <td>{{$sale['NAME']}}</td>
                            <td>{{$sale['amount']}}</td>
                            <td>{{$sale['taxed_amount']}}</td>
                        </tr>
                        @endforeach
                        @else
                        @php
                        $isSearch = 0;
                        @endphp
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    (function() {
    let isSearch = {{ $isSearch }};

    if(isSearch){
        $('html, body').animate({
        scrollTop: $("#target").offset().top
        }, 2000);
    }

    $('#table').DataTable({
        "searching": false
    });

    if($("#AC_Code_Chkbx").prop("checked") == true){
        $("#AC_Code_frm").prop('disabled', false);
        $("#AC_Code_to").prop('disabled', false);
    }else{
        $("#AC_Code_frm").prop('disabled', true);
        $("#AC_Code_to").prop('disabled', true);
    }

    $("#AC_Code_Chkbx").change(function() {
        if(this.checked) {
            $("#AC_Code_frm").prop('disabled', false);
            $("#AC_Code_to").prop('disabled', false);
        }else{
            $("#AC_Code_frm").prop('disabled', true);
            $("#AC_Code_to").prop('disabled', true);
        }
    });

    if($("#DocNo_Chkbx").prop("checked") == true){
        $("#DocNo_frm").prop('disabled', false);
        $("#DocNo_to").prop('disabled', false);
    }else{
        $("#DocNo_frm").prop('disabled', true);
        $("#DocNo_to").prop('disabled', true);
    }

    $("#DocNo_Chkbx").change(function() {
        if(this.checked) {
            $("#DocNo_frm").prop('disabled', false);
            $("#DocNo_to").prop('disabled', false);
        }else{
            $("#DocNo_frm").prop('disabled', true);
            $("#DocNo_to").prop('disabled', true);
        } 
    });

    if($("#Cat_Chkbx").prop("checked") == true){
        $("#Cat_frm").prop('disabled', false);
        $("#Cat_to").prop('disabled', false);
    }else{
        $("#Cat_frm").prop('disabled', true);
        $("#Cat_to").prop('disabled', true);
    }

    $("#Cat_Chkbx").change(function() {
        if(this.checked) {
            $("#Cat_frm").prop('disabled', false);
            $("#Cat_to").prop('disabled', false);
        }else{
            $("#Cat_frm").prop('disabled', true);
            $("#Cat_to").prop('disabled', true);
        } 
    });

    $("#grp_1_Checkbox").change(function() {
        if(this.checked) {
            $("#grp_1").prop('disabled', false);
        }else{
            $("#grp_1").prop('disabled', true);
        } 
    });

    $("#grp_2_Checkbox").change(function() {
        if(this.checked) {
            $("#grp_2").prop('disabled', false);
        }else{
            $("#grp_2").prop('disabled', true);
        } 
    });

    $("#grp_3_Checkbox").change(function() {
        if(this.checked) {
            $("#grp_3").prop('disabled', false);
        }else{
            $("#grp_3").prop('disabled', true);
        } 
    });
    
    if($("#grp_1_Checkbox").prop("checked") == true){
        $("#grp_1").prop('disabled', false);
    }else{
        $("#grp_1").prop('disabled', true);
    }

    if($("#grp_2_Checkbox").prop("checked") == true){
        $("#grp_2").prop('disabled', false);
    }else{
        $("#grp_2").prop('disabled', true);
    }

    if($("#grp_3_Checkbox").prop("checked") == true){
        $("#grp_3").prop('disabled', false);
    }else{
        $("#grp_3").prop('disabled', true);
    }

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('.select').select2();
}) ()
</script>
@endpush