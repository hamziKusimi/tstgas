<div class="row">
        <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">
            {{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:16px;margin-top: 0px;">{{ config('config.company.company_no') }}</p>
        <p style="text-align:center; font-size:22px; font-weight:bold;margin-top: 0px;">
                Sales Listing Report {{$date}}
        </p>
        <br>
    </div>
    
    <div class="card">
        <div class="card-body">
            <div class="bg-white">
                <div class="row">
                    <table class="table" cellspacing="0" style="width:100%">
                        <thead class="" border="1">
                            <tr>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">SN</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Doc. No</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Date</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Code</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Name</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Total</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Disc</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Chrg</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Gross</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 1;
                            $totTot = 0;
                            $totGross = 0;
                            @endphp
                            @foreach ($SalesArray as $key1=>$Sales_1)
                            <thead>
                                <tr>
                                    <th colspan="9" style="text-align:left;">{{($key1)}}</th>
                                </tr>
                            </thead>

                            <tbody>
                                    @php
                                    $totTot_g1 = 0;
                                    $totGross_g1 = 0;
                                    @endphp  
                                    @foreach ($Sales_1 as $key2=>$Sales_2)
                                    <thead>
                                            <tr>
                                                <th></th>
                                                <th colspan="2" style="text-align:left;">{{($key2)}}</th>
                                                <th colspan="7" ></th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                            @php
                                            $totTot_g2 = 0;
                                            $totGross_g2 = 0;
                                            @endphp
                                        @foreach ($Sales_2 as $key3=>$Sales_3)
                                        <thead>
                                                <tr>
                                                    <th></th>
                                                    <th style="text-align:center;">{{($key3)}}</th>
                                                    <th colspan="8" ></th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                                @php
                                                $totTot_g3 = 0;
                                                $totGross_g3 = 0;
                                                @endphp
                                                @foreach ($Sales_3 as $sale)
                                                <tr>
                                                        <td>{{($i)}}</td>
                                                        <td style="text-align:left;">{{ $sale['docno']}}</td>
                                                        <td style="text-align:left;">{{date("d/m/Y", strtotime( $sale['DATE']))}}</td>
                                                        <td style="text-align:left;">{{ $sale['CODE']}}</td>
                                                        <td style="text-align:left;">{{ $sale['NAME']}}</td>
                                                        <td style="text-align:right;">{{ $sale['amount']}}</td>
                                                        <td style="text-align:right;">{{ "-"}}</td>
                                                        <td style="text-align:right;">{{ "-"}}</td>
                                                        <td style="text-align:right;">{{ $sale['taxed_amount']}}</td>
                                                    </tr>
                                                    @php
                                                    $i++;
                                                    $totTot_g3 = $totTot_g3 + $sale['amount'];
                                                    $totGross_g3 =  $totGross_g3 + $sale['taxed_amount'];
                                                    @endphp
                                                    @endforeach  
                                                <tr>
                                                    <td colspan="5" style="text-align:right;" ><b>{{"Sub Total by " . ucfirst($group_3) . ":"}}</b></td>
                                                    <td style="text-align:right;border-top:2px solid;">{{  number_format((float)$totTot_g3, 2, '.', '')}}</td>
                                                    <td style="text-align:right;border-top:2px solid;">{{ "-"}}</td>
                                                    <td style="text-align:right;border-top:2px solid;">{{ "-"}}</td>
                                                    <td style="text-align:right;border-top:2px solid;">{{  number_format((float)$totGross_g3, 2, '.', '')}}</td>
                                                </tr>
                                        </tbody>
                                        @php
                                        $totTot_g2 = $totTot_g2 + $totTot_g3;
                                        $totGross_g2 =  $totGross_g2 + $totGross_g3;
                                        @endphp
                                        @endforeach
                                        <tr>
                                            <td colspan="5" style="text-align:right;" ><b>{{"Sub Total by " . ucfirst($group_2) . ":"}}</b></td>
                                            <td style="text-align:right;border-top:2px solid;">{{  number_format((float)$totTot_g2, 2, '.', '')}}</td>
                                            <td style="text-align:right;border-top:2px solid;">{{ "-"}}</td>
                                            <td style="text-align:right;border-top:2px solid;">{{ "-"}}</td>
                                            <td style="text-align:right;border-top:2px solid;">{{  number_format((float)$totGross_g2, 2, '.', '')}}</td>
                                        </tr>
                                    </tbody>
                                    @php
                                    $totTot_g1 = $totTot_g1 + $totTot_g2;
                                    $totGross_g1 =  $totGross_g1 + $totGross_g2;
                                    @endphp
                                    @endforeach
                                <tr>
                                    <td colspan="5" style="text-align:right;" ><b>{{"Sub Total by " . ucfirst($group_1) . ":"}}</b></td>
                                    <td style="text-align:right;border-top:2px solid;">{{  number_format((float)$totTot_g1, 2, '.', '')}}</td>
                                    <td style="text-align:right;border-top:2px solid;">{{ "-"}}</td>
                                    <td style="text-align:right;border-top:2px solid;">{{ "-"}}</td>
                                    <td style="text-align:right;border-top:2px solid;">{{  number_format((float)$totGross_g1, 2, '.', '')}}</td>
                                </tr>
                            </tbody>
                            
                            @php
                            $totTot = $totTot + $totTot_g1;
                            $totGross =  $totGross + $totGross_g1;
                            @endphp
                            @endforeach
                            <tr>
                                <td colspan="5" style="text-align:right;" ></td>
                                <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totTot, 2, '.', '')}}</td>
                                <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{ "-"}}</td>
                                <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{ "-"}}</td>
                                <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totGross, 2, '.', '')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>