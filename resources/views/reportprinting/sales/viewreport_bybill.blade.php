<div class="row">
        <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">
            {{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:16px;margin-top: 0px;">{{ config('config.company.company_no') }}</p>
        <p style="text-align:center; font-size:22px; font-weight:bold;margin-top: 0px;">
                Sales Listing Report {{$date}}
        </p>
        <br>
    </div>
    
    <div class="card">
        <div class="card-body">
            <div class="bg-white">
                <div class="row">
                    <table class="table" cellspacing="0" style="width:100%">
                        <thead class="" border="1">
                            <tr>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">SN</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Doc. No</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Date</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Code</th>
                                <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Name</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Total</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Disc</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Chrg</th>
                                <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Gross</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                                $totTot = 0;
                                $totGross = 0;
                            @endphp
                            @foreach ($SalesArray as $sale)
                            <tr>
                                <td style="text-align:left;">{{ $i}}</td>
                                <td style="text-align:left;">{{ $sale['docno']}}</td>
                                <td style="text-align:left;">{{date("d/m/Y", strtotime($sale['DATE']))}}</td>
                                <td style="text-align:left;">{{ $sale['CODE']}}</td>
                                <td style="text-align:left;">{{ $sale['NAME']}}</td>
                                <td style="text-align:right;">{{ $sale['amount']}}</td>
                                <td style="text-align:right;">{{ "-"}}</td>
                                <td style="text-align:right;">{{ "-"}}</td>
                                <td style="text-align:right;">{{ $sale['taxed_amount']}}</td>
                            </tr>
                            @php
                            $i++;
                            $totTot =$totTot + $sale['amount'];
                            $totGross = $totGross + $sale['taxed_amount'];
                            @endphp
                            @endforeach
                            <tr>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:left;"></td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totTot, 2, '.', '')}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{ "-"}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{ "-"}}</td>
                                    <td style="text-align:right;border-top:2px solid;border-bottom:2px solid;">{{  number_format((float)$totGross, 2, '.', '')}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>