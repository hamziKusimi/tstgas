@extends('master')
@section('content')
@if (Session::has('Error'))
<div class="alert alert-danger " role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Error')}}</p>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h5>Conditions  {{Session('ac_chk')}}</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('salesreport.processbystockcode') }}" method="GET">
            <div class="form-group">
                <p><b>Filter by :</b></p>
            </div>
            <div class="row">
                    @php
                    if(Session::has('checkbox')){
                        $isInv = in_array("INV" ,Session('checkbox'));
                        $isCB = in_array("CB" ,Session('checkbox'));
                        $isDO = in_array("DO" ,Session('checkbox'));
                        $isSR = in_array("SR" ,Session('checkbox'));
                    }else{
                        $isInv = true;
                        $isCB = true;
                        $isDO = true;
                        $isSR = true;
                    }
                    @endphp
                    <div class="col-2">
                        {!! Form::checkbox('sales[]','INV',$isInv == false ?false:true,
                        [ 'id' =>'sales'])!!}
                        {!! Form::label('DocNo', 'Invoice Sales', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-2">
                        {!! Form::checkbox('sales[]','CB', $isCB == false ?false:true,
                        [ 'id' =>'sales'])!!}
                        {!! Form::label('DocNo', 'Cash Bill', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-2">
                        {!! Form::checkbox('sales[]','DO', $isDO == false ?false:true,
                        [ 'id' =>'sales'])!!}
                        {!! Form::label('DocNo', 'Delivery Order', ['class' => 'col-form-label']) !!}
                    </div>
                    <div class="col-2">
                        {!! Form::checkbox('sales[]','SR', $isSR == false ?false:true,
                        [ 'id' =>'sales'])!!}
                        {!! Form::label('DocNo', 'Sales Return', ['class' => 'col-form-label']) !!}
                    </div>
                </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('Date ', 'Date', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('dates', Session::has('date')?Session('date'):null, ['class' => 'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'dates'])!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('STK_Code_Chkbx',null,(Session('STK_Code_Chkbx')=='on'?true:false),
                         [ 'id' =>'STK_Code_Chkbx'])!!}
                        {!! Form::label('STK_Code', 'Stock Code ', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('STK_Code_frm', $Stockcode,
                            Session::has('STK_Code_from')?Session('STK_Code_from'):null,
                            ['class' => 'form-control select', 'id'
                            =>'STK_Code_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('STK_Code_to', $Stockcode, Session::has('STK_Code_from')?Session('STK_Code_from'):null,
                            ['class' => 'form-control select',
                            'id' =>'STK_Code_to','disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Category_Chkbx',null,(Session('Category_Chkbx')=='on'?true:false), [ 'id' =>'Category_Chkbx'])!!}
                        {!! Form::label('Category', 'Category', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Category_frm', $Category, Session::has('Category_from')?Session('Category_from'):null,
                            ['class' => 'form-control select', 'id'
                            =>'Category_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Category_to', $Category, Session::has('Category_from')?Session('Category_from'):null,
                             ['class' => 'form-control select', 'id'
                            =>'Category_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Product_Chkbx',null,(Session('Product_Chkbx')=='on'?true:false), [ 'id' =>'Product_Chkbx'])!!}
                        {!! Form::label('Product', 'Product', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Product_frm', $Product, Session::has('Product_from')?Session('Product_from'):null,
                             ['class' => 'form-control select', 'id'
                            =>'Product_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Product_to', $Product, Session::has('Product_from')?Session('Product_from'):null,
                             ['class' => 'form-control select', 'id'
                            =>'Product_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Brand_Chkbx',null,(Session('Brand_Chkbx')=='on'?true:false), [ 'id' =>'Brand_Chkbx'])!!}
                        {!! Form::label('Brand', 'Brand', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Brand_frm', $Brand, Session::has('Brand_from')?Session('Brand_from'):null,
                             ['class' => 'form-control select', 'id'
                            =>'Brand_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Brand_to', $Brand, Session::has('Brand_from')?Session('Brand_from'):null,
                             ['class' => 'form-control select', 'id' =>'Brand_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Location_Chkbx',null,(Session('Location_Chkbx')=='on'?true:false), [ 'id' =>'Location_Chkbx'])!!}
                        {!! Form::label('Location', 'Location', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Location_frm', $Location, Session::has('Location_from')?Session('Location_from'):null,
                             ['class' => 'form-control select', 'id'
                            =>'Location_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Location_to', $Location, Session::has('Location_from')?Session('Location_from'):null,
                             ['class' => 'form-control select', 'id'
                            =>'Location_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>

            <hr>

            <div class="form-group">
                <p><b>Group By :</b></p>
            </div>
            <div class="row">
                <div class="col-2">
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            {!! Form::label('grp1', '1.&nbsp;', ['class' => 'col-form-label']) !!}
                            {!! Form::checkbox('grp[]', 'grp1',null, [ 'id' =>'grp_1_Checkbox',
                            'style'=>'margin-top: 13px;'])!!}
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::select('grp_1', $grouping ,0, ['class' => 'form-control form-data ','disabled'
                        =>'disabled', 'id' =>'grp_1'])!!}
                    </div>
                </div>

                <div class="col-2">
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            {!! Form::label('grp2', '2.&nbsp;', ['class' => 'col-form-label']) !!}
                            {!! Form::checkbox('grp[]', 'grp2',null, [ 'id' =>'grp_2_Checkbox',
                             'style'=>'margin-top: 13px;'])!!}
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::select('grp_2', $grouping ,0, ['class' => 'form-control form-data ','disabled'
                        =>'disabled', 'id' =>'grp_2'])!!}
                    </div>
                </div>

                <div class="col-2">
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            {!! Form::label('grp3', '3.&nbsp;', ['class' => 'col-form-label']) !!}
                            {!! Form::checkbox('grp[]', 'grp3',null, [ 'id' =>'grp_3_Checkbox',
                             'style'=>'margin-top: 13px;'])!!}
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::select('grp_3', $grouping ,0, ['class' => 'form-control form-data ','disabled'
                        =>'disabled', 'id' =>'grp_3'])!!}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-2" style="right: -20px;">
                    <button type="submit" name="Btn" value="search"
                        class="btn btn-primary form-control submit">Search</button>
                </div>
                <div class="col-md-2">
                    <button type="submit" name="Btn" value="print" class="btn btn-primary form-control submit"
                  {{Session::has('Sales')?null:'disabled'}}>Print</button>
                </div>
            </div>
            <hr>
        </form>

        @php
            $systemSetup = DB::select(DB::raw(" SELECT qty_decimal, qty_decimal_place, rate_decimal, rate_decimal_place FROM system_setups"))[0];
        @endphp
        <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body" id="target">
                <table id="table" class="table table-bordered table-striped" cellspacing="0">
                    <thead class="">
                        <tr>
                            <th>Stock Code</th>
                            <th>Description</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th>Discount</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(Session::has('Sales'))
                        @php
                        $Sales = Session::get('Sales');
                        session(['SalesReport' => $Sales]);
                        $isSearch = 1;
                        @endphp
                        @foreach ($Sales as $sale)
                        @php
                            if($systemSetup->qty_decimal == 1){
                                $decimalPlace = $systemSetup->qty_decimal_place == '' ? 2:$systemSetup->qty_decimal_place;
                                $qty = number_format((float)$sale['quantity'], $decimalPlace, '.', '');
                            }else{
                                $qty = number_format($sale['quantity']);
                            }
                        @endphp
                        <tr>
                            <td>{{ $sale['item_code']}}</td>
                            <td>{{ $sale['subject']}}</td>
                            <td>{{ $qty  }}</td>
                            <td>{{ number_format((float)$sale['totalprice'], 2, '.', '')}}</td>
                            <td>{{ number_format((float)$sale['discount'], 2, '.', '')}}</td>
                            <td>{{ number_format((float)$sale['amount'], 2, '.', '')}}</td>
                        </tr>
                        @endforeach
                        @else
                        @php
                        $isSearch = 0;
                        @endphp
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    (function() {
    let isSearch = {{ $isSearch }};

    if(isSearch){
        $('html, body').animate({
        scrollTop: $("#target").offset().top
        }, 2000);
    }

    $('#table').DataTable({
        "searching": false
    });

    if($("#STK_Code_Chkbx").prop("checked") == true){
        $("#STK_Code_frm").prop('disabled', false);
        $("#STK_Code_to").prop('disabled', false);
    }else{
        $("#STK_Code_frm").prop('disabled', true);
        $("#STK_Code_to").prop('disabled', true);
    }

    if($("#Category_Chkbx").prop("checked") == true){
        $("#Category_frm").prop('disabled', false);
        $("#Category_to").prop('disabled', false);
    }else{
        $("#Category_frm").prop('disabled', true);
        $("#Category_to").prop('disabled', true);
    }

    if($("#Product_Chkbx").prop("checked") == true){
        $("#Product_frm").prop('disabled', false);
        $("#Product_to").prop('disabled', false);
    }else{
        $("#Product_frm").prop('disabled', true);
        $("#Product_to").prop('disabled', true);
    }

    if($("#Brand_Chkbx").prop("checked") == true){
        $("#Brand_frm").prop('disabled', false);
        $("#Brand_to").prop('disabled', false);
    }else{
        $("#Brand_frm").prop('disabled', true);
        $("#Brand_to").prop('disabled', true);
    }

    if($("#Location_Chkbx").prop("checked") == true){
        $("#Location_frm").prop('disabled', false);
         $("#Location_to").prop('disabled', false);
    }else{
        $("#Location_frm").prop('disabled', true);
        $("#Location_to").prop('disabled', true);
    }

    $("#STK_Code_Chkbx").change(function() {
        if(this.checked) {
            $("#STK_Code_frm").prop('disabled', false);
            $("#STK_Code_to").prop('disabled', false);
        }else{
            $("#STK_Code_frm").prop('disabled', true);
            $("#STK_Code_to").prop('disabled', true);
        }
    });

    $("#Category_Chkbx").change(function() {
        if(this.checked) {
            $("#Category_frm").prop('disabled', false);
            $("#Category_to").prop('disabled', false);
        }else{
            $("#Category_frm").prop('disabled', true);
            $("#Category_to").prop('disabled', true);
        }
    });

    $("#Product_Chkbx").change(function() {
        if(this.checked) {
            $("#Product_frm").prop('disabled', false);
            $("#Product_to").prop('disabled', false);
        }else{
            $("#Product_frm").prop('disabled', true);
            $("#Product_to").prop('disabled', true);
        }
    });

    $("#Brand_Chkbx").change(function() {
        if(this.checked) {
            $("#Brand_frm").prop('disabled', false);
            $("#Brand_to").prop('disabled', false);
        }else{
            $("#Brand_frm").prop('disabled', true);
            $("#Brand_to").prop('disabled', true);
        }
    });

    $("#Location_Chkbx").change(function() {
        if(this.checked) {
            $("#Location_frm").prop('disabled', false);
            $("#Location_to").prop('disabled', false);
        }else{
            $("#Location_frm").prop('disabled', true);
            $("#Location_to").prop('disabled', true);
        }
    });

    $("#grp_1_Checkbox").change(function() {
        if(this.checked) {
            $("#grp_1").prop('disabled', false);
        }else{
            $("#grp_1").prop('disabled', true);
        }
    });

    $("#grp_2_Checkbox").change(function() {
        if(this.checked) {
            $("#grp_2").prop('disabled', false);
        }else{
            $("#grp_2").prop('disabled', true);
        }
    });

    $("#grp_3_Checkbox").change(function() {
        if(this.checked) {
            $("#grp_3").prop('disabled', false);
        }else{
            $("#grp_3").prop('disabled', true);
        }
    });

    if($("#grp_1_Checkbox").prop("checked") == true){
        $("#grp_1").prop('disabled', false);
    }else{
        $("#grp_1").prop('disabled', true);
    }

    if($("#grp_2_Checkbox").prop("checked") == true){
        $("#grp_2").prop('disabled', false);
    }else{
        $("#grp_2").prop('disabled', true);
    }

    if($("#grp_3_Checkbox").prop("checked") == true){
        $("#grp_3").prop('disabled', false);
    }else{
        $("#grp_3").prop('disabled', true);
    }

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('.select').select2();
}) ()
</script>
@endpush