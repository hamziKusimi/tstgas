@extends('master')
@section('content')

<div class="card">

    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="retailer" class="table table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>PERUNCIT</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>{{ $item }}</th>
                            <th>{{$r_date}}</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>A</td>
                            <td>B</td>
                            <td>C</td>
                            <td>D</td>
                            {{-- <td>{{ $data['name_address'] }}</td> --}}
                            <td>E</td>
                            {{-- <td>{{ $data['opening_stock'] }}</td> --}}
                            <td>F</td>
                            <td>G</td>
                            <td>H</td>
                            <td>I</td>
                            <td>J</td>
                        </tr>
                        <tr>
                            <td>BIL</td>
                            <td>TARIKH PENERIMAAN</td>
                            <td>KUANTITI BELIAN</td>
                            <td>HARGA BELIAN</td>
                            <td>NAMA DAN ALAMAT PENUH PEMBEKAL</td>
                            <td>NO INVOIS</td>
                            <td>STOK PERMULAAN</td>
                            <td>STOK SEMASA</td>
                            <td>JUMLAH JUALAN</td>
                            <td>STOK TAMAT</td>
                        </tr>
                        @foreach ($inv as $data)
                        <tr>
                            <td>{{ $data['no'] }}</td>
                            <td>{{ $data['doc_date'] }}</td>
                            <td>{{ $data['quantity'] }}</td>
                            <td>{{ $data['price'] }}</td>
                            <td>{{ "" }}</td>
                            <td>{{ $data['doc_no'] }}</td>
                            <td>{{ ""}}</td>
                            <td>{{ $data['current_stock'] }}</td>
                            <td>{{ $data['total_sale'] }}</td>
                            <td>{{ $data['closing_stock'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- {{ $pagi->links() }} --}}
</div>

@endsection
@push('scripts')
<script>
    $(document).ready(function() {
            $('#retailer').DataTable({
                ordering: false,
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5'
                ]
            });

        } );
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
@endpush