<table style = "border-collapse: collapse;font-size:9px;" >
    <thead >
     <tr>
        <th colspan="8" style = "font-size:12px;">{{$title}}</th>
    </tr>
    <tr>
        <th style = "font-size:9px;" >PEMBEKAL / PEMBORONG</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    <th  style = "border: 1px solid #000000;font-size:9px;" >{{$item}}</th>
    <th  style = "border: 1px solid #000000;font-size:9px;" >{{strtoupper(date("M",strtotime( $date))). "-" . date("y",strtotime( $date))}}</th>
        <th ></th>
    </tr>
    <tr>
        <th colspan="8"></th>
    </tr>
    <tr>
        <th colspan="8"></th>
    </tr>
    <tr >
        <th style = "text-align: center;font-size:9px;">JUALAN</th>
        <th ></th>
        <th ></th>
        <th ></th>
        <th ></th>
        <th ></th>
        <th ></th>
        <th style = "border: 1px solid #000000;font-size:9px;"></th>
    </tr>
    </thead>
    <tbody>

        <tr>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center" >BIL.</td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" >TARIKH</td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" >KUANTITI</td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" >HARGA KG</td>
            <td style = "border: 1px solid #000000;width: 30px;text-align: center;font-size:9px;" >NAMA DAN ALAMAT PENUH <br>PEMBELI</td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" >NO. INVOICE</td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" >NO. <br>KENDERAAN <br>MENGHANTAR</td>
            <td style = "border: 1px solid #000000;width: 16px;text-align: center;font-size:9px;" valign="center" >STOK TAMAT</td>
        </tr>
        @php
            $i = 1;
            $t_qty = 0;
        @endphp
        @foreach ( $data as $dt)
        <tr>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $i++ }}</td>
        <td style = "border: 1px solid #000000;font-size:9px;" >{{  $dt['doc_date'] /*strtoupper(date("d",strtotime( $date))). "." . date("m",strtotime( $date)) . "." . date("y",strtotime( $date))*/}}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" > {{ $dt['quantity'] }} </td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['price'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['name'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['doc_no'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['vehicle_no'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
        </tr>
        @php
            $isAllNull = ($dt['addr1'] == null) && ($dt['addr2'] == null) && ($dt['addr3'] == null) && ($dt['addr4'] == null);
        @endphp
        @if (!$isAllNull)

            <tr>
                @php
                if($dt['addr1'] != null){
                    $display = $dt['addr1'];
                }else{
                    $display = $dt['licenseNo'];
                    $isAllNull = true;
                }
                @endphp
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
                <td style = "border: 1px solid #000000;text-align:left;font-size:9px;" >{{ $display }}</td>
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            </tr>

        <tr>
            @php
            if($dt['addr2'] != null){
                $display = $dt['addr2'];
            }else{
                if(!$isAllNull){
                    $display = $dt['licenseNo'];
                    $isAllNull = true;
                }else{
                    $display = null;
                }
            }
            @endphp
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;text-align:left;font-size:9px;" >{{ $display }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
        </tr>
        <tr>
            @php
            if($dt['addr3'] != null){
                $display = $dt['addr3'] . " " . $dt['addr4'];
            }else{
                if(!$isAllNull){
                    $display = $dt['licenseNo'];
                    $isAllNull = true;
                }else{
                    $display = null;
                }
            }
            @endphp
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;text-align:left;font-size:9px;" >{{ $display }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;" ></td>
            <td style = "border: 1px solid #000000;" ></td>
        </tr>

        @endif
        @php
            $t_qty =  $t_qty + (int)$dt['quantity'];
        @endphp
        @endforeach
        <tr>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center" ></td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" >TOTAL</td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" >{{ $t_qty}}</td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" > </td>
            <td style = "border: 1px solid #000000;width: 30px;text-align: center;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" ></td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" ></td>
            <td style = "border: 1px solid #000000;width: 16px;text-align: center;font-size:9px;" valign="center" ></td>
        </tr>

    </tbody>
</table>