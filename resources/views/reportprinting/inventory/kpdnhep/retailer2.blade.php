<table style = "border-collapse: collapse;font-size:9px;" >
    <thead >
     <tr>
        <th colspan="10" style = "font-size:12px;">{{$title}}</th>
    </tr>
    <tr>
        <th colspan="10"></th>
    </tr>
    <tr>
        <th></th>
        <th style = "font-size:9px;">PERUNCIT</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th  style = "border: 1px solid #000000;font-size:9px;" >{{$item}}</th>
        <th  style = "border: 1px solid #000000;font-size:9px;" >{{strtoupper($date)}}</th>
        <th ></th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">A</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">B</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">C</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">D</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">E</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">F</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">G</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">H</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">I</td>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center">J</td>
        </tr>
        <tr>
            <td style = "border: 1px solid #000000;width: 7px;text-align: center;font-size:9px;" valign="center" >BIL.</td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" >TARIKH <br>PENERIMAAN</td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" >KUANTITI <br>BELIAN</td>
            <td style = "border: 1px solid #000000;width: 10px;text-align: center;font-size:9px;" valign="center" >HARGA <br>BELIAN</td>
            <td style = "border: 1px solid #000000;width: 30px;text-align: center;font-size:9px;" >NAMA DAN ALAMAT PENUH <br>PEMBEKAL</td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" >NO. INVOICE</td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" >STOK <br>PERMULAAN</td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" >STOK <br>SEMASA</td>
            <td style = "border: 1px solid #000000;width: 15px;text-align: center;font-size:9px;" valign="center" >JUMLAH  <br>JUALAN</td>
            <td style = "border: 1px solid #000000;width: 16px;text-align: center;font-size:9px;" valign="center" >STOK TAMAT</td>
        </tr>
        @php

        $t_qty = 0;
        $t_sale = 0;
        $t_data = count($data);
        // dd($t_data);
        @endphp
        @foreach ( $data as $dt)
        @php
            $t_qty =  $t_qty + (int)$dt['quantity'];
            $t_sale = $t_sale +  (int)$dt['total_sale'];
        @endphp
        <tr>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['no'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['doc_date']}}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" > {{ $dt['quantity'] }} </td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['price'] != 0 ? strval(number_format((float)$dt['price'],2)) :'' }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['supplier'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['doc_no'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ "" }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['current_stock'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['total_sale'] }}</td>
            <td style = "border: 1px solid #000000;font-size:9px;" >{{ $dt['closing_stock'] }}</td>
        </tr>
        @endforeach
        @if ( $t_data < 7)
        @php
            $t_row = 7 - $t_data;
            for($i=0;$i<$t_row;$i++){
        @endphp
        <tr>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            @php
            if($checkRow >0){
            @endphp
                <td style = "border: 1px solid #000000;font-size:9px;" >{{$supplierDetail[5-$checkRow]}}</td>
            @php
            $checkRow = $checkRow - 1;
            }else{
            @endphp
                <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            @php
            }
            @endphp
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
            <td style = "border: 1px solid #000000;font-size:9px;" ></td>
        </tr>
        @php
            }
        @endphp
    @endif

        <tr>
            <td></td>
            <td style = "border: 1px solid #000000;font-size:9px;">TOTAL</td>
            <td style = "border: 1px solid #000000;font-size:9px;"> {{  $t_qty }} </td>
            <td colspan ="4"></td>
            <td style = "border: 1px solid #000000;font-size:9px;">TOTAL</td>
            <td style = "border: 1px solid #000000;font-size:9px;">{{$t_sale}}</td>
            <td></td>
        </tr>

    </tbody>
</table>