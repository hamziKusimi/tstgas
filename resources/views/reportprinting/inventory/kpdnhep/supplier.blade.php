@extends('master')
@section('content')

<div class="card">

    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="supplier" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Name, Full Adress</th>
                            <th>DOC. No</th>
                            <th>Closing Stock</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($inv as $data)
                        <tr>
                            <td>{{ $data['no'] }}</td>
                            <td>{{ $data['doc_date'] }}</td>
                            <td>{{ $data['quantity'] }}</td>
                            <td>{{ $data['price'] }}</td>
                            <td>{{ $data['name_address'] }}</td>
                            <td>{{ $data['doc_no'] }}</td>
                            <td>{{ $data['closing_stock'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- {{ $pagi->links() }} --}}
</div>

@endsection
@push('scripts')
<script>
    $(document).ready(function() {
            $('#supplier').DataTable({
                ordering: false,
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5'
                ]
            });
        } );
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
@endpush