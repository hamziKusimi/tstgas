@extends('master')
@section('content')
@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

@if (Session::has('Error'))
<div class="alert alert-danger " role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Error')}}</p>
</div>
@endif
<div class="card">

    <div class="card-header">

    </div>
    <div class="card-body">
        <form action="{{ route('codereplacement.replace') }}" method="GET">
            {{-- @csrf --}}

            <div class="row">
                    <div class="col-2">
                            {!! Form::label('code', 'Old Stock Code', ['class' => 'col-form-label']) !!}
                    </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append">
                                    {!! Form::text('old_code', Session::has('old_code')?Session::get('old_code'):null, ['class' => 'form-control old_code', 'id'
                                    =>'old_code', 'required'=>'required'])!!}
                                    <div class="input-group-text bg-dark-blue search-stock pointer"  data-toggle="modal" data-target=".stockcode-modal" style="text-decoration: underline;"">
                                        <small>
                                                <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>

            <div class="row">
                    <div class="col-2">
                            {!! Form::label('code', 'New Stock Code', ['class' => 'col-form-label']) !!}
                    </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append">
                                    {!! Form::text('new_code', Session::has('new_code')?Session::get('new_code'):null, ['class' => 'form-control new_code', 'id'
                                    =>'new_code','style'=>'max-width: 85%;','required'=>'required'])!!}
                                </div>
                            </div>
                        </div>
            </div>

            <div class="row">
                    <div class="col-2"> </div>
                        <label class="col-form-label"></label>
                        <div class="col-2">
                                <div class="form-group row">
                                        <button type="submit" class="btn btn-primary form-control submit" style="margin-left:19px;">Submit</button>
                                </div>
                        </div>
            </div>


        </form>

    </div>
</div>

@endsection

@section('modals')

{{-- @include('dailypro.components.stockcode-modal-v2') --}}

@include('common.modal.search-stock')

@endsection

@push('scripts')
<script>
    (function() {


    // $('.fa-search').on('click', function () {
    // let modal = $('#stockcode-modal').clone()
    // let getRoute = modal.data('get-route')
    // let table = modal.find('#stockcode-table')

    // // show modal
    // // modal.modal()
    // // modal.on('hidden.bs.modal', function (e) { $(this).remove() })
    
    // $.ajax({
    //     url: getRoute,
    //     method: 'GET',
    //     }).done(function (resources) {
    //     resources.forEach(resource => {
    //         let markup = `
    //         <tr class="pointer" data-choice="${resource.code}">
    //             <td class="acode" >${resource.code}</td>
    //             <td class="name" >${resource.descr}</td>
    //         </tr>
    //         `

    //         table.find('tbody').append(markup)
    //     });

    //     // init datatable
    //     table.DataTable()
    //     // table.on('click', 'tr', function () {
    //     //     // first we check if the select2 exist (in billing, uses debtor-select, in vouchers using account_code)
    //     //     // then triggers the change depending on which one exist - by default check for debtor-select first
    //     //     if ($('#debtor-select').val()) {
    //     //         $('#debtor-select').val($(this).data('choice')).trigger('change')
    //     //     } else {
    //     //         $('#account_code').val($(this).data('choice')).trigger('change')
    //     //     }
    //     //     $('#debtor-select').val($(this).data('choice')).trigger('change')
    //     //     modal.modal('hide')
    //     // })
    // })
    // })


    $('.search-stock').on('click', function () {
    let modal = $('.search-stock-modal-template').clone().removeClass('search-document-modal-template')
    let getRoute = modal.data('get-route')
    let table = modal.find('table')
    // show modal
    modal.modal()
    modal.on('hidden.bs.modal', function (e) { $(this).remove() })
    $.ajax({
        url: getRoute,
        method: 'GET',
    }).done(function (resources) {
        resources.forEach(resource => {
            let markup = `
            <tr class="pointer" data-choice="${resource.code}">
                <td class="acode" width="25%">${resource.code}</td>
                <td class="name" width="75%">${resource.descr}</td>
            </tr>
            `

            table.find('tbody').append(markup)
        });

        // init datatable
        table.DataTable()
        table.on('click', 'tr', function () {
                
            $('#old_code').val($(this).data('choice')).trigger('change')
            modal.modal('hide')
        })
    })
})

}) ()
</script>
@endpush