@extends('master')
@section('content')

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

@if (Session::has('Error'))
<div class="alert alert-danger " role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Error')}}</p>
</div>
@endif
<div class="card">
    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('Kpdnhep_ret.process') }}" method="GET">
            <div class="row">
                <div class="col-7">
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                </div>
            </div>
            @php
            $i=1;
            @endphp
            @foreach ($Stockcode as $item)
            @php
            if(Session::has('Items')){
            $tickItem = Session::get('Items');
            $isTick = is_integer(array_search($item,$tickItem)) ? true:false;
            }else{
            $isTick = false;
            }

            @endphp
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Stockcode_chk[]', $item, $isTick, [ 'id' =>'Stockcode_chk'])!!}
                    {!! Form::label('Stockcode', $item, ['class' => 'col-form-label']) !!}
                </div>
            </div>
            @php
            $i++;
            @endphp
            @endforeach
            <div class="row">
                <div class="col-2">
                </div>
                <div class="col-2">
                    {!! Form::checkbox('noQuota', true, null, [ 'id' =>'noQuota'])!!}
                    {!! Form::label('Stockcode', "No Quota", ['class' => 'col-form-label']) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    {!! Form::label('Quota ', 'Quota', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::input('number','Quota',(Session::has('Quota') ? Session::get('Quota'): ""), ['class' =>
                            'form-control form-data', 'type'=>'number',
                            'autocomplete'=>'off', 'id' =>'Quota' ,'required'=>'required'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    {!! Form::label('Date ', 'Date', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-3">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('dates', (Session::has('Dates') ? Session::get('Dates'): ""), ['class' =>
                            'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'dates'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="row">
                <div class="col-2">
                    {!! Form::label('Doc NO ', 'Initial Document No.', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-3">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('doc_no',(Session::has('DocNo') ? Session::get('DocNo'): ""), ['class' =>
                            'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'doc_no' ,'required'=>'required'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div> --}}
            <hr>

            <div class="form-group row">
                <div class="col-md-3">
                    <button type="submit" name="Btn" value="view"
                        class="btn btn-primary form-control submit">View</button>
                </div>
                @if(Auth::user()->hasPermissionTo('PROCESSING_CR') || Auth::user()->hasPermissionTo('PROCESSING_UP'))
                    <div class="col-md-3">
                        <button type="submit" name="Btn" value="generate"
                            class="btn btn-primary form-control submit">Generate Purchased</button>
                    </div>
                @endif
            </div>
        </form>

    </div>
</div>
<br>
<br>
@if (Session::has('view'))
<div class="card">
    <div class="card-header">
        <h5>View Reports</h5>
    </div>
    <div class="card-body" id="target">
        <table id="retailer" class="table table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Document Type</th>
                    <th>Purchased Quantity</th>
                    <th>Document No</th>
                    <th>Date</th>
                    <th>Item Code</th>
                    <th>Sale Quantity</th>
                </tr>
            </thead>
            @php
            $Data = Session::get('view');
            @endphp
            @foreach ($Data as $item)
            <tr>
                <td>{{ $item['no'] }}</td>
                <td>{{ $item['doc_type'] }}</td>
                <td>{{ $item['p_qty'] }}</td>
                <td>{{ $item['doc_no'] }}</td>
                <td>{{ $item['date'] }}</td>
                <td>{{ $item['stock_code'] }}</td>
                <td>{{ $item['qty'] }}</td>
            </tr>
            @endforeach

        </table>
    </div>
</div>
@endif
@endsection

@push('scripts')
<script>
    (function() {
        $('#retailer').DataTable({
                ordering: false,
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'csvHtml5'
                ]

            });

    if($("#noQuota").prop("checked") == true){
        $("#Quota").prop('disabled', true);
    }else{
        $("#Quota").prop('disabled', false);
    }

    $("#noQuota").change(function() {
        if(this.checked) {
            $("#Quota").prop('disabled', true);
        }else{
            $("#Quota").prop('disabled', false);
        }
    });

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('.select').select2();
    $('html, body').animate({
        scrollTop: $("#target").offset().top
    }, 2000);
}) ()
</script>
@endpush