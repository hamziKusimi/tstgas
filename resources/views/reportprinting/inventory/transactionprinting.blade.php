@extends('master')

@section('content')



@push('scripts')

<script>

    var val = ''

$( document ).ready(function() {

    $('#type').on('change', function(){

        let form = $('.card-body').find('form')

        let type = $(this).val()

        let route = $('.routes').data(type)

            form.attr("action", route)      



    })

})

</script>

@endpush

<div class="card">

    <div class="card-header">

        <h5>Conditions</h5>

    </div>

    <div class="card-body">



                {{-- <form action="" method="POST">

                @csrf --}}

                <label for="" class="col-md-2">Transaction<span class="text-danger">*</span></label>   

                    <div class="col-md-4">

                        {!! Form::select('type',[

                        'cashbill' => 'Cashbill',

                        'invoice' => 'Invoice'],

                        null, ['placeholder' => '', 'class' => 'myselect form-control', 'id'=> 'type'])!!}

                    {{-- <select class="myselect form-control" name="type" id="type" required> 

                        <option value=""></option>

                        <option value="Cashbill">Cashbill</option>

                        <option value="Invoice">Invoice</option>

                    </select> --}}

                </div>      

                {{-- </form> --}}



            {{-- <div class="cashbill ">

                    @php $route= route('cashbills.print') @endphp

            </div>

            <div class="invoices">

                    @php $route= route('invoices.print') @endphp

            </div>

            <form action="{{ $route }}" method="GET" target="_blank"> --}}



            <div class="routes" data-invoice="{{ route('invoices.print') }}" data-cashbill="{{ route('cashbills.print') }}">


            <form method="GET" target=_blank>

                        



            <div class="modal-header">

            </div>



                <div class="row">

                    <div class="col-3">

                        <p><b>Report Type :</b></p>

                    </div>

                </div>

                <div class="row">

                    <div class="col-2">

                        {!! Form::radio('cashbill_rcv','listing',null, ['checked'=>'checked','id'

                        =>'listing'])!!}

                        {!! Form::label('cashbill_rcv_label', 'Listing', ['class' => 'col-form-label']) !!}

                    </div>

                    <div class="col-2">

                        {!! Form::radio('cashbill_rcv','summary',null, [ 'id' =>'summary'])!!}

                        {!! Form::label('cashbill_rcv_label', 'Summary', ['class' => 'col-form-label']) !!}

                    </div>

                </div>

                <hr>

                <div class="form-group">

                    <p><b>Filter by :</b></p>

                </div>

                <div class="row">

                    <div class="col-3">

                        {!! Form::checkbox('dates_Chkbx',null,null, [ 'id' =>'dates_Chkbx'])!!}

                        {!! Form::label('Date', 'Date', ['class' => 'col-form-label']) !!}

                    </div>



                    <label class="col-form-label">:</label>

                    <div class="col-4">

                        <div class="form-group">

                            <div class="input-group date">

                                {!! Form::text('dates', null, ['class' => 'form-control form-data',

                                'autocomplete'=>'off', 'id' =>'dates'

                                ])!!}

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-3">

                        {!! Form::checkbox('docno_Chkbx',null,null, [ 'id' =>'docno_Chkbx'])!!}

                        {!! Form::label('docno', 'Document No.', ['class' => 'col-form-label']) !!}

                    </div>

                    <label class="col-form-label">:</label>

                    <div class="col-3">

                        <div class="form-group">

                            <div class="input-group-append">

                                {!! Form::text('docno_frm', null, ['class' => 'form-control docno_frm', 'id'

                                =>'docno_frm'

                                ,'disabled' =>'disabled'])!!}

                                {{-- {!! Form::select('docno_frm',  $docno_select, null, ['class' => 'form-control select',

                                'id'

                                =>'docno_frm'

                                ,'disabled' =>'disabled'])!!} --}}

                                <div class="input-group-text bg-dark-blue text-white pointer" data-toggle="modal" data-target=".reportdocno-modal" style="text-decoration: underline;"">

                                    <small>

                                            <i class="fa fa-search"></i>

                                    </small>

                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="col-3">

                        <div class="form-group">

                            <div class="input-group-append">

                                {!! Form::text('docno_to', null, ['class' => 'form-control docno_to', 'id'

                                =>'docno_to'

                                ,'disabled' =>'disabled'])!!}

                                {{-- {!! Form::select('docno_to',  $docno_select, null, ['class' => 'form-control select',

                                'id'

                                =>'docno_to'

                                ,'disabled' =>'disabled'])!!} --}}

                                <div class="input-group-text bg-dark-blue text-white pointer docnoto" data-toggle="modal" data-target=".reportdocnoto-modal" style="text-decoration: underline;"">

                                    <small>

                                            <i class="fa fa-search"></i>

                                    </small>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-3">

                        {!! Form::checkbox('LPO_Chkbx_1',null,null, [ 'id' =>'LPO_Chkbx_1'])!!}

                        {!! Form::label('LPO', 'LPO No.', ['class' => 'col-form-label']) !!}

                    </div>

                    <label class="col-form-label">:</label>

                    <div class="col-3">

                        <div class="form-group">

                            {!! Form::text('LPO_frm_1', null, ['class' => 'form-control', 'id'

                            =>'LPO_frm_1'

                            ,'disabled' =>'disabled'])!!}

                        </div>

                    </div>

                    <div class="col-3">

                        <div class="form-group">

                            {!! Form::text('LPO_to_1', null, ['class' => 'form-control', 'id'

                            =>'LPO_to_1'

                            ,'disabled' =>'disabled'])!!}

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-3">

                        {!! Form::checkbox('debCode_Chkbx',null,null, [ 'id' =>'debCode_Chkbx'])!!}

                        {!! Form::label('debCode', 'Debtor Code', ['class' => 'col-form-label']) !!}

                    </div>

                    <label class="col-form-label">:</label>

                    <div class="col-3">

                        <div class="form-group">

                            {!! Form::select('debCode_frm', $debtor_select, null, ['class' => 'form-control

                            select', 'id'

                            =>'debCode_frm'

                            ,'disabled' =>'disabled'])!!}

                        </div>

                    </div>

                    <div class="col-3">

                        <div class="form-group">

                            {!! Form::select('debCode_to', $debtor_select, null, ['class' => 'form-control

                            select', 'id'

                            =>'debCode_to'

                            ,'disabled' =>'disabled'])!!}

                        </div>

                    </div>

                </div>

                {{-- @elseif(Auth::user()->hasPermissionTo('DEBTOR_LISTING_PRY')) --}}

                    <div class="form-group row">

                        <div class="col-md-8"></div>

                        <div class="col-md-4">

                            <button type="submit" class="btn btn-primary form-control submit">Print</button>

                        </div>

                    </div>

                {{-- @endif --}}

        </form>



    </div>

</div>

    

@endsection

@section('modals')

    @include('dailypro.components.reportdocno-modal')
    @include('dailypro.components.reportdocnoto-modal')

@endsection

@push('scripts')

<script>

    

    $(".myselect").select2({

        

        theme: 'bootstrap4',

        width: 'style',

      });

      

    (function() {


    $('.select').select2();

    $('#dates').daterangepicker({

        linkedCalendars: false,

        locale: { format: 'DD/MM/YYYY' }

    });



    $("#dates_Chkbx").change(function() {

        if(this.checked) {

            $("#dates").prop('disabled', false);

        }else{

            $("#dates").prop('disabled', true);

        }

    });



    if($("#dates_Chkbx").prop("checked") == true){

        $("#dates").prop('disabled', false);

    }else{

        $("#dates").prop('disabled', true);

    }



    if($("#docno_Chkbx").prop("checked") == true){

        $("#docno_frm").prop('disabled', false);

        $("#docno_to").prop('disabled', false);

    }else{

        $("#docno_frm").prop('disabled', true);

        $("#docno_to").prop('disabled', true);

    }



    if($("#LPO_Chkbx_1").prop("checked") == true){

        $("#LPO_frm_1").prop('disabled', false);

        $("#LPO_to_1").prop('disabled', false);

    }else{

        $("#LPO_frm_1").prop('disabled', true);

        $("#LPO_to_1").prop('disabled', true);

    }



    if($("#debCode_Chkbx").prop("checked") == true){

        $("#debCode_frm").prop('disabled', false);

        $("#debCode_to").prop('disabled', false);

    }else{

        $("#debCode_frm").prop('disabled', true);

        $("#debCode_to").prop('disabled', true);

    }



    $("#docno_Chkbx").change(function() {

        if(this.checked) {

            $("#docno_frm").prop('disabled', false);

            $("#docno_to").prop('disabled', false);

        }else{

            $("#docno_frm").prop('disabled', true);

            $("#docno_to").prop('disabled', true);

        }

    });



    $("#LPO_Chkbx_1").change(function() {

        if(this.checked) {

            $("#LPO_frm_1").prop('disabled', false);

            $("#LPO_to_1").prop('disabled', false);

        }else{

            $("#LPO_frm_1").prop('disabled', true);

            $("#LPO_to_1").prop('disabled', true);

        }

    });



    $("#refNo_Chkbx_2").change(function() {

        if(this.checked) {

            $("#refNo_frm_2").prop('disabled', false);

            $("#refNo_to_2").prop('disabled', false);

        }else{

            $("#refNo_frm_2").prop('disabled', true);

            $("#refNo_to_2").prop('disabled', true);

        }

    });



    $("#debCode_Chkbx").change(function() {

        if(this.checked) {

            $("#debCode_frm").prop('disabled', false);

            $("#debCode_to").prop('disabled', false);

        }else{

            $("#debCode_frm").prop('disabled', true);

            $("#debCode_to").prop('disabled', true);

        }

    });

    $('.reportdocno-modal').on('shown.bs.modal', function() {

        let getRoute = $('.reportdocno-modal').data('get-route')
        let table = $('.reportdocno-modal').find('table')
        
        $('.searchdocno').on('click', function() {
            
            let data = {
                'search' : $('.search').val(),
            }

            $.ajax({

                data: data,
                method: 'POST',
                url: getRoute

            }).done(function(resources) {

                resources.forEach(resource => {

                    // $("tbody").remove()

                    let markup = `

                        <tr class="route">
                            <input type="hidden" value="${resource.docno}" class="docno">
                            <td class="docNo" width="25%">${resource.docno}</td>
                            <td class="date" width="15%">${resource.date}</td>
                            <td class="name" width="15%">${resource.name}</td>           
                        </tr>

                    `
                    table.find('tbody').append(markup)
                });

                
                table.on('dblclick', 'tr', function() {
                    let id = $(this).find('.docno').val()
                    $('.docno_frm').val(id)      
                })

            })
        
        
        })
       

    })

    $('.reportdocnoto-modal').on('shown.bs.modal', function() {

        let getRoute = $('.reportdocnoto-modal').data('get-route')

        let table = $('.reportdocnoto-modal').find('table')

        $('.searchdocnoto').on('click', function() {
            
            let data = {

                'search' : $('.searchdocto').val(),

            }



            $.ajax({

                data: data,

                method: 'POST',

                url: getRoute

            }).done(function(resources) {

                resources.forEach(resource => {

                    // let id = resource.id
                    
                        
                    // $("tbody").remove()

                    let markup = `

                        <tr class="route">

                            <input type="hidden" value="${resource.docno}" class="docno">
                            <td class="docNo" width="25%">${resource.docno}</td>

                            <td class="date" width="15%">${resource.date}</td>

                            <td class="name" width="15%">${resource.name}</td>
                            

                        </tr>

                    `
                    table.find('tbody').append(markup)
                    // table.find('.route').attr('data-route', '{{ route('cashbill.editRoute','ADD') }}')
                });

                
                table.on('dblclick', 'tr', function() {
                    let id = $(this).find('.docno').val()
                    $('.docno_to').val(id)
                    // console.log(id)
                    // let getRoute2 = $(this).data('route')
                    // let route = getRoute2.replace('ADD', id)
                    


                    // $.ajax({

                    //         url: route,

                    //         method: 'GET',

                    //     }).done(function(responses) {

                    //         console.log(responses)
                            

                    //     })        

                })

            })


        })


        })


}) ()

</script>

@endpush