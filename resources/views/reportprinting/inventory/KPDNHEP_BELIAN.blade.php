@extends('master')
@section('content')

<div class="card">

    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('Kpdnhep_ret.processbelian') }}" method="GET" target="_blank">
            <div class="row">
                <div class="col-7">
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    {!! Form::label('Stockcode', 'Stock Item code', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                    <div class="form-group">
                        {!! Form::select('Stockcode', $Stockcode, null, ['class' => 'form-control select', 'id'
                        =>'Stockcode'])!!}
                    </div>
                </div>
                <div class="col-1">
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    {!! Form::label('Date ', 'Date', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-4">
                        <div class="form-group">
                            <div class="input-group date">
                                {!! Form::text('dates', null, ['class' => 'form-control form-data',
                                'autocomplete'=>'off', 'id' =>'dates'
                                ])!!}
                            </div>
                        </div>
                    </div>
            </div>
            <hr>

            @if(Auth::user()->hasPermissionTo('PERUNCIT_PR'))
                <div class="form-group row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary form-control submit">Submit</button>
                    </div>
                </div>
            @endif
        </form>

    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('.select').select2();
}) ()
</script>
@endpush