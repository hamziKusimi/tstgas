@extends('master')
@section('content')

<div class="card">
    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('stockbal.print') }}" method="GET" target="_blank">
            {{-- @csrf --}}
            <div class="form-group">
                <p><b>Filter by :</b></p>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Active_Chkbx',null,null, [ 'id' =>'Active_Chkbx','checked'=>'checked'])!!}
                    {!! Form::label('active', 'Active', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::checkbox('Inactive_Chkbx',null,null, [ 'id' =>'Inactive_Chkbx'])!!}
                    {!! Form::label('inactive', 'Inactive', ['class' => 'col-form-label']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('As at date', 'As at date', ['class' => 'col-form-label']) !!}
                </div>

                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        <div class="input-group date">
                            {!! Form::text('date', null, ['class' => 'form-control form-data',
                            'autocomplete'=>'off', 'id' =>'datepicker','readonly'=>'readonly'
                            ])!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('STK_Code_Chkbx',null,null, [ 'id' =>'STK_Code_Chkbx'])!!}
                    {!! Form::label('STK_Code', 'Stock Code ', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('STK_Code_frm', $Stockcode, null, ['class' => 'form-control select', 'id'
                        =>'STK_Code_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('STK_Code_to', $Stockcode, null, ['class' => 'form-control select',
                         'id' =>'STK_Code_to','disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Category_Chkbx',null,null, [ 'id' =>'Category_Chkbx'])!!}
                    {!! Form::label('Category', 'Category', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Category_frm', $Category, null, ['class' => 'form-control select', 'id' =>'Category_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Category_to', $Category, null, ['class' => 'form-control select', 'id' =>'Category_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Product_Chkbx',null,null, [ 'id' =>'Product_Chkbx'])!!}
                    {!! Form::label('Product', 'Product', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Product_frm', $Product, null, ['class' => 'form-control select', 'id' =>'Product_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Product_to', $Product, null, ['class' => 'form-control select', 'id' =>'Product_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Brand_Chkbx',null,null, [ 'id' =>'Brand_Chkbx'])!!}
                    {!! Form::label('Brand', 'Brand', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Brand_frm', $Brand, null, ['class' => 'form-control select', 'id' =>'Brand_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Brand_to', $Brand, null, ['class' => 'form-control select', 'id' =>'Brand_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Location_Chkbx',null,null, [ 'id' =>'Location_Chkbx'])!!}
                    {!! Form::label('Location', 'Location', ['class' => 'col-form-label']) !!}
                </div>
                <label class="col-form-label">:</label>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Location_frm', $Location, null, ['class' => 'form-control select', 'id' =>'Location_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('Location_to', $Location, null, ['class' => 'form-control select', 'id' =>'Location_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div>
            </div>
            <hr>

            <div class="form-group">
                <div class="row">
                    <div class="col-4">
                        <p><b>Report Format :</b></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::radio('stock_report','stock_bal_1',null, ['checked'=>'checked','id' =>'stock_bal_1'])!!}
                    {!! Form::label('STK_bal', 'Stock Balance 1', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::radio('stock_report','stock_bal_2',null, [ 'id' =>'stock_bal_2'])!!}
                    {!! Form::label('stock_report', 'Stock Balance 2', ['class' => 'col-form-label']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::radio('stock_report','stock_tl_1',null, [ 'id' =>'stock_tl_1'])!!}
                    {!! Form::label('stock_report', 'Stock Take List 1', ['class' => 'col-form-label',]) !!}
                </div>
                <div class="col-2">
                    {!! Form::radio('stock_report','stock_tl_2',null, [ 'id' =>'stock_tl_2'])!!}
                    {!! Form::label('stock_report', 'Stock Take List 2', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::radio('stock_report','stock_tl_3',null, [ 'id' =>'stock_tl_3'])!!}
                    {!! Form::label('stock_report', 'Stock Take List 3', ['class' => 'col-form-label']) !!}
                </div>
            </div>

            @if(Auth::user()->hasPermissionTo('STOCK_BALANCE_PR') && $print->isEmpty())
            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit">Print</button>
                </div>                
            </div>
            @elseif(Auth::user()->hasPermissionTo('STOCK_BALANCE_PRY'))
            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit">Print</button>
                </div>                
            </div>
            @endif
        </form>
    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {
    
    if($("#STK_Code_Chkbx").prop("checked") == true){
        $("#STK_Code_frm").prop('disabled', false);
        $("#STK_Code_to").prop('disabled', false);
    }else{
        $("#STK_Code_frm").prop('disabled', true);
        $("#STK_Code_to").prop('disabled', true);
    }

    if($("#Category_Chkbx").prop("checked") == true){
        $("#Category").prop('disabled', false);
    }else{
        $("#Category").prop('disabled', true);
    }

    if($("#Product_Chkbx").prop("checked") == true){
        $("#Product").prop('disabled', false);
    }else{
        $("#Product").prop('disabled', true);
    }

    if($("#Brand_Chkbx").prop("checked") == true){
        $("#Brand").prop('disabled', false);
    }else{
        $("#Brand").prop('disabled', true);
    }

    if($("#Location_Chkbx").prop("checked") == true){
        $("#Location").prop('disabled', false);
    }else{
        $("#Location").prop('disabled', true);
    }

    $("#Active_Chkbx").change(function() {
        if(this.checked) {
            $("#Inactive_Chkbx").prop('checked', false);
        }
    });

    $("#Inactive_Chkbx").change(function() {
        if(this.checked) {
            $("#Active_Chkbx").prop('checked', false);
        }
    });

    $("#STK_Code_Chkbx").change(function() {
        if(this.checked) {
            $("#STK_Code_frm").prop('disabled', false);
            $("#STK_Code_to").prop('disabled', false);
        }else{
            $("#STK_Code_frm").prop('disabled', true);
            $("#STK_Code_to").prop('disabled', true);
        }
    });

    $("#Category_Chkbx").change(function() {
        if(this.checked) {
            $("#Category_frm").prop('disabled', false);
            $("#Category_to").prop('disabled', false);
        }else{
            $("#Category_frm").prop('disabled', true);
            $("#Category_to").prop('disabled', true);
        }
    });

    $("#Product_Chkbx").change(function() {
        if(this.checked) {
            $("#Product_frm").prop('disabled', false);
            $("#Product_to").prop('disabled', false);
        }else{
            $("#Product_frm").prop('disabled', true);
            $("#Product_to").prop('disabled', true);
        }
    });

    $("#Brand_Chkbx").change(function() {
        if(this.checked) {
            $("#Brand_frm").prop('disabled', false);
            $("#Brand_to").prop('disabled', false);
        }else{
            $("#Brand_frm").prop('disabled', true);
            $("#Brand_to").prop('disabled', true);
        }
    });

    $("#Location_Chkbx").change(function() {
        if(this.checked) {
            $("#Location_frm").prop('disabled', false);
            $("#Location_to").prop('disabled', false);
        }else{
            $("#Location_frm").prop('disabled', true);
            $("#Location_to").prop('disabled', true);
        }
    });

    $("#grp_1").change(function() {
        if(this.checked) {
            $("#select_grp1").prop('disabled', false);
        }else{
            $("#select_grp1").prop('disabled', true);
        }
    });

    $("#grp_2").change(function() {
        if(this.checked) {
            $("#select_grp2").prop('disabled', false);
        }else{
            $("#select_grp2").prop('disabled', true);
        }
    });

    $("#grp_3").change(function() {
        if(this.checked) {
            $("#select_grp3").prop('disabled', false);
        }else{
            $("#select_grp3").prop('disabled', true);
        }
    });


    $('#datepicker').datepicker({
      autoclose: true,
      dateFormat: 'dd-mm-yy'
    }).datepicker("setDate", new Date());
    
  
    $('.select').select2();


}) ()
</script>
@endpush