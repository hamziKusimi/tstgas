@extends('master')
@section('content')

<div class="card">

    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('debtors.print') }}" method="GET" target="_blank" >
            {{-- @csrf --}}
            <div class="form-group">
                <p><b>Filter by :</b></p>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::checkbox('AC_Code_Chkbx',null,null, [ 'id' =>'AC_Code_Chkbx'])!!}
                    {!! Form::label('AC_Code', 'A/C Code', ['class' => 'col-form-label']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('AC_Code', 'Code :', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('AC_Code_frm', $Debtor,null, ['class' => 'form-control form-data select',
                        'id' =>'AC_Code_frm'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div> 
                <div class="col-2">
                    <div class="form-group">
                        {!! Form::select('AC_Code_to', $Debtor,null, ['class' => 'form-control form-data select', 
                        'id' =>'AC_Code_to'
                        ,'disabled' =>'disabled'])!!}
                    </div>
                </div> 
            </div>

            <hr>

            <div class="form-group">
                <p><b>Include :</b></p>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::checkbox('Address_Checkbox', null,null, [ 'id' =>'Address_Checkbox'])!!}
                    {!! Form::label('Address', 'Show Address', ['class' => 'col-form-label']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::checkbox('Tel_Fax',null,null, [ 'id' =>'Tel_Fax'] )!!}
                    {!! Form::label('Tel_Fax', 'Show Telephone & Fax', ['class' => 'col-form-label']) !!}
                </div>
            </div>
            @if(Auth::user()->hasPermissionTo('DEBTOR_LISTING_PR') && $print->isEmpty())
                <div class="form-group row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary form-control submit">Print</button>
                    </div>
                </div>
            @elseif(Auth::user()->hasPermissionTo('DEBTOR_LISTING_PRY'))
                <div class="form-group row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary form-control submit">Print</button>
                    </div>
                </div>
            @endif
        </form>

    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {
    
    if($("#AC_Code_Chkbx").prop("checked") == true){
        $("#AC_Code_frm").prop('disabled', false);
        $("#AC_Code_to").prop('disabled', false);
    }else{
        $("#AC_Code_frm").prop('disabled', true);
        $("#AC_Code_to").prop('disabled', true);
    }

    $("#AC_Code_Chkbx").change(function() {
        if(this.checked) {
            $("#AC_Code_frm").prop('disabled', false);
            $("#AC_Code_to").prop('disabled', false);
        }else{
            $("#AC_Code_frm").prop('disabled', true);
            $("#AC_Code_to").prop('disabled', true);
        }
    });

    $('.select').select2();

}) ()
</script>
@endpush