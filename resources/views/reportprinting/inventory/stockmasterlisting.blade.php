@extends('master')
@section('content')

<div class="card">

    <div class="card-header">
        <h5>Conditions</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('stock.print') }}" method="GET" target="_blank">
            {{-- @csrf --}}
            <div class="form-group">
                <p><b>Filter by :</b></p>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::checkbox('Active_Chkbx',null,null, [ 'id' =>'Active_Chkbx'])!!}
                    {!! Form::label('active', 'Active', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::checkbox('Inactive_Chkbx',null,null, [ 'id' =>'Inactive_Chkbx'])!!}
                    {!! Form::label('inactive', 'Inactive', ['class' => 'col-form-label']) !!}
                </div>
            </div>
            <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('STK_Code_Chkbx',null,null, [ 'id' =>'STK_Code_Chkbx'])!!}
                        {!! Form::label('STK_Code', 'Stock Code ', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('STK_Code_frm', $Stockcode, null, ['class' => 'form-control select', 'id'
                            =>'STK_Code_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('STK_Code_to', $Stockcode, null, ['class' => 'form-control select',
                             'id' =>'STK_Code_to','disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Category_Chkbx',null,null, [ 'id' =>'Category_Chkbx'])!!}
                        {!! Form::label('Category', 'Category', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Category_frm', $Category, null, ['class' => 'form-control select', 'id' =>'Category_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Category_to', $Category, null, ['class' => 'form-control select', 'id' =>'Category_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Product_Chkbx',null,null, [ 'id' =>'Product_Chkbx'])!!}
                        {!! Form::label('Product', 'Product', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Product_frm', $Product, null, ['class' => 'form-control select', 'id' =>'Product_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Product_to', $Product, null, ['class' => 'form-control select', 'id' =>'Product_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Brand_Chkbx',null,null, [ 'id' =>'Brand_Chkbx'])!!}
                        {!! Form::label('Brand', 'Brand', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Brand_frm', $Brand, null, ['class' => 'form-control select', 'id' =>'Brand_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Brand_to', $Brand, null, ['class' => 'form-control select', 'id' =>'Brand_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        {!! Form::checkbox('Location_Chkbx',null,null, [ 'id' =>'Location_Chkbx'])!!}
                        {!! Form::label('Location', 'Location', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label">:</label>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Location_frm', $Location, null, ['class' => 'form-control select', 'id' =>'Location_frm'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            {!! Form::select('Location_to', $Location, null, ['class' => 'form-control select', 'id' =>'Location_to'
                            ,'disabled' =>'disabled'])!!}
                        </div>
                    </div>
                </div>
            <hr>

            <div class="form-group">
                <p><b>Include :</b></p>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('show', 'Show Price:', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::select('select_price', [ 'minprice' =>'Min price',
                    'unitprice' =>'Unit Price','wsprice' =>'Ws Price','retailprice' =>'Retail Price',
                    'price1' =>'Price 1','price2' =>'Price 2','price3' =>'Price 3',]
                    ,'t', ['class' => 'form-control'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    {!! Form::label('show', 'Show Cost:', ['class' => 'col-form-label']) !!}
                </div>
                <div class="col-2">
                    {!! Form::select('select_cost', [ 'unitcost' =>'Unit Cost','avgcost' =>'Average Cost'],'t',['class'
                    => 'form-control'])!!}
                </div>
            </div>

            @if(Auth::user()->hasPermissionTo('STOCK_LISTING_PR') && $print->isEmpty())
                <div class="form-group row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary form-control submit">Print</button>
                    </div>
                </div>
            @elseif(Auth::user()->hasPermissionTo('STOCK_LISTING_PRY'))
            <div class="form-group row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary form-control submit">Print</button>
                </div>
            </div>
            @endif
        </form>

    </div>
</div>

@endsection

@push('scripts')
<script>
    (function() {
    
    if($("#STK_Code_Chkbx").prop("checked") == true){
        $("#STK_Code_frm").prop('disabled', false);
        $("#STK_Code_to").prop('disabled', false);
    }else{
        $("#STK_Code_frm").prop('disabled', true);
        $("#STK_Code_to").prop('disabled', true);
    }

    if($("#Category_Chkbx").prop("checked") == true){
        $("#Category").prop('disabled', false);
    }else{
        $("#Category").prop('disabled', true);
    }

    if($("#Product_Chkbx").prop("checked") == true){
        $("#Product").prop('disabled', false);
    }else{
        $("#Product").prop('disabled', true);
    }

    if($("#Brand_Chkbx").prop("checked") == true){
        $("#Brand").prop('disabled', false);
    }else{
        $("#Brand").prop('disabled', true);
    }

    if($("#Location_Chkbx").prop("checked") == true){
        $("#Location").prop('disabled', false);
    }else{
        $("#Location").prop('disabled', true);
    }

    $("#Active_Chkbx").change(function() {
        if(this.checked) {
            $("#Inactive_Chkbx").prop('checked', false);
        }
    });

    $("#Inactive_Chkbx").change(function() {
        if(this.checked) {
            $("#Active_Chkbx").prop('checked', false);
        }
    });

    $("#STK_Code_Chkbx").change(function() {
        if(this.checked) {
            $("#STK_Code_frm").prop('disabled', false);
            $("#STK_Code_to").prop('disabled', false);
        }else{
            $("#STK_Code_frm").prop('disabled', true);
            $("#STK_Code_to").prop('disabled', true);
        }
    });

    $("#Category_Chkbx").change(function() {
        if(this.checked) {
            $("#Category_frm").prop('disabled', false);
            $("#Category_to").prop('disabled', false);
        }else{
            $("#Category_frm").prop('disabled', true);
            $("#Category_to").prop('disabled', true);
        }
    });

    $("#Product_Chkbx").change(function() {
        if(this.checked) {
            $("#Product_frm").prop('disabled', false);
            $("#Product_to").prop('disabled', false);
        }else{
            $("#Product_frm").prop('disabled', true);
            $("#Product_to").prop('disabled', true);
        }
    });

    $("#Brand_Chkbx").change(function() {
        if(this.checked) {
            $("#Brand_frm").prop('disabled', false);
            $("#Brand_to").prop('disabled', false);
        }else{
            $("#Brand_frm").prop('disabled', true);
            $("#Brand_to").prop('disabled', true);
        }
    });

    $("#Location_Chkbx").change(function() {
        if(this.checked) {
            $("#Location_frm").prop('disabled', false);
            $("#Location_to").prop('disabled', false);
        }else{
            $("#Location_frm").prop('disabled', true);
            $("#Location_to").prop('disabled', true);
        }
    });

    $('.select').select2();

}) ()
</script>
@endpush