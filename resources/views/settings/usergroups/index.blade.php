@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('UG_SETUP_CR'))
    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('usergroups.create') }}"> Add User Group Setup</a>
        </div>
    </div>
@endif
    {{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif --}}

    
@if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>
@endif
@php ($i = 0)
    
<div class="card">
        <div class="card-body">
            <div class="bg-white">        
                <div class="table-responsive">
                    <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User Group</th>
                                <th width="280px">More</th>
                            </tr>
                        </thead>
                    @if(sizeof($usergroups) > 0)
                        <tbody>
                            @foreach ($usergroups as $usergroup)
                            <tr>
                            
                                <td>{{ ++$i }}</td>
                                @if(Auth::user()->hasPermissionTo('UG_SETUP_UP'))
                                    <td><a href="{{ route('usergroups.edit',$usergroup->id) }}">{{ $usergroup->name }}</td>
                                @else
                                <td>
                                    {{ $usergroup->name }}
                                </td>
                                @endif
                                @if(Auth::user()->hasPermissionTo('UG_SETUP_DL'))
                                <td>
                                    <a href="{{ route('usergroups.destroy', $usergroup->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>  
                                </td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    @endif
                    </table>    
                </div>
            </div>
        </div>
    </div>


    {{-- {!! $usergroups->links() !!} --}}

@endsection
{{-- @push('script') --}}
@section('js')

    <script>
    // $(system).ready(function() {
    //     $('#example').DataTable();
    // } );
    </script>
    <script>        
        $(system).ready(function() {
            $('#example').DataTable({
            });
        } );
    </script>
{{-- @endpush --}}
@endsection

