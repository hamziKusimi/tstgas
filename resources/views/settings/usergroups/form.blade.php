<div class="card">
    <div class="card-body">        
        <div class="form-group row">
            <label for="name" class="col-md-2"> Name <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input name="name" type="text" maxlength="50" class="form-control usergroup" data-values="{{ $user }}" value="{{ isset($role->name)?$role->name:'' }}" required>
            </div>
        </div>

        {{-- <div class="form-group row">
            <label for="description" class="col-md-2">Description <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input name="descr" type="text" maxlength="100" class="form-control" value="{{ isset($usergroup)?$usergroup->descr:'' }}" required>
            </div>
        </div> --}}

        {{-- <div class="form-group row">		
            <label class="control-label col-md-2">Active</label>
            <div class="col-md-10">
                <div class="radio-list">
                    <label class="radio-inline">
                    <input type="radio" name="active" value="1" {{ isset($usergroup)?($usergroup->active=='1')? 'checked' : '': 'checked' }}/> Active </font></label>
                    <label class="radio-inline">
                    <input type="radio" name="active" value="0" {{ isset($usergroup)?($usergroup->active=='0')? 'checked' : '': '' }}/> Non-Active </font></label>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<br>
<div class="card">
    <div class="card-body">  
        <label><h4>User Group Rights</h4></label>
        
        <hr/>
            {{-- <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <label class="control-label col-md-2">Inventory</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVENTORY_AS"> Access</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div> --}}
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Goods Received</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GOOD_RC_VR"  {{ (isset($role) && $role->hasPermissionTo('GOOD_RC_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GOOD_RC_CR" {{ (isset($role) && $role->hasPermissionTo('GOOD_RC_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GOOD_RC_UP" {{ (isset($role) && $role->hasPermissionTo('GOOD_RC_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GOOD_RC_DL" {{ (isset($role) && $role->hasPermissionTo('GOOD_RC_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GOOD_RC_PR" {{ (isset($role) && $role->hasPermissionTo('GOOD_RC_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GOOD_RC_RPT" {{ (isset($role) && $role->hasPermissionTo('GOOD_RC_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GOOD_RC_PRY" {{ (isset($role) && $role->hasPermissionTo('GOOD_RC_PRY'))?'checked':'' }}> Print Always</label>																		
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Purchase Orders</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_OD_VR"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_OD_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_OD_CR"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_OD_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_OD_UP"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_OD_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_OD_DL"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_OD_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_OD_PR"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_OD_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_OD_RPT"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_OD_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_OD_PRY" {{ (isset($role) && $role->hasPermissionTo('PURCHASE_OD_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Purchase Returns</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_RT_VR"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_RT_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_RT_CR"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_RT_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_RT_UP"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_RT_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_RT_DL"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_RT_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_RT_PR"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_RT_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_RT_RPT"  {{ (isset($role) && $role->hasPermissionTo('PURCHASE_RT_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PURCHASE_RT_PRY" {{ (isset($role) && $role->hasPermissionTo('PURCHASE_RT_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Cash Bill</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_BILL_VR"  {{ (isset($role) && $role->hasPermissionTo('CASH_BILL_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_BILL_CR"  {{ (isset($role) && $role->hasPermissionTo('CASH_BILL_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_BILL_UP"  {{ (isset($role) && $role->hasPermissionTo('CASH_BILL_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_BILL_DL"  {{ (isset($role) && $role->hasPermissionTo('CASH_BILL_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_BILL_PR" {{ (isset($role) && $role->hasPermissionTo('CASH_BILL_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_BILL_RPT" {{ (isset($role) && $role->hasPermissionTo('CASH_BILL_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_BILL_PRY" {{ (isset($role) && $role->hasPermissionTo('CASH_BILL_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Invoices</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVOICE_VR" {{ (isset($role) && $role->hasPermissionTo('INVOICE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVOICE_CR" {{ (isset($role) && $role->hasPermissionTo('INVOICE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVOICE_UP" {{ (isset($role) && $role->hasPermissionTo('INVOICE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVOICE_DL" {{ (isset($role) && $role->hasPermissionTo('INVOICE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVOICE_PR" {{ (isset($role) && $role->hasPermissionTo('INVOICE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVOICE_RPT" {{ (isset($role) && $role->hasPermissionTo('INVOICE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVOICE_PRY" {{ (isset($role) && $role->hasPermissionTo('INVOICE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Delivery Order</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_OD_VR" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_OD_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_OD_CR" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_OD_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_OD_UP" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_OD_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_OD_DL" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_OD_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_OD_PR" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_OD_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_OD_RPT" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_OD_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_OD_PRY" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_OD_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Quotation</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="QUOTATION_VR" {{ (isset($role) && $role->hasPermissionTo('QUOTATION_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="QUOTATION_CR" {{ (isset($role) && $role->hasPermissionTo('QUOTATION_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="QUOTATION_UP" {{ (isset($role) && $role->hasPermissionTo('QUOTATION_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="QUOTATION_DL" {{ (isset($role) && $role->hasPermissionTo('QUOTATION_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="QUOTATION_PR" {{ (isset($role) && $role->hasPermissionTo('QUOTATION_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="QUOTATION_RPT" {{ (isset($role) && $role->hasPermissionTo('QUOTATION_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="QUOTATION_PRY" {{ (isset($role) && $role->hasPermissionTo('QUOTATION_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Adjustment In</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_IN_VR" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_IN_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_IN_CR" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_IN_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_IN_UP" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_IN_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_IN_DL" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_IN_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_IN_PR" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_IN_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_IN_RPT" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_IN_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_IN_PRY" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_IN_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Adjustment Out</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_OUT_VR" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_OUT_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_OUT_CR" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_OUT_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_OUT_UP" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_OUT_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_OUT_DL" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_OUT_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_OUT_PR" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_OUT_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_OUT_RPT" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_OUT_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ADJUSTMENT_OUT_PRY" {{ (isset($role) && $role->hasPermissionTo('ADJUSTMENT_OUT_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group-prepend">
                            <label class="control-label col-md-2">Sales Return</label>
                                <div class="col-md-10">
                                    <div style="margin-top:7px;">
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_RETURN_VR" {{ (isset($role) && $role->hasPermissionTo('SALES_RETURN_VR'))?'checked':'' }}> View</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_RETURN_CR" {{ (isset($role) && $role->hasPermissionTo('SALES_RETURN_CR'))?'checked':'' }}> Create</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_RETURN_UP" {{ (isset($role) && $role->hasPermissionTo('SALES_RETURN_UP'))?'checked':'' }}> Update</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_RETURN_DL" {{ (isset($role) && $role->hasPermissionTo('SALES_RETURN_DL'))?'checked':'' }}> Delete</label>																																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_RETURN_PR" {{ (isset($role) && $role->hasPermissionTo('SALES_RETURN_PR'))?'checked':'' }}> Print</label>		
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_RETURN_RPT" {{ (isset($role) && $role->hasPermissionTo('SALES_RETURN_RPT'))?'checked':'' }}> Report</label>
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_RETURN_PRY" {{ (isset($role) && $role->hasPermissionTo('SALES_RETURN_PRY'))?'checked':'' }}> Print Always</label>																			
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">									
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group-prepend">
                                <label class="control-label col-md-2">Sales Order</label>
                                    <div class="col-md-10">
                                        <div style="margin-top:7px;">
                                            <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_OD_VR" {{ (isset($role) && $role->hasPermissionTo('SALES_OD_VR'))?'checked':'' }}> View</label>																	
                                            <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_OD_CR" {{ (isset($role) && $role->hasPermissionTo('SALES_OD_CR'))?'checked':'' }}> Create</label>																	
                                            <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_OD_UP" {{ (isset($role) && $role->hasPermissionTo('SALES_OD_UP'))?'checked':'' }}> Update</label>																	
                                            <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_OD_DL" {{ (isset($role) && $role->hasPermissionTo('SALES_OD_DL'))?'checked':'' }}> Delete</label>																																	
                                            <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_OD_PR" {{ (isset($role) && $role->hasPermissionTo('SALES_OD_PR'))?'checked':'' }}> Print</label>		
                                            <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_OD_RPT" {{ (isset($role) && $role->hasPermissionTo('SALES_OD_RPT'))?'checked':'' }}> Report</label>
                                            <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SALES_OD_PRY" {{ (isset($role) && $role->hasPermissionTo('SALES_OD_PRY'))?'checked':'' }}> Print Always</label>																			
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">									
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group-prepend">
                                    <label class="control-label col-md-2">Cash Sales</label>
                                        <div class="col-md-10">
                                            <div style="margin-top:7px;">
                                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_SALES_VR" {{ (isset($role) && $role->hasPermissionTo('CASH_SALES_VR'))?'checked':'' }}> View</label>																	
                                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_SALES_CR" {{ (isset($role) && $role->hasPermissionTo('CASH_SALES_CR'))?'checked':'' }}> Create</label>																	
                                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_SALES_UP" {{ (isset($role) && $role->hasPermissionTo('CASH_SALES_UP'))?'checked':'' }}> Update</label>																	
                                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_SALES_DL" {{ (isset($role) && $role->hasPermissionTo('CASH_SALES_DL'))?'checked':'' }}> Delete</label>																																	
                                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_SALES_PR" {{ (isset($role) && $role->hasPermissionTo('CASH_SALES_PR'))?'checked':'' }}> Print</label>		
                                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_SALES_RPT" {{ (isset($role) && $role->hasPermissionTo('CASH_SALES_RPT'))?'checked':'' }}> Report</label>
                                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CASH_SALES_PRY" {{ (isset($role) && $role->hasPermissionTo('CASH_SALES_PRY'))?'checked':'' }}> Print Always</label>																			
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            <hr/>
            @if(config('config.cylinder.cylinder') == "true")
                <div class="row">									
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group-prepend">
                                <label class="control-label col-md-2">Cylinder</label>
                                <div class="col-md-10">
                                    <div style="margin-top:7px;">
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CYLINDER_AS"> Access</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <div class="row">									
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group-prepend">
                            <label class="control-label col-md-2">Delivery Note</label>
                                <div class="col-md-10">
                                    <div style="margin-top:7px;">
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_NT_VR" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_NT_VR'))?'checked':'' }}> View</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_NT_CR" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_NT_CR'))?'checked':'' }}> Create</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_NT_UP" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_NT_UP'))?'checked':'' }}> Update</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_NT_DL" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_NT_DL'))?'checked':'' }}> Delete</label>																																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_NT_PR" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_NT_PR'))?'checked':'' }}> Print</label>		
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_NT_RPT" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_NT_RPT'))?'checked':'' }}> Report</label>
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DELIVERY_NT_PRY" {{ (isset($role) && $role->hasPermissionTo('DELIVERY_NT_PRY'))?'checked':'' }}> Print Always</label>																			
                                    </div>            
                                </div>	
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <div class="row">									
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group-prepend">
                            <label class="control-label col-md-2">Return Note</label>
                                <div class="col-md-10">
                                    <div style="margin-top:7px;">
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="RETURN_NT_VR" {{ (isset($role) && $role->hasPermissionTo('RETURN_NT_VR'))?'checked':'' }}> View</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="RETURN_NT_CR" {{ (isset($role) && $role->hasPermissionTo('RETURN_NT_CR'))?'checked':'' }}> Create</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="RETURN_NT_UP" {{ (isset($role) && $role->hasPermissionTo('RETURN_NT_UP'))?'checked':'' }}> Update</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="RETURN_NT_DL" {{ (isset($role) && $role->hasPermissionTo('RETURN_NT_DL'))?'checked':'' }}> Delete</label>																																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="RETURN_NT_PR" {{ (isset($role) && $role->hasPermissionTo('RETURN_NT_PR'))?'checked':'' }}> Print</label>		
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="RETURN_NT_RPT" {{ (isset($role) && $role->hasPermissionTo('RETURN_NT_RPT'))?'checked':'' }}> Report</label>
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="RETURN_NT_PRY" {{ (isset($role) && $role->hasPermissionTo('RETURN_NT_PRY'))?'checked':'' }}> Print Always</label>																			
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <div class="row">									
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group-prepend">
                            <label class="control-label col-md-2">Refill</label>
                                <div class="col-md-10">
                                    <div style="margin-top:7px;">
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REFILL_VR" {{ (isset($role) && $role->hasPermissionTo('REFILL_VR'))?'checked':'' }}> View</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REFILL_CR" {{ (isset($role) && $role->hasPermissionTo('REFILL_CR'))?'checked':'' }}> Create</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REFILL_UP" {{ (isset($role) && $role->hasPermissionTo('REFILL_UP'))?'checked':'' }}> Update</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REFILL_DL" {{ (isset($role) && $role->hasPermissionTo('REFILL_DL'))?'checked':'' }}> Delete</label>																																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REFILL_PR" {{ (isset($role) && $role->hasPermissionTo('REFILL_PR'))?'checked':'' }}> Print</label>		
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REFILL_RPT" {{ (isset($role) && $role->hasPermissionTo('REFILL_RPT'))?'checked':'' }}> Report</label>
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REFILL_PRY" {{ (isset($role) && $role->hasPermissionTo('REFILL_PRY'))?'checked':'' }}> Print Always</label>																			
                                    </div>
                                </div>	
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <div class="row">									
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group-prepend">
                            <label class="control-label col-md-2">Repair</label>
                                <div class="col-md-10">
                                    <div style="margin-top:7px;">
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REPAIR_VR" {{ (isset($role) && $role->hasPermissionTo('REPAIR_VR'))?'checked':'' }}> View</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REPAIR_CR" {{ (isset($role) && $role->hasPermissionTo('REPAIR_CR'))?'checked':'' }}> Create</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REPAIR_UP" {{ (isset($role) && $role->hasPermissionTo('REPAIR_UP'))?'checked':'' }}> Update</label>																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REPAIR_DL" {{ (isset($role) && $role->hasPermissionTo('REPAIR_DL'))?'checked':'' }}> Delete</label>																																	
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REPAIR_PR" {{ (isset($role) && $role->hasPermissionTo('REPAIR_PR'))?'checked':'' }}> Print</label>		
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REPAIR_RPT" {{ (isset($role) && $role->hasPermissionTo('REPAIR_RPT'))?'checked':'' }}> Report</label>
                                        <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="REPAIR_PRY" {{ (isset($role) && $role->hasPermissionTo('REPAIR_PRY'))?'checked':'' }}> Print Always</label>																			
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
            <hr/>
            @endif
            {{-- <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <label class="control-label col-md-2">Inventory Module</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVENTORY_MD_AS"> Access</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div> --}}
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_STOCK_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_STOCK_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_STOCK_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_STOCK_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_STOCK_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_STOCK_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_STOCK_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_STOCK_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_STOCK_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_STOCK_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_STOCK_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_STOCK_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_STOCK_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_STOCK_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Category</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CATEGORY_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CATEGORY_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CATEGORY_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CATEGORY_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CATEGORY_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CATEGORY_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CATEGORY_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CATEGORY_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CATEGORY_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CATEGORY_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CATEGORY_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CATEGORY_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CATEGORY_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CATEGORY_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Product</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_PRODUCT_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_PRODUCT_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_PRODUCT_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_PRODUCT_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_PRODUCT_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_PRODUCT_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_PRODUCT_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_PRODUCT_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_PRODUCT_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_PRODUCT_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_PRODUCT_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_PRODUCT_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_PRODUCT_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_PRODUCT_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Brand</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_BRAND_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_BRAND_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_BRAND_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_BRAND_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_BRAND_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_BRAND_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_BRAND_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_BRAND_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_BRAND_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_BRAND_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_BRAND_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_BRAND_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_BRAND_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_BRAND_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Location</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_LOCATION_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_LOCATION_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_LOCATION_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_LOCATION_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_LOCATION_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_LOCATION_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_LOCATION_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_LOCATION_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_LOCATION_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_LOCATION_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_LOCATION_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_LOCATION_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_LOCATION_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_LOCATION_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">UOM</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_UOM_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_UOM_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_UOM_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_UOM_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_UOM_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_UOM_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_UOM_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_UOM_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_UOM_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_UOM_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_UOM_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_UOM_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_UOM_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_UOM_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Creditor</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CREDITOR_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CREDITOR_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CREDITOR_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CREDITOR_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CREDITOR_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CREDITOR_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CREDITOR_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CREDITOR_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CREDITOR_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CREDITOR_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CREDITOR_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CREDITOR_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CREDITOR_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CREDITOR_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Debtor</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_DEBTOR_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_DEBTOR_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_DEBTOR_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_DEBTOR_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_DEBTOR_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_DEBTOR_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_DEBTOR_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_DEBTOR_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_DEBTOR_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_DEBTOR_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_DEBTOR_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_DEBTOR_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_DEBTOR_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_DEBTOR_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Adjustment In Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_I_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_I_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_I_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_I_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_I_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_I_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_I_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_I_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_I_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_I_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_I_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_I_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_I_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_I_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Adjustment Out Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_O_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_O_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_O_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_O_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_O_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_O_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_O_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_O_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_O_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_O_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_O_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_O_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_ADJUSTMENT_O_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_ADJUSTMENT_O_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Reason</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_REASON_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_REASON_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_REASON_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_REASON_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_REASON_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_REASON_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_REASON_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_REASON_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_REASON_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_REASON_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_REASON_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_REASON_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_REASON_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_REASON_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Area</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_AREA_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_AREA_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_AREA_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_AREA_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_AREA_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_AREA_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_AREA_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_AREA_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_AREA_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_AREA_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_AREA_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_AREA_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_AREA_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_AREA_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Currency</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CURRENCY_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CURRENCY_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CURRENCY_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CURRENCY_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CURRENCY_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CURRENCY_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CURRENCY_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CURRENCY_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CURRENCY_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CURRENCY_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CURRENCY_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CURRENCY_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CURRENCY_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CURRENCY_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Customer</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CUSTOMER_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CUSTOMER_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CUSTOMER_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CUSTOMER_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CUSTOMER_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CUSTOMER_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CUSTOMER_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CUSTOMER_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CUSTOMER_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CUSTOMER_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CUSTOMER_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CUSTOMER_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_CUSTOMER_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_CUSTOMER_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Salesman</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_SALESMAN_VR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_SALESMAN_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_SALESMAN_CR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_SALESMAN_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_SALESMAN_UP" {{ (isset($role) && $role->hasPermissionTo('IV_MD_SALESMAN_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_SALESMAN_DL" {{ (isset($role) && $role->hasPermissionTo('IV_MD_SALESMAN_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_SALESMAN_PR" {{ (isset($role) && $role->hasPermissionTo('IV_MD_SALESMAN_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_SALESMAN_RPT" {{ (isset($role) && $role->hasPermissionTo('IV_MD_SALESMAN_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="IV_MD_SALESMAN_PRY" {{ (isset($role) && $role->hasPermissionTo('IV_MD_SALESMAN_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            
            @if(config('config.cylinder.cylinder') == "true")
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <label class="control-label col-md-2">Cylinder Module</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CYLINDER_MD_AS"> Access</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Cylinder Category</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_CATEGORY_VR" {{ (isset($role) && $role->hasPermissionTo('CY_CATEGORY_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_CATEGORY_CR" {{ (isset($role) && $role->hasPermissionTo('CY_CATEGORY_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_CATEGORY_UP" {{ (isset($role) && $role->hasPermissionTo('CY_CATEGORY_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_CATEGORY_DL" {{ (isset($role) && $role->hasPermissionTo('CY_CATEGORY_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_CATEGORY_PR" {{ (isset($role) && $role->hasPermissionTo('CY_CATEGORY_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_CATEGORY_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_CATEGORY_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_CATEGORY_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_CATEGORY_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Cylinder Group</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_GROUP_VR" {{ (isset($role) && $role->hasPermissionTo('CY_GROUP_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_GROUP_CR" {{ (isset($role) && $role->hasPermissionTo('CY_GROUP_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_GROUP_UP" {{ (isset($role) && $role->hasPermissionTo('CY_GROUP_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_GROUP_DL" {{ (isset($role) && $role->hasPermissionTo('CY_GROUP_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_GROUP_PR" {{ (isset($role) && $role->hasPermissionTo('CY_GROUP_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_GROUP_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_GROUP_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_GROUP_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_GROUP_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Cylinder Product</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_PRODUCT_VR" {{ (isset($role) && $role->hasPermissionTo('CY_PRODUCT_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_PRODUCT_CR" {{ (isset($role) && $role->hasPermissionTo('CY_PRODUCT_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_PRODUCT_UP" {{ (isset($role) && $role->hasPermissionTo('CY_PRODUCT_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_PRODUCT_DL" {{ (isset($role) && $role->hasPermissionTo('CY_PRODUCT_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_PRODUCT_PR" {{ (isset($role) && $role->hasPermissionTo('CY_PRODUCT_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_PRODUCT_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_PRODUCT_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_PRODUCT_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_PRODUCT_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Cylinder Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_TYPE_VR" {{ (isset($role) && $role->hasPermissionTo('CY_TYPE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_TYPE_CR" {{ (isset($role) && $role->hasPermissionTo('CY_TYPE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_TYPE_UP" {{ (isset($role) && $role->hasPermissionTo('CY_TYPE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_TYPE_DL" {{ (isset($role) && $role->hasPermissionTo('CY_TYPE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_TYPE_PR" {{ (isset($role) && $role->hasPermissionTo('CY_TYPE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_TYPE_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_TYPE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_TYPE_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_TYPE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Cylinder Handwheel Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_HANDWHEEL_VR" {{ (isset($role) && $role->hasPermissionTo('CY_HANDWHEEL_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_HANDWHEEL_CR" {{ (isset($role) && $role->hasPermissionTo('CY_HANDWHEEL_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_HANDWHEEL_UP" {{ (isset($role) && $role->hasPermissionTo('CY_HANDWHEEL_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_HANDWHEEL_DL" {{ (isset($role) && $role->hasPermissionTo('CY_HANDWHEEL_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_HANDWHEEL_PR" {{ (isset($role) && $role->hasPermissionTo('CY_HANDWHEEL_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_HANDWHEEL_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_HANDWHEEL_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_HANDWHEEL_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_HANDWHEEL_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Cylinder Valve Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_VALVE_VR" {{ (isset($role) && $role->hasPermissionTo('CY_VALVE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_VALVE_CR" {{ (isset($role) && $role->hasPermissionTo('CY_VALVE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_VALVE_UP" {{ (isset($role) && $role->hasPermissionTo('CY_VALVE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_VALVE_DL" {{ (isset($role) && $role->hasPermissionTo('CY_VALVE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_VALVE_PR" {{ (isset($role) && $role->hasPermissionTo('CY_VALVE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_VALVE_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_VALVE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_VALVE_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_VALVE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Manufacturer</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_VR" {{ (isset($role) && $role->hasPermissionTo('CY_MANUFACTURER_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_CR" {{ (isset($role) && $role->hasPermissionTo('CY_MANUFACTURER_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_UP" {{ (isset($role) && $role->hasPermissionTo('CY_MANUFACTURER_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_DL" {{ (isset($role) && $role->hasPermissionTo('CY_MANUFACTURER_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_PR" {{ (isset($role) && $role->hasPermissionTo('CY_MANUFACTURER_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_MANUFACTURER_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_MANUFACTURER_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Ownership</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_VR" {{ (isset($role) && $role->hasPermissionTo('CY_OWNERSHIP_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_CR" {{ (isset($role) && $role->hasPermissionTo('CY_OWNERSHIP_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_UP" {{ (isset($role) && $role->hasPermissionTo('CY_OWNERSHIP_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_DL" {{ (isset($role) && $role->hasPermissionTo('CY_OWNERSHIP_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_PR" {{ (isset($role) && $role->hasPermissionTo('CY_OWNERSHIP_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_RPT" {{ (isset($role) && $role->hasPermissionTo('CY_OWNERSHIP_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_PRY" {{ (isset($role) && $role->hasPermissionTo('CY_OWNERSHIP_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Gas Rack</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_VR" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_CR" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_UP" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_DL" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_PR" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_RPT" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_PRY" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Gas Rack Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_VR" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_TYPE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_CR" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_TYPE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_UP" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_TYPE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_DL" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_TYPE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_PR" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_TYPE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_RPT" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_TYPE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_PRY" {{ (isset($role) && $role->hasPermissionTo('GS_RACK_TYPE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Sling</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_VR" {{ (isset($role) && $role->hasPermissionTo('SLING_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_CR" {{ (isset($role) && $role->hasPermissionTo('SLING_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_UP" {{ (isset($role) && $role->hasPermissionTo('SLING_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_DL" {{ (isset($role) && $role->hasPermissionTo('SLING_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_PR" {{ (isset($role) && $role->hasPermissionTo('SLING_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_RPT" {{ (isset($role) && $role->hasPermissionTo('SLING_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_PRY" {{ (isset($role) && $role->hasPermissionTo('SLING_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Sling Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_VR" {{ (isset($role) && $role->hasPermissionTo('SLING_TYPE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_CR" {{ (isset($role) && $role->hasPermissionTo('SLING_TYPE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_UP" {{ (isset($role) && $role->hasPermissionTo('SLING_TYPE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_DL" {{ (isset($role) && $role->hasPermissionTo('SLING_TYPE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_PR" {{ (isset($role) && $role->hasPermissionTo('SLING_TYPE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_RPT" {{ (isset($role) && $role->hasPermissionTo('SLING_TYPE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_PRY" {{ (isset($role) && $role->hasPermissionTo('SLING_TYPE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Shackle</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_VR" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_CR" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_UP" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_DL" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_PR" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_RPT" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_PRY" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Shackle Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_VR" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_TYPE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_CR" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_TYPE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_UP" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_TYPE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_DL" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_TYPE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_PR" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_TYPE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_RPT" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_TYPE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_PRY" {{ (isset($role) && $role->hasPermissionTo('SHACKLE_TYPE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <hr/>
        @endif
        {{-- <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <label class="control-label col-md-2">Inventory Report</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVENTORY_RPT_AS"> Access</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div> --}}
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Debtor Master Listing</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DEBTOR_LISTING_VR" {{ (isset($role) && $role->hasPermissionTo('DEBTOR_LISTING_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DEBTOR_LISTING_CR" {{ (isset($role) && $role->hasPermissionTo('DEBTOR_LISTING_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DEBTOR_LISTING_UP" {{ (isset($role) && $role->hasPermissionTo('DEBTOR_LISTING_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DEBTOR_LISTING_DL" {{ (isset($role) && $role->hasPermissionTo('DEBTOR_LISTING_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DEBTOR_LISTING_PR" {{ (isset($role) && $role->hasPermissionTo('DEBTOR_LISTING_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DEBTOR_LISTING_RPT" {{ (isset($role) && $role->hasPermissionTo('DEBTOR_LISTING_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DEBTOR_LISTING_PRY" {{ (isset($role) && $role->hasPermissionTo('DEBTOR_LISTING_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Creditor Master Listing</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CREDITOR_LISTING_VR" {{ (isset($role) && $role->hasPermissionTo('CREDITOR_LISTING_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CREDITOR_LISTING_CR" {{ (isset($role) && $role->hasPermissionTo('CREDITOR_LISTING_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CREDITOR_LISTING_UP" {{ (isset($role) && $role->hasPermissionTo('CREDITOR_LISTING_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CREDITOR_LISTING_DL" {{ (isset($role) && $role->hasPermissionTo('CREDITOR_LISTING_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CREDITOR_LISTING_PR" {{ (isset($role) && $role->hasPermissionTo('CREDITOR_LISTING_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CREDITOR_LISTING_RPT" {{ (isset($role) && $role->hasPermissionTo('CREDITOR_LISTING_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CREDITOR_LISTING_PRY" {{ (isset($role) && $role->hasPermissionTo('CREDITOR_LISTING_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Master Listing</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LISTING_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_LISTING_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LISTING_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_LISTING_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LISTING_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_LISTING_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LISTING_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_LISTING_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LISTING_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_LISTING_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LISTING_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_LISTING_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LISTING_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_LISTING_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Balance</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_BALANCE_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_BALANCE_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_BALANCE_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_BALANCE_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_BALANCE_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_BALANCE_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_BALANCE_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_BALANCE_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_BALANCE_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_BALANCE_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_BALANCE_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_BALANCE_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_BALANCE_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_BALANCE_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Ledger</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LEDGER_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_LEDGER_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LEDGER_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_LEDGER_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LEDGER_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_LEDGER_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LEDGER_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_LEDGER_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LEDGER_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_LEDGER_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LEDGER_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_LEDGER_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_LEDGER_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_LEDGER_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Value (FIFO)</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_FIFO_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_FIFO_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_FIFO_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_FIFO_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_FIFO_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_FIFO_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_FIFO_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_FIFO_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_FIFO_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_FIFO_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_FIFO_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_FIFO_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_FIFO_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_FIFO_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Value (As At)</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_ASAT_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_ASAT_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_ASAT_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_ASAT_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_ASAT_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_ASAT_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_ASAT_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_ASAT_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_ASAT_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_ASAT_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_ASAT_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_ASAT_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_VAL_ASAT_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_VAL_ASAT_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Movement</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_MOVEMENT_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_MOVEMENT_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_MOVEMENT_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_MOVEMENT_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_MOVEMENT_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_MOVEMENT_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_MOVEMENT_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_MOVEMENT_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_MOVEMENT_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_MOVEMENT_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_MOVEMENT_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_MOVEMENT_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_MOVEMENT_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_MOVEMENT_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Pembekal</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PEMBEKAL_VR" {{ (isset($role) && $role->hasPermissionTo('PEMBEKAL_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PEMBEKAL_CR" {{ (isset($role) && $role->hasPermissionTo('PEMBEKAL_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PEMBEKAL_UP" {{ (isset($role) && $role->hasPermissionTo('PEMBEKAL_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PEMBEKAL_DL" {{ (isset($role) && $role->hasPermissionTo('PEMBEKAL_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PEMBEKAL_PR" {{ (isset($role) && $role->hasPermissionTo('PEMBEKAL_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PEMBEKAL_RPT" {{ (isset($role) && $role->hasPermissionTo('PEMBEKAL_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PEMBEKAL_PRY" {{ (isset($role) && $role->hasPermissionTo('PEMBEKAL_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Peruncit</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PERUNCIT_VR" {{ (isset($role) && $role->hasPermissionTo('PERUNCIT_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PERUNCIT_CR" {{ (isset($role) && $role->hasPermissionTo('PERUNCIT_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PERUNCIT_UP" {{ (isset($role) && $role->hasPermissionTo('PERUNCIT_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PERUNCIT_DL" {{ (isset($role) && $role->hasPermissionTo('PERUNCIT_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PERUNCIT_PR" {{ (isset($role) && $role->hasPermissionTo('PERUNCIT_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PERUNCIT_RPT" {{ (isset($role) && $role->hasPermissionTo('PERUNCIT_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PERUNCIT_PRY" {{ (isset($role) && $role->hasPermissionTo('PERUNCIT_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Processing</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PROCESSING_VR" {{ (isset($role) && $role->hasPermissionTo('PROCESSING_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PROCESSING_CR" {{ (isset($role) && $role->hasPermissionTo('PROCESSING_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PROCESSING_UP" {{ (isset($role) && $role->hasPermissionTo('PROCESSING_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PROCESSING_DL" {{ (isset($role) && $role->hasPermissionTo('PROCESSING_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PROCESSING_PR" {{ (isset($role) && $role->hasPermissionTo('PROCESSING_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PROCESSING_RPT" {{ (isset($role) && $role->hasPermissionTo('PROCESSING_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="PROCESSING_PRY" {{ (isset($role) && $role->hasPermissionTo('PROCESSING_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Customer Enq</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_CUST_ENQ_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_CUST_ENQ_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_CUST_ENQ_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_CUST_ENQ_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_CUST_ENQ_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_CUST_ENQ_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_CUST_ENQ_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_CUST_ENQ_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_CUST_ENQ_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_CUST_ENQ_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_CUST_ENQ_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_CUST_ENQ_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_CUST_ENQ_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_CUST_ENQ_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Stock Supplier Enq</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_SUPP_ENQ_VR" {{ (isset($role) && $role->hasPermissionTo('STOCK_SUPP_ENQ_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_SUPP_ENQ_CR" {{ (isset($role) && $role->hasPermissionTo('STOCK_SUPP_ENQ_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_SUPP_ENQ_UP" {{ (isset($role) && $role->hasPermissionTo('STOCK_SUPP_ENQ_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_SUPP_ENQ_DL" {{ (isset($role) && $role->hasPermissionTo('STOCK_SUPP_ENQ_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_SUPP_ENQ_PR" {{ (isset($role) && $role->hasPermissionTo('STOCK_SUPP_ENQ_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_SUPP_ENQ_RPT" {{ (isset($role) && $role->hasPermissionTo('STOCK_SUPP_ENQ_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="STOCK_SUPP_ENQ_PRY" {{ (isset($role) && $role->hasPermissionTo('STOCK_SUPP_ENQ_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <hr>
        {{-- <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <label class="control-label col-md-2">Setup</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="INVENTORY_RPT_AS"> Access</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div> --}}
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">System Setup</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SYSTEM_SETUP_VR" {{ (isset($role) && $role->hasPermissionTo('SYSTEM_SETUP_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SYSTEM_SETUP_CR" {{ (isset($role) && $role->hasPermissionTo('SYSTEM_SETUP_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SYSTEM_SETUP_UP" {{ (isset($role) && $role->hasPermissionTo('SYSTEM_SETUP_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SYSTEM_SETUP_DL" {{ (isset($role) && $role->hasPermissionTo('SYSTEM_SETUP_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SYSTEM_SETUP_PR" {{ (isset($role) && $role->hasPermissionTo('SYSTEM_SETUP_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SYSTEM_SETUP_RPT" {{ (isset($role) && $role->hasPermissionTo('SYSTEM_SETUP_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SYSTEM_SETUP_PRY" {{ (isset($role) && $role->hasPermissionTo('SYSTEM_SETUP_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Document Setup</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DOC_SETUP_VR" {{ (isset($role) && $role->hasPermissionTo('DOC_SETUP_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DOC_SETUP_CR" {{ (isset($role) && $role->hasPermissionTo('DOC_SETUP_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DOC_SETUP_UP" {{ (isset($role) && $role->hasPermissionTo('DOC_SETUP_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DOC_SETUP_DL" {{ (isset($role) && $role->hasPermissionTo('DOC_SETUP_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DOC_SETUP_PR" {{ (isset($role) && $role->hasPermissionTo('DOC_SETUP_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DOC_SETUP_RPT" {{ (isset($role) && $role->hasPermissionTo('DOC_SETUP_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="DOC_SETUP_PRY" {{ (isset($role) && $role->hasPermissionTo('DOC_SETUP_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">User Setup</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="USER_SETUP_VR" {{ (isset($role) && $role->hasPermissionTo('USER_SETUP_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="USER_SETUP_CR" {{ (isset($role) && $role->hasPermissionTo('USER_SETUP_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="USER_SETUP_UP" {{ (isset($role) && $role->hasPermissionTo('USER_SETUP_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="USER_SETUP_DL" {{ (isset($role) && $role->hasPermissionTo('USER_SETUP_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="USER_SETUP_PR" {{ (isset($role) && $role->hasPermissionTo('USER_SETUP_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="USER_SETUP_RPT" {{ (isset($role) && $role->hasPermissionTo('USER_SETUP_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="USER_SETUP_PRY" {{ (isset($role) && $role->hasPermissionTo('USER_SETUP_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">User Group Setup</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="UG_SETUP_VR" {{ (isset($role) && $role->hasPermissionTo('UG_SETUP_VR'))?'checked':'' }}> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="UG_SETUP_CR" {{ (isset($role) && $role->hasPermissionTo('UG_SETUP_CR'))?'checked':'' }}> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="UG_SETUP_UP" {{ (isset($role) && $role->hasPermissionTo('UG_SETUP_UP'))?'checked':'' }}> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="UG_SETUP_DL" {{ (isset($role) && $role->hasPermissionTo('UG_SETUP_DL'))?'checked':'' }}> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="UG_SETUP_PR" {{ (isset($role) && $role->hasPermissionTo('UG_SETUP_PR'))?'checked':'' }}> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="UG_SETUP_RPT" {{ (isset($role) && $role->hasPermissionTo('UG_SETUP_RPT'))?'checked':'' }}> Report</label>
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="UG_SETUP_PRY" {{ (isset($role) && $role->hasPermissionTo('UG_SETUP_PRY'))?'checked':'' }}> Print Always</label>																			
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
        <hr>
        <div class="row">									
            <div class="col-md-12">
                <div class="form-group">
                    <div class="input-group-prepend">
                        <label class="control-label col-md-2">Modify After Print</label>
                        <div class="col-md-10">
                            <div style="margin-top:7px;">
                                <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="MOD_AFT_PR" {{ (isset($role) && $role->hasPermissionTo('MOD_AFT_PR'))?'checked':'' }}>Active</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <label class="control-label col-md-2">Below Minimum Price</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="ACC_MIN_PRICE" {{ (isset($role) && $role->hasPermissionTo('ACC_MIN_PRICE'))?'checked':'' }}>Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
        <div class="input-group row narrow-padding-global">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-form-submit">Submit</button>
            </div>
        </div>
    </div>
</div>