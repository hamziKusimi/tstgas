<div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <label class="control-label col-md-2">Cylinder Module</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CYLINDER_AS"> Access</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Gas Rack</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_RPT"> Report</label>																		
                                </div>            
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Gas Rack Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="GS_RACK_TYPE_RPT"> Report</label>																		
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Sling</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_RPT"> Report</label>																		
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Sling Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SLING_TYPE_RPT" > Report</label>																		
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Shackle</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_RPT"> Report</label>																		
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Shackle Type</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="SHACKLE_TYPE_RPT"> Report</label>																		
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Manufacturer</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_MANUFACTURER_RPT"> Report</label>																		
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">									
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                        <label class="control-label col-md-2">Ownership</label>
                            <div class="col-md-10">
                                <div style="margin-top:7px;">
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_VW"> View</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_CR"> Create</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_UP"> Update</label>																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_DL"> Delete</label>																																	
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_PR"> Print</label>		
                                    <label style="display:inline;"><input type="checkbox" name="ugRights[]" value="CY_OWNERSHIP_RPT"> Report</label>																		
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>