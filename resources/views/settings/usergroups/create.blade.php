@extends('master')
@section('content')


<div class="alert red-alert text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist"></p>
</div>

<div class="alert red-alert2 text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist2"></p>
</div>

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('usergroups.store') }}" method="POST">
            @csrf
            @include('settings/usergroups/form')

        </form>
    </div>
</div>

{{-- <table>
    @foreach ($collection as $item)
    <tr>
        <td>{{ $activity->changes()['attributes']['usergroup'] }}</td>
        <td>{{ $activity->changes()['attributes']['action'] }}</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    @endforeach
</table> --}}
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function() {
            let originalVal = $('.usergroup').val()
            originalVal = originalVal.trim()
        $('.usergroup').on('blur', function() {
            let contents = $(this).data('values')
            let usergroup = $(this).val()  
            usergroup = usergroup.trim()
            let selectedUsergroup = contents.filter(content => {
                return (content.trim().toLowerCase() == usergroup.toLowerCase() && content.trim().toLowerCase() != originalVal.toLowerCase())
            }) [0]

            console.log(selectedUsergroup)
            if (selectedUsergroup) {
                if(selectedUsergroup.toLowerCase() == usergroup.toLowerCase()){
                    $('.alert-exist').text('User Group Exist! Create a User Group')
                    $(".red-alert").removeClass('d-none')
                    $('.btn-form-submit').prop('disabled', true)
                }else{                    
                    $(".red-alert").addClass('d-none')
                    $('.btn-form-submit').prop('disabled', false)
                }
            } else {
                    console.log('error')
                    $(".red-alert").addClass('d-none')
                    $('.btn-form-submit').prop('disabled', false)
            }
            
        })
        
        });
    </script>
@endpush

