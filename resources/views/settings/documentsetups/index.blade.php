@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('DOC_SETUP_UP'))
    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('documentsetups.create') }}"> Edit Document Setup</a>
        </div>
    </div>
@endif

    {{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif --}}

    
@if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>
@endif
@php ($i = 0)
    
<div class="card">
        <div class="card-body">
            <div class="bg-white">        
                <div class="table-responsive">
                    <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Prefix</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                    @if(sizeof($documentSetups) > 0)
                        <tbody>
                            @foreach ($documentSetups as $documentSetup)
                            <tr>
                            
                                <td>{{ ++$i }}</td>
                                <td>{{ $documentSetup->D_NAME }}</td>
                                <td>{{ $documentSetup->D_PREFIX}}</td>
                                <td>{{ $documentSetup->D_ACTIVE }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    @endif
                    </table>
                </div>
            </div>
        </div>
    </div>


    {{-- {!! $documentSetups->links() !!} --}}

@endsection
{{-- @push('script') --}}
@section('js')

    <script>
    // $(document).ready(function() {
    //     $('#example').DataTable();
    // } );
    </script>
    <script>        
        $(document).ready(function() {
            $('#example').DataTable({
            });
        } );
    </script>
{{-- @endpush --}}
@endsection

