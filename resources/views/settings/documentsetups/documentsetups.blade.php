<div class="card">
    <div class="card-header bg-primary text-white">
        Running Numbers Setup
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10 row">
                <div class="col-md-2"><span class="pl-3">Prefix</span></div>
                <div class="col-md-1"></div>
                <div class="col-md-2"><span class="pl-3">Zeroes</span></div>
                <div class="col-md-2"><span class="pl-3">Active</span></div>
                <div class="col-md-3"><span class="pl-3">Format</span></div>
                <div class="col-md-2"><span class="pl-3">Last No.</span></div>
            </div>
        </div>
        <div class="pt-3"></div>
        @foreach ($documentSetups as $runningNumber)
            <div class="form-group row">
                {!! Form::label('label', $runningNumber->D_NAME, ['class' => 'col-md-2 col-form-label']) !!}
                <div class="col-md-10">
                    <div class="row">
                        {!! Form::hidden('RN_ID[]', $runningNumber->ID) !!}
                        <div class="col-md-2">
                            {!! Form::text('RN_D_PREFIX[]', $runningNumber->D_PREFIX, ['class' => 'form-control prefix running-number-fields']) !!}
                        </div>
                        <div class="col-md-1">
                            {!! Form::text('RN_D_SEPARATOR[]', $runningNumber->D_SEPARATOR, ['class' => 'form-control separator unning-number-fields']) !!}
                        </div>
                        <div class="col-md-2">
                            {!! Form::text('RN_D_ZEROES[]', $runningNumber->D_ZEROES, ['class' => 'form-control zeroes running-number-fields']) !!}
                        </div>
                        <div class="col-md-2">    
                            <input type="checkbox" name="RN_ACTIVE[]" value="1" {{ isset($runningNumber)?($runningNumber->D_ACTIVE=='1')? 'checked' : '': ''}}> 
                
                            {{-- @include('settings.documentsetups.toggle-check-input', ['checked' => $runningNumber->D_ACTIVE, 'key' => '1', 'name' => 'RN_ACTIVE']) --}}
                            {{-- {!! Form::checkbox('RN_ACTIVE[]', $runningNumber->D_ACTIVE, ['class' => 'form-control', ($runningNumber->D_ACTIVE == "1")?'checked':'']) !!} --}}
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="RN_SAMPLE[]" class="form-control-plaintext sample" readonly data-content="{{ $runningNumber }}">
                        </div>
                        <div class="col-md-2">
                            {!! Form::text('RN_D_LAST_NO[]', $runningNumber->D_LAST_NO, ['class' => 'form-control last-no running-number-fields']) !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
(function() {
    function leftPad(value, length) {
        return ('0'.repeat(length) + value).slice(-length);
    }

    function transformString(prefix, separator, last_no, zeroes) {
        let runningNumberString = ''
        let nextNumber = last_no;
        let leftPadNumbersString = leftPad(nextNumber, zeroes);
        runningNumberString = prefix + separator + leftPadNumbersString

        return runningNumberString
    }

    // set default value
    let samples = $('.sample')
    $.each(samples, function(index, sample) {
        let content = $(this).data('content')
        $(this).val(transformString(content.D_PREFIX, content.D_SEPARATOR, content.D_LAST_NO, content.D_ZEROES))
    })

    // set value on change
    $('.running-number-fields').on('change', function() {
        let parent = $(this).parent().parent()
        let sample = parent.find('.sample')
        let content = sample.data('content')
        let prefix = parent.find('.prefix').val()
        let separator = parent.find('.separator').val()
        let last_no = parent.find('.last-no').val()
        let zeroes = parent.find('.zeroes').val()

        sample.val(transformString(prefix, separator, last_no, zeroes))
    })

}) ()
</script>
@endpush
