@extends('master')
@section('content')

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('documentsetups.store') }}" method="POST">
            @csrf
            @include('settings/documentsetups/documentsetups')

        </form>
    </div>
</div>

@endsection
