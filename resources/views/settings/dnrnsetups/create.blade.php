@extends('master')
@section('content')

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('dnrnsetups.update', 0) }}" method="POST">
            <input type="hidden" name="_method" value="PUT"/>
            @csrf
            @include('settings/dnrnsetups/dnrns')

        </form>
    </div>
</div>

@endsection
