<input type="checkbox" name="{{ isset($name) ? $name : 'OPTIONS' }}[]" class="check"
    data-toggle="toggle" data-on="Yes" data-off="No"
    data-onstyle="success" data-offstyle="secondary"
    data-width="70" data-height="10"
    {{ isset($checked) && $checked ? 'checked' : ''}}
    value="{{ $key }}">
