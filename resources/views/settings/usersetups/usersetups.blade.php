<div class="card">
    <div class="card-header bg-primary text-white">
        User Setup
    </div>
    <div class="card-body">    
        <div class="form-group row">
            <label for="usercode" class="col-md-2"> Usercode <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input name="usercode" type="text" maxlength="50" class="form-control" id="usercode" value="{{ isset($userSetup->usercode)?$userSetup->usercode:'' }}" required>
                <input name="usercodecoy" type="hidden" maxlength="50" class="form-control" id="usercodecoy" value="{{ isset($userSetup->usercode)?$userSetup->usercode:'' }}">
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-2"> Name <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input type="text" maxlength="100" name="name" id="name" class="form-control" value="{{ isset($userSetup->name)?$userSetup->name:'' }}" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-2"> Email <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input type="text" maxlength="100" name="email" id="email" class="form-control" value="{{ isset($userSetup->email)?$userSetup->email:'' }}" required>
                <input type="hidden" maxlength="100" name="emailcoy" id="emailcoy" class="form-control" value="{{ isset($userSetup->email)?$userSetup->email:'' }}">
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-2">Password <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <input name="passwordcoy" type="password" maxlength="100" class="form-control" value="" placeholder="********" required>
                <input name="password" type="hidden" maxlength="100" class="form-control" value="{{ isset($userSetup->password)?$userSetup->password:'' }}">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="role" class="col-md-2">Role<span class="text-danger">*</span></label>
            <div class="col-md-10">
                <select class="myselect form-control" name="role" id="role" required>
                    <option value=""></option>
                    @foreach($usergroups as $usergroup)
                    <option value="{{ $usergroup->name }}"
                        {{ isset($userSetup)?$userSetup->role == $usergroup->name? 'selected' : '' : ''}}>{{ $usergroup->name }}</option>
                    @endforeach
                </select>
                {{-- <input name="role" type="text" maxlength="100" class="form-control" value="{{ isset($userSetup->role)?$userSetup->role:'' }}" required> --}}
            </div>
        </div>
        
        <div class="form-group row">		
            <label class="control-label col-md-2">Active <span class="text-danger">*</span></label>
            <div class="col-md-10">
                <div class="radio-list">
                    <label class="radio-inline">
                    <input type="radio" name="active" value="1" {{ isset($cylindercategory)?($cylindercategory->active=='1')? 'checked' : '': 'checked' }}/> Active </font></label>
                    <label class="radio-inline">
                    <input type="radio" name="active" value="0" {{ isset($cylindercategory)?($cylindercategory->active=='0')? 'checked' : '': '' }}/> Non-Active </font></label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary submit">Submit</button>
        </div>
    </div>