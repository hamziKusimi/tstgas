@extends('master')
@section('content')

<div class="alert red-alert text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist"></p>
</div>

<div class="alert red-alert2 text-secondary d-none" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="alert-exist2"></p>
</div>

<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
        <form action ="{{ route('usersetups.store') }}" method="POST">
            @csrf
            @include('settings/usersetups/usersetups')

        </form>
    </div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
    $('#email').on('change', function(){
        let data = $(this).val()
        data = data.trim()
        let originalEmail = $('#emailcoy').val()
        originalEmail = originalEmail.trim()
        let broute = "{{ route('usersetups.get.email', 'EMAIL') }}"
        let route = broute.replace('EMAIL', data)
        console.log(route)
        $.ajax({
                url: route,
                method: "GET",
                data: '',
            }).done(function (response) {
                // console.log(response[0].email)
                console.log(response)
                if(response.length){
                    let selectedEmail = response[0]

                    if(data.toLowerCase() == selectedEmail.email.trim().toLowerCase() && data.toLowerCase() != originalEmail.toLowerCase()){
                        alert('Email already exist!\n If submit, user with same email address will be updated')
                    }
                }else{
                }
            }).fail(function (response) {
                console.log('heree')
            })

    })
   
   $('#usercode').on('change', function(){
       let data = $(this).val()
       data = data.trim()
       let originalUsercode = $('#usercodecoy').val()
       originalUsercode = originalUsercode.trim()
       let broute = "{{ route('usersetups.get.usercode', 'UCODE') }}"
       let route = broute.replace('UCODE', data)
       
       $.ajax({
               url: route,
               method: "GET",
               data: '',
           }).done(function (response) {
                           
               if(response.length){
                   let selectedEmail = response[0]
                   
                   if(data.toLowerCase() == selectedEmail.usercode.trim().toLowerCase() && data.toLowerCase() != originalUsercode.toLowerCase()){
                    //    alert('Usercode already exist!\nPlease Create a new Usercode')
                        $('.alert-exist').text('Usercode already exist! Please Create a new Usercode')
                        $(".red-alert").removeClass('d-none')
                    //    $('#usercode').val('')
                       $('.submit').prop('disabled',true)
                   }else{
                        $(".red-alert").addClass('d-none')
                       $('.submit').prop('disabled',false)

                    }
               }else{
                    $(".red-alert").addClass('d-none')
                   $('.submit').prop('disabled',false)
               }
           }).fail(function (response) {
               console.log('heree')
           })

   })
    </script>
@endpush    