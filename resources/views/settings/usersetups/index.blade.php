@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('USER_SETUP_CR'))
    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('usersetups.create') }}"> Add User</a>
        </div>
    </div>
@endif
    
@if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>
@endif
@php ($i = 0)
    
<div class="card">
        <div class="card-body">
            <div class="bg-white">        
                <div class="table-responsive">
                    <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Prefix</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    @if(sizeof($userSetups) > 0)
                        <tbody>
                            @foreach ($userSetups as $userSetup)
                            <tr>
                            
                                <td>{{ ++$i }}</td>
                                {{-- @if(Auth::user()->hasPermissionTo('USER_SETUP_UP')) --}}
                                    <td><a href="{{ route('usersetups.edit', $userSetup->id) }}">{{ $userSetup->name }}</td>
                                {{-- @else --}}
                                {{-- <td>
                                    {{ $userSetup->name }}
                                </td>
                                @endif --}}
                                <td>{{ $userSetup->email}}</td>
                                <td>{{ $userSetup->active }}</td>
                                @if(Auth::user()->hasPermissionTo('USER_SETUP_DL'))
                                    <td>
                                        <a href="{{ route('usersetups.destroy', $userSetup->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                        <i class="fa fa-trash"></i></a>  
                                    </td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    @endif
                    </table>
                </div>
            </div>
        </div>
    </div>


    {{-- {!! $userSetups->links() !!} --}}

@endsection
{{-- @push('script') --}}
@section('js')

    <script>
    // $(user).ready(function() {
    //     $('#example').DataTable();
    // } );
    </script>
    <script>        
        $(user).ready(function() {
            $('#example').DataTable({
            });
        } );
    </script>
{{-- @endpush --}}
@endsection

