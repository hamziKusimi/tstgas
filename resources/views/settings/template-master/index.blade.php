@extends('layouts.adminlte.master')
@section('title', 'Template Master')
@section('page-header', 'Templates')
@section('breadcrumbs', Breadcrumbs::render('template-master.index'))

@section('header')
    @include('file-maintenance.template-master.index-options')
    @include('common.errors')
    @include('common.messages')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="template-master-datatable">
                <thead>
                    <tr class="bg-primary text-white">
                        <th width="5%">#</th>
                        <th width="70%">Description</th>
                        <th width="10%">Type</th>
                        <th width="15%"></th>
                    </tr>
                </thead>
                <tbody>
                    @if (isset($templates))
                        @foreach ($templates as $index => $template)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{!! $template->T_DESC !!}</td>
                                <td>{{ config('kusimi.template-category')[$template->T_CATEGORY] }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('template-master.edit', $template->ID) }}" class="btn btn-success">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a href="{{ route('template-master.destroy', $template->ID) }}" class="btn btn-danger"
                                            data-method="delete" data-confirm="Confirm delete template {{ $index+1 }}?">
                                            <i class="fa fa-times"></i>
                                            Delete
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
(function() {
    $('#template-master-datatable').DataTable()
}) ()
</script>
@endpush
