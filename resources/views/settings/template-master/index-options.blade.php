<div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3">
        @if (Auth::user()->hasPermissionTo('Create Templates'))
            <a class="btn bg-dark-blue text-white form-control text-center" href="{{ route('template-master.create') }}">
                <i class="fa fa-plus pr-2"></i> Create
            </a>
        @endif
    </div>
</div>
