@extends('layouts.adminlte.master')
@section('title', 'Template Master')
@section('page-header', 'Templates')
@section('breadcrumbs', Breadcrumbs::render('template-master.edit', $template))
@section('subheader', 'Edit')

@section('content')
<div class="card">
    <div class="card-body">
        {!! Form::model($template, ['url' => route('template-master.update', $template->ID), 'method' => 'PUT']) !!}
            <div class="form-group row">
                {!! Form::label('T_CATEGORY', 'Category', ['class' => 'col-md-1 col-form-label']) !!}
                <div class="col-md-11">
                    {!! Form::select('T_CATEGORY', config('kusimi.template-category'), null,
                        ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::textarea('T_DESC', null, ['class' => 'form-control summernote', 'rows' => '4']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Save', ['class' => 'form-control btn btn-success']) !!}
            </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
@include('common.form.inputmask')
@include('common.form.summernote')
@endpush
