<div class="row">
    <div class="col-md-2 narrow-padding-global"></div>
    <div class="col-md-2 narrow-padding-global"></div>
    <div class="col-md-2 narrow-padding-global"></div>
    <div class="col-md-2 narrow-padding-global"></div>
    <div class="col-md-2 narrow-padding-global"></div>
    <div class="col-md-2 narrow-padding-global">
        @if (Auth::user()->hasPermissionTo('Create Templates') || Auth::user()->hasPermissionTo('Update Templates'))
            <button type="submit" class="btn bg-dark-blue text-white form-control text-center">
                <i class="fa fa-share-square-o pr-2"></i>
                Save
            </button>
        @endif
    </div>
</div>
