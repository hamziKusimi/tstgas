
@extends('master')

@section('content')


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <form id="formEnquiry">
                @csrf
                @include('settings.accountposting.form')
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modals')

    @include('dailypro.components.reportdebtor-modal')
    @include('dailypro.components.reportdebtorto-modal')
    @include('dailypro.components.reportcreditor-modal')
    @include('dailypro.components.reportcreditorto-modal')

@endsection

@push('scripts')
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>
{{-- <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script> --}}
<script type="text/javascript">
    $(".myselect").select2();
    $(".overlay").hide();

    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $( document ).ready(function() {
        let isDbConn = {{ $DBConnection }};
        if(!isDbConn){
            alert("Failed to connect to account database, go configure it in system setup first")
        }
        @include('js.account-posting-js')
    });
</script>
<script type="text/javascript">
    $('.type').change(function() {
        if(this.checked) {
            $("#itemType").prop('disabled', false);
        }else{
            $("#itemType").prop('disabled', true);
        }
    });

    $('.docno').change(function() {
        if(this.checked) {
            $(".docnofrm").prop('disabled', false);
            $(".docnoto").prop('disabled', false);
        }else{
            $(".docnofrm").prop('disabled', true);
            $(".docnoto").prop('disabled', true);
            $(".docnofrm").val('');
            $(".docnoto").val('');
        }
    });

    $('.docnofrm').on('change', function (){
        $docnofrm = $(this).val()
        $('.docnoto').val($docnofrm)
    });

    $('.debtor').change(function() {
        if(this.checked) {
            $("#debtorfrm").prop('disabled', false);
            $("#debtorto").prop('disabled', false);
            $(".debtor-input .btn").removeClass( "disabled");
        }else{
            $("#debtorfrm").prop('disabled', true);
            $("#debtorto").prop('disabled', true);
            $("#debtorfrm").val('');
            $("#debtorto").val('');
            $(".debtor-input .btn").addClass( "disabled");
        }
    });

    $('.debtorfrm').on('change', function (){
        $debtorfrm = $(this).val()
        $('.debtorto').val($debtorfrm)
    });

    $('.creditor').change(function() {
        if(this.checked) {
            $("#creditorfrm").prop('disabled', false);
            $("#creditorto").prop('disabled', false);
            $(".creditor-input .btn").removeClass( "disabled");
        }else{
            $("#creditorfrm").prop('disabled', true);
            $("#creditorto").prop('disabled', true);
            $("#creditorfrm").val('');
            $("#creditorto").val('');
            $(".creditor-input .btn").addClass( "disabled");
        }
    });

    $('.creditorfrm').on('change', function (){
        $creditorfrm = $(this).val()
        $('.creditorto').val($creditorfrm)
    });

    $('.date').change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    $('#debtorlisttable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('cylinder.debtor.list.data') !!}',
        columns: [
            { data: 'accountcode', name: 'accountcode' },
            { data: 'name', name: 'name' }
        ],
    });

    $('#debtorlisttable2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('cylinder.debtor.list.data') !!}',
        columns: [
            { data: 'accountcode', name: 'accountcode' },
            { data: 'name', name: 'name' }
        ],
    });

    $('.reportdebtor-modal').on('shown.bs.modal', function() {
        var rowmodal = $('.reportdebtor-modal').find('tbody')
        var row = rowmodal.find('tr')
        row.attr({'data-id':'id', 'id':'route2'})

            let modal = $('.reportdebtor-modal')
            let firstRow = $(this).parents('tr')
            modal.on('dblclick', 'tr', function() {
                let id = $(this).find('td:eq(0)').html();
                $('.debtorfrm').val(id)
                $('.debtorto').val(id)

                $('.reportdebtor-modal').modal('hide');
            })
    })

    $('.reportdebtorto-modal').on('shown.bs.modal', function() {
        var rowmodal = $('.reportdebtorto-modal').find('tbody')
        var row = rowmodal.find('tr')
        row.attr({'data-id':'id', 'id':'route2'})

            let modal = $('.reportdebtorto-modal')
            let firstRow = $(this).parents('tr')
            modal.on('dblclick', 'tr', function() {
                let id = $(this).find('td:eq(0)').html();
                $('.debtorto').val(id)

                $('.reportdebtorto-modal').modal('hide');
            })
    })

    $('#creditorlisttable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('creditor.list.data') !!}',
        columns: [
            { data: 'accountcode', name: 'accountcode' },
            { data: 'name', name: 'name' }
        ],
    });

    $('#creditorlisttable2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('creditor.list.data') !!}',
        columns: [
            { data: 'accountcode', name: 'accountcode' },
            { data: 'name', name: 'name' }
        ],
    });

    $('.reportcreditor-modal').on('shown.bs.modal', function() {
        var rowmodal = $('.reportcreditor-modal').find('tbody')
        var row = rowmodal.find('tr')
        row.attr({'data-id':'id', 'id':'route2'})

            let modal = $('.reportcreditor-modal')
            let firstRow = $(this).parents('tr')
            modal.on('dblclick', 'tr', function() {
                let id = $(this).find('td:eq(0)').html();
                $('.creditorfrm').val(id)
                $('.creditorto').val(id)

                $('.reportcreditor-modal').modal('hide');
            })
    })

    $('.reportcreditorto-modal').on('shown.bs.modal', function() {
        var rowmodal = $('.reportcreditorto-modal').find('tbody')
        var row = rowmodal.find('tr')
        row.attr({'data-id':'id', 'id':'route2'})

            let modal = $('.reportcreditorto-modal')
            let firstRow = $(this).parents('tr')
            modal.on('dblclick', 'tr', function() {
                let id = $(this).find('td:eq(0)').html();
                $('.creditorto').val(id)

                $('.reportcreditorto-modal').modal('hide');
            })
    })



</script>
@endpush