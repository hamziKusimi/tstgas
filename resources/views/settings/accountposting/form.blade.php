
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-6 form-input">
                <div class="input-group row narrow-padding-global">
                    <div class="col-md-3">
                        <input type="checkbox" name="type" value="1" class="type"> Type
                    </div>
                    <div class="col-md-5">
                        <select class="form-control " style="width:100%" name="itemType" id="itemType" disabled>
                                <option value="gr" selected>Goods Received</option>
                                {{-- <option value="pr">Purchase Return</option> --}}
                                <option value="cb">Cash Bill</option>
                                <option value="inv">Invoice</option>
                                {{-- <option value="sr">Sales Return</option> --}}
                        </select>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <div class="col-md-3">
                        <input type="checkbox" name="docno" value="1" class="docno"> Doc. No
                    </div>
                    <div class="col-md-5">
                        <input name="docnofrm" type="text" maxlength="100" maxlength="30" class="form-control docnofrm" disabled>
                    </div>
                    <div class="col-md-4">
                        <input name="docnoto" type="text" maxlength="100" maxlength="30" class="form-control docnoto" disabled>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global debtor-input">
                    <div class="col-md-3">
                        <input type="checkbox" name="debtor" value="1" class="debtor"> Debtor
                    </div>
                    <div class="col-md-5">
                            <div class="input-group-append" style="width:100%">
                                    {!! Form::text('debtorfrm', null, ['class' => 'form-control debtorfrm', 'id'
                                    =>'debtorfrm',
                                    'disabled' =>'disabled'])!!}
                                <div class="btn input-group bg-dark-blue text-white pointer searchdebtor disabled" data-toggle="modal" data-target=".reportdebtor-modal" style="width:25%">
                                    <small>
                                        <i class="fa fa-search"></i>
                                    </small>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="input-group-append" style="width:100%">
                                    {!! Form::text('debtorto', null, ['class' => 'form-control debtorto', 'id'
                                    =>'debtorto',
                                    'disabled' =>'disabled'])!!}
                                <div class="btn input-group bg-dark-blue text-white pointer searchdebtor disabled" data-toggle="modal" data-target=".reportdebtorto-modal" style="width:25%">
                                    <small>
                                        <i class="fa fa-search"></i>
                                    </small>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global creditor-input">
                    <div class="col-md-3">
                        <input type="checkbox" name="creditor" value="1" class="creditor"> Creditor
                    </div>
                    <div class="col-md-5">
                            <div class="input-group-append" style="width:100%">
                                    {!! Form::text('creditorfrm', null, ['class' => 'form-control creditorfrm', 'id'
                                    =>'creditorfrm',
                                    'disabled' =>'disabled'])!!}
                                <div class="btn input-group bg-dark-blue text-white pointer searchcreditor disabled" data-toggle="modal" data-target=".reportcreditor-modal" style="width:25%">
                                    <small>
                                        <i class="fa fa-search"></i>
                                    </small>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="input-group-append" style="width:100%">
                                    {!! Form::text('creditorto', null, ['class' => 'form-control creditorto', 'id'
                                    =>'creditorto',
                                    'disabled' =>'disabled'])!!}
                                <div class="btn input-group bg-dark-blue text-white pointer searchcreditor disabled" data-toggle="modal" data-target=".reportcreditorto-modal" style="width:25%">
                                    <small>
                                        <i class="fa fa-search"></i>
                                    </small>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <div class="col-md-3">
                        <input type="checkbox" name="date" value="1" class="date"> Date
                    </div>
                    <div class="col-md-6">
                            <div class="input-group">
                                {!! Form::text('dates', null, ['class' => 'form-control form-data',
                                'autocomplete'=>'off', 'id' =>'dates', 'disabled'=>'disabled'
                                ])!!}
                            </div>
                    </div>
                </div>
                <div class="input-group">
                    {!! Form::hidden('orderBy', null, ['id'=>'orderBy','data-sorting'])!!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group row narrow-padding-global">
                    <div class="col-md-6">
                        <input type="checkbox" name="showPosted" value="1" class="showPosted"> Show Posted Data
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <div class="col-md-6">
                        <input type="checkbox" name="allowRePosted" value="1" class="allowRePosted"> Re-Post Posted Data
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <div class="col-md-3" style="padding-right: 0px;">
                        Batch No.
                    </div>
                    <div class="col-md-5" style="padding-left: 0px;">
                        <input name="batchNo" id="batchNo" type="text" maxlength="100" maxlength="30" class="form-control batchNo">
                    </div>
                </div>
                <br>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-2" style="padding-right:0px">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-search" name="search" value="search" style="width:100%">Search</button>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-post" name="post" value="post" style="width:100%" disabled>Post</button>
                </div>
            </div>
        </div>
    </div>
<div class="overlay search-result">
  <i class="fa fa-refresh fa-spin"></i>
</div>
</div>



<hr>
<div class="box">
    <div class="box-body">
        <div class="table-responsive" id="target">
            <table id="enquiry_data" class="table table-bordered table-hover " style="max-width:100%;white-space:nowrap;">
                    <thead class="bg-dark-blue text-white text-sm" >
                        <tr>
                            <th style="font-size: 12px;" ></th>
                            <th style="font-size: 12px;" >Type</th>
                            <th style="font-size: 12px;" >Doc No.</th>
                            <th style="font-size: 12px;" >Date</th>
                            <th style="font-size: 12px;" >Ref No</th>
                            <th style="font-size: 12px;" >Acc. Code</th>
                            <th style="font-size: 12px;" >Name</th>
                            <th style="font-size: 12px;" >Amount</th>
                            <th style="font-size: 12px;" >Tax Amt</th>
                            <th style="font-size: 12px;" > Rd Adj.</th>
                            <th style="font-size: 12px;" > E.Amount</th>
                            <th style="font-size: 12px;" >Posted</th>
                        </tr>
                    </thead>
            </table>
        </div>
    </div>
    <div class="overlay table-result">
      <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>


