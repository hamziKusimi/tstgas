@extends('master')
@section('content')
@if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>
@endif
<div class="box box-solid">
    <div class="box-header"></div>

    <div class="box-body with-border">
            @if(isset($systemSetup->id))
                <form action ="{{ route('systemsetups.update', isset($systemSetup->id)?$systemSetup->id:'' )}}" method="POST">
                    <input type="hidden" name="_method" value="PUT"/>
                @csrf
                @include('settings/systemsetups/form')

                </form>
            @else
                <form action ="{{ route('systemsetups.store')}}" method="POST">
                @csrf
                @include('settings/systemsetups/form')

                </form>
            @endif
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {

    $('.nav-link').on("click",function(){
        var tab = $(this).data('target');
        $('.padding-systemsetup').not(tab).removeClass('show');
    })

    if ($('#qty_decimal').prop('checked')) {
        $('.qty_decimal_place').show();
    }else{
        $('.qty_decimal_place').hide();
    }

    $('#qty_decimal').change(function(){
        if ($(this).prop('checked')) {
            $('.qty_decimal_place').show();
        }else{
            $('.qty_decimal_place').hide();
        }
    })

    if ($('#rate_decimal').prop('checked')) {
        $('.rate_decimal_place').show();
    }else{
        $('.rate_decimal_place').hide();
    }

    $('#rate_decimal').change(function(){
        if ($(this).prop('checked')) {
            $('.rate_decimal_place').show();
        }else{
            $('.rate_decimal_place').hide();
        }
    })

    $("#DR_Cash_Purchase_box").change(function() {
        if(this.checked) {
            $("#DR_Cash_Purchase").prop('readonly', true);
        }else{
            $("#DR_Cash_Purchase").prop('readonly', false);
        }
    });

    $("#DR_Credit_Purchase_box").change(function() {
        if(this.checked) {
            $("#DR_Credit_Purchase").prop('readonly', true);
        }else{
            $("#DR_Credit_Purchase").prop('readonly', false);
        }
    });

    $("#CR_Purchase_Return_box").change(function() {
        if(this.checked) {
            $("#CR_Purchase_Return").prop('readonly', true);
        }else{
            $("#CR_Purchase_Return").prop('readonly', false);
        }
    });

    $("#DR_Cash_Sales_box").change(function() {
        if(this.checked) {
            $("#DR_Cash_Sales").prop('readonly', true);
        }else{
            $("#DR_Cash_Sales").prop('readonly', false);
        }
    });

    $("#CR_Cash_Sales_box").change(function() {
        if(this.checked) {
            $("#CR_Cash_Sales").prop('readonly', true);
        }else{
            $("#CR_Cash_Sales").prop('readonly', false);
        }
    });

    $("#DR_Cash_Sales_Return_box").change(function() {
        if(this.checked) {
            $("#DR_Cash_Sales_Return").prop('readonly', true);
        }else{
            $("#DR_Cash_Sales_Return").prop('readonly', false);
        }
    });

    $("#CR_Invoice_Sales_box").change(function() {
        if(this.checked) {
            $("#CR_Invoice_Sales").prop('readonly', true);
        }else{
            $("#CR_Invoice_Sales").prop('readonly', false);
        }
    });

    $("#DR_Credit_Sales_Return_box").change(function() {
        if(this.checked) {
            $("#DR_Credit_Sales_Return").prop('readonly', true);
        }else{
            $("#DR_Credit_Sales_Return").prop('readonly', false);
        }
    });


});
</script>
@endpush
