<div class="row">
    <div class="col-md-12">
        <div class="input-group row narrow-padding-global">
            <label for="code" class="col-md-2">Company Code</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="code" id="code" type="text" maxlength="100" class="form-control code"
                        value="{{ config('config.company.company_no') }}" readonly>
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="name" class="col-md-2"> Name </label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="name" id="name" type="text" maxlength="100" class="form-control name"
                        value="{{ config('config.company.name') }}" readonly>
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="company_no" class="col-md-2">Company No</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="company_no" id="company_no" type="text" maxlength="100" class="form-control company_no"
                        value="{{ config('config.company.company_no') }}" readonly>
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="address" class="col-md-2">Address</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <textarea name="address" id="address" rows="4" type="text" maxlength="100"
                        class="form-control address">{{ isset($systemSetup)?$systemSetup->address:'' }}</textarea>
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="tel" class="col-md-2">Telephone</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="tel" id="tel" type="text" maxlength="100" class="form-control tel"
                        value="{{ isset($systemSetup)?$systemSetup->tel:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="fax" class="col-md-2">Fax</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="fax" id="fax" maxlength="100" class="form-control fax"
                        value="{{ isset($systemSetup)?$systemSetup->fax:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="email" class="col-md-2">Email</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="email" id="email" type="text" maxlength="100" class="form-control email"
                        value="{{ isset($systemSetup)?$systemSetup->email:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="name1" class="col-md-2">Name 1</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="name1" id="name1" type="text" maxlength="100" class="form-control name1"
                        value="{{ isset($systemSetup)?$systemSetup->name1:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="name2" class="col-md-2">Name 2</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="name2" id="name2" type="text" maxlength="100" class="form-control name2"
                        value="{{ isset($systemSetup)?$systemSetup->name2:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="licenseno" class="col-md-2">License No.</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <textarea name="licenseno" id="address" rows="4" type="text" maxlength="100"
                        class="form-control licenseno">{{ isset($systemSetup)?$systemSetup->licenseno:'' }}</textarea>
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="salestaxno" class="col-md-2">Sales Tax No.</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="salestaxno" id="salestaxno" type="text" maxlength="100" class="form-control salestaxno"
                        value="{{ isset($systemSetup)?$systemSetup->salestaxno:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="servtaxno" class="col-md-2">Service Tax No.</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="servtaxno" id="servtaxno" type="text" maxlength="100" class="form-control servtaxno"
                        value="{{ isset($systemSetup)?$systemSetup->servtaxno:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Bank Detail</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="bankDetail" id="" type="text" maxlength="100" class="form-control"
                        value="{{ isset($systemSetup)?$systemSetup->bank_detail:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="" class="col-md-2">Bank Account No.</label>
            <div class="col-md-10">
                <div class="input-group-prepend">
                    <input name="BankAccNo" id="" type="text" maxlength="100" class="form-control"
                        value="{{ isset($systemSetup)?$systemSetup->bank_account_no:'' }}">
                </div>
            </div>
        </div>
        <br><br>
        <ul class="nav nav-pills" style="background-color:#3c8dbc">
            <li class="nav-item">
                <a href="#" data-toggle="collapse" data-target="#settings" class="nav-link">Settings</a>
            </li>
            <li class="nav-item">
                <a href="#" data-toggle="collapse" data-target="#customfield" class="nav-link">Custom Field</a>
            </li>
            <li class="nav-item">
                <a href="#" data-toggle="collapse" data-target="#authorization" class="nav-link">Authorization</a>
            </li>
            @if(config('config.cylinder.cylinder') == "true")
            <li class="nav-item">
                <a href="#" data-toggle="collapse" data-target="#cylinder" class="nav-link">Cylinder</a>
            </li>
            @endif
            <li class="nav-item">
                <a href="#" data-toggle="collapse" data-target="#account" class="nav-link">Account</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="collapse padding-systemsetup" id="settings">
                <div class="input-group row narrow-padding-global">
                    <label for="use_tax" class="col-md-2">Use Tax</label>
                    <div class="col-md-3">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="use_tax" value="1"
                                {{ isset($systemSetup)?($systemSetup->use_tax=='1')? 'checked' : '': ''}}>
                        </div>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global ">
                    <label for="qty_decimal" class="col-md-2">Quantity(Decimal)</label>
                    <div class="col-md-1">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="qty_decimal" value="1" id="qty_decimal"
                                {{ isset($systemSetup)?($systemSetup->qty_decimal=='1')? 'checked' : '': ''}}>
                        </div>
                    </div>
                    <label class="qty_decimal_place">Decimal Place :</label>
                    <div class="col-md-1 qty_decimal_place">
                        <input type="number" name="qty_decimal_place" class="form-control" min="0" id="qty_decimal_place"
                        value="{{ isset($systemSetup)?$systemSetup->qty_decimal_place :0}}">
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <label for="rate_decimal" class="col-md-2">Rate(Decimal)</label>
                    <div class="col-md-1">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="rate_decimal" value="1" id="rate_decimal"
                                {{ isset($systemSetup)?($systemSetup->rate_decimal=='1')? 'checked' : '': ''}}>
                        </div>
                    </div>
                    <label class="rate_decimal_place">Decimal Place :</label>
                    <div class="col-md-1 rate_decimal_place">
                            <input type="number" name="rate_decimal_place" class="form-control"  min="0" id="rate_decimal_place"
                            value="{{ isset($systemSetup)?$systemSetup->rate_decimal_place :0}}">
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <label for="stock_item" class="col-md-2">Report (Stock Item)</label>
                    <div class="col-md-3">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="stock_item" value="1"
                                {{ isset($systemSetup)?($systemSetup->stock_item=='1')? 'checked' : '': ''}}>
                        </div>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <label for="uom1" class="col-md-2">UOM 1</label>
                    <div class="col-md-3">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="uom1" value="1"
                                {{ isset($systemSetup)?($systemSetup->uom1=='1')? 'checked' : '': 'checked'}}>
                        </div>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <label for="uom2" class="col-md-2">UOM 2</label>
                    <div class="col-md-3">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="uom2" value="1"
                                {{ isset($systemSetup)?($systemSetup->uom2=='1')? 'checked' : '': ''}}>
                        </div>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <label for="uom3" class="col-md-2">UOM 3</label>
                    <div class="col-md-3">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="uom3" value="1"
                                {{ isset($systemSetup)?($systemSetup->uom3=='1')? 'checked' : '': ''}}>
                        </div>
                    </div>
                </div>
                @if(config('config.cylinder.cylinder') == "true")
                <div class="input-group row narrow-padding-global">
                    <label class="col-md-3">Gas Sales Invoice Task Scheduler</label>
                    <div class="input-group-prepend">
                        <input type="number" name="taskScheduler" value="1" class="form-control"
                            style="width: 35%;margin-right: 5px;">
                        <label> Day(s)</label>
                    </div>
                </div>
                @endif
                <div>
                    <hr>
                </div>

            </div>

            @php

            @endphp
            <div class="tab-content">
                <div class="collapse padding-systemsetup" id="customfield">
                    <label class="control-label col-md-12">Debtor Master</label>
                    <div class="input-group row narrow-padding-global">
                        <label for="custom1" class="col-md-2">Custom Field 1</label>
                        <div class="col-md-5">
                            <div class="input-group-prepend">
                                <input name="custom1" id="custom1" type="text" maxlength="100"
                                    class="form-control custom1"
                                    value="{{ isset($systemSetup)?$systemSetup->custom1:'License No' }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="radio-inline"><input type="radio" name="custom1_type" value="text"
                                    {{ isset($systemSetup) && $systemSetup->custom1_type=='text'?'checked':''}}>&nbsp;Text
                                &nbsp;</label>
                            <label class="radio-inline"><input type="radio" name="custom1_type" value="checkbox"
                                    {{ isset($systemSetup) && $systemSetup->custom1_type=='checkbox'?'checked':''}}>&nbsp;Checkbox
                                &nbsp;</label>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="custom2" class="col-md-2">Custom Field 2</label>
                        <div class="col-md-5">
                            <div class="input-group-prepend">
                                <input name="custom2" id="custom2" type="text" maxlength="100"
                                    class="form-control custom2"
                                    value="{{ isset($systemSetup)?$systemSetup->custom2:'' }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="radio-inline"><input type="radio" name="custom2_type" value="text"
                                    {{ isset($systemSetup) && $systemSetup->custom2_type=='text'?'checked':''}}>&nbsp;Text
                                &nbsp;</label>
                            <label class="radio-inline"><input type="radio" name="custom2_type" value="checkbox"
                                    {{ isset($systemSetup) && $systemSetup->custom2_type=='checkbox'?'checked':''}}>&nbsp;Checkbox
                                &nbsp;</label>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="custom3" class="col-md-2">Custom Field 3</label>
                        <div class="col-md-5">
                            <div class="input-group-prepend">
                                <input name="custom3" id="custom3" type="text" maxlength="100"
                                    class="form-control custom3"
                                    value="{{ isset($systemSetup)?$systemSetup->custom3:'' }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="radio-inline"><input type="radio" name="custom3_type" value="text"
                                    {{ isset($systemSetup) && $systemSetup->custom3_type=='text'?'checked':''}}>&nbsp;Text
                                &nbsp;</label>
                            <label class="radio-inline"><input type="radio" name="custom3_type" value="checkbox"
                                    {{ isset($systemSetup) && $systemSetup->custom3_type=='checkbox'?'checked':''}}>&nbsp;Checkbox
                                &nbsp;</label>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="custom4" class="col-md-2">Custom Field 4</label>
                        <div class="col-md-5">
                            <div class="input-group-prepend">
                                <input name="custom4" id="custom4" type="text" maxlength="100"
                                    class="form-control custom4"
                                    value="{{ isset($systemSetup)?$systemSetup->custom4:'' }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="radio-inline"><input type="radio" name="custom4_type" value="text"
                                    {{ isset($systemSetup) && $systemSetup->custom4_type=='text'?'checked':''}}>&nbsp;Text
                                &nbsp;</label>
                            <label class="radio-inline"><input type="radio" name="custom4_type" value="checkbox"
                                    {{ isset($systemSetup) && $systemSetup->custom4_type=='checkbox'?'checked':''}}>&nbsp;Checkbox
                                &nbsp;</label>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="custom5" class="col-md-2">Custom Field 5</label>
                        <div class="col-md-5">
                            <div class="input-group-prepend">
                                <input name="custom5" id="custom5" type="text" maxlength="100"
                                    class="form-control custom5"
                                    value="{{ isset($systemSetup)?$systemSetup->custom5:'' }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="radio-inline"><input type="radio" name="custom5_type" value="text"
                                    {{ isset($systemSetup) && $systemSetup->custom5_type=='text'?'checked':''}}>&nbsp;Text
                                &nbsp;</label>
                            <label class="radio-inline"><input type="radio" name="custom5_type" value="checkbox"
                                    {{ isset($systemSetup) && $systemSetup->custom5_type=='checkbox'?'checked':''}}>&nbsp;Checkbox
                                &nbsp;</label>
                        </div>
                    </div>
                    <br>
                    <label class="control-label col-md-12">Stock Master</label>
                    <div class="input-group row narrow-padding-global">
                        <label for="price1" class="col-md-2">Price 1</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input name="price1" id="price1" type="text" maxlength="100" class="form-control price1"
                                    value="{{ isset($systemSetup)?$systemSetup->price1:'' }}">
                            </div>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="price2" class="col-md-2">Price 2</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input name="price2" id="price2" type="text" maxlength="100" class="form-control price2"
                                    value="{{ isset($systemSetup)?$systemSetup->price2:'' }}">
                            </div>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="price3" class="col-md-2">Price 3</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input name="price3" id="price3" type="text" maxlength="100" class="form-control price3"
                                    value="{{ isset($systemSetup)?$systemSetup->price3:'' }}">
                            </div>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="price4" class="col-md-2">Price 4</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input name="price4" id="price4" type="text" maxlength="100" class="form-control price4"
                                    value="{{ isset($systemSetup)?$systemSetup->price4:'' }}">
                            </div>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="price5" class="col-md-2">Price 5</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input name="price5" id="price5" type="text" maxlength="100" class="form-control price5"
                                    value="{{ isset($systemSetup)?$systemSetup->price5:'' }}">
                            </div>
                        </div>
                    </div>
                    <div class="input-group row narrow-padding-global">
                        <label for="price6" class="col-md-2">Price 6</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input name="price6" id="price6" type="text" maxlength="100" class="form-control price6"
                                    value="{{ isset($systemSetup)?$systemSetup->price6:'' }}">
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="tab-content">
                <div class="collapse padding-systemsetup" id="authorization">
                    <div class="input-group row narrow-padding-global">
                        <label for="warning_min" class="col-md-3">Warning Below Minimum Price</label>
                        <div class="col-md-1">
                            <div class="input-group-prepend">
                                <input type="checkbox" name="warning_min" value="1"
                                    {{ isset($systemSetup)?($systemSetup->warning_min=='1')? 'checked' : '': ''}}>
                            </div>
                        </div>
                        <label for="req_pass" class="col-md-3">Require Password</label>
                        <div class="col-md-1">
                            <div class="input-group-prepend">
                                <input type="checkbox" name="req_pass" value="1"
                                    {{ isset($systemSetup)?($systemSetup->req_pass=='1')? 'checked' : '': ''}}>
                            </div>
                        </div>
                        <label for="password_min" class="col-md-1">Password</label>
                        <div class="col-md-3">
                            <div class="input-group-prepend">
                                <input name="password_min" id="password_min" type="password" maxlength="100"
                                    class="form-control password_min"
                                    value="{{ isset($systemSetup)?$systemSetup->password_min:'' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="collapse padding-systemsetup" id="account">
                    <div class="input-group row narrow-padding-global">
                        <label for="DB_Server" class="col-md-2">DB Server</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="db_server" name="db_server" value="{{ isset($systemSetup)?$systemSetup->db_server:'' }}">
                            </div>
                        </div><br><br>

                        <label for="DB_Name" class="col-md-2">DB Name</label>
                        <div class="col-md-10">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="db_name" name="db_name" value="{{ isset($systemSetup)?$systemSetup->db_name:'' }}">
                            </div>
                        </div><br><br><br><br>

                        <!--<label for="direct_posting" class="col-md-2">Direct Posting</label>-->
                        <!--<div class="col-md-10">-->
                        <!--    <div class="input-group-prepend">-->
                        <!--        {{-- <input type="text" class="form-control" id="direct_posting" name="direct_posting" value="{{ isset($systemSetup)?$systemSetup->db_name:'' }}"> --}}-->
                        <!--        <input type="hidden" id="direct_posting" name="direct_posting"-->

                        <!--        {{ isset($systemSetup)?($systemSetup->direct_posting=='1')? 'checked' : '' : ''}}>-->

                        <!--    </div>-->
                        <!--</div>-->

                        <label for="DR_Cash_Purchase" class="col-md-2">DR Cash Purchase Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" type="text" id="DR_Cash_Purchase" name="DR_Cash_Purchase"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->dr_cashpurchase_acc: '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category1=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="DR_Cash_Purchase_box" name="DR_Cash_Purchase_box"

                                {{ isset($systemSetup)?($systemSetup->set_category1=='true')? 'checked' : '' : ''}}>by Category Setting
                            </label>
                            </div>
                        </div><br><br>

                        <label for="DR_Credit_Purchase" class="col-md-2">DR Credit Purchase Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="DR_Credit_Purchase" name="DR_Credit_Purchase"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->dr_creditpurchase_acc : '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category2=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="DR_Credit_Purchase_box" name="DR_Credit_Purchase_box"

                                {{ isset($systemSetup)?($systemSetup->set_category2=='true')? 'checked' : '' : ''}}> by Category Setting
                            </label>
                            </div>
                        </div><br><br>

                        <label for="CR_Purchase_Return" class="col-md-2">CR Purchase Return Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="CR_Purchase_Return" name="CR_Purchase_Return"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->cr_purchasereturn_acc : '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category3=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="CR_Purchase_Return_box" name="CR_Purchase_Return_box"

                                {{ isset($systemSetup)?($systemSetup->set_category3=='true')? 'checked' : '' : ''}}> by Category Setting
                            </label>
                            </div>
                        </div> <br><br><br><br>

                        <label for="DR_Cash_Sales" class="col-md-2">DR Cash Bill Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="DR_Cash_Sales" name="DR_Cash_Sales"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->dr_cashsales_acc : '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category4=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="DR_Cash_Sales_box" name="DR_Cash_Sales_box"

                                {{ isset($systemSetup)?($systemSetup->set_category4=='true')? 'checked' : '' : ''}}> by Debtor
                            </label>
                            </div>
                        </div><br><br>

                        <label for="CR_Cash_Sales" class="col-md-2">CR Cash Bill Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="CR_Cash_Sales" name="CR_Cash_Sales"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->cr_cashsales_acc : '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category5=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="CR_Cash_Sales_box" name="CR_Cash_Sales_box"

                                {{ isset($systemSetup)?($systemSetup->set_category5=='true')? 'checked' : '' : ''}}> by Category Setting
                            </label>
                            </div>
                        </div><br><br>

                        <label for="DR_Cash_Sales_Return" class="col-md-2">DR Cash sales Return Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="DR_Cash_Sales_Return" name="DR_Cash_Sales_Return"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->dr_cashsales_return_acc : '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category6=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="DR_Cash_Sales_Return_box" name="DR_Cash_Sales_Return_box"
                                {{ isset($systemSetup)?($systemSetup->set_category6=='true')? 'checked' : '' : ''}}> by Category Setting
                            </label>
                            </div>
                        </div><br><br><br><br>

                        <label for="CR_Invoice_Sales" class="col-md-2">CR Invoice Sales Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="CR_Invoice_Sales" name="CR_Invoice_Sales"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->cr_invoicesales_acc : '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category7=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="CR_Invoice_Sales_box" name="CR_Invoice_Sales_box"
                                {{ isset($systemSetup)?($systemSetup->set_category7=='true')? 'checked' : '' : ''}}> by Category Setting
                            </label>
                            </div></div><br><br>

                        <label for="DR_Credit_Sales_Return" class="col-md-2">DR Credit Sales Return Account</label>
                        <div class="col-md-6">
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" id="DR_Credit_Sales_Return" name="DR_Credit_Sales_Return"
                                data-inputmask="'mask': '*****-**'"
                                value="{{ isset($systemSetup)?$systemSetup->dr_creditsales_return_acc : '' }}"
                                {{ isset($systemSetup)?($systemSetup->set_category8=='true')? 'readonly' : '': ''}}>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label>
                                <input type="checkbox" id="DR_Credit_Sales_Return_box" name="DR_Credit_Sales_Return_box"
                                {{ isset($systemSetup)?($systemSetup->set_category8=='true')? 'checked' : '' : ''}}> by Category Setting
                            </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(config('config.cylinder.cylinder') == "true")
        <div class="tab-content">
            <div class="collapse padding-systemsetup" id="cylinder">
                <div class="input-group row narrow-padding-global">
                    <label for="uom3" class="col-md-4" style="max-width: 27%;">Allow Upload Documents for Gas
                        Rack</label>
                    <div class="col-md-3">
                        <div class="input-group-prepend">
                            <input type="checkbox" name="updoc_gasrack" value="1"
                                {{ isset($systemSetup)?($systemSetup->updoc_gasrack=='1')? 'checked' : '': ''}}>
                        </div>
                    </div>
                </div>
                <div class="input-group row narrow-padding-global">
                    <label for="uom2" class="col-md-2">Cylinder Rental Free Period (Days)</label>
                    <div class="col-md-3">
                        <div class="input-group-prepend">
                            <input name="free_cy" id="free_cy" type="number" maxlength="100"
                                class="form-control free_cy" value="{{ isset($systemSetup)?$systemSetup->free_cy:'' }}">
                        </div>
                    </div>
                </div>
                <div>
                    <hr>
                </div>
            </div>
        </div>
        @endif
        @if(Auth::user()->hasPermissionTo('SYSTEM_SETUP_CR') || Auth::user()->hasPermissionTo('SYSTEM_SETUP_UP'))
        <div class="input-group row narrow-padding-global padding-systemsetup">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-form-submit">Submit</button>
            </div>
        </div>
        @endif
    </div>
</div>