@extends('master')

@section('content')

    <div class="row">
        <div class="col-lg-12 text-left" style="margin-top:10px;margin-bottom: 10px;">
            <a class="btn btn-success " href="{{ route('systemsetups.create') }}"> Add System Setup</a>
        </div>
    </div>

    {{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif --}}

    
@if (Session::has('Success'))
    <div class="alert white-alert text-secondary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ Session::get('Success') }}</p>
    </div>
@endif
@php ($i = 0)
    
<div class="card">
        <div class="card-body">
            <div class="bg-white">        
                <div class="table-responsive">
                    <table id="example" class="table table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th width="280px">More</th>
                            </tr>
                        </thead>
                    @if(sizeof($systemSetups) > 0)
                        <tbody>
                            @foreach ($systemSetups as $systemSetup)
                            <tr>
                            
                                <td>{{ ++$i }}</td>
                                <td><a href="{{ route('systemsetups.edit',$systemSetup->id) }}">{{ $systemSetup->code }}</td>
                                <td>{{ $systemSetup->name}}</td>
                                <td>{{ $systemSetup->address }}</td>
                                <td>
                                    <a href="{{ route('systemsetups.destroy', $systemSetup->id) }}" data-method="delete" data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>  
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    @endif
                    </table>    
                </div>
            </div>
        </div>
    </div>


    {{-- {!! $systemSetups->links() !!} --}}

@endsection
{{-- @push('script') --}}
@section('js')

    <script>
    // $(system).ready(function() {
    //     $('#example').DataTable();
    // } );
    </script>
    <script>        
        $(system).ready(function() {
            $('#example').DataTable({
            });
        } );
    </script>
{{-- @endpush --}}
@endsection

