@extends('master')

@section('page-form-open', Form::open(['url' => route('salesorders.update', $item->id), 'method' => 'PUT']))
@section('header')
<br>
@include('common.ajax-message')
<br>
@endsection

@section('content')
@include('dailypro.salesorders.options',['edit' => true])
<br>
@include('dailypro.components.system-param',[
'systemParam' => [
'data-rounding' => 'false',
'data-use-tax' => 'false',
'data-date' => isset($item) ? $item->date : '',
'data-due-date' => isset($item) ? $item->m_duedate : '',
'data-acode' => isset($item) ? $item->account_code: '',
'data-acode-name' => isset($item) ? $item->name : '',
]
])

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

<div class="box">
    <div class="box-body">
        <div class="card">
            <div class="card-body">
                <div class="pl-2 pr-2">
                    <div class="row">
                        <div class="col-md-5">
                            @include('dailypro.components.bills.left-panel')
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-6">
                            @include('dailypro.components.bills.right-panel-salesorders', [
                            'getDocsApiRoute' => route('salesorders.api.post'),
                            'getQtRoute' => route('salesorders.qtno.get', 'ACC_CODE'),
                            'except' => [
                            'refInvDate', 'referenceNo', 'amount'
                            ],
                            ])
                        </div>
                    </div>
                </div>
                <div class="mt-3"></div>
                @if(isset($systemsetup) && $systemsetup->use_tax)
                @include('dailypro.components.table.salesorders.table', [
                'items' => $contents,
                'deleteRoute' => route('salesorders.data.destroy', 'id')
                ])
                @else
                @include('dailypro.components.table.salesorders.table-withouttax', [
                'items' => $contents,
                'deleteRoute' => route('salesorders.data.destroy', 'id')
                ])
                @endif
            </div>
        </div>
    </div>
    <div class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>


<div class="mt-3"></div>
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.extras')
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.totaling', [
                'roundingIsEnabled' => false
                ])
            </div>
        </div>
    </div>
</div>

@php
if(Auth::user()->hasPermissionTo('MOD_AFT_PR') || isset($item) && $item->printed_by == null){
$mod = "true";
}elseif(isset($item) && $item->printed_by != null){
$mod = "false";
}
@endphp

@endsection
@section('page-form-close', Form::close())

@section('modals')
@include('dailypro.components.item-modal')
@include('dailypro.components.template-master-modal')
@include('dailypro.components.account-modal', [
'removeDebtorFinder' => true
])
@include('dailypro.components.stockcode-modal')
@include('dailypro.salesorders.print-format')
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script>

@include('common.form.inputmask')
@include('common.form.summernote-sm')

<script type="text/javascript">
    (function() {
    @include('dailypro.components.js.debtor-select-js')

    // enable the button if detected the account code has value
    if ($('#debtor-select').val())
        $('.btn-form-submit').prop('disabled', false)

    // prevent # from jumping to top of the screen
    $('a').on('click', function(e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
        }
    })

    //datatable
    $('#stockcode-table').DataTable();

    // salesorder date and due date
    let date = moment.utc(new Date($('.system-settings-param').data('date')))
    let dueDate = moment.utc(new Date($('.system-settings-param').data('due-date')))
    $('#date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#due_date-picker').datetimepicker({ date: dueDate, format: 'DD/MM/YYYY' })

    let postToAccount = 'creditSalesAcc'
    @include('common.billing.js.table-js')
    @include('common.billing.js.left-panel-js')
    @include('common.billing.js.right-panel-js')
    @include('js.right-panel-docno-focusout-js')

    let value = {{ $mod }}
    if(value == false){
        $('input').prop("disabled", true)
        $('select').prop("disabled", true)
        $('.btn-form-submit').prop('disabled', true)
    }

}) ()
</script>
@endpush