<div class="row">
    @isset($edit)
    <div class="col-md-2" style="padding-right:0px">
        <a href="{{ route('salesorders.create') }}" style="width: 100%;"
            class="btn form-control bg-dark-green text-white text-center btn-form-submit">New</a>
    </div>
    @endisset
    <div class="col-md-2" style="padding-right:0px;{{isset($edit) ? "padding-left:0px":null}}">
        @if(Auth::user()->hasPermissionTo('SALES_OD_CR') || Auth::user()->hasPermissionTo('SALES_OD_UP'))
        @include('common.billing.options.submit', [
        'faIcon' => 'share-square-o',
        'color' => 'bg-dark-blue text-white',
        'permissions' => ['Create SALES Orders', 'Update SALES Orders'],
        'name' => 'Submit'
        ])
        @endif
    </div>
    <div class="col-md-2" style="padding-right:0px;padding-left:0px">
        @if(Auth::user()->hasPermissionTo('SALES_OD_PR') && (isset($item) && $item->printed_by == null))
        @if (isset($item->id))
        <button type="button" class="btn form-control bg-secondary text-white text-center btn-form-submit"
            data-toggle="modal" data-target="#exampleModal" style="width: 100%;">
            <i class="fa fa-print pr-2"></i>Print
        </button>
        @endif
        @elseif(Auth::user()->hasPermissionTo('SALES_OD_PRY'))
        @if (isset($item->id))
        <button type="button" class="btn form-control bg-secondary text-white text-center btn-form-submit"
            data-toggle="modal" data-target="#exampleModal" style="width: 100%;">
            <i class="fa fa-print pr-2"></i>Print
        </button>
        @endif
        @endif
    </div>
    @if(isset($edit))
    <div class="col-md-2" style="padding-left:0px">
        <a href="{{ route('salesorders.destroy', $item->id) }}" data-method="delete" style="width: 100%;"
            data-confirm="Confirm delete this account?" class="btn form-control bg-danger text-white text-center btn-form-submit">
            <i class="fa fa-trash pr-2"></i>Delete</a>
    </div>
    @else
        <div class="col-md-2 pl-1 pr-1"></div>
    @endif
    @if (!isset($edit))
    <div class="col-md-2" style="padding-right:0px"></div>
    @endif
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2">
        <a href="{{ route('salesorders.index') }}" style="width: 100%;" class="btn btn-danger"><i
                class="fa"></i>Back</a>
    </div>
</div>