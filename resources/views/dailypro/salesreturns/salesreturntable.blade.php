
<tr class="template-new d-none">
    <td class="text-left" style="padding:1px;"><input type="number" id="sn" name="sn[]" class="form-control input-sm sn"></td>
    <td class="text-left" style="padding:1px;">
        <select class="form-control calculate-field2 input-sm stockcode" style="width:100%" id="stockcode" name="stockcode[]" data-values="{{ $stockcodes }}"> 
        <option value=""></option>
        @foreach($stockcodes as $stockcode)
            <option value="{{$stockcode->id}}" {{ isset($salesreturn)?$salesreturn->stockcode_id == $stockcode->id  ? 'selected' : '' : ''}}>{{$stockcode->code}}</option>
        @endforeach
        </select>
        <input type="hidden" name="stockcode_dt[]" id="stockcode_dt" class="form-control stockcode_dt">
    </td>
    <input type="hidden" name="salesreturndts_id[]" value="{{  isset($salesreturndt)?$salesreturndt->id:null }} "/>
    <td class="text-left" style="padding:1px;"><input type="text" id="descr" name="descr[]" class="form-control input-sm descr"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="qty" name="qty[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->qty, 2):'1.00' }}" class="form-control calculate-field2 input-sm qty"></td>
    <td class="text-left" style="padding:1px;">
        <select class="form-control input-sm uom" style="width:100%" id="uom" name="uom[]" data-values="{{ $uoms }}"> 
            <option value=""></option>
            @foreach($uoms as $uom)
                <option value="{{$uom->id}}" {{ isset($salesreturn)?$salesreturn->uom == $uom->id  ? 'selected' : '' : ''}}>{{$uom->code}}</option>
            @endforeach
        </select>
        {{-- <input type="text" id="uom" name="uom[]" class="form-control calculate-field2 input-sm uom"> --}}
    </td>
    <td class="text-left" style="padding:1px;"><input type="text" id="rate" name="rate[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->rate, 2):'1.00' }}" class="form-control calculate-field2 input-sm rate"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="uprice" name="uprice[]"value="{{ isset($salesreturndt)?number_format($salesreturndt->uprice, 2):'0.00' }}" class="form-control calculate-field2 input-sm uprice"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="ty" name="ty[]" class="form-control input-sm ty"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="discount" name="discount[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->discount, 2):'0.00' }}" class="form-control calculate-field2 input-sm discount"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="amount" name="amount[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->amount, 2):'0.00' }}" class="form-control calculate-field2 input-sm amount"></td>
    <td class="text-left" style="padding:1px;">
        {{-- <a href="#" onclick="ClearFields()" data-confirm="Confirm delete this account?"  type="button" class="btn btn-danger clear"> --}}
        {{-- <small><i class ="fa fa-remove"></i></small> --}}
    </td>
</tr>  


