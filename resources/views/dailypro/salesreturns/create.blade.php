@extends('master')

@section('page-form-open', Form::open(['url' => route('salesreturns.store'), 'method' => 'POST','id'=>'create-form']))
@section('header')
<br>
@include('common.ajax-message')
<br>
@endsection

@section('content')

@include('dailypro.salesreturns.options')
<br>
@include('dailypro.components.system-param',[
    'systemParam' => [
        'data-rounding' => 'false',
        'data-use-tax' => 'false',
    ]
])

<div class="box">
    <div class="box-body">
        <div class="card">
            <div class="card-body">
                <div class="pl-2 pr-2">
                    <div class="row">
                        <div class="col-md-5">
                            @include('dailypro.components.bills.left-panel')
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-6">
                            @include('dailypro.components.bills.right-panel-salesreturns', [
                                'getDocsApiRoute' => route('salesreturns.api.post'),
                                'except' => [
                                    'refInvDate', 'referenceNo', 'amount'
                                ],
                            ])
                        </div>
                    </div>
                </div>
                <div class="mt-3"></div>
                @if(isset($systemsetup) && $systemsetup->use_tax == "1")
                    @include('dailypro.components.table.salesreturns.table')
                @else
                    @include('dailypro.components.table.salesreturns.table-withouttax')
                @endif
            </div>
        </div>
    </div>
    <div class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>


<div class="mt-3"></div>
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.extras')
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.totaling', [
                    'roundingIsEnabled' => 'false'
                ])
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-form-close', Form::close())

@section('modals')
    @include('dailypro.components.item-modal')
    @include('dailypro.components.template-master-modal')
    @include('dailypro.components.account-modal', [
        'removeDebtorFinder' => true
    ])
    @include('dailypro.components.stockcode-modal')
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
{{-- <script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script> --}}

@include('common.form.inputmask')
@include('common.form.summernote-sm')

<script type="text/javascript">
(function() {
    @include('dailypro.components.js.debtor-select-js')

    // prevent # from jumping to top of the screen
    $('a').on('click', function(e) {
        if ($(this).attr('href') == '#') e.preventDefault()
    })

    //datatable
    $('#stockcode-table').DataTable();

    // invoice date and due date
    let date = moment(new Date())
    $('#date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#suppinvdate-picker').datetimepicker({ format: 'DD/MM/YYYY' })

    let postToAccount = 'creditSalesAcc'
    @include('common.billing.js.table-js')
    @include('common.billing.js.left-panel-js')
    @include('common.billing.js.right-panel-js')

    $("#create-form").submit(function () {
        $(".btn-form-submit").attr("disabled", true);
        return true;
    });
        
    $('#debtor-select').on('change', function(){

        let contents = $('.salesman').data('contents')
        console.log(contents)
        let salesmanroute = "{{ route('debtors.get.salesman', 'DEB_CODE') }}"

        let getRouteSalesman = salesmanroute.replace('DEB_CODE', $('#debtor-select').val())

        console.log(getRouteSalesman)

        $.ajax({
            'url': getRouteSalesman,
            'method': 'GET'
        }).done(function(response) {
            console.log(response)

            $('.salesman').val(response)
            $('.salesman').trigger('change')
            
        })
    })
    @include('js.right-panel-docno-focusout-js')

}) ()
</script>
@endpush
