
@if(isset($salesreturndts))
    @foreach ($salesreturndts as $salesreturndt)    
    @php ($i = 0)                 
    <tr class="sortable-row existing-data">
                
        <td class="text-left" style="padding:1px;" ><input type="number" name="sn[]" id="sn" value="{{ isset($salesreturndt)?$salesreturndt->sn:'' }}" class="form-control input-sm sn"></td>
        <td class="text-left" style="padding:1px;">
            <select class="form-control input-sm stockcode2" style="width:100%" name="stockcode[]" id="stockcode2" data-values="{{ $stockcodes }}"> 
            <option value=""></option>
            @foreach($stockcodes as $stockcode)
                <option value="{{$stockcode->id}}" {{ isset($salesreturndt)?$salesreturndt->stockcode_id == $stockcode->id  ? 'selected' : '' : ''}}>{{$stockcode->code}}</option>
            @endforeach
            </select>
        </td>
        <input type="hidden" name="stockcode_dt[]" id="stockcode_dt" value="{{  isset($salesreturndt)?$salesreturndt->stockcode_id:'null' }} " class="form-control stockcode_dt">
        <input type="hidden" name="salesreturndts_id[]" value="{{  isset($salesreturndt)?$salesreturndt->id:null }} "/>
        <input type="hidden" name="salesreturn_id[]" value="{{ $i++ }} "/>
        <td class="text-left" style="padding:1px;" ><input type="text" name="descr[]" id="descr" value="{{ isset($salesreturndt)?$salesreturndt->descr:'' }}" class="form-control input-sm descr"></td>
        <td class="text-left" style="padding:1px;"><input type="text" id="qty" name="qty[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->qty, 2):'1.00' }}" class="form-control calculate-field2 input-sm qty"></td>
        <td class="text-left" style="padding:1px;" >
            <select class="form-control input-sm uom2" style="width:100%" name="uom[]" id="uom" data-values="{{ $uoms }}"> 
                <option value=""></option>
                @foreach($uoms as $uom)
                    <option value="{{$uom->id}}" {{ isset($salesreturndt)?$salesreturndt->uom == $uom->id  ? 'selected' : '' : ''}}>{{$uom->code}}</option>
                @endforeach
            </select>
            {{-- <input type="text" name="uom[]" id="uom" value="{{ isset($salesreturndt)?$salesreturndt->uom:'' }}" class="form-control input-sm uom"> --}}
        </td>
        <td class="text-left" style="padding:1px;"><input type="text" id="rate" name="rate[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->rate, 2):'1.00' }}" class="form-control calculate-field2 input-sm rate"></td>
        <td class="text-left" style="padding:1px;"><input type="text" id="uprice" name="uprice[]"value="{{ isset($salesreturndt)?number_format($salesreturndt->uprice, 2):'0.00' }}" class="form-control calculate-field2 input-sm uprice"></td>
        <td class="text-left" style="padding:1px;" ><input type="text" name="ty[]" id="ty" value="{{ isset($salesreturndt)?$salesreturndt->ty:'' }}" class="form-control input-sm ty"></td>
        <td class="text-left" style="padding:1px;"><input type="text" id="discount" name="discount[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->discount, 2):'0.00' }}" class="form-control calculate-field2 input-sm discount"></td>
        <td class="text-left" style="padding:1px;"><input type="text" id="amount" name="amount[]" value="{{ isset($salesreturndt)?number_format($salesreturndt->amount, 2):'0.00' }}" class="form-control calculate-field2 input-sm amount"></td>
        <td class="text-left" style="padding:1px;">
            <a href="{{ route('salesreturns.destroySalesreturndt', $salesreturndt->id) }}" data-method="delete" data-confirm="Confirm delete this stock?"  type="button" class="btn btn-danger">
            <small><i class ="fa fa-remove"></i></small>
        </td>
    </tr>
    @endforeach        
@endif