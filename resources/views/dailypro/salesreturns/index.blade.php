@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('SALES_RETURN_CR'))
    <div class="col-3" style="flex: 0 0 16%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('salesreturns.create') }}"> Add Sales Return</a>
        </div>
    </div>
    @endif
    @if(Auth::user()->hasPermissionTo('SALES_RETURN_PR') && $print->isEmpty())
    <div class="col-2" style="flex: 0 0 16%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @elseif(Auth::user()->hasPermissionTo('SALES_RETURN_PRY'))
    <div class="col-2" style="flex: 0 0 16%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="salesreturnTab" class="table table-bordered table-striped" cellspacing="0" style="font-size: 11px;">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Ref. No 1</th>
                            <th>Ref. No 2</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


{{-- {!! $salesreturns->links() !!} --}}
@include('dailypro.salesreturns.printmodal')
@endsection

@push('scripts')
<script>
    (function() {

        $('#salesreturnTab').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: '{!! route('salesreturn.datatable.data') !!}',
            dataType: 'json',
            type: 'GET',
            data:{ _token: '{{csrf_token()}}'}
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'docno', name: 'docno' },
            { data: 'date', name: 'date' },
            { data: 'account_code', name: 'account_code' },
            { data: 'name', name: 'name' },
            { data: 'ref1', name: 'ref1' },
            { data: 'ref2', name: 'ref2' },
            { data: 'amount', name: 'amount' },
            { data: 'more', name: 'more', orderable: false }
        ]
    });
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#refNo_Chkbx_1").prop("checked") == true){
        $("#refNo_frm_1").prop('disabled', false);
        $("#refNo_to_1").prop('disabled', false);
    }else{
        $("#refNo_frm_1").prop('disabled', true);
        $("#refNo_to_1").prop('disabled', true);
    }

    if($("#refNo_Chkbx_2").prop("checked") == true){
        $("#refNo_frm_2").prop('disabled', false);
        $("#refNo_to_2").prop('disabled', false);
    }else{
        $("#refNo_frm_2").prop('disabled', true);
        $("#refNo_to_2").prop('disabled', true);
    }

    if($("#DebtorCode_Chkbx").prop("checked") == true){
        $("#DebtorCode_frm").prop('disabled', false);
        $("#DebtorCode_to").prop('disabled', false);
    }else{
        $("#DebtorCode_frm").prop('disabled', true);
        $("#DebtorCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_1").change(function() {
        if(this.checked) {
            $("#refNo_frm_1").prop('disabled', false);
            $("#refNo_to_1").prop('disabled', false);
        }else{
            $("#refNo_frm_1").prop('disabled', true);
            $("#refNo_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#DebtorCode_Chkbx").change(function() {
        if(this.checked) {
            $("#DebtorCode_frm").prop('disabled', false);
            $("#DebtorCode_to").prop('disabled', false);
        }else{
            $("#DebtorCode_frm").prop('disabled', true);
            $("#DebtorCode_to").prop('disabled', true);
        }
    });

}) ()
</script>
@endpush