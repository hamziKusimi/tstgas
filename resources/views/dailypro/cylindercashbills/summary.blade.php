@php ($i = 0) @endphp
<div class="row">
    <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">
        {{ config('config.company.name') }}</p>
    <p style="text-align:center; font-size:16px;margin-top: 0px;">{{ config('config.company.company_no') }}</p>
    <p style="text-align:center; font-size:22px; font-weight:bold;">Transaction Summary - Cashbill</p>
    <br>


</div>
<div class="bg-white">
    <div class="table-responsive">
        <table id="cashbillTab" class="table" cellspacing="0" style="width:100%">
            <thead class="">
                <tr>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">SN</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Doc No</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Date</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Lpo No</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Debtor</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Name</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Quantity</th>
                    <th style="text-align:right; border-top:2px solid;border-bottom:2px solid;">Total Amt</th>
                </tr>
            </thead>

            @if(sizeof($cashbillJSON) > 0)
            <tbody>
                @php
                $qty = 0;
                @endphp
                @foreach ($cashbillJSON as $cashbill)
                {{-- {{ dd($cashbill) }} --}}
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $cashbill->docno}}</td>
                    <td>{{ date('d-m-Y', strtotime($cashbill->date))}}</td>
                    <td>{{ $cashbill->lpono}}</td>
                    <td>{{ $cashbill->debtor}}</td>
                    <td>{{ $cashbill->name}}</td>
                    <td>{{ number_format($cashbill->quantity, 2)}}</td>
                    <td style="text-align:right;">{{ number_format($cashbill->amount, 2) }}</td>
                </tr>
                @php
                $qty = $qty + $cashbill->quantity;
                @endphp
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-top:2px solid;border-bottom:2px solid;">{{ number_format($qty, 2) }}</td>
                    <td style="border-top:2px solid;border-bottom:2px solid;text-align:right;">
                        {{ number_format($totalamount, 2) }}</td>
                </tr>
            </tbody>
            @endif
            {{-- @endif --}}
        </table>
    </div>
    {{-- @if(isset($cashbillJSON))
        {{ $cashbillJSON->links() }}
    @endif --}}
</div>