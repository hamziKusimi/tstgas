@extends('master')

@section('content')

<div class="row">
    <div class="col-2" style="flex: 0 0 13%;padding-right: 5px;">
        <div style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('cylindercashbills.create') }}"> Add Cash Bill
            </a>
        </div>
    </div>
    <div class="col-2" style="padding-left: 0px;">
        <div style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 90%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button customer="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif
@if (Session::has('Error'))
<div class="alert alert-error text-secondary" role="alert">
    <button customer="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Error') }}</p>
</div>
@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="col-md-12">

        <br>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="invTab" class="table table-bordered table-striped" cellspacing="0">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Lpo No</th>
                            <th>D/O No.</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                    @if(sizeof($cashbills) > 0)
                    <tbody>
                        @foreach ($cashbills as $cashbill)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td><a href=" {{ route('cylindercashbills.edit',$cashbill->id) }}">{{ $cashbill->docno }}</a>
                            </td>
                            <td>{{  isset($cashbill->date)?date('d-m-Y', strtotime($cashbill->date)):'' }}</td>
                            <td>{{ $cashbill->account_code}}</td>
                            <td>{{ $cashbill->name }}</td>
                            <td>{{ $cashbill->lpono }}</td>
                            <td>{{ $cashbill->dono }}</td>
                            <td>{{ $cashbill->taxed_amount }}</td>
                            <td>
                                <a href="{{ route('cylindercashbills.destroy', $cashbill->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this record?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@include('dailypro.cylindercashbills.printmodal')
@endsection

@push('scripts')
<script>
    (function() {

    $('#invTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('#datefrmto').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });


    $("#AC_Code_Chkbx").change(function() {
        if(this.checked) {
            $("#AC_Code_frm").prop('disabled', false);
            $("#AC_Code_to").prop('disabled', false);
        }else{
            $("#AC_Code_frm").prop('disabled', true);
            $("#AC_Code_to").prop('disabled', true);
        }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#AC_Code_Chkbx").prop("checked") == true){
        $("#AC_Code_frm").prop('disabled', false);
        $("#AC_Code_to").prop('disabled', false);
    }else{
        $("#AC_Code_frm").prop('disabled', true);
        $("#AC_Code_to").prop('disabled', true);
    }

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#DO_Chkbx_1").prop("checked") == true){
        $("#DO_frm_1").prop('disabled', false);
        $("#DO_to_1").prop('disabled', false);
    }else{
        $("#DO_frm_1").prop('disabled', true);
        $("#DO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#DO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#DO_frm_1").prop('disabled', false);
            $("#DO_to_1").prop('disabled', false);
        }else{
            $("#DO_frm_1").prop('disabled', true);
            $("#DO_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });

}) ()
</script>
@endpush