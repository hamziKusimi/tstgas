<div class="row">
    <div class="col-md-2">
        @if(Auth::user()->hasPermissionTo('INVOICE_CR') || Auth::user()->hasPermissionTo('INVOICE_UP'))
        @include('common.billing.options.submit', [
        'faIcon' => 'share-square-o',
        'color' => 'bg-dark-blue text-white',
        'permissions' => ['Create Invoices', 'Update Invoices'],
        'name' => 'Submit'
        ])
        @endif
    </div>
    <div class="col-md-2" style="padding-left:0px">
        @if (isset($item->id))
        <a href="{{ route('cylindercashbills.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
            class="btn btn-secondary"><i class="fa fa-print pr-2"></i>Print</a>
        @endif
    </div>
    <div class="col-md-2 pr-1">
    </div>
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2">
        <a href="{{ route('cylindercashbills.index') }}" style="width: 100%;" class="btn btn-danger"><i
                class="fa"></i>Back</a>
    </div>
</div>