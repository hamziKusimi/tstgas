<head>
    <link href="{{ asset('css/pdf-reporting.css') }}" rel="stylesheet">
</head>
@php ($i = 0) @endphp

<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Summary - Cash Sales  {{ ($date) }}</p>


</div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%;font-size: 12px;">
                    <thead class="" style="border:5px solid #095484;">
                        <tr>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">SN</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Doc. No.</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Date</p>
                            </th>

                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Debtor</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Name</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Quantity</p>
                            </th>
                            <th style="text-align:right; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Total Amt</p>
                            </th>
                        </tr>
                    </thead>

                @if(sizeof($CashsalesJSON) > 0)
                    <tbody>
                            @php
                            $qty = 0;
                        @endphp
                        @foreach ($CashsalesJSON as $cashsales)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $cashsales->docno}}</td>
                                <td>{{ date('d-m-Y', strtotime($cashsales->date))}}</td>
                               
                                <td>{{ $cashsales->debtor}}</td>
                                <td>{{ $cashsales->name}}</td>
                                <td>{{ number_format($cashsales->quantity, 2)}}</td>
                                <td style="text-align:right;">{{ number_format($cashsales->amount, 2) }}</td>
                            </tr>
                            @php
                            $qty = $qty + $cashsales->quantity;
                        @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                               
                                <td></td>
                                <td></td>
                                <td style="border-top:1px solid;border-bottom:1px solid;">{{ number_format($qty, 2) }}</td>
                                <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">{{ number_format($totalamount, 2) }}</td>
                            </tr>
                        </tbody>
                    @endif
                    {{-- @endif --}}
                </table>
            </div>
            {{-- @if(isset($CashsalesJSON))
                {{ $CashsalesJSON->links() }}
            @endif --}}
        </div>