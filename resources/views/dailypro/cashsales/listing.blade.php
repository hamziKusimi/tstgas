<head>
    <link href="{{ asset('css/pdf-reporting.css') }}" rel="stylesheet">
</head>
<div class="row">
    <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
    @if (config('config.company.show_company_no') == 'true')
    <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
@endif
    <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Listing - Cash Sales  {{ ($date) }}</p>
    <br>
</div>
@php
($i = 0)
@endphp
<div class="card">
<div class="card-body">
    <div class="bg-white">
        <div class="row">
            <table id="cashbillTab" class="table" cellspacing="0" style="width:100%;font-size: 12px;">
                <thead class="" border="1">
                    <tr>
                        <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                            
                        </th>
                        <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                             <p style="margin-top:3px;">Description</p>
                        </th>
                        <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Location</p>
                        </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Quantity</p>
                        </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">UOM</p>
                        </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Unit Price</p>
                        </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Amount</p>
                        </th>
                    </tr>
                </thead>

            @php $docno = "" @endphp
            @if(sizeof($CashsalesJSON) > 0)
                <tbody>
                    @foreach ($CashsalesJSON as $cashsales)
                        <!-- check last updated doc no if not same with new updated then display -->
                        @if($docno != $cashsales->docno)

                            <thead class="">
                                <tr>
                                    <th></th>
                                    <th colspan="6" style="text-align:left;">
                                        <p style="margin-top:3px;">  {{ $cashsales->details }}</p>
                                      </th>
                                </tr>
                            </thead>
                        @endif
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>{{ $cashsales->description}}</td>
                                    <td>{{ $cashsales->location}}</td>
                                    <td style="text-align: right">{{ number_format($cashsales->quantity, 2)}}</td>
                                    <td style="text-align: right">{{ $cashsales->uom}}</td>
                                    <td style="text-align: right">{{ number_format($cashsales->unit_price, 2)}}</td>
                                    <td style="text-align: right">{{ number_format($cashsales->amount, 2)}}</td>
                                </tr>
                            </tbody>
                            <!-- count the row for each doc no -->
                            @php
                                $count = 0;
                                $co = DB::select(DB::raw("
                                            SELECT count(cashsalesdts.doc_no) as count from cashsalesdts where cashsalesdts.doc_no = :docno
                                    "), [
                                        'docno'=>$cashsales->docno,
                                    ]);
                                    foreach($co as $c){
                                        $count = $count + $c->count;
                                    }
                            @endphp

                            @php
                            $qty = 0;
                            $id = '';
                            $inv = DB::select(DB::raw("
                                        SELECT cashsalesdts.id, cashsalesdts.qty from cashsalesdts where cashsalesdts.doc_no = :docno
                                "), [
                                    'docno'=>$cashsales->docno,
                                ]);
                                foreach($inv as $invc){
                                    //get total quantity for each cashbills doc no
                                    $qty = $qty + $invc->qty;
                                    //get last id
                                    $id = $invc->id;
                                }
                            @endphp
                            <!-- check last id then display -->
                            @if($cashsales->id == $id)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="border-top:1px solid;text-align:right;">
                                         <p style="margin-top:3px;">  
                                        {{ number_format($cashsales->quantity, 2) }}
                                        </p>
                                    </td>
                                    <td style="border-top:1px solid;text-align:right;"></td>
                                    <td style="border-top:1px solid;text-align:right;"></td>
                                    <td style="border-top:1px solid;text-align:right;">
                                        <p style="margin-top:3px;">  
                                        {{ number_format($cashsales->totalamt, 2) }}
                                        </p></td>
                                </tr>
                            <!-- else if row have only 1 data also display -->
                            {{-- @elseif($count <= '1')
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="border-top:1px solid;">
                                        <p style="margin-top:3px;">  
                                        {{ number_format($cashsales->quantity, 2) }}
                                        </p>
                                        
                                        </td>
                                    <td style="border-top:1px solid;"></td>
                                    <td style="border-top:1px solid;"></td>
                                    <td style="border-top:1px solid;">
                                        <p style="margin-top:3px;">  
                                        {{ number_format($cashsales->totalamt, 2) }}
                                        </p>
                                        </td>
                                </tr> --}}
                            @endif
                        @php
                            $docno = $cashsales->docno;
                        @endphp
                    @endforeach
                    <!-- Display Grand Total -->
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">Grand Total</td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">{{ number_format($totalamount, 2) }}</td>
                    </tr>
                    </tbody>
                @endif
            </table>
        </div>
    </div>
</div>
</div>


