@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('CASH_SALES_CR'))
    <div class="col-3" style="flex: 0 0 15%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('cashsales.create') }}"> Add Cash Sales</a>
        </div>
    </div>
    @endif
    {{-- @if(Auth::user()->hasPermissionTo('CASH_SALES_PR') && $print->isEmpty()) --}}
    @if(Auth::user()->hasPermissionTo('CASH_SALES_PR'))
    <div class="col-2" style="flex: 0 0 15%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @elseif(Auth::user()->hasPermissionTo('CASH_SALES_PRY'))
    <div class="col-2" style="flex: 0 0 15%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
    <div class="row">
            <div class="col-md-12" style="text-align: right;">
            <form action ="{{ route('cashsales.searchindex') }}" method="POST">
            @csrf
                <input name="search" type="text" id="search" placeholder="Search " class="search"
                data-get-search="{{ route('ajax.index.get.search','search') }}">
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="" class="table table-bordered table-striped" cellspacing="0" style="font-size: 11px;">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Lpo No.</th>
                            <th>D/O No.</th>
                            <th>Quotation No.</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                @if(sizeof($cashsales) > 0)    
                    <tbody>     
                        @foreach ($cashsales as $CS)
                            <tr>
                                <td>{{ ++$i }}</td>
                                @if(Auth::user()->hasPermissionTo('CASH_SALES_UP'))
                                    <td><a href=" {{ route('cashsales.edit',$CS->id) }}">{{ $CS->docno }}</a></td>
                                @else
                                    <td>{{ $CS->docno }}</td>
                                @endif                                
                                <td>{{  isset($CS->date)?date('d-m-Y', strtotime($CS->date)):'' }}</td>
                                <td>{{ $CS->account_code}}</td>
                                <td>{{ $CS->name }}</td>
                                <td>{{ $CS->lpono}}</td>
                                <td>{{ $CS->dono}}</td>
                                <td>{{ $CS->quotno}}</td>
                                <td>{{ $CS->taxed_amount }}</td>
                                @if(Auth::user()->hasPermissionTo('CASH_SALES_DL'))
                                <td>
                                    <a href="{{ route('cashsales.destroy', $CS->id) }}" data-method="delete"
                                        data-confirm="Confirm delete this account?">
                                        <i class="fa fa-trash"></i></a>
                                </td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    @endif
                    {{-- @endif --}}
                </table>
            </div>
            @if(isset($cashsales))
                {{ $cashsales->links() }}
            @endif
        </div>
    </div>
</div>

@include('dailypro.cashsales.printmodal')
{{-- {!! $cashsales->links() !!} --}}

@endsection

@push('scripts')
<script>
    (function() {


        // $('#exampleModal').on('hidden.bs.modal', function() {
        //     // $("#docno_frm").clear();
        //     // $("#docno_to").clear();
        //     // $("#debCode_frm").clear();
        //     // $("#debCode_to").clear();
        // })
        
    // $('#CSTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#LPO_Chkbx_1").prop("checked") == true){
        $("#LPO_frm_1").prop('disabled', false);
        $("#LPO_to_1").prop('disabled', false);
    }else{
        $("#LPO_frm_1").prop('disabled', true);
        $("#LPO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#LPO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#LPO_frm_1").prop('disabled', false);
            $("#LPO_to_1").prop('disabled', false);
        }else{
            $("#LPO_frm_1").prop('disabled', true);
            $("#LPO_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });



}) ()
</script>
@endpush