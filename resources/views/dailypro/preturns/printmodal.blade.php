<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('preturns.print') }}" method="GET" target="_blank">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Purchase Returns Printing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-3">
                            <p><b>Report Type :</b></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            {!! Form::radio('preturn_rcv','listing',null, ['checked'=>'checked','id'
                            =>'listing'])!!}
                            {!! Form::label('preturn_rcv_label', 'Listing', ['class' => 'col-form-label']) !!}
                        </div>
                        <div class="col-2">
                            {!! Form::radio('preturn_rcv','summary',null, [ 'id' =>'summary'])!!}
                            {!! Form::label('preturn_rcv_label', 'Summary', ['class' => 'col-form-label']) !!}
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('dates_Chkbx',null,null, [ 'id' =>'dates_Chkbx'])!!}
                            {!! Form::label('Date', 'Date', ['class' => 'col-form-label']) !!}
                        </div>

                        <label class="col-form-label">:</label>
                        <div class="col-4">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('dates', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'dates'
                                    ])!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('docno_Chkbx',null,null, [ 'id' =>'docno_Chkbx'])!!}
                            {!! Form::label('docno', 'Document No.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                        {{-- {!! Form::select('docno_frm', [], null, ['class' => 'form-control select',
                                        'id'
                                        =>'docno_frm'
                                        ,'disabled' =>'disabled'])!!} --}}
                                        {!! Form::text('docno_frm', null, ['class' => 'form-control docno_frm', 'id'

                                        =>'docno_frm'
        
                                        ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdocno" data-toggle="modal" data-target=".reportdocno-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                    {{-- {!! Form::select('docno_to', $docno_select, null, ['class' => 'form-control select',
                                    'id' =>'docno_to','disabled' =>'disabled'])!!} --}}
                                    {!! Form::text('docno_to', null, ['class' => 'form-control docno_to', 'id'

                                    =>'docno_to'

                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdocnoto" data-toggle="modal" data-target=".reportdocnoto-modal" style="width:35%">
                                        <small style="text-align:center;">
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('refNo_Chkbx_1',null,null, [ 'id' =>'refNo_Chkbx_1'])!!}
                            {!! Form::label('refNo', 'Reference No. 1', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('refNo_frm_1', null, ['class' => 'form-control', 'id'
                                =>'refNo_frm_1'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('refNo_to_1', null, ['class' => 'form-control', 'id'
                                =>'refNo_to_1'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('refNo_Chkbx_2',null,null, [ 'id' =>'refNo_Chkbx_2'])!!}
                            {!! Form::label('refNo', 'Reference No. 2', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('refNo_frm_2', null, ['class' => 'form-control', 'id'
                                =>'refNo_frm_2'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('refNo_to_2', null, ['class' => 'form-control', 'id'
                                =>'refNo_to_2'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('credCode_Chkbx',null,null, [ 'id' =>'credCode_Chkbx'])!!}
                            {!! Form::label('credCode', 'Creditor Code', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                    {{-- {!! Form::select('credCode_frm', $debtor_select, null, ['class' => 'form-control
                                    select credCode_frm', 'id'
                                    =>'credCode_frm'
                                    ,'disabled' =>'disabled'])!!} --}}
                                    {!! Form::text('credCode_frm', null, ['class' => 'form-control credCode_frm', 'id'
                                    =>'credCode_frm'
                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdebtor" data-toggle="modal" data-target=".reportdebtor-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                    {{-- {!! Form::select('credCode_to', $debtor_select, null, ['class' => 'form-control
                                    select credCode_to', 'id'
                                    =>'credCode_to'
                                    ,'disabled' =>'disabled'])!!} --}}
                                    {!! Form::text('credCode_to', null, ['class' => 'form-control credCode_to', 'id'
                                    =>'credCode_to'
                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdebtorto" data-toggle="modal" data-target=".reportdebtorto-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>       
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Print</button>
                </div>
            </form>
        </div>
    </div>
</div>