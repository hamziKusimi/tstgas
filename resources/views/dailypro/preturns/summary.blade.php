
@php ($i = 0) @endphp

<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Summary -  Purchase Return  {{ ($date) }}</p>


</div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%;font-size: 12px;">
                    <thead class="" style="border:5px solid #095484;">
                        <tr>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">SN</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Doc No</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Date</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">refrence 1</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">refrence 2</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Creditor</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Name</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Quantity</th>
                            <th style="text-align:right; border-top:1px solid;border-bottom:1px solid;">Total Amt</th>
                        </tr>
                    </thead>

                @if(sizeof($PreturnsJSON) > 0)
                    <tbody>
                        @php
                                $amount = 0;
                                $qty = 0;
                        @endphp
                        @foreach ($PreturnsJSON as $PR)
                        {{-- {{ dd($PR) }} --}}
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $PR->docno}}</td>
                                <td>{{ date('d-m-Y', strtotime($PR->date))}}</td>
                                <td>{{ $PR->ref1}}</td>
                                <td>{{ $PR->ref2}}</td>
                                <td>{{ $PR->debtor}}</td>
                                <td>{{ $PR->name}}</td>
                                @if($systemsetup == '1')
                                    <td>{{ number_format($PR->totalqty, 2)}}</td>
                                @else
                                    <td>{{ number_format($PR->totalqty)}}</td>
                                @endif
                                <td style="text-align:right;">{{ number_format($PR->totalamount, 2) }}</td>
                            </tr>
                            @php
                                $amount = $amount + $PR->amount;
                                $qty = $qty + $PR->totalqty;
                            @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                @if($systemsetup == '1')
                                <td style="border-top:1px solid;border-bottom:1px solid;">{{ number_format($qty, 2) }}</td>
                                @else
                                <td style="border-top:1px solid;border-bottom:1px solid;">{{ number_format($qty) }}</td>
                                @endif
                                <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">{{ number_format($finaltotalamount, 2) }}</td>
                            </tr>
                        </tbody>
                    @endif
                    {{-- @endif --}}
                </table>
            </div>
            {{-- @if(isset($PreturnsJSON))
                {{ $PreturnsJSON->links() }}
            @endif --}}
        </div>