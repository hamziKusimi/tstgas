
<div class="form-group row">
    <div class="col-md-6">
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="creditor" class="col-md-2">Creditor</label>
            <div class="col-md-4">
                <select class="form-control myselect" name="creditor_id" id="creditor_id" data-values="{{ $creditors }}">
                    <option value=""></option>
                    @foreach($creditors as $creditor)
                        <option value="{{$creditor->id}}" {{ isset($preturn)?$preturn->creditor_id == $creditor->id  ? 'selected' : '' : ''}}>{{$creditor->accountcode}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <input name="creditordet" id="creditordet" type="text" class="form-control name" value="{{ isset($preturn)?$preturn->creditordet:'' }}" >
            </div>
        </div>

        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="address" class="col-md-2">Address</label>
            <div class="col-md-10">
                <input name="address" type="text" class="form-control address" value="{{ isset($preturn)?$preturn->address:'' }}" >
            </div>
        </div>
    
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-fax"></i></small>
                    </div>
                    <input name="fax" type="text" class="form-control fax" value="{{ isset($preturn)?$preturn->fax:'' }}" >
                </div>
            </div>

            <div class="col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-mobile-phone"></i></small>
                    </div>
                    <input name="hp" type="text" class="form-control hp" value="{{ isset($preturn)?$preturn->hp:'' }}" >
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-phone"></i></small>
                    </div>
                    <input name="tel" type="text" class="form-control tel" value="{{ isset($preturn)?$preturn->tel:'' }}" >
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="docno" class="col-md-3">Document No</label>
                <div class="col-md-9">
                    {{-- <input name="docno" type="text" class="form-control" value="{{ isset($preturn)?$preturn->docno:$runningNumber->nextRunningNoString }}" > --}}
                    <input name="docno" type="text" class="form-control" value="{{ isset($preturn)?$preturn->docno: $runningNumber->nextRunningNoString }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="date" class="col-md-3">Date</label>
                <div class="col-md-9">   
                    <div class="input-group-prepend">
                        <input type="text" name="date" id="datepicker" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($preturn->date)?date('d-m-Y', strtotime($preturn->date)):'' }}">
                            <div class="input-group-text bg-dark-blue text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="ref1" class="col-md-3">Ref No 1</label>
                <div class="col-md-9">
                    <input name="ref1" type="text" class="form-control" value="{{ isset($preturn)?$preturn->ref1:'' }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="ref2" class="col-md-3">Ref No 2</label>
                <div class="col-md-9">
                    <input name="ref2" type="text" class="form-control" value="{{ isset($preturn)?$preturn->ref2:'' }}" >
                </div>
            </div>
        </div>
@php($true='false')
</div>
<div class="form-group row" style="margin: 0px;padding: 0px">
    <div class="col-md-12">
        <div class="table cell-border stripe table table-sm">
            <table class="table cell-border stripe" id="dataTable">
                <thead style="background-color:#3c8dbc; color:white;">
                    <tr class="text-sm">
                        <th width="9%" class="text-center">SN</th>
                        <th width="12%" class="text-center">Stock Code</th>
                        <th width="25%" class="text-left">Description</th>
                        <th width="8%" class="text-left">Qty</th>
                        <th width="8%" class="text-left">UOM</th>
                        <th width="8%" class="text-left">Rate</th>
                        <th width="8%" class="text-left">Unit Cost</th>
                        <th width="8%" class="text-left">Amount</th>
                        <th width="12%" class="text-left">Reason</th>
                        <th width="12%" class="text-left">GR</th>
                        <th width="1%" class="text-left">More</th>
                    </tr>
                </thead>
                
                <tbody class="table-form-tbody">
                    @include('dailypro/preturns/preturntable') 
                    @includeWhen(isset($preturndts), 'dailypro/preturns/preturntabledata')
                </tbody>
                
                <tfoot>
                    <tr>
                        <td colspan="12" style="padding:1px;">
                            <button type="button" class="btn" style="background-color: #4aa94a; color:white" id="add-new">
                                Add Line
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

