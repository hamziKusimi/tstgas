<div class="row">
    @isset($edit)
    <div class="col-md-2" style="padding-right:0px">
        <a href="{{ route('goods.create') }}" style="width: 100%;"
            class="btn form-control bg-dark-green text-white text-center btn-form-submit">New</a>
    </div>
    @endisset
    @if(Auth::user()->hasPermissionTo('GOOD_RC_CR') || Auth::user()->hasPermissionTo('GOOD_RC_UP'))
    <div class="col-md-2" style="padding-right:0px;{{isset($edit) ? "padding-left:0px":null}}">
        @include('common.billing.options.submit', [
        'faIcon' => 'share-square-o',
        'color' => 'bg-dark-blue text-white',
        'permissions' => ['Create Goods', 'Update Goods'],
        'name' => 'Submit'
        ])
    </div>
    @else

    @endif
    @if(Auth::user()->hasPermissionTo('GOOD_RC_PR') && (isset($item) && $item->printed_by == null))
    <div class="col-md-2" style="padding-right:0px;padding-left:0px">
        @if (isset($item->id))
        <a href="{{ route('goods.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
            class="btn form-control bg-secondary text-white text-center btn-form-submit"><i
                class="fa fa-print pr-2"></i>Print</a>
        @endif
    </div>
    @elseif(Auth::user()->hasPermissionTo('GOOD_RC_PRY'))
    <div class="col-md-2" style="padding-right:0px;padding-left:0px">
        @if (isset($item->id))
        <a href="{{ route('goods.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
            class="btn form-control bg-secondary text-white text-center btn-form-submit"><i
                class="fa fa-print pr-2"></i>Print</a>
        @endif
    </div>
    @else
    @endif
    @if(isset($edit))
    <div class="col-md-2" style="padding-left:0px">
        <a href="{{ route('goods.destroy', $item->id) }}" data-method="delete" style="width: 100%;"
            data-confirm="Confirm delete this account?" class="btn form-control bg-danger text-white text-center btn-form-submit">
            <i class="fa fa-trash pr-2"></i>Delete</a>
    </div>
    @else
        <div class="col-md-2 pl-1 pr-1"></div>
    @endif
    @if (!isset($edit))
    <div class="col-md-2" style="padding-right:0px"></div>
    @endif
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2">
        <a href="{{ route('goods.index') }}" style="width: 100%;" class="btn btn-danger"><i class="fa"></i>Back</a>
    </div>
</div>