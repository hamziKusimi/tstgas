
<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="currency" class="col-md-3"> Currency </label>
    <div class="col-md-9">
        <input name="currency" type="text" class="form-control" value="{{ isset($good)?$good->currency:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="currate" class="col-md-3"> Currency Rate </label>
    <div class="col-md-9">
        <input name="currate" type="text" class="form-control" value="{{ isset($good)?$good->currate:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="packcg" class="col-md-3"> Packing Charges </label>
    <div class="col-md-9">
        <input name="packcg" type="text" class="form-control" value="{{ isset($good)?$good->packcg:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="handlingcg" class="col-md-3"> Handling Charges </label>
    <div class="col-md-9">
        <input name="handlingcg" type="text" class="form-control" value="{{ isset($good)?$good->handlingcg:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="fcg" class="col-md-3"> F.Charges </label>
    <div class="col-md-9">
        <input name="fcg" type="text" class="form-control" value="{{ isset($good)?$good->fcg:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="insurance" class="col-md-3"> Insurance </label>
    <div class="col-md-9">
        <input name="insurance" type="text" class="form-control" value="{{ isset($good)?$good->insurance:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="forwarding" class="col-md-3"> Forwarding </label>
    <div class="col-md-9">
        <input name="forwarding" type="text" class="form-control" value="{{ isset($good)?$good->forwarding:'' }}" >
    </div>
</div>
