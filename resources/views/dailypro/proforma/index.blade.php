@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('PROFORMA_IV_CR'))
    <div class="col-3" style="flex: 0 0 15%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('proformainvoices.create') }}"> Add Pro Forma </a>
        </div>
    </div>
    @endif
    @if(Auth::user()->hasPermissionTo('PROFORMA_IV_PR') && $print->isEmpty())
        <div class="col-2" style="flex: 0 0 15%;padding-right: 5px;">
            <div  style="margin-bottom: 10px;">
                <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                    data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
            </div>
        </div>
    @elseif(Auth::user()->hasPermissionTo('PROFORMA_IV_PRY'))
        <div class="col-2" style="flex: 0 0 15%;padding-right: 5px;">
            <div  style="margin-bottom: 10px;">
                <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                    data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
            </div>
        </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
    <div class="row">
        @if(isset($proformasearch))
            <div class="col-md-2">
                <a href="{{ route('proformainvoices.index') }}" style="width: 100%;"
                        class="btn btn-danger"><i class="fa"></i>Back</a>
            </div>
            <div class="col-md-10" style="text-align: right;">
        @else
            <div class="col-md-12" style="text-align: right;">
        @endif
            <form action ="{{ route('proformainvoices.searchindex') }}" method="POST">
            @csrf
                <input name="search" type="text" id="search" placeholder="Search " class="search"
                data-get-search="{{ route('ajax.index.get.search','search') }}">
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="invTab" class="table table-bordered table-striped" cellspacing="0" ">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Terms</th>
                            <th>D/O No.</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                {{-- @if(sizeof($proforma) > 0)     --}}
                    <tbody> 
                    @if(isset($proformasearch))
                        @foreach ($proformasearch as $proforma)
                        <tr>
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('PROFORMA_IV_UP'))
                                <td><a href=" {{ route('proformainvoices.edit',$proforma->id) }}">{{ $proforma->docno }}</a></td>
                            @else
                                <td>{{ $proforma->docno }}</td>
                            @endif
                            <td>{{ isset($proforma->date)?date('d-m-Y', strtotime($proforma->date)):'' }}</td>
                            <td>{{ $proforma->account_code}}</td>
                            <td>{{ $proforma->name }}</td>
                            <td>{{ $proforma->credit_term }}</td>
                            <td>{{ $proforma->do_no }}</td>
                            <td>{{ $proforma->taxed_amount }}</td>
                            @if(Auth::user()->hasPermissionTo('PROFORMA_IV_DL'))
                            <td>
                                <a href="{{ route('proformainvoices.destroy', $proforma->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    @else
                        @foreach ($proformas as $proforma)
                        <tr>
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('PROFORMA_IV_UP'))
                                <td><a href=" {{ route('proformainvoices.edit',$proforma->id) }}">{{ $proforma->docno }}</a></td>
                            @else
                                <td>{{ $proforma->docno }}</td>
                            @endif
                            <td>{{ isset($proforma->date)?date('d-m-Y', strtotime($proforma->date)):'' }}</td>
                            <td>{{ $proforma->account_code}}</td>
                            <td>{{ $proforma->name }}</td>
                            <td>{{ $proforma->credit_term }}</td>
                            <td>{{ $proforma->do_no }}</td>
                            <td>{{ $proforma->taxed_amount }}</td>
                            @if(Auth::user()->hasPermissionTo('PROFORMA_IV_DL'))
                            <td>
                                <a href="{{ route('proformainvoices.destroy', $proforma->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                    {{-- @endif --}}
                </table>
            </div>
            @if(isset($proformas))
                {{ $proformas->links() }}
            @endif
        </div>
    </div>
</div>
@include('dailypro.proforma.printmodal')
@endsection

@push('scripts')
<script>
    (function() {
        
    // $('#invTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#DO_Chkbx_1").prop("checked") == true){
        $("#DO_frm_1").prop('disabled', false);
        $("#DO_to_1").prop('disabled', false);
    }else{
        $("#DO_frm_1").prop('disabled', true);
        $("#DO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#DO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#DO_frm_1").prop('disabled', false);
            $("#DO_to_1").prop('disabled', false);
        }else{
            $("#DO_frm_1").prop('disabled', true);
            $("#DO_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });

}) ()
</script>
@endpush