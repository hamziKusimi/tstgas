<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <style type="text/css" media="screen,print">
        @page {
            margin: 100px 25px;
        }

        .new-page {
            page-break-before: always;
        }

        #spacer {height: 2em;} /* height of footer + a little extra */
        #footer {
            margin-bottom: 0;
            /* position: fixed; */
            bottom: 0;
        }

        table {
            font-size: 14px;
        }

        .last {
            position: absolute;
            bottom: -20px;
            left: 0px;
            right: 0px;
            height: 100px;
            background-color: green;
        }
    </style>
    {{-- <link rel="stylesheet" href="{{ asset('css/bootstrap-3.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/pdf.css') }}">
</head>

@php ($i = 0) @endphp
<body>

    <table class="table" cellspacing="0" width="100%" height="100%" autosize="1">
        <thead>
            <tr>
                <h2 style="text-align:center; font-weight:bold;margin: 0px;">{{ config('config.company.name') }}</h2>
                <p style="text-align:center;margin: 0px;">{{ config('config.company.company_no') }}
                @if(isset($sytemsetups))
                    <p style="text-align:center;margin: 0px;">{{ $sytemsetups->address1 }}
                    <p style="text-align:center;margin: 0px;">{{ $sytemsetups->address2 }}
                    <p style="text-align:center;">Tel: {{ $sytemsetups->tel }} Fax: {{ $sytemsetups->fax }}
                    <p style="text-align:center;margin: 0px;">{{ $sytemsetups->email }}
                @endif
                <h3 style="text-align: center;text-decoration: underline;">PRO-FORMA TAX INVOICE</h3>
            </tr>
            @if(isset($items))
                <tr>
                    <td width="60%">
                        {{ $items->name }}
                    </td>
                    <td width="20%" style="text-align:left;">
                            Invoice No
                    </td>
                    <td width="25%" style="text-align:left; ">
                        {{ $items->docno }}
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        {{ $items->addr1 }} {{ $items->addr2 }}
                        @if(($items->addr3) || ($items->addr4))
                            {{ $items->addr3 }} {{ $items->addr4 }}
                        
                        @endif
                    </td>
                    <td width="20%" style="text-align:left;">
                            Date
                    </td>
                    <td width="25%" style="text-align:left; ">
                        {{ $items->date }}</p
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        TEL: {{ $items->tel_no }} H/P NO: 
                    </td>
                    <td width="20%" style="text-align:left;">
                            Your Reference No
                    </td>
                    <td width="25%" style="text-align:left; ">
                        {{ $items->reference_no }}<br>
                    </td>
                </tr>
                <tr>
                    <td width="60%">
                        FAX: {{ $items->fax_no }}
                    </td>
                    <td width="20%" style="text-align:left;">
                            Terms
                    </td>
                    <td width="25%" style="text-align:left; ">
                        {{ $items->credit_term }}<br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p></p>
                    </td>
                </tr>
            @endif  
        </thead>
        <tbody>
            <tr>
                <table style="" class="table" cellspacing="0" width="100%" autosize="1">
                    <thead>
                        <th style="border:1px solid">ITEM NO</th>  
                        <th style="border:1px solid;border-left:none;">DESCRIPTION</th>
                        <th style="border:1px solid;border-left:none;">QUANTITY</th>
                        <th style="border:1px solid;border-left:none;">UOM</th>
                        <th style="border:1px solid;border-left:none;">UNIT PRICE</th>
                        <th width="12%" style="border:1px solid;border-left:none;">AMOUNT</th>
                    </thead>
                    <tbody>
                        @foreach ($contents as $content)   
                            <tr>
                                <td style="text-align:center;border:none;border-right:1px solid;border-left:1px solid;">{{ ++$i }}</td> 
                                <td style="border:none;border-right:1px solid;">{{ $content->subject }}</td>
                                <td style="text-align:center;border:none;border-right:1px solid;">{{ $content->quantity }}</td>
                                <td style="text-align:center;border:none;border-right:1px solid;">{{ $content->uom }}</td>
                                <td style="border:none;border-right:1px solid;">{{ $content->unit_price }}</td>
                                <td style="border:none;border-right:1px solid;">{{ $content->amount }}</td>                                
                            </tr>
                        @endforeach
                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" style="border:1px solid">
                                E. & O.E. <br>
                                No complaint will be considered unless notified within 10 days of this invoice date <br>
                                All cheques should be crossed A/C payee only.
                            </td>
                            <td colspan="2" style="border:1px solid;border-left:none;">
                                Sub-Total <br>
                                Discount <br>
                                Tax (6% GST) <br>
                                Total <br>
                            </td>
                            @if(isset($items))
                            <td style="margin: 0px;padding:0px;border:1px solid;border-left:none;">
                                RM {{ $items->amount }}<br>
                                {{ $items->discount }}<br>
                                {{ $items->tax_amount }}<br>
                                RM {{ $items->taxed_amount }}<br>
                            </td>
                            @endif 
                        </tr>
                    </tfoot>
                </table>
            </tr>
        </tbody> 
        <tfoot style="font-size: 11px;" >
            <table class="table" cellspacing="0" width="100%" height="100%" autosize="1"> 
                <tr>
                    <td colspan="3" valign="bottom">
                        <p></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="bottom">
                        <p></p>
                    </td>
                </tr>
                <tr>
                    <td width="25%" valign="bottom">
                        <p></p>
                    </td>
                    <td width="50%" valign="bottom">
                        <p></p>
                    </td>
                    <td width="25%" valign="bottom">
                        <p style="text-align:center;font-size: 10px;">  for {{  config('config.company.name') }} <p>
                    </td>
                </tr>
                <tr>
                    <td width="25%" valign="bottom">
                        <p style="text-align:left;"> 
                            .................................................................. </p>
                    </td>
                    <td width="50%" valign="bottom">
                        <p></p>
                    </td>
                    <td width="25%" valign="bottom">
                        <p style="text-align:right;"> .................................................................. </p>
                    </td>
                </tr>
                <tr>
                    <td width="25%" valign="bottom">
                        <p style="text-align:center;font-size: 10px;"> Company`s Chop & Authorised Signature </p>
                    </td>
                    <td width="50%" valign="bottom">
                        <p></p>
                    </td>
                    <td width="25%" valign="bottom">
                        <p></p>
                    </td>
                </tr>
                <tr>
                    <td id="spacer"></td>
                </tr>
            </table>
        </tfoot>
    </table>
    <div id="footer">
    </div>



</body>