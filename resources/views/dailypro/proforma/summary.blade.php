@php ($i = 0) @endphp
<div class="row">
    <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">
        {{ config('config.company.name') }}</p>
    <p style="text-align:center; font-size:16px;margin-top: 0px;">{{ config('config.company.company_no') }}</p>
    <p style="text-align:center; font-size:22px; font-weight:bold;">Transaction Summary - Proforma</p>
    <br>


</div>
<div class="bg-white">
    <div class="table-responsive">
        <table id="invoiceTab" class="table" cellspacing="0" style="width:100%">
            <thead class="">
                <tr>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">SN</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Doc No</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Date</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Do No</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Debtor</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Name</th>
                    <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Quantity</th>
                    <th style="text-align:right; border-top:2px solid;border-bottom:2px solid;">Total Amt</th>
                </tr>
            </thead>

            @if(sizeof($invoiceJSON) > 0)
            <tbody>
                @php
                $qty = 0;
                @endphp
                @foreach ($invoiceJSON as $invoice)
                {{-- {{ dd($invoice) }} --}}
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $invoice->docno}}</td>
                    <td>{{ date('d-m-Y', strtotime($invoice->date))}}</td>
                    <td>{{ $invoice->do_no}}</td>
                    <td>{{ $invoice->debtor}}</td>
                    <td>{{ $invoice->name}}</td>
                    <td>{{ number_format($invoice->quantity, 2)}}</td>
                    <td style="text-align:right;">{{ number_format($invoice->amount, 2) }}</td>
                </tr>
                @php
                $qty = $qty + $invoice->quantity;
                @endphp
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-top:2px solid;border-bottom:2px solid;">{{ number_format($qty, 2) }}</td>
                    <td style="border-top:2px solid;border-bottom:2px solid;text-align:right;">
                        {{ number_format($totalamount, 2) }}</td>
                </tr>
            </tbody>
            @endif
            {{-- @endif --}}
        </table>
    </div>
    {{-- @if(isset($invoiceJSON))
        {{ $invoiceJSON->links() }}
    @endif --}}
</div>