<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Listing - Delivery Order  {{ ($date) }}</p>


    </div>
    @php
    ($i = 0)
    @endphp
    <div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="row">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%;font-size: 12px;">
                    <thead class="" border="1">
                        <tr>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">SN</th>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">Description</th>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">Location</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">Quantity</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">U.O.M</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">Unit Price</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">Amount</th>
                        </tr>
                    </thead>

                @php $docno = "" @endphp
                @if(sizeof($DeliveryOrderJSON) > 0)
                    <tbody>
                        @foreach ($DeliveryOrderJSON as $DO)
                            <!-- check last updated doc no if not same with new updated then display -->
                            @if($docno != $DO->docno)

                                <thead class="">
                                    <tr>
                                        <th></th>
                                        <th colspan="6" style="text-align:left;">{{ $DO->details }}</th>
                                    </tr>
                                </thead>
                            @endif
                                <tbody>
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $DO->description}}</td>
                                        <td>{{ $DO->location}}</td>
                                        <td style="text-align: right">{{ number_format($DO->quantity, 2)}}</td>
                                        <td style="text-align: right">{{ $DO->uom}}</td>
                                        <td style="text-align: right">{{ number_format($DO->uprice, 2)}}</td>
                                        <td style="text-align: right">{{ number_format($DO->amount, 2)}}</td>
                                    </tr>
                                </tbody>
                                <!-- count the row for each doc no -->
                                @php
                                    $count = 0;
                                    $co = DB::select(DB::raw("
                                                SELECT count(deliveryorderdts.doc_no) as count from deliveryorderdts where deliveryorderdts.doc_no = :docno
                                        "), [
                                            'docno'=>$DO->docno,
                                        ]);
                                        foreach($co as $c){
                                            $count = $count + $c->count;
                                        }
                                @endphp

                                @php
                                $qty = 0;
                                $id = '';
                                $inv = DB::select(DB::raw("
                                            SELECT deliveryorderdts.id, deliveryorderdts.qty from deliveryorderdts where deliveryorderdts.doc_no = :docno
                                    "), [
                                        'docno'=>$DO->docno,
                                    ]);
                                    foreach($inv as $invc){
                                        //get total quantity for each cashbills doc no
                                        $qty = $qty + $invc->qty;
                                        //get last id
                                        $id = $invc->id;
                                    }
                                @endphp
                                <!-- check last id then display -->
                                @if($DO->id == $id)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-top:1px solid;text-align:right;">{{ number_format($qty, 2) }}</td>
                                        <td style="border-top:1px solid;text-align:right;"></td>
                                        <td style="border-top:1px solid;text-align:right;"></td>
                                        <td style="border-top:1px solid;text-align:right;">{{ number_format($DO->totalamt, 2) }}</td>
                                    </tr>
                                <!-- else if row have only 1 data also display -->
                                {{-- @elseif($count <= '1')
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-top:1px solid;">{{ number_format($DO->quantity, 2) }}</td>
                                        <td style="border-top:1px solid;"></td>
                                        <td style="border-top:1px solid;"></td>
                                        <td style="border-top:1px solid;">{{ number_format($DO->totalamt, 2) }}</td>
                                    </tr> --}}
                                @endif
                            @php
                                $docno = $DO->docno;
                            @endphp
                        @endforeach
                        <!-- Display Grand Total -->
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">Grand Total</td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">{{ number_format($totalamount, 2) }}</td>
                        </tr>
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
    </div>


