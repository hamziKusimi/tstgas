
@php ($i = 0) @endphp

<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Summary -  Delivery Order  {{ ($date) }}</p>


</div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%;font-size: 12px;">
                    <thead class="" style="border:5px solid #095484;">
                        <tr>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">SN</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Doc No</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Date</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Reference 1</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Cashbill / Invoice No.</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Creditor</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Name</th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">Quantity</th>
                            <th style="text-align:right; border-top:1px solid;border-bottom:1px solid;">Total Amt</th>
                        </tr>
                    </thead>

                @if(sizeof($DeliveryOrderJSON) > 0)
                    <tbody>
                        @php
                                $amount = 0;
                                $qty = 0;
                        @endphp
                        @foreach ($DeliveryOrderJSON as $DO)
                        {{-- {{ dd($DO) }} --}}
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $DO->docno}}</td>
                                <td>{{ date('d-m-Y', strtotime($DO->date))}}</td>
                                <td>{{ $DO->ref1}}</td>
                                <td>{{ $DO->cbinvno}}</td>
                                <td>{{ $DO->debtor}}</td>
                                <td>{{ $DO->name}}</td>
                                <td>{{ number_format($DO->quantity, 2)}}</td>
                                <td style="text-align:right;">{{ number_format($DO->amount, 2) }}</td>
                            </tr>
                            @php
                                $amount = $amount + $DO->amount;
                                $qty = $qty + $DO->quantity;
                            @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="border-top:1px solid;border-bottom:1px solid;">{{ number_format($qty, 2) }}</td>
                                <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">{{ number_format($amount, 2) }}</td>
                            </tr>
                        </tbody>
                    @endif
                    {{-- @endif --}}
                </table>
            </div>
            {{-- @if(isset($DeliveryOrderJSON))
                {{ $DeliveryOrderJSON->links() }}
            @endif --}}
        </div>