
<div class="form-group row">
    <div class="col-md-6">
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="debtor" class="col-md-2">Debtor</label>
            <div class="col-md-4">
                <select class="form-control myselect" name="debtor_id" id="debtor_id" data-values="{{ $debtors }}">
                    <option value=""></option>
                    @foreach($debtors as $debtor)
                        <option value="{{$debtor->id}}" {{ isset($deliveryorder)?$deliveryorder->debtor_id == $debtor->id  ? 'selected' : '' : ''}}>{{$debtor->accountcode}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <input name="debtordet" id="debtordet" type="text" class="form-control name" value="{{ isset($deliveryorder)?$deliveryorder->debtordet:'' }}" >
            </div>
        </div>

        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="address" class="col-md-2">Address</label>
            <div class="col-md-10">
                <input name="address" type="text" class="form-control address" value="{{ isset($deliveryorder)?$deliveryorder->address:'' }}" >
            </div>
        </div>
    
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-fax"></i></small>
                    </div>
                    <input name="fax" type="text" class="form-control fax" value="{{ isset($deliveryorder)?$deliveryorder->fax:'' }}" >
                </div>
            </div>

            <div class="col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-mobile-phone"></i></small>
                    </div>
                    <input name="hp" type="text" class="form-control hp" value="{{ isset($deliveryorder)?$deliveryorder->hp:'' }}" >
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-phone"></i></small>
                    </div>
                    <input name="tel" type="text" class="form-control tel" value="{{ isset($deliveryorder)?$deliveryorder->tel:'' }}" >
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="docno" class="col-md-3">Document No</label>
                <div class="col-md-9">
                    {{-- <input name="docno" type="text" class="form-control" value="{{ isset($deliveryorder)?$deliveryorder->docno:$runningNumber->nextRunningNoString }}" > --}}
                    <input name="docno" type="text" class="form-control" value="{{ isset($deliveryorder)?$deliveryorder->docno: $runningNumber->nextRunningNoString }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="date" class="col-md-3">Date</label>
                <div class="col-md-9">   
                    <div class="input-group-prepend">
                        <input type="text" name="date" id="datepicker" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($deliveryorder->date)?date('d-m-Y', strtotime($deliveryorder->date)):'' }}">
                            <div class="input-group-text bg-dark-blue text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="lpono" class="col-md-3">LPO No</label>
                <div class="col-md-9">
                    <input name="lpono" type="text" class="form-control" value="{{ isset($deliveryorder)?$deliveryorder->lpono:'' }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="ref1" class="col-md-3">Ref No 1 </label>
                <div class="col-md-9">
                    <input name="ref1" type="text" class="form-control" value="{{ isset($deliveryorder)?$deliveryorder->ref1:'' }}" >
                </div>
            </div>

            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="cbinvno" class="col-md-3">CB/Inv No</label>
                <div class="col-md-9">
                    <input name="cbinvno" type="text" class="form-control" value="{{ isset($deliveryorder)?$deliveryorder->cbinvno:'' }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="quotno" class="col-md-3">Quotation No</label>
                <div class="col-md-9">
                    <input name="quotno" type="text" class="form-control" value="{{ isset($deliveryorder)?$deliveryorder->quotno:'' }}" >
                </div>
            </div>
        </div>
@php($true='false')
</div>
<div class="form-group row" style="margin: 0px;padding: 0px">
    <div class="col-md-12">
        <div class="table cell-border stripe table table-sm">
            <table class="table cell-border stripe" id="dataTable">
                <thead style="background-color:#3c8dbc; color:white;">
                    <tr class="text-sm">
                        <th width="9%" class="text-center">SN</th>
                        <th width="12%" class="text-center">Stock Code</th>
                        <th width="25%" class="text-left">Description</th>
                        <th width="8%" class="text-left">Qty</th>
                        <th width="8%" class="text-left">UOM</th>
                        <th width="8%" class="text-left">Rate</th>
                        <th width="8%" class="text-left">Unit Price</th>
                        <th width="8%" class="text-left">TY</th>
                        <th width="7.5%" class="text-left">Discount</th>
                        <th width="12%" class="text-left">Amount</th>
                        <th width="1%" class="text-left">More</th>
                    </tr>
                </thead>
                
                <tbody class="table-form-tbody">
                    @include('dailypro/deliveryorders/deliveryordertable') 
                    @includeWhen(isset($deliveryorderdts), 'dailypro/deliveryorders/deliveryordertabledata')
                </tbody>
                
                <tfoot>
                    <tr>
                        <td colspan="12" style="padding:1px;">
                            <button type="button" class="btn" style="background-color: #4aa94a; color:white" id="add-new">
                                Add Line
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

