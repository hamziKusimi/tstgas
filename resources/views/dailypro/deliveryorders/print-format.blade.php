<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Print Option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="print_form" action="{{ route('deliveryorders.jasper', ['id' => $item->id])  }}" method="GET"
                target="_blank">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            {!! Form::radio('Print_radio','with_amount',null, [ 'id' =>'Print_radio', 'checked'
                            =>'checked']
                            )!!}
                            {!! Form::label('Print', 'With Amount', ['class' => 'col-form-label']) !!}
                        </div>
                        <div class="col-4">
                            {!! Form::radio('Print_radio', 'Without_amount',null, [ 'id' =>'Print_radio'])!!}
                            {!! Form::label('ValPrintManu', 'Without Amount', ['class' => 'col-form-label']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="print_form" type="submit" class="btn btn-primary">Print</button>
                </div>
            </form>
        </div>
    </div>
</div>