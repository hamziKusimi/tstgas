<div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3">
        <a class="btn bg-dark-blue text-white form-control text-center" href="{{ route('invoices.create') }}">
            <i class="fa fa-plus pr-2"></i> Create
        </a>
    </div>
</div>
</br>