<head>
    <link href="{{ asset('css/pdf-reporting.css') }}" rel="stylesheet">
</head>
@php
($i = 0)
@endphp
<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Listing - Invoice {{ $date }}</p>


</div>
<div class="card">
<div class="card-body">
    <div class="bg-white">
        <div class="row">
            <table id="invoiceTab" class="table" cellspacing="0" style="width:100%;font-size:12px;">
                <thead class="" border="1">
                    <tr>
                       <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;"></p>
                        </th>
                        <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Description</p>
                        </th>
                        <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Location</p>
                        </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Quantity</p>
                        </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">UOM</p>
                        </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Unit Price</p>
                             </th>
                        <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                            <p style="margin-top:3px;">Amount</p>
                        </th>
                    </tr>
                </thead>

                @php $docno = '' @endphp
            @if(sizeof($invoiceJSON) > 0)
                <tbody>
                    @foreach ($invoiceJSON as $invoice)
                    <!-- check last updated doc no if not same with new updated then display -->
                    @if($docno != $invoice->docno)

                        <thead class="" style="font-size: 12px;">
                            <tr>
                                <th></th>
                                <th colspan="6" style="text-align:left;">
                                    <p style="margin-top:3px;">{{ $invoice->details }}</p></th>
                            </tr>
                        </thead>
                    @endif
                        <tbody>
                            <tr>
                                <td>{{ ""}}</td>
                                <td>{{ $invoice->description}}</td>
                                <td>{{ $invoice->location}}</td>
                                @if($systemsetup == '1')
                                <td style="text-align:right;">{{ number_format($invoice->quantity, 2)}}</td>
                                @else
                                <td style="text-align:right;">{{ number_format($invoice->quantity)}}</td>
                                @endif
                                <td style="text-align:right;">{{ $invoice->uom}}</td>
                                <td style="text-align:right;">{{ number_format($invoice->unit_price, 2)}}</td>
                                <td style="text-align:right;">{{ number_format($invoice->amount, 2)}}</td>
                            </tr>
                        </tbody>

                    @php
                        $count = 0;
                        $co = DB::select(DB::raw("
                                    SELECT count(invoice_data.doc_no) as count from invoice_data where invoice_data.doc_no = :docno and invoice_data.deleted_at IS NULL
                            "), [
                                'docno'=>$invoice->docno,
                            ]);
                            foreach($co as $c){
                                $count = $count + $c->count;
                            }
                    @endphp
                    @php
                    $qty = 0;
                    $id = '';
                    $inv = DB::select(DB::raw("
                                SELECT invoice_data.id, invoice_data.quantity from invoice_data where invoice_data.doc_no = :docno and invoice_data.deleted_at IS NULL GROUP BY invoice_data.doc_no, invoice_data.subject, invoice_data.item_code
                        "), [
                            'docno'=>$invoice->docno,
                        ]);
                        foreach($inv as $invc){
                            //get total quantity for each cashbills doc no
                            $qty = $qty + $invc->quantity;
                            //get last id
                            $id = $invc->id;
                        }
                    @endphp
                    <!-- check last id then display -->
                    @if($invoice->id == $id)
                        <tr style="border: 1px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            @if($systemsetup == '1')
                            <td style="border-top:1px solid;text-align:right;">{{ number_format($qty, 2) }}</td>
                            @else
                            <td style="border-top:1px solid;text-align:right;">{{ number_format($qty) }}</td>
                            @endif
                            <td style="border-top:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;text-align:right;">{{ number_format($invoice->totalamt, 2) }}</td>
                        </tr>
                    {{-- @elseif($count <= '1')

                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-top:1px solid;">{{ number_format($invoice->quantity, 2) }}</td>
                            <td style="border-top:1px solid;"></td>
                            <td style="border-top:1px solid;"></td>
                            <td style="border-top:1px solid;">{{ number_format($invoice->totalamt, 2) }}</td>
                        </tr> --}}
                    @endif

                    @php
                        $docno = $invoice->docno
                    @endphp
                @endforeach
                <!-- Display Grand Total -->
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">Grand Total</td>
                        <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">{{ number_format($finaltotalamount, 2) }}</td>
                    </tr>
                </tbody>
                @endif
                {{-- @endif --}}
            </table>
        </div>
    </div>
</div>
</div>



