<div class="row">
    @isset($edit)
    <div class="col-md-2" style="padding-right:0px">
        <a href="{{ route('invoices.create') }}" style="width: 100%;"
            class="btn form-control bg-dark-green text-white text-center btn-form-submit">New</a>
    </div>
    @endisset
    <div class="col-md-2" style="padding-right:0px;{{isset($edit) ? "padding-left:0px":null}}">
        @if(Auth::user()->hasPermissionTo('INVOICE_CR') || Auth::user()->hasPermissionTo('INVOICE_UP'))
        @include('common.billing.options.submit', [
        'faIcon' => 'share-square-o',
        'color' => 'bg-dark-blue text-white',
        'permissions' => ['Create Invoices', 'Update Invoices'],
        'name' => 'Submit'
        ])
        @endif
    </div>
    <div class="col-md-2" style="padding-right:0px;padding-left:0px">
        @if(Auth::user()->hasPermissionTo('INVOICE_PR') && (isset($item) && $item->printed_by == null))
        @if (isset($item->id))
        <a href="{{ route('invoices.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
            class="btn form-control bg-secondary text-white text-center btn-form-submit"><i
                class="fa fa-print pr-2"></i>Print</a>
        @endif
        @elseif(Auth::user()->hasPermissionTo('INVOICE_PRY'))
        @if (isset($item->id))
        <a href="{{ route('invoices.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
            class="btn form-control bg-secondary text-white text-center btn-form-submit"><i
                class="fa fa-print pr-2"></i>Print</a>
        @endif
        @endif
    </div>
    @isset($edit)
    <div class="col-md-2" style="padding-left:0px">
        <a href="{{ route('invoices.destroy', $item->id) }}" data-method="delete" style="width: 100%;"
            data-confirm="Confirm delete this account?" class="btn form-control bg-danger text-white text-center btn-form-submit">
            <i class="fa fa-trash pr-2"></i>Delete</a>
    </div>
    @endif
    @if (!isset($edit))
    <div class="col-md-2" style="padding-right:0px"></div>
    <div class="col-md-2 pl-1 pr-1"></div>
    @endif
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2">
        <a href="{{ route('invoices.index') }}" style="width: 100%;" class="btn btn-danger"><i class="fa"></i>Back</a>
    </div>
</div>