<div class="form-group row">
    {!! Form::label('I_CODE', 'Code', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-2">
        {!! Form::text('I_CODE', null, ['class' => 'form-control required I_CODE', 'required']) !!}
    </div>
    <div class="col-md-8">
        <p class="mb-0 col-form-label">
            <small>
                <span class="item-code-message text-danger"></span>
            </small>
        </p>
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_DESC', 'Description', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::text('I_DESC', null, ['class' => 'form-control required I_DESC', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_DETAIL', 'Detail', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::textarea('I_DETAIL', null, ['class' => 'form-control I_DETAIL']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_UOM', 'Unit of Measurement', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-2">
        {!! Form::text('I_UOM', null, ['class' => 'form-control required I_UOM', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_COST', 'Cost', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="input-group col-md-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <i class="fa fa-usd"></i>
            </div>
        </div>
        {!! Form::text('I_COST', null, ['class' => 'form-control required money I_COST', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_PRICE', 'Price', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="input-group col-md-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <i class="fa fa-usd"></i>
            </div>
        </div>
        {!! Form::text('I_PRICE', null, ['class' => 'form-control required money I_PRICE', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_CASH_SALES_ACC', 'Cash Sales Account', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::select('I_CASH_SALES_ACC', $generalLedgers, null, ['class' => 'form-control required dynamic-select2 I_CASH_SALES_ACC', 'placeholder' => '', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_DEBIT_SALES_ACC', 'Debit Sales Account', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::select('I_DEBIT_SALES_ACC', $generalLedgers, null, ['class' => 'form-control required dynamic-select2 I_DEBIT_SALES_ACC', 'placeholder' => '', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_CREDIT_SALES_ACC', 'Credit Sales Account', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::select('I_CREDIT_SALES_ACC', $generalLedgers, null, ['class' => 'form-control required dynamic-select2 I_CREDIT_SALES_ACC', 'placeholder' => '', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_DEBIT_SALES_RETURN_ACC', 'Debit Sales Return Account', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::select('I_DEBIT_SALES_RETURN_ACC', $generalLedgers, null, ['class' => 'form-control required dynamic-select2 I_DEBIT_SALES_RETURN_ACC', 'placeholder' => '', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_CREDIT_SALES_RETURN_ACC', 'Credit Sales Return Account', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::select('I_CREDIT_SALES_RETURN_ACC', $generalLedgers, null, ['class' => 'form-control required dynamic-select2 I_CREDIT_SALES_RETURN_ACC', 'placeholder' => '', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_DEBIT_PURCHASE_ACC', 'Debit Purchase Account', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::select('I_DEBIT_PURCHASE_ACC', $generalLedgers, null, ['class' => 'form-control required dynamic-select2 I_DEBIT_PURCHASE_ACC', 'placeholder' => '', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('I_CREDIT_PURCHASE_ACC', 'Credit Purchase Account', ['class' => 'col-md-2 text-right col-form-label']) !!}
    <div class="col-md-10">
        {!! Form::select('I_CREDIT_PURCHASE_ACC', $generalLedgers, null, ['class' => 'form-control required dynamic-select2 I_CREDIT_PURCHASE_ACC', 'placeholder' => '', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-md-2"></div>
    <div class="col-md-3">
        {!! Form::submit('Save', ['class' => 'form-control btn btn-success save']) !!}
    </div>
</div>