
@php ($i = 0) @endphp

<div class="row">
        <p style="text-align:center; font-size:32px; font-weight:bold;">{{ config('config.company.name') }}</p>
        <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        <p style="text-align:center; font-size:22px; font-weight:bold;">Transaction Summary - Purchase Order</p>
    
    
</div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%">
                    <thead class="" style="border:5px solid #095484;">
                        <tr>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">SN</th>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Doc No</th>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Date</th>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Supplier DO</th>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Supplier Invoice</th>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Creditor</th>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Name</th>
                            <th style="text-align:left; border-top:2px solid;border-bottom:2px solid;">Quantity</th>
                            <th style="text-align:right; border-top:2px solid;border-bottom:2px solid;">Total Amt</th>
                        </tr>
                    </thead>
                    
                @if(sizeof($PurchaseOrderJSON) > 0)    
                    <tbody>     
                        @php
                                $amount = 0;
                                $qty = 0;
                        @endphp
                        @foreach ($PurchaseOrderJSON as $porder)
                        {{-- {{ dd($porder) }} --}}
                            <tr>    
                                <td>{{ ++$i }}</td>                               
                                <td>{{ $porder->docno}}</td>
                                <td>{{ date('d-m-Y', strtotime($porder->date))}}</td>
                                <td>{{ $porder->suppdo}}</td>
                                <td>{{ $porder->suppinv}}</td>
                                <td>{{ $porder->debtor}}</td>
                                <td>{{ $porder->name}}</td>
                                <td>{{ number_format($porder->quantity, 2)}}</td>
                                <td style="text-align:right;">{{ number_format($porder->amount, 2) }}</td>
                            </tr>
                            @php
                                $amount = $amount + $porder->amount;
                                $qty = $qty + $porder->quantity;
                            @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="border-top:2px solid;border-bottom:2px solid;">{{ number_format($qty, 2) }}</td>
                                <td style="border-top:2px solid;border-bottom:2px solid;text-align:right;">{{ number_format($amount, 2) }}</td>
                            </tr>
                        </tbody>
                    @endif
                    {{-- @endif --}}
                </table>
            </div>
            {{-- @if(isset($PurchaseOrderJSON))
                {{ $PurchaseOrderJSON->links() }}
            @endif --}}
        </div>