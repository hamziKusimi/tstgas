@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('PURCHASE_OD_CR'))
    <div class="col-3" style="flex: 0 0 18%;padding-right: 5px;">
        <div style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('cylinderporders.create') }}"> Add Purchase Order</a>
        </div>
    </div>
    @endif
    @if(Auth::user()->hasPermissionTo('PURCHASE_OD_PR') && $print->isEmpty())
        <div class="col-2" style="flex: 0 0 18%;padding-right: 5px; ">
            <div style="margin-bottom: 10px;padding-left: 0px;">    
                    <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                        data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
            </div>
        </div>
    @elseif(Auth::user()->hasPermissionTo('PURCHASE_OD_PRY'))
        <div class="col-2" style="flex: 0 0 18%;padding-right: 5px; ">
            <div style="margin-bottom: 10px;padding-left: 0px;">    
                    <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                        data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
            </div>
        </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-2">
                @if(isset($cylinderpordersearch))
                <a href="{{ route('cylinderporders.index') }}" style="width: 100%;" class="btn btn-danger"><i
                        class="fa"></i>Back</a>
                @endif
            </div>
            <div class="col-md-6">
            </div>
            <div class="col-md-4">
                <form action="{{ route('cylinderporders.searchindex') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <input name="search" type="text" id="search" class="form-control search" placeholder="Search"
                            data-get-search="{{ route('ajax.index.get.search','search') }}">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="table" class="table table-bordered table-striped" cellspacing="0">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Supplier</th>
                            <th>Name</th>
                            <th>Supp. D/O</th>
                            <th>Supp. Inv</th>
                            <th>Ref. No</th>
                            <th>Purchase Type</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                    {{-- @if(sizeof($cylinderporders) > 0) --}}

                    <tbody>
                        @if(isset($cylinderpordersearch))
                        @foreach ($cylinderpordersearch as $cylinderporder)
                        <tr>
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('PURCHASE_OD_UP'))
                            <td>
                                <a href=" {{ route('cylinderporders.edit',$cylinderporder->id) }}">
                                    {{ $cylinderporder->docno }}</a></td>
                            @else
                            <td>{{ $cylinderporder->docno }}</td>
                            @endif
                            <td>{{  isset($cylinderporder->date)?date('d-m-Y', strtotime($cylinderporder->date)):'' }}</td>
                            <td>{{ $cylinderporder->account_code}}</td>
                            <td>{{ $cylinderporder->name }}</td>
                            <td>{{ $cylinderporder->suppdo}}</td>
                            <td>{{ $cylinderporder->suppinv}}</td>
                            <td>{{ $cylinderporder->ref}}</td>
                            <td>{{ $cylinderporder->ptype}}</td>
                            <td>{{ $cylinderporder->taxed_amount }}</td>
                            @if(Auth::user()->hasPermissionTo('PURCHASE_OD_DL'))
                            <td>
                                <a href="{{ route('cylinderporders.destroy', $cylinderporder->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                        @else
                        @foreach ($cylinderporders as $cylinderporder)
                        <tr>
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('PURCHASE_OD_UP'))
                            <td>
                                <a href=" {{ route('cylinderporders.edit',$cylinderporder->id) }}">
                                    {{ $cylinderporder->docno }}</a></td>
                            @else
                            <td>{{ $cylinderporder->docno }}</td>
                            @endif
                            <td>{{  isset($cylinderporder->date)?date('d-m-Y', strtotime($cylinderporder->date)):'' }}</td>
                            <td>{{ $cylinderporder->account_code}}</td>
                            <td>{{ $cylinderporder->name }}</td>
                            <td>{{ $cylinderporder->suppdo}}</td>
                            <td>{{ $cylinderporder->suppinv}}</td>
                            <td>{{ $cylinderporder->ref}}</td>
                            <td>{{ $cylinderporder->ptype}}</td>
                            <td>{{ $cylinderporder->taxed_amount }}</td>
                            @if(Auth::user()->hasPermissionTo('PURCHASE_OD_DL'))
                            <td>
                                <a href="{{ route('cylinderporders.destroy', $cylinderporder->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    {{-- @endif --}}
                </table>
            </div>
            @if(isset($cylinderporders))
            {{ $cylinderporders->links() }}
            @endif
        </div>
    </div>
</div>
@include('dailypro.cylinderporders.printmodal')
@endsection

@push('scripts')
<script>
    (function() {
    
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#suppDo_Chkbx").prop("checked") == true){
        $("#suppDo_frm").prop('disabled', false);
        $("#suppDo_to").prop('disabled', false);
    }else{
        $("#suppDo_frm").prop('disabled', true);
        $("#suppDo_to").prop('disabled', true);
    }

    if($("#suppInv_Chkbx").prop("checked") == true){
        $("#suppInv_frm").prop('disabled', false);
        $("#suppInv_to").prop('disabled', false);
    }else{
        $("#suppInv_frm").prop('disabled', true);
        $("#suppInv_to").prop('disabled', true);
    }

    if($("#refNo_Chkbx").prop("checked") == true){
        $("#refNo_frm").prop('disabled', false);
        $("#refNo_to").prop('disabled', false);
    }else{
        $("#refNo_frm").prop('disabled', true);
        $("#refNo_to").prop('disabled', true);
    }

    if($("#credCode_Chkbx").prop("checked") == true){
        $("#credCode_frm").prop('disabled', false);
        $("#credCode_to").prop('disabled', false);
    }else{
        $("#credCode_frm").prop('disabled', true);
        $("#credCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#suppDo_Chkbx").change(function() {
        if(this.checked) {
            $("#suppDo_frm").prop('disabled', false);
            $("#suppDo_to").prop('disabled', false);
        }else{
            $("#suppDo_frm").prop('disabled', true);
            $("#suppDo_to").prop('disabled', true);
        }
    });

    $("#suppInv_Chkbx").change(function() {
        if(this.checked) {
            $("#suppInv_frm").prop('disabled', false);
            $("#suppInv_to").prop('disabled', false);
        }else{
            $("#suppInv_frm").prop('disabled', true);
            $("#suppInv_to").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx").change(function() {
        if(this.checked) {
            $("#refNo_frm").prop('disabled', false);
            $("#refNo_to").prop('disabled', false);
        }else{
            $("#refNo_frm").prop('disabled', true);
            $("#refNo_to").prop('disabled', true);
        }
    });

    $("#credCode_Chkbx").change(function() {
        if(this.checked) {
            $("#credCode_frm").prop('disabled', false);
            $("#credCode_to").prop('disabled', false);
        }else{
            $("#credCode_frm").prop('disabled', true);
            $("#credCode_to").prop('disabled', true);
        }
    });

}) ()
</script>
@endpush