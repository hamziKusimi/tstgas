<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('cylinderporders.listsummary.print') }}" method="GET" target="_blank">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Purchased Printing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-3">
                            <p><b>Report Type :</b></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            {!! Form::radio('porders_rcv','listing',null, ['checked'=>'checked','id'
                            =>'listing'])!!}
                            {!! Form::label('porders_rcv_label', 'Listing', ['class' => 'col-form-label']) !!}
                        </div>
                        <div class="col-2">
                            {!! Form::radio('porders_rcv','summary',null, [ 'id' =>'summary'])!!}
                            {!! Form::label('porders_rcv_label', 'Summary', ['class' => 'col-form-label']) !!}
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('dates_Chkbx',null,null, [ 'id' =>'dates_Chkbx'])!!}
                            {!! Form::label('Date', 'Date', ['class' => 'col-form-label']) !!}
                        </div>

                        <label class="col-form-label">:</label>
                        <div class="col-4">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('dates', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'dates'])!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('docno_Chkbx',null,null, [ 'id' =>'docno_Chkbx'])!!}
                            {!! Form::label('docno', 'Document No.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('docno_frm', $docno_select, null, ['class' => 'form-control select', 'id'
                                =>'docno_frm'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('docno_to', $docno_select, null, ['class' => 'form-control select',
                                'id' =>'docno_to','disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('suppDo_Chkbx',null,null, [ 'id' =>'suppDo_Chkbx'])!!}
                            {!! Form::label('suppDo', 'Supplier D/O', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('suppDo_frm', null, ['class' => 'form-control', 'id'
                                =>'suppDo_frm'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('suppDo_to', null, ['class' => 'form-control', 'id'
                                =>'suppDo_to'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('suppInv_Chkbx',null,null, [ 'id' =>'suppInv_Chkbx'])!!}
                            {!! Form::label('suppInv', 'Supplier Inv.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('suppInv_frm', null, ['class' => 'form-control', 'id'
                                =>'suppInv_frm'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('suppInv_to', null, ['class' => 'form-control', 'id'
                                =>'suppInv_to'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('refNo_Chkbx',null,null, [ 'id' =>'refNo_Chkbx'])!!}
                            {!! Form::label('refNo', 'Reference No.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('refNo_frm', null, ['class' => 'form-control', 'id'
                                =>'refNo_frm'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('refNo_to', null, ['class' => 'form-control', 'id'
                                =>'refNo_to'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('credCode_Chkbx',null,null, [ 'id' =>'credCode_Chkbx'])!!}
                            {!! Form::label('credCode', 'Creditor Code', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('credCode_frm', $creditor_select, null, ['class' => 'form-control select', 'id'
                                =>'credCode_frm'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('credCode_to', $creditor_select, null, ['class' => 'form-control select', 'id'
                                =>'credCode_to'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Print</button>
                </div>
            </form>
        </div>
    </div>
</div>