<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <style type="text/css" media="screen,print">
        @page {
            margin: 100px 25px;
        }

        .new-page {
            page-break-before: always;
        }

        table {
            font-size: 14px;
        }
        
        tfoot {
            position:fixed;
            left:0px;
            bottom:0px;
            height:30px;
            width:100%;
            /* background:#999; */
        }

        body {
        position: relative;
        margin-bottom: 6rem;

        }

        .last {
            position: absolute;
            bottom: -20px;
            left: 0px;
            right: 0px;
            height: 100px;
            background-color: green;
        }
    </style>
    {{-- <link rel="stylesheet" href="{{ asset('css/bootstrap-3.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/pdf.css') }}">
</head>

@php ($i = 0) @endphp
<body>

<table class="table" cellspacing="0" width="100%" height="100%">
    <thead>
        <tr>
            <h2 style="text-align:center; font-weight:bold;">{{ config('config.company.name') }}</h2>
            <p style="text-align:center;">{{ config('config.company.company_no') }}
                @if(isset($sytemsetups))
                    <p style="text-align:center;">{{ $sytemsetups->address1 }}
                    <p style="text-align:center;">{{ $sytemsetups->address2 }}
                    <p style="text-align:center;">Tel: {{ $sytemsetups->tel }} Fax: {{ $sytemsetups->fax }}
                    <p style="text-align:center;">{{ $sytemsetups->email }}
                @endif
                <h3 style="text-align: center;text-decoration: underline;">PURCHASE ORDER</h3>
        </tr>
        @if(isset($creditor) && isset($items))
        <tr>
            <td style="text-align:right;font-weight:bold;" colspan="4">
                
                    PO NO. {{ $items->docno }}
                
            </td>
        </tr>
        <tr>
            <td width="10%">
                M/S
            </td>
            <td width="5%">
                :
            </td>
            <td width="15%">
                {{ $items->name }}
            </td>
            <td style="text-align:right;" colspan="4">
                 
            </td>
        </tr> 
        <tr>
            <td width="10%">
                Address
            </td>
            <td width="5%">
                :
            </td>
            <td width="15%">
                {{ $items->addr1 }}, {{ $items->addr2 }}
                @if((isset($items->addr3) && ($items->addr3 == '')) || (isset($items->addr4) && ($items->addr4 == '')))
                {{ $items->addr3 }} {{ $items->addr4 }}
                @endif
            </td>
            <td style="text-align:right;" colspan="4">
                 
            </td>
        </tr>
        <tr>
            <td width="10%">
                ATTN
            </td>
            <td width="5%">
                :
            </td>
            <td width="15%">
                {{ $creditor->hp }}
            </td>
            <td style="text-align:right;" colspan="4">
                 
            </td>
        </tr>
        <tr>
            <td width="10%">
                TEL
            </td>
            <td width="5%">
                :
            </td>
            <td width="15%">
                {{ $creditor->hp }}
            </td>
            <td style="text-align:right;" colspan="4">
                 
            </td>
        </tr>
        <tr>
            <td width="10%">
                FAX
            </td>
            <td width="5%">
                :
            </td>
            <td width="15%">
                {{ $creditor->fax }}
            </td>
            <td style="text-align:right;" colspan="4">
                 
            </td>
        </tr>
        <tr>
            <td width="10%">
                Your Ref.
            </td>
            <td width="5%">
                :
            </td>
            <td width="15%">
                {{ $items->ref }}
            </td>
            <td style="text-align:right;" colspan="4">
                {{ $items->date }}
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <p></p> 
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <p>PLEASE SUPPLY THE FOLLOWINGS:-</p> 
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <p></p> 
            </td>
        </tr>
        @endif  
    </thead>

    <tbody>
        <table class="table" cellspacing="0" width="100%" autosize="1">
            <thead>
                <th style="border:1px solid;">QTY</th>
                <th style="border:1px solid;border-left:none;">UOM</th>
                <th style="border:1px solid;border-left:none;">DESCRIPTION</th>
                <th style="border:1px solid;border-left:none;">UNIT PRICE</th>
                <th style="border:1px solid;border-left:none;">AMOUNT</th>
            </thead>
            <tbody>
                @foreach ($contents as $content)   
                    <tr>
                        <td style="border:none;border-right:1px solid;border-left:1px solid;text-align:center;">{{ $content->qty }}</td>
                        <td style="border:none;border-right:1px solid;text-align:center;">{{ $content->uom }}</td>
                        <td style="border:none;border-right:1px solid;">{{ $content->subject }}</td>
                        <td style="border:none;border-right:1px solid;">{{ $content->ucost }}</td>
                        <td style="border:none;border-right:1px solid;">{{ $content->amount }}</td>        
                    </tr>
                @endforeach
                <tr height="70px">
                    <td style="border:none;border-right:1px solid;border-left:1px solid;text-align:center;"></td>
                    <td style="border:none;border-right:1px solid;text-align:center;"></td>
                    <td style="border:none;border-right:1px solid;"></td>
                    <td style="border:none;border-right:1px solid;"></td>
                    <td style="border:none;border-right:1px solid;"></td>  
                </tr>
                <tr style="margin: 0 auto">
                    <td colspan="3" style="border:1px solid;border-right:none;">
                    </td>
                    <td style="text-align:center;border:1px solid;border-left:none;">
                        Total
                    </td>
                    <td style="border:1px solid;border-left:none;">
                        RM &nbsp;  {{ $content->taxed_amount }}
                    </td>
                </tr>
            </tbody>
            <tfoot>
            </tfoot>
        </table>
    </tbody>
            
    <tfoot style="position: fixed; bottom: 0;">
        <br>
        <br>
                <table width="100%" style="border: 1px solid black; border-collapse:collapse;height:100%;">
                    <tr>
                        <td valign="bottom" style="border: 1px solid black;height: 40px;">
                            <p style="text-align:center;"><i>Requested By </i>
                            <p style="text-align:center;"><i>...............................</i>
                        </td>
                        <td valign="bottom" style="border: 1px solid black;height: 40px;">
                            <p style="text-align:center;"><i>Prepared By</i>
                            <p style="text-align:center;"></i>...............................</i>
                        </td>
                        <td valign="bottom" style="border: 1px solid black;height: 40px;">
                            <p style="text-align:center;"><i>Approved By</i>
                            <p style="text-align:center;">  <i>................................</i>
                        </td>
                    </tr>
                </table>
    </tfoot>
</table>



</body>