
<div class="form-group row">
    <div class="col-md-6">
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="creditor" class="col-md-2">Creditor</label>
            <div class="col-md-4">
                <select class="form-control myselect" name="creditor_id" id="creditor_id" data-values="{{ $creditors }}">
                    <option value=""></option>
                    @foreach($creditors as $creditor)
                        <option value="{{$creditor->id}}" {{ isset($porder)?$porder->creditor_id == $creditor->id  ? 'selected' : '' : ''}}>{{$creditor->accountcode}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <input name="creditordet" id="creditordet" type="text" class="form-control name" value="{{ isset($porder)?$porder->creditordet:'' }}" >
            </div>
        </div>

        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="address" class="col-md-2">Address</label>
            <div class="col-md-10">
                <input name="address" type="text" class="form-control address" value="{{ isset($porder)?$porder->address:'' }}" >
            </div>
        </div>
    
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-fax"></i></small>
                    </div>
                    <input name="fax" type="text" class="form-control fax" value="{{ isset($porder)?$porder->fax:'' }}" >
                </div>
            </div>

            <div class="col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-mobile-phone"></i></small>
                    </div>
                    <input name="hp" type="text" class="form-control hp" value="{{ isset($porder)?$porder->hp:'' }}" >
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-phone"></i></small>
                    </div>
                    <input name="tel" type="text" class="form-control tel" value="{{ isset($porder)?$porder->tel:'' }}" >
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="docno" class="col-md-3">Document No</label>
                <div class="col-md-9">
                    {{-- <input name="docno" type="text" class="form-control" value="{{ isset($porder)?$porder->docno:$runningNumber->nextRunningNoString }}" > --}}
                    <input name="docno" type="text" class="form-control" value="{{ isset($porder)?$porder->docno: $runningNumber->nextRunningNoString }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="date" class="col-md-3">Date</label>
                <div class="col-md-9">   
                    <div class="input-group-prepend">
                        <input type="text" name="date" id="datepicker" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($porder->date)?date('d-m-Y', strtotime($porder->date)):'' }}">
                            <div class="input-group-text bg-dark-blue text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="suppdo" class="col-md-3">Supplier D/O</label>
                <div class="col-md-9">
                    <input name="suppdo" type="text" class="form-control" value="{{ isset($porder)?$porder->suppdo:'' }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="suppinv" class="col-md-3">Supplier Inv. </label>
                <div class="col-md-9">
                    <input name="suppinv" type="text" class="form-control" value="{{ isset($porder)?$porder->suppinv:'' }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="suppinvdate" class="col-md-3"> Supp Inv Date </label>
                <div class="col-md-9">
                        <div class="input-group-prepend">
                        <input type="text" name="suppinvdate" id="datepicker1" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($porder->suppinvdate)?date('d-m-Y', strtotime($porder->suppinvdate)):'' }}">
                            <div class="input-group-text bg-dark-blue text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="ref" class="col-md-3">Reference No</label>
                <div class="col-md-9">
                    <input name="ref" type="text" class="form-control" value="{{ isset($porder)?$porder->ref:'' }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="ptype" class="col-md-3"> Purchase Type </label>
                <div class="col-md-9">
                        <input name="ptype" type="text" class="form-control" value="{{ isset($porder)?$porder->ptype:'' }}" >
                </div>
            </div>
        </div>
@php($true='false')
</div>
<div class="form-group row" style="margin: 0px;padding: 0px">
    <div class="col-md-12">
        <div class="table-responsive" >
            <table class="table cell-border stripe table table-sm" id="dataTable">
                <thead style="background-color:#3c8dbc; color:white;">
                    <tr class="text-sm">
                        <th width="9%" class="text-center">SN</th>
                        <th width="10%" class="text-left">Stock Code</th>
                        <th width="22%" class="text-left">Description</th>
                        <th width="6%" class="text-left">Qty</th>
                        <th width="9%" class="text-left">UOM</th>
                        <th width="8%" class="text-left">Rate</th>
                        <th width="8%" class="text-left">C Unit Cost</th>
                        <th width="6%" class="text-left">Markup</th>
                        <th width="8%" class="text-left">Unit Cost</th>
                        <th width="6%" class="text-left">Discount</th>
                        <th width="6%" class="text-left">Amount</th>
                        <th width="1%" class="text-left">More</th>
                    </tr>
                </thead>
                
                <tbody class="table-form-tbody">
                    @include('dailypro/porders/pordertable') 
                    @includeWhen(isset($porderdts), 'dailypro/porders/pordertabledata')
                </tbody>
                
                <tfoot>
                    <tr>
                        <td colspan="12" style="padding:1px;">
                            <button type="button" class="btn" style="background-color: #4aa94a; color:white" id="add-new">
                                Add Line
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

