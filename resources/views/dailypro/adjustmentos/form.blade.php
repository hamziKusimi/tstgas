
<div class="form-group row">
    <div class="col-md-6">

    </div>
    <div class="col-md-6">
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="docno" class="col-md-3">Document No</label>
                <div class="col-md-9">
                    {{-- <input name="docno" type="text" class="form-control" value="{{ isset($adjustmento)?$adjustmento->docno:$runningNumber->nextRunningNoString }}" > --}}
                    <input name="docno" type="text" class="form-control" value="{{ isset($adjustmento)?$adjustmento->docno: $runningNumber->nextRunningNoString }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="date" class="col-md-3">Date</label>
                <div class="col-md-9">   
                    <div class="input-group-prepend">
                        <input type="text" name="date" id="datepicker" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($adjustmento->date)?date('d-m-Y', strtotime($adjustmento->date)):'' }}">
                            <div class="input-group-text bg-dark-blue text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>           
        </div>
@php($true='false')
</div>
<div class="form-group row" style="margin: 0px;padding: 0px">
    <div class="col-md-12">
        <div class="table cell-border stripe table table-sm">
            <table class="table cell-border stripe" id="dataTable">
                <thead style="background-color:#3c8dbc; color:white;">
                    <tr class="text-sm">
                        <th width="9%" class="text-center">SN</th>
                        <th width="12%" class="text-center">Stock Code</th>
                        <th width="25%" class="text-left">Description</th>
                        <th width="8%" class="text-left">Qty</th>
                        <th width="8%" class="text-left">UOM</th>
                        <th width="8%" class="text-left">Rate</th>
                        <th width="8%" class="text-left">Unit Cost</th>
                        <th width="12%" class="text-left">Amount</th>
                        <th width="12%" class="text-left">Type</th>
                        <th width="1%" class="text-left">More</th>
                    </tr>
                </thead>
                
                <tbody class="table-form-tbody">
                    @include('dailypro/adjustmentos/adjustmentotable') 
                    @includeWhen(isset($adjustmentodts), 'dailypro/adjustmentos/adjustmentotabledata')
                </tbody>
                
                <tfoot>
                    <tr>
                        <td colspan="12" style="padding:1px;">
                            <button type="button" class="btn" style="background-color: #4aa94a; color:white" id="add-new">
                                Add Line
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

