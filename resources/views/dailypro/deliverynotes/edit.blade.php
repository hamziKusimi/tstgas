@extends('master')

@section('page-form-open', Form::open(['url' => route('deliverynotes.update', $item->id), 'method' => 'PUT', 'onsubmit'=>'return checkPrice()']))
@section('header')
<br>
@include('common.ajax-message')
<br>
@endsection

@section('content')
@include('dailypro.deliverynotes.options',['edit' => true])
<br>
@include('dailypro.components.system-param',[
'systemParam' => [
'data-rounding' => 'false',
'data-use-tax' => 'false',
'data-date' => isset($item) ? $item->date : '',
'data-due-date' => isset($item) ? $item->m_duedate : '',
'data-acode' => isset($item) ? $item->account_code: '',
'data-acode-name' => isset($item) ? $item->name : '',
]
])

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif

<div class="card">
    <div class="card-body">
        <div class="pl-2 pr-2">
            <div class="row">
                <div class="col-md-5">
                    @include('dailypro.components.bills.left-panel')
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    @include('dailypro.components.bills.right-panel-deliverynotes', [
                    'getDocsApiRoute' => route('deliverynotes.api.get'),
                    'except' => [
                    'refInvDate', 'referenceNo', 'amount'
                    ],
                    ])
                </div>
            </div>
        </div>
        <div class="mt-3"></div>
        <h5>Cylinder</h5>
        @include('dailypro.components.table.deliverynotes.table')

        <div class="mt-3"></div>
        <h5>Gas Rack</h5>
        @include('dailypro.components.table.deliverynotes.gasrack.table')

    </div>
</div>
<div class="mt-3"></div>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5> Prepare List </h5>
                        </div>
                        @include('dailypro.deliverynotes.preparelist')
                    </div>
                </div>
            </div>
            <div class="overlay loading-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box ">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5> Loading List </h5>
                        </div>
                        @include('dailypro.deliverynotes.loadinglist')
                    </div>
                </div>
            </div>
            <div class="overlay loading-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>

</div>
<div class="mt-3"></div>
<div class="row">
    <div class="col-md-6">
        <div class="box ">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5> Unloading List </h5>
                        </div>
                        @include('dailypro.deliverynotes.unloadinglist')
                    </div>
                </div>
            </div>
            <div class="overlay loading-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box ">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        @include('dailypro.components.bills.extras')

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-form-close', Form::close())

@section('modals')
@include('dailypro.components.item-modal')
@include('dailypro.components.template-master-modal')
@include('dailypro.components.account-modal', [
'removeDebtorFinder' => true
])
@include('dailypro.deliverynotes.printmodal2')
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script>

@include('common.form.inputmask')
@include('common.form.summernote-sm')

<script type="text/javascript">
    (function() {
    @include('dailypro.components.js.debtor-select-js')

    // enable the button if detected the account code has value
    if ($('#debtor-select').val())
        $('.btn-form-submit').prop('disabled', false)

    // prevent # from jumping to top of the screen
    $('a').on('click', function(e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
        }
    })

    $('.overlay').hide();


    $('#datepicker-inspDate').datepicker({
      autoclose: true,
      format: "dd/mm/yyyy"
    }).datepicker("setDate", new Date());

    $('#datepicker-testDate').datepicker({
      autoclose: true,
      format: "dd/mm/yyyy"
    }).datepicker("setDate", new Date());

    let preparecylinder = false
    let rowexist = false
    let rowExistinCylinder = false
    let loadcylinder = false
    let unloadcylinder = false
    let triggercylinder = false
    let barcodemanyfor = ''
    let serialmanyfor = ''
    let categorymanyfor = ''
    let productmanyfor = ''
    let descrmanyfor = ''
    let capacitymanyfor = ''
    let pressuremanyfor = ''
    let count = 1
    let cyexistingValues2 = []
    let pdexistingValues2 = []
    let cpexistingValues2 = []
    let prexistingValues2 = []

    // deliverynote date and due date
    let date = moment.utc(new Date($('.system-settings-param').data('date')))
    let dueDate = moment.utc(new Date($('.system-settings-param').data('startfrom')))
    $('#date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker1').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker2').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker3').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker4').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker5').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#startfrom-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })

    let postToAccount = 'creditSalesAcc'
    @include('common.billing.js.dnrn-table-js')
    @include('common.billing.js.dnrn-table-gasrack-js')
    @include('common.billing.js.cylinder-nested-loadinglist-table-js')
    @include('common.billing.js.gasrack-nested-loadinglist-table-js')
    @include('common.billing.js.nested-addall-loadinglist-table-js')
    @include('common.billing.js.left-panel-js')
    @include('common.billing.js.right-panel-js')

}) ()


function checkPrice(){
        var isGasPriceValid = true;
        var isDailyPriceValid = true;
        var isMonthlyPriceValid = true;
        var isDailyPriceValidGr = true;
        var isMonthlyPriceValidGr = true;
        $('input[name^="gas_price"]').each(function(index){

            if(index >0){
                var gasPrice = $(this).val();
                if(!gasPrice){
                    alert("Make Sure Gas Price Input is Not Empty");
                    isGasPriceValid = false;
                    return false;
                }
            }
        });

        $('input[name^="daily_price"]').each(function(index){

            if(index >0){
                var dailyPrice = $(this).val();
                if(!dailyPrice){
                    alert("Make Sure Cylinder Daily Rate Input is Not Empty");
                    isDailyPriceValid = false;
                    return false;
                }
            }
        });

        $('input[name^="monthly_price"]').each(function(index){

            if(index >0){
                var monthlyPrice = $(this).val();
                if(!monthlyPrice){
                    alert("Make Sure Cylinder Monthly Rate Input is Not Empty");
                    isMonthlyPriceValid = false;
                    return false;
                }
            }
        });

        $('input[name^="grdaily_price"]').each(function(index){

            if(index >0){
                var dailyPriceGr = $(this).val();
                if(!dailyPriceGr){
                    alert("Make Sure Gas Rack Daily Rate Input is Not Empty");
                    isDailyPriceValidGr = false;
                    return false;
                }
            }
        });

        $('input[name^="grmonthly_price"]').each(function(index){

            if(index >0){
                var dailyPriceGr = $(this).val();
                if(!dailyPriceGr){
                    alert("Make Sure Gas Rack Monthly Rate Input is Not Empty");
                    isMonthlyPriceValidGr = false;
                    return false;
                }
            }
        });



        return (isGasPriceValid && isDailyPriceValid && isMonthlyPriceValid && isDailyPriceValidGr && isMonthlyPriceValidGr);

    }
</script>
@endpush