<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('datepr', 'Date', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker5" data-target-input="nearest">
                    {!! Form::text('datepr', isset($item) ? $item->date : null, [
                    'class' => 'form-control invoice-date datetimepicker-input',
                    'placeholder' => '',
                    'data-target' => '#date-picker5'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker5" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <smapr><i class="fa fa-calendar"></i></smapr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('barcodepr', 'Barcode', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('barcodepr', isset($item) ? $item->barcodepr : null, [
                    'class' => 'form-control validate-barcode barcodepr',
                    'id' => 'barcodepr',
                    'data-item'=> route('cylinders.search.filter'),
                    'data-gs'=> $gsbarcodes,
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('serialpr', 'Serial No', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('serialpr', isset($item) ? $item->serialpr : null, [
                    'class' => 'form-control serialpr',
                    'id' => 'serialpr',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('driverpr', 'Driver', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('driverpr', isset($item) ? $item->driver : null, [
                    'class' => 'form-control driverpr',
                    'id' => 'driverpr',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('', '', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-5 pr-0">
                    <button type="button" class="form-control btn-primary text-white" style="width:70px" id="prepare-cyl" value="addcylinder">
                        Load Cylinder
                    </button>
                </div>
                <div class="input-group col-md-5 pr-0">
                    <button type="button" class="form-control btn-primary text-white" style="width:70px" id="prepare-gas">
                        Load Gasrack
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>