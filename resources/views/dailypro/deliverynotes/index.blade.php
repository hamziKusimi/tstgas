@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('DELIVERY_NT_CR'))
<div class="row">
    <div class="col-3" style="flex: 0 0 16%;padding-right: 5px;">
        <div style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('deliverynotes.create') }}"> Add Delivery
                Notes</a>
        </div>
    </div>
    <div class="col-2" style="padding-left: 0px;">
        <div style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
</div>
@endif

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="DNTab" class="table table-bordered table-striped" cellspacing="0">
                    <thead class="">
                        <tr class="">
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Lpo No</th>
                            <th>Driver</th>
                            <th>Charge Type</th>
                            <th>Start From</th>
                            <th>More</th>
                        </tr>
                    </thead>
                    @if(sizeof($deliverynotes) > 0)
                    <tbody class="">
                        @foreach ($deliverynotes as $deliverynote)
                        <tr class="">
                            <td>{{ ++$i }}</td>
                            <td>
                                @if(Auth::user()->hasPermissionTo('DELIVERY_NT_UP'))
                                <a href=" {{ route('deliverynotes.edit',$deliverynote->id) }}">
                                    {{ $deliverynote->dn_no }}
                                </a>
                                @else
                                {{ $deliverynote->dn_no }}
                                @endif
                            </td>
                            <td>{{  isset($deliverynote->date)?date('d-m-Y', strtotime($deliverynote->date)):'' }}
                            <td>{{ $deliverynote->account_code}}</td>
                            <td>{{ $deliverynote->name }}</td>
                            <td>{{ $deliverynote->lpono }}</td>
                            <td>{{ $deliverynote->driver }}</td>
                            <td>{{ $deliverynote->charge_type }}</td>
                            <td>{{ $deliverynote->start_from }}</td>
                            </td>
                            <td>
                                @if(Auth::user()->hasPermissionTo('DELIVERY_NT_DL'))
                                <a href="{{ route('deliverynotes.destroy', $deliverynote->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>

@include('dailypro.deliverynotes.printmodal')
@endsection
@push('scripts')
<script>
    (function() {

    $('#DNTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#LPO_Chkbx_1").prop("checked") == true){
        $("#LPO_frm_1").prop('disabled', false);
        $("#LPO_to_1").prop('disabled', false);
    }else{
        $("#LPO_frm_1").prop('disabled', true);
        $("#LPO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#LPO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#LPO_frm_1").prop('disabled', false);
            $("#LPO_to_1").prop('disabled', false);
        }else{
            $("#LPO_frm_1").prop('disabled', true);
            $("#LPO_to_1").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });
}) ()
</script>
@endpush