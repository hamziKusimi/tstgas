<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('dateul', 'Date', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker2" data-target-input="nearest">
                    {!! Form::text('dateul', isset($item) ? $item->date : null, [
                    'class' => 'form-control invoice-date datetimepicker-input',
                    'placeholder' => '',
                    'data-target' => '#date-picker2'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker2" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('barcodeul', 'Barcode', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('barcodeul', isset($item) ? $item->barcodeul : null, [
                    'class' => 'form-control barcodeul',
                    'id' => 'barcodeul',
                    'data-item'=> route('cylinders.search.filter'),
                    'data-gs'=> $gsbarcodes,
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('seriaull', 'Serial No', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('seriaull', isset($item) ? $item->seriaull : null, [
                    'class' => 'form-control',
                    'id' => 'seriaull',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('driverul', 'Driver', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('driverul', isset($item) ? $item->driver : null, [
                    'class' => 'form-control driverul',
                    'id' => 'driverul',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        <div class="input-group col-md-3 pr-0">
            <button type="button" class="form-control btn-primary text-white" style="width:70px" id="unload-all">
                Unload All
            </button>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-5 pr-0">
                    <button type="button" class="form-control btn-primary text-white" style="width:70px"
                        id="unload-cyl">
                        Unload Cylinder
                    </button>
                </div>
                <div class="input-group col-md-5 pr-0">
                    <button type="button" class="form-control btn-primary text-white" style="width:70px"
                        id="unload-gas">
                        Unload Gasrack
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>