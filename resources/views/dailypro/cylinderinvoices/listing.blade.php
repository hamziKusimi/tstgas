@php
($i = 0)
@endphp
<div class="row">
    <p style="text-align:center; font-size:32px; font-weight:bold;margin-bottom: 0px;">
        {{ config('config.company.name') }}</p>
    <p style="text-align:center; font-size:16px;margin-top: 0px;">{{ config('config.company.company_no') }}</p>
    <p style="text-align:center; font-size:22px; font-weight:bold;">Transaction Listing - Invoice</p>
    <br>


</div>
<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="row">
                <table id="invoiceTab" class="table" cellspacing="0" style="width:100%">
                    <thead class="" border="1">
                        <tr>
                            <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">SN</th>
                            <th style="text-align:left;border-top:2px solid;border-bottom:2px solid;">Description</th>
                            <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Quantity</th>
                            <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">U.O.M</th>
                            <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Unit Price</th>
                            <th style="text-align:right;border-top:2px solid;border-bottom:2px solid;">Amount</th>
                        </tr>
                    </thead>

                    @php $docno = '' @endphp
                    @if(sizeof($invoiceJSON) > 0)
                    <tbody>
                        @foreach ($invoiceJSON as $invoice)
                        <!-- check last updated doc no if not same with new updated then display -->
                        @if($docno != $invoice->docno)

                        <thead class="">
                            <tr>
                                <th></th>
                                <th colspan="6" style="text-align:left;">{{ $invoice->details }}</th>
                            </tr>
                        </thead>
                        @endif
                    <tbody>
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $invoice->description}}</td>
                            <td style="text-align:right;">{{ number_format($invoice->quantity, 2)}}</td>
                            <td style="text-align:right;">{{ $invoice->uom}}</td>
                            <td style="text-align:right;">{{ number_format($invoice->unit_price, 2)}}</td>
                            <td style="text-align:right;">{{ number_format($invoice->amount, 2)}}</td>
                        </tr>
                    </tbody>

                    @php
                    $count = 0;
                    $co = DB::select(DB::raw("
                    SELECT count(invoice_data.doc_no) as count from invoice_data where invoice_data.doc_no = :docno
                    "), [
                    'docno'=>$invoice->docno,
                    ]);
                    foreach($co as $c){
                    $count = $count + $c->count;
                    }
                    @endphp
                    @php
                    $qty = 0;
                    $id = '';
                    $inv = DB::select(DB::raw("
                    SELECT invoice_data.id, invoice_data.quantity from invoice_data where invoice_data.doc_no = :docno
                    "), [
                    'docno'=>$invoice->docno,
                    ]);
                    foreach($inv as $invc){
                    //get total quantity for each cashbills doc no
                    $qty = $qty + $invc->quantity;
                    //get last id
                    $id = $invc->id;
                    }
                    @endphp
                    <!-- check last id then display -->
                    @if($invoice->id == $id)
                    <tr style="border: 1px;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="border-top:2px solid;text-align:right;">{{ number_format($qty, 2) }}</td>
                        <td style="border-top:2px solid;text-align:right;"></td>
                        <td style="border-top:2px solid;text-align:right;"></td>
                        <td style="border-top:2px solid;text-align:right;">{{ number_format($invoice->totalamt, 2) }}
                        </td>
                    </tr>
                    {{-- @elseif($count <= '1')

                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-top:2px solid;">{{ number_format($invoice->quantity, 2) }}</td>
                    <td style="border-top:2px solid;"></td>
                    <td style="border-top:2px solid;"></td>
                    <td style="border-top:2px solid;">{{ number_format($invoice->totalamt, 2) }}</td>
                    </tr> --}}
                    @endif

                    @php
                    $docno = $invoice->docno
                    @endphp
                    @endforeach
                    <!-- Display Grand Total -->
                    <tr>
                        <td></td>
                        <td></td>
                        <td style="border-top:2px solid;border-bottom:2px solid;text-align:right;"></td>
                        <td style="border-top:2px solid;border-bottom:2px solid;text-align:right;"></td>
                        <td style="border-top:2px solid;border-bottom:2px solid;text-align:right;">Grand Total</td>
                        <td style="border-top:2px solid;border-bottom:2px solid;text-align:right;">
                            {{ number_format($totalamount, 2) }}</td>
                    </tr>
                    </tbody>
                    @endif
                    {{-- @endif --}}
                </table>
            </div>
        </div>
    </div>
</div>