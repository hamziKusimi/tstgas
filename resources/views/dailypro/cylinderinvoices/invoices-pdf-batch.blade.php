<!DOCTYPE html>

<head>
    <meta charset="utf-8" />
    <style type="text/css" media="screen,print">
        @page {
            margin: 100px 25px;
        }

        .new-page {
            page-break-before: always;
        }

        table {
            font-size: 18px;
        }

        .last {
            position: absolute;
            bottom: -20px;
            left: 0px;
            right: 0px;
            height: 100px;
            background-color: green;
        }
    </style>
    {{-- <link rel="stylesheet" href="{{ asset('css/bootstrap-3.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/pdf.css') }}">
</head>

<body>

    @php
    $totalInvoice = count($Invoice);
    $count = 1;
    @endphp
    @foreach ($Invoice as $inv)
    @php
    $address = $inv['invoice']->addr1;
    $address .= $inv['invoice']->addr2 == null || $inv['invoice']->addr2 == ''? '': $inv['invoice']->addr2;
    $address2 = $inv['invoice']->addr3 == null || $inv['invoice']->addr3 == ''? '': $inv['invoice']->addr3;
    $address2 .= $inv['invoice']->addr4 == null || $inv['invoice']->addr4 == ''? '': $inv['invoice']->addr4;
    $header = str_ireplace('<p>','', $inv['invoice']->header);
        $header = str_ireplace('</p>','', $header);
    @endphp
    <table width="100%">
        <thead>
            <br>
            <br>
            <br>
            <br>
            <div class="row">
                <h1 style="text-align: center;text-decoration: underline;">INVOICE</h1>
            </div>
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <i>To:</i> <b>{{ $inv['invoice']->name }}</b>
                    </td>
                    <td style="width: 240px; overflow: hidden;">
                        <h3 style="margin-bottom: 0px;"><span style="font-size: 14px;font-weight: normal;">No:</span>
                            {{ $inv['invoice']->docno }}
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <i>Address:</i>
                    </td>
                    <td>
                        {!! $address !!}
                    </td>
                    <td><i>Date:</i>{{ date("d/m/Y", strtotime($inv['invoice']->date)) }} </td>
                </tr>
                <tr>
                    <td></td>
                    <td> {!! $address2 !!}</td>
                    <td><i>Our D/O No:</i></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tel : {{ $inv['invoice']->tel_no }} &nbsp; Fax : {{ $inv['invoice']->fax_no }}</td>
                    <td><i>Terms:</i> {{ $inv['invoice']->credit_term }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><i>Account No:</i>{{ $inv['invoice']->account_code }}</td>
                </tr>
            </table>
        </thead>
        <tbody>
            <br />
            <table width="100%" style="border:1px solid black;border-collapse:collapse;height:100%;">
                <thead>
                    <tr>
                        <th style="border: 1px solid black;width:5%;">Item</th>
                        <th style="border: 1px solid black;width:45%;">Description</th>
                        <th style="border: 1px solid black;width:7%;">Qty</th>
                        <th style="border: 1px solid black;width:7%;">UOM</th>
                        <th style="border: 1px solid black;width:7%;">Rate</th>
                        <th style="border: 1px solid black;width:7%;">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row" style="border-right: 1px solid black;" valign="top">
                        </th>
                        <td style="border-right: 1px solid black;padding-left: 15px;padding-bottom: 15px;" valign="top">
                            {{ $header }}
                            <br>
                        </td>
                        <td align="center" style="border-right: 1px solid black;" valign="top">
                        </td>
                        <td align="center" style="border-right: 1px solid black;" valign="top">
                        </td>
                        <td align="center" style="border-right: 1px solid black;" valign="top">
                        </td>
                        <td align="center" valign="top">
                        </td>
                    </tr>
                    @php
                    $itemNo = 1;
                    $totalAmount = 0;
                    @endphp
                    @foreach ($inv['invdata'] as $item)
                    {{-- {{ dd($item) }} --}}
                    <tr>
                        <th scope="row" style="border-right: 1px solid black;" valign="top">
                            {{ $itemNo }}
                        </th>
                        <td style="border-right: 1px solid black;padding-left: 15px;" valign="top">
                            {{ $item->subject }}
                            <p>{!! $item->details !!}</p>
                        </td>
                        <td align="center" style="border-right: 1px solid black;" valign="top">
                            {{  $item->quantity }}
                        </td>
                        <td align="center" style="border-right: 1px solid black;" valign="top">
                            {{  $item->uom }}
                        </td>
                        <td align="center" style="border-right: 1px solid black;" valign="top">
                            {{  $item->unit_price }}
                        </td>
                        <td align="center" valign="top">
                            {{   number_format((float) $item->amount, 2, '.', '') }}
                        </td>
                    </tr>
                    @php
                    $itemNo++;
                    $totalAmount = $totalAmount + $item->amount;
                    @endphp
                    @endforeach

                    <tr>
                        <th scope="row" style="border-right: 1px solid black;"></th>
                        <td style="border-right: 1px solid black;padding-left: 15px;">
                            {!! $inv['invoice']->summary !!}
                        </td>
                        <td style="border-right: 1px solid black;"></td>
                        <td style="border-right: 1px solid black;"></td>
                        <td style="border-right: 1px solid black;"></td>
                        <td></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="4" rowspan="2" valign="top"
                            style="border-top: 1px solid black;border-bottom: 1px solid white;border-left: 1px solid white;">
                            <p><b> Ringgit Malaysia:
                                    {{ App\Model\CustomObject\Transaction::convertHelper($inv['invoice']->amount) }}</b>
                            </p>
                        </td>
                        <td align="center" style="border-top: 1px solid black;border-bottom: 1px solid white;">
                            Net Total
                        </td>
                        <td align="center" style="border: 1px solid black;">
                            {{ number_format((float) $inv['invoice']->amount, 2, '.', '')  }}
                        </td>
                    </tr>
                </tfoot>
            </table>

        </tbody>
        <tfoot>

            <tr>
                <td valign="top" colspan="2">
                    <table width="100%" style=" border-collapse:collapse;height:100%;">
                        <tr>
                            <td align="left" style="Width: 30%;">
                                <p>Bank Details: </p>
                                <p>1) {{ $systemSetup->bank_detail }}</p>
                                <p>A/C No. {{ $systemSetup->bank_account_no }}</p>
                            </td>
                            <td style="Width: 30%;"></td>
                            <td align="center" style="Width: 30%;">
                                <p> <i>for</i><b> {{  config('config.company.name') }}</b> </p>
                                <p></p>
                                <p>..................................................................</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" style="border: 1px solid black;height: 40px;">
                                <p><i>Billed By ..............................</i></p>
                            </td>
                            <td valign="bottom" style="border: 1px solid black;height: 40px;">
                                <p><i>Checked By ..............................</i></p>
                            </td>
                            <td valign="bottom" style="border: 1px solid black;height: 40px;">
                                <p><i>Certified By ..............................</i></p>
                            </td>
                        </tr>
                    </table>
                </td>
                <tr />
            <tr>
                <td><i> {{ $inv['invoice']->footer }}</i></td>
            </tr>
        </tfoot>
    </table>

    @if ($count<$totalInvoice) <div class="new-page">

        </div>
        @endif
        @php
        $count++;
        @endphp
        @endforeach




</body>