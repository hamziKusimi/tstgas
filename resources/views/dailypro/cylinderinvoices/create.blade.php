@extends('master')

@section('page-form-open', Form::open(['url' => route('cylinderinvoices.store'), 'method' => 'POST']))
@section('header')
<br>
@include('common.ajax-message')
<br>
@endsection

@section('content')

@include('dailypro.cylinderinvoices.options')
<br>
@include('dailypro.components.system-param',[
'systemParam' => [
'data-rounding' => 'false',
'data-use-tax' => 'false',
]
])

<div class="card">
    <div class="card-body">
        <div class="pl-2 pr-2">
            <div class="row">
                <div class="col-md-5">
                    @include('dailypro.components.bills.left-panel', [
                    'getRoute' => route('debtors.api.store'),
                    ])
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    @include('dailypro.components.bills.right-panel-cyinvoice', [
                    'getDocsApiRoute' => '',
                    'except' => [
                    'refInvDate', 'referenceNo', 'amount'
                    ],
                    ])
                </div>
            </div>
        </div>
        <div class="mt-3"></div>
        @include('dailypro.components.table.cylinderinvoices.table-withouttax')

    </div>
</div>

<div class="mt-3"></div>
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.extras')
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.totaling', [
                'roundingIsEnabled' => 'false'
                ])
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-form-close', Form::close())

@section('modals')
{{-- @include('dailypro.components.item-modal') --}}
@include('dailypro.components.template-master-modal')
@include('dailypro.components.account-modal', [
'removeDebtorFinder' => true
])
{{-- @include('common.modal.search-cylinder') --}}
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
{{-- <script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script> --}}

@include('common.form.inputmask')
@include('common.form.summernote-sm')

<script type="text/javascript">
    (function() {
    @include('dailypro.components.js.debtor-select-js')

    // prevent # from jumping to top of the screen
    $('a').on('click', function(e) {
        if ($(this).attr('href') == '#') e.preventDefault()
    })


    // cylinderinvoice date and due date
    let date = moment(new Date())
    $('#date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#reference_date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#due_date-picker').datetimepicker({ format: 'DD/MM/YYYY' })

    let postToAccount = 'creditSalesAcc'
    @include('common.billing.js.cylinderinvoice-table-js')
    @include('common.billing.js.left-panel-js')
    @include('common.billing.js.right-panel-js')


    $('#invdate').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });



    $('#cylinderTab').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('cylinder.datatable.data') !!}',
        columns: [
            { data: 'serial', name: 'Serial' },
            { data: 'barcode', name: 'Barcode' },
            { data: 'descr', name: 'descr' },
            { data: 'product', name: 'product' },
            { data: 'ctype', name: 'ctype' },
        ],
    });

    $('#cylinderTab tbody').css('cursor', 'pointer');

    $('#cylinderTab tbody').on('click', 'tr', function () {
        serial = $(this).find('td:eq(0)').html();
        barcode = $(this).find('td:eq(1)').html();
        descr = $(this).find('td:eq(2)').html();
        product = $(this).find('td:eq(3)').html();
        type = $(this).find('td:eq(4)').html();
        // let row = $(this).prev().parents('tr')
        // console.log(row);
        // row.find('.serial').val(serial)
        // row.find('.barcode').val(barcode)
        // row.find('.subject').val(descr)
        // row.find('.product').val(product)
        // row.find('.type').val(type)
        // row.find('.quantity').val(1)
        // row.find('.unit_price').val(0)
        // row.find('.discount').val(0)

        $('#search-cyl').val(name)
        $("#cylModal").modal("hide");
    });

}) ()

function myFunction(element) {
    $("#cylModal").modal("show");
    var row = $('.serial').parents('tr')

    $('#cylinderTab tbody').on('click', 'tr', function () {
        serial = $(this).find('td:eq(0)').html();
        barcode = $(this).find('td:eq(1)').html();
        descr = $(this).find('td:eq(2)').html();
        product = $(this).find('td:eq(3)').html();
        type = $(this).find('td:eq(4)').html();

        row.find('.serial').val(serial)
        row.find('.barcode').val(barcode)
        row.find('.subject').val(descr)
        row.find('.product').val(product)
        row.find('.type').val(type)
        row.find('.quantity').val(1)
        row.find('.unit_price').val(0)
        row.find('.discount').val(0)

        $('.btn-form-submit').prop('disabled', false)
        $("#add-new").removeClass('d-none')
        $("#cylModal").modal("hide");
    });
}
</script>
@endpush