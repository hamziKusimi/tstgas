@extends('master')

@section('content')

<div class="row">
    <div class="col-2" style="flex: 0 0 13%;padding-right: 5px;">
        <div style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('cylinderinvoices.create') }}"> Add Invoices
            </a>
        </div>
    </div>
    <div class="col-2" style="padding-left: 0px;">
        <div style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 90%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button customer="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif
@if (Session::has('Error'))
<div class="alert alert-error text-secondary" role="alert">
    <button customer="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Error') }}</p>
</div>
@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="col-md-12">

            <form action="{{ route('cylinderinvoices.query') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-2">

                        {!! Form::checkbox('AC_Code_Chkbx',null,(Session('ac_chk')=='on'?true:false),
                        [ 'id' =>'AC_Code_Chkbx'])!!}
                        {!! Form::label('AC_Code', 'Debtor ', ['class' => 'col-form-label']) !!}
                    </div>
                    <label class="col-form-label"></label>
                    <div class="col-4">
                        <div class="form-group">
                            <div class="input-group-append" style="width:100%">
                                {!! Form::text('AC_Code_frm', Session::has('ac_frm')?Session('ac_frm'):null, ['class' => 'form-control AC_Code_frm', 'id'
                                =>'AC_Code_frm'
                                ,'disabled' =>'disabled'])!!}
                                <div class="btn input-group bg-dark-blue text-white pointer searchdebtor" data-toggle="modal" data-target=".reportdebtor-modal" style="width:15%">
                                    <small>
                                        <i class="fa fa-search"></i>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <div class="input-group-append" style="width:100%">
                                {!! Form::text('AC_Code_to', null, ['class' => 'form-control AC_Code_to', 'id'
                                =>'AC_Code_to'
                                ,'disabled' =>'disabled'])!!}
                                <div class="btn input-group bg-dark-blue text-white pointer searchdebtorto" data-toggle="modal" data-target=".reportdebtorto-modal" style="width:15%">
                                    <small>
                                        <i class="fa fa-search"></i>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label for="barcode" class="col-md-2">Date</label>
                    <div class="col-md-3">
                        <div class="input-group datefrmto">
                            <input type="text" name="datefrmto" class="form-control datefrmto" id="datefrmto">
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-form-submit">Process invoice</button>
                    </div>
                </div>
            </form>
        </div>

        <br>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="invTab" class="table table-bordered table-striped" cellspacing="0">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Terms</th>
                            <th>D/O No.</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                    @if(sizeof($invoices) > 0)
                    <tbody>
                        @foreach ($invoices as $invoice)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td><a href=" {{ route('cylinderinvoices.edit',$invoice->id) }}">{{ $invoice->docno }}</a>
                            </td>
                            <td>{{  isset($invoice->date)?date('d-m-Y', strtotime($invoice->date)):'' }}</td>
                            <td>{{ $invoice->account_code}}</td>
                            <td>{{ $invoice->name }}</td>
                            <td>{{ $invoice->credit_term }}</td>
                            <td>{{ $invoice->do_no }}</td>
                            <td>{{ $invoice->taxed_amount }}</td>
                            <td>
                                <a href="{{ route('cylinderinvoices.destroy', $invoice->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this record?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@include('dailypro.cylinderinvoices.printmodal')
@endsection
@section('modals')
    @include('dailypro.components.reportdebtor-modal')
    @include('dailypro.components.reportdebtorto-modal')
    @include('dailypro.components.reportdocno-modal')
    @include('dailypro.components.reportdocnoto-modal')
@endsection
@push('scripts')
<script>
    (function() {

    $('#invTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });
    $('#datefrmto').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });


    $("#AC_Code_Chkbx").change(function() {
        if(this.checked) {
            $("#AC_Code_frm").prop('disabled', false);
            $("#AC_Code_to").prop('disabled', false);
        }else{
            $("#AC_Code_frm").prop('disabled', true);
            $("#AC_Code_to").prop('disabled', true);
        }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#AC_Code_Chkbx").prop("checked") == true){
        $("#AC_Code_frm").prop('disabled', false);
        $("#AC_Code_to").prop('disabled', false);
    }else{
        $("#AC_Code_frm").prop('disabled', true);
        $("#AC_Code_to").prop('disabled', true);
    }

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#DO_Chkbx_1").prop("checked") == true){
        $("#DO_frm_1").prop('disabled', false);
        $("#DO_to_1").prop('disabled', false);
    }else{
        $("#DO_frm_1").prop('disabled', true);
        $("#DO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#DO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#DO_frm_1").prop('disabled', false);
            $("#DO_to_1").prop('disabled', false);
        }else{
            $("#DO_frm_1").prop('disabled', true);
            $("#DO_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });

    $('#cashbilldebtortable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('cylinder.debtor.list.data') !!}',
        columns: [
            { data: 'accountcode', name: 'accountcode' },
            { data: 'name', name: 'name' }
        ],
    });

    $('#cashbilldebtortable2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('cylinder.debtor.list.data') !!}',
        columns: [
            { data: 'accountcode', name: 'accountcode' },
            { data: 'name', name: 'name' }
        ],
    });

    $('.reportdebtor-modal').on('shown.bs.modal', function() {

        var rowmodal = $('.reportdebtor-modal').find('tbody')
        var row = rowmodal.find('tr')
        row.attr({'data-id':'id', 'id':'route2'})

            let modal = $('.reportdebtor-modal')
            let firstRow = $(this).parents('tr')
            modal.on('dblclick', 'tr', function() {
                let id = $(this).find('td:eq(0)').html();
                $('.AC_Code_frm').val(id)
                $('.AC_Code_to').val(id)
                $('.reportdebtor-modal').modal('hide');
            })

    })

    $('.reportdebtorto-modal').on('shown.bs.modal', function() {

        var rowmodal = $('.reportdebtorto-modal').find('tbody')
        var row = rowmodal.find('tr')
        row.attr({'data-id':'id', 'id':'route2'})

            let modal = $('.reportdebtorto-modal')
            let firstRow = $(this).parents('tr')
            modal.on('dblclick', 'tr', function() {
                let id = $(this).find('td:eq(0)').html();
                $('.AC_Code_to').val(id)

                $('.reportdebtorto-modal').modal('hide');
            })

    })

    $(".AC_Code_frm").change(function(){
        let debFrom = $('.AC_Code_frm').val();
        $('.AC_Code_to').val(debFrom)
    });

}) ()
</script>
@endpush