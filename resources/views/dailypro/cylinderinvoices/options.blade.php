<div class="row">
    @isset($edit)
    <div class="col-md-2" style="padding-right:0px">
        <a href="{{ route('cylinderinvoices.create') }}" style="width: 100%;"
            class="btn form-control bg-dark-green text-white text-center btn-form-submit">New</a>
    </div>
    @endisset
    <div class="col-md-2" style="padding-right:0px;{{isset($edit) ? "padding-left:0px":null}}">
        @if(Auth::user()->hasPermissionTo('INVOICE_CR') || Auth::user()->hasPermissionTo('INVOICE_UP'))
        @include('common.billing.options.submit', [
        'faIcon' => 'share-square-o',
        'color' => 'bg-dark-blue text-white',
        'permissions' => ['Create Invoices', 'Update Invoices'],
        'name' => 'Submit'
        ])
        @endif
    </div>
    <div class="col-md-2" style="padding-left:0px">
        @if (isset($item->id))
        <a href="{{ route('cylinderinvoices.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
            class="btn form-control bg-secondary text-white text-center btn-form-submit"><i
                class="fa fa-print pr-2"></i>Print</a>
        @endif
    </div>
    @if (!isset($edit))
    <div class="col-md-2" style="padding-right:0px"></div>
    @endif
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2">
        <a href="{{ route('cylinderinvoices.index') }}" style="width: 100%;" class="btn btn-danger"><i
                class="fa"></i>Back</a>
    </div>
</div>