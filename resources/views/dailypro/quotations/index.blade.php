@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('QUOTATION_CR'))
    <div class="col-3" style="flex: 0 0 14%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('quotations.create') }}"> Add Quotation</a>
        </div>
    </div>
    @endif
    @if(Auth::user()->hasPermissionTo('QUOTATION_PR') && $print->isEmpty())
    <div class="col-2" style="flex: 0 0 14%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @elseif(Auth::user()->hasPermissionTo('QUOTATION_PRY'))
    <div class="col-2" style="flex: 0 0 14%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="quotationTab" class="table table-bordered table-striped" cellspacing="0" style="font-size: 11px;">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Attention</th>
                            <th>Contact</th>
                            <th>CB/Inv/Do No.</th>
                            <th>Quotation No.</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


{{-- {!! $quotations->links() !!} --}}
@include('dailypro.quotations.printmodal')
@endsection

@push('scripts')
<script>
    (function() {

    $('#quotationTab').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: '{!! route('quotation.datatable.data') !!}',
            dataType: 'json',
            type: 'GET',
            data:{ _token: '{{csrf_token()}}'}
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'docno', name: 'docno' },
            { data: 'date', name: 'date' },
            { data: 'account_code', name: 'account_code' },
            { data: 'name', name: 'name' },
            { data: 'attention', name: 'attention' },
            { data: 'contact', name: 'contact' },
            { data: 'cbinvdono', name: 'cbinvdono' },
            { data: 'quot', name: 'quot' },
            { data: 'taxed_amount', name: 'taxed_amount' },
            { data: 'more', name: 'more', orderable: false }
        ]
    });
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#CB_INV_DO_Chkbx_1").prop("checked") == true){
        $("#CB_INV_DO_frm_1").prop('disabled', false);
        $("#CB_INV_DO_to_1").prop('disabled', false);
    }else{
        $("#CB_INV_DO_frm_1").prop('disabled', true);
        $("#CB_INV_DO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#CB_INV_DO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#CB_INV_DO_frm_1").prop('disabled', false);
            $("#CB_INV_DO_to_1").prop('disabled', false);
        }else{
            $("#CB_INV_DO_frm_1").prop('disabled', true);
            $("#CB_INV_DO_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });
}) ()
</script>
@endpush