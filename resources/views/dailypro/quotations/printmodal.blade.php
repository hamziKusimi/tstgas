<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('quotations.print') }}" method="GET" target="_blank">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Quotation Printing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-3">
                            <p><b>Report Type :</b></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            {!! Form::radio('Quotation_rcv','listing',null, ['checked'=>'checked','id'
                            =>'listing'])!!}
                            {!! Form::label('Quotation_rcv_label', 'Listing', ['class' => 'col-form-label']) !!}
                        </div>
                        <div class="col-2">
                            {!! Form::radio('Quotation_rcv','summary',null, [ 'id' =>'summary'])!!}
                            {!! Form::label('Quotation_rcv_label', 'Summary', ['class' => 'col-form-label']) !!}
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('dates_Chkbx',null,null, [ 'id' =>'dates_Chkbx'])!!}
                            {!! Form::label('Date', 'Date', ['class' => 'col-form-label']) !!}
                        </div>

                        <label class="col-form-label">:</label>
                        <div class="col-4">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('dates', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'dates'
                                    ])!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('docno_Chkbx',null,null, [ 'id' =>'docno_Chkbx'])!!}
                            {!! Form::label('docno', 'Document No.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('docno_frm', $docno_select, null, ['class' => 'form-control select',
                                'id'
                                =>'docno_frm'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('docno_to', $docno_select, null, ['class' => 'form-control select',
                                'id' =>'docno_to','disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('CB_INV_DO_Chkbx_1',null,null, [ 'id' =>'CB_INV_DO_Chkbx_1'])!!}
                            {!! Form::label('CB_INV_DO', 'CB/INV/DO No.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('CB_INV_DO_frm_1', null, ['class' => 'form-control', 'id'
                                =>'CB_INV_DO_frm_1'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('CB_INV_DO_to_1', null, ['class' => 'form-control', 'id'
                                =>'CB_INV_DO_to_1'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('debCode_Chkbx',null,null, [ 'id' =>'debCode_Chkbx'])!!}
                            {!! Form::label('debCode', 'Debtor Code', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('debCode_frm', $debtor_select, null, ['class' => 'form-control
                                select', 'id'
                                =>'debCode_frm'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::select('debCode_to', $debtor_select, null, ['class' => 'form-control
                                select', 'id'
                                =>'debCode_to'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Print</button>
                </div>
            </form>
        </div>
    </div>
</div>