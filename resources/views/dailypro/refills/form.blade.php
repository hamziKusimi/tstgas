
<div class="form-group row">
    <label for="wo_no" class="col-md-2"> WO No <span class="text-danger">*</span></label>
    <div class="col-md-10">
        <input name="wo_no" type="text" maxlength="50" class="form-control"
            value="{{ isset($refill)?$refill->wo_no:$runningNumber->nextRunningNoString }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="tech" class="col-md-2"> Technician <span class="text-danger">*</span></label>
    <div class="col-md-10">
        <input name="tech" type="text" maxlength="50" class="form-control" value="{{ isset($refill)?$refill->tech:'' }}"
            required>
    </div>
</div>

<div class="form-group row">
    <label for="datetime" class="col-md-2"> Date <span class="text-danger">*</span></label>
    <div class="col-md-10">
        <div class="input-group-prepend">
            <div class="input-group-text bg-dark-blue text-white">
                <small><i class="fa fa-calendar"></i></small>
            </div>
            <input type="text" maxlength="100" name="datetime" id="datepicker" class="form-control"
                placeholder="dd-mm-yyyy" autocomplete="off" 
                value="{{ isset($refill->datetime)?date('d-m-Y', strtotime($refill->datetime)):'' }}" required>
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="barcode" class="col-md-2">Barcode</label>
    <div class="col-md-10">
        <input name="barcode" type="text" maxlength="100" class="form-control"
            value="{{ isset($refill)?$refill->barcode:'' }}">
    </div>
</div>

<div class="form-group row">
    <label for="serial" class="col-md-2">Serial No<span class="text-danger">*</span></label>
    <div class="col-md-10">
        <input name="serial" type="text" maxlength="100" class="form-control"
            value="{{ isset($refill)?$refill->serial:'' }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="charge_in" class="col-md-2">Charge In</label>
    <div class="col-md-10">
        <input name="charge_in" type="text" maxlength="100" class="form-control"
            value="{{ isset($refill)?$refill->charge_in:'' }}">
    </div>
</div>

<div class="form-group row">
    <label for="charge_out" class="col-md-2">Charge Out</label>
    <div class="col-md-10">
        <input name="charge_out" type="text" maxlength="100" class="form-control"
            value="{{ isset($refill)?$refill->charge_out:'' }}">
    </div>
</div>
