@extends('master')
@section('content')
<form action="{{ route('refills.update', $refill->id )}}" method="POST">
    <input type="hidden" name="_method" value="PUT" />
    @csrf

    <div class="form-group row">
        <div class="col-md-2">
            <a href="{{ route('refills.create') }}" style="width: 100%;" class="btn btn-success">New</a>
        </div>
        <div class="col-md-2" style=";padding-left:0px">
            @if(Auth::user()->hasPermissionTo('REFILL_CR') || Auth::user()->hasPermissionTo('REFILL_UP'))
            <div class="form-group">
                <button type="submit" style="width: 100%;" class="btn btn-primary">Submit</button>
            </div>
            @endif
        </div>
        <div class="col-md-2" style="padding-left:0px">
            @if (isset($refill->id))
            <div class="form-group">
                <a href="{{ route('refills.jasper', ['id' => $refill->id]) }}" target="_blank" style="width: 100%;"
                    class="btn btn-secondary"><i class="fa fa-print pr-2"></i>Print</a>
            </div>
            @endif
        </div>
        <div class="col-md-2 pl-1 pr-1"></div>
        <div class="col-md-2 pl-1 pr-1"></div>

        <div class="col-md-2">
            <a href="{{ route('refills.index') }}" style="width: 100%;" class="btn btn-danger"><i
                    class="fa"></i>Back</a>
        </div>
    </div>
    <div class="box box-solid">
        <div class="box-header"></div>

        <div class="box-body with-border">
            @include('dailypro/refills/form')
        </div>
    </div>
</form>

@endsection