@extends('master')

@section('content')

<div class="row">
    <div class="col-3" style="flex: 0 0 14%;padding-right: 5px;">
        <div style="margin-bottom: 10px;">
            <a class="btn btn-success " style="width: 100%;"href="{{ route('repairs.create') }}"> Add Repair</a>
        </div>
    </div>
    <div class="col-2"  style="padding-left: 0px;">
        <div style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
</div>

{{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
</div>
@endif --}}


@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="RepairTab" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>RP No</th>
                            <th>Technician</th>
                            <th>Barcode</th>
                            <th>Serial</th>
                            <th>Description</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    @if(sizeof($repairs) > 0)
                    <tbody>
                        @foreach ($repairs as $repair)
                        <tr>

                            <td>{{ ++$i }}</td>
                            <td>
                                @if(Auth::user()->hasPermissionTo('REPAIR_UP'))
                                <a href="{{ route('repairs.edit',$repair->id) }}">
                                    {{ $repair->rp_no }}
                                </a>
                                @else
                                {{ $repair->rp_no }}
                                @endif
                            </td>
                            <td>{{ $repair->tech}}</td>
                            <td>{{ $repair->barcode}}</td>
                            <td>{{ $repair->serial}}</td>
                            <td>{{ $repair->logindate }}</td>
                            <td>
                                <a href="{{ route('repairs.destroy', $repair->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>

{{-- {!! $repairs->links() !!} --}}
@include('dailypro.repairs.printmodal')
@endsection

@push('scripts')
<script>
    (function() {
        
    $('#RepairTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#RP_Chkbx").prop("checked") == true){
        $("#RP_frm").prop('disabled', false);
        $("#RP_to").prop('disabled', false);
    }else{
        $("#RP_frm").prop('disabled', true);
        $("#RP_to").prop('disabled', true);
    }

    $("#RP_Chkbx").change(function() {
        if(this.checked) {
            $("#RP_frm").prop('disabled', false);
            $("#RP_to").prop('disabled', false);
        }else{
            $("#RP_frm").prop('disabled', true);
            $("#RP_to").prop('disabled', true);
        }
    });

}) ()
</script>
@endpush