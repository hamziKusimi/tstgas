<div class="form-group row">
    <label for="rp_no" class="col-md-2"> RP No <span class="text-danger">*</span></label>
    <div class="col-md-10">
        <input name="rp_no" type="text" maxlength="50" class="form-control"
            value="{{ isset($repair)?$repair->rp_no: $runningNumber->nextRunningNoString }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="tech" class="col-md-2"> Technician <span class="text-danger">*</span></label>
    <div class="col-md-10">
        <input name="tech" type="text" maxlength="50" class="form-control" value="{{ isset($repair)?$repair->tech:'' }}"
            required>
    </div>
</div>

<div class="form-group row">
    <label for="logindate" class="col-md-2"> Login Date <span class="text-danger">*</span></label>
    <div class="col-md-10">
        <div class="input-group-prepend">
            <div class="input-group-text bg-dark-blue text-white">
                <small><i class="fa fa-calendar"></i></small>
            </div>
            <input type="text" maxlength="100" name="logindate" id="datepicker" class="form-control"
                placeholder="dd-mm-yyyy" autocomplete="off"
                value="{{ isset($repair->logindate)?date('d-m-Y', strtotime($repair->logindate)):'' }}">
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="barcode" class="col-md-2">Barcode</label>
    <div class="col-md-10">
        <input name="barcode" type="text" maxlength="100" class="form-control"
            value="{{ isset($repair)?$repair->barcode:'' }}">
    </div>
</div>

<div class="form-group row">
    <label for="serial" class="col-md-2">Serial No<span class="text-danger">*</span></label>
    <div class="col-md-10">
        <input name="serial" type="text" maxlength="100" class="form-control"
            value="{{ isset($repair)?$repair->serial:'' }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="completedate" class="col-md-2"> Complete Date <span class="text-danger">*</span></label>
    <div class="col-md-10">
        <div class="input-group-prepend">
            <div class="input-group-text bg-dark-blue text-white">
                <small><i class="fa fa-calendar"></i></small>
            </div>
            <input type="text" maxlength="100" name="completedate" id="datepicker1" class="form-control"
                placeholder="dd-mm-yyyy" autocomplete="off"
                value="{{ isset($repair->completedate)?date('d-m-Y', strtotime($repair->completedate)):'' }}">
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="charge_in" class="col-md-2">Charge In</label>
    <div class="col-md-10">
        <input name="charge_in" type="text" maxlength="100" class="form-control"
            value="{{ isset($repair)?$repair->charge_in:'' }}">
    </div>
</div>

<div class="form-group row">
    <label for="charge_out" class="col-md-2">Charge Out</label>
    <div class="col-md-10">
        <input name="charge_out" type="text" maxlength="100" class="form-control"
            value="{{ isset($repair)?$repair->charge_out:'' }}">
    </div>
</div>

<div class="form-group row">
    <label for="fault" class="col-md-2">Faulty</label>
    <div class="col-md-10">
        <textarea name="fault" rows="3" class="form-control">{{ isset($repair)?$repair->fault:'' }}</textarea>
    </div>
</div>
