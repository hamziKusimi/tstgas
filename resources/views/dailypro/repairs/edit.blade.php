@extends('master')
@section('content')

<form action="{{ route('repairs.update', $repair->id )}}" method="POST">
    <input type="hidden" name="_method" value="PUT" />
    @csrf
    <div class="form-group row">
        <div class="col-md-2">
            <a href="{{ route('repairs.create') }}" style="width: 100%;" class="btn btn-success">New</a>
        </div>
        <div class="col-md-2" style=";padding-left:0px">
            @if(Auth::user()->hasPermissionTo('REPAIR_CR') || Auth::user()->hasPermissionTo('REPAIR_UP'))
            <div class="form-group">
                <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
            </div>
            @endif
        </div>
        <div class="col-md-2" style="padding-left:0px">
            @if (isset($repair->id))
            <div class="form-group">
                <a href="{{ route('repairs.jasper', ['id' => $repair->id]) }}" target="_blank" style="width: 100%;"
                    class="btn btn-secondary"><i class="fa fa-print pr-2"></i>Print</a>
            </div>
            @endif
        </div>
        <div class="col-md-2 pl-1 pr-1"></div>
        <div class="col-md-2 pl-1 pr-1"></div>
        <div class="col-md-2">
            <a href="{{ route('repairs.index') }}" style="width: 100%;" class="btn btn-danger"><i
                    class="fa"></i>Back</a>
        </div>
    </div>


    <div class="box box-solid">
        <div class="box-header"></div>

        <div class="box-body with-border">
            @include('dailypro/repairs/form')
        </div>
    </div>

</form>

@endsection