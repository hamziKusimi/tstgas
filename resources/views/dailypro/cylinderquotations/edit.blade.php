@extends('master')

@section('page-form-open', Form::open(['url' => route('cylinderquotations.update', $item->id), 'method' => 'PUT']))
@section('header')
<br>
@include('common.ajax-message')
<br>
@endsection

@section('content')
    @include('dailypro.cylinderquotations.options')
    <br>    
@include('dailypro.components.system-param',[
    'systemParam' => [
        'data-rounding' => 'false',
        'data-use-tax' => 'false',
        'data-date' => isset($item) ? $item->date : '',
        'data-due-date' => isset($item) ? $item->m_duedate : '',
        'data-reference-date' => isset($item) ? $item->refinvdate : '',
        'data-acode' => isset($item) ? $item->account_code: '',
        'data-acode-name' => isset($item) ? $item->name : '',
    ]
])

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

<div class="card">
    <div class="card-body">
        <div class="pl-2 pr-2">
            <div class="row">
                <div class="col-md-5">
                    @include('dailypro.components.bills.left-panel')
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    @include('dailypro.components.bills.right-panel-cyquotation', [
                        'getDocsApiRoute' => '',
                        'except' => [
                            'refInvDate', 'referenceNo', 'amount'
                        ],
                    ])
                </div>
            </div>
        </div>
        <div class="mt-3"></div>
            {{-- @if(isset($systemsetup) && $systemsetup->use_tax == "1")
                @include('dailypro.components.table.cylinderquotations.table', [
                    'items' => $contents,
                    'deleteRoute' => route('cylinderquotations.data.destroy', 'id')
                ])
            @else --}}
                @include('dailypro.components.table.cylinderquotations.table-withouttax', [
                    'items' => $contents,
                    'deleteRoute' => route('cylinderquotations.data.destroy', 'id')
                ])
            {{-- @endif --}}
    </div>
</div>
<div class="mt-3"></div>
<div class="card">
    <div class="card-body">
        <div class="pl-2 pr-2">
            @include('dailypro.cylinderquotations.details')
        </div>
    </div>
</div>

<div class="mt-3"></div>
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.extras')
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.totaling', [
                    'roundingIsEnabled' => false
                ])
            </div>
        </div>
    </div>
</div>

@php
if(Auth::user()->hasPermissionTo('MOD_AFT_PR') || isset($item) && $item->printed_by == null){
    $mod = "true";
}elseif(isset($item) && $item->printed_by != null){
    $mod = "false";
}
@endphp

@endsection
@section('page-form-close', Form::close())

@section('modals')
    @include('dailypro.components.item-modal')
    @include('dailypro.components.template-master-modal')
    @include('dailypro.components.account-modal', [
        'removeDebtorFinder' => true
    ])
    @include('common.modal.search-cylinder')
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script>

@include('common.form.inputmask')
@include('common.form.summernote-sm')

<script type="text/javascript">
(function() {
    @include('dailypro.components.js.debtor-select-js')

    // enable the button if detected the account code has value
    if ($('#debtor-select').val())
        $('.btn-form-submit').prop('disabled', false)

    // prevent # from jumping to top of the screen
    $('a').on('click', function(e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
        }
    })

    //datatable
    $('#stockcode-table').DataTable();

    // cylinderquotation date and due date
    let date = moment.utc(new Date($('.system-settings-param').data('date')))
    let refInvDate = moment.utc(new Date($('.system-settings-param').data('reference-date')))
    let dueDate = moment.utc(new Date($('.system-settings-param').data('due-date')))
    $('#date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#reference_date-picker').datetimepicker({ date: refInvDate, format: 'DD/MM/YYYY' })
    $('#due_date-picker').datetimepicker({ date: dueDate, format: 'DD/MM/YYYY' })

    let postToAccount = 'creditSalesAcc'
    @include('common.billing.js.cylinderinvoice-table-js')
    @include('common.billing.js.left-panel-js')
    @include('common.billing.js.right-panel-js')

    let value = {{ $mod }}
    if(value == false){
        $('input').prop("disabled", true)
        $('select').prop("disabled", true)
        $('.btn-form-submit').prop('disabled', true)
    }

    
    $('#cylinderTab').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{!! route('cylinder.datatable.data') !!}',
            dataFilter: function(response) {
            console.log(response)
                return response;
            },
            error: function (xhr) {
                console.error(xhr.responseJSON);
            }
        },
        columns: [
            { data: 'serial', name: 'Serial' },
            { data: 'barcode', name: 'Barcode' },
            { data: 'descr', name: 'descr' },
            { data: 'product', name: 'product' },
            { data: 'ctype', name: 'ctype' },
            { data: 'id', name: 'id' },
        ],
    });

}) ()


let leftPad = function (value, length) {
    return ('0'.repeat(length) + value).slice(-length);
}

let intTSN = 0
$('.sequence_no').each(function () {
    intTSN += 5
})

function myFunction(element) {
    intTSN += 5
    $("#cylModal").modal("show");
    var row = $('tbody').find('tr')
    row.attr({'data-route':'{{ route("cylinderquotations.editRoute", "ID") }}', 'id':'route2'})   


    $('#cylModal').on('shown.bs.modal', function() {
        let firstRow = $(this).parents('tr')
        let modal = $('#cylModal')    
            modal.on('dblclick', 'tr', function(){

                let serial = $(this).find('td:eq(0)').html();
                let barcode = $(this).find('td:eq(1)').html();
                let descr = $(this).find('td:eq(2)').html();
                let product = $(this).find('td:eq(3)').html();
                let type = $(this).find('td:eq(4)').html();
                let id = $(this).find('td:eq(5)').html();
                console.log(id)
                let getRoute = '{{ route("cylinderquotations.editRoute", "ID") }}'
                let route = getRoute.replace('ID', id)
                $.ajax({
                        url: route,
                        method: 'GET',
                    }).done(function(resources) {
                        // resources.forEach(function (resource) {     
                            let tsnvalue = intTSN - 5
                            let tsnvalueminus = intTSN - 10
                            tsnvalue = leftPad(tsnvalue, 4)
                            tsnvalueminus = leftPad(tsnvalue, 4)
                            // $('#table-form-tbody').find('.'+tsnvalueminus).hide()
                            console.log(tsnvalue)
                            let select = $('tbody').find('.'+tsnvalue)

                            
                            select.find('.serial').val(serial)
                            select.find('.barcode').val(barcode)
                            select.find('.subject').val(descr)
                            select.find('.product').val(product)
                            select.find('.type').val(type)
                            select.find('.quantity').val(1)
                            select.find('.unit_price').val(0)
                            select.find('.discount').val(0)

                            $('.btn-form-submit').prop('disabled', false)
                            $("#add-new").removeClass('d-none')
                            $("#cylModal").modal("hide");
                            select.find('#search-cyl-btn').prop('hidden', true)
                    })        

                    $('#cylModal').modal('hide');
            })
        
            //     $('#cylinderTab tbody').on('click', 'tr', function () {
            //     serial = $(this).find('td:eq(0)').html();
            //     barcode = $(this).find('td:eq(1)').html();
            //     descr = $(this).find('td:eq(2)').html();
            //     product = $(this).find('td:eq(3)').html();
            //     type = $(this).find('td:eq(4)').html();

            // });
        
    })

    
}


</script>
@endpush
