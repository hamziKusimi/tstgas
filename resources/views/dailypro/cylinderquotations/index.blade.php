@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('CY_QT_CR'))
    <div class="col-3" style="flex: 0 0 14%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('cylinderquotations.create') }}"> Add Quotation</a>
        </div>
    </div>
    @endif
    @if(Auth::user()->hasPermissionTo('CY_QT_PR') && $print->isEmpty())
    <div class="col-2" style="flex: 0 0 14%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @elseif(Auth::user()->hasPermissionTo('CY_QT_PRY'))
    <div class="col-2" style="flex: 0 0 14%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
    <div class="row">
        @if(isset($cylinderquotationsearch))
            <div class="col-md-2">
                <a href="{{ route('cylinderquotations.index') }}" style="width: 100%;"
                        class="btn btn-danger"><i class="fa"></i>Back</a>
            </div>
            <div class="col-md-10" style="text-align: right;">
        @else
            <div class="col-md-12" style="text-align: right;">
        @endif
            <form action ="{{ route('cylinderquotations.searchindex') }}" method="POST">
            @csrf
                <input name="search" type="text" id="search" placeholder="Search " class="search"
                data-get-search="{{ route('ajax.index.get.search','search') }}">
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="QuotTab" class="table table-bordered table-striped" cellspacing="0" ">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Attention</th>
                            <th>Contact</th>
                            <th>CB/Inv/Do No.</th>
                            <th>cylinderQuotation No.</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                {{-- @if(sizeof($cylinderquotations) > 0)     --}}
                    <tbody> 
                    @if(isset($cylinderquotationsearch))
                        @foreach ($cylinderquotationsearch as $cylinderquotation)
                        <tr>
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('CY_QT_UP'))
                            <td><a href=" {{ route('cylinderquotations.edit',$cylinderquotation->id) }}">{{ $cylinderquotation->docno }}</a>
                            </td>
                            @else
                            <td>{{ $cylinderquotation->docno }}</td>
                            @endif
                            <td>{{  isset($cylinderquotation->date)?date('d-m-Y', strtotime($cylinderquotation->date)):'' }}</td>
                            <td>{{ $cylinderquotation->account_code}}</td>
                            <td>{{ $cylinderquotation->name }}</td>
                            <td>{{ $cylinderquotation->attention }}</td>
                            <td>{{ $cylinderquotation->contact }}</td>
                            <td>{{ $cylinderquotation->cbinvdono }}</td>
                            <td>{{ $cylinderquotation->quot }}</td>
                            <td>{{ $cylinderquotation->taxed_amount }}</td>
                            @if(Auth::user()->hasPermissionTo('CY_QT_DL'))
                            <td>
                                <a href="{{ route('cylinderquotations.destroy', $cylinderquotation->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @endforeach
                    @else
                        @foreach ($cylinderquotations as $cylinderquotation)
                        <tr>
                            <td>{{ ++$i }}</td>
                            @if(Auth::user()->hasPermissionTo('CY_QT_UP'))
                            <td><a href=" {{ route('cylinderquotations.edit',$cylinderquotation->id) }}">{{ $cylinderquotation->docno }}</a>
                            </td>
                            @else
                            <td>{{ $cylinderquotation->docno }}</td>
                            @endif
                            <td>{{  isset($cylinderquotation->date)?date('d-m-Y', strtotime($cylinderquotation->date)):'' }}</td>
                            <td>{{ $cylinderquotation->account_code}}</td>
                            <td>{{ $cylinderquotation->name }}</td>
                            <td>{{ $cylinderquotation->attention }}</td>
                            <td>{{ $cylinderquotation->contact }}</td>
                            <td>{{ $cylinderquotation->cbinvdono }}</td>
                            <td>{{ $cylinderquotation->quot }}</td>
                            <td>{{ $cylinderquotation->taxed_amount }}</td>
                            @if(Auth::user()->hasPermissionTo('CY_QT_DL'))
                            <td>
                                <a href="{{ route('cylinderquotations.destroy', $cylinderquotation->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                            </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                    {{-- @endif --}}
                </table>
            </div>
            @if(isset($cylinderquotations))
                {{ $cylinderquotations->links() }}
            @endif
        </div>
    </div>
</div>


{{-- {!! $cylinderquotations->links() !!} --}}
@include('dailypro.cylinderquotations.printmodal')
@endsection

@push('scripts')
<script>
    (function() {
        
    // $('#QuotTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#CB_INV_DO_Chkbx_1").prop("checked") == true){
        $("#CB_INV_DO_frm_1").prop('disabled', false);
        $("#CB_INV_DO_to_1").prop('disabled', false);
    }else{
        $("#CB_INV_DO_frm_1").prop('disabled', true);
        $("#CB_INV_DO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#CB_INV_DO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#CB_INV_DO_frm_1").prop('disabled', false);
            $("#CB_INV_DO_to_1").prop('disabled', false);
        }else{
            $("#CB_INV_DO_frm_1").prop('disabled', true);
            $("#CB_INV_DO_to_1").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx_2").change(function() {
        if(this.checked) {
            $("#refNo_frm_2").prop('disabled', false);
            $("#refNo_to_2").prop('disabled', false);
        }else{
            $("#refNo_frm_2").prop('disabled', true);
            $("#refNo_to_2").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });
}) ()
</script>
@endpush