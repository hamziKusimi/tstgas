<div class="row">
    <br><br>
    <ul class="nav nav-pills rounded border border-dark" style="background-color:#3c8dbc">
        <li class="nav-item">
            <a href="#" data-toggle="collapse" data-target="#RC" class="nav-link">Replacement Cost</a>
        </li>
        <li class="nav-item">
            <a href="#" data-toggle="collapse" data-target="#OTC" class="nav-link">Other Terms & Condition</a>
        </li>
    </ul>
</div>
<div class="tab-content">
    <div class="collapse padding-item" id="RC">
        <div class="input-group row narrow-padding-global col-md-12">
            <label for="use_tax" class="col-md-6">Damaged / Lost / High Corroded Cylinder</label>
            <div class="col-md-6">
                <div class="input-group-prepend">   
                    <input name="custom1" id="custom1" type="hidden" maxlength="100" class="form-control custom1"
                        value="Damaged / Lost / High Corroded Cylinder">
                    <input name="customval1" id="customval1" type="text" maxlength="100" class="form-control customval1"
                        value="{{ isset($item)?$item->customval1:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom1" class="col-md-6">Protective Caps Damaged / Lost Chargeable</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom2" id="custom2" type="hidden" maxlength="100" class="form-control custom2"
                        value="Protective Caps Damaged / Lost Chargeable">
                    <input name="customval2" id="customval2" type="text" maxlength="100" class="form-control customval2"
                        value="{{ isset($item)?$item->customval2:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom2" class="col-md-6">Cylinder Valve Damage Chargeable</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom3" id="custom3" type="hidden" maxlength="100" class="form-control custom3"
                        value="Cylinder Valve Damage Chargeable">
                    <input name="customval3" id="customval3" type="text" maxlength="100" class="form-control customval3"
                        value="{{ isset($item)?$item->customval3:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Cylinder Valve Handle Damaged Chargeable</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom4" id="custom4" type="hidden" maxlength="100" class="form-control custom4"
                        value="Cylinder Valve Handle Damaged Chargeable">
                    <input name="customval4" id="customval4" type="text" maxlength="100" class="form-control customval4"
                        value="{{ isset($item)?$item->customval4:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of 4-Legged Sling Set (BS)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom5" id="custom5" type="hidden" maxlength="100" class="form-control custom5"
                        value="Damaged / Lost Of 4-Legged Sling Set (BS)">
                    <input name="customval5" id="customval5" type="text" maxlength="100" class="form-control customval5"
                        value="{{ isset($item)?$item->customval5:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of 4-Legged Lifting Sling Set (DNV)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom6" id="custom6" type="hidden" maxlength="100" class="form-control custom6"
                        value="Damaged / Lost Of 4-Legged Lifting Sling Set (DNV)">
                    <input name="customval6" id="customval6" type="text" maxlength="100" class="form-control customval6"
                        value="{{ isset($item)?$item->customval6:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of 4-3/4 Ton Shackle (BS)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom7" id="custom7" type="hidden" maxlength="100" class="form-control custom7"
                        value="Damaged / Lost Of 4-3/4 Ton Shackle (BS)">
                    <input name="customval7" id="customval7" type="text" maxlength="100" class="form-control customval7"
                        value="{{ isset($item)?$item->customval7:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of 4-3/4 Ton Shackle (DNV)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom8" id="custom8" type="hidden" maxlength="100" class="form-control custom8"
                        value="Damaged / Lost Of 4-3/4 Ton Shackle (DNV)">
                    <input name="customval8" id="customval8" type="text" maxlength="100" class="form-control customval8"
                        value="{{ isset($item)?$item->customval8:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of Standard Gas Rack (BS)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom9" id="custom9" type="hidden" maxlength="100" class="form-control custom9"
                        value="Damaged / Lost Of Standard Gas Rack (BS)">
                    <input name="customval9" id="customval9" type="text" maxlength="100" class="form-control customval9"
                        value="{{ isset($item)?$item->customval9:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of Standard Gas Rack (DNV)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom10" id="custom10" type="hidden" maxlength="100" class="form-control custom10"
                        value="Damaged / Lost Of Standard Gas Rack (DNV)">
                    <input name="customval10" id="customval10" type="text" maxlength="100" class="form-control customval10"
                        value="{{ isset($item)?$item->customval10:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of Manifold (BS)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom11" id="custom11" type="hidden" maxlength="100" class="form-control custom11"
                        value="Damaged / Lost Of Manifold (BS)">
                    <input name="customval11" id="customval11" type="text" maxlength="100" class="form-control customval11"
                        value="{{ isset($item)?$item->customval11:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged /  Lost Of Manifold Pallet (BS)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom12" id="custom12" type="hidden" maxlength="100" class="form-control custom12"
                        value="Damaged /  Lost Of Manifold Pallet (BS)">
                    <input name="customval12" id="customval12" type="text" maxlength="100" class="form-control customval12"
                        value="{{ isset($item)?$item->customval12:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of Manifold (DNV)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom13" id="custom13" type="hidden" maxlength="100" class="form-control custom13"
                        value="Damaged /  Lost Of Manifold Pallet (DNV)">
                    <input name="customval13" id="customval13" type="text" maxlength="100" class="form-control customval13"
                        value="{{ isset($item)?$item->customval13:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Damaged / Lost Of Manifold Pallet (DNV)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom14" id="custom14" type="hidden" maxlength="100" class="form-control custom14"
                        value="Damaged / Lost Of Manifold Pallet (DNV)">
                    <input name="customval14" id="customval14" type="text" maxlength="100" class="form-control customval14"
                        value="{{ isset($item)?$item->customval14:'' }}">
                </div>
            </div>
        </div>
        <div>
            <hr>    
        </div>

    </div>
</div>
<div class="tab-content">
    <div class="collapse padding-item" id="OTC">
        <div class="input-group row narrow-padding-global col-md-12">
            <label for="use_tax" class="col-md-6">Delivery Time</label>
            <div class="col-md-6">
                <div class="input-group-prepend">   
                    <input name="custom15" id="custom15" type="hidden" maxlength="100" class="form-control custom15"
                        value="Delivery Time">
                    <input name="customval15" id="customval15" type="text" maxlength="100" class="form-control customval15"
                        value="{{ isset($item)?$item->customval15:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom1" class="col-md-6">Delivery Term</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom16" id="custom16" type="hidden" maxlength="100" class="form-control custom16"
                        value="Delivery Term">
                    <input name="customval16" id="customval16" type="text" maxlength="100" class="form-control customval16"
                        value="{{ isset($item)?$item->customval16:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom2" class="col-md-6">Payment Term</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom17" id="custom17" type="hidden" maxlength="100" class="form-control custom17"
                        value="Payment Term">
                    <input name="customval17" id="customval17" type="text" maxlength="100" class="form-control customval17"
                        value="{{ isset($item)?$item->customval17:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Credit Limit</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom18" id="custom18" type="hidden" maxlength="100" class="form-control custom18"
                        value="Credit Limit">
                    <input name="customval18" id="customval18" type="text" maxlength="100" class="form-control customval18"
                        value="{{ isset($item)?$item->customval18:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Cylinder Returned</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom19" id="custom19" type="hidden" maxlength="100" class="form-control custom19"
                        value="Cylinder Returned">
                    <input name="customval19" id="customval19" type="text" maxlength="100" class="form-control customval19"
                        value="{{ isset($item)?$item->customval19:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Price</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom20" id="custom20" type="hidden" maxlength="100" class="form-control custom20"
                        value="Price">
                    <input name="customval20" id="customval20" type="text" maxlength="100" class="form-control customval20"
                        value="{{ isset($item)?$item->customval20:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Contact</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom21" id="custom21" type="hidden" maxlength="100" class="form-control custom21"
                        value="Contact">
                    <input name="customval21" id="customval21" type="text" maxlength="100" class="form-control customval21"
                        value="{{ isset($item)?$item->customval21:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Extra Charge for Delivery to ASB (If Any)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom22" id="custom22" type="hidden" maxlength="100" class="form-control custom22"
                        value="Extra Charge for Delivery to ASB (If Any)">
                    <input name="customval22" id="customval22" type="text" maxlength="100" class="form-control customval22"
                        value="{{ isset($item)?$item->customval22:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Other T&C</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom23" id="custom23" type="hidden" maxlength="100" class="form-control custom23"
                        value="Other T&C">
                    <input name="customval23" id="customval23" type="text" maxlength="100" class="form-control customval23"
                        value="{{ isset($item)?$item->customval23:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Extension Period (If Any)</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom24" id="custom24" type="hidden" maxlength="100" class="form-control custom24"
                        value="Extension Period (If Any)">
                    <input name="customval24" id="customval24" type="text" maxlength="100" class="form-control customval24"
                        value="{{ isset($item)?$item->customval24:'' }}">
                </div>
            </div>
        </div>
        <div class="input-group row narrow-padding-global">
            <label for="uom3" class="col-md-6">Validity</label>
            <div class="col-md-6">
                <div class="input-group-prepend">
                    <input name="custom25" id="custom25" type="hidden" maxlength="100" class="form-control custom25"
                        value="Validity">
                    <input name="customval25" id="customval25" type="text" maxlength="100" class="form-control customval25"
                        value="{{ isset($item)?$item->customval25:'' }}">
                </div>
            </div>
        </div>
        <div>
            <hr>    
        </div>

    </div>
</div>