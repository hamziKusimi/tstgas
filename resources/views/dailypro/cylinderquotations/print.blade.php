<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <style type="text/css" media="screen,print">
        @page {
            margin: 100px 25px;
        }

        .new-page {
            page-break-before: always;
        }

        table {
            font-size: 14px;
        }

        .last {
            position: absolute;
            bottom: -20px;
            left: 0px;
            right: 0px;
            height: 100px;
            background-color: green;
        }

        .tfoot{
            position:absolute;
            bottom:0;
            width:100%;
            height:60px;   /* Height of the footer */
            background:#6cf;
            margin-top: -200px;
        }

    </style>
    {{-- <link rel="stylesheet" href="{{ asset('css/bootstrap-3.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/pdf.css') }}">
</head>

@php ($i = 0) @endphp
@php ($y = 1) @endphp
@php ($x = 1) @endphp
<body>

 <table class="table" cellspacing="0" width="100%" height="100%" autosize="1">
    <thead>
        <tr>
            <h1 style="text-align:center; font-weight:bold;margin: 0px;">{{ config('config.company.name') }}</h1>
            <p style="text-align:center;margin: 0px;">{{ config('config.company.company_no') }}</p>
            @if(isset($sytemsetups))
                <p style="text-align:center;margin: 0px;">{{ $sytemsetups->address1 }}</p>
                <p style="text-align:center;margin: 0px;">{{ $sytemsetups->address2 }}</p>
                <p style="text-align:center;margin: 0px;">{{ $sytemsetups->tel }}</p>
                <p style="text-align:center;margin: 0px;">{{ $sytemsetups->fax }}</p>
                <p style="text-align:center;margin: 0px;">{{ $sytemsetups->email }}</p>
            @endif
            <h2 style="text-align: center;text-decoration: underline;">QUOTATION/AGREEMENT</h2>
        </tr>
        @if(isset($items))
        <tr>
            <td width="10%" style="border:1px solid;border-right:none;border-bottom:none;">
                <p style="margin: 0px;">To</p>
            </td>
            <td width="45%" style="border:1px solid;border-left:none;border-bottom:none;">
                <p style="margin: 0px;">{{ $items->name }}</p>
            </td>
            <td width="10%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;border-bottom:none">
                    <p style="margin: 0px;">From</p>
            </td>
            <td width="35%" style="text-align:left; border:1px solid;border-left:none;border-bottom:none">
                <p style="margin: 0px;">: {{ $items->docno }}</p>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border:1px solid;border-right:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;"></p><br>
                <p style="margin: 0px;"></p>
            </td>
            <td width="45%" style="border:1px solid;border-left:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">{{ $items->addr1 }}{{ $items->addr2 }}{{ $items->addr3 }}{{ $items->addr4 }}</p>
            </td>
            <td width="10%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;border-top:none;border-bottom:none">
                    <p style="margin: 0px;"></p>
                    <p style="margin: 0px;"></p>
            </td>
            <td width="35%" style="text-align:left; border:1px solid;border-left:none;border-top:none;border-bottom:none">
                <p style="margin: 0px;"></p>
                <p style="margin: 0px;"></p>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border:1px solid;border-right:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">Attn</p>
            </td>
            <td width="45%" style="border:1px solid;border-left:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">{{ $items->tel_no }}</p>
            </td>
            <td width="10%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;border-top:none;border-bottom:none">
                    <p style="margin: 0px;">Tel</p>
            </td>
            <td width="35%" style="text-align:left; border:1px solid;border-left:none;border-top:none;border-bottom:none">
                <p style="margin: 0px;">: {{ $items->tel_no }}</p>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border:1px solid;border-right:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">Tel</p>
            </td>
            <td width="45%" style="border:1px solid;border-left:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">{{ $items->tel_no }}</p>
            </td>
            <td width="10%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;border-top:none;border-bottom:none">
                    <p style="margin: 0px;">Fax</p>
            </td>
            <td width="35%" style="text-align:left; border:1px solid;border-left:none;border-top:none;border-bottom:none">
                <p style="margin: 0px;">: {{ $items->fax_no }}</p>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border:1px solid;border-right:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">H/P No</p>
            </td>
            <td width="45%" style="border:1px solid;border-left:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">{{ $items->tel_no }}</p>
            </td>
            <td width="10%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;border-top:none;border-bottom:none">
                    <p style="margin: 0px;">Date</p>
            </td>
            <td width="35%" style="text-align:left; border:1px solid;border-left:none;border-top:none;border-bottom:none">
                <p style="margin: 0px;">: {{ $items->date }}</p>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border:1px solid;border-right:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">Your Ref. No</p>
            </td>
            <td width="45%" style="border:1px solid;border-left:none;border-bottom:none;border-top:none;">
                <p style="margin: 0px;">{{ $items->reference_no }}</p>
            </td>
            <td width="10%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;border-top:none;border-bottom:none">
                    <p style="margin: 0px;">Total Page</p>
            </td>
            <td width="35%" style="text-align:left; border:1px solid;border-left:none;border-top:none;border-bottom:none">
                <p style="margin: 0px;">: {{ $items->reference_no }}</p>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border:1px solid;border-right:none;border-top:none;">
                <p style="margin: 0px;"></p><br>
            </td>
            <td width="45%" style="border:1px solid;border-left:none;border-top:none;">
                <p style="margin: 0px;"></p>
            </td>
            <td width="10%" style="text-align:left;border-top:1px solid;border-bottom:1px solid;border-top:none;">
                    <p style="margin: 0px;">Our Ref. No.</p>
            </td>
            <td width="35%" style="text-align:left; border:1px solid;border-left:none;border-top:none;">
                <p style="margin: 0px;">: {{ $items->reference_no }}</p>
            </td>
        </tr>
        
        @endif  
        <tr>
            <td colspan="4">
                <br>
                Dear Sir / Mdm, <br>
                {!!  $items->header !!} 
            </td>
        </tr>
     </thead>
     <tbody>
        <tr>
            <td colspan="7"><h3 style="text-decoration: underline;">A. Sales of Gases In  Cylinder c/w Cap & Rental of Cylinder</h3></td><br>
        </tr>
        <tr>
            <table border="1" class="table" cellspacing="0" width="100%" style="border-color:black;border-style:solid;border-left:none;border-right:none;border-bottom:none;" autosize="1">
                <thead bgcolor="#808080">
                    <th style="">ITEM NO</th>  
                    <th style="" colspan = "2">DESCRIPTION</th>
                    <th style="">QUANTITY</th>
                    <th style="">UOM</th>
                    <th style="">UNIT PRICE</th>
                    <th width="12%" style="">AMOUNT</th>
                </thead>
                <tbody>
                    @foreach ($contents as $content)   
                        <tr>
                            <td style="text-align:center;">{{ ++$i }}
                                </td> 
                            <td style="" colspan="2">{{ $content->subject }}</td>
                            <td style="text-align:center;">{{ $content->quantity }}</td>
                            <td style="text-align:center;">{{ $content->uom }}</td>
                            <td style="">{{ $content->unit_price }}</td>
                            <td style="">{{ $content->amount }}</td>                                
                        </tr>

                        
                    @endforeach
                    
                    <tr>
                        <td style="border-left:none;border-right:none;" colspan="7" ><h3 style="text-decoration: underline;">B. Replacement Cost</h3></td>
                    </tr>
                        @if($items->customval1 != null && $items->customval1 != '')
                            <tr>
                                <td style="text-align:center;">{{ $y++ }}</td> 
                                <td style="" colspan = "2">{{ $items->custom1 }}</td>
                                <td colspan="4" style="text-align:left;">: {{ $items->customval1 }}</td>                               
                            </tr>
                        @endif
                        @if($items->customval2 != null && $items->customval2 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom2 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval2 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval3 != null && $items->customval3 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom3 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval3 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval4 != null && $items->customval4 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom4 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval4 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval5 != null && $items->customval5 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom5 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval5 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval6 != null && $items->customval6 != '')
                            <tr>
                                <td style="text-align:center;">{{ $y++ }}</td> 
                                <td style="" colspan = "2">{{ $items->custom6 }}</td>
                                <td colspan="4" style="text-align:left;">: {{ $items->customval6 }}</td>                               
                            </tr>
                        @endif
                        @if($items->customval7 != null && $items->customval7 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom7 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval7 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval8 != null && $items->customval8 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom8 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval8 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval9 != null && $items->customval9 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom9 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval9 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval10 != null && $items->customval10 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom10 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval10 }}</td>                               
                        </tr>
                        @endif     @if($items->customval11 != null && $items->customval11 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom11 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval11 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval12 != null && $items->customval12 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom12 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval12 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval13 != null && $items->customval13 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom13 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval13 }}</td>                               
                        </tr>
                        @endif
                        @if($items->customval14 != null && $items->customval14 != '')
                        <tr>
                            <td style="text-align:center;">{{ $y++ }}</td> 
                            <td style="" colspan = "2">{{ $items->custom14 }}</td>
                            <td colspan="4" style="text-align:left;">: {{ $items->customval14 }}</td>                               
                        </tr>
                        @endif
                      
                    <tr>
                        <td style="border:none;" colspan="7" ><h3 style="text-decoration: underline;">C. Other Terms & Conditions</h3></td>
                    </tr>  
                    @if($items->customval15 != null && $items->customval15 != '')
                    <tr>
                        <td style="text-align:center;border:none;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom15 }}</td>
                        <td colspan="4" style="text-align:left;border:none;">: {{ $items->customval15 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval16 != null && $items->customval16 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom16 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval16 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval17 != null && $items->customval17 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom17 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval17 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval18 != null && $items->customval18 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom18 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval18 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval19 != null && $items->customval19 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom19 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval19 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval20 != null && $items->customval20 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom20 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval20 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval21 != null && $items->customval21 != '')
                        <tr>
                            <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                            <td style="border:none;">{{ $items->custom21 }}</td>
                            <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval21 }}</td>                               
                        </tr>
                    @endif
                    @if($items->customval22 != null && $items->customval22 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom22 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval22 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval23 != null && $items->customval23 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom23 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval23 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval24 != null && $items->customval24 != '')
                    <tr>
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom24 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval24 }}</td>                               
                    </tr>
                    @endif
                    @if($items->customval25 != null && $items->customval25 != '')
                    <tr style="border:none">
                        <td style="border:none;text-align:center;">{{ $x++ }}</td> 
                        <td style="border:none;">{{ $items->custom25 }}</td>
                        <td colspan="4" style="border:none;text-align:left;">: {{ $items->customval25 }}</td>                               
                    </tr>
                    @endif
                </tbody>
            </table>
        </tr>
        
     </tbody>
     <tfoot class="tfoot" style="font-size: 11px;">
        <table class="table" cellspacing="0" width="100%" height="100%" autosize="1"> 
            <tr>
                <td width="" style="" colspan="2">
                    <p style="font-size: 12px"></p>
                </td>       
            </tr>  
            <tr>
                <td width="" style="" colspan="2">
                    <p style="font-size: 12px"></p>
                </td>       
            </tr>            
            <tr>
                <td width="" style="" colspan="2">
                    Thank You,
                </td>     
            </tr>
            <tr>
                <td width="30%" style="">
                </td>               
                <td width="45%" style="">
                </td>           
                <td width="25%" style="">
                    Confirmed and Accepted by,
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                    Yours Faithfully,
                </td>               
                <td width="45%" style="">
                </td>           
                <td width="25%" style="">
                    <i style="font-size: 12px;font-weight: bold;">{{ $items->name }}</i>
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                    <i style="font-size: 12px;font-weight: bold;">For {{  config('config.company.name') }}</i>
                </td>              
                <td width="45%" style="">
                </td>
                <td width="25%" style="">
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                    <p></p>
                </td>              
                <td width="45%" style="">
                    <p></p>
                </td>
                <td width="25%" style="">
                    <p></p>
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                    <i style="font-size: 12px;font-weight: bold;">..................................................................</i>
                </td>              
                <td width="45%" style="">
                </td>
                <td width="25%" style="">
                    <i style="font-size: 12px;font-weight: bold;">..................................................................</i>
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                    {{ Auth::user()->name }}
                </td>              
                <td width="45%" style="">
                </td>
                <td width="25%" style="">
                    Sign and Stamp
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                    {{ Auth::user()->role }}
                </td>              
                <td width="45%" style="">
                </td> 
                <td width="25%" style="">
                    Name: 
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                </td>                
                <td width="45%" style="">
                </td>
                <td width="25%" style="">
                    I/C No.: 
                </td>
            </tr>
            <tr>
                <td width="30%" style="">
                </td>
                <td width="45%" style="">
                </td>
                <td width="25%" style="">
                    Date.: 
                </td>
            </tr>
        </table>
    </tfoot>
</table>



</body>