<div class="row">
    <div class="col-md-2">
        @if(Auth::user()->hasPermissionTo('QUOTATION_CR') || Auth::user()->hasPermissionTo('QUOTATION_UP'))
        @include('common.billing.options.submit', [
        'faIcon' => 'share-square-o',
        'color' => 'bg-dark-blue text-white',
        'permissions' => ['Create Quotation', 'Update Quotations'],
        'name' => 'Submit'
        ])
        @endif
    </div>
    <div class="col-md-2" style="padding-left:0px">
        @if(Auth::user()->hasPermissionTo('QUOTATION_PR') && (isset($item) && $item->printed_by == null))
            @if (isset($item->id))
            <a href="{{ route('cylinderquotations.print', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
                class="btn btn-secondary"><i class="fa fa-print pr-2"></i>Print</a>
            @endif
        @elseif(Auth::user()->hasPermissionTo('QUOTATION_PRY'))
            @if (isset($item->id))
            <a href="{{ route('cylinderquotations.print', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
                class="btn btn-secondary"><i class="fa fa-print pr-2"></i>Print</a>
            @endif
        @endif
    </div>
    <div class="col-md-2 pr-1">
    </div>
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2 pl-1 pr-1"></div>
    <div class="col-md-2">
        <a href="{{ route('cylinderquotations.index') }}" style="width: 100%;" class="btn btn-danger"><i class="fa"></i>Back</a>
    </div>
</div>