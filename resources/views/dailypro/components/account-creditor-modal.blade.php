<div class="modal fade" id="createAccountModalCreditor" tabindex="-1" role="dialog" aria-hidden="true"  data-route="{{ route('creditors.api.store') }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New Account</h5>
                <button type="button" class="close" data-dismiss="modal">&times</button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '', 'method' => 'POST', 'class' => 'account-modal-form']) !!}
                    @include('stockmaster.creditors.form')
                    <div class="form-group row">
                        <div class="col-md-8"></div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary form-control submit" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>