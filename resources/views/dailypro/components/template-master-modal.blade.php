<div class="modal fade" id="template-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Prefilled Templates</h5>
                <button type="button" class="close" data-dismiss="modal">&times</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="template-master-datatable">
                        <thead>
                            <tr class="bg-primary text-white">
                                <th width="95%">Description</th>
                                <th width="5%">Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($templates))
                                @foreach ($templates as $index => $template)
                                    <tr class="template-row" data-param="{{ $template }}">
                                        <td>{!! $template->T_DESC !!}</td>
                                        <td>{{ config('kusimi.template-category')[$template->T_CATEGORY] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
(function() {
    $('#template-master-datatable').DataTable()
    $('#template-modal').on('show.bs.modal', function (event) {
        let modal = $(this)
        let button = $(event.relatedTarget)                     // Button that triggered the modal
        let textareaTarget = button.data('textarea-target')     // Extract info from data-* attributes

        // onclick on any of the rows in this modal
        modal.find('.template-row').on('click', function() {
            let content = $(this).data('param')
            $(textareaTarget).summernote('code', content.T_DESC)

            modal.modal('hide')
            modal.on('hidden.bs.modal', function (e) {
                modal.find('.template-row').off('click')
            })
        })
    })
}) ()
</script>
@endpush
