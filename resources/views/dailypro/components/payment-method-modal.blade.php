<div class="modal fade payment-method-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Payment Method Master</h5>
            </div>
            <div class="modal-body">
                {{-- This form is using AJAX --}}
                <div class="payment-form" data-url="{{ route('payment-methods.update', 'ID') }}" data-method="PUT">
                    <div class="form-group row">
                        {!! Form::label('P_CC_BANK_GL_ID', 'Credit Card Bank GL', ['class' => 'col-md-3 col-form-label']) !!}
                        <div class="col-md-9">
                            {!! Form::select('P_CC_BANK_GL_ID', $masterCodes->pluck('d_detail', 'd_acode'), null, [
                                'class' => 'form-control custom-select2 card_bank',
                                'placeholder' => '',
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('P_CC_ACCOUNT_NO', 'Credit Card Account No', ['class' => 'col-md-3 col-form-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('P_CC_ACCOUNT_NO', null, [ 'class' => 'form-control credit_card_account' ]) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('P_CHQ_BANK_GL_ID', 'Cheque Bank GL', ['class' => 'col-md-3 col-form-label']) !!}
                        <div class="col-md-9">
                            {!! Form::select('P_CHQ_BANK_GL_ID', $masterCodes->pluck('d_detail', 'd_acode'), null, [
                                'class' => 'form-control custom-select2 cheque_bank',
                                'placeholder' => '',
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('P_TT_BANK_GL_ID', 'TT Bank GL', ['class' => 'col-md-3 col-form-label']) !!}
                        <div class="col-md-9">
                            {!! Form::select('P_TT_BANK_GL_ID', $masterCodes->pluck('d_detail', 'd_acode'), null, [
                                'class' => 'form-control custom-select2 tt_bank',
                                'placeholder' => '',
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('P_ONLINE_BANK_GL_ID', 'Online Bank GL', ['class' => 'col-md-3 col-form-label']) !!}
                        <div class="col-md-9">
                            {!! Form::select('P_ONLINE_BANK_GL_ID', $masterCodes->pluck('d_detail', 'd_acode'), null, [
                                'class' => 'form-control custom-select2 online_bank',
                                'placeholder' => '',
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            {!! Form::button('Save', ['class' => 'btn btn-primary form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
