<div class="modal fade reportdocno-modal" id="reportdocno-modal" tabindex="-1" role="dialog" aria-hidden="true"  data-get-route="{{ route('cashbills.docno.frm') }}">
    {{-- This should only show for related documents (if this is Invoice, the results should be just for Invoice) --}}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Documents</h4>
            </div>
            <div class="modal-body">
                {{-- <form action="" method="POST">
                  @csrf
                  <div class="col-md-12" style="text-align: right;">
                      <input name="searchdoc" type="text" id="searchdoc" placeholder="Search Document No" class="searchdoc">
                      <button type="button" class="btn btn-primary searchdoc1">Search</button>
                  </div>
                </form>       --}}
                <table id="cashbillcodetable" class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Doc. No</th>
                            <th>Date</th>
                            <th>Debtor</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button id="close-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

