<div class="system-settings-param"
@if (isset($systemParam) && count($systemParam) > 0)
    @foreach ($systemParam as $key => $param)
        {{ $key.'='.$param.'' }}
    @endforeach
@endif
></div>
