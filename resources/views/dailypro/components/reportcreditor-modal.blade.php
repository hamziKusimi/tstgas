<div class="modal fade reportcreditor-modal" id="reportcreditor-modal" tabindex="-1" role="dialog" aria-hidden="true"  data-get-route="{{ route('cashbills.docno.frm') }}">
    {{-- This should only show for related documents (if this is Invoice, the results should be just for Invoice) --}}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Creditor</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered" id="creditorlisttable" width="100%">
                    <thead>
                        <tr>
                            <th>Creditor</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button id="close-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>