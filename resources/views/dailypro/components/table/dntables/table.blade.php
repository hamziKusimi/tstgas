<div class="tax-container2"></div>
<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="12%" class="text-center">D/N No.</th>
                <th width="12%" class="text-center">Serial</th>
                <th width="18%" class="text-center">Barcode</th>
                <th width="24%" class="text-left">Description</th>
                <th width="8%" class="text-center">Qty</th>
                <th width="8%" class="text-center">Price</th>
                <th width="8%" class="text-center">Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="modal-table-form-tbody">
            @include('dailypro.components.table.dntables.form')
            {{-- @includeWhen(isset($items), 'dailypro.components.table.dntables.row') --}}
        </tbody>
        {{-- <tfoot>
            <tr>
                <td colspan="2">
                    <button type="button" class="form-control btn bg-dark-green text-white" id="modal-add-new">
                        Add Line
                    </button>
                </td>
                <td colspan="8"></td>
            </tr>
        </tfoot> --}}
    </table>
</div>

