<tr class="narrow-padding modal-template-new d-none">
    <td>
        {!! Form::text('dndn_no[]', null, ['class' => 'form-control dndn_no', 'disabled']) !!}
    </td>
    {{-- {{ dd($cylinder) }} --}}
    <td>
        {!! Form::text('dserial[]', null, [
            'class' => 'form-control dserial calculate-field2',
            'placeholder' => '',
            'data-contents' => $cylinder, 'disabled'
            // 'data-route' => route('item-master.api.store')
        ]) !!}
        {!! Form::hidden('daccount_code[]', null, ['class' => 'daccount_code']) !!}
    </td>
    <td>
        {!! Form::text('dbarcode[]', null, ['class' => 'form-control dbarcode']) !!}
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('dsubject[]', null, ['class' => 'form-control dsubject', 'disabled']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#row-detail-id">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand" ></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::number('dquantity[]', null, ['class' => 'form-control dquantity calculate-field2', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('dsupp-price[]', null, ['class' => 'form-control dsupp-price calculate-field2']) !!}
    </td>
    <td>
        {!! Form::text('damount[]', null, ['class' => 'form-control damount calculate-field2', 'disabled']) !!}
        {!! Form::hidden('damount_dt[]', null, ['class' => 'form-control damount_dt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>