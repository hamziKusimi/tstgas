<tr class="narrow-padding template-new d-none">
    <td>
        {!! Form::hidden('item_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('sequence_no[]', null, ['class' => 'form-control sequence_no']) !!}
    </td>
    <td>
        {{-- {!! Form::select('serial[]', [], null, [
        'class' => 'form-control serial calculate-field',
        'placeholder' => '',
        'style' => 'width:100%; height:100%',
        'data-contents' => $product,
        'data-get-cylinder-route' => route('ajax.cylinder.get.by-serial', 'SERIAL')
        ]) !!} --}}
        <div class="input-group mb-3">
            <input name="serial[]" type="text" id="search-cyl" class="form-control serial calculate-field"
                placeholder="Search">
            <div class="input-group-append">
                <button class="btn btn-primary" onclick="myFunction(this)" type="button" id="search-cyl-btn">
                    <small>
                        <i class="fa fa-search"></i>
                    </small>
                </button>
            </div>
        </div>
        {!! Form::hidden('account_code[]', null, ['class' => 'account_code']) !!}
    </td>
    <td>
        {!! Form::text('barcode[]', null, ['class' => 'form-control barcode calculate-field']) !!}
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('subject[]', null, ['class' => 'form-control subject']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#row-detail-id">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand"></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::text('product[]', null, ['class' => 'form-control product calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('type[]', null, ['class' => 'form-control type calculate-field']) !!}
    </td>
    <td>
        {!! Form::number('quantity[]', null, ['class' => 'form-control quantity calculate-field', 'min' => '1', 'step'
        => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('unit_price[]', null, ['class' => 'form-control unit_price calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('uom[]', null, ['class' => 'form-control uom calculate-field']) !!}
        {!! Form::hidden('discount[]', null, ['class' => 'form-control discount calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('amount[]', null, ['class' => 'form-control amount calculate-field', 'disabled']) !!}
        {!! Form::hidden('amount_dt[]', null, ['class' => 'form-control amount_dt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="narrow-padding template-new d-none row-detail collapse">
    <td colspan="13">
        {!! Form::textarea('details[]', null, ['class' => 'form-control t_detail', 'rows' => 1]) !!}
    </td>
</tr>