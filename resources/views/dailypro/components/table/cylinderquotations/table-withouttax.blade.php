<div class="tax-container"></div>
<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="7%" class="text-center">Sequence</th>
                <th width="12%" class="text-center stockcode-th" data-toggle="modal" data-target=".stockcode-modal"
                    style="text-decoration: underline;">Serial No</th>
                <th width="12%" class="text-left">Barcode</th>
                <th width="25%" class="text-left">Description</th>
                <th width="7%" class="text-left">Product</th>
                <th width="7%" class="text-left">Type</th>
                <th width="7%" class="text-center">Qty</th>
                <th width="7%" class="text-center">U.Price</th>
                <th width="7%" class="text-center">U.O.M</th>
                <th width="9%" class="text-center">Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="table-form-tbody">
            @include('dailypro.components.table.cylinderquotations.form-withouttax')
            @includeWhen(isset($items), 'dailypro.components.table.cylinderquotations.row-withouttax')
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1">
                    <button type="button" class="form-control btn bg-dark-green text-white" id="add-new">
                        Add
                    </button>
                </td>
                <td colspan="8"></td>
            </tr>
        </tfoot>
    </table>
</div>