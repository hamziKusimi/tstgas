{{-- Paid Bills is from view_bill_payments --}}

@if (isset($paidBills))
    @foreach ($paidBills as $bill)
        @php $appliedAmount += $bill->payment_amount; @endphp
        <tr class="bg-very-light-blue tr-paid-bills">
            <td width="5%" class="text-center">
                {!! Form::checkbox('RV_CHECKED[]', $bill->id, true, ['class' => 'item_check', 'checked']) !!}
                {!! Form::hidden('existing_payment[]', 'true', ['class' => 'existing_payment']) !!}
                {!! Form::hidden('is_checked[]', 'true', ['class' => 'is_checked']) !!}
            </td>
            <td width="10%">
                {{ $bill->pay_bill_no }}
                {!! Form::hidden('bill_no[]', $bill->pay_bill_no, ['class' => 'bill_no']) !!}
            </td>
            <td width="20%">
                {!! Form::text('description[]', !empty($bill->item) ? $bill->item : $bill->pay_bill_no, [
                    'class' => 'form-control description'
                ]) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->bill_amount, 2) }}</span>
                {!! Form::hidden('bill_amount[]', number_format($bill->bill_amount, 2), ['class' => 'bill_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->payment_amount, 2) }}</span>
                {!! Form::hidden('paid_amount[]', number_format($bill->payment_amount, 2), ['class' => 'paid_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format(($bill->bill_amount - $bill->payment_amount), 2) }}</span>
                {!! Form::hidden('outstanding_amount[]', number_format($bill->bill_amount - $bill->payment_amount, 2), ['class' => 'outstanding_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->payment_amount, 2) }}</span>
                {!! Form::hidden('applied_amount[]', number_format($bill->payment_amount, 2), ['class' => 'applied_amount']) !!}
            </td>
        </tr>
    @endforeach
@endif