<tr class="tr-template d-none">
    <td width="5%" class="text-center">
        {!! Form::checkbox('RV_CHECKED[]', null, false, ['class' => 'item_check']) !!}
        {!! Form::hidden('existing_payment[]', false, ['class' => 'existing_payment']) !!}
        {!! Form::hidden('is_checked[]', false, ['class' => 'is_checked']) !!}
    </td>
    <td width="10%" class="">
        <span class="bill_no"></span>
        {!! Form::hidden('bill_no[]', null, ['class' => 'bill_no']) !!}
    </td>
    <td width="20%">
        {!! Form::text('description[]', null, ['class' => 'form-control description']) !!}
    </td>
    <td width="8%" class="text-right">
        <span class="displayed-value">0.00</span>
        {!! Form::hidden('bill_amount[]', null, ['class' => 'bill_amount']) !!}
    </td>
    <td width="8%" class="text-right">
        <span class="displayed-value">0.00</span>
        {!! Form::hidden('paid_amount[]', null, ['class' => 'paid_amount']) !!}
    </td>
    <td width="8%" class="text-right">
        <span class="displayed-value">0.00</span>
        {!! Form::hidden('outstanding_amount[]', null, ['class' => 'outstanding_amount']) !!}
    </td>
    <td width="8%" class="text-right">
        <span class="displayed-value">0.00</span>
        {!! Form::hidden('applied_amount[]', null, ['class' => 'applied_amount']) !!}
    </td>
</tr>