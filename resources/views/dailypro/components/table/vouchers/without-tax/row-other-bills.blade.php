{{-- Other Bills is from view_outstanding_bills --}}

@if (isset($otherBills))
    @foreach ($otherBills as $bill)
        <tr>
            <td width="5%" class="text-center">
                {!! Form::checkbox('RV_CHECKED[]', null, false, ['class' => 'item_check']) !!}
                {!! Form::hidden('existing_payment[]', false, ['class' => 'existing_payment']) !!}
                {!! Form::hidden('is_checked[]', false, ['class' => 'is_checked']) !!}
            </td>
            <td width="10%" class="">
                {{ $bill->doc_no }}
                {!! Form::hidden('bill_no[]', $bill->doc_no, ['class' => 'bill_no']) !!}
            </td>
            <td width="20%">
                {!! Form::text('description[]', !empty($bill->description) ? $bill->description : $bill->doc_no, [
                    'class' => 'form-control description'
                ]) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->amount, 2) }}</span>
                {!! Form::hidden('bill_amount[]', number_format($bill->amount, 2), ['class' => 'bill_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->amount - $bill->remaining_amount, 2) }}</span>
                {!! Form::hidden('paid_amount[]', number_format($bill->amount - $bill->remaining_amount, 2), ['class' => 'paid_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->remaining_amount, 2) }}</span>
                {!! Form::hidden('outstanding_amount[]', number_format($bill->remaining_amount, 2), ['class' => 'outstanding_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">0.00</span>
                {!! Form::hidden('applied_amount[]', null, ['class' => 'applied_amount']) !!}
            </td>
        </tr>
    @endforeach
@endif