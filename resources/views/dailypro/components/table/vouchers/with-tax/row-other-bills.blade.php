{{-- Other Bills is from view_outstanding_bills --}}

@if (isset($otherBills))
    @foreach ($otherBills as $bill)
        <tr>
            <td width="5%" class="text-center">
                {!! Form::checkbox('RV_CHECKED[]', null, false, ['class' => 'item_check']) !!}
                {!! Form::hidden('existing_payment[]', false, ['class' => 'existing_payment']) !!}
                {!! Form::hidden('is_checked[]', false, ['class' => 'is_checked']) !!}
            </td>
            <td width="10%" class="">
                {{ $bill->doc_no }}
                {!! Form::hidden('bill_no[]', $bill->doc_no, ['class' => 'bill_no']) !!}
            </td>
            <td width="20%">
                {!! Form::text('description[]', !empty($bill->description) ? $bill->description : $bill->doc_no, [
                    'class' => 'form-control description'
                ]) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->amount, 2) }}</span>
                {!! Form::hidden('bill_amount[]', number_format($bill->amount, 2), ['class' => 'bill_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->amount - $bill->remaining_amount, 2) }}</span>
                {!! Form::hidden('paid_amount[]', number_format($bill->amount - $bill->remaining_amount, 2), ['class' => 'paid_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->remaining_amount, 2) }}</span>
                {!! Form::hidden('outstanding_amount[]', number_format($bill->remaining_amount, 2), ['class' => 'outstanding_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">0.00</span>
                {!! Form::hidden('applied_amount[]', null, ['class' => 'applied_amount']) !!}
            </td>
            <td width="8%">
                {!! Form::select('tax_code[]', $taxCodes->pluck('taxInfo', 't_code'), $bill->tax_code, [
                    'class' => 'form-control tax_code calculate-field form-select2',
                    'style' => 'width:100%',
                    'placeholder' => '',
                    'data-taxes' => $taxCodes,
                ]) !!}
            </td>
            <td width="10%">
                {!! Form::text('tax_rate[]', $bill->tax_rate, ['class' => 'form-control tax_rate', 'disabled']) !!}
                {!! Form::hidden('tax_rate_dt[]', $bill->tax_rate, ['class' => 'form-control tax_rate_dt']) !!}
            </td>
            <td width="10%">
                {!! Form::text('tax_amount[]', $bill->tax_amount, ['class' => 'form-control tax_amount money', 'disabled']) !!}
                {!! Form::hidden('tax_amount_dt[]', $bill->tax_amount, ['class' => 'form-control tax_amount_dt']) !!}
            </td>
        </tr>
    @endforeach
@endif