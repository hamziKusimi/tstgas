{{-- Paid Bills is from view_bill_payments --}}

@if (isset($paidBills))
    @foreach ($paidBills as $bill)
        @php $appliedAmount += $bill->payment_amount; @endphp
        <tr class="bg-very-light-blue">
            <td width="5%" class="text-center">
                {!! Form::checkbox('RV_CHECKED[]', $bill->id, true, ['class' => 'item_check', 'checked']) !!}
                {!! Form::hidden('existing_payment[]', 'true', ['class' => 'existing_payment']) !!}
                {!! Form::hidden('is_checked[]', 'true', ['class' => 'is_checked']) !!}
            </td>
            <td width="10%">
                {{ $bill->pay_bill_no }}
                {!! Form::hidden('bill_no[]', $bill->pay_bill_no, ['class' => 'bill_no']) !!}
            </td>
            <td width="20%">
                {!! Form::text('description[]', !empty($bill->item) ? $bill->item : $bill->pay_bill_no, [
                    'class' => 'form-control description'
                ]) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->bill_amount, 2) }}</span>
                {!! Form::hidden('bill_amount[]', number_format($bill->bill_amount, 2), ['class' => 'bill_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->currentPaidAmount, 2) }}</span>
                {!! Form::hidden('paid_amount[]', number_format($bill->currentPaidAmount, 2), ['class' => 'paid_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format(($bill->bill_amount - $bill->currentPaidAmount), 2) }}</span>
                {!! Form::hidden('outstanding_amount[]', number_format($bill->bill_amount - $bill->currentPaidAmount, 2), ['class' => 'outstanding_amount']) !!}
            </td>
            <td width="8%" class="text-right">
                <span class="displayed-value">{{ number_format($bill->payment_amount, 2) }}</span>
                {!! Form::hidden('applied_amount[]', number_format($bill->payment_amount, 2), ['class' => 'applied_amount']) !!}
            </td>
            <td width="8%">
                {!! Form::select('tax_code[]', $taxCodes->pluck('taxInfo', 't_code'), $bill->tax_code, [
                    'class' => 'form-control tax_code calculate-field form-select2',
                    'style' => 'width:100%',
                    'placeholder' => '',
                    'data-taxes' => $taxCodes,
                ]) !!}
            </td>
            <td width="10%">
                {!! Form::text('tax_rate[]', $bill->tax_rate, ['class' => 'form-control tax_rate', 'disabled']) !!}
                {!! Form::hidden('tax_rate_dt[]', $bill->tax_rate, ['class' => 'form-control tax_rate_dt']) !!}
            </td>
            <td width="10%">
                {!! Form::text('tax_amount[]', $bill->tax_amount, ['class' => 'form-control tax_amount money', 'disabled']) !!}
                {!! Form::hidden('tax_amount_dt[]', $bill->tax_amount, ['class' => 'form-control tax_amount_dt']) !!}
            </td>
        </tr>
    @endforeach
@endif