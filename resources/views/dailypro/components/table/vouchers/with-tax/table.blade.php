{{--
    data passed here from the controllers are:
    - $paidBills : array of bills (from SQL View: view_bills)
    - $otherBills : array of bills (from SQL View: view_bills)
    - item : Receipt/Payment Voucher
--}}
@php $appliedAmount = 0; @endphp
<div class="table-responsive">
    <table class="table table-bordered table-sm" id="vouchers-table">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="5%" class="text-center">#</th>
                <th width="10%">Outstanding Bill</th>
                <th width="20%">Description</th>
                <th width="8%" class="text-right">Bill Amt</th>
                <th width="8%" class="text-right">Paid Amt</th>
                <th width="8%" class="text-right">Outstanding Amt</th>
                <th width="8%" class="text-right">Applied Amt</th>
                <th width="8%" class="text-right">Tax Code</th>
                <th width="10%" class="text-right">Tax Rate</th>
                <th width="10%" class="text-right">Tax Amount</th>
            </tr>
        </thead>
        <tbody id="table-form-tbody">
            @include('billing-module.components.table.vouchers.with-tax.row-template')
            @include('billing-module.components.table.vouchers.with-tax.row-paid-bills')
            @include('billing-module.components.table.vouchers.with-tax.row-other-bills')
        </tbody>
        <tfoot>
            <tr class="bg-info text-white">
                <td colspan="4" class="text-right"><strong>REMAINING</strong></td>
                <td class="text-right">
                    <strong>
                        @if (isset($bill))
                            {{ number_format($bill->amount - $appliedAmount, 2) }}
                            {!! Form::hidden('remaining_amount', number_format($bill->amount - $appliedAmount, 2)) !!}
                        @else
                            0.00
                            {!! Form::hidden('remaining_amount', 0) !!}
                        @endif
                    </strong>
                </td>
                <td class="text-right"><strong>TOTAL</strong></td>
                <td class="text-right">
                    <strong>
                        {{ number_format($appliedAmount, 2) }}
                        {!! Form::hidden('total_paid', number_format($appliedAmount, 2)) !!}
                    </strong>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>
