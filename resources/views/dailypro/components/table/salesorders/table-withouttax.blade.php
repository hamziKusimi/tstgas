<div class="tax-container" data-taxes="{{ $taxCodes }}"></div>
<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="7.5%" class="text-center">SN</th>
                <th width="12%" class="text-center stockcode-th" data-toggle="modal" data-target=".stockcode-modal"  style="text-decoration: underline;">Stock Code</th>
                <th width="25%" class="text-left">Description</th>
                <th width="9%" class="text-center">Qty</th>
                <th width="9%" class="text-center">UOM</th>
                <th width="9%" class="text-center">Rate</th>
                <th width="9%" class="text-center">U.Price</th>
                <th width="9%" class="text-center">Discount</th>
                <th width="9%" class="text-center">Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="table-form-tbody">
            @include('dailypro.components.table.salesorders.form-withouttax')
            @includeWhen(isset($items), 'dailypro.components.table.salesorders.row-withouttax')
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <button type="button" class="form-control btn bg-dark-green text-white" id="add-new">
                        Add Line
                    </button>
                </td>
                <td colspan="8"></td>
            </tr>
        </tfoot>
    </table>
</div>

