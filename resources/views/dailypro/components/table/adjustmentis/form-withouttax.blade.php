<tr class="narrow-padding template-new d-none">
    <td>
        {!! Form::hidden('item_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('sequence_no[]', null, ['class' => 'form-control sequence_no']) !!}
    </td>
    {{-- {{ dd($stockcode) }} --}}
    <td>
        <div class="input-group calculate-field">
            <div class="input-group-append" style="width:100%">
                {!! Form::select('item_code[]', [], null, [
                    'class' => 'form-control item_code',
                    'placeholder' => '',
                    'style' => 'width:100%; height:100%',
                    'data-contents' => $stockcode,
                    // 'data-route' => route('item-master.api.store')
                ]) !!}
                {!! Form::hidden('account_code[]', null, ['class' => 'account_code']) !!}
                <div class="input-group-text bg-dark-blue text-white pointer stockcode-th" data-toggle="modal" data-target=".stockcode-modal" style="text-decoration: underline;"">
                    <small>
                            <i class="fa fa-arrow-down"></i>
                    </small>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('subject[]', null, ['class' => 'form-control subject']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#row-detail-id">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand" ></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::number('quantity[]', null, ['class' => 'form-control quantity calculate-field', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::select('unit_measure[]', [], null, [
            'class' => 'form-control unit_measure',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-uoms' => $uom,
            // 'data-route' => route('item-master.api.store')
        ]) !!}
        {!! Form::hidden('unit_measuredt[]', null, ['class' => 'unit_measuredt']) !!}
    </td>
    <td>
        {!! Form::text('rate[]', null, [ 'class' => 'form-control rate' ]) !!}
    </td>
    <td>
        {!! Form::text('unit_cost[]', null, ['class' => 'form-control unit_cost calculate-field']) !!}
    </td>   
    <td>
        {!! Form::text('type[]', null, [ 'class' => 'form-control type' ]) !!}
    </td>
    <td>
        {!! Form::text('discount[]', null, ['class' => 'form-control discount calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('amount[]', null, ['class' => 'form-control amount calculate-field', 'disabled']) !!}
        {!! Form::hidden('amount_dt[]', null, ['class' => 'form-control amount_dt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="narrow-padding template-new d-none row-detail collapse">
    <td colspan="13">
        {!! Form::textarea('details[]', null, ['class' => 'form-control t_detail', 'rows' => 1]) !!}
    </td>
</tr>
