@if (isset($items))
    @foreach ($items as $item)
        <tr class="narrow-padding sortable-row existing-data" data-item-id="{{ $item->id }}" data-delete-route="{{ $deleteRoute }}">
            <td>
                {!! Form::hidden('item_id[]', $item->id, ['class' => 'item-id']) !!}
                {!! Form::text('sequence_no[]', $item->sequence_no, ['class' => 'form-control sequence_no']) !!}
            </td>
            <td>
                {!! Form::select('item_code[]', $stockcode->pluck('itemMastercode', 'code'), $item->item_code, ['class' => 'form-control item_code', 'style' => 'width:100%; height:100%']) !!}
                {!! Form::hidden('account_code[]', $item->account_code, ['class' => 'account_code']) !!}
            </td>
            <td>
                <div class="input-group">
                    {!! Form::text('subject[]', $item->subject, ['class' => 'form-control subject', ($item->editDescr->first() == '1')?'':'readonly']) !!}
                    <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#details_{{ $item->id }}">
                        <span class="input-group-text  bg-dark-blue text-white">
                            <i class="fa fa-expand" ></i>
                        </span>
                    </div>
                </div>
            </td>
            <td>
                {!! Form::number('quantity[]', $item->qty, ['class' => 'form-control quantity calculate-field', 'min' => '1', 'step' => '0.01']) !!}
            </td>
            <td>
                {!! Form::select('unit_measure[]', [], $item->uom, ['class' => 'form-control unit_measure new-item', 'style' => 'width:100%; height:100%']) !!}

                {!! Form::hidden('unit_measuredt[]',  $item->uom, ['class' => 'unit_measuredt']) !!}
            </td>
            <td>
                {!! Form::text('rate[]', $item->rate, [ 'class' => 'form-control rate' ]) !!}
            </td>
            <td>
                {!! Form::text('unit_cost[]', $item->ucost, ['class' => 'form-control unit_cost calculate-field text-right']) !!}
            </td>
            <td>
                {!! Form::text('type[]', $item->type, [ 'class' => 'form-control type' ]) !!}
            </td>
            <td>
                {!! Form::text('discount[]', $item->discount, ['class' => 'form-control discount calculate-field text-right']) !!}
            </td>
            <td>
                {!! Form::text('amount[]', $item->amount, ['class' => 'form-control amount calculate-field text-right', 'disabled']) !!}
                {!! Form::hidden('amount_dt[]', $item->amount, ['class' => 'form-control amount_dt']) !!}
            </td>
            <td>
                {!! Form::select('tax_code[]', $taxCodes->pluck('taxInfo', 't_code'), $item->tax_code, [
                    'class' => 'form-control tax_code calculate-field form-select2',
                    'style' => 'width:100%',
                    'placeholder' => '',
                    'data-taxes' => $taxCodes,
                ]) !!}
            </td>
            <td>
                {!! Form::text('tax_rate[]', $item->tax_rate, ['class' => 'form-control tax_rate', 'disabled']) !!}
                {!! Form::hidden('tax_rate_dt[]', $item->tax_rate, ['class' => 'form-control tax_rate_dt']) !!}
            </td>
            <td>
                {!! Form::text('tax_amount[]', $item->tax_amount, ['class' => 'form-control tax_amount money', 'disabled']) !!}
                {!! Form::hidden('tax_amount_dt[]', $item->tax_amount, ['class' => 'form-control tax_amount_dt']) !!}
            </td>
            <td>
                {!! Form::text('taxed_amount[]', $item->taxed_amount, ['class' => 'form-control taxed_amount money', 'disabled']) !!}
                {!! Form::hidden('taxed_amount_dt[]', $item->taxed_amount, ['class' => 'form-control taxed_amount_dt']) !!}
            </td>
            <td>
                <button type="button" class="btn btn-danger remove-row-exist">
                    <i class="fa fa-times"></i>
                </button>
            </td>
        </tr>
        <tr class="narrow-padding row-detail-filled collapse" id="details_{{ $item->id }}" data-item-detail-for-id="{{ $item->id }}">
            <td colspan="13">
                {!! Form::textarea('details[]', $item->details, ['class' => 'form-control t_detail summernote', 'rows' => 1]) !!}
            </td>
        </tr>
    @endforeach
@endif
