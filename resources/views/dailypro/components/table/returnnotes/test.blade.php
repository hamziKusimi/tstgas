
<tr>
    <td class="details-control">
    </td>
    <td>
        {!! Form::hidden('item_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('sequence_no[]', null, ['class' => 'form-control sequence_no']) !!}
    </td>
    {{-- {{ dd($stockcode) }} --}}
    <td>
        {!! Form::text('category[]', null, ['class' => 'form-control validate-barcode category', 'id' => 'category']) !!}
    </td>
    <td>
        {!! Form::text('product[]', null, [ 'class' => 'form-control validate-barcode product' ]) !!}
    </td>
    <td>
        {!! Form::text('capacity[]', null, [ 'class' => 'form-control validate-barcode capacity' ]) !!}
    </td>
    <td>
        {!! Form::text('pressure[]', null, ['class' => 'form-control validate-barcode pressure']) !!}
    </td>
    <td>
        {!! Form::number('quantity[]', null, ['class' => 'form-control validate-barcode quantity', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('price[]', null, ['class' => 'form-control price']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row-child" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
