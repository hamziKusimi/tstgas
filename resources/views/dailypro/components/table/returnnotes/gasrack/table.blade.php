<div class="tax-container"></div>
<div class="table-responsive">
    <table class="table table-sm" id="gasrackdt">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th></th>
                <th class="text-left">Gas Rack List</th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody id="table-form-tbody-gasrack" class="gasrack">
            @include('dailypro.components.table.returnnotes.gasrack.form')
            @includeWhen(isset($grcontents), 'dailypro.components.table.returnnotes.gasrack.row')
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1">    
                    <button type="button" class="form-control btn bg-dark-green text-white d-none" style="width:70px" id="add-new2">
                        Add 
                    </button>
                </td>
                <td colspan="8"></td>
            </tr> 
        </tfoot>
    </table>
</div>

