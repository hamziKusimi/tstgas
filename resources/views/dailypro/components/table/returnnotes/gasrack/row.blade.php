@if (isset($grcontents))
@foreach ($grcontents as $i => $grcontent)
<tr class="narrow-padding sortable-row existing-data2" data-grcontent-id="{{ $grcontent->id }}"
    data-row-id2="{{ $grcontent->id}}" data-existed="1">
    <td class="details-control toggle-inner-table-button-exist" data-second-row-id="row-id2-{{ $grcontent->id }}">
    </td>
    <td>
        {{-- {!! Form::hidden('gr_sequence_no[]', $grcontent->sequence_no, ['class' => 'form-control grsequence_no']) !!}  --}}
        {{-- @if($grcontent->sequence_no == "1") --}}
        <div class="existgasrack">Gas Rack</div>
    {{-- @else --}}
       {{-- <div class="Notexistgasrack">Not Exist Gas Rack</div> --}}
    {{-- @endif --}}
    </td>
    <td>
    </td>
    <td>
        {{-- {!! Form::hidden('grquantity[]', $grcontent->qty, ['class' => 'form-control grquantity', 'min' => '1', 'step' => '0.01']) !!} --}}
    </td>
    <td>
        {{-- {!! Form::hidden('grprice[]', $grcontent->price, ['class' => 'form-control grprice text-right']) !!} --}}
    </td>
    <td>
    </td>
</tr>
<tr class="inner-table d-none  row-id2-{{ $grcontent->id }}" data-row-id2="{{ $grcontent->id}}">
    <td colspan="9">
        <table class="table table-sm narrow-padding gasrack-table">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Description</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">DN No.</th>
                    <th class="text-center">DN Date</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            @if (isset($grcontent->gasracks))
            <tbody class="gasrack">
                @foreach ($grcontent->gasracks as $gasrack)
                @php
                $edit_ul = '<input name="edit_ul_barcode[]" type="hidden" value="' . $gasrack->barcode . '">';
                $edit_ul .= '<input name="edit_ul_serial[]" type="hidden" value="' . $gasrack->serial . '">';
                $edit_ul .= '<input name="edit_ul_descr[]" type="hidden" value="' . $gasrack->descr . '">';
                $edit_ul .= '<input name="edit_ul_sequence_no[]" type="hidden" value="' . $gasrack->dt_sqn . '">';
                $edit_ul .= '<input name="edit_ul_sqn_no[]" type="hidden" value="' . $gasrack->list_sqn . '">';
                $edit_ul .= '<input name="edit_ul_datetime[]" class="datetime" type="hidden" value="">';
                $edit_ul .= '<input name="edit_ul_driver[]" type="hidden" value="' . $gasrack->driver . '">';
                $edit_ul .= '<input name="edit_ul_dn_no[]" type="hidden" value="' . $gasrack->rt_dnno . '">';
                $edit_ul .= '<input name="edit_ul_dn_date[]" type="hidden" value="' . $gasrack->rt_dndate . '">';
                $edit_ul .= '<input name="edit_ul_type[]" type="hidden" value="gr">';
                @endphp
                <tr class="new-row2" data-isexisted="1">
                    <td class="text-center barcode">{{ $gasrack->barcode }}</td>
                    <td class="text-center serialno">{{ $gasrack->serial }}</td>
                    <td class="text-center">{{ $gasrack->descr }}</td>
                    <td class="text-center">{{ $gasrack->driver }}</td>
                    <td class="text-center">{{ $gasrack->rt_dnno }}</td>
                    <td class="text-center">{{ $gasrack->rt_dndate }}</td>
                    <td class="text-center loadingdt">{{ date('d/m/Y', strtotime($gasrack->loading_date)) }}</td>
                    <td class="text-center"  width="20%">{!! $gasrack->unloading_date == null ?'<input
                            class="form-control uloadingdt calculate-field" readonly="" name="edit_ul_date[]"
                            type="text">' . $edit_ul : date('d/m/Y', strtotime($gasrack->unloading_date)) !!}</td>
                    <td class="text-center">
                        <button type="button" class="btn btn-danger remove-row-child" data-dnno="{{ $gasrack->dn_no }}" 
                            data-serial="{{ $gasrack->serial }}"
                            data-sqndt="{{ $gasrack->dt_sqn }}" data-sqnlist="{{ $gasrack->list_sqn }}"
                            data-delete-routelist="{{ route('returnnotes.data.destroylist') }}">
                            <i class="fa fa-times"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
            @endif
        </table>
    </td>
</tr>
@endforeach
@endif