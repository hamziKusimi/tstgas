<div class="tax-container" data-taxes="{{ $taxCodes }}"></div>
<div class="table-responsive">
    <table class="table table-sm" id="opiniondt">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th class="text-center"></th>
                <th class="text-left">Cylinder List</th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th width="10%" class="text-center"></th>
                {{-- {{-- <th width="10%" class="text-center">Loading</th> --}}
                {{-- <th width="10%" class="text-center">Unloading</th> --}} 
            </tr>
        </thead>
        <tbody id="table-form-tbody">
            @include('dailypro.components.table.returnnotes.form')
            @includeWhen(isset($contents), 'dailypro.components.table.returnnotes.row')
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1">    
                    <button type="button" class="form-control btn bg-dark-green text-white d-none" id="add-new">
                        Add Line
                    </button>
                </td>
                <td colspan="8"></td>
            </tr> 
        </tfoot>
    </table>
</div>

