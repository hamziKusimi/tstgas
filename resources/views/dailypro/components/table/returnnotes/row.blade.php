@if (isset($contents))
    @foreach ($contents as $i => $content)
        <tr class="narrow-padding sortable-row existing-data" data-content-id="{{ $content->id }}" data-content-id="{{ $content->id }}" data-row-id="{{ $content->id }}" data-existed="1">
            <td class="details-control toggle-inner-table-button-exist" data-second-row-id="row-id-{{ $content->id }}">
            </td>
            <td class="exist-row-cylinder">
                {!! Form::hidden('sequence_no_edit[]', $content->sequence_no, ['class' => 'form-control sequence_no']) !!}
                {{-- @if($content->sequence_no == "1") --}}
                    <div class="existcylinder">Cylinder</div>
                {{-- @else --}}
                    {{-- <div class="Notexistcylinder">Not Exist Cylinder</div> --}}
                {{-- @endif --}}
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr class="inner-table d-none row-id-{{ $content->id }}" data-row-id="0">
            <td colspan="9">
                <table class="table table-sm narrow-padding cylinder-table">
                    <thead>
                        <tr class="text-sm">
                            <th class="text-center">Barcode</th>
                            <th class="text-center">Serial</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Driver</th>
                            <th class="text-center">DN No.</th>
                            <th class="text-center">DN Date</th>
                            <th class="text-center">Loading</th>
                            <th class="text-center">Unloading</th>
                            <th></th>
                        </tr>
                    </thead>
                    @if (isset($content->cylinders))
                        <tbody>
                        @foreach ($content->cylinders as $master)
                        @php
                        $edit_ul =  '<input name="edit_ul_barcode[]" type="hidden" value="' . $master->barcode . '">';
                        $edit_ul .= '<input name="edit_ul_serial[]" type="hidden" value="' . $master->serial . '">';
                        $edit_ul .= '<input name="edit_ul_descr[]" type="hidden" value="' . $master->descr . '">';
                        $edit_ul .= '<input name="edit_ul_sequence_no[]" type="hidden" value="' . $master->dt_sqn . '">';
                        $edit_ul .= '<input name="edit_ul_sqn_no[]" class="sqn_no" type="hidden" value="' . $master->list_sqn . '">';
                        $edit_ul .= '<input name="edit_ul_datetime[]" class="datetime" type="hidden" value="">';
                        $edit_ul .= '<input name="edit_ul_driver[]" type="hidden" value="' . $master->driver . '">';
                        $edit_ul .= '<input name="edit_ul_dn_no[]" type="hidden" value="' . $master->rt_dnno . '">';
                        $edit_ul .= '<input name="edit_ul_dn_date[]" type="hidden" value="' . $master->rt_dndate . '">';
                        $edit_ul .= '<input name="edit_ul_type[]" type="hidden" value="cy">';
                        @endphp
                            <tr class="new-row" data-isexisted="1">
                                <td class="text-center barcode">{{ $master->barcode }}</td>
                                <td class="text-center serialno">{{ $master->serial }}</td>
                                <td class="text-center descr">{{ $master->descr }}</td>
                                <td class="text-center driver">{{ $master->driver }}</td>
                                <td class="text-center dn_no">{{ $master->rt_dnno }}</td>
                                <td class="text-center dn_date">{{ date('d/m/Y', strtotime($master->rt_dndate)) }}</td>
                                <td class="text-center">{{ date('d/m/Y', strtotime($master->loading_date)) }}</td>
                                <td class="text-center"  width="20%">{!!$master->unloading_date == null ?
                                '<input class="form-control uloadingdt calculate-field" readonly=""
                                name="edit_ul_date[]" type="text">' . $edit_ul: date('d/m/Y', strtotime($master->unloading_date)) !!}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-danger remove-row-child"
                                    data-dnno="{{ $master->dn_no }}"
                                    data-serial="{{ $master->serial }}"
                                    data-sqnex="{{ $master->dt_sqn }}"
                                    data-sqnlist="{{ $master->list_sqn }}"
                                    data-delete-routelist="{{ route('returnnotes.data.destroylist') }}">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                </table>
            </td>
        </tr>
    @endforeach
@endif