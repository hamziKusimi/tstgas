
<tr class="narrow-padding toggle-details main-table-row-template d-none" data-row-id="0">
    <td class="details-control toggle-inner-table-button text-left">
        {!! Form::hidden('sequence_no[]', null, ['class' => 'form-control sequence_no exist-val-cy']) !!}
    </td>
    <td class="text-left exist-cylinder">
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td></td>
</tr>
<tr class="inner-table second-row-template d-none" data-row-id="0">
    <td colspan="9">
        <table class="table table-sm narrow-padding cylinder-table">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Description</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">DN No.</th>
                    <th class="text-center">DN Date</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </td>
</tr>
