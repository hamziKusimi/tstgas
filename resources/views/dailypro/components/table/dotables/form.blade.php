<tr class="narrow-padding modal-template-new d-none">
    <td>
        {!! Form::text('dodocno[]', null, ['class' => 'form-control dodocno', 'disabled']) !!}
    </td>
    <td>
        {!! Form::text('dsequence_no[]', null, ['class' => 'form-control dsequence_no', 'disabled']) !!}
    </td>
    {{-- {{ dd($stockcode) }} --}}
    <td>
        {!! Form::select('ditem_code[]', $stockcode->pluck('code', 'code'), null, [
            'class' => 'form-control ditem_code calculate-field2',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-contents' => $stockcode, 'disabled'
            // 'data-route' => route('item-master.api.store')
        ]) !!}
        {!! Form::hidden('daccount_code[]', null, ['class' => 'daccount_code']) !!}
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('dsubject[]', null, ['class' => 'form-control dsubject', 'disabled']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#row-detail-id">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand" ></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::number('dquantity[]', null, ['class' => 'form-control dquantity calculate-field2', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
            {!! Form::select('dunit_measure[]', [], null, [
                'class' => 'form-control dunit_measure',
                'placeholder' => '',
                'style' => 'width:100%; height:100%',
                'data-uoms' => $uom, 'disabled'
                // 'data-route' => route('item-master.api.store')
            ]) !!}
            {{-- {!! Form::hidden('punit_measure[]', null, ['class' => 'punit_measure']) !!} --}}
        {{-- {!! Form::text('punit_measure[]', null, [ 'class' => 'form-control punit_measure' ]) !!} --}}
    </td>
    <td>
        {!! Form::text('drate[]', null, [ 'class' => 'form-control drate', 'disabled']) !!}
    </td>
    <td>
        {!! Form::text('dsupp-price[]', null, ['class' => 'form-control dsupp-price calculate-field2']) !!}
    </td>
    <td>
        {!! Form::text('damount[]', null, ['class' => 'form-control damount calculate-field2', 'disabled']) !!}
        {!! Form::hidden('damount_dt[]', null, ['class' => 'form-control damount_dt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>