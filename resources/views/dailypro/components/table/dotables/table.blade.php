<div class="tax-container2"></div>
<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="12%" class="text-center">P/O No.</th>
                <th width="8%" class="text-center">SN</th>
                <th width="12%" class="text-center">Stock Code</th>
                <th width="20%" class="text-left">Description</th>
                <th width="8%" class="text-center">Qty</th>
                <th width="8%" class="text-center">UOM</th>
                <th width="8%" class="text-center">Rate</th>
                <th width="8%" class="text-center">Unit Price</th>
                <th width="8%" class="text-center">Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="modal-table-form-tbody">
            @include('dailypro.components.table.dotables.form')
            @includeWhen(isset($items), 'dailypro.components.table.dotables.row')
        </tbody>
        {{-- <tfoot>
            <tr>
                <td colspan="2">
                    <button type="button" class="form-control btn bg-dark-green text-white" id="modal-add-new">
                        Add Line
                    </button>
                </td>
                <td colspan="8"></td>
            </tr>
        </tfoot> --}}
    </table>
</div>

