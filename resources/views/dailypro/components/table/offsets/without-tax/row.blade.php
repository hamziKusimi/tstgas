@if (isset($items))
    @foreach ($items as $item)
        <tr class="narrow-padding sortable-row" data-item-id="{{ $item->id }}">
            <td class="text-center">{{ $item->item_code }}</td>
            <td>
                <div class="input-group">
                    {!! Form::text('subject', $item->subject, ['class' => 'form-control subject']) !!}
                    <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#details_{{ $item->id }}">
                        <span class="input-group-text  bg-dark-blue text-white">
                            <i class="fa fa-expand" ></i>
                        </span>
                    </div>
                </div>
            </td>
            <td>
                {!! Form::number('quantity', $item->quantity, ['class' => 'form-control quantity calculate-field', 'min' => '1', 'step' => '0.01']) !!}
            </td>
            <td>
                {!! Form::text('unit_measure', $item->unit_measure, [ 'class' => 'form-control unit_measure' ]) !!}
            </td>
            <td>
                {!! Form::text('unit_price', $item->unit_price, ['class' => 'form-control unit_price calculate-field']) !!}
            </td>
            <td>
                {!! Form::text('discount', $item->discount, ['class' => 'form-control discount calculate-field']) !!}
            </td>
            <td>
                {!! Form::text('amount', $item->amount, ['class' => 'form-control amount calculate-field', 'disabled']) !!}
            </td>
            <td>
                <button type="button" class="btn btn-danger remove-row">
                    <i class="fa fa-times"></i>
                </button>
            </td>
        </tr>
        <tr class="narrow-padding row-detail-filled collapse" id="details_{{ $item->id }}" data-item-detail-for-id="{{ $item->id }}">
            <td colspan="13">
                {!! Form::textarea('details', $item->details, ['class' => 'form-control t_detail', 'rows' => 1]) !!}
            </td>
        </tr>
    @endforeach
@endif
