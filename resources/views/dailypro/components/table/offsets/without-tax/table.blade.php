<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="10%" class="text-center">Code</th>
                <th width="40%">Description</th>
                <th width="10%" class="text-center">Qty</th>
                <th width="10%" class="text-center">UOM</th>
                <th width="10%" class="text-center">U.Price</th>
                <th width="10%" class="text-center">Discount</th>
                <th width="10%" class="text-center">Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="table-form-tbody" id="sortable">
            @include('billing-module.components.table.without-tax.form')
            @if (isset($contents))
                @include('billing-module.components.table.without-tax.row', ['items' => $contents])
            @endif
        </tbody>
        <tfoot>
            <tr>
                <td colspan="12">
                    <button type="button" class="btn bg-dark-green text-white" id="add-new">
                        Add Line
                    </button>
                </td>
            </tr>
        </tfoot>
    </table>
</div>


@push('scripts')
<script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script>
<script type="text/javascript">
(function() {
    let systemSettings = $('.system-settings-param')
    let boolRounding = systemSettings.data('rounding')

    // custom select2
    function formatState (state) { return state.id }
    $('.form-select2').select2({ templateSelection: formatState, dropdownAutoWidth : true })
    $('.row-detail-filled').find('.t_detail').summernote({ height: 100, followingToolbar: false, toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
    })

    // sortable (drag and drop item list in tbody)
    var sortableHelper = function (e, ui) {
         ui.children().each(function () {
             $(this).width($(this).width());
         });
         return ui;
     };

    $('tbody').sortable({
        items: 'tr.sortable-row',
        opacity: '.5',
        helper: sortableHelper,
        start: function (e, ui) {
            ui.placeholder.height(ui.helper.outerHeight());
        },
        update: function (e, ui) {
            let tableHasUnsortableRows = $(this).find('tr:not(.sortable-row)').length;
            $(this).find('tr.sortable-row').each(function (idx, row) {

                /* If we are reordering a table with some fixed rows, make sure the fixed rows
                 * always follow their corresponding sortable row so they always appear together. */
                if (tableHasUnsortableRows) {
                    let uniqueID = $(this).attr('data-item-id')
                    correspondingFixedRow = $('tr:not(.sortable-row)[data-item-detail-for-id=' + uniqueID + ']');
                    correspondingFixedRow.detach().insertAfter($(this));
                }
            });
       }
    }).disableSelection();

    let rowID = 0;
    $('#item-master-datatable').DataTable()
    $('#add-new').on('click', function() {
        rowID++
        let template = $('.template-new').clone().removeClass('template-new d-none')
        let rowDetailID = 'row-detail-'+rowID

        template.find('.item_code').select2({ templateSelection: formatState, dropdownAutoWidth : true })
        template.find('.subject').val()
        template.find('.quantity').val(1)
        template.find('.unit_measure').val(null)
        template.find('.unit_price').val(0)
        template.find('.unit_price').inputmask('currency', { prefix: '', rightAlign: true })
        template.find('.discount').val(0)
        template.find('.discount').inputmask({ rightAlign: true })
        template.find('.amount').val(0)
        template.find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
        template.find('.taxed_amount').inputmask('currency', { prefix: '', rightAlign: true })
        template.find('.toggle-details').attr('data-target', '#'+rowDetailID)
        template.eq(1).attr('id', rowDetailID)

        $('#table-form-tbody').append(template)
        $(this).addClass('d-none')
        onHandler(rowDetailID)
    })

    function onHandler(rowDetailID) {
        $('.item_code').on('change', function() {
            let row = $(this).parent().parent()
            let content = $(this).data('contents').filter(content => content.I_CODE == $(this).val())[0]

            row.find('.subject').val(content.I_DESC)
            row.find('.quantity').val(1)
            row.find('.unit_measure').val(content.I_UOM)
            row.find('.unit_price').val(content.I_PRICE)
            row.find('.discount').val(0)
            row.find('.amount').val(content.I_PRICE)
            row.find('.toggle-details').attr('data-target', '#'+rowDetailID)
            row.eq(1).attr('id', rowDetailID)
            row.parent().find('#'+rowDetailID).find('.t_detail').summernote('code', content.I_DETAIL)

            $("#add-new").removeClass('d-none')
        })

        $('.calculate-field').change(function() {
            let row = $(this).parent().parent()
            let amount = row.find('.amount').val()
            let uprice = row.find('.unit_price').val()
            let qty = row.find('.quantity').val()
            let discount = row.find('.discount')
            let theDiscount = (discount.val().indexOf('%') > -1) ? discount.val().replace('%', '') : discount.val()

            // calculate discount
            let discountAmount = 0.00
            if (discount.val().indexOf('%') > -1)
                discountAmount = uprice * qty * theDiscount / 100
            else
                discountAmount = row.find('.discount').val()

            let price = (uprice * qty) - discountAmount
            row.find('.amount').val(price)

            row.find('.taxed_amount').val(price)

            // update the subtotal and grandtotal fields
            updateTotals()
        })

        $('.global_discount').on('change', function() {
            updateTotals()
        })

        $('.remove-row').on('click', function() {
            $(this).closest('tr').next('tr').remove()
            $(this).parent().parent().remove()
            $("#add-new").removeClass('d-none')
            updateTotals()
        })

        function updateTotals() {
            let amountFields = $('.amount')
            let discountFields = $('.discount')
            let globalDiscount = $('.global_discount')

            let subtotal = 0.00
            for (var i = 1; i < amountFields.length; i++)
                subtotal += parseFloat(amountFields.eq(i).val())


            // calculate discount
            let globalDiscountAmount = 0.00
            let theGlobalDiscount = (globalDiscount.val().indexOf('%') > -1) ? globalDiscount.val().replace('%', '') : globalDiscount.val()
            if (globalDiscount.val().indexOf('%') > -1)
                globalDiscountAmount = subtotal * theGlobalDiscount / 100
            else
                globalDiscountAmount = theGlobalDiscount

            let discountedPrice = subtotal - globalDiscountAmount;
            let grandTotal = (subtotal) - globalDiscountAmount

            // show subtotal
            $('#subtotal').val(subtotal.toFixed(2))

            // show rounding if allowed
            if (boolRounding) {
                let rounding = calculateRounding(discountedPrice)
                let roundedPrice = calculateRoundedPrice(discountedPrice, rounding)
                $('#rounding').val(rounding)
                $('#grand_total').val(roundedPrice.toFixed(2))
            } else {
                $('#grand_total').val(grandTotal.toFixed(2))
            }
        }

        function calculateRounding(price) {
            price = price.toFixed(2)
            let lastDecimal = price.toString().slice(-1);
            switch (lastDecimal) {
                case '1': return -0.01; break;
                case '2': return -0.02; break;
                case '3': return 0.02; break;
                case '4': return 0.01; break;
                case '5': return 0.00; break;
                case '6': return -0.01; break;
                case '7': return -0.02; break;
                case '8': return 0.02; break
                case '9': return 0.01; break;
                default: return 0.00; break;
            }
        }

        function calculateRoundedPrice(price, rounding) {
            return roundedPrice = parseFloat(price) + parseFloat(rounding)
        }
    }

}) ()
</script>
@endpush
