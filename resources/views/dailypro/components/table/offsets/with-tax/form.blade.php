<tr class="narrow-padding template-new d-none">
    <td class="voucher">
        <span class="value"></span>
    </td>
    <td class="deposit-amount">
        <span class="value"></span>
    </td>
    <td class="date">
        <span class="value"></span>
    </td>
    <td class="bill-no">
        <span class="value"></span>
    </td>
    <td class="amount">
        <span class="value"></span>
    </td>
    <td class="apply-amount">
        {!! Form::checkbox('item_check[]', null, false, ['class' => 'item_check']) !!}
        {!! Form::hidden('existing_payment[]', false, ['class' => 'existing_payment']) !!}
        {!! Form::hidden('is_checked[]', false, ['class' => 'is_checked']) !!}
    </td>
</tr>
