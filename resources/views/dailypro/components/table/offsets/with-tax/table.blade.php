<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark-blue text-white text-sm offsets-table">
                <th>Vouchers</th>
                <th>Deposit Amount</th>
                <th>Date</th>
                <th>Bill No</th>
                <th>Amount</th>
                <th>Apply</th>
            </tr>
        </thead>
        <tbody id="table-form-tbody">
            @include('billing-module.components.table.offsets.with-tax.form')
            @includeWhen(isset($items), 'billing-module.components.table.offsets.with-tax.row')
        </tbody>
    </table>
</div>

