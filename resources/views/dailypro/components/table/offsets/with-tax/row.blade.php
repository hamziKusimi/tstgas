@if (isset($items))
    @foreach ($items as $item)
        <tr class="narrow-padding template-new d-none">
            <td class="voucher">
                <span class="value">
                    {{ $item->doc_no }}
                </span>
            </td>
            <td class="deposit-amount">
                <span class="value">
                    {{ $item->amount }}
                </span>
            </td>
            <td class="date">
                <span class="value"></span>
            </td>
            <td class="bill-no">
                <span class="value"></span>
            </td>
            <td class="amount">
                <span class="value"></span>
            </td>
            <td class="apply-amount">
                {!! Form::checkbox('item_check[]', 'checked', true, ['class' => 'item_check']) !!}
                {!! Form::hidden('existing_payment[]', true, ['class' => 'existing_payment']) !!}
                {!! Form::hidden('is_checked[]', true, ['class' => 'is_checked']) !!}
            </td>
        </tr>
    @endforeach
@endif
