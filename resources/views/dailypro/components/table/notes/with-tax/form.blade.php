<tr class="narrow-padding template-new d-none">
    <td>
        {!! Form::hidden('item_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('sequence_no[]', null, ['class' => 'form-control sequence_no']) !!}
    </td>
    <td>
        {{-- use reference_no for invoice_no --}}
        {!! Form::select('invoice_no[]', [], null, [
            'class' => 'form-control invoice_no',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-route' => route('invoices.api.get.items', 'INV_ID')
        ]) !!}
    </td>
    <td>
        {!! Form::select('item_code[]', [], null, [
            'class' => 'form-control item_code calculate-field',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-route' => route('item-master.api.store')
        ]) !!}
        {!! Form::hidden('account_code[]', null, ['class' => 'account_code']) !!}
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('subject[]', null, ['class' => 'form-control subject']) !!}
        </div>
    </td>
    <td>
        {!! Form::number('quantity[]', null, ['class' => 'form-control quantity calculate-field', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('unit_measure[]', null, [ 'class' => 'form-control unit_measure' ]) !!}
    </td>
    <td>
        {!! Form::text('unit_price[]', null, ['class' => 'form-control unit_price calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('discount[]', null, ['class' => 'form-control discount calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('amount[]', null, ['class' => 'form-control amount calculate-field', 'disabled']) !!}
        {!! Form::hidden('amount_dt[]', null, ['class' => 'form-control amount_dt']) !!}
    </td>
    <td>
        {!! Form::text('tax_amount[]', null, ['class' => 'form-control tax_amount money']) !!}
    </td>
    <td>
        {!! Form::text('taxed_amount[]', null, ['class' => 'form-control taxed_amount money']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
