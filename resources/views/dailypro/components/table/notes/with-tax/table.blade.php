<div class="tax-container" data-taxes="{{ $taxCodes }}"></div>
<div class="table-responsive">
    <table class="table table-sm" id="top-table">
        <thead>
            <tr class="bg-dark-blue text-white text-sm notes-table">
                <th width="7.5%" class="text-center">SQN</th>
                <th width="10%" class="text-center">Doc No</th>
                <th width="7.5%" class="text-center">Code</th>
                <th width="25%" class="text-left">Description</th>
                <th width="5%" class="text-center">Qty</th>
                <th width="6%" class="text-center">UOM</th>
                <th width="8%" class="text-center">U.Price</th>
                <th width="4%" class="text-center">Discount</th>
                <th width="8%" class="text-center">Amount</th>
                <th width="8%" class="text-center">Tax Amt</th>
                <th width="10%" class="text-center">Amount (Tax)</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="table-form-tbody">
            @include('dailypro.components.table.notes.with-tax.form')
            @includeWhen(isset($items), 'dailypro.components.table.notes.with-tax.row')
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <button type="button" class="form-control btn bg-dark-green text-white" id="add-new">
                        Add Line
                    </button>
                </td>
                <td>
                    <button type="button" class="form-control btn btn-secondary text-white d-none" id="add-new-item"
                        data-contents="{{ $itemsMaster }}" data-route={{ route('item-master.api.store') }}>
                        Add New Item
                    </button>
                </td>
                <td colspan="8"></td>
            </tr>
        </tfoot>
    </table>
</div>

