@if (isset($items))
    @foreach ($items as $item)
        <tr class="narrow-padding bill sortable-row existing-data" data-item-id="{{ $item->id }}"
            data-delete-route="{{ $deleteRoute }}">
            <td>
                {!! Form::hidden('item_id[]', $item->id, ['class' => 'item-id']) !!}
                {!! Form::text('sequence_no[]', $item->sequence_no, ['class' => 'form-control sequence_no']) !!}
            </td>
            <td>
                {{-- use reference_no for invoice_no --}}
                {{-- the value is added from JS --}}
                {!! Form::select('invoice_no[]', [], null, [
                    'class' => 'form-control invoice_no',
                    'placeholder' => '',
                    'style' => 'width:100%; height:100%',
                    'data-route' => route('invoices.api.get.items', 'INV_ID'),
                    'data-value' => $item->reference_no
                ]) !!}
            </td>
            <td>
                {!! Form::select('item_code[]', $itemsMaster->pluck('itemMastercode', 'I_CODE'), $item->item_code, ['class' => 'form-control item_code']) !!}
                {!! Form::hidden('account_code[]', $item->account_code, ['class' => 'account_code']) !!}
            </td>
            <td>
                <div class="input-group">
                    {!! Form::text('subject[]', $item->subject, ['class' => 'form-control subject']) !!}
                </div>
            </td>
            <td>
                {!! Form::number('quantity[]', $item->quantity, ['class' => 'form-control quantity calculate-field', 'min' => '1', 'step' => '0.01']) !!}
            </td>
            <td>
                {!! Form::text('unit_measure[]', $item->unit_measure, [ 'class' => 'form-control unit_measure' ]) !!}
            </td>
            <td>
                {!! Form::text('unit_price[]', $item->unit_price, ['class' => 'form-control unit_price calculate-field text-right']) !!}
            </td>
            <td>
                {!! Form::text('discount[]', $item->discount, ['class' => 'form-control discount calculate-field text-right']) !!}
            </td>
            <td>
                {!! Form::text('amount[]', $item->amount, ['class' => 'form-control amount calculate-field text-right', 'disabled']) !!}
                {!! Form::hidden('amount_dt[]', $item->amount, ['class' => 'form-control amount_dt']) !!}
            </td>
            <td>
                {!! Form::text('tax_amount[]', $item->tax_amount, ['class' => 'form-control tax_amount money']) !!}
            </td>
            <td>
                {!! Form::text('taxed_amount[]', $item->taxed_amount, ['class' => 'form-control taxed_amount money']) !!}
            </td>
            <td>
                <button type="button" class="btn btn-danger remove-row">
                    <i class="fa fa-times"></i>
                </button>
            </td>
        </tr>
        <tr class="narrow-padding row-detail-filled collapse" id="details_{{ $item->id }}" data-item-detail-for-id="{{ $item->id }}">
            <td colspan="13">
                {!! Form::textarea('details[]', $item->details, ['class' => 'form-control t_detail', 'rows' => 1]) !!}
            </td>
        </tr>
    @endforeach
@endif
