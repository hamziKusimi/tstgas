<tr class="narrow-padding template-new d-none">
    <td>
        {!! Form::select('item_code[]', $itemsMaster->pluck('itemMastercode', 'I_CODE'), null, [
            'class' => 'form-control item_code calculate-field',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-contents' => $itemsMaster
        ]) !!}
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('subject[]', null, ['class' => 'form-control subject']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#row-detail-id">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand" ></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::number('quantity[]', null, ['class' => 'form-control quantity calculate-field', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('unit_measure[]', null, ['class' => 'form-control unit_measure']) !!}
    </td>
    <td>
        {!! Form::text('unit_price[]', null, ['class' => 'form-control unit_price calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('discount[]', null, ['class' => 'form-control discount calculate-field']) !!}
    </td>
    <td>
        {!! Form::text('amount[]', null, ['class' => 'form-control amount calculate-field', 'disabled']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="narrow-padding template-new d-none row-detail collapse">
    <td colspan="8">
        {!! Form::textarea('t_detail[]', null, ['class' => 'form-control t_detail', 'rows' => 1]) !!}
    </td>
</tr>
