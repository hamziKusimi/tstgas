@if (isset($items))
@foreach ($items as $item)
<tr class="narrow-padding sortable-row existing-data" data-item-id="{{ $item->id }}"
    data-delete-route="{{ $deleteRoute }}">
    <td>
        {!! Form::hidden('item_id[]', $item->id, ['class' => 'item-id']) !!}
        {!! Form::text('sequence_no[]', $item->sequence_no, ['class' => 'form-control sequence_no']) !!}
    </td>
    <td>
                {!! Form::text('serial[]', $item->serial, ['class' => 'form-control serial']) !!}
                {!! Form::hidden('account_code[]', $item->account_code, ['class' => 'account_code']) !!}
                {!! Form::hidden('reference_no[]', $item->reference_no, ['class' => 'reference_no']) !!}
            </td>
    <td>
                {!! Form::text('barcode[]', $item->barcode, ['class' => 'form-control barcode calculate-field text-right']) !!}
            </td>
    <td>
        <div class="input-group">
            {!! Form::text('subject[]', $item->subject, ['class' => 'form-control subject']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse"
                data-target="#details_{{ $item->id }}">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand"></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::text('product[]', $item->product, ['class' => 'form-control product calculate-field text-right']) !!}
    </td>
    <td>
        {!! Form::text('type[]', $item->type, ['class' => 'form-control type calculate-field text-right']) !!}
    </td>
    <td>
        {!! Form::number('quantity[]', $item->quantity, ['class' => 'form-control quantity calculate-field', 'min' =>
        '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('unit_price[]', $item->unit_price, ['class' => 'form-control unit_price calculate-field
        text-right']) !!}
    </td>
    <td>
        {!! Form::text('uom[]', $item->uom, ['class' => 'form-control uom calculate-field text-right'])
        !!}
        {!! Form::hidden('discount[]', $item->discount, ['class' => 'form-control discount calculate-field text-right'])
        !!}
    </td>
    <td>
        {!! Form::text('amount[]', $item->amount, ['class' => 'form-control amount calculate-field text-right',
        'disabled']) !!}
        {!! Form::hidden('amount_dt[]', $item->amount, ['class' => 'form-control amount_dt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="narrow-padding row-detail-filled collapse" id="details_{{ $item->id }}"
    data-item-detail-for-id="{{ $item->id }}">
    <td colspan="13">
        {!! Form::textarea('details[]', $item->details, ['class' => 'form-control t_detail summernote', 'rows' => 1])
        !!}
    </td>
</tr>
@endforeach
@endif