<tr class="narrow-padding modal-template-new d-none">
    <td>
        {!! Form::text('qodocno[]', null, ['class' => 'form-control qodocno', 'disabled']) !!}
    </td>
    <td>
        {!! Form::text('qsequence_no[]', null, ['class' => 'form-control qsequence_no', 'disabled']) !!}
    </td>
    {{-- {{ dd($stockcode) }} --}}
    <td>
        {!! Form::select('qitem_code[]', $stockcode->pluck('code', 'code'), null, [
            'class' => 'form-control qitem_code calculate-field2',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-contents' => $stockcode, 'disabled'
            // 'data-route' => route('item-master.api.store')
        ]) !!}
        {!! Form::hidden('qaccount_code[]', null, ['class' => 'paccount_code', 'disabled']) !!}
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('qsubject[]', null, ['class' => 'form-control qsubject', 'disabled']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#row-detail-id">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand" ></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::number('qquantity[]', null, ['class' => 'form-control qquantity calculate-field2', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
            {!! Form::select('qunit_measure[]', [], null, [
                'class' => 'form-control qunit_measure',
                'placeholder' => '',
                'style' => 'width:100%; height:100%',
                'data-uoms' => $uom, 'disabled'
                // 'data-route' => route('item-master.api.store')
            ]) !!}
            {{-- {!! Form::hidden('qunit_measure[]', null, ['class' => 'punit_measure']) !!} --}}
        {{-- {!! Form::text('qunit_measure[]', null, [ 'class' => 'form-control qunit_measure' ]) !!} --}}
    </td>
    <td>
        {!! Form::text('qrate[]', null, [ 'class' => 'form-control qrate', 'disabled' ]) !!}
    </td>
    <td>
        {!! Form::text('qsupp-price[]', null, ['class' => 'form-control qsupp-price calculate-field2']) !!}
    </td>
    <td>
        {!! Form::text('qamount[]', null, ['class' => 'form-control qamount calculate-field2', 'disabled']) !!}
        {!! Form::hidden('qamount_dt[]', null, ['class' => 'form-control qamount_dt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>