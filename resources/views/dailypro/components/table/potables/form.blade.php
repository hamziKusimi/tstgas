<tr class="narrow-padding modal-template-new d-none">
    <td>
        {!! Form::text('podocno[]', null, ['class' => 'form-control podocno', 'disabled']) !!}
    </td>
    <td>
        {!! Form::text('psequence_no[]', null, ['class' => 'form-control psequence_no', 'disabled']) !!}
    </td>
    {{-- {{ dd($stockcode) }} --}}
    <td>
        {!! Form::select('pitem_code[]', $stockcode->pluck('code', 'code'), null, [
            'class' => 'form-control pitem_code calculate-field2',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-contents' => $stockcode, 'disabled'
            // 'data-route' => route('item-master.api.store')
        ]) !!}
        {!! Form::hidden('paccount_code[]', null, ['class' => 'paccount_code', 'disabled']) !!}
    </td>
    <td>
        <div class="input-group">
            {!! Form::text('psubject[]', null, ['class' => 'form-control psubject', 'disabled']) !!}
            <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#row-detail-id">
                <span class="input-group-text  bg-dark-blue text-white">
                    <i class="fa fa-expand" ></i>
                </span>
            </div>
        </div>
    </td>
    <td>
        {!! Form::number('pquantity[]', null, ['class' => 'form-control pquantity calculate-field2', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
            {!! Form::select('punit_measure[]', [], null, [
                'class' => 'form-control punit_measure',
                'placeholder' => '',
                'style' => 'width:100%; height:100%',
                'data-uoms' => $uom, 'disabled'
                // 'data-route' => route('item-master.api.store')
            ]) !!}
            {{-- {!! Form::hidden('punit_measure[]', null, ['class' => 'punit_measure']) !!} --}}
        {{-- {!! Form::text('punit_measure[]', null, [ 'class' => 'form-control punit_measure' ]) !!} --}}
    </td>
    <td>
        {!! Form::text('prate[]', null, [ 'class' => 'form-control prate', 'disabled' ]) !!}
    </td>
    <td>
        {!! Form::text('psupp-price[]', null, ['class' => 'form-control psupp-price calculate-field2']) !!}
    </td>
    <td>
        {!! Form::text('pamount[]', null, ['class' => 'form-control pamount calculate-field2', 'disabled']) !!}
        {!! Form::hidden('pamount_dt[]', null, ['class' => 'form-control pamount_dt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>