@if (isset($items))
    @foreach ($items as $item)
        <tr class="narrow-padding sortable-row existing-data" data-item-id="{{ $item->id }}"
            data-delete-route="{{ $deleteRoute }}">
            <td>
                {!! Form::hidden('pitem_id[]', $item->id, ['class' => 'item-id']) !!}
                {!! Form::text('psequence_no[]', $item->psequence_no, ['class' => 'form-control psequence_no']) !!}
            </td>
            <td>
                {!! Form::select('pitem_code[]', $stockcode->pluck('code', 'code'), $item->pitem_code, ['class' => 'form-control pitem_code new-item', 'style' => 'width:100%; height:100%']) !!}
                {!! Form::hidden('account_code[]', $item->account_code, ['class' => 'account_code']) !!}
            </td>
            <td>
                <div class="input-group">
                    {!! Form::text('psubject[]', $item->psubject, ['class' => 'form-control psubject']) !!}
                    <div class="input-group-append toggle-details" data-toggle="collapse" data-target="#details_{{ $item->id }}">
                        <span class="input-group-text  bg-dark-blue text-white">
                            <i class="fa fa-expand" ></i>
                        </span>
                    </div>
                </div>
            </td>
            <td>
                {!! Form::number('pquantity[]', $item->qty, ['class' => 'form-control pquantity calculate-field2', 'min' => '1', 'step' => '0.01']) !!}
            </td>
            <td>  
                {!! Form::select('punit_measure[]', $uom->pluck('code', 'code'), $item->uom, ['class' => 'form-control punit_measure new-item', 'style' => 'width:100%; height:100%']) !!}
                {{-- {!! Form::text('punit_measure[]', $item->uom, [ 'class' => 'form-control punit_measure' ]) !!} --}}
            </td>
            <td>
                {!! Form::text('prate[]', $item->prate, ['class' => 'form-control prate calculate-field2 text-right']) !!}
            </td>
            <td>
                {!! Form::text('pcunit_cost[]', $item->cucost, ['class' => 'form-control pcunit_cost calculate-field2 text-right']) !!}
            </td>
            <td>
                {!! Form::text('markup[]', $item->mkup, ['class' => 'form-control markup calculate-field2 text-right']) !!}
            </td>
            <td>
                {!! Form::text('unit_cost[]', $item->ucost, ['class' => 'form-control unit_cost calculate-field2 text-right', 'readonly']) !!}
            </td>
            <td>
                {!! Form::text('discount[]', $item->discount, ['class' => 'form-control discount calculate-field2 text-right']) !!}
            </td>
            <td>
                {!! Form::text('amount[]', $item->amount, ['class' => 'form-control amount calculate-field2 text-right', 'disabled']) !!}
                {!! Form::hidden('amount_dt[]', $item->amount, ['class' => 'form-control amount_dt']) !!}
                {!! Form::hidden('taxed_amount_dt[]', $item->taxed_amount, ['class' => 'form-control taxed_amount_dt']) !!}
            </td>
            <td>
                <button type="button" class="btn btn-danger remove-row">
                    <i class="fa fa-times"></i>
                </button>
            </td>
        </tr>
        <tr class="narrow-padding row-detail-filled collapse" id="details_{{ $item->id }}" data-item-detail-for-id="{{ $item->id }}">
            <td colspan="13">
                {!! Form::textarea('details[]', $item->details, ['class' => 'form-control t_detail summernote', 'rows' => 1]) !!}
            </td>
        </tr>
    @endforeach
@endif
