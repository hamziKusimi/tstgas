<div class="tax-container" data-taxes="{{ $taxCodes }}"></div>
<div class="table-responsive">
    <div class="box">
        <table class="table table-sm" id="opiniondt">
            <thead>
                <tr class="bg-dark-blue text-white text-sm">
                    <th width="1%"></th>
                    <th width="4%" class="text-center">No</th>
                    <th width="6%" class="text-center">Category</th>
                    <th width="6%" class="text-left">Product</th>
                    <th width="6%" class="text-center">Capacity</th>
                    <th width="6%" class="text-center">Pressure</th>
                    <th width="4%" class="text-center">Qty</th>
                    <th width="6%" class="text-center">Gas Price</th>
                    <th width="6%" class="text-center">Daily Rate</th>
                    <th width="6%" class="text-center">Monthly Rate</th>
                    <th width="7%" class="text-center">Bill Date</th>
                    <th width="2%">Rmk</th>
                    <th width="1%"></th>
                    {{-- {{-- <th width="10%" class="text-center">Loading</th> --}}
                    {{-- <th width="10%" class="text-center">Unloading</th> --}}
                </tr>
            </thead>
            <tbody id="table-form-tbody" class="cylindertable">
                @include('dailypro.components.table.deliverynotes.form')
                @includeWhen(isset($contents), 'dailypro.components.table.deliverynotes.row')
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="1">
                        <button type="button" class="form-control btn bg-dark-green text-white" style="width:70px"
                            id="add-new">
                            Add
                        </button>
                    </td>
                    <td colspan="8"></td>
                </tr>
            </tfoot>
        </table>
        <div class="overlay delete-overlay">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm-4">
      <div class="modal-content">
        <div class="modal-header">
        <h5>Remarks </h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <label style="font-style: italic; font-weight: bold">Only Key In Remark for Loose Cylinder</label>
            <input class="form-control remarkText" name="remarkModal" data-index="null"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success update-remark" data-dismiss="modal">Update</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

    <!-- Modal -->
    <div class="modal fade" id="gasRack" role="dialog" data-url="null" data-dnno="null" data-rackno="null">
        <div class="modal-dialog modal-sm-4">
            <div class="box">
          <div class="modal-content">
            <div class="modal-header">
            <h5>Gas Rack</h5>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                    <div class="row">
                        {!! Form::label('date', 'Next Insp. Due at ', ['class' => 'col-form-label col-md-4 text-right']) !!}
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('inspDate', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'datepicker-inspDate','readonly'=>'readonly'
                                    ])!!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {!! Form::label('date', 'Re-Test Date', ['class' => 'col-form-label col-md-4 text-right']) !!}
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('testDate', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'datepicker-testDate','readonly'=>'readonly'
                                    ])!!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {!! Form::label('date', '4 Legged Sling No.', ['class' => 'col-form-label col-md-4 text-right']) !!}
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('slingNo', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'txtLeggedSling'
                                    ])!!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {!! Form::label('date', 'G.W.L (Ton)', ['class' => 'col-form-label col-md-4 text-right']) !!}
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('gwl', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'txtGWL'
                                    ])!!}
                                </div>
                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success save-detail" >Save</button>
              <button type="button" class="btn btn-default close-detail" data-dismiss="modal">Close</button>
            </div>
          </div>
            <div class="overlay rack-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
        </div>
      </div>

