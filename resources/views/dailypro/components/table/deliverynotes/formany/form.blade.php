
<tr class="narrow-padding toggle-details main-table-row-template2 d-none" data-row-id3="0">
    <td class="details-control toggle-inner-table-button">
    </td>
    <td>
        {!! Form::hidden('fmitem_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('fmsequence_no[]', null, ['class' => 'form-control fmsequence_no']) !!}
    </td>
    <td>
        {!! Form::select('fmproduct[]', [], null, [
            'class' => 'form-control fmproduct',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            // 'data-gstype' => $formanytype,
        ]) !!}
      
    </td>
    <td>
        {!! Form::number('fmquantity[]', null, ['class' => 'form-control validate-barcode fmquantity', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('fmprice[]', null, ['class' => 'form-control fmprice']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id3="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="inner-table second-row-template2 d-none" data-row-id3="0">
    <td colspan="9">
        <table class="table table-sm narrow-padding formany-table">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="formany"></tbody>
        </table>
    </td>
</tr>
