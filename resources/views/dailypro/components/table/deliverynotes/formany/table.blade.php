<div class="tax-container"></div>
<div class="table-responsive">
    <table class="table table-sm" id="formanydt">
        <thead>
            <tr class="bg-dark-blue text-white text-sm">
                <th width="4.5%"></th>
                <th width="12%" class="text-center">No</th>
                <th width="14%" class="text-center">Product</th>
                <th width="14%" class="text-center">Qty</th>
                <th width="14%" class="text-center">Price</th>
                <th width="12%"></th>
            </tr>
        </thead>
        <tbody id="table-form-tbody-formany" class="formany">
            @include('dailypro.components.table.deliverynotes.formany.form')
            @includeWhen(isset($grcontents), 'dailypro.components.table.deliverynotes.formany.row')
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1">    
                    <button type="button" class="form-control btn bg-dark-green text-white" id="add-new3">
                        Add Line
                    </button>
                </td>
                <td colspan="8"></td>
            </tr> 
        </tfoot>
    </table>
</div>

