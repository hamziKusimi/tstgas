<tr class="narrow-padding template-new-child d-none" style="
background-color: #d7f3e3;">
        <td>
            {!! Form::hidden('item_id[]', null, ['class' => 'item-id']) !!}
            {{-- {!! Form::select('barcode[]', [], null, [
            'class' => 'form-control barcode',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            // 'data-val' => $cylinder,
            // 'data-route' => route('item-master.api.store')
            ]) !!} --}}
            
            {!! Form::text('barcode[]', null, ['class' => 'form-control barcode', 'placeholder'=>'Barcode']) !!}
            {!! Form::text('serialno[]', null, ['class' => 'form-control serialno', 'placeholder'=>'Serial No']) !!}
        </td>
        {{-- {{ dd($cylinder) }} --}}
      
        <td>
            {!! Form::text('categorydt[]', null, [ 'class' => 'form-control categorydt' ]) !!}
        </td>
        <td>
            {!! Form::text('productdt[]', null, [ 'class' => 'form-control productdt' ]) !!}
        </td>
        <td>
            {!! Form::text('capacitydt[]', null, [ 'class' => 'form-control capacitydt' ]) !!}
        </td>
        <td>
            {!! Form::text('pressuredt[]', null, ['class' => 'form-control pressuredt calculate-field']) !!}
        </td>
        <td>
            {!! Form::number('quantitydt[]', null, ['class' => 'form-control quantitydt calculate-field', 'min' => '1', 'step' => '0.01']) !!}
        </td>
        <td>
            {!! Form::text('pricedt[]', null, ['class' => 'form-control pricedt calculate-field']) !!}
        </td>
        <td>
            {!! Form::text('loadingdt[]', null, ['class' => 'form-control loadingdt calculate-field']) !!}
        </td>
        <td>
            {!! Form::text('uloadingdt[]', null, ['class' => 'form-control uloadingdt calculate-field']) !!}
        </td>
        <td>
            <button type="button" class="btn btn-danger remove-row-child" data-id="0">
                <i class="fa fa-times"></i>
            </button>
        </td>
    </tr>
    <tr class="narrow-padding template-new d-none row-detail collapse">
        <td colspan="13">
            {!! Form::textarea('details[]', null, ['class' => 'form-control t_detail', 'rows' => 1]) !!}
        </td>
    </tr>