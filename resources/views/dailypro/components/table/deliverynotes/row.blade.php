@if (isset($contents))
@foreach ($contents as $i => $content)
<tr class="narrow-padding sortable-row existing-data existing-data-id-{{ $content->id }}"
    data-content-id="{{ $content->id }}" data-row-id="{{ $content->id }}" data-existed="1">
    <td class="details-control toggle-inner-table-button-exist" data-second-row-id="row-id-{{ $content->id }}">
    </td>
    <td>
        {!! Form::hidden('content_id[]', $content->id, ['class' => 'content-id']) !!}
        {!! Form::hidden('content_cyl_id[]', $content->id, ['class' => 'content-id']) !!}
        {!! Form::text('sequence_noex[]', $content->sequence_no, ['class' => 'form-control sequence_noex', 'disabled'])
        !!}
        {!! Form::hidden('sequence_no_edit[]', $content->sequence_no, ['class' => 'form-control sequence_no']) !!}
        {{-- {!! Form::hidden('edit_ul[]', $content->sequence_no, ['class' => 'form-control edit_ul']) !!} --}}
    </td>
    <td>
        {!! Form::text('categoryex[]', $content->category, [ 'class' => 'form-control categoryex category', 'disabled'
        ]) !!}
        {{-- {!! Form::hidden('categorydt[]', $content->category, [ 'class' => 'form-control categorydt']) !!} --}}
    </td>
    <td>
        {!! Form::text('productex[]', $content->product, [ 'class' => 'form-control productex product', 'disabled' ])
        !!}
        {{-- {!! Form::hidden('productdt[]', $content->product, [ 'class' => 'form-control productdt']) !!} --}}
    </td>
    <td>
        {!! Form::text('capacityex[]', $content->capacity, [ 'class' => 'form-control capacityex capacity', 'disabled'])
        !!}
        {{-- {!! Form::hidden('capacitydt[]', $content->capacity, [ 'class' => 'form-control capacitydt']) !!} --}}
    </td>
    <td>
        {!! Form::text('pressureex[]', $content->pressure, ['class' => 'form-control pressureex text-right pressure',
        'disabled']) !!}
        {{-- {!! Form::hidden('pressuredt[]', $content->pressure, ['class' => 'form-control pressuredt text-right']) !!} --}}
    </td>
    <td>
        {!! Form::number('quantityex[]', $content->qty, ['class' => 'form-control quantityex', 'min' => '1', 'step' =>
        '1', 'disabled']) !!}
        {{-- {!! Form::hidden('quantity[]', $content->qty, ['class' => 'form-control quantity', 'min' => '1', 'step' => '1']) !!} --}}
    </td>
    <td>
        {!! Form::text('gas_priceex[]', $content->gas_price, ['class' => 'form-control gas_priceex text-right',
        'required'=>'required', 'disabled']) !!}
        {{-- {!! Form::hidden('gas_price[]', $content->gas_price, ['class' => 'form-control gas_price text-right']) !!} --}}
    </td>
    <td>
        {!! Form::text('daily_priceex[]', $content->daily_price, ['class' => 'form-control daily_priceex text-right',
        'required'=>'required', 'disabled']) !!}
        {{-- {!! Form::hidden('daily_price[]', $content->daily_price, ['class' => 'form-control daily_price text-right']) !!} --}}
    </td>
    <td>
        {!! Form::text('monthly_priceex[]', $content->monthly_price, ['class' => 'form-control monthly_priceex
        text-right', 'required'=>'required', 'disabled']) !!}
        {{-- {!! Form::hidden('monthly_price[]', $content->monthly_priceprice, ['class' => 'form-control monrhly_price text-right']) !!} --}}
    </td>
    <td>
        {!! Form::text('bill_dateex[]', date('d/m/Y', strtotime($content->bill_date)), ['class' => 'form-control
        bill_dateex text-right', 'required'=>'required', 'disabled']) !!}
        {{-- {!! Form::hidden('bill_date[]', date('d/m/Y', strtotime($content->bill_date)), ['class' => 'form-control bill_date text-right']) !!} --}}
    </td>
    <td>
        <button type="button" class="btn btn-primary add-remark" data-toggle="modal" data-target="#myModal" data-remark="0">
            <i class="fa fa-plus"></i>
        </button>
        {!! Form::hidden('remark_exist[]', $content->remarks, ['class' => 'form-control remarkdt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row"
            data-delete-route="{{  route('deliverynotes.data.destroy', $content->id)  }}">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="inner-table d-none row-id-{{  $content->id  }}" data-row-id="{{ $content->id }}">
    <td colspan="12">
        <table class="table table-sm narrow-padding cylinder-table">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">Prepare</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            @if (isset($content->cylinders))
            <tbody>

                @foreach ($content->cylinders as $master)
                @php
                $edit_ll = '<input name="edit_ll_barcode[]" type="hidden" value="' . $master->barcode . '">';
                $edit_ll .= '<input name="edit_ll_serial[]" type="hidden" value="' . $master->serial . '">';
                $edit_ll .= '<input name="edit_ll_sequence_no[]" type="hidden" value="' . $master->dt_sqn . '">';
                $edit_ll .= '<input name="edit_ll_sqn_no[]" class="sqn_no" type="hidden" value="' . $master->list_sqn . '">';
                $edit_ll .= '<input name="edit_ll_datetime[]" class="datetime" type="hidden" value="">';
                $edit_ll .= '<input name="edit_ll_driver[]" type="hidden" value="' . $master->driver . '">';
                $edit_ll .= '<input name="edit_ll_type[]" type="hidden" value="cy">';

                $edit_ul = '<input name="edit_ul_barcode[]" type="hidden" value="' . $master->barcode . '">';
                $edit_ul .= '<input name="edit_ul_serial[]" type="hidden" value="' . $master->serial . '">';
                $edit_ul .= '<input name="edit_ul_sequence_no[]" type="hidden" value="' . $master->dt_sqn . '">';
                $edit_ul .= '<input name="edit_ul_sqn_no[]" class="sqn_no" type="hidden" value="' . $master->list_sqn . '">';
                $edit_ul .= '<input name="edit_ul_datetime[]" class="datetime" type="hidden" value="">';
                $edit_ul .= '<input name="edit_ul_driver[]" type="hidden" value="' . $master->driver . '">';
                $edit_ul .= '<input name="edit_ul_type[]" type="hidden" value="cy">';
                @endphp
                <tr class="new-row" data-isexisted="1">
                    <td class="text-center barcode">{{ $master->barcode }}</td>
                    <td class="text-center serialno">{{ $master->serial }}</td>
                    <td class="text-center driver">{{ $master->driver }}</td>
                    <td class="text-center">
                        {{ (isset($master->prepare_date))?date('d/m/Y', strtotime($master->prepare_date)):'' }}</td>
                    <td class="text-center loadingdt" width="20%">{!! (isset($master->loading_date))?date('d/m/Y',
                        strtotime($master->loading_date)):'<input class="form-control loadingdt calculate-field"
                            readonly="" name="edit_ll_date[]" type="text">' . $edit_ll !!}</td>
                    <td class="text-center" width="20%">{!! $master->unloading_date == null ? '<input
                            class="form-control uloadingdt calculate-field" readonly="" name="edit_ul_date[]"
                            type="text">' . $edit_ul :date('d/m/Y', strtotime($master->unloading_date))
                        !!}</td>
                    <td class="text-center">
                        <button type="button" class="btn btn-danger remove-row-child" data-dnno="{{ $master->dn_no }}"
                            data-sqnex="{{ $master->dt_sqn }}" data-sqnlist="{{ $master->list_sqn }}" data-type="cy"
                            data-delete-routelist="{{ route('deliverynotes.data.destroylist') }}">
                            <i class="fa fa-times"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
            @endif
        </table>
    </td>
</tr>
@endforeach
@endif