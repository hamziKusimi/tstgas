@if (isset($grcontents))
@foreach ($grcontents as $i => $grcontent)
<tr class="narrow-padding sortable-row existing-data2" data-grcontent-id="{{ $grcontent->id }}"
    data-row-id2="{{ $grcontent->id}}" data-existed="1">
    <td class="details-control toggle-inner-table-button-exist" data-second-row-id="row-id2-{{ $grcontent->id }}">
    </td>
    <td>
        {!! Form::hidden('content_id[]', $grcontent->id, ['class' => 'grcontent-id']) !!}
        {!! Form::text('grsequence_nodt[]', $grcontent->sequence_no, ['class' => 'form-control grsequence_nodt',
        'disabled']) !!}
        {!! Form::hidden('gr_sequence_no[]', $grcontent->sequence_no, ['class' => 'form-control grsequence_no']) !!}
    </td>
    <td>
        {!! Form::text('typedt[]', $grcontent->type, [ 'class' => 'form-control typedt', 'disabled' ]) !!}
        {!! Form::hidden('grtypedt_t[]', $grcontent->type, [ 'class' => 'form-control grtype']) !!}
    </td>
    <td>
        {!! Form::number('quantitydt[]', $grcontent->qty, ['class' => 'form-control quantitydt', 'min' => '1', 'step' =>
        '0.01', 'disabled']) !!}
        {{-- {!! Form::hidden('grquantity[]', $grcontent->qty, ['class' => 'form-control grquantity', 'min' => '1', 'step' => '0.01']) !!} --}}
    </td>
    <td>
        {!! Form::text('daily_pricedt[]', $grcontent->daily_price, ['class' => 'form-control daily_pricedt text-right',
        'disabled']) !!}
        {{-- {!! Form::hidden('grdaily_price[]', $grcontent->daily_price, ['class' => 'form-control grdaily_price text-right']) !!} --}}
    </td>
    <td>
        {!! Form::text('monthly_pricedt[]', $grcontent->monthly_price, ['class' => 'form-control monthly_pricedt
        text-right', 'disabled']) !!}
        {{-- {!! Form::hidden('grmonthly_price[]', $grcontent->monthly_price, ['class' => 'form-control grmonthly_price text-right']) !!} --}}
    </td>
    <td>
        {!! Form::text('bill_datedt[]', date('d/m/Y', strtotime($grcontent->bill_date)), ['class' => 'form-control
        bill_datedt text-right', 'required'=>'required', 'disabled']) !!}
        {{-- {!! Form::hidden('grbill_date[]', date('d/m/Y', strtotime($grcontent->bill_date)), ['class' => 'form-control grbill_date text-right']) !!} --}}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row"
            data-delete-route="{{  route('deliverynotes.data.destroygr', $grcontent->id)  }}">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="inner-table d-none row-id2-{{ $grcontent->id }}" data-row-id2="{{ $grcontent->id}}">
    <td colspan="9">
        <table class="table table-sm narrow-padding gasrack-table" id="rackTable">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">Prepare</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            @if (isset($grcontent->gasracks))
            <tbody class="gasrack">
                @foreach ($grcontent->gasracks as $gasrack)
                @php
                $edit_ll = '<input name="edit_ll_barcode[]" type="hidden" value="' . $gasrack->barcode . '">';
                $edit_ll .= '<input name="edit_ll_serial[]" type="hidden" value="' . $gasrack->serial . '">';
                $edit_ll .= '<input name="edit_ll_sequence_no[]" type="hidden" value="' . $gasrack->dt_sqn . '">';
                $edit_ll .= '<input name="edit_ll_sqn_no[]" class="sqn_no" type="hidden" value="' . $gasrack->list_sqn . '">';
                $edit_ll .= '<input name="edit_ll_datetime[]" class="datetime" type="hidden" value="">';
                $edit_ll .= '<input name="edit_ll_driver[]" type="hidden" value="' . $gasrack->driver . '">';
                $edit_ll .= '<input name="edit_ll_type[]" type="hidden" value="gr">';

                $edit_ul = '<input name="edit_ul_barcode[]" type="hidden" value="' . $gasrack->barcode . '">';
                $edit_ul .= '<input name="edit_ul_serial[]" type="hidden" value="' . $gasrack->serial . '">';
                $edit_ul .= '<input name="edit_ul_sequence_no[]" type="hidden" value="' . $gasrack->dt_sqn . '">';
                $edit_ul .= '<input name="edit_ul_sqn_no[]" type="hidden" value="' . $gasrack->list_sqn . '">';
                $edit_ul .= '<input name="edit_ul_datetime[]" class="datetime" type="hidden" value="">';
                $edit_ul .= '<input name="edit_ul_driver[]" type="hidden" value="' . $gasrack->driver . '">';
                $edit_ul .= '<input name="edit_ul_type[]" type="hidden" value="gr">';
                @endphp
                <tr class="new-row2" data-isexisted="1" data-url="{{ route('deliverynote.set.rackdetail') }}" data-get-url="{{ route('deliverynote.get.rackdetail') }}" data-dnno="{{$grcontent->dn_no}}">
                    <td class="text-center barcode">{{ $gasrack->barcode }}</td>
                    <td class="text-center serialno">{{ $gasrack->serial }}</td>
                    <td class="text-center driver">{{ $gasrack->driver }}</td>
                    <td class="text-center">
                        {{ (isset($gasrack->prepare_date))?date('d/m/Y', strtotime($gasrack->prepare_date)):'' }}</td>
                    <td class="text-center" width="20%">{!! (isset($gasrack->loading_date))?date('d/m/Y',
                        strtotime($gasrack->loading_date)):'<input class="form-control loadingdt calculate-field"
                            readonly="" name="edit_ll_date[]" type="text">'. $edit_ll !!}</td>
                    <td class="text-center" width="20%">{!! $gasrack->unloading_date == null ?'<input
                            class="form-control uloadingdt calculate-field" readonly="" name="edit_ul_date[]"
                            type="text">' . $edit_ul : date('d/m/Y', strtotime($gasrack->unloading_date)) !!}</td>
                    <td>
                        <button type="button" class="btn btn-primary add-rack-detail" data-toggle="modal" data-target="#gasRack" >
                            <i class="fa fa-plus"></i>
                        </button>
                    </td>
                    <td class="text-center">
                        <button type="button" class="btn btn-danger remove-row-child" data-dnno="{{ $gasrack->dn_no }}"
                            data-sqndt="{{ $gasrack->dt_sqn }}" data-sqnlist="{{ $gasrack->list_sqn }}" data-type="gr"
                            data-delete-routelist="{{ route('deliverynotes.data.destroylist') }}">
                            <i class="fa fa-times"></i>
                        </button>
                    </td>

                </tr>
                @endforeach
            </tbody>
            @endif
        </table>
    </td>
</tr>
@endforeach
@endif