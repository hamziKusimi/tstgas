
<tr class="narrow-padding toggle-details main-table-row-template2 d-none" data-row-id2="0">
    <td class="details-control toggle-inner-table-button">
    </td>
    <td>
        {!! Form::hidden('gritem_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('grsequence_no[]', null, ['class' => 'form-control grsequence_no exist-val-gr']) !!}
    </td>
    <td>
        {!! Form::select('grtype[]', [], null, [
            'class' => 'form-control grtype exist-val-gr-select',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-gstype' => $gasracktype,
        ]) !!}
        {!! Form::hidden('grtypedt[]', null, ['class' => 'form-control grtypedt']) !!}      
    </td>
    <td>
        {!! Form::number('grquantity[]', null, ['class' => 'form-control validate-barcode grquantity', 'min' => '1', 'step' => '0.01']) !!}
    </td>
    <td>
        {!! Form::text('grdaily_price[]', null, ['class' => 'form-control grdaily_price']) !!}
    </td>
    <td>
        {!! Form::text('grmonthly_price[]', null, ['class' => 'form-control grmonthly_price']) !!}
    </td>
    <td>
        <div class="input-group" id="date-picker4" data-target-input="nearest">
            {!! Form::text('grbill_date[]', null, [
            'class' => 'form-control grbill_date datetimepicker-input',
            'required',
            'placeholder' => '',
            'data-target' => '#date-picker4'
            ]) !!}
            {{-- <div class="input-group-append" data-target="#date-picker4" data-toggle="datetimepicker">
                <div class="input-group-text bg-dark-green text-white">
                    <small><i class="fa fa-calendar"></i></small>
                </div>
            </div> --}}
        </div>
        {{-- {!! Form::text('grbill_date[]', null, ['class' => 'form-control grbill_date exist-val-gr']) !!} --}}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id2="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="inner-table second-row-template2 d-none" data-row-id2="0">
    <td colspan="8">
        <table class="table table-sm narrow-padding gasrack-table">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">Prepare</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="gasrack"></tbody>
        </table>
    </td>
</tr>
