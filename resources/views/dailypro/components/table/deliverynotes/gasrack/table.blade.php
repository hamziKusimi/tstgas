<div class="tax-container"></div>
<div class="table-responsive">
    <div class="box">
        <table class="table table-sm" id="gasrackdt">
            <thead>
                <tr class="bg-dark-blue text-white text-sm">
                    <th width="4.5%"></th>
                    <th width="12%" class="text-center">No</th>
                    <th width="14%" class="text-center">Type</th>
                    <th width="14%" class="text-center">Qty</th>
                    <th width="12%" class="text-center">Daily Rate</th>
                    <th width="12%" class="text-center">Monthly Rate</th>
                    <th width="12%" class="text-center">Bill Date</th>
                    <th width="1%"></th>
                </tr>
            </thead>
            <tbody id="table-form-tbody-gasrack" class="gasrack">
                @include('dailypro.components.table.deliverynotes.gasrack.form')
                @includeWhen(isset($grcontents), 'dailypro.components.table.deliverynotes.gasrack.row')
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="1">
                        <button type="button" class="form-control btn bg-dark-green text-white" style="width:70px"
                            id="add-new2">
                            Add
                        </button>
                    </td>
                    <td colspan="8"></td>
                </tr>
            </tfoot>
        </table>
        <div class="overlay delete-overlay2">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
</div>