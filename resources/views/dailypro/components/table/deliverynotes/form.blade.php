
<tr class="narrow-padding toggle-details main-table-row-template d-none" data-row-id="0">
    <td class="details-control toggle-inner-table-button">
    </td>
    <td>
        {!! Form::hidden('item_id[]', null, ['class' => 'item-id']) !!}
        {!! Form::text('sequence_no[]', null, ['class' => 'form-control sequence_no exist-val-cy']) !!}
    </td>
    <td>
        {!! Form::select('category[]', [], null, [
            'class' => 'form-control category exist-val-cy-select',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-cat' => $cylindercat,
        ]) !!}
        {!! Form::hidden('categorydt[]', null, ['class' => 'form-control categorydt']) !!}
    </td>
    <td>
        {!! Form::select('product[]', [], null, [
            'class' => 'form-control product exist-val-cy-select',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-prod' => $cylinderprod,
        ]) !!}
        {!! Form::hidden('productdt[]', null, ['class' => 'form-control productdt']) !!}
    </td>
    <td>
        {!! Form::select('capacity[]', [], null, [
            'class' => 'form-control capacity exist-val-cy-select',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-cap' => $capacities,
        ]) !!}
        {!! Form::hidden('capacitydt[]', null, ['class' => 'form-control capacitydt']) !!}
    </td>
    <td>
        {!! Form::select('pressure[]', [], null, [
            'class' => 'form-control pressure exist-val-cy-select',
            'placeholder' => '',
            'style' => 'width:100%; height:100%',
            'data-press' => $pressures,
        ]) !!}
        {!! Form::hidden('pressuredt[]', null, ['class' => 'form-control pressuredt']) !!}
    </td>
    <td>
        {!! Form::number('quantity[]', null, ['class' => 'form-control validate-barcode quantity exist-val-c', 'min' => '1', 'step' => '1']) !!}
    </td>
    <td>
        {!! Form::text('gas_price[]', null, ['class' => 'form-control gas_price exist-val-c']) !!}
    </td>
    <td>
        {!! Form::text('daily_price[]', null, ['class' => 'form-control daily_price exist-val-c']) !!}
    </td>

    <td>
        {!! Form::text('monthly_price[]', null, ['class' => 'form-control monthly_price exist-val-c']) !!}
    </td>
    <td>
        <div class="input-group" id="date-picker1" data-target-input="nearest">
            {!! Form::text('bill_date[]', null, [
            'class' => 'form-control bill_date datetimepicker-input',
            'required',
            'placeholder' => '',
            'data-target' => '#date-picker1'
            ]) !!}
            {{-- <div class="input-group-append" data-target="#date-picker1" data-toggle="datetimepicker">
                <div class="input-group-text bg-dark-green text-white">
                    <small><i class="fa fa-calendar"></i></small>
                </div>
            </div> --}}
        </div>
        {{-- {!! Form::text('bill_date[]', null, ['class' => 'form-control bill_date exist-val-gr']) !!} --}}
    </td>
    <td>
        <button type="button" class="btn btn-primary add-remark" data-toggle="modal" data-target="#myModal" data-remark="0">
            <i class="fa fa-plus"></i>
        </button>
        {!! Form::hidden('remarkdt[]', null, ['class' => 'form-control remarkdt']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-danger remove-row" data-id="0">
            <i class="fa fa-times"></i>
        </button>
    </td>
</tr>
<tr class="inner-table second-row-template d-none" data-row-id="0">
    <td colspan="12">
        <table class="table table-sm narrow-padding cylinder-table">
            <thead>
                <tr class="text-sm">
                    <th class="text-center">Barcode</th>
                    <th class="text-center">Serial</th>
                    <th class="text-center">Driver</th>
                    <th class="text-center">Prepare</th>
                    <th class="text-center">Loading</th>
                    <th class="text-center">Unloading</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </td>
</tr>
