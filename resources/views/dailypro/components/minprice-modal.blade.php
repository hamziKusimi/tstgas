<div class="modal minprice-modal" tabindex="-1" role="dialog" id="minprice-modal" data-authroute="{{ route('auth.minprice.post') }}">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Below Minimum Price</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          {!! Form::open(['url' => '', 'method' => 'POST', 'class' => 'account-modal-form']) !!}
          
            {{-- {!! Form::text('usercode', null, ['class' => 'form-control form-data', 'id' =>
            'usercode', 'placeholder' => 'Usercode']) !!}
            {!! Form::text('password', null, ['class' => 'form-control form-data', 'id' =>
            'password', 'placeholder' => 'Password']) !!} --}}
            <input type="usercode" class="form-control" name="usercode" class="form-control" id="usercode" placeholder="usercode">
            <input type="password" class="form-control" name="password" class="form-control" id="password" placeholder="password">
            <button type="submit" class="btn btn-primary form-control submit">Submit</button>
          {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button id="close-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>