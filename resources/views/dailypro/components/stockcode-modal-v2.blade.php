<div class="modal stockcode-modal" tabindex="-1" role="dialog" id="stockcode-modal" data-get-route="{{route('stockcode.api.get')}}">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Stock</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
                <button type="button" data-toggle="collapse" data-target="#authorization" class="btn btn-default bg-dark-blue font-white">More Details</button>
                <div class="tab-content">
                  <div class="collapse padding-systemsetup" id="authorization">
                  <div class="row">
                    <div class="col-md-4">  
                        <div class="input-group row narrow-padding-global"> 
                            <label for="" class="col-md-6">Type</label>   
                            <div class="col-md-6">
                              <input name="type" type="text" maxlength="100" class="form-control type" readonly>
                            </div>
                        </div>
                
                      
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">U.O.M. (1)</label>
                            <div class="col-md-6">
                              <input name="uom" type="text" maxlength="100" class="form-control uom" readonly>
                              
                            </div>

                        </div>
                      
                            
                        <div class="input-group row narrow-padding-global">
                            <label for="code" class="col-md-6"> Unit Cost </label>
                            <div class="col-md-6">
                              <input name="unit_cost" type="text" maxlength="100" class="form-control unit_cost" readonly>
                            </div>
                        </div>

                        <div class="input-group row narrow-padding-global">
                          <label for="" class="col-md-6">Prev Cost</label>
                          <div class="col-md-6">
                            <input name="prev_cost" type="text" maxlength="100" class="form-control prev_cost" readonly>
                          
                          </div>
                        </div>

                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">Ave Cost</label>
                            <div class="col-md-6">
                              <input name="ave_cost" type="text" maxlength="100" class="form-control ave_cost" readonly>
                            
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">Weight</label>
                            <div class="col-md-6">
                              <input name="weight" type="text" maxlength="100" class="form-control weight" readonly>
                            </div>
                        </div>
                
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">Min Stk Qty</label>
                            <div class="col-md-6">
                              <input name="minstk" type="text" maxlength="100" class="form-control minstk" readonly>
                            </div>
                        </div>
                        
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">Max Stk Qty</label>
                            <div class="col-md-6">
                              <input name="maxstk" type="text" maxlength="100" class="form-control maxstk" readonly>

                            </div>
                        </div>                          
                        <div class="input-group row narrow-padding-global">                            
                          <label for="" class="col-md-6">Stock Bal</label>
                          <div class="col-md-6">
                              <input name="stockbal" type="text" maxlength="100" class="form-control stockbal" readonly>
                          </div> 
                        </div>

                        <div class="input-group row narrow-padding-global">                          
                          <label for="" class="col-md-6">Last Stck Check</label>
                          <div class="col-md-6">
                            <input name="last_stk" type="text" maxlength="100" class="form-control last_stk" readonly>
                          
                          </div>
                        </div>

                    </div>                    
                    <div class="col-md-4">        
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">Min Price</label>
                            <div class="col-md-6">
                                <input name="minprice" type="text" maxlength="100" class="form-control minprice" readonly>
                            </div>
                            
                        </div>
                        @if(($systemsetup->price1))
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">{{ $systemsetup->price1 }}</label>
                            <div class="col-md-6">
                              <input name="price1" type="text" maxlength="100" class="form-control price1" readonly>
                            </div>
                            
                        </div>
                        @endif
                        @if(($systemsetup->price2))
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">{{ $systemsetup->price2 }}</label>
                            <div class="col-md-6">
                                <input name="price2" type="text" maxlength="100" class="form-control price2" readonly>
                            </div>
                            
                        </div>
                        @endif
                        @if(($systemsetup->price3))
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">{{ $systemsetup->price3 }}</label>
                            <div class="col-md-6">
                                <input name="price3" type="text" maxlength="100" class="form-control price3" readonly>
                            </div>
                            
                        </div>
                        @endif
                        @if(($systemsetup->price4))
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">{{ $systemsetup->price4 }}</label>
                            <div class="col-md-6">
                                <input name="price4" type="text" maxlength="100" class="form-control price4" readonly>
                            </div>
                            
                        </div>
                        @endif
                        @if(($systemsetup->price5))
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">{{ $systemsetup->price5 }}</label>
                            <div class="col-md-6">
                                <input name="price5" type="text" maxlength="100" class="form-control price5" readonly>
                            </div>
                            
                        </div>
                        @endif
                        @if(($systemsetup->price6))
                        <div class="input-group row narrow-padding-global">
                            <label for="" class="col-md-6">{{ $systemsetup->price6 }}</label>
                            <div class="col-md-6">
                                <input name="price6" type="text" maxlength="100" class="form-control price6" readonly>
                            </div>                          
                        </div>
                        @endif
                    </div>
                  </div>
                  
                <div class="row">
                  <div class="col-md-8">
                    <div class="input-group row narrow-padding-global">
                      <label for="" class="col-md-3">Remark</label>
                      <div class="col-md-6">
                        <textarea name="remark" rows="2" class="form-control remark" readonly></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
                <br>
                <table class="table table-striped table-bordered" id="stockcode-table">
                    <thead>
                        <tr>
                            <th>Stock Code</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button id="close-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>