<div class="cheque-section">
    <div class="form-group row">
        {!! Form::label('cheque_account', 'Account', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('cheque_account', null, ['class' => 'form-control-plaintext cheque_account']) !!}
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('cheque_no', 'Cheque No.', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('cheque_no', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('cheque_date', 'C. Date', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="input-group col-md-9">
            {!! Form::date('cheque_date', null, [
                'class' => 'form-control cheque-date',
                'placeholder' => '',
            ]) !!}
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('cheque_type', 'C. Type', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="clearfix"></div>
        <div class="col-md-9">
            <div class="form-check form-check-inline col-form-label">
                <input class="form-check-input" type="radio" name="cheque_type" id="cheque_type_L" value="L" checked>
                <label class="form-check-label" for="cheque_type_L">Local Cheque</label>
            </div>
            <div class="form-check form-check-inline col-form-label">
                <input class="form-check-input" type="radio" name="cheque_type" id="cheque_type_O" value="O">
                <label class="form-check-label" for="cheque_type_O">Outstanding Cheque</label>
            </div>
        </div>
    </div>

    {!! Form::hidden('cheque_bank', null, ['class' => 'cheque_bank']) !!}
</div>

<div class="credit-card-section">
    <div class="form-group row">
        {!! Form::label('bank_account', 'Account No.', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('bank_account', null, ['class' => 'form-control-plaintext bank_account']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('bank_charges_account', 'Bank Charges Acc', ['class' => 'col-md-4 col-form-label']) !!}
        <div class="col-md-8">
            {!! Form::text('bank_charges_account', null, ['class' => 'form-control-plaintext bank_account']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('bank_charges', 'Charges Amount', ['class' => 'col-md-4 col-form-label']) !!}
        <div class="col-md-8">
            {!! Form::text('bank_charges', null, [ 'class' => 'form-control bank_charges money' ]) !!}
        </div>
    </div>
    {!! Form::hidden('bank_description', null, ['class' => 'bank_description']) !!}
</div>

<div class="default-section">
    <div class="form-group row">
        {!! Form::label('account', 'Account', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('account', null, ['class' => 'form-control-plaintext account']) !!}
        </div>
    </div>
</div>