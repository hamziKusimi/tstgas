{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')
@include('common.modal.search-qtno')

{{--
    each components have their identifiers:
    - runningNumber
    -
--}}

@php
    if(Request::exists('docno')){
        $docno = Request::get('docno');
    }else{
        $docno = isset($item) ? $item->docno : $runningNumber->nextRunningNoString;
    }
@endphp

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('doc_no', 'Document No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('doc_no', isset($item) ? $item->docno : $docno, [
                        'class' => 'form-control',
                        'data-url' => route('salesorders.searchdocno'),
                        'required',
                        'placeholder' => 'Document No.',
                        'maxlength' => 20,
                        isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                    ]) !!}
                    @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                        {!! Form::hidden('doc_no', $item->doc_no) !!}
                    @endif
                    @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' => 'search-document pointer' ])
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0" id="date-picker" data-target-input="nearest">
                        {!! Form::text('date', null, [
                            'class' => 'form-control invoice-date datetimepicker-input',
                            'required',
                            'placeholder' => '',
                            'data-target' => '#date-picker'
                        ]) !!}
                        <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                            <div class="input-group-text bg-dark-green text-white">
                                <small><i class="fa fa-calendar"></i></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('lpono', 'Lpo No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('lpono', isset($item) ? $item->lpono : null, [
                            'class' => 'form-control',
                            'id' => 'lpono',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('ref1', 'Ref No 1', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('ref1', isset($item) ? $item->ref1 : null, [
                            'class' => 'form-control',
                            'id' => 'ref1',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('cbinvno', 'CB/Inv No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('cbinvno', isset($item) ? $item->cbinvno : null, [
                            'class' => 'form-control',
                            'id' => 'cbinvno',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('quotno', 'Quotation No', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('quotno', isset($item) ? $item->quotno : null, [
                            'class' => 'form-control',
                            'id' => 'quotno',
                            'placeholder' => '',
                        ]) !!}
                          <div class="input-group-append get-qtno" data-toggle="modal" data-target="#qtno-modal">
                            <div class="input-group-text bg-dark-blue text-white pointer">
                                <small>
                                        <i class="fa fa-plus"></i>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
