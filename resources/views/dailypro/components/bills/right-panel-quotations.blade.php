{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')

{{--
    each components have their identifiers:
    - runningNumber
    -
--}}

@php
    if(Request::exists('docno')){
        $docno = Request::get('docno');
    }else{
        $docno = isset($item) ? $item->docno : $runningNumber->nextRunningNoString;
    }
@endphp
<div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('doc_no', 'Document No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('doc_no', isset($item) ? $item->docno : $docno, [
                            'class' => 'form-control',
                            'data-url' => route('quotations.searchdocno'),
                            'required',
                            'placeholder' => 'Document No.',
                            'maxlength' => 20,
                            isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                        ]) !!}
                        @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                            {!! Form::hidden('doc_no', $item->docno) !!}
                        @endif
                        @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' => 'search-document pointer' ])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0" id="date-picker" data-target-input="nearest">
                        {!! Form::text('date', null, [
                            'class' => 'form-control invoice-date datetimepicker-input',
                            'required',
                            'placeholder' => '',
                            'data-target' => '#date-picker'
                        ]) !!}
                        <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                            <div class="input-group-text bg-dark-green text-white">
                                <small><i class="fa fa-calendar"></i></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('attention', 'Attention', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('attention', isset($item) ? $item->attention : null, [
                            'class' => 'form-control',
                            'id' => 'attention',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('contact', 'Contact', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('contact', isset($item) ? $item->contact : null, [
                            'class' => 'form-control',
                            'id' => 'contact',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('cbinvdono', 'CB/Inv/DO No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('cbinvdono', isset($item) ? $item->cbinvdono : null, [
                            'class' => 'form-control',
                            'id' => 'cbinvdono',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="narrow-padding-global">
        <div class="row">
            {!! Form::label('quotno', 'Quotation No', ['class' => 'col-form-label col-md-3 text-right']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="input-group col-md-11 pr-0">
                        {!! Form::text('quotno', isset($item) ? $item->quot : null, [
                            'class' => 'form-control',
                            'id' => 'quotno',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
