{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')
@include('common.modal.search-dono')

{{--
    each components have their identifiers:
    - runningNumber
    -
--}}

@php
    if(Request::exists('docno')){
        $docno = Request::get('docno');
    }else{
        $docno = isset($item) ? $item->docno : $runningNumber->nextRunningNoString;
    }
@endphp

@if(isset($except) && !in_array('runningNumber', $except))
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('doc_no', 'Document No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('doc_no', isset($item) ? $item->docno : $docno, [
                        'class' => 'form-control',
                        'data-url' => route('invoices.searchdocno'),
                        'required',
                        'placeholder' => 'Document No.',
                        'maxlength' => 20,
                        isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                    ]) !!}
                    @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                        {!! Form::hidden('doc_no', $item->doc_no) !!}
                    @endif
                    @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' => 'search-document pointer' ])
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker" data-target-input="nearest">
                    {!! Form::text('date',  isset($item) ? $item->date: null, [
                        'class' => 'form-control invoice-date datetimepicker-input',
                        'required',
                        'placeholder' => '',
                        'data-target' => '#date-picker'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global refInvDateContainer">
    <div class="row">
        {!! Form::label('reference_date', 'Ref. Inv Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="reference_date-picker" data-target-input="nearest">
                    {!! Form::text('reference_date', isset($item) ? $item->refinvdate: null, [
                        'class' => 'form-control reference-date datetimepicker-input',
                        'placeholder' => '',
                        'data-target' => '#reference_date-picker',
                        'required',
                    ]) !!}
                    <div class="input-group-append" data-target="#reference_date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('ref_no', 'Reference No', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('ref_no', isset($item) ? $item->refno : null, [
                        'class' => 'form-control',
                        'id' => 'ref_no',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag' ])
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('do_no', 'Do No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('do_no', isset($item) ? $item->do_no : '', [
                        'class' => 'form-control dono',
                        'id' => 'dono',
                        'placeholder' => '',
                    ]) !!}
                    <div class="input-group-append get-dono" data-toggle="modal" data-target="#dono-modal">
                        <div class="input-group-text bg-dark-blue text-white pointer">
                            <small>
                                    <i class="fa fa-plus"></i>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('credit_term', 'Credit Term', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('credit_term', isset($item) ? $item->credit_term : null, [
                        'class' => 'form-control',
                        'id' => 'credit_term',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-calendar-o' ])
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('credit_limit', 'Credit Limit', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('credit_limit', isset($item) ? $item->credit_limit : null, [
                        'class' => 'form-control',
                        'id' => 'credit_limit',
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global dueDateContainer">
    <div class="row">
        {!! Form::label('due_date', 'Due Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="due_date-picker" data-target-input="nearest">
                    {!! Form::text('due_date', isset($item) ? $item->m_duedate:null, [
                        'class' => 'form-control due-date datetimepicker-input',
                        'required',
                        'placeholder' => '',
                        'data-target' => '#due_date-picker'
                    ]) !!}
                    <div class="input-group-append" data-target="#due_date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('currency', 'Currency', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3 pr-0">
                    {!! Form::text('currency', isset($item) ? $item->currency : '', [
                        'class' => 'form-control',
                        'id' => 'currency',
                        'placeholder' => '',
                    ]) !!}
                </div>
                <div class="input-group col-md-8 pl-0 pr-0">
                    {!! Form::number('exchange_rate', isset($item) ? $item->exchange_rate : 1.00, [
                        'class' => 'form-control',
                        'id' => 'exchange_rate',
                        'step' => '0.0001',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-money' ])
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('amount_mt', 'Amount', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::text('amount_mt', isset($item) ? $item->amount : null, ['class' => 'form-control amount_mt']) !!}
        </div>
        {!! Form::label('remaining_amount_mt', 'Remaining', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::text('remaining_amount_mt', 0.00, ['class' => 'form-control remaining_amount_mt text-danger']) !!}
        </div>
    </div>
</div> --}}

<div class="narrow-padding-global">
    <div class="row">
        {{-- {!! Form::label('salesman', 'salesman', ['class' => 'col-form-label col-md-3 text-right']) !!} --}}
        <label for="salesman" class="col-form-label col-md-3 text-right">Salesman</label>
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::select('salesman', $salesmans, isset($item) ? $item->salesman: null, [
                    'class' => 'form-control myselect salesman',
                    'id' => 'salesman',
                    'placeholder' => ''
                    ]) !!}
                    {{-- {!! Form::text('salesman', isset($item) ? $item->salesman : null, [
                            'class' => 'form-control salesman',
                            'id' => 'salesman',
                            'placeholder' => '',
                        ]) !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>