<div class="mt-3"></div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('cash_purchase', 'Cash Purchase', ['class' => 'col-form-label col-md-4']) !!}
        <div class="col-md-8">
            <div class="row">
                {!! Form::select('cash_purchase', $masterCodes->pluck('d_detail', 'd_acode'), isset($item) ? $item->cash_account: null, [
                    'class' => 'form-control select2 cash_purchase',
                    'placeholder' => '',
                ]) !!}
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('self_billed', 'Self Billed', ['class' => 'col-form-label col-md-4']) !!}
        <div class="col-md-2">
            <div class="row">
                @include('common.form.toggle-check-input-single', [
                    'checked' => isset($item) ? $item->self_billed : false,
                    'id' => 'self_billed',
                    'name' => 'self_billed',
                    'value' => 'self_billed'
                ])
            </div>
        </div>
    </div>
</div>