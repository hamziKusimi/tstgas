<div class="form-group">
    <div class="row">
        <div class="col">
            {!! Form::label('header', 'Header', ['class' => 'col-form-label']) !!}
            <small id="header_template_option">
                <a href="#" id="header_button" class="d-none" data-toggle="modal" data-target="#template-modal" data-textarea-target="#header">Get template</a>
            </small>
        </div>
        <div class="col">
            <div class="float-right">
                <a href="#" type="button" name="button" class="btn btn-sm bg-dark-blue text-white" data-toggle="collapse" data-target="#header_div">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="collapse" id="header_div">
        {!! Form::textarea('header', isset($item) ? $item->header : "", [
            'class' => 'form-control summernote',
            'rows' => '3',
            'id' => 'header',
        ]) !!}
    </div>
</div>

    <div class="form-group">
    <div class="row">
        <div class="col">
            {!! Form::label('footer', 'Footer', ['class' => 'col-form-label']) !!}
            <small id="footer_template_option">
                <a href="#" id="footer_button" class="d-none" data-toggle="modal" data-target="#template-modal" data-textarea-target="#footer">Get template</a>
            </small>
        </div>
        <div class="col">
            <div class="float-right">
                <a href="#" type="button" name="button" class="btn btn-sm bg-dark-blue text-white" data-toggle="collapse" data-target="#footer_div">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="collapse" id="footer_div">
        {!! Form::textarea('footer', isset($item) ? $item->footer : "", [
            'class' => 'form-control summernote',
            'rows' => '2',
            'id' => 'footer',
        ]) !!}
    </div>
</div>

    <div class="form-group">
    <div class="row">
        <div class="col">
            {!! Form::label('summary', 'Summary', ['class' => 'col-form-label']) !!}
            <small id="summary_template_option">
                <a href="#" id="summary_button" class="d-none" data-toggle="modal" data-target="#template-modal" data-textarea-target="#summary">Get template</a>
            </small>
        </div>
        <div class="col">
            <div class="float-right">
                <a href="#" type="button" name="button" class="btn btn-sm bg-dark-blue text-white" data-toggle="collapse" data-target="#summary_div">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="collapse" id="summary_div">
        {!! Form::textarea('summary', isset($item) ? $item->summary : "", [
            'class' => 'form-control summernote',
            'rows' => '2',
            'id' => 'summary',
        ]) !!}
    </div>
</div>

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
@include('common.form.summernote-sm')
<script type="text/javascript">
(function() {
    $('#header_div').on('hidden.bs.collapse', function () { $('#header_button').addClass('d-none') })
    $('#footer_div').on('hidden.bs.collapse', function () { $('#footer_button').addClass('d-none') })
    $('#summary_div').on('hidden.bs.collapse', function () { $('#summary_button').addClass('d-none') })
        $('#header_div').on('shown.bs.collapse', function () { $('#header_button').removeClass('d-none') })
    $('#footer_div').on('shown.bs.collapse', function () { $('#footer_button').removeClass('d-none') })
    $('#summary_div').on('shown.bs.collapse', function () { $('#summary_button').removeClass('d-none') })
        $('#add-header-footer').on('click', function() {
        $('#add-on').hasClass('d-none') ? $('#add-on').removeClass('d-none') : $('#add-on').addClass('d-none');
    })
        $('#header').summernote({ placeholder: 'Header', height: 100, followingToolbar: false, toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
    })
        $('#footer').summernote({ placeholder: 'Footer', height: 100, followingToolbar: false, toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
    })
        $('#summary').summernote({ placeholder: 'Summary', height: 100, followingToolbar: false, toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
    })
    }) ()
</script>
@endpush