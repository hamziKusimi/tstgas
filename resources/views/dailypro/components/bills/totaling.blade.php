<div class="row">
    {!! Form::label('subtotal', 'Subtotal', ['class' => 'col-md-6 col-form-label']) !!}
    <div class="col-md-6">
        {!! Form::text('subtotal', isset($item) ? $item->amount : null, ['class' => 'form-control-plaintext form-control-lg pr-2 money font-weight-bold', 'id' => 'subtotal', 'readonly']) !!}
    </div>
</div>

<div class="row {{ (isset($systemsetup) && $systemsetup->use_tax)? '': 'd-none'  }}">
    {!! Form::label('tax_mt', 'Tax', ['class' => 'col-md-6 col-form-label']) !!}
    <div class="col-md-6">
        {{-- {{ dd($item) }} --}}
        {!! Form::text('tax', isset($item) ? $item->tax_amount : null, ['class' => 'form-control-plaintext form-control-lg pr-2 money font-weight-bold', 'id' => 'tax', 'readonly']) !!}
    </div>
</div>

    <div class="row">
    {!! Form::label('discount_mt', 'Discount', ['class' => 'col-md-9 col-form-label']) !!}
    <div class="col-md-3">
        {!! Form::text('discount_mt', isset($item) ? $item->discount : 0.00, ['class' => 'form-control form-control-lg number-right-align discount_mt']) !!}
    </div>
</div>

{{-- <div class="row {{ $roundingIsEnabled ? '' : 'd-none' }}" id="rounding-container">
    {!! Form::label('rounding', 'Rounding', ['class' => 'col-md-6 col-form-label']) !!}
    <div class="col-md-6">
        {!! Form::text('rounding', isset($item) ? $item->rounding : null, ['class' => 'form-control-plaintext form-control-lg pr-2 money font-weight-bold', 'id' => 'rounding', 'readonly']) !!}
    </div>
</div> --}}

    <hr/>
<div class="row">
    {!! Form::label('grand_total', 'Grand Total', ['class' => 'col-md-6 col-form-label col-form-label-lg', 'style' => 'font-size:1.55rem;']) !!}
    <div class="col-md-6">
        {!! Form::text('grand_total', isset($item) ? $item->taxed_amount : null, [
            'class' => 'form-control-plaintext pr-2 form-control-lg money font-weight-bold h1',
            'id' => 'grand_total',
            'style' => 'font-size:1.55rem;',
            'readonly',
        ]) !!}
    </div>
</div>