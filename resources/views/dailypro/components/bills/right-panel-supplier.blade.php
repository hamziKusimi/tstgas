<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('supp_inv_no', 'SI No. *', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('supp_inv_no', isset($item) ? $item->m_spinvno : null, [
                        'class' => 'form-control',
                        'id' => 'supp_inv_no',
                        'placeholder' => '',
                        'required',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag' ])
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('supp_inv_date', 'SI Date *', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="supp_inv-date-picker" data-target-input="nearest">
                    {!! Form::text('supp_inv_date', null, [
                        'class' => 'form-control supp_inv-date datetimepicker-input',
                        'required',
                        'placeholder' => '',
                        'data-target' => '#supp_inv-date-picker'
                    ]) !!}
                    <div class="input-group-append" data-target="#supp_inv-date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>