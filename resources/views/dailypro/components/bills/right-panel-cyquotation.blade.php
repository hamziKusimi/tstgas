{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')

{{--
    each components have their identifiers:
    - runningNumber
    -
--}}
@if(isset($except) && !in_array('runningNumber', $except))
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('doc_no', 'Document No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('doc_no', isset($item) ? $item->docno : $runningNumber->nextRunningNoString, [
                        'class' => 'form-control',
                        'required',
                        'placeholder' => 'Document No.',
                        'maxlength' => 20,
                        isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                    ]) !!}
                    @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                        {!! Form::hidden('doc_no', $item->doc_no) !!}
                    @endif
                    @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' => 'search-document pointer' ])
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker" data-target-input="nearest">
                    {!! Form::text('date', null, [
                        'class' => 'form-control invoice-date datetimepicker-input',
                        'required',
                        'placeholder' => '',
                        'data-target' => '#date-picker',
                        'autocomplete' => 'off'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global refInvDateContainer">
    <div class="row">
        {!! Form::label('reference_date', 'Ref. Inv Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="reference_date-picker" data-target-input="nearest">
                    {!! Form::text('reference_date', null, [
                        'class' => 'form-control reference-date datetimepicker-input',
                        'placeholder' => '',
                        'data-target' => '#reference_date-picker',
                        'autocomplete' => 'off',
                        'required',
                    ]) !!}
                    <div class="input-group-append" data-target="#reference_date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('reference_no', 'Reference No', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('reference_no', isset($item) ? $item->refno : null, [
                        'class' => 'form-control',
                        'id' => 'reference_no',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag' ])
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('do_no', 'Do No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('do_no', isset($item) ? $item->do_no : '', [
                        'class' => 'form-control',
                        'id' => 'do_no',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag' ])
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('credit_term', 'Credit Term', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('credit_term', isset($item) ? $item->credit_term : null, [
                        'class' => 'form-control',
                        'id' => 'credit_term',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-calendar-o' ])
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('credit_limit', 'Credit Limit', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('credit_limit', isset($item) ? $item->credit_limit : null, [
                        'class' => 'form-control',
                        'id' => 'credit_limit',
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global dueDateContainer">
    <div class="row">
        {!! Form::label('due_date', 'Due Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="due_date-picker" data-target-input="nearest">
                    {!! Form::text('due_date', null, [
                        'class' => 'form-control due-date datetimepicker-input',
                        'required',
                        'placeholder' => '',
                        'data-target' => '#due_date-picker'
                    ]) !!}
                    <div class="input-group-append" data-target="#due_date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
{{-- <h5>Customer Setup Choose</h5>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('cust', 'Customer', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('cust', isset($item) ? $item->cust : null, [
                        'class' => 'form-control',
                        'id' => 'cust',
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
            {!! Form::label('Date ', 'Date :', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0 date">
                    {!! Form::text('dates', null, ['class' => 'form-control form-data',
                    'autocomplete'=>'off', 'id' =>'invdate'
                    ])!!}
                </div>
            </div>
        </div>
    </div>
</div> --}}
{{-- <div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('amount_mt', 'Amount', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::text('amount_mt', isset($item) ? $item->amount : null, ['class' => 'form-control amount_mt']) !!}
        </div>
        {!! Form::label('remaining_amount_mt', 'Remaining', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::text('remaining_amount_mt', 0.00, ['class' => 'form-control remaining_amount_mt text-danger']) !!}
        </div>
    </div>
</div> --}}