{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')

{{--
    each components have their identifiers:
    - runningNumber
    -
--}}
@php
    if(Request::exists('docno')){
        $docno = Request::get('docno');
    }else{
        $docno = isset($item) ? $item->docno : $runningNumber->nextRunningNoString;
    }
@endphp
@if(isset($except) && !in_array('runningNumber', $except))
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('doc_no', 'Document No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('doc_no', isset($item) ? $item->docno : $docno, [
                        'class' => 'form-control',
                        'data-url' => route('salesreturns.searchdocno'),
                        'required',
                        'placeholder' => 'Document No.',
                        'maxlength' => 20,
                        isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                    ]) !!}
                    @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                        {!! Form::hidden('doc_no', $item->docno) !!}
                    @endif
                    @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' => 'search-document pointer' ])
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(isset($except) && !in_array('date', $except))
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker" data-target-input="nearest">
                    {!! Form::text('date', null, [
                        'class' => 'form-control invoice-date datetimepicker-input',
                        'required',
                        'placeholder' => '',
                        'data-target' => '#date-picker'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('ref1', 'Ref No. 1', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="ref1">
                    {!! Form::text('ref1', isset($item) ? $item->ref1 : null, [
                        'class' => 'form-control',
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('ref2', 'Ref No. 2', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="ref2">
                    {!! Form::text('ref2', isset($item) ? $item->ref2 : null, [
                        'class' => 'form-control',
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {{-- {!! Form::label('salesman', 'salesman', ['class' => 'col-form-label col-md-3 text-right']) !!} --}}
        <label for="salesman" class="col-form-label col-md-3 text-right">Salesman</label>
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::select('salesman', $salesmans, isset($item) ? $item->salesman: null, [
                    'class' => 'form-control myselect salesman',
                    'id' => 'salesman',
                    'placeholder' => ''
                    ]) !!}
                    {{-- {!! Form::text('salesman', isset($item) ? $item->salesman : null, [
                            'class' => 'form-control salesman',
                            'id' => 'salesman',
                            'placeholder' => '',
                        ]) !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>
