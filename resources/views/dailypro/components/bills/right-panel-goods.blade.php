{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')
@include('common.modal.search-pono')

{{--
    each components have their identifiers:
    - runningNumber
    -
--}}
@php
    if(Request::exists('docno')){
        $docno = Request::get('docno');
    }else{
        $docno = isset($item) ? $item->docno : $runningNumber->nextRunningNoString;
    }
@endphp

@if(isset($except) && !in_array('runningNumber', $except))
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('doc_no', 'Document No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('doc_no', isset($item) ? $item->docno : $docno, [
                        'class' => 'form-control',
                        'data-url' => route('goods.searchdocno'),
                        'required',
                        'placeholder' => 'Document No.',
                        'maxlength' => 20,
                        isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                    ]) !!}
                    @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                        {!! Form::hidden('doc_no', $item->docno) !!}
                    @endif
                    @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' => 'search-document pointer' ])
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(isset($except) && !in_array('date', $except))
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker" data-target-input="nearest">
                    {!! Form::text('date', isset($item)?$item->date: null, [
                        'class' => 'form-control invoice-date datetimepicker-input',
                        'required',
                        'placeholder' => '',
                        'data-target' => '#date-picker'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('suppdo', 'Supplier D.O', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="suppdo">
                    {!! Form::text('suppdo', isset($item) ? $item->suppdo : null, [
                        'class' => 'form-control',
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('suppinv', 'Supplier Inv.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="suppinv">
                    {!! Form::text('suppinv', isset($item) ? $item->suppinv : null, [
                        'class' => 'form-control',
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global dueDateContainer">
    <div class="row">
        {!! Form::label('suppinvdate', 'Supp Inv Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="suppinvdate-picker" data-target-input="nearest">
                    {!! Form::text('suppinvdate', isset($item) ? $item->suppinvdate : null, [
                        'class' => 'form-control datetimepicker-input',
                        'placeholder' => '',
                        'data-target' => '#suppinvdate-picker'
                    ]) !!}
                     <div class="input-group-append" data-target="#suppinvdate-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('ref', 'Reference No', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('ref', isset($item) ? $item->ref : null, [
                        'class' => 'form-control',
                        'id' => 'ref',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag'])
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('ptype', 'Purchase Type', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {{-- {!! Form::text('ptype', isset($item) ? $item->ptype : null, [
                        'class' => 'form-control',
                        'id' => 'ptype',
                        'placeholder' => '',
                    ]) !!} --}}

                    {!! Form::select('ptype', array(
                        'Cash'=>'Cash',
                        'Credit'=>'Credit',
                        ),  isset($item) ? $item->ptype : null, [
                        'class' => 'form-control myselect',
                        'id' => 'ptype',
                        'placeholder' => '',
                    ]) !!}
                    {{-- @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag' ]) --}}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('pono', 'PO No', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('pono', isset($item) ? $item->pono : null, [
                        'class' => 'form-control pono',
                        'id' => 'pono',
                        'placeholder' => '',
                    ]) !!}
                    <div class="input-group-append get-pono" data-toggle="modal" data-target="#pono-modal">
                        <div class="input-group-text bg-dark-blue text-white pointer">
                            <small>
                                    <i class="fa fa-plus"></i>
                            </small>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('currency', 'Currency', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('currency', isset($item) ? $item->currency : null, [
                        'class' => 'form-control',
                        'id' => 'currency',
                        'placeholder' => '',
                    ]) !!}
                    @include('common.form.fa-append', [ 'append' => 'fa fa-money' ])
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($except) && !in_array('amount', $except))
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('amount_mt', 'Amount', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::text('amount_mt', isset($item) ? $item->amount : null, ['class' => 'form-control amount_mt']) !!}
        </div>
        {!! Form::label('remaining_amount_mt', 'Remaining', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::text('remaining_amount_mt', 0.00, ['class' => 'form-control remaining_amount_mt text-danger']) !!}
        </div>
    </div>
</div>
@endif