{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-debtor')
{{-- ni dia --}}

<div class="row">
    <div class="col-md-12 narrow-padding-global">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text pointer bg-dark-blue text-white create-new-account-option">
                    <small>
                        <i class="fa fa-file"></i>
                    </small>
                </div>
            </div>
            {!! Form::select('account_code_mt', $masterCodes->pluck('full_detail', 'account_code'), isset($item) ? $item->account_code: null, [
                'class' => 'form-control custom-select2 acc_code',
                'placeholder' => '',
                'required',
                'id' => 'debtor-select',
                isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                // 'data-route' => route('api.get.bills-for-account', 'ACCOUNT'),
                'data-master' => $masterCodes,
                'data-acodes' => $masterCodes->pluck('account_code')
            ]) !!}

            @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                {!! Form::hidden('account_code_mt', $item->account_code) !!}
            @endif
        </div>
    </div>
    <div class="col-md-12 narrow-padding-global">
        <div class="input-group">
            @include('common.form.fa-prepend', [ 'prepend' => 'fa fa-search', 'additionalClass' => 'search-debtor pointer' ])
            {!! Form::text('debtor_name', isset($item) ? $item->name : null, [
                'class' => 'form-control',
                'id' => 'debtor_name',
                'placeholder' => 'Name',
                isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
            ]) !!}
            @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                {!! Form::hidden('debtor_name', $item->name) !!}
            @endif
        </div>
    </div>
    <div class="col-md-12 narrow-padding-global">
        <small class="form-text">
            Change billing address for this invoice
            {{-- <a href="#" id="edit-address-button">
            </a> --}}
        </small>
        {!! Form::textarea('detail', isset($item) ? $item->address : null, ['class' => 'form-control', 'id' => 'detail', 'rows' => 4]) !!}
    </div>
    <div class="col-md-6 narrow-padding-global create-telno">
        <div class="input-group">
            @include('common.form.fa-prepend', [ 'prepend' => 'fa fa-phone' ])
            {!! Form::text('tel_no', isset($item) ? $item->tel_no : null, ['class' => 'form-control bg-white', 'id' => 'tel_no']) !!}
        </div>
    </div>
    <div class="col-md-6 narrow-padding-global">
        <div class="input-group">
            @include('common.form.fa-prepend', [ 'prepend' => 'fa fa-fax' ])
            {!! Form::text('fax_no', isset($item) ? $item->fax_no : null, ['class' => 'form-control bg-white', 'id' => 'fax_no']) !!}
        </div>
    </div>
</div>