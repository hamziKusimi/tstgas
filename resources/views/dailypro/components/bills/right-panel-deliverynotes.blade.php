{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')

{{--
    each components have their identifiers:
    - runningNumber
    -
--}}
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('doc_no', 'Document No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('doc_no', isset($item) ? $item->dn_no : $runningNumber->nextRunningNoString, [
                    'class' => 'form-control',
                    'required',
                    'placeholder' => 'Document No.',
                    'maxlength' => 20,
                    isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
                    ]) !!}
                    @if (isset($item) && isset($item->hasOffsets) && $item->hasOffsets)
                    {!! Form::hidden('doc_no', $item->doc_no) !!}
                    @endif
                    @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' =>
                    'search-document pointer' ])
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker" data-target-input="nearest">
                    {!! Form::text('date', null, [
                    'class' => 'form-control invoice-date datetimepicker-input',
                    'required',
                    'placeholder' => '',
                    'data-target' => '#date-picker'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('lpono', 'Lpo No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('lpono', isset($item) ? $item->lpono : null, [
                    'class' => 'form-control',
                    'id' => 'lpono',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {{-- {!! Form::label('driver', 'Driver', ['class' => 'col-form-label col-md-3 text-right']) !!} --}}
        <label for="driver" class="col-form-label col-md-3 text-right">Driver <span class="text-danger">*</span></label>
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::select('driver', $drivers, isset($item) ? $item->driver: null, [
                    'class' => 'form-control myselect driver',
                    'id' => 'driver',
                    'pattern'=>".{6,}",
                    'required' => 'required',
                    'title'=>'driver',
                    'oninvalid'=>"setCustomValidity('Enter Driver')",
                    'oninput'=>"setCustomValidity('')"
                    ]) !!}
                    {{-- {!! Form::text('driver', isset($item) ? $item->driver : null, [
                            'class' => 'form-control driver',
                            'id' => 'driver',
                            'placeholder' => '',
                        ]) !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('vehicleNo', 'Vehicle No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="vehicleNo">
                    {!! Form::text('vehicleNo', isset($item) ? $item->vehicle_no : "", [
                    'class' => 'form-control vehicleNo',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('charge_type', 'Charge Type', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::select('charge_type', ['Daily'=>'Daily',
                    'Weekly'=>'Weekly',
                    'Monthly'=>'Monthly','Quarterly'=>'Quarterly'],isset($item) ? $item->charge_type : 'Daily', [
                    'class' => 'form-control myselect',
                    'id' => 'charge_type',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div> --}}

{{-- <div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('startfrom', 'Start From', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker1" data-target-input="nearest">
                    {!! Form::text('startfrom', isset($item) ? $item->start_from : null, [
                    'class' => 'form-control invoice-date datetimepicker-input',
                    'required',
                    'placeholder' => '',
                    'data-target' => '#date-picker1'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker1" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
{{-- @push('scripts')
<script>
    $(document).ready(function() {
        $('.select').select2();
    });
</script>
@endpush --}}