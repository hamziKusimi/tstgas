@if (isset($onRight) && $onRight)
{{-- if put on right panel --}}
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('rounding_adjustment', 'Use Rounding', ['class' => 'col-form-label col-md-3 rounding_adjustment']) !!}
        <div class="col-md-2">
            <div class="row pl-3">
                @include('common.form.toggle-check-input-single', [
                    'checked' => isset($item) ? $item->rounding : (boolean) get_array_result_value($_companySettings, $roundingOn),
                    'id' => 'rounding-adjustment',
                    'name' => 'rounding_adjustment',
                    'value' => 'Allow Rounding'
                ])
            </div>
        </div>
    </div>
</div>
@else
{{-- default behaviour (on left-panel) --}}
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('rounding_adjustment', 'Use Rounding', ['class' => 'col-form-label col-md-4 rounding_adjustment']) !!}
        <div class="col-md-2">
            <div class="row">
                @include('common.form.toggle-check-input-single', [
                    'checked' => isset($item) ? $item->rounding : (boolean) get_array_result_value($_companySettings, $roundingOn),
                    'id' => 'rounding-adjustment',
                    'name' => 'rounding_adjustment',
                    'value' => 'Allow Rounding'
                ])
            </div>
        </div>
    </div>
</div>
@endif