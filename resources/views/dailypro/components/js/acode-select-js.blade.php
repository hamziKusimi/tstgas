function formatStateInTable (state) { return state.id }

// trigger AJAX response to populate datatable
$('#account_code').on('change', function() {
    let route = $(this).data('route')
    route = route.replace('ACCOUNT', $(this).val())

    // remove previous results, then re-add new ones
    let rows = $('#vouchers-table tbody').find('tr').not('.tr-template')
    rows.each(function () { $(this).remove() })

    $.ajax({
        url: route,
        method: 'GET'
    }).done(function(response) {
        response.forEach(function(record) {
            let row = $('.tr-template').clone().removeClass('tr-template d-none')

            let defaultZeroValue = 0.00
            let billDescription = record.description
            if (!billDescription)
                billDescription = record.doc_no

            // if amount is 0, make the checkbox disabled
            if ($('.amount_mt').val() == 0)
                row.find('.item_check').prop('disabled', true)

            row.find('.existing_payment').val(false)
            row.find('.bill_no').text(record.doc_no)
            row.find('.bill_no').val(record.doc_no)
            row.find('.bill_amount').val(record.amount)
            row.find('.bill_amount').siblings('.displayed-value').text(record.amount)
            row.find('.description').val(billDescription)
            row.find('.paid_amount').val(record.paid_amount)
            row.find('.paid_amount').siblings('.displayed-value').text(record.paid_amount)
            row.find('.outstanding_amount').val(record.remaining_amount)
            row.find('.outstanding_amount').siblings('.displayed-value').text(record.remaining_amount)
            row.find('.applied_amount').val(defaultZeroValue)
            row.find('.applied_amount').siblings('.displayed-value').text(defaultZeroValue)
            row.find('.tax_rate').val(defaultZeroValue)
            row.find('.tax_amount').val(defaultZeroValue)
            row.find('.tax_amount').inputmask('currency', { prefix: '', rightAlign: true })
            row.find('.tax_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth : true })

            // append to table
            $('#vouchers-table').find('tbody').append(row)
        })
    })
})