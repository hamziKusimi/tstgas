// select2 on debtor select
let acode = $('.system-settings-param').data('acode')
let acodeName = $('.system-settings-param').data('acode-name')
let existingAcodes = $('#debtor-select').data('acodes')
let search = ''
var def_price = ''

// for the acode, use a different template to display back results on the select box
function formatState(state) {
    // put the name (only - without the id) values into the next textbox
    let name = state.text.replace(state.id + ' ', '')
    $('#debtor_name').val(name)

    // put the name (only id) value into the select2 dropdown
    return state.id
}

// get the value from select2 search to transfer to the ajax_acode field on the modal
// on press enter to register key and trigger modal
$(document).on('keyup', '.select2-search__field', function (event) {
    search = $(this).val()
    // let keycode = event.which
    // if (keycode === 13)
    //     $('#createNewAccountOption').modal('toggle')

})

$('#debtor-select').select2({
    templateSelection: formatState,
    dropdownAutoWidth: true,
    tags: true,
})

$('.create-new-account-option').on('click', function () {
    // alert('yes')

    
    // console.log($('#createAccountModal').html())
    // clone and start the modal
    let modal = $('#createAccountModal').modal()
    modal.on('hidden.bs.modal', function (e) {
        // clear all inputs and error messages
        modal.find('.acode-input').val('')
        modal.find('.form-data').val('')
        modal.find('.d-acode-message').text('')
    })
    modal.find('#D_ACODE').val(search)

    let route = modal.data('route')
    
    $('.submit').on('click', function () {
        $('.submit').off('click')
        let address = modal.find('#D_ADDRESS').val().split('\n')

        // AJAX call
        let data = {
            'D_DC': 'D',
            'D_ACODE': modal.find('#D_ACODE').val(),
            'D_NAME': modal.find('#D_NAME').val(),
            'D_ADDR1': typeof (address[0]) !== 'undefined' ? address[0] : '',
            'D_ADDR2': typeof (address[1]) !== 'undefined' ? address[1] : '',
            'D_ADDR3': typeof (address[2]) !== 'undefined' ? address[2] : '',
            'D_ADDR4': typeof (address[3]) !== 'undefined' ? address[3] : '',
            'D_TEL': modal.find('#D_PHONE').val(),
            'D_FAX': modal.find('#D_FAX').val(),
            'D_EMAIL': modal.find('#D_EMAIL').val(),
            'D_CTERM': modal.find('#D_CTERM').val(),
            'D_CLIMIT': modal.find('#D_CLIMIT').val(),
            'd_gstno': modal.find('#D_GSTNO').val(),
            'd_brn': modal.find('#D_BRN').val(),
            'D_MEMO': modal.find('#D_MEMO').val(),

        }
        
        $.ajax({
            data: data,
            method: "POST",
            url: route,
        }).done(function (response) {
            modal.modal('hide')
            $('#success-message').find('.alert').text('Successfully added new account')
            $('#success-message').fadeIn(1000).removeClass('d-none').delay(5000).fadeOut(1000)

            // only add to select2 if ajax is successful
            let code = response.accountcode
            let name = response.name
            let entry = code + ' ' + name

            let newAccount = new Option(entry, code, true, true);
            $('#debtor-select').append(newAccount).trigger('change')
            
            $('#detail').val(response.address)
            $('#tel_no').val(response.tel)
            $('#fax_no').val(response.fax)
            $('#credit_term').val(response.cterm)
            $('#credit_limit').val(response.climit)

        }).fail(function (response) {
            modal.modal('hide')

            $('#failure-message').find('.custom-message').text('Failed adding new account')
            $('#failure-message').fadeIn(1000).removeClass('d-none').delay(5000).fadeOut(1000)
        })

        toggleDetails()
    })
})


$('#debtor-select').on('change', function () {
    // detach the click event handler (later it will be attached again)
    $('#edit-address-button').off('click')

    // enable the form submit button
    $('.btn-form-submit').prop('disabled', false)

    let acodes = $(this).data('acodes')
    
    // if this value exist in array, then update the address
    if ($.inArray($(this).val(), acodes) !== -1) {
        let data = $(this).data('master')
        let value = $(this).find(':selected').val()
        let selectedData = data.filter(record => record.account_code == value)[0]
        let address = `${selectedData.addr1} \n${selectedData.addr2} \n${selectedData.addr3} \n${selectedData.addr4}`
        let creditTerm = selectedData.credit_term
        // if (creditTerm === null) 
        //     creditTerm = 0
        let dueDate = moment(date).add(creditTerm, 'days')
        def_price = selectedData.def_price
        var ADD = address.replace(/null/g, '');
        
        // $('#detail').val(null)
        $('#detail').val(ADD.trim())
        $('#tel_no').val(selectedData.tel_no)
        $('#fax_no').val(selectedData.fax_no)
        $('#credit_term').val(selectedData.credit_term)
        $('.def_price').val(selectedData.def_price)
        $('#credit_limit').val(selectedData.credit_limit)
        $('#currency').val(selectedData.currency)
        $('#due_date-picker').datetimepicker('destroy')
        $('#due_date-picker').datetimepicker({ date: dueDate, format: 'DD/MM/YYYY' })

        // toggleDetails()
    }
})

let toggleDetails = function () {
    $('#edit-address-button').on('click', function () {
        $('#detail').hasClass('form-control-plaintext') ?
            $('#detail').removeClass('form-control-plaintext', false).addClass('form-control').prop('readonly', false) :
            $('#detail').removeClass('form-control', true).addClass('form-control-plaintext').prop('readonly', true)

        $('#tel_no').prop('readonly') ? $('#tel_no').prop('readonly', false) : $('#tel_no').prop('readonly', true)
        $('#fax_no').prop('readonly') ? $('#fax_no').prop('readonly', false) : $('#fax_no').prop('readonly', true)
    })
}
