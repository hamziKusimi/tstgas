// calculation
let remainingAmount = $('.system-settings-param').data('remaining-amount')
let amount = parseFloat($('.amount_mt').val() || 0)

/**
 * If the item is NaN, defaults to 0, otherwise, parse to fixed 2 decimal place
 * */
let parsingDecimal = function(item) {
    let transformedNumber = parseFloat(Number(item || 0).toFixed(2))
    return transformedNumber
}

let calculateTotals = function()
{
    let totalBillAmount = 0
    let totalPaidAmount = 0
    let totalOutstandingAmount = 0
    let totalAppliedAmount = 0

    // loop through each amounts
    $('.bill_amount').each(function() { totalBillAmount = parsingDecimal(totalBillAmount) + parsingDecimal($(this).val().replace(',', '') || 0) })
    $('.paid_amount').each(function() { totalPaidAmount = parsingDecimal(totalPaidAmount) + parsingDecimal($(this).val().replace(',', '') || 0) })
    $('.outstanding_amount').each(function() { totalOutstandingAmount = parsingDecimal(totalOutstandingAmount) + parsingDecimal($(this).val().replace(',', '') || 0) })
    $('.applied_amount').each(function() { totalAppliedAmount = parsingDecimal(totalAppliedAmount) + parsingDecimal($(this).val().replace(',', '') || 0) })

    // update the view (the td in tfoot)
    $('.total-bill-amount').text(totalBillAmount.toFixed(2))
    $('.total-paid-amount').text(totalPaidAmount.toFixed(2))
    $('.total-outstanding-amount').text(totalOutstandingAmount.toFixed(2))
    $('.total-applied-amount').text(totalAppliedAmount.toFixed(2))
}


$('.remaining_amount_mt').val(remainingAmount.toFixed(2))
$('.amount_mt').on('keyup', function() {
    amount = $(this).val()
    remainingAmount = parsingDecimal($(this).val().replace(',','') || 0)
    $('.remaining_amount_mt').val(remainingAmount.toFixed(2))

    if (amount == 0)
        $('.item_check').prop('disabled', true)
    else
        $('.item_check').prop('disabled', false)

})

$('tbody').on('change', '.tax_code', function() {
    let row = $(this).parents('tr')

    // update the value of tax_rate by choosing tax_code
    if ($(this).val()) {
        let selectedTax = row.find('.tax_code').find(':selected').val()
        let tax = taxCodes.filter(tax => tax.t_code == selectedTax)[0]
        row.find('.tax_rate').val(tax.t_rate)
    }

}).on('change', '.item_check', function() {
    let row = $(this).parents('tr')

    if ($(this).prop('checked')) {
        $(this).siblings('.is_checked').val('true')
        minusCalculateFields(row)
    } else {
        $(this).siblings('.is_checked').val('false')
        plusCalculateFields(row)
    }

    calculateTotals()
})

let minusCalculateFields = function(row) {
    if (remainingAmount >= 0 ) {
        let billAmount = parsingDecimal(row.find('.bill_amount').val().replace(',','') || 0)

        // if current remaining rv amount is more than billamount, use bill amount, otherwise, if less than that left, use until finish
        // default to using remaining amount
        let appliedAmount = remainingAmount
        if (remainingAmount > billAmount)
            appliedAmount = billAmount

        let outstanding = parsingDecimal(row.find('.outstanding_amount').val().replace(',','') || 0)
        if (outstanding <= billAmount && remainingAmount >= outstanding)
            appliedAmount = outstanding


        // keep on reducing the amount
        remainingAmount = remainingAmount - appliedAmount
        $('.remaining_amount_mt').val(remainingAmount)

        row.find('.applied_amount').val(appliedAmount.toFixed(2))
        row.find('.applied_amount').siblings('.displayed-value').text(appliedAmount.toFixed(2))

        let outstandingAmount = parsingDecimal(row.find('.outstanding_amount').val().replace(',', '') || 0) - appliedAmount
        row.find('.outstanding_amount').val(outstandingAmount.toFixed(2))
        row.find('.outstanding_amount').siblings('.displayed-value').text(outstandingAmount.toFixed(2))

        let paidAmount = parsingDecimal(row.find('.paid_amount').val().replace(',', '') || 0) + appliedAmount
        row.find('.paid_amount').val(paidAmount.toFixed(2))
        row.find('.paid_amount').siblings('.displayed-value').text(paidAmount.toFixed(2))
    }
}

let plusCalculateFields = function(row) {
    let appliedAmount = parsingDecimal(row.find('.applied_amount').val().replace(',','') || 0)

    // keep on adding the amount
    remainingAmount = remainingAmount + appliedAmount
    $('.remaining_amount_mt').val(remainingAmount)

    let outstandingAmount = parsingDecimal(row.find('.outstanding_amount').val().replace(',', '') || 0) + appliedAmount
    row.find('.outstanding_amount').val(outstandingAmount.toFixed(2))
    row.find('.outstanding_amount').siblings('.displayed-value').text(outstandingAmount.toFixed(2))

    let paidAmount = parsingDecimal(row.find('.paid_amount').val().replace(',', '') || 0) - appliedAmount
    row.find('.paid_amount').val(paidAmount.toFixed(2))
    row.find('.paid_amount').siblings('.displayed-value').text(paidAmount.toFixed(2))

    row.find('.applied_amount').val(0)
    row.find('.applied_amount').siblings('.displayed-value').text(0.00)
}