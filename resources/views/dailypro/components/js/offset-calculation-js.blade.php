// setting the method of calculation
let calculationMethod = $('.calculate_method:checked').val()
$('.calculate_method').on('change', function() {
    calculationMethod = $(this).val()

    // when changed, clear all selection and amount
    $('.item_check').each(function() {
        $(this).prop('checked', false)
        $(this).siblings('.is_checked').val('false')

        // reset the value
        let row = $(this).parents('tr')
        if (calculationMethod == 'F')
            plusCalculateFields(row)
        else
            reversePlusCalculateFields(row)
    })

    amount = 0
    remainingAmount = 0
    $('.amount_mt').val(amount)
    $('.remaining_amount_mt').val(remainingAmount)
})

// calculation
let remainingAmount = $('.system-settings-param').data('remaining-amount')
let amount = parseFloat($('.amount_mt').val() || 0)

/**
 * If the item is NaN, defaults to 0, otherwise, parse to fixed 2 decimal place
 * */
let parsingDecimal = function(item) {
    let transformedNumber = parseFloat(Number(item || 0).toFixed(2))
    return transformedNumber
}

$('.remaining_amount_mt').val(remainingAmount.toFixed(2))
$('.amount_mt').on('keyup', function() {
    amount = $(this).val()
    remainingAmount = parseFloat($(this).val().replace(',','') || 0)
    $('.remaining_amount_mt').val(remainingAmount.toFixed(2))
})

$('tbody').on('change', '.tax_code', function() {
    let row = $(this).parents('tr')

    // update the value of tax_rate by choosing tax_code
    if ($(this).val()) {
        let selectedTax = row.find('.tax_code').find(':selected').val()
        let tax = taxCodes.filter(tax => tax.t_code == selectedTax)[0]
        row.find('.tax_rate').val(tax.t_rate)
    }

}).on('change', '.item_check', function() {
    let row = $(this).parents('tr')

    if ($(this).prop('checked')) {
        $(this).siblings('.is_checked').val('true')
        if (calculationMethod == 'F')
            minusCalculateFields(row)
        else
            reverseMinusCalculateFields(row)
    } else {
        $(this).siblings('.is_checked').val('false')
        if (calculationMethod == 'F')
            plusCalculateFields(row)
        else
            reversePlusCalculateFields(row)
    }
})

let minusCalculateFields = function(row) {
    if (remainingAmount >= 0 ) {
        let billAmount = parseFloat(row.find('.bill_amount').val().replace(',','') || 0)

        // if current remaining rv amount is more than billamount, use bill amount, otherwise, if less than that left, use until finish
        // default to using remaining amount
        let appliedAmount = remainingAmount
        if (remainingAmount > billAmount)
            appliedAmount = billAmount

        let outstanding = parseFloat(row.find('.outstanding_amount').val().replace(',','') || 0)
        if (outstanding <= billAmount && remainingAmount >= outstanding)
            appliedAmount = outstanding


        // keep on reducing the amount
        remainingAmount = remainingAmount - appliedAmount
        $('.remaining_amount_mt').val(remainingAmount)

        row.find('.applied_amount').val(appliedAmount.toFixed(2))
        row.find('.applied_amount').siblings('.displayed-value').text(appliedAmount.toFixed(2))

        let outstandingAmount = parseFloat(row.find('.outstanding_amount').val().replace(',', '') || 0) - appliedAmount
        row.find('.outstanding_amount').val(outstandingAmount.toFixed(2))
        row.find('.outstanding_amount').siblings('.displayed-value').text(outstandingAmount.toFixed(2))

        let paidAmount = parseFloat(row.find('.paid_amount').val().replace(',', '') || 0) + appliedAmount
        row.find('.paid_amount').val(paidAmount.toFixed(2))
        row.find('.paid_amount').siblings('.displayed-value').text(paidAmount.toFixed(2))
    }
}

let plusCalculateFields = function(row) {
    let appliedAmount = parseFloat(row.find('.applied_amount').val().replace(',','') || 0)

    // keep on adding the amount
    remainingAmount = remainingAmount + appliedAmount
    $('.remaining_amount_mt').val(remainingAmount)

    let outstandingAmount = parseFloat(row.find('.outstanding_amount').val().replace(',', '') || 0) + appliedAmount
    row.find('.outstanding_amount').val(outstandingAmount.toFixed(2))
    row.find('.outstanding_amount').siblings('.displayed-value').text(outstandingAmount.toFixed(2))

    let paidAmount = parseFloat(row.find('.paid_amount').val().replace(',', '') || 0) - appliedAmount
    row.find('.paid_amount').val(paidAmount.toFixed(2))
    row.find('.paid_amount').siblings('.displayed-value').text(paidAmount.toFixed(2))

    row.find('.applied_amount').val(0)
    row.find('.applied_amount').siblings('.displayed-value').text(0.00)
}

let reverseMinusCalculateFields = function(row) {
    let outstandingAmount = parseFloat(row.find('.outstanding_amount').val().replace(',', '') || 0)
    let paidAmount = parseFloat(row.find('.paid_amount').val().replace(',', '') || 0)

    // keep on adding the amount
    amount = amount + outstandingAmount

    // by default, the applied amount should take the outstanding amount, then, outstanding amount should be 0 afterwards
    appliedAmount = outstandingAmount

    // update the paidAmount according to the applied amount
    paidAmount = paidAmount + appliedAmount
    $('.amount_mt').val(amount)

    row.find('.applied_amount').val(appliedAmount.toFixed(2))
    row.find('.applied_amount').siblings('.displayed-value').text(appliedAmount.toFixed(2))

    row.find('.outstanding_amount').val(0)
    row.find('.outstanding_amount').siblings('.displayed-value').text(0)

    row.find('.paid_amount').val(paidAmount.toFixed(2))
    row.find('.paid_amount').siblings('.displayed-value').text(paidAmount.toFixed(2))
}

let reversePlusCalculateFields = function(row) {
    if (amount > 0) {
        let outstandingAmount = parsingDecimal(row.find('.outstanding_amount').val().replace(',', ''))
        let appliedAmount = parsingDecimal(row.find('.applied_amount').val().replace(',', ''))
        let paidAmount = parsingDecimal(row.find('.paid_amount').val().replace(',', ''))

        // keep on reducing the amount
        amount = parsingDecimal(amount - appliedAmount)

        // update the outstandingAmount and paidAmount according to the applied amount
        outstandingAmount = parsingDecimal(outstandingAmount + appliedAmount)
        paidAmount = parsingDecimal(paidAmount - appliedAmount)

        $('.amount_mt').val(amount)

        row.find('.applied_amount').val(0)
        row.find('.applied_amount').siblings('.displayed-value').text(0)

        row.find('.outstanding_amount').val(outstandingAmount.toFixed(2))
        row.find('.outstanding_amount').siblings('.displayed-value').text(outstandingAmount.toFixed(2))

        row.find('.paid_amount').val(paidAmount.toFixed(2))
        row.find('.paid_amount').siblings('.displayed-value').text(paidAmount.toFixed(2))
    }
}
