<div class="modal fade grnoto-modal" id="grnoto-modal" tabindex="-1" role="dialog" aria-hidden="true">
    {{-- This should only show for related documents (if this is Invoice, the results should be just for Invoice) --}}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Gasracks</h4>
            </div>
            <div class="modal-body">        
                {{-- <form action="" method="POST">
                  @csrf
                  <div class="col-md-12" style="text-align: right;">
                      <input name="searchdocto" type="text" id="searchdocto" placeholder="Search Document No" class="searchdocto">
                      <button type="button" class="btn btn-primary searchdocnoto1">Search</button>
                  </div>
                </form>       --}}
                <table class="table table-striped table-bordered" id="grnotable2" width="100%">
                    <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button id="close-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>