<div class="row">
    <div class="col-md-4 narrow-padding-global">
        {!! Form::select('account_code', $masterCodes->pluck('d_detail', 'd_acode'), isset($receiptVoucher) ? $receiptVoucher->accountCode : null, [
            'class' => 'form-control',
            'placeholder' => '',
            'id' => 'account_code',
            'data-master' => $masterCodes,
            'data-acodes' => $masterCodes->pluck('d_acode'),
            'data-route' => route('api.get.bills-for-account', 'ACCOUNT')
        ]) !!}
    </div>
    <div class="col-md-8 narrow-padding-global">
        <div class="input-group">
            @include('common.form.fa-prepend', [ 'prepend' => 'fa fa-user' ])
            {!! Form::text('account_holder', null, [ 'class' => 'form-control', 'id' => 'account_holder', 'placeholder' => 'Customer Name' ]) !!}
        </div>
    </div>

    {{-- Payment Method --}}
    <div class="col-md-12 narrow-padding-global">
        <div class="pt-3">
            <h5>
                Payment Method
                <small>
                    <a href="#" id="set-payment-methods" class="d-none" data-toggle="modal" data-target=".payment-method-modal"></a>
                </small>
            </h5>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 col-md-4 pr-0">
                <div class="payment-methods">
                    {!! Form::select('method', $paymentMethods->pluck('label', 'P_TYPE'), null, [
                        'class' => 'form-control select2',
                        'data-methods' => $paymentMethods,
                        'id' => 'payment-method',
                        'placeholder' => '',
                        'required'
                    ]) !!}
                </div>
            </div>
            <div class="col-sm-6 col-md-8 pl-2">
                <div class="payment-methods-detail d-none">
                    <div class="card">
                        <div class="card-body">
                            @include('billing-module.components.vouchers.payment-methods')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@include('billing-module.components.payment-method-modal')

@push('scripts')
<script type="text/javascript">
(function() {
    function isEmpty(obj) {
        for (let key in obj) if (obj.hasOwnProperty(key))
            return false;

        return true;
    }

    function formatState (state) {
        // put the name (only - without the id) values into the next textbox
        let name = state.text.replace(state.id + ' ', '')
        $('#account_holder').val(name)

        // put the name (only id) value into the select2 dropdown
        return state.id
    }

    let accountCode = $('.system-settings-param').data('account-code')
    let accountHolder = $('.system-settings-param').data('account-holder')
    let newAccount = new Option(accountHolder, accountCode, false, false);
    $('.custom-select2').select2({ dropdownAutoWidth : true, tags: true })
    $('#account_code').select2({ templateSelection: formatState, dropdownAutoWidth : true, tags: true })
    $('#account_code').append(newAccount).trigger('change')

    $('.amount').inputmask('currency', { prefix: '', rightAlign: true })
    $('#payment-method').on('change', function() {
        let paymentMethods = $(this).data('methods')

        if ($(this).val() === '') {
            $('.payment-methods-detail').hasClass('d-none') ? '' : $('.payment-methods-detail').addClass('d-none')
            $('.default-section').hasClass('d-none') ? '' : $('.default-section').addClass('d-none')
            $('.cheque-section').hasClass('d-none') ? '' : $('.cheque-section').addClass('d-none')
            $('.credit-card-section').hasClass('d-none') ? '' : $('.credit-card-section').addClass('d-none')
        } else {
            // find the selected method
            let selectedMethod = paymentMethods.find(paymentMethod => paymentMethod.P_TYPE == $(this).val())

            // show which section and fill the account no
            switch ($(this).val()) {
                case 'Credit Card':
                    $('.payment-methods-detail').hasClass('d-none') ? $('.payment-methods-detail').removeClass('d-none') : ''
                    $('.credit-card-section').hasClass('d-none') ? $('.credit-card-section').removeClass('d-none') : ''
                    $('.cheque-section').hasClass('d-none') ? '' : $('.cheque-section').addClass('d-none')
                    $('.default-section').hasClass('d-none') ? '' : $('.default-section').addClass('d-none')
                    $('.bank_account').val(selectedMethod.glAccountDetail)
                    $('.bank_description').val(selectedMethod.glAccountFor)
                    break
                case 'Cheque':
                    $('.payment-methods-detail').hasClass('d-none') ? $('.payment-methods-detail').removeClass('d-none') : ''
                    $('.cheque-section').hasClass('d-none') ? $('.cheque-section').removeClass('d-none') : ''
                    $('.credit-card-section').hasClass('d-none') ? '' : $('.credit-card-section').addClass('d-none')
                    $('.default-section').hasClass('d-none') ? '' : $('.default-section').addClass('d-none')
                    $('.cheque_account').val(selectedMethod.glAccountDetail)
                    $('.cheque_bank').val(selectedMethod.glAccountFor)
                    break
                default:
                    $('.payment-methods-detail').hasClass('d-none') ? $('.payment-methods-detail').removeClass('d-none') : ''
                    $('.default-section').hasClass('d-none') ? $('.default-section').removeClass('d-none') : ''
                    $('.cheque-section').hasClass('d-none') ? '' : $('.cheque-section').addClass('d-none')
                    $('.credit-card-section').hasClass('d-none') ? '' : $('.credit-card-section').addClass('d-none')
                    $('.account').val(selectedMethod.glAccountDetail)
                    break
            }

        }
    })

}) ()
</script>
@endpush
