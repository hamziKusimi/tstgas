<div class="form-group">
    <div class="row">
        <div class="col">
            {!! Form::label('memo', 'Memo', ['class' => 'col-form-label']) !!}
            <small id="memo_template_option">
                <a href="#" id="memo_button" class="d-none" data-toggle="modal" data-target="#template-modal" data-textarea-target="#memo">Get template</a>
            </small>
        </div>
        <div class="col">
            <div class="float-right">
                <button type="button" name="button" class="btn btn-sm bg-dark-blue text-white" data-toggle="collapse" data-target="#memo_div">
                    <i class="fa fa-expand"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="collapse" id="memo_div">
        {!! Form::textarea('memo', null, [
            'class' => 'form-control summernote',
            'rows' => '2',
            'id' => 'memo',
        ]) !!}
    </div>
</div>