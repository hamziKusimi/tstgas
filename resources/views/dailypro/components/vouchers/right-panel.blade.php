{{-- this will only add the modal once because one page can have only one left/right panel --}}
@include('common.modal.search-document')

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('doc_no', 'Doc No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="input-group col-md-9">
            {!! Form::text('doc_no', isset($item) ? $item->doc_no : $runningNumber->nextRunningNoString, [
                'class' => 'form-control doc_no',
                'placeholder' => 'Document No.',
                'maxlength' => 20,
                isset($item) && isset($item->hasOffsets) && $item->hasOffsets ? 'disabled': '',
            ]) !!}
            @include('common.form.fa-append', [ 'append' => 'fa fa-search', 'additionalClass' => 'search-document pointer' ])
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('date', 'Date', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="input-group col-md-9" id="date-picker" data-target-input="nearest">
            {!! Form::text('date', null, [
                'class' => 'form-control date datetimepicker-input',
                'placeholder' => '',
                'data-target' => '#date-picker'
            ]) !!}
            <div class="input-group-append" data-target="#date-picker" data-toggle="datetimepicker">
                <div class="input-group-text bg-dark-green text-white">
                    <small><i class="fa fa-calendar"></i></small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('batch_no', 'Batch No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="input-group col-md-9">
            {!! Form::text('batch_no', isset($item) ? $item->batch_no : null, [
                'class' => 'form-control',
                'placeholder' => '',
                'maxlength' => 20,
            ]) !!}
            @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag' ])
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('reference_no', 'Ref No.', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="input-group col-md-9">
            {!! Form::text('reference_no', isset($item) ? $item->m_ref : null, [
                'class' => 'form-control reference_no',
                'placeholder' => '',
                'maxlength' => 20,
            ]) !!}
            @include('common.form.fa-append', [ 'append' => 'fa fa-hashtag' ])
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('amount_mt', 'Rec. Amount', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::number('amount_mt', isset($item) ? $item->amount : 0.00, ['class' => 'form-control amount_mt', 'min' => '0', 'step' => '0.01']) !!}
        </div>
        {!! Form::label('remaining_amount_mt', 'Remaining', ['class' => 'col-form-label col-md-3 text-right']) !!}
        <div class="col-md-3">
            {!! Form::text('remaining_amount_mt', 0.00, ['class' => 'form-control remaining_amount_mt text-danger']) !!}
        </div>
    </div>
</div>
