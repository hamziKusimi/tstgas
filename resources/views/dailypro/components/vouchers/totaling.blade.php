<div class="row">
    {!! Form::label('subtotal', 'Subtotal', ['class' => 'col-md-6 col-form-label']) !!}
    <div class="col-md-6">
        {!! Form::text('subtotal', isset($item) ? $item->subTotal : null, ['class' => 'form-control-plaintext form-control-lg pr-2 money font-weight-bold', 'id' => 'subtotal', 'readonly']) !!}
    </div>
</div>

<div class="row {{ (boolean) get_array_result_value($_companySettings, 'S_USE_TAX') ? '': 'd-none' }}">
    {!! Form::label('tax', 'Tax', ['class' => 'col-md-6 col-form-label']) !!}
    <div class="col-md-6">
        {!! Form::text('tax', isset($item) ? $item->taxAmount : null, ['class' => 'form-control-plaintext form-control-lg pr-2 money font-weight-bold', 'id' => 'tax', 'readonly']) !!}
    </div>
</div>

<div class="row {{ (boolean) get_array_result_value($_companySettings, 'S_USE_TAX') ? 'd-none' : '' }}">
    {!! Form::label('discount', 'Discount', ['class' => 'col-md-9 col-form-label']) !!}
    <div class="col-md-3">
        {!! Form::text('global_discount', 0.00, ['class' => 'form-control form-control-lg number-right-align global_discount']) !!}
    </div>
</div>

<div class="row {{ $roundingIsEnabled ? '' : 'd-none' }}">
    {!! Form::label('rounding', 'Rounding', ['class' => 'col-md-6 col-form-label']) !!}
    <div class="col-md-6">
        {!! Form::text('rounding', isset($item) ? $item->rounding : null, ['class' => 'form-control-plaintext form-control-lg pr-2 money font-weight-bold', 'id' => 'rounding', 'readonly']) !!}
    </div>
</div>

<hr/>
<div class="row">
    {!! Form::label('grand_total', 'Grand Total', ['class' => 'col-md-6 col-form-label col-form-label-lg', 'style' => 'font-size:1.55rem;']) !!}
    <div class="col-md-6">
        {!! Form::text('grand_total', isset($item) ? $item->grandTotal : null, [
            'class' => 'form-control-plaintext pr-2 form-control-lg money font-weight-bold h1',
            'id' => 'grand_total',
            'style' => 'font-size:1.55rem;',
            'readonly',
        ]) !!}
    </div>
</div>