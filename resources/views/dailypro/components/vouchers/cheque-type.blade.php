@if (isset($item))
<div class="form-check form-check-inline col-form-label">
        <input class="form-check-input" type="radio" name="cheque_type" id="cheque_type_L" value="L" {{ $item->cheque_type == 'L' ? 'checked' : '' }}>
        <label class="form-check-label" for="cheque_type_L">Local Cheque</label>
    </div>
    <div class="form-check form-check-inline col-form-label">
        <input class="form-check-input" type="radio" name="cheque_type" id="cheque_type_O" value="O" {{ $item->cheque_type == 'O' ? 'checked' : '' }}>
        <label class="form-check-label" for="cheque_type_O">Outstanding Cheque</label>
    </div>
@else
<div class="form-check form-check-inline col-form-label">
    <input class="form-check-input" type="radio" name="cheque_type" id="cheque_type_L" value="L" checked>
    <label class="form-check-label" for="cheque_type_L">Local Cheque</label>
</div>
<div class="form-check form-check-inline col-form-label">
    <input class="form-check-input" type="radio" name="cheque_type" id="cheque_type_O" value="O">
    <label class="form-check-label" for="cheque_type_O">Outstanding Cheque</label>
</div>
@endif