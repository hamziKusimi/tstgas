<div class="cheque-section">
    <div class="form-group row">
        {!! Form::label('cheque_account', 'Account', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('cheque_account', isset($item) ? $item->cheque_account : null, ['class' => 'form-control-plaintext cheque_account']) !!}
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('cheque_no', 'Cheque No.', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('cheque_no', isset($item) ? $item->cheque_no : null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('cheque_date', 'C. Date', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="input-group col-md-9">
            {!! Form::date('cheque_date', isset($item) ? $item->payment_date : null, [
                'class' => 'form-control cheque-date',
                'placeholder' => '',
            ]) !!}
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('cheque_type', 'C. Type', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="clearfix"></div>
        <div class="col-md-9">
            @include('billing-module.components.vouchers.cheque-type')
        </div>
    </div>
</div>

<div class="credit-card-section">
    <div class="form-group row">
        {!! Form::label('bank_account', 'Account No.', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('bank_account', isset($item) ? $item->account_code : null, ['class' => 'form-control-plaintext bank_account']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('bank_charges_account', 'Bank Charges Acc', ['class' => 'col-md-4 col-form-label']) !!}
        <div class="col-md-8">
            {!! Form::text('bank_charges_account', isset($item) ? $item->bank_charges_account : null, ['class' => 'form-control-plaintext bank_account']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('bank_charges', 'Bank Charges.', ['class' => 'col-md-4 col-form-label']) !!}
        <div class="col-md-8">
            {!! Form::text('bank_charges', isset($item) ? $item->charges_amount : null, [ 'class' => 'form-control bank_charges' ]) !!}
        </div>
    </div>
    {!! Form::hidden('bank_description', isset($item) ? $item->charges_description : null, ['class' => 'bank_description']) !!}
</div>

<div class="default-section">
    <div class="form-group row">
        {!! Form::label('account', 'Account', ['class' => 'col-md-3 col-form-label']) !!}
        <div class="col-md-9">
            {!! Form::text('account', isset($item) ? $item->account_code : null, ['class' => 'form-control-plaintext account']) !!}
        </div>
    </div>
</div>