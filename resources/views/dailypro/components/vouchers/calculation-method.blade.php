@if (isset($item))
<div class="col-md-9">
    <div class="form-check form-check-inline col-form-label">
        <input class="form-check-input calculate_method" type="radio" name="calculation_method" id="calculation_method_F" value="F" {{ $item->calculation_method == 'F' ? 'checked' : '' }}>
        <label class="form-check-label" for="calculate_method_F">Forward</label>
    </div>
    <div class="form-check form-check-inline col-form-label">
        <input class="form-check-input calculate_method" type="radio" name="calculation_method" id="calculation_method_R" value="R" {{ $item->calculation_method == 'R' ? 'checked' : '' }}>
        <label class="form-check-label" for="calculate_method_R">Reverse</label>
    </div>
</div>
@else
<div class="col-md-9">
    <div class="form-check form-check-inline col-form-label">
        <input class="form-check-input calculate_method" type="radio" name="calculation_method" id="calculation_method_F" value="F" checked>
        <label class="form-check-label" for="calculate_method_F">Forward</label>
    </div>
    <div class="form-check form-check-inline col-form-label">
        <input class="form-check-input calculate_method" type="radio" name="calculation_method" id="calculation_method_R" value="R">
        <label class="form-check-label" for="calculate_method_R">Reverse</label>
    </div>
</div>
@endif