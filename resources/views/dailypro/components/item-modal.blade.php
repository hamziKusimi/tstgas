<div class="d-none">
    {{-- createNewItemOption will trigger the modal from JS --}}
    <a href="#" class="createNewItemOption">
        <span class="pl-3 pr-3">
            <i class="fa fa-plus pr-3 pt-3 pb-3"></i>
            Create new item
        </span>
    </a>
</div>

<div class="modal fade item-modal-template" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New Item</h5>
                <button type="button" class="close" data-dismiss="modal">&times</button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '', 'method' => 'POST', 'class' => 'item-form']) !!}
                    @include('dailypro.invoices.item-form-modal')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>