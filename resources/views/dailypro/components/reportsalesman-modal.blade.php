<div class="modal fade reportsalesman-modal" id="reportsalesman-modal" tabindex="-1" role="dialog" aria-hidden="true"  data-get-route="{{ route('cashbills.docno.frm') }}">
    {{-- This should only show for related documents (if this is Invoice, the results should be just for Invoice) --}}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Documents</h4>
            </div>
            <div class="modal-body">        
                {{-- <form action="" method="POST">
                  @csrf
                  <div class="col-md-12" style="text-align: right;">
                      <input name="search" type="text" id="search" placeholder="Search Document No" class="search">
                      <button type="button" class="btn btn-primary searchdocno">Search</button>
                  </div>
                </form>       --}}
                <table class="table table-striped table-bordered" id="cashbillsalesmantable" width="100%">
                    <thead>
                        <tr>
                            <th>Salesman</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button id="close-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
