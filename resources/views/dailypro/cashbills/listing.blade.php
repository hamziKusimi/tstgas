<head>
    <link href="{{ asset('css/pdf-reporting.css') }}" rel="stylesheet">
</head>
<div class="row">
        <p style="text-align:center; font-size:24px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Listing - Cash Bill {{ ($date) }}</p>

</div>
@php
    ($i = 0)
@endphp
<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="row">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%;font-size: 12px;">
                    <thead class="" border="1" style="font-size: 12px;">
                        <tr>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                                  <p style="margin-top:3px;"></p>
                            </th>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                                  <p style="margin-top:3px;">Description</p>
                            </th>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">
                                  <p style="margin-top:3px;">Location</p>
                            </th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                                  <p style="margin-top:3px;">Quantity</p>
                            </th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                                  <p style="margin-top:3px;">UOM</p>
                            </th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                                  <p style="margin-top:3px;">Unit Price</p>
                            </th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">
                                  <p style="margin-top:3px;">Amount</p>
                            </th>
                        </tr>
                    </thead>

                @php $docno = "" @endphp
                @if(sizeof($cashbillJSON) > 0)
                    <tbody>
                        @foreach ($cashbillJSON as $cashbill)
                            <!-- check last updated doc no if not same with new updated then display -->
                            @if($docno != $cashbill->docno)

                                <thead class="" style="font-size: 12px;">
                                    <tr>
                                        <th></th>
                                        <th colspan="6" style="text-align:left;">
                                              <p style="margin-top:3px;">{{ $cashbill->details }}</p></th>
                                    </tr>
                                </thead>
                            @endif
                                <tbody>
                                    <tr>
                                        <td style="font-size: 12px;"></td>
                                        <td style="font-size: 12px;">{{ $cashbill->description}}</td>
                                        <td style="font-size: 12px;">{{ $cashbill->location}}</td>

                                        @if($systemsetup == '1')
                                        <td style="text-align: right;font-size: 12px;">{{ number_format($cashbill->quantity, 2)}}</td>
                                        @else
                                        <td style="text-align: right;font-size: 12px;">{{ number_format($cashbill->quantity)}}</td>
                                        @endif
                                        <td style="text-align: right;font-size: 12px;">{{ $cashbill->uom}}</td>
                                        <td style="text-align: right;font-size: 12px;">{{ number_format($cashbill->unit_price, 2)}}</td>
                                        <td style="text-align: right;font-size: 12px;">{{ number_format($cashbill->amount, 2)}}</td>
                                    </tr>
                                </tbody>
                                <!-- count the row for each doc no -->
                                @php
                                    $count = 0;
                                    $co = DB::select(DB::raw("
                                                SELECT count(cashbilldts.doc_no) as count from cashbilldts where cashbilldts.doc_no = :docno and cashbilldts.deleted_at IS NULL
                                        "), [
                                            'docno'=>$cashbill->docno,
                                        ]);
                                        foreach($co as $c){
                                            $count = $count + $c->count;
                                        }
                                @endphp

                                @php
                                $qty = 0;
                                $id = '';
                                $inv = DB::select(DB::raw("
                                            SELECT cashbilldts.id as id, cashbilldts.qty as qty from cashbilldts where cashbilldts.doc_no = :docno and cashbilldts.deleted_at IS NULL GROUP BY cashbilldts.doc_no, cashbilldts.subject, cashbilldts.item_code
                                    "), [
                                        'docno'=>$cashbill->docno,
                                    ]);
                                    foreach($inv as $invc){
                                        //get total quantity for each cashbills doc no
                                        $qty = $qty + $invc->qty;
                                        //get last id
                                        $id = $invc->id;
                                    }
                                @endphp
                                <!-- check last id then display -->
                                @if($cashbill->id == $id)
                                {{-- {{ dd($cashbill->id == $id) }}       --}}

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        @if($systemsetup == '1')
                                            {{-- @if($totalqty == "") --}}
                                                <td style="border-top:1px solid;text-align:right;font-size: 12px;">{{ number_format($qty, 2) }}</td>
                                            {{-- @else
                                                <td style="border-top:1px solid;text-align:right;font-size: 12px;">{{ number_format($totalqty, 2) }}</td>
                                            @endif --}}
                                        @else
                                            {{-- @if($totalqty == "") --}}
                                                <td style="border-top:1px solid;text-align:right;font-size: 12px;">
                                                     <p style="margin-top:3px;">{{ number_format($qty) }}</p></td>
                                            {{-- @else
                                                <td style="border-top:1px solid;text-align:right;font-size: 12px;">
                                                    <p style="margin-top:3px;">{{ number_format($totalqty) }}</p>
                                                    </td>
                                            @endif --}}
                                        @endif
                                        <td style="border-top:1px solid;text-align:right;font-size: 12px;"></td>
                                        <td style="border-top:1px solid;text-align:right;font-size: 12px;"></td>
                                        <td style="border-top:1px solid;text-align:right;font-size: 12px;">{{ number_format($cashbill->totalamount, 2) }}</td>
                                    </tr>
                                <!-- else if row have only 1 data also display -->
                                {{-- @elseif($count <= '1')
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-top:1px solid;">{{ number_format($cashbill->quantity, 2) }}</td>
                                        <td style="border-top:1px solid;"></td>
                                        <td style="border-top:1px solid;"></td>
                                        <td style="border-top:1px solid;">{{ number_format($cashbill->totalamt, 2) }}</td>
                                    </tr> --}}
                                @endif
                            @php
                                $docno = $cashbill->docno;
                            @endphp
                        @endforeach
                        <!-- Display Grand Total -->
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;font-size: 12px;">Grand Total</td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">
                                <p style="margin-top:3px;"> {{ number_format($finaltotalamount, 2) }}</p>
                               </td>
                        </tr>
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>


