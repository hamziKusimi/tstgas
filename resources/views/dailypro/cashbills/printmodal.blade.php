<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('cashbills.print') }}" method="GET" target="_blank">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cash Bill Printing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-3">
                            <p><b>Report Type :</b></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            {!! Form::radio('cashbill_rcv','listing',null, ['checked'=>'checked','id'
                            =>'listing', 'class' => 'Cashbill_rcv'])!!}
                            {!! Form::label('cashbill_rcv_label', 'Listing', ['class' => 'col-form-label']) !!}
                        </div>
                        <div class="col-2">
                            {!! Form::radio('cashbill_rcv','summary',null, [ 'id' =>'summary', 'class' => 'Cashbill_rcv'])!!}
                            {!! Form::label('cashbill_rcv_label', 'Summary', ['class' => 'col-form-label']) !!}
                        </div>
                        <div class="col-2">
                            {!! Form::radio('cashbill_rcv','reprint',null, [ 'id' =>'reprint', 'class' => 'Cashbill_rcv'])!!}
                            {!! Form::label('cashbill_rcv_label', 'Re-Print', ['class' => 'col-form-label']) !!}
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('dates_Chkbx',null,null, [ 'id' =>'dates_Chkbx'])!!}
                            {!! Form::label('Date', 'Date', ['class' => 'col-form-label']) !!}
                        </div>

                        <label class="col-form-label">:</label>
                        <div class="col-4">
                            <div class="form-group">
                                <div class="input-group date">
                                    {!! Form::text('dates', null, ['class' => 'form-control form-data',
                                    'autocomplete'=>'off', 'id' =>'dates'
                                    ])!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('docno_Chkbx',null,null, [ 'id' =>'docno_Chkbx'])!!}
                            {!! Form::label('docno', 'Document No.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                        {!! Form::text('docno_frm', null, ['class' => 'form-control docno_frm', 'id'

                                        =>'docno_frm'

                                        ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdocno" data-toggle="modal" data-target=".reportdocno-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">{!! Form::text('docno_to', null, ['class' => 'form-control docno_to', 'id'

                                    =>'docno_to'

                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdocnoto" data-toggle="modal" data-target=".reportdocnoto-modal" style="width:35%">
                                        <small style="text-align:center;">
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('LPO_Chkbx_1',null,null, [ 'id' =>'LPO_Chkbx_1'])!!}
                            {!! Form::label('LPO', 'LPO No.', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('LPO_frm_1', null, ['class' => 'form-control', 'id'
                                =>'LPO_frm_1'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {!! Form::text('LPO_to_1', null, ['class' => 'form-control', 'id'
                                =>'LPO_to_1'
                                ,'disabled' =>'disabled'])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('debCode_Chkbx',null,null, [ 'id' =>'debCode_Chkbx'])!!}
                            {!! Form::label('debCode', 'Debtor Code', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                    {{-- {!! Form::select('debCode_frm', $debtor_select, null, ['class' => 'form-control
                                    select debCode_frm', 'id'
                                    =>'debCode_frm'
                                    ,'disabled' =>'disabled'])!!} --}}
                                    {!! Form::text('debCode_frm', null, ['class' => 'form-control debCode_frm', 'id'
                                    =>'debCode_frm'
                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdebtor" data-toggle="modal" data-target=".reportdebtor-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                    {{-- {!! Form::select('debCode_to', $debtor_select, null, ['class' => 'form-control
                                    select debCode_to', 'id'
                                    =>'debCode_to'
                                    ,'disabled' =>'disabled'])!!} --}}
                                    {!! Form::text('debCode_to', null, ['class' => 'form-control debCode_to', 'id'
                                    =>'debCode_to'
                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchdebtorto" data-toggle="modal" data-target=".reportdebtorto-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            {!! Form::checkbox('salesman_Chkbx',null,null, [ 'id' =>'salesman_Chkbx'])!!}
                            {!! Form::label('salesman', 'Salesman Code', ['class' => 'col-form-label']) !!}
                        </div>
                        <label class="col-form-label">:</label>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                    {{-- {!! Form::select('salesman_frm', $salesman_select, null, ['class' => 'form-control
                                    select', 'id'
                                    =>'salesman_frm'
                                    ,'disabled' =>'disabled'])!!} --}}
                                    {!! Form::text('salesman_frm', null, ['class' => 'form-control salesman_frm', 'id'
                                    =>'salesman_frm'
                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchsalesman" data-toggle="modal" data-target=".reportsalesman-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <div class="input-group-append" style="width:100%">
                                    {{-- {!! Form::select('salesman_to', $salesman_select, null, ['class' => 'form-control
                                    select', 'id'
                                    =>'salesman_to'
                                    ,'disabled' =>'disabled'])!!} --}}
                                    {!! Form::text('salesman_to', null, ['class' => 'form-control salesman_to', 'id'
                                    =>'salesman_to'
                                    ,'disabled' =>'disabled'])!!}
                                    <div class="btn input-group bg-dark-blue text-white pointer searchsalesmanto" data-toggle="modal" data-target=".reportsalesmanto-modal" style="width:35%">
                                        <small>
                                            <i class="fa fa-search"></i>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary modal-btn" data-toggle="modal" data-target="#printModal">Print</button>
                    <button type="submit" class="btn btn-primary submit-btn" value="pdf" style="display: none">Print</button>
                </div>
                <div class="modal fade" id="printModal" tabindex="-1" role="dialog"
                aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Report Format</h5>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2" >
                                            <button type="submit" name="submitBtn" style="width: 100%;" value="excel" class="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Excel</a>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" name="submitBtn" style="width: 100%;" value="pdf" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>