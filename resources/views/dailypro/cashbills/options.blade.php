<div class="row">
    @isset($edit)
    <div class="col-md-2" style="padding-right:0px">
        <a href="{{ route('cashbills.create') }}" style="width: 100%;"
            class="btn form-control bg-dark-green text-white text-center btn-form-submit">New</a>
    </div>
    @endisset
    <div class="col-md-2" style="padding-right:0px;{{isset($edit) ? "padding-left:0px":null}}">
        @if(Auth::user()->hasPermissionTo('CASH_BILL_CR') || Auth::user()->hasPermissionTo('CASH_BILL_UP'))
        @include('common.billing.options.submit', [
        'faIcon' => 'share-square-o',
        'color' => 'bg-dark-blue text-white',
        'permissions' => ['Create Invoices', 'Update Cashbills'],
        'name' => 'Submit'
        ])
        @endif
    </div>
    <div class="col-md-2" style="padding-right:0px;padding-left:0px">
        @if(Auth::user()->hasPermissionTo('CASH_BILL_PR') && (isset($item) && $item->printed_by == null))
            @if (isset($item->id))
            {{-- <a href="{{ route('cashbills.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
                class="btn form-control bg-secondary text-white text-center btn-form-submit"><i
                    class="fa fa-print pr-2"></i>Print</a> --}}
                <button  type="button"  class="btn btn-secondary" data-toggle="modal" data-target="#printModal"
                    style="width: 100%;" ><i class="fa fa-print pr-2"></i>Print</button>
            @endif
        @elseif(Auth::user()->hasPermissionTo('CASH_BILL_PRY'))
            @if (isset($item->id))
            {{-- <a href="{{ route('cashbills.jasper', ['id' => $item->id]) }}" target="_blank" style="width: 100%;"
                class="btn form-control bg-secondary text-white text-center btn-form-submit"><i
                    class="fa fa-print pr-2"></i>Print</a> --}}
                <button  type="button"  class="btn btn-secondary" data-toggle="modal" data-target="#printModal"
                    style="width: 100%;" ><i class="fa fa-print pr-2"></i>Print</button>
            @endif
        @endif
    </div>
    @if(isset($edit))
    <div class="col-md-2" style="padding-left:0px">
        <a href="{{ route('cashbills.destroy', $item->id) }}" data-method="delete" style="width: 100%;"
            data-confirm="Confirm delete this account?" class="btn form-control bg-danger text-white text-center btn-form-submit">
            <i class="fa fa-trash pr-2"></i>Delete</a>
    </div>
    @endif
    @if (!isset($edit))
    <div class="col-md-2" style="padding-right:0px"></div>
    <div class="col-md-2" style="padding-right:0px"></div>
    @endif
    <div class="col-md-2 pl-1"></div>
    <div class="col-md-2">
        <a href="{{ route('cashbills.index') }}" style="width: 100%;" class="btn btn-danger"><i class="fa"></i>Back</a>
    </div>
</div>

@if (isset($item))
<div class="modal fade" id="printModal" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Report Type</h5>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="{{ route('cashbills.jasper', ['id' => $item->id,'docType' => 'A11']) }}" target="_blank" style="width: 100%;"
                                class="btn btn-primary"> A11 (Receipt)</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('cashbills.jasper', ['id' => $item->id, 'docType' => 'A4']) }}" target="_blank" style="width: 100%;"
                                class="btn btn-primary">A4</a>
                            </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endif