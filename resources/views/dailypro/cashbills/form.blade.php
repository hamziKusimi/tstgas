
<div class="form-group row">
    <div class="col-md-6">
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="debtor" class="col-md-2">Debtor</label>
            <div class="col-md-4">
                <select class="form-control myselect" name="debtor_id" id="debtor_id" data-values="{{ $debtors }}">
                    <option value=""></option>
                    @foreach($debtors as $debtor)
                        <option value="{{$debtor->id}}" {{ isset($cashbill)?$cashbill->debtor_id == $debtor->id  ? 'selected' : '' : ''}}>{{$debtor->accountcode}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <input name="debtordet" id="debtordet" type="text" class="form-control name" value="{{ isset($cashbill)?$cashbill->debtordet:'' }}" >
            </div>
        </div>

        <div class="form-group row" style="margin: 2px;padding: 2px">
            <label for="address" class="col-md-2">Address</label>
            <div class="col-md-10">
                <input name="address" type="text" class="form-control address" value="{{ isset($cashbill)?$cashbill->address:'' }}" >
            </div>
        </div>
    
        <div class="form-group row" style="margin: 2px;padding: 2px">
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-fax"></i></small>
                    </div>
                    <input name="fax" type="text" class="form-control fax" value="{{ isset($cashbill)?$cashbill->fax:'' }}" >
                </div>
            </div>

            <div class="col-md-4">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-mobile-phone"></i></small>
                    </div>
                    <input name="hp" type="text" class="form-control hp" value="{{ isset($cashbill)?$cashbill->hp:'' }}" >
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group-prepend">
                    <div class="input-group-text bg-dark-blue text-white">
                        <small><i class="fa fa-phone"></i></small>
                    </div>
                    <input name="tel" type="text" class="form-control tel" value="{{ isset($cashbill)?$cashbill->tel:'' }}" >
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="docno" class="col-md-3">Document No</label>
                <div class="col-md-9">
                    {{-- <input name="docno" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->docno:$runningNumber->nextRunningNoString }}" > --}}
                    <input name="docno" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->docno: $runningNumber->nextRunningNoString }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="date" class="col-md-3">Date</label>
                <div class="col-md-9">   
                    <div class="input-group-prepend">
                        <input type="text" name="date" id="datepicker" class="form-control" placeholder="dd-mm-yyyy"  value="{{ isset($cashbill->date)?date('d-m-Y', strtotime($cashbill->date)):'' }}">
                            <div class="input-group-text bg-dark-blue text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="lpono" class="col-md-3">LPO No</label>
                <div class="col-md-9">
                    <input name="lpono" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->lpono:'' }}" >
                </div>
            </div>
    
            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="ref1" class="col-md-3">Ref No 1 </label>
                <div class="col-md-9">
                    <input name="ref1" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->ref1:'' }}" >
                </div>
            </div>

            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="dono" class="col-md-3">D/O No</label>
                <div class="col-md-9">
                    <input name="dono" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->dono:'' }}" >
                </div>
            </div>

            <div class="form-group row" style="margin: 2px;padding: 2px">
                <label for="quotno" class="col-md-3">Quotation No</label>
                <div class="col-md-9">
                    <input name="quotno" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->quotno:'' }}" >
                </div>
            </div>
        </div>
@php($true='false')
</div>
<div class="form-group row" style="margin: 0px;padding: 0px">
    <div class="col-md-12">
        <div class="table cell-border stripe table table-sm">
            <table class="table cell-border stripe" id="dataTable">
                <thead style="background-color:#3c8dbc; color:white;">
                    <tr class="text-sm">
                        <th width="9%" class="text-center">SN</th>
                        <th width="12%" class="text-center">Stock Code</th>
                        <th width="25%" class="text-left">Description</th>
                        <th width="8%" class="text-left">Qty</th>
                        <th width="8%" class="text-left">UOM</th>
                        <th width="8%" class="text-left">Rate</th>
                        <th width="8%" class="text-left">Unit Price</th>
                        <th width="8%" class="text-left">TY</th>
                        <th width="7.5%" class="text-left">Discount</th>
                        <th width="12%" class="text-left">Amount</th>
                        <th width="1%" class="text-left">More</th>
                    </tr>
                </thead>
                
                <tbody class="table-form-tbody">
                    @include('dailypro/cashbills/cashbilltable') 
                    @includeWhen(isset($cashbilldts), 'dailypro/cashbills/cashbilltabledata')
                </tbody>
                
                <tfoot>
                    <tr>
                        <td colspan="12" style="padding:1px;">
                            <button type="button" class="btn" style="background-color: #4aa94a; color:white" id="add-new">
                                Add Line
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

