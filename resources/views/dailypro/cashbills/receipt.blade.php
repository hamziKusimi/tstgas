<html>
<head></head>
<body>
<style>
#invoice-POS{
	padding:2mm;
	margin: 0 auto;
	width: 80mm;
	background: #FFF;
}

h1{
	color: #000;
}

h2{
	font-size: 1.1em;
}

h3{
	font-weight: 300;
	line-height: 2em;
}

p{
	font-size: .8em;
	color: #000;
	line-height: 1.2em;
}

#roundingadj, #totamt, #paid{ /* Targets all id with 'col-' */
	border-bottom: 1px solid #000;
}

#totalexcltax{
	border-top: 1px solid #000;
}

#mid{
	min-height: 80px;
}
#bot{
	min-height: 50px;
}


.clientlogo{
	float: left;
	height: 60px;
	width: 60px;
	background-size: 60px 60px;
	border-radius: 50px;
}
.info{
	display: block;
	margin-left: 0;
}
.info p{
	font-size: 0.7em;
}

.title{
	float: right;
}
.title p{
	text-align: right;
}
table{
	width: 100%;
	border-collapse: collapse;
}
td{
	padding: 5px 0 5px 5px;
	//border: 1px solid #EEE
}
.tabletitle{
	font-size: .8em;
}

#header{
	border-top: 1px solid #000;
	border-bottom: 1px solid #000;
}


.item{
	width: 24mm;
}


#legalcopy{
	margin-top: 5mm;
}

.text-right{
	text-align: right;
}

.legal{
	text-align: center;
	font-size: 14px;
}

#totamt{
	font-size:16px;
	font-weight: bold;
}

.itemtext2{
	font-size: .9em;
}

</style>
<div id="invoice-POS">
	<center id="top">
		<div class="logo"></div>
		<div class="info">
			<p>
			<span style="font-size:18px;"><strong>{{ strtoupper(config('config.company.name')) }}</strong></span>
            @if (config('config.company.show_company_no') == 'true')
			<br/><span style="font-size:10px; margin-bottom:5px;">{{ config('config.company.company_no') }}</span>
			@endif
            {!! $coyaddr1 != "" ? "<br/>".$coyaddr1 : "" !!}
            {!! $coyaddr2 != "" ? "<br/>".$coyaddr2 : "" !!}
            {!! $coyaddr3 != "" ? "<br/>".$coyaddr3 : "" !!}
            {!! $coyaddr4 != "" ? "<br/>".$coyaddr4 : "" !!}

            {!! $coytel != "" ? "<br/>Tel No. : ".$coytel : "" !!}
            {!! $coyfax != "" ? "<br/>Fax No. : ".$coyfax : "" !!}
            {!! $coyemail != "" ? "<br/>Email : ".$coyemail : "" !!}
            {!! $coysstno != "" ? "<br/>SST No. : ".$coysstno : "" !!}
			</p>

			<p><span style="font-size:18px;"><strong>CASH BILL</strong></span></p>
		</div><!--End Info-->
	</center><!--End InvoiceTop-->

	<div id="mid">
		<div class="info-2">
			<p>
                Document No. : <strong>{{ $cashbillmt['DOC_NO'] }}</strong><br/>
                Date :{{ date("d/m/Y", strtotime($cashbillmt['DOC_DATE'])) }}<br/>
                {!! $cashbillmt['ACC_HOLDER'] != "" ? "<br/>Customer : ".$cashbillmt['ACC_HOLDER'] :  ""!!}

			</p>
		</div>
	</div><!--End Invoice Mid-->

	<div id="bot">

		<div id="table">
			<table>
				<tr class="tabletitle" id="header">
					<td class=""></td>
					<td class="item"><h2>Description</h2></td>
					<td class="text-right"><h2>Qty</h2></td>
					<td class="text-right"><h2>Price</h2></td>
					<td class="text-right"><h2>Amount</h2></td>
				</tr>
                @php
                    $no = 0;
                @endphp
                @foreach ($cashbilldt as $cb)
                @php
                    $rate = $cb->rate > 1 ? " x ".$cb->rate : "";
                @endphp
                    <tr class="service">
                        <td class="tableitem" valign='top'><p class="itemtext">{{ ++$no }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ $cb->description }}</p></td>
                        <td class="tableitem" valign='top'><p class="itemtext text-right">{{ $cb->quantity.$rate }}</p></td>
                        <td class="tableitem" valign='top'><p class="itemtext text-right">{{ $cb->unit_price }}</p></td>
                        <td class="tableitem" valign='top'><p class="itemtext text-right">{{ $cb->amount }}</p></td>
                    </tr>
                    @if ($cb->discount > 0)
                        <tr class="service">
                            <td><p class="itemtext">Discount :</p></td>
                            <td class="tableitem" colspan='4'><p class="itemtext">{{ $cb->discount }}</p></td>
                        </tr>
                    @endif

                @endforeach

				<tr class="" id="totalexcltax">
					<td class="itemtext2 text-right" colspan='4'> Sub Total </td>
					<td class="itemtext2 text-right">{{ $cashbillmt['SUBTOTAL'] }}</td>
                </tr>

                <tr class="" id="totalexcltax">
					<td class="itemtext2 text-right" colspan='4'> Discount </td>
					<td class="itemtext2 text-right">{{ $cashbillmt['DISCOUNT'] }}</td>
				</tr>

				<tr class="" id="roundingadj">
					<td class="itemtext2 text-right" colspan='4'>Rounding Adj.</td>
					<td class="itemtext2 text-right">{{ $cashbillmt['GRANDTOTAL'] }}</td>
				</tr>

				<tr class="" id="totamt">
					<td class="text-right" colspan='4'>Total</td>
					<td class="text-right">{{ $cashbillmt['GRANDTOTAL'] }}</td>
				</tr>

			</table>
		</div><!--End Table-->

		<div id="legalcopy">
			<p class="legal">
			 E. & O.E.<br/>
				***** Thank You *****<br/>
				Please Come Again.
			</p>
		</div>

	</div><!--End InvoiceBot-->
</div><!--End Invoice-->

<script>
print();
//close();
setTimeout(function () { close();}, 1000);
</script>

</body>
</html>
