<html>
<head>
    <link href="{{ asset('css/pdf-reporting.css') }}" rel="stylesheet">
</head>
@php ($i = 0) @endphp
<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Summary - Cash Bill {{ ($date) }}</p>
 
</div>
        <div class="bg-white">
            <div class="table-responsive">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%">
                    <thead class="" style="border:1px solid #095484;font-size: 12px;">
                        <tr>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;"> 
                                <p style="margin-top:3px;">Doc No</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                 <p style="margin-top:3px;">Date</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Debtor</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Name</p>
                            </th>
                            <th style="text-align:left; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Qty</p>
                            </th>
                            <th style="text-align:right; border-top:1px solid;border-bottom:1px solid;">
                                <p style="margin-top:3px;">Total Amt</p>
                            </th>
                        </tr>
                    </thead>

                @if(sizeof($cashbillJSON) > 0)
                    <tbody>
                            @php
                            $qty = 0;
                        @endphp
                        @foreach ($cashbillJSON as $cashbill)
                        {{-- {{ dd($cashbill) }} --}}
                            <tr>
                                {{-- <td style="font-size: 12px;">{{ ++$i }}</td>                                --}}
                                <td style="font-size: 12px;">{{ $cashbill->docno}}</td>
                                <td style="font-size: 12px;">{{ date('d-m-Y', strtotime($cashbill->date))}}</td>
                                {{-- <td>{{ $cashbill->lpono}}</td> --}}
                                <td style="font-size: 12px;">{{ $cashbill->debtor}}</td>
                                <td style="font-size: 12px;">{{ $cashbill->name}}</td>
                                @if($systemsetup == '1')
                                <td style="font-size: 12px;">{{ number_format($cashbill->totalqty, 2)}}</td>
                                @else
                                <td style="font-size: 12px;">{{ number_format($cashbill->totalqty)}}</td>
                                @endif
                                <td style="text-align:right;font-size: 12px;">{{ number_format($cashbill->totalamount, 2) }}</td>
                            </tr>
                            @php
                            $qty = $qty + $cashbill->totalqty;
                        @endphp
                            @endforeach
                            <tr>
                                {{-- <td></td> --}}
                                <td></td>
                                <td></td>
                                {{-- <td></td> --}}
                                <td></td>
                                <td></td>
                                @if($systemsetup == '1')
                                <td style="border-top:1px solid;border-bottom:1px solid;font-size: 12px;">{{ number_format($qty, 2) }}</td>
                                @else
                                <td style="border-top:1px solid;border-bottom:1px solid;font-size: 12px;">{{ number_format($qty) }}</td>
                                @endif
                                <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;font-size: 12px;">{{ number_format($finaltotalamount, 2) }}</td>
                            </tr>
                        </tbody>
                    @endif
                    {{-- @endif --}}
                </table>
            </div>
            {{-- @if(isset($cashbillJSON))
                {{ $cashbillJSON->links() }}
            @endif --}}
        </div>
</html>