@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('CASH_BILL_CR'))
    <div class="col-3" style="flex: 0 0 15%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('cashbills.create') }}"> Add Cash Bill</a>
        </div>
    </div>
    @endif
    {{-- @if(Auth::user()->hasPermissionTo('CASH_BILL_PR') && $print->isEmpty()) --}}
    @if(Auth::user()->hasPermissionTo('CASH_BILL_PR'))
    <div class="col-2" style="flex: 0 0 15%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @elseif(Auth::user()->hasPermissionTo('CASH_BILL_PRY'))
    <div class="col-2" style="flex: 0 0 15%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="cashbillTab" class="table table-bordered table-striped" cellspacing="0" style="font-size: 11px;">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Lpo No.</th>
                            <th>D/O No.</th>
                            <th>Quotation No.</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>

@include('dailypro.cashbills.printmodal')
{{-- {!! $cashbills->links() !!} --}}

@endsection

@section('modals')

    @include('dailypro.components.reportdocno-modal')
    @include('dailypro.components.reportdocnoto-modal')
    @include('dailypro.components.reportdebtor-modal')
    @include('dailypro.components.reportdebtorto-modal')
    @include('dailypro.components.reportsalesman-modal')
    @include('dailypro.components.reportsalesmanto-modal')

@endsection

@push('scripts')
<script>

    $('#cashbillTab').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                url: '{!! route('cashbill.datatable.data') !!}',
                dataType: 'json',
                type: 'GET',
                data:{ _token: '{{csrf_token()}}'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'docno', name: 'docno' },
                { data: 'date', name: 'date' },
                { data: 'account_code', name: 'account_code' },
                { data: 'name', name: 'name' },
                { data: 'lpono', name: 'lpono' },
                { data: 'dono', name: 'dono' },
                { data: 'quotno', name: 'quotno' },
                { data: 'taxed_amount', name: 'taxed_amount' },
                { data: 'more', name: 'more', orderable: false }
            ]
    });

    (function() {


        // $('#cashbillTab').DataTable();
        $('.select').select2();
        $('#dates').daterangepicker({
            linkedCalendars: false,
            locale: { format: 'DD/MM/YYYY' }
        });

        $(".docno_frm").change(function(){
            let docNo = $('.docno_frm').val();
            $('.docno_to').val(docNo);
        });

        $("#LPO_frm_1").change(function(){
            let LPO = $('#LPO_frm_1').val();
            $('#LPO_to_1').val(LPO);
        });

        $(".debCode_frm").change(function(){
            let debCode = $('.debCode_frm').val();
            $('.debCode_to').val(debCode);
        });

        $(".salesman_frm").change(function(){
            let salesman = $('.salesman_frm').val();
            $('.salesman_to').val(salesman);
        });

        $("#dates_Chkbx").change(function() {
            if(this.checked) {
                $("#dates").prop('disabled', false);
            }else{
                $("#dates").prop('disabled', true);
            }
        });

        if($("#dates_Chkbx").prop("checked") == true){
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }

        if($("#docno_Chkbx").prop("checked") == true){
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }

        if($("#LPO_Chkbx_1").prop("checked") == true){
            $("#LPO_frm_1").prop('disabled', false);
            $("#LPO_to_1").prop('disabled', false);
        }else{
            $("#LPO_frm_1").prop('disabled', true);
            $("#LPO_to_1").prop('disabled', true);
        }

        if($("#debCode_Chkbx").prop("checked") == true){
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }

        if($("#salesman_Chkbx").prop("checked") == true){
            $("#salesman_frm").prop('disabled', false);
            $("#salesman_to").prop('disabled', false);
        }else{
            $("#salesman_frm").prop('disabled', true);
            $("#salesman_to").prop('disabled', true);
        }

        $("#docno_Chkbx").change(function() {
            if(this.checked) {
                $("#docno_frm").prop('disabled', false);
                $("#docno_to").prop('disabled', false);
            }else{
                $("#docno_frm").prop('disabled', true);
                $("#docno_to").prop('disabled', true);
            }
        });

        $("#LPO_Chkbx_1").change(function() {
            if(this.checked) {
                $("#LPO_frm_1").prop('disabled', false);
                $("#LPO_to_1").prop('disabled', false);
            }else{
                $("#LPO_frm_1").prop('disabled', true);
                $("#LPO_to_1").prop('disabled', true);
            }
        });

        $("#refNo_Chkbx_2").change(function() {
            if(this.checked) {
                $("#refNo_frm_2").prop('disabled', false);
                $("#refNo_to_2").prop('disabled', false);
            }else{
                $("#refNo_frm_2").prop('disabled', true);
                $("#refNo_to_2").prop('disabled', true);
            }
        });

        $("#debCode_Chkbx").change(function() {
            if(this.checked) {
                $("#debCode_frm").prop('disabled', false);
                $("#debCode_to").prop('disabled', false);
            }else{
                $("#debCode_frm").prop('disabled', true);
                $("#debCode_to").prop('disabled', true);
            }
        });

        $("#salesman_Chkbx").change(function() {
            if(this.checked) {
                $("#salesman_frm").prop('disabled', false);
                $("#salesman_to").prop('disabled', false);
            }else{
                $("#salesman_frm").prop('disabled', true);
                $("#salesman_to").prop('disabled', true);
            }
        });

        $("#listing").on('click', function(){
            console.log('here')
            $('#stockitem').prop('disabled', false)
        })
        $("#summary").on('click', function(){
            console.log('there')
            $('#stockitem').prop('disabled', false)
        })

        $('#cashbillcodetable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('cashbill.docno.list.data') !!}',
            columns: [
                { data: 'docno', name: 'docno' },
                { data: 'date', name: 'date' },
                { data: 'name', name: 'name' }
            ],
        });

        $('#cashbillcodetable2').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('cashbill.docno.list.data') !!}',
            columns: [
                { data: 'docno', name: 'docno' },
                { data: 'date', name: 'date' },
                { data: 'name', name: 'name' }
            ],
        });

        $('#cashbilldebtortable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('cashbill.debtor.list.data') !!}',
            columns: [
                { data: 'accountcode', name: 'accountcode' },
                { data: 'name', name: 'name' }
            ],
        });

        $('#cashbilldebtortable2').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('cashbill.debtor.list.data') !!}',
            columns: [
                { data: 'accountcode', name: 'accountcode' },
                { data: 'name', name: 'name' }
            ],
        });

        $('#cashbillsalesmantable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('cashbill.salesman.list.data') !!}',
            columns: [
                { data: 'code', name: 'code' },
                { data: 'descr', name: 'descr' }
            ],
        });

        $('#cashbillsalesmantable2').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('cashbill.salesman.list.data') !!}',
            columns: [
                { data: 'code', name: 'code' },
                { data: 'descr', name: 'descr' }
            ],
        });

        $('.reportdocno-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdocno-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})
                let modal = $('.reportdocno-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.docno_frm').val(id);
                    $('.docno_to').val(id);

                    $('.reportdocno-modal').modal('hide');
                })
        })

        $('.reportdocnoto-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdocnoto-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportdocnoto-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.docno_to').val(id)

                    $('.reportdocnoto-modal').modal('hide');
                })

        })

        $('.reportdebtor-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdebtor-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportdebtor-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.debCode_frm').val(id);
                    $('.debCode_to').val(id);

                    $('.reportdebtor-modal').modal('hide');
                })

        })

        $('.reportdebtorto-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdebtorto-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportdebtorto-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.debCode_to').val(id)

                    $('.reportdebtorto-modal').modal('hide');
                })

        })

        $('.reportsalesman-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportsalesman-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportsalesman-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.salesman_frm').val(id);
                    $('.salesman_to').val(id);

                    $('.reportsalesman-modal').modal('hide');
                })

        })

        $('.reportsalesmanto-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportsalesmanto-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportsalesmanto-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.salesman_to').val(id)

                    $('.reportsalesmanto-modal').modal('hide');
                })
        })

        $(".Cashbill_rcv").change(function(){
            if(this.value != 'reprint'){
                $('.submit-btn').hide();
                $('.modal-btn').show();
            }else{
                $('.submit-btn').show();
                $('.modal-btn').hide();
            }
        });

    }) ()
</script>
@endpush