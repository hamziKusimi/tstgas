
<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="currency" class="col-md-3"> Currency </label>
    <div class="col-md-9">
        <input name="currency" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->currency:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="currate" class="col-md-3"> Currency Rate </label>
    <div class="col-md-9">
        <input name="currate" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->currate:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="packcg" class="col-md-3"> Packing Charges </label>
    <div class="col-md-9">
        <input name="packcg" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->packcg:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="handlingcg" class="col-md-3"> Handling Charges </label>
    <div class="col-md-9">
        <input name="handlingcg" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->handlingcg:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="fcg" class="col-md-3"> F.Charges </label>
    <div class="col-md-9">
        <input name="fcg" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->fcg:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="insurance" class="col-md-3"> Insurance </label>
    <div class="col-md-9">
        <input name="insurance" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->insurance:'' }}" >
    </div>
</div>

<div class="form-group row" style="margin: 6px;padding: 2px">
    <label for="forwarding" class="col-md-3"> Forwarding </label>
    <div class="col-md-9">
        <input name="forwarding" type="text" class="form-control" value="{{ isset($cashbill)?$cashbill->forwarding:'' }}" >
    </div>
</div>
