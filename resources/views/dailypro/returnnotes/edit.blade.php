@extends('master')

@section('page-form-open', Form::open(['url' => route('returnnotes.update', $item->id), 'method' => 'PUT']))
@section('header')
<br>
@include('common.ajax-message')
<br>
@endsection

@section('content')
@include('dailypro.returnnotes.options', ['edit' => true])
<br>
@include('dailypro.components.system-param',[
'systemParam' => [
'data-rounding' => 'false',
'data-use-tax' => 'false',
'data-date' => isset($item) ? $item->date : '',
'data-due-date' => isset($item) ? $item->m_duedate : '',
'data-acode' => isset($item) ? $item->account_code: '',
'data-acode-name' => isset($item) ? $item->name : '',
]
])

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif

<div class="card">
    <div class="card-body">
        <div class="pl-2 pr-2">
            <div class="row">
                <div class="col-md-5">
                    @include('dailypro.components.bills.left-panel')
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    @include('dailypro.components.bills.right-panel-returnnotes', [
                    'getDocsApiRoute' => route('returnnotes.api.get'),
                    'except' => [
                    'refInvDate', 'referenceNo', 'amount'
                    ],
                    ])
                </div>
            </div>
        </div>
        <div class="mt-3"></div>
        <h5>Cylinder</h5>
        @include('dailypro.components.table.returnnotes.table')

        <div class="mt-3"></div>
        <h5>Gas Rack</h5>
        @include('dailypro.components.table.returnnotes.gasrack.table')

    </div>
</div>
</div>
<div class="mt-3"></div>
<div class="row">
    <div class="col-md-6">
        <div class="box ">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5> Loading List </h5>
                        </div>
                        @include('dailypro.returnnotes.loadinglist')
                    </div>
                </div>
            </div>
            <div class="overlay loading-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box ">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5> Unloading List </h5>
                        </div>
                        @include('dailypro.returnnotes.unloadinglist')
                    </div>
                </div>
            </div>
            <div class="overlay unloading-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<div class="mt-3"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.extras')

            </div>
        </div>
    </div>
</div>

@endsection
@section('page-form-close', Form::close())

@section('modals')
@include('dailypro.components.item-modal')
@include('dailypro.components.template-master-modal')
@include('dailypro.returnnotes.printmodal2')
@include('dailypro.components.account-modal', [
'removeDebtorFinder' => true
])
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script>

@include('common.form.inputmask')
@include('common.form.summernote-sm')

<script type="text/javascript">
    (function() {
    @include('dailypro.components.js.debtor-select-js')

    // enable the button if detected the account code has value
    if ($('#debtor-select').val())
        $('.btn-form-submit').prop('disabled', false)

    // prevent # from jumping to top of the screen
    $('a').on('click', function(e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
        }
    })

    $('.overlay').hide();

    let preparecylinder = false
    let loadcylinder = false
    let unloadcylinder = false
    let triggerexist = false
    let triggernotexist = false
    let grtriggerexist = false
    let grtriggernotexist = false
    let cylinderexist = false
    let barcodemanyfor = ''
    let serialmanyfor = ''
    let categorymanyfor = ''
    let productmanyfor = ''
    let descrmanyfor = ''
    let capacitymanyfor = ''
    let pressuremanyfor = ''

    // returnnote date and due date
    // deliverynote date and due date
    let date = moment.utc(new Date($('.system-settings-param').data('date')))
    let dueDate = moment.utc(new Date($('.system-settings-param').data('startfrom')))
    $('#date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker1').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker2').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker3').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#startfrom-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })

    let postToAccount = 'creditSalesAcc'
    @include('common.billing.js.rn-table-js')
    @include('common.billing.js.rn-table-gasrack-js')
    @include('common.billing.js.rn-cylinder-nested-loadinglist-table-js')
    @include('common.billing.js.rn-gasrack-nested-loadinglist-table-js')
    @include('common.billing.js.left-panel-js')
    @include('common.billing.js.right-panel-js')

}) ()
</script>
@endpush