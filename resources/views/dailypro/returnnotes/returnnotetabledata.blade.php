
@if(isset($returnnotedts))
    @foreach ($returnnotedts as $returnnotedt)    
    @php ($i = 0)                 
    <tr class="sortable-row existing-data">
                
        <td class="text-left" style="padding:1px;" ><input type="number" name="sn[]" id="sn" value="{{ isset($returnnotedt)?$returnnotedt->sn:'' }}" class="form-control input-sm sn"></td>
        <td class="text-left" style="padding:1px;">
            <select class="form-control input-sm stockcode2" style="width:100%" name="stockcode[]" id="stockcode2" data-values="{{ $stockcodes }}"> 
            <option value=""></option>
            @foreach($stockcodes as $stockcode)
                <option value="{{$stockcode->id}}" {{ isset($returnnotedt)?$returnnotedt->stockcode_id == $stockcode->id  ? 'selected' : '' : ''}}>{{$stockcode->code}}</option>
            @endforeach
            </select>
        </td>
        <input type="hidden" name="stockcode_dt[]" id="stockcode_dt" value="{{  isset($returnnotedt)?$returnnotedt->stockcode_id:'null' }} " class="form-control stockcode_dt">
        <input type="hidden" name="returnnotedts_id[]" value="{{  isset($returnnotedt)?$returnnotedt->id:null }} "/>
        <input type="hidden" name="returnnote_id[]" value="{{ $i++ }} "/>
        <td class="text-left" style="padding:1px;" ><input type="text" name="descr[]" id="descr" value="{{ isset($returnnotedt)?$returnnotedt->descr:'' }}" class="form-control input-sm descr"></td>
        <td class="text-left" style="padding:1px;"><input type="text" id="qty" name="qty[]" value="{{ isset($returnnotedt)?number_format($returnnotedt->qty, 2):'1.00' }}" class="form-control calculate-field2 input-sm qty"></td>
        <td class="text-left" style="padding:1px;" >
            <select class="form-control input-sm uom2" style="width:100%" name="uom[]" id="uom" data-values="{{ $uoms }}"> 
                <option value=""></option>
                @foreach($uoms as $uom)
                    <option value="{{$uom->id}}" {{ isset($returnnotedt)?$returnnotedt->uom == $uom->id  ? 'selected' : '' : ''}}>{{$uom->code}}</option>
                @endforeach
            </select>
            {{-- <input type="text" name="uom[]" id="uom" value="{{ isset($returnnotedt)?$returnnotedt->uom:'' }}" class="form-control input-sm uom"> --}}
        </td>
        <td class="text-left" style="padding:1px;"><input type="text" id="rate" name="rate[]" value="{{ isset($returnnotedt)?number_format($returnnotedt->rate, 2):'1.00' }}" class="form-control calculate-field2 input-sm rate"></td>
        <td class="text-left" style="padding:1px;"><input type="text" id="uprice" name="uprice[]"value="{{ isset($returnnotedt)?number_format($returnnotedt->uprice, 2):'0.00' }}" class="form-control calculate-field2 input-sm uprice"></td>
       <td class="text-left" style="padding:1px;"><input type="text" id="amount" name="amount[]" value="{{ isset($returnnotedt)?number_format($returnnotedt->amount, 2):'0.00' }}" class="form-control calculate-field2 input-sm amount"></td>
        <td class="text-left" style="padding:1px;">
            <a href="{{ route('returnnotes.destroyReturnnotedt', $returnnotedt->id) }}" data-method="delete" data-confirm="Confirm delete this stock?"  type="button" class="btn btn-danger">
            <small><i class ="fa fa-remove"></i></small>
        </td>
    </tr>
    @endforeach        
@endif