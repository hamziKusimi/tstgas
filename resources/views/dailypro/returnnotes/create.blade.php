@extends('master')

@section('page-form-open', Form::open(['url' => route('returnnotes.store'), 'method' => 'POST']))
@section('header')
<br>
@include('common.ajax-message')
<br>
@endsection

@section('content')

@include('common.ajax-message')

@include('dailypro.returnnotes.options')
<br>
@include('dailypro.components.system-param',[
'systemParam' => [
'data-rounding' => 'false',
'data-use-tax' => 'false',
]
])

<div class="card">
    <div class="card-body">
        <div class="pl-2 pr-2">
            <div class="row">
                <div class="col-md-5">
                    @include('dailypro.components.bills.left-panel')
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    @include('dailypro.components.bills.right-panel-returnnotes', [
                    'getDocsApiRoute' => route('returnnotes.api.get'),
                    'except' => [
                    'refInvDate', 'referenceNo', 'amount'
                    ],
                    ])
                </div>
            </div>
        </div>
        <div class="mt-3"></div>
        <h5>Cylinder</h5>
        @include('dailypro.components.table.returnnotes.table')

        <div class="mt-3"></div>
        <h5>Gas Rack</h5>
        @include('dailypro.components.table.returnnotes.gasrack.table')

    </div>
</div>
</div>
<div class="mt-3"></div>
<div class="row">
    <div class="col-md-6">
        <div class="box ">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5> Loading List </h5>
                        </div>
                        @include('dailypro.returnnotes.loadinglist')
                    </div>
                </div>
            </div>
            <div class="overlay loading-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box ">
            <div class="box-body">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5> Unloading List </h5>
                        </div>
                        @include('dailypro.returnnotes.unloadinglist')
                    </div>
                </div>
            </div>
            <div class="overlay unloading-overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<div class="mt-3"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                @include('dailypro.components.bills.extras')
                
            </div>
        </div>
    </div>
</div>

@endsection
@section('page-form-close', Form::close())

@section('modals')
@include('dailypro.components.item-modal')
@include('dailypro.components.template-master-modal')
@include('dailypro.components.account-modal', [
'removeDebtorFinder' => true
])
@endsection

@push('scripts')
<script src="{{ asset('js/summernote-bs4.js') }}"></script>
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
{{-- <script src="{{ asset('js/jquery-ui-sortable.min.js') }}"></script> --}}

@include('common.form.inputmask')
@include('common.form.summernote-sm')

<script type="text/javascript">
    (function() {
    @include('dailypro.components.js.debtor-select-js')

    // prevent # from jumping to top of the screen
    $('a').on('click', function(e) {
        if ($(this).attr('href') == '#') e.preventDefault()
    })

    $('.overlay').hide();
    
    let preparecylinder = false
    let loadcylinder = false
    let unloadcylinder = false
    let triggerexist = false
    let triggernotexist = false
    let grtriggerexist = false
    let grtriggernotexist = false
    let cylinderexist = false
    let triggerformany = false
    let barcodemanyfor = ''
    let serialmanyfor = ''
    let categorymanyfor = ''
    let productmanyfor = ''
    let descrmanyfor = ''
    let capacitymanyfor = ''
    let pressuremanyfor = ''
    
    let rowexist = false
    let rowExistinCylinder = false
    let triggercylinder = false
    let count = 1


    // invoice date and due date
    let date = moment(new Date())
    $('#date-picker').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker1').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker2').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#date-picker3').datetimepicker({ date: date, format: 'DD/MM/YYYY' })
    $('#startfrom-picker').datetimepicker({ format: 'DD/MM/YYYY' })

    let postToAccount = 'creditSalesAcc'
    @include('common.billing.js.rn-table-js')
    @include('common.billing.js.rn-table-gasrack-js')
    @include('common.billing.js.rn-cylinder-nested-loadinglist-table-js')
    @include('common.billing.js.rn-gasrack-nested-loadinglist-table-js')
    @include('common.billing.js.left-panel-js')
    @include('common.billing.js.right-panel-js')

}) ()
</script>
@endpush