<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('datell', 'Date', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0" id="date-picker3" data-target-input="nearest">
                    {!! Form::text('datell', isset($item) ? $item->date : null, [
                    'class' => 'form-control invoice-date datetimepicker-input',
                    'placeholder' => '',
                    'data-target' => '#date-picker3'
                    ]) !!}
                    <div class="input-group-append" data-target="#date-picker3" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('barcodell', 'Barcode', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('barcodell', isset($item) ? $item->barcodell : null, [
                    'class' => 'form-control validate-barcode barcodell',
                    'id' => 'barcodell',
                    'data-item'=> route('cylinders.search.filter', 'ACC_CODE'),
                    'data-item2'=> route('cylinders.search.rnfilter', 'ACC_CODE'),
                    'data-item3'=> route('gasracks.search.filter', 'ACC_CODE'),
                    'data-item4'=> route('gasracks.search.rnfilter', 'ACC_CODE'),
                    'data-gs'=> $gsbarcodes,
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('serialll', 'Serial No', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('serialll', isset($item) ? $item->serialll : null, [
                    'class' => 'form-control serialll',
                    'id' => 'serialll',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('driverll', 'Driver', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="input-group col-md-11 pr-0">
                    {!! Form::text('driverll', isset($item) ? $item->driver : null, [
                    'class' => 'form-control driverll',
                    'id' => 'driverll',
                    'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="narrow-padding-global">
    <div class="row">
        {!! Form::label('', '', ['class' => 'col-form-label col-md-3 text-center']) !!}
        <div class="col-md-9">
            <div class="row">

                <div class="input-group col-md-5 pr-0">
                    <button type="button" class="form-control btn-primary text-white" style="width:70px" id="load-cyl">
                        Load Cylinder
                    </button>
                </div>
                <div class="input-group col-md-5 pr-0">
                    <button type="button" class="form-control btn-primary text-white" style="width:70px" id="load-gas">
                        Load Gasrack
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>