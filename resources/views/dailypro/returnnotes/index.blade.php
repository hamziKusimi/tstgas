@extends('master')

@section('content')

@if(Auth::user()->hasPermissionTo('RETURN_NT_CR'))
<div class="row">
    <div class="col-3" style="flex: 0 0 16%;padding-right: 5px;">
        <div  style="margin-bottom: 10px;">
            <a class="btn btn-success"  style="width: 100%;" href="{{ route('returnnotes.create') }}"> Add Return Notes</a>
        </div>
    </div>
    <div class="col-2" style="padding-left: 0px;">
        <div  style="margin-bottom: 10px;">
            <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
        </div>
    </div>
</div>
@endif

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>

@endif
@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="RNTab" class="table table-bordered table-striped" cellspacing="0" ">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Lpo No</th>
                            <th>Driver</th>
                            <th>Charge Type</th>
                            <th>Start From</th>
                            <th>More</th>
                        </tr>
                    </thead>
                @if(sizeof($returnnotes) > 0)    
                    <tbody> 
                        @foreach ($returnnotes as $returnnote)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>
                                @if(Auth::user()->hasPermissionTo('RETURN_NT_UP'))
                                    <a href=" {{ route('returnnotes.edit',$returnnote->id) }}">
                            {{ $returnnote->dn_no }}
                            </a>
                            @else
                            {{ $returnnote->dn_no }}
                            @endif
                            </td>
                            <td>{{  isset($returnnote->date)?date('d-m-Y', strtotime($returnnote->date)):'' }}</td>
                            <td>{{ $returnnote->account_code}}</td>
                            <td>{{ $returnnote->name }}</td>
                            <td>{{ $returnnote->lpono }}</td>
                            <td>{{ $returnnote->driver }}</td>
                            <td>{{ $returnnote->charge_type }}</td>
                            <td>{{ $returnnote->start_from }}</td>
                            <td>
                                @if(Auth::user()->hasPermissionTo('RETURN_NT_DL'))
                                <a href="{{ route('returnnotes.destroy', $returnnote->id) }}" data-method="delete"
                                    data-confirm="Confirm delete this account?">
                                    <i class="fa fa-trash"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@include('dailypro.returnnotes.printmodal')
@endsection


@push('scripts')
<script>
    (function() {
        
    $('#RNTab').DataTable();
    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#LPO_Chkbx_1").prop("checked") == true){
        $("#LPO_frm_1").prop('disabled', false);
        $("#LPO_to_1").prop('disabled', false);
    }else{
        $("#LPO_frm_1").prop('disabled', true);
        $("#LPO_to_1").prop('disabled', true);
    }

    if($("#debCode_Chkbx").prop("checked") == true){
        $("#debCode_frm").prop('disabled', false);
        $("#debCode_to").prop('disabled', false);
    }else{
        $("#debCode_frm").prop('disabled', true);
        $("#debCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#LPO_Chkbx_1").change(function() {
        if(this.checked) {
            $("#LPO_frm_1").prop('disabled', false);
            $("#LPO_to_1").prop('disabled', false);
        }else{
            $("#LPO_frm_1").prop('disabled', true);
            $("#LPO_to_1").prop('disabled', true);
        }
    });

    $("#debCode_Chkbx").change(function() {
        if(this.checked) {
            $("#debCode_frm").prop('disabled', false);
            $("#debCode_to").prop('disabled', false);
        }else{
            $("#debCode_frm").prop('disabled', true);
            $("#debCode_to").prop('disabled', true);
        }
    });
}) ()
</script>
@endpush