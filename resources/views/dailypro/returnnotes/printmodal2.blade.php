<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('deliverynotes.print') }}" method="GET" target="_blank">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Return Note Printing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <p><b>Filter by :</b></p>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pl-1 pr-1">
                            <a style="width: 100%;" type="button" class="btn btn-primary"
                                href="{{ route('returnnotes.jasper', ['id' => $item->id, 'type'=>'cyl']) }}"
                                target="_blank"><i class="fa fa-print pr-2"></i>Cylinders/Rack</a>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>

</div>