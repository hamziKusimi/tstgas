
<tr class="template-new d-none">
    <td class="text-left" style="padding:1px;"><input type="number" id="sn" name="sn[]" class="form-control input-sm sn"></td>
    <td class="text-left" style="padding:1px;">
        <select class="form-control calculate-field2 input-sm stockcode" style="width:100%" id="stockcode" name="stockcode[]" data-values="{{ $stockcodes }}"> 
        <option value=""></option>
        @foreach($stockcodes as $stockcode)
            <option value="{{$stockcode->id}}" {{ isset($returnnote)?$returnnote->stockcode_id == $stockcode->id  ? 'selected' : '' : ''}}>{{$stockcode->code}}</option>
        @endforeach
        </select>
        <input type="hidden" name="stockcode_dt[]" id="stockcode_dt" class="form-control stockcode_dt">
    </td>
    <input type="hidden" name="returnnotedts_id[]" value="{{  isset($returnnotedt)?$returnnotedt->id:null }} "/>
    <td class="text-left" style="padding:1px;"><input type="text" id="descr" name="descr[]" class="form-control input-sm descr"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="qty" name="qty[]" value="{{ isset($returnnotedt)?number_format($returnnotedt->qty, 2):'1.00' }}" class="form-control calculate-field2 input-sm qty"></td>
    <td class="text-left" style="padding:1px;">
        <select class="form-control input-sm uom" style="width:100%" id="uom" name="uom[]" data-values="{{ $uoms }}"> 
            <option value=""></option>
            @foreach($uoms as $uom)
                <option value="{{$uom->id}}" {{ isset($returnnote)?$returnnote->uom == $uom->id  ? 'selected' : '' : ''}}>{{$uom->code}}</option>
            @endforeach
        </select>
        {{-- <input type="text" id="uom" name="uom[]" class="form-control calculate-field2 input-sm uom"> --}}
    </td>
    <td class="text-left" style="padding:1px;"><input type="text" id="rate" name="rate[]" value="{{ isset($returnnotedt)?number_format($returnnotedt->rate, 2):'1.00' }}" class="form-control calculate-field2 input-sm rate"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="uprice" name="uprice[]"value="{{ isset($returnnotedt)?number_format($returnnotedt->uprice, 2):'0.00' }}" class="form-control calculate-field2 input-sm uprice"></td>
    <td class="text-left" style="padding:1px;"><input type="text" id="amount" name="amount[]" value="{{ isset($returnnotedt)?number_format($returnnotedt->amount, 2):'0.00' }}" class="form-control calculate-field2 input-sm amount"></td>
    <td class="text-left" style="padding:1px;">
        {{-- <a href="#" onclick="ClearFields()" data-confirm="Confirm delete this account?"  type="button" class="btn btn-danger clear"> --}}
        {{-- <small><i class ="fa fa-remove"></i></small> --}}
    </td>
</tr>  


