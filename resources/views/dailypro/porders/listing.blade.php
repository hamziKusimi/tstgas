<div class="row">
        <p style="text-align:center; font-size:18px; font-weight:bold;">{{ config('config.company.name') }}</p>
        @if (config('config.company.show_company_no') == 'true')
            <p style="text-align:center; font-size:16px;">{{ config('config.company.company_no') }}</p>
        @endif
        <p style="text-align:center; font-size:16px; font-weight:bold;">Transaction Listing - Purchase Order  {{ ($date) }}</p>
        <br>


    </div>
    @php
    ($i = 0)
    @endphp
    <div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="row">
                <table id="cashbillTab" class="table" cellspacing="0" style="width:100%;font-size: 12px;">
                    <thead class="" border="1">
                        <tr>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">SN</th>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">Description</th>
                            <th style="text-align:left;border-top:1px solid;border-bottom:1px solid;">Location</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">Quantity</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">U.O.M</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">Unit Cost</th>
                            <th style="text-align:right;border-top:1px solid;border-bottom:1px solid;">Amount</th>
                        </tr>
                    </thead>

                @php $docno = "" @endphp
                @if(sizeof($PurchaseOrderJSON) > 0)
                    <tbody>
                        @foreach ($PurchaseOrderJSON as $porder)
                            <!-- check last updated doc no if not same with new updated then display -->
                            @if($docno != $porder->docno)

                                <thead class="">
                                    <tr>
                                        <th></th>
                                        <th colspan="6" style="text-align:left;">{{ $porder->details }}</th>
                                    </tr>
                                </thead>
                            @endif
                                <tbody>
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $porder->description}}</td>
                                        <td>{{ $porder->location}}</td>
                                        @if($systemsetup == '1')
                                        <td style="text-align:right;">{{ number_format($porder->quantity, 2)}}</td>
                                        @else
                                        <td style="text-align:right;">{{ number_format($porder->quantity)}}</td>
                                        @endif
                                        <td style="text-align: right">{{ $porder->uom}}</td>
                                        <td style="text-align: right">{{ number_format($porder->ucost, 2)}}</td>
                                        <td style="text-align: right">{{ number_format($porder->amount, 2)}}</td>
                                    </tr>
                                </tbody>
                                <!-- count the row for each doc no -->
                                @php
                                    $count = 0;
                                    $co = DB::select(DB::raw("
                                                SELECT count(porderdts.doc_no) as count from porderdts where porderdts.doc_no = :docno and porderdts.deleted_at IS NULL
                                        "), [
                                            'docno'=>$porder->docno,
                                        ]);
                                        foreach($co as $c){
                                            $count = $count + $c->count;
                                        }
                                @endphp

                                @php
                                $qty = 0;
                                $id = '';
                                $inv = DB::select(DB::raw("
                                            SELECT porderdts.id, porderdts.qty from porderdts where porderdts.doc_no = :docno and porderdts.deleted_at IS NULL GROUP BY porderdts.doc_no, porderdts.item_code
                                    "), [
                                        'docno'=>$porder->docno,
                                    ]);
                                    foreach($inv as $invc){
                                        //get total quantity for each cashbills doc no
                                        $qty = $qty + $invc->qty;
                                        //get last id
                                        $id = $invc->id;
                                    }
                                @endphp
                                <!-- check last id then display -->
                                @if($porder->id == $id)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        @if($systemsetup == '1')
                                        <td style="border-top:1px solid;text-align:right;">{{ number_format($qty, 2) }}</td>
                                        @else
                                        <td style="border-top:1px solid;text-align:right;">{{ number_format($qty) }}</td>
                                        @endif
                                        <td style="border-top:1px solid;text-align:right;"></td>
                                        <td style="border-top:1px solid;text-align:right;"></td>
                                        <td style="border-top:1px solid;text-align:right;">{{ number_format($porder->totalamt, 2) }}</td>
                                    </tr>
                                <!-- else if row have only 1 data also display -->
                                {{-- @elseif($count <= '1')
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-top:1px solid;">{{ number_format($porder->quantity, 2) }}</td>
                                        <td style="border-top:1px solid;"></td>
                                        <td style="border-top:1px solid;"></td>
                                        <td style="border-top:1px solid;">{{ number_format($porder->totalamt, 2) }}</td>
                                    </tr> --}}
                                @endif
                            @php
                                $docno = $porder->docno;
                            @endphp
                        @endforeach
                        <!-- Display Grand Total -->
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;"></td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">Grand Total</td>
                            <td style="border-top:1px solid;border-bottom:1px solid;text-align:right;">{{ number_format($finaltotalamount, 2) }}</td>
                        </tr>
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
    </div>


