@extends('master')

@section('content')

<div class="row">
    @if(Auth::user()->hasPermissionTo('PURCHASE_OD_CR'))
    <div class="col-3" style="flex: 0 0 18%;padding-right: 5px;">
        <div style="margin-bottom: 10px;">
            <a class="btn btn-success" style="width: 100%;" href="{{ route('porders.create') }}"> Add Purchase Order</a>
        </div>
    </div>
    @endif
    @if(Auth::user()->hasPermissionTo('PURCHASE_OD_PR') && $print->isEmpty())
        <div class="col-2" style="flex: 0 0 18%;padding-right: 5px; ">
            <div style="margin-bottom: 10px;padding-left: 0px;">
                    <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                        data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
            </div>
        </div>
    @elseif(Auth::user()->hasPermissionTo('PURCHASE_OD_PRY'))
        <div class="col-2" style="flex: 0 0 18%;padding-right: 5px; ">
            <div style="margin-bottom: 10px;padding-left: 0px;">
                    <button class="btn btn-primary" target="_blank" style="width: 100%;" data-toggle="modal"
                        data-target="#exampleModal"><i class="fa fa-print pr-2"></i>Print</button>
            </div>
        </div>
    @endif
</div>

@if (Session::has('Success'))
<div class="alert white-alert text-secondary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('Success') }}</p>
</div>
@endif

@php ($i = 0)

<div class="card">
    <div class="card-body">
        <div class="bg-white">
            <div class="table-responsive">
                <table id="porderTab" class="table table-bordered table-striped" cellspacing="0" style="font-size: 11px;">
                    <thead class="">
                        <tr>
                            <th>SN</th>
                            <th>Document No</th>
                            <th>Date</th>
                            <th>Supplier</th>
                            <th>Name</th>
                            <th>Supp. D/O</th>
                            <th>Supp. Inv</th>
                            <th>Ref. No</th>
                            <th>Purchase Type</th>
                            <th>Total Amt</th>
                            <th>More</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@include('dailypro.porders.printmodal')
@endsection

@section('modals')

    @include('dailypro.components.reportdocno-modal')
    @include('dailypro.components.reportdocnoto-modal')
    @include('dailypro.components.reportdebtor-modal')
    @include('dailypro.components.reportdebtorto-modal')

@endsection

@push('scripts')
<script>

    $('#porderTab').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: '{!! route('porder.datatable.data') !!}',
            dataType: 'json',
            type: 'GET',
            data:{ _token: '{{csrf_token()}}'}
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'docno', name: 'docno' },
            { data: 'date', name: 'date' },
            { data: 'account_code', name: 'account_code' },
            { data: 'name', name: 'name' },
            { data: 'suppdo', name: 'suppdo' },
            { data: 'suppinv', name: 'suppinv' },
            { data: 'ref', name: 'ref' },
            { data: 'ptype', name: 'ptype' },
            { data: 'taxed_amount', name: 'taxed_amount' },
            { data: 'more', name: 'more', orderable: false }
        ]
    });

    (function() {

    $('.select').select2();
    $('#dates').daterangepicker({
        linkedCalendars: false,
        locale: { format: 'DD/MM/YYYY' }
    });

    $("#dates_Chkbx").change(function() {
        if(this.checked) {
            $("#dates").prop('disabled', false);
        }else{
            $("#dates").prop('disabled', true);
        }
    });

    if($("#dates_Chkbx").prop("checked") == true){
        $("#dates").prop('disabled', false);
    }else{
        $("#dates").prop('disabled', true);
    }

    if($("#docno_Chkbx").prop("checked") == true){
        $("#docno_frm").prop('disabled', false);
        $("#docno_to").prop('disabled', false);
    }else{
        $("#docno_frm").prop('disabled', true);
        $("#docno_to").prop('disabled', true);
    }

    if($("#suppDo_Chkbx").prop("checked") == true){
        $("#suppDo_frm").prop('disabled', false);
        $("#suppDo_to").prop('disabled', false);
    }else{
        $("#suppDo_frm").prop('disabled', true);
        $("#suppDo_to").prop('disabled', true);
    }

    if($("#suppInv_Chkbx").prop("checked") == true){
        $("#suppInv_frm").prop('disabled', false);
        $("#suppInv_to").prop('disabled', false);
    }else{
        $("#suppInv_frm").prop('disabled', true);
        $("#suppInv_to").prop('disabled', true);
    }

    if($("#refNo_Chkbx").prop("checked") == true){
        $("#refNo_frm").prop('disabled', false);
        $("#refNo_to").prop('disabled', false);
    }else{
        $("#refNo_frm").prop('disabled', true);
        $("#refNo_to").prop('disabled', true);
    }

    if($("#credCode_Chkbx").prop("checked") == true){
        $("#credCode_frm").prop('disabled', false);
        $("#credCode_to").prop('disabled', false);
    }else{
        $("#credCode_frm").prop('disabled', true);
        $("#credCode_to").prop('disabled', true);
    }

    $("#docno_Chkbx").change(function() {
        if(this.checked) {
            $("#docno_frm").prop('disabled', false);
            $("#docno_to").prop('disabled', false);
        }else{
            $("#docno_frm").prop('disabled', true);
            $("#docno_to").prop('disabled', true);
        }
    });

    $("#suppDo_Chkbx").change(function() {
        if(this.checked) {
            $("#suppDo_frm").prop('disabled', false);
            $("#suppDo_to").prop('disabled', false);
        }else{
            $("#suppDo_frm").prop('disabled', true);
            $("#suppDo_to").prop('disabled', true);
        }
    });

    $("#suppInv_Chkbx").change(function() {
        if(this.checked) {
            $("#suppInv_frm").prop('disabled', false);
            $("#suppInv_to").prop('disabled', false);
        }else{
            $("#suppInv_frm").prop('disabled', true);
            $("#suppInv_to").prop('disabled', true);
        }
    });

    $("#refNo_Chkbx").change(function() {
        if(this.checked) {
            $("#refNo_frm").prop('disabled', false);
            $("#refNo_to").prop('disabled', false);
        }else{
            $("#refNo_frm").prop('disabled', true);
            $("#refNo_to").prop('disabled', true);
        }
    });

    $("#credCode_Chkbx").change(function() {
        if(this.checked) {
            $("#credCode_frm").prop('disabled', false);
            $("#credCode_to").prop('disabled', false);
        }else{
            $("#credCode_frm").prop('disabled', true);
            $("#credCode_to").prop('disabled', true);
        }
    });


    $('#cashbillcodetable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('porder.docno.list.data') !!}',
            columns: [
                { data: 'docno', name: 'docno' },
                { data: 'date', name: 'date' },
                { data: 'name', name: 'name' }
            ],
        });

        $('#cashbillcodetable2').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('porder.docno.list.data') !!}',
            columns: [
                { data: 'docno', name: 'docno' },
                { data: 'date', name: 'date' },
                { data: 'name', name: 'name' }
            ],
        });

        $('#cashbilldebtortable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('porder.debtor.list.data') !!}',
            columns: [
                { data: 'accountcode', name: 'accountcode' },
                { data: 'name', name: 'name' }
            ],
        });

        $('#cashbilldebtortable2').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('porder.debtor.list.data') !!}',
            columns: [
                { data: 'accountcode', name: 'accountcode' },
                { data: 'name', name: 'name' }
            ],
        });

        $('.reportdocno-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdocno-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})
                let modal = $('.reportdocno-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.docno_frm').val(id)

                    $('.reportdocno-modal').modal('hide');
                })
        })

        $('.reportdocnoto-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdocnoto-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportdocnoto-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.docno_to').val(id)

                    $('.reportdocnoto-modal').modal('hide');
                })

        })

        $('.reportdebtor-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdebtor-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportdebtor-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.credCode_frm').val(id)

                    $('.reportdebtor-modal').modal('hide');
                })

        })

        $('.reportdebtorto-modal').on('shown.bs.modal', function() {

            var rowmodal = $('.reportdebtorto-modal').find('tbody')
            var row = rowmodal.find('tr')
            row.attr({'data-id':'id', 'id':'route2'})

                let modal = $('.reportdebtorto-modal')
                let firstRow = $(this).parents('tr')
                modal.on('dblclick', 'tr', function() {
                    let id = $(this).find('td:eq(0)').html();
                    $('.credCode_to').val(id)

                    $('.reportdebtorto-modal').modal('hide');
                })

        })


}) ()
</script>
@endpush