
<div id="success-message" class="alert white-alert text-secondary alert-dismissible mt-2 mb-0 d-none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5 class="mb-0">
        <span class="custom-message">Success</span>
    </h5>
</div>
<div id="failure-message" class="alert red-alert text-secondary alert-dismissible mt-2 mb-0 d-none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5 class="mb-0">
        <span class="custom-message">Failed</span>
    </h5>
</div>
