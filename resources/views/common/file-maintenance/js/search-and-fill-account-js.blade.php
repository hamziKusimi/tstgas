$('.search-debtor').on('click', function () {
    let modal = $('.search-debtor-modal-template').clone().removeClass('search-debtor-modal-template')
    let getRoute = modal.data('get-route')
    let editRoute = modal.data('edit-route')
    let table = modal.find('table')
    // show modal
    modal.modal()
    modal.on('hidden.bs.modal', function (e) { $(this).remove() })

    // should show processing while waiting for the data to be loaded

    // populate table and reload Datatable
    $.ajax({
        url: getRoute,
        method: 'GET',
    }).done(function (resources) {
        resources.forEach(resource => {
            let resourceEditRoute = editRoute.replace('id', resource.ID)
            let markup = ''
            if (resource.D_DC) {
                markup = `
                <tr class="pointer" data-choice="${resource.accountCode}" data-id="${resource.ID}" data-type="D"
                    data-edit-route="${resourceEditRoute}">
                    <td class="acode" width="25%">${resource.accountCode}</td>
                    <td class="name" width="75%">${resource.D_NAME}</td>
                </tr>
                `
            } else if (resource.C_DC) {
                markup = `
                <tr class="pointer" data-choice="${resource.accountCode}" data-id="${resource.ID}" data-type="C"
                    data-edit-route="${resourceEditRoute}">
                    <td class="acode" width="25%">${resource.accountCode}</td>
                    <td class="name" width="75%">${resource.C_NAME}</td>
                </tr>
                `
            }
            table.find('tbody').append(markup)
        });

        // init datatable
        table.DataTable()
        table.on('click', 'tr', function () {
            // $('.acode-input').val($(this).data('choice')).trigger('change')
            window.location = $(this).data('edit-route')
            modal.modal('hide')
        })
    })
})
