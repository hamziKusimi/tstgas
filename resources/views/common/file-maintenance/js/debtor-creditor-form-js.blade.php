// get existing accounts, except this account - to allow edit
let existingAccounts = $('.acode-input').data('existing-accounts')

// response when validated or exist
let acodeHandler = function (message, setButton) {
    $('.d-acode-message').text(message)
    $('.submit').prop('disabled', setButton)
}

// handle situation if input is validated, exist, and vice versa
$('.acode-input').on('keyup change', function () {
    let validated = validateInput($(this).val())                    // validate DA000-00
    let exist = checkAccountExist($(this).val(), existingAccounts)  // check if account exist

    if (exist) acodeHandler('This code is already used in another account', true)
    if (!validated) acodeHandler('Error, please follow the following format: DAXXX', true)
    if (!exist && validated) acodeHandler('', false)
})
