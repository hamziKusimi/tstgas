// do validation on string
let validateInput = function (input) {
    let regex = new RegExp('^([A-Z0-9]{5})$');
    return input.match(regex) ? true : false
}

// do existing check
let checkAccountExist = function (input, accounts) {
    return ($.inArray(input, accounts) !== -1) ? true : false
}
