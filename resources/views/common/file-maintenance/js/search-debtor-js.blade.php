$('.search-debtor').on('click', function () {
    let modal = $('.search-debtor-modal-template').clone().removeClass('search-debtor-modal-template')
    let getRoute = modal.data('get-route')
    let table = modal.find('table')
    // show modal
    modal.modal()
    modal.on('hidden.bs.modal', function (e) { $(this).remove() })

    // should show processing while waiting for the data to be loaded

    // populate table and reload Datatable
    $.ajax({
        url: getRoute,
        method: 'GET',
    }).done(function (resources) {
        resources.forEach(resource => {
            let markup = ''
            if (resource.D_DC) {
                markup = `
                <tr class="pointer" data-choice="${resource.accountcode}" data-id="${resource.id}">
                    <td class="acode" width="25%">${resource.accountcode}</td>
                    <td class="name" width="75%">${resource.name}</td>
                </tr>
                `
            } else if (resource.C_DC) {
                markup = `
                <tr class="pointer" data-choice="${resource.accountcode}" data-id="${resource.id}">
                    <td class="acode" width="25%">${resource.accountcode}</td>
                    <td class="name" width="75%">${resource.name}</td>
                </tr>
                `
            }
            table.find('tbody').append(markup)
        });

        // init datatable
        table.DataTable()
        table.on('click', 'tr', function () {
            window.location = $(this).data('id')
            modal.modal('hide')
        })
    })
})
