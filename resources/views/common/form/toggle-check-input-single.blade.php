<input type="checkbox"
    name="{{ isset($name) ? $name : 'VALUE' }}"
    class="check {{ isset($class) ? $class : '' }}"
    data-toggle="toggle"
    data-on="Yes" data-off="No"
    data-onstyle="success"
    data-offstyle="secondary"
    data-width="70"
    data-height="10"
    value="{{ isset($value) ? $value : '' }}"
    {{ isset($id) ? 'id='.$id : '' }}
    {{ isset($checked) && $checked ? 'checked' : '' }}
> {{ isset($label) ? $label : '' }}
