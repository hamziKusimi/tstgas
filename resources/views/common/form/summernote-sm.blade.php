<script type="text/javascript">
    (function() {
    /**
    *   the default height for the summernote textarea is 200px, with empty placeholder
    *   to prevent injection, only text/font related options are shown
    *   this blade will show a smaller version of the summernote but functions still the same
    * */
    $('.summernote').summernote({
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        },
        placeholder: '',
        height: 100,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']]
        ],
        followingToolbar: false
    })
}) ()
</script>