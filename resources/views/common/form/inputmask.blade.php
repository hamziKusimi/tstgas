<script type="text/javascript">
(function() {
    /**
    *   the 'currency' is an alias; from the repo:
    *    With an alias you can define a complex mask definition and call it by using an alias name.
    *    So this is mainly to simplify the use of your masks. Some aliases found in the extensions are:
    *    email, currency, decimal, integer, date, datetime, dd/mm/yyyy, etc.
    * */
    $('.money').inputmask('currency', {
        prefix: '',
        rightAlign: true,
        removeMaskOnSubmit: true,
        autounmask: true
    })

    $('.money-default').inputmask('currency', {
        prefix: '',
        rightAlign: true,
        removeMaskOnSubmit: false,
        autounmask: false
    })

    // $('.sequence_no').inputmask('9999', {
    //     numericInput:false,
    //     placeholder: '0',
    //     removeMaskOnSubmit: false,
    //     autounmask: false
    // })

    $('.number-right-align').inputmask({
        rightAlign: true,
    })

    $('.date-mask').inputmask('datetime', {
        mask: "dd/mm/yyyy",
    })

}) ()
</script>
