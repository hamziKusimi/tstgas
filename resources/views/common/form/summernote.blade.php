<script type="text/javascript">
(function() {
    /**
    *   the default height for the summernote textarea is 200px, with empty placeholder
    *   to prevent injection, only text/font related options are shown
    * */
    $('.summernote').summernote({
        placeholder: '',
        height: 200,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    })
}) ()
</script>
