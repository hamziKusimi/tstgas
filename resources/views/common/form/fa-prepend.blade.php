<div class="input-group-prepend {{ isset($additionalClass) ? $additionalClass : '' }}">
    <div class="input-group-text  bg-dark-blue text-white">
        <small>
            @if (isset($prepend))
                <i class="{{ $prepend }}"></i>
            @endif
        </small>
    </div>
</div>
