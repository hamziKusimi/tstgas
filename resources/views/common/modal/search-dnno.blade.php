<div class="modal fade search-dnno-modal-template" id="dnno-modal" tabindex="-1" role="dialog" aria-hidden="true" data-modal-route="{{ $getModalRoute }}" data-get-route="{{ $getDnnoRoute }}" data-get-route2="{{ $getDocsApiRoute }}">
        {{-- This should only show for related dnnos (if this is Invoice, the results should be just for Invoice) --}}
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 getdnno">
                            <select id="list1" size="4" class="form-control left-select" style="width:300px;height:200px;">
                            
                            </select>
                        <br><br><br><br></div>
                        
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-4">
                            <select id="list2" size="4" class="form-control right-select" style="width:300px;height:200px">
                            
                            </select>
                        </div>
                    </div>
                        @include('dailypro.components.table.dntables.table')
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-success">ok</button>
                </div>
            </div>

        </div>
    </div>