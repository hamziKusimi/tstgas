<div class="modal fade search-creditor-modal-template" tabindex="-1" role="dialog" aria-hidden="true"
    data-get-route="{{ isset($customRoute) ? $customRoute : route('creditors.api.get') }}"
    data-edit-route="{{ isset($customEditRoute) ? $customEditRoute : route('creditors.edit', 'id') }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Accounts</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th width="25%">A/C Code</th>
                                <th width="75%">Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{--
                            <tr class="pointer">
                                <td class="acode" width="25%"></td>
                                <td class="name" width="75%"></td>
                            </tr>
                            --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>