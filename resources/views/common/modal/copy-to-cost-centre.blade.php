<div class="modal fade" id="copy-to-cost-centre-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Copy to Cost Centre</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => $copyRoute, 'method' => 'POST', 'class' => 'form-copy']) !!}
                    <div class="form-group row">
                        {!! Form::label('COPY_COST_CENTRE', 'Cost Centre', ['class' => 'col-md-2 col-form-label text-right']) !!}
                        <div class="col-md-10">
                            {!! Form::select('COPY_COST_CENTRE', $_costCentres->pluck('detail', 'C_CODE'), null, [
                                'class' => 'form-control select2 select-cost-centre',
                                'placeholder' => 'Please select a cost centre to continue'
                            ]) !!}
                            <small><span class="message col-form-label text-danger text-right"></span></small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-8"></div>
                        <div class="col-md-4">
                            {!! Form::submit('Confirm', ['class' => 'form-control btn btn-primary copy-submit-btn', 'disabled']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>