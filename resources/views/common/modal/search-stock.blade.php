<div class="modal fade search-stock-modal-template" tabindex="-1" role="dialog" aria-hidden="true"
    data-get-route="{{  route('stockcode.api.get') }}" data-edit-route="{{ route('debtors.edit', 'id') }}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Stock</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th width="25%">Stock Code</th>
                                <th width="75%">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>