<div class="modal fade search-stock-modal-template" id="cylModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Cylinder</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="cylinderTab">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>Serial</th>
                                <th>Barcode</th>
                                <th>Description</th>
                                <th>Product</th>
                                <th>Type</th>
                                <th>Id</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>