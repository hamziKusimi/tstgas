<div class="modal fade search-document-modal-template" tabindex="-1" role="dialog" aria-hidden="true" data-get-route="{{ $getDocsApiRoute }}">
    {{-- This should only show for related documents (if this is Invoice, the results should be just for Invoice) --}}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Documents</h4>
            </div>
            <div class="modal-body">
                
                <form action="" method="POST">
                @csrf
                <div class="col-md-12" style="text-align: right;">
                    <input name="search" type="text" id="search" placeholder="Search " class="search">
                    <button type="submit" class="btn btn-primary searchdocno">Search</button>
                </div>
                </form>

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th width="25%">Doc No.</th>
                                <th width="15%">Acc Code</th>
                                <th width="35%">Name</th>
                                <th width="15%">Date</th>
                            </tr>
                        </thead>
                        <tbody class="doc">
                            {{--
                            <tr class="pointer">
                                <td class="docNo" width="25%"></td>
                                <td class="acode" width="15%"></td>
                                <td class="name" width="35%"></td>
                                <td class="date" width="15%"></td>
                            </tr>
                            --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>