{{-- @php
    $hasPermission = false;
    foreach ($permissions as $permission)
        if (Auth::user()->hasPermissionTo($permission)) {
            $hasPermission = true;
            break;
        }
@endphp --}}

{{-- @if ($hasPermission) --}}
<button type="submit" class="btn {{ $color }} form-control text-center btn-form-submit" disabled="true">
    <i class="fa fa-{{ $faIcon }} pr-2"></i>
    {{ $name }}
</button>
{{-- @endif --}}