{{--
    We can allow the button to appear even if there's no permission for that button
    Otherwise, if we give permission, it must pass through user has permission check

    $permission : string    (i.e.: Export PDF Invoices)
    $issetCheck : boolean   (check if item is not null where item can be any instance of BillingModule object/model)
    $item : object          (an instance of any BillingModule object/model (Invoice / Supplier Invoice / etc....))
    $name : string          name of the button to be displayed to user
    $faIcon : string        icon from font-awesome, (i.e.: fa fa-file)
    $color : string         first part is background, then text color (i.e.: bg-primary text-white)
    $route : string         contains the link if clicked (i.e.: route('home'))
--}}
@if (isset($permission))
    @if (Auth::user()->hasPermissionTo($permission))
        @if ($issetCheck)
            {{-- normally issetCheck is set when there's item to be attached, for example at edit page --}}
            @if (isset($item))
                <a type="button" class="btn {{ $color }} form-control text-center" href="{{ $route }}" target="_blank">
                    <i class="fa fa-{{ $faIcon }} pr-2"></i>
                    {{ $name  }}
                </a>
            @endif
        @else
            {{-- normally issetCheck is null at create page --}}
            <a type="button" class="btn {{ $color }} form-control text-center" href="{{ $route }}" target="_blank">
                <i class="fa fa-{{ $faIcon }} pr-2"></i>
                {{ $name  }}
            </a>
        @endif
    @endif
@else
    <a type="button" class="btn {{ $color }} form-control text-center" href="{{ $route }}" target="_blank">
        <i class="fa fa-{{ $faIcon }} pr-2"></i>
        {{ $name  }}
    </a>
@endif