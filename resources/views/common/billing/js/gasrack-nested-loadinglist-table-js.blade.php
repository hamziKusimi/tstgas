
$(document).ready(function () {
    // let sqn = $('.newrow').find('.sqn_no').val()
    // $('.sqn_no').each(function () { sqn += 1 })
    
    let sqn = 1;
    $('.sqn_no').each(function () { sqn += 1 })

    function innerTableTbodyRowFormat2() {
      return `
          <tr class="narrow-padding new-row2 text-sm inner-row">
              <td>
                {!! Form::hidden("dt_sqn_no[]", null, ["class" => "form-control dt_sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("sqn_no[]", null, ["class" => "form-control sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("type[]", "gr", ["class" => "form-control type", "placeholder"=>"sqn_no", "readonly"]) !!}
                  {!! Form::text("barcode[]", null, ["class" => "form-control barcode", "placeholder"=>"Barcode", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("serialno[]", null, ["class" => "form-control serialno", "placeholder"=>"Serial No", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("driverdt[]", null, [ "class" => "form-control driverdt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("preparedt[]", null, ["class" => "form-control preparedt", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("loadingdt[]", null, ["class" => "form-control loadingdt", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("uloadingdt[]", null, ["class" => "form-control uloadingdt", "readonly"]) !!}
              </td>
              <td>
                  <button type="button" class="btn btn-danger remove-row-child" data-id2="0">
                      <i class="fa fa-times"></i>
                  </button>
              </td>
          </tr>
        `
    }
    function MainTableTbodyRowFormat2() {
        return `
        <tr class="narrow-padding toggle-details main-table-row-template d-none" data-row-id="0">
            <td class="details-control toggle-inner-table-button">
            </td>
            <td>
                {!! Form::hidden('item_id[]', null, ['class' => 'item-id']) !!}
                {!! Form::text('sequence_no[]', null, ['class' => 'form-control sequence_no exist-val-cy']) !!}
            </td>
            <td>
                {!! Form::text('category[]', null, ['class' => 'form-control category exist-val-cy-select']) !!}
                {!! Form::hidden('categorydt[]', null, ['class' => 'form-control categorydt']) !!}
            </td>
            <td>
                {!! Form::text('product[]', null, ['class' => 'form-control product exist-val-cy-select']) !!}
                {!! Form::hidden('productdt[]', null, ['class' => 'form-control productdt']) !!}
            </td>
            <td>
                {!! Form::text('capacity[]', null, ['class' => 'form-control capacity exist-val-cy-select']) !!}
                {!! Form::hidden('capacitydt[]', null, ['class' => 'form-control capacitydt']) !!}
            </td>
            <td>
                {!! Form::text('pressure[]', null, ['class' => 'form-control pressure exist-val-cy-select']) !!}
                {!! Form::hidden('pressuredt[]', null, ['class' => 'form-control pressuredt']) !!}
            </td>
            <td>
                {!! Form::number('quantity[]', null, ['class' => 'form-control validate-barcode quantity exist-val-c', 'min' => '1', 'step' => '1']) !!}
            </td>
            <td>
                {!! Form::text('gas_price[]', null, ['class' => 'form-control gas_price exist-val-c']) !!}
            </td>    
            <td>
                {!! Form::text('daily_price[]', null, ['class' => 'form-control daily_price exist-val-c']) !!}
            </td>
            
            <td>
                {!! Form::text('monthly_price[]', null, ['class' => 'form-control monthly_price exist-val-c']) !!}
            </td>
            <td>
                <div class="input-group" id="date-picker1" data-target-input="nearest">
                    {!! Form::text('bill_date[]', null, [
                    'class' => 'form-control bill_date datetimepicker-input',
                    'required',
                    'placeholder' => '',
                    'data-target' => '#date-picker1'
                    ]) !!}
                    {{-- <div class="input-group-append" data-target="#date-picker1" data-toggle="datetimepicker">
                        <div class="input-group-text bg-dark-green text-white">
                            <small><i class="fa fa-calendar"></i></small>
                        </div>
                    </div> --}}
                </div>
                {{-- {!! Form::text('bill_date[]', null, ['class' => 'form-control bill_date exist-val-gr']) !!} --}}
            </td> 
            <td>
                <button type="button" class="btn btn-danger remove-row" data-id="0">
                    <i class="fa fa-times"></i>
                </button>
            </td>
        </tr>
        <tr class="inner-table second-row-template d-none" data-row-id="0">
            <td colspan="12">
                <table class="table table-sm narrow-padding cylinder-table">
                    <thead>
                        <tr class="text-sm">
                            <th class="text-center">Barcode</th>
                            <th class="text-center">Serial</th>
                            <th class="text-center">Driver</th>
                            <th class="text-center">Prepare</th>
                            <th class="text-center">Loading</th>
                            <th class="text-center">Unloading</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </td>
        </tr>
    
          `
    }

    $('#gasrackdt').on('click', '.toggle-inner-table-button', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#gasrackdt').find('.'+secondRowIdentifier)

        // now we want to show the table or hide it
        if (row.hasClass('d-none'))
            row.removeClass('d-none')     // if its hidden, show it, when button is clicked
        else
            row.addClass('d-none')        // if its shown, hide it, when button is clicked

    })

    $('#gasrackdt').on('click', '.toggle-inner-table-button-exist', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#gasrackdt').find('.'+secondRowIdentifier)
        // now we want to show the table or hide it
        if (row.hasClass('d-none'))
            row.removeClass('d-none')     // if its hidden, show it, when button is clicked
        else
            row.addClass('d-none')        // if its shown, hide it, when button is clicked

    })

    //Check if the main row have matched value with the loading list in uloading list
    let isMatched = function(selectedBarcode2, mainRow) {
        if (selectedBarcode2.gstype.code == mainRow.find('.grtype').val())
        {
            return true    
        }

        return false
    }

    /**
     * On Change Barcode,
     * 1. compare the value of barcode against database records
     * 2. if found, search the existing "category" & "product" in the main-table above
     *      3. if found, add a new-row to the inner-table of this main-table
     * 
     * NOTE: 
     *      - we are comparing the barcode value against the first tr (main-row)
     *      - we only use row-id to find the next tr (second-row)
     * 
     *          <tr class="main-row"> </tr> 
     *          <tr class="second-row"> </tr>
     * * */
    
    $('#prepare-gas').click(function () {
        let card = $(this).parents('.card')
        let date = card.find('.invoice-date')
        let driver = card.find('.driverpr')
        let barcode = $('#barcodepr').val()
        let serial = $('#serialpr').val();
        var isAdded =  false;
        let dbRecords2 = $('#barcodepr').data('gs');
        

        if(barcode == ''){
            serial = serial.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.serial == serial
            })[0]
        }else{

            barcode = barcode.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.barcode == barcode
            })[0]
        }
   
        let selectedBarcodeManyfor = [] 
        selectedBarcodeManyfor = selectedBarcode2.gscylinder
        let existRows = $('#gasrackdt').find('.existing-data2');


        existRows.each(function (index) {
            let mainRow = $(this)
            let id =  $(this).data('row-id2')
            let secondRow = $(this).siblings('.row-id2-' + id)
          
            let gasrackTable = secondRow.find('.gasrack-table')
            let gasrackTableRow = gasrackTable.find('.new-row2')
            if (selectedBarcode2 && isMatched(selectedBarcode2, mainRow) ) {
                // generating new element and inputting the value
                
                let newRow = $(innerTableTbodyRowFormat2())
                newRow.find('.dt_sqn_no').val(mainRow.find('.grsequence_no').val())
                newRow.find('.barcode').val(selectedBarcode2.barcode)
                newRow.find('.serialno').val(selectedBarcode2.serial)
                newRow.find('.preparedt').val(date.val())       
                newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                newRow.find('.sqn_no').val(sqn)
                sqn += 1
                $('.serialpr').val(selectedBarcode2.serial)

            
                // newRow.find('.preparedt').val($('.invoice-date').val())     // on page load
                // check if there's existing row in the gasrack-table
                if (gasrackTableRow.length > 0) {

                    // We need to check one condition: the table does not contain the same barcode value (barcodepr)
                    let existingValues = []
                    gasrackTableRow.each(function() {
                        existingValues.push($(this).find('.barcode').html())
                    })

                    if ($.inArray(selectedBarcode2.barcode, existingValues) == -1){
                        console.log('gralert1')
                        alert("Prepare Gasrack Succesfully Added");
                        isAdded = true;

                        gasrackTable.find(".gasrack").append(newRow)
                        $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                        $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)

                        let type = $('.main-row2-id-' + id).find('.grtype').val()
                        $('.main-row2-id-' + id).find('.grtypedt').val(type)

                                
                            //add cylinder automatically if exist

                            
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                preparecylinder = true
                                //   if (($.inArray(categorymanyfor, cyexistingValues2) == -1)
                                //     && ($.inArray(productmanyfor, pdexistingValues2) == -1)
                                //     && ($.inArray(capacitymanyfor, cpexistingValues2) == -1)
                                //     && ($.inArray(pressuremanyfor, prexistingValues2) == -1)){
                                        
                                //         rowexist = true
                                //     }else{
                                //         rowexist = false

                                //     }
                                console.log('ddd')
                                $('#add-new').trigger('click')
                                $('#prepare-cyl').trigger('click')
                                preparecylinder = false
                                $('#barcodepr').val('')
                                $('#serialpr').val('')
                            })


                            }else{
                                console.log('gralert2')
                                alert("Gasrack Already Exist");
                                isAdded = true;
                                $('#barcodepr').val('')
                                $('#serialpr').val('')
                            }

    
                } else {
                     //add cylinder automatically if exist
                     
                     $.each(selectedBarcodeManyfor, function(index, value){
                         barcodemanyfor = value.cy_barcode
                         serialmanyfor = value.cy_serial
                         categorymanyfor = value.cy_category
                         productmanyfor = value.cy_product
                         descrmanyfor = value.cy_descr
                         capacitymanyfor = value.cy_capacity
                         pressuremanyfor = value.cy_pressure
                         preparecylinder = true
                         rowExistinCylinder = true
                         $('#add-new').trigger('click')
                         $('#prepare-cyl').trigger('click')
                         preparecylinder = false
                        //               if (($.inArray(categorymanyfor, cyexistingValues2) == -1)
                        // && ($.inArray(productmanyfor, pdexistingValues2) == -1)
                        // && ($.inArray(capacitymanyfor, cpexistingValues2) == -1)
                        // && ($.inArray(pressuremanyfor, prexistingValues2) == -1)){
                            
                        //     rowexist = true
                        // }else{
                        //     rowexist = false

                        // }
                        })

                        // Directly append whatever value that we found/
                        console.log('gralert3')
                        alert("Prepare Gasrack Succesfully Added");
                        isAdded = true;

                        gasrackTable.find(".gasrack").append(newRow)
                        $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                        $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)
                        
                        let type = $('.main-row2-id-' + id).find('.grtype').val()
                        $('.main-row2-id-' + id).find('.grtypedt').val(type)
                        $('#barcodepr').val('')
                        $('#serialpr').val('')
                
                    return false
                }

                // exit the loop
                return false
            }
        })

        
        if (!isAdded) {
            let mainRows = $('#gasrackdt').find('.main-row2')
            // this loop is to find the second row
            total_row = mainRows.length;
            
            mainRows.each(function(index) {
                let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)
                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                
                if (selectedBarcode2 && isMatched(selectedBarcode2, mainRow) ) {
                    // generating new element and inputting the value

                    console.log(selectedBarcode2.barcode)
                    console.log(selectedBarcode2.serial)
                    let newRow = $(innerTableTbodyRowFormat2())
                    newRow.find('.dt_sqn_no').val(mainRow.find('.grsequence_no').val())
                    newRow.find('.barcode').val(selectedBarcode2.barcode)
                    newRow.find('.serialno').val(selectedBarcode2.serial)
                    newRow.find('.preparedt').val(date.val())       
                    newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                    newRow.find('.sqn_no').val(sqn)
                    sqn += 1
                    $('.serialpr').val(selectedBarcode2.serial)


                    // newRow.find('.preparedt').val($('.invoice-date').val())     // on page load
                    // check if there's existing row in the gasrack-table
                    if (gasrackTableRow.length > 0) {

                        

                        // We need to check one condition: the table does not contain the same barcode value (barcodepr)
                        let existingValues = []
                        gasrackTableRow.each(function() {
                            existingValues.push($(this).find('.barcode').val())
                            existingValues.push($(this).find('.barcode').html())
                        })

                        if ($.inArray(selectedBarcode2.barcode, existingValues) == -1){
                            console.log('gralert4')
                            alert("Prepare Gasrack Succesfully Added");
                            isAdded = true;

                            gasrackTable.find(".gasrack").append(newRow)
                            $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                            $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)

                            let type = $('.main-row2-id-' + id).find('.grtype').val()
                            $('.main-row2-id-' + id).find('.grtypedt').val(type)

                                
                            //add cylinder automatically if exist

                            $.each(selectedBarcodeManyfor, function(index, value){  
                                
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                preparecylinder = true

                                    //  if (($.inArray(categorymanyfor, cyexistingValues2) == -1)
                                    //     && ($.inArray(productmanyfor, pdexistingValues2) == -1)
                                    //     && ($.inArray(capacitymanyfor, cpexistingValues2) == -1)
                                    //     && ($.inArray(pressuremanyfor, prexistingValues2) == -1)){
                                            
                                    //         rowexist = true
                                    //     }else{
                                    //         rowexist = false

                                    //     }
                                console.log('xxx')
                                $('#add-new').trigger('click')
                                //     count++
                                //  console.log(count)
                                // }
                                $('#prepare-cyl').trigger('click')
                                preparecylinder = false
                                $('#barcodepr').val('')
                                $('#serialpr').val('')
                            })

                        }else{
                            console.log('gralert5')
                            alert("Gasrack Already Exist");
                            isAdded = true;
                            $('#barcodepr').val('')
                            $('#serialpr').val('')
                        }

        
                    } else {

                         //add cylinder automatically if exist


                         // Directly append whatever value that we found/
                         console.log('gralert6')
                         alert("Prepare Gasrack Succesfully Added");
                         isAdded = true;
 
                         gasrackTable.find(".gasrack").append(newRow)
                         $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                         $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)
                         
                         let type = $('.main-row2-id-' + id).find('.grtype').val()
                         $('.main-row2-id-' + id).find('.grtypedt').val(type)

                         
                         
                         $.each(selectedBarcodeManyfor, function(index, value){                            
                            barcodemanyfor = value.cy_barcode
                            serialmanyfor = value.cy_serial
                            categorymanyfor = value.cy_category
                            productmanyfor = value.cy_product
                            descrmanyfor = value.cy_descr
                            capacitymanyfor = value.cy_capacity
                            pressuremanyfor = value.cy_pressure
                           preparecylinder = true
                           rowExistinCylinder = true
                           $('#add-new').trigger('click')
                           $('#prepare-cyl').trigger('click')
                           preparecylinder = false
                               //  count++

                           //      console.log(count)
                           //  }
                        })

                         $('#barcodepr').val('')
                         $('#serialpr').val('')
                         


                        return false
                    }

                    // exit the loop
                    return false
                }else{
                    if (index === total_row - 1) {
                        // this is the last one
                        console.log('gralert7')
                        alert("No Gasrack Found");
                        $('#barcodepr').val('')
                        $('#serialpr').val('')
                    }
                }
                
                
            })

            if (total_row == 0 && !isAdded) {
                console.log('gralert8')
                alert("No Gasrack Found");
                $('#barcodepr').val('')
                $('#serialpr').val('')
            }

        }

    })

    $('#load-gas').click(function () {
        let card = $(this).parents('.card')
        let date = card.find('.invoice-date')
        let driver = card.find('.driverll')
        let barcodell = $('#barcodell').val()
        let serialll = $('#serialll').val();
        var isAdded = false;
        let dbRecords2 = $('#barcodell').data('gs');


        if(barcodell == ''){
            serialll = serialll.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.serial == serialll
            })[0]
        }else{
            barcodell = barcodell.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.barcode == barcodell
            })[0]
        }

        let selectedBarcodeManyfor = [] 
        selectedBarcodeManyfor = selectedBarcode2.gscylinder  
        let existRows = $('#gasrackdt').find('.existing-data2')

        existRows.each(function (index) {
            console.log(serialll)
            let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)

                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                
                //Get value of loading list barcode if match in the existing row of each inner table
                //then update the loading date
                //
                gasrackTableRow.each(function() {
                    let barcode = $(this).find('.barcode').val() == '' ? $(this).find('.barcode').html() : $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val() == '' ? $(this).find('.serialno').html() : $(this).find('.serialno').val()
                    let loadingdate = $(this).find('.loadingdt')

                    if (barcodell == '') {
                        console.log('1')
                        if(serial == serialll && loadingdate.val() == ''){
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $(this).find('.datetime').val(date.val())
                            $('.serialll').val(serial)

                            isAdded = true   
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                loadcylinder = true
                                $('#load-cyl').trigger('click')
                                loadcylinder = false
                            })     
                            console.log('gralert9')
                            alert("Loading Gasrack Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                        }
                    }else{
                        console.log('2')
                        if(barcode == barcodell && loadingdate.val() == ''){
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $(this).find('.datetime').val(date.val())
                            $('.serialll').val(serial)

                            isAdded = true 
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                loadcylinder = true
                                $('#load-cyl').trigger('click')
                                loadcylinder = false
                            })
                            
                            console.log('gralert10')
                            alert("Loading Gasrack Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                        }
                    }

                })
        })

        if (!isAdded) {
            let mainRows = $('#gasrackdt').find('.main-row2')
            var total = mainRows.length;
            mainRows.each(function(index) {

                let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)

                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                let total_2 = gasrackTableRow.length
                //Get value of loading list barcode if match in the existing row of each inner table
                //then update the loading date
                //
                gasrackTableRow.each(function(index_2) {
                    let barcode = $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val()
                    let loadingdate = $(this).find('.loadingdt')

                    if (barcodell == '') {
                        console.log('3')
                        if(serial == serialll && loadingdate.val() == ''){
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialll').val(serial)

                            isAdded = true
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                loadcylinder = true
                                $('#load-cyl').trigger('click')
                                loadcylinder = false
                            })
                            console.log('gralert11')
                            alert("Loading Gasrack Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('gralert12')
                                alert("No Gasrack Found");
                                $('#barcodell').val('')
                                $('#serialll').val('')
                            }
                        }
                    }else{
                        console.log('4')
                        if(barcode == barcodell && loadingdate.val() == ''){
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            
                            $('.serialll').val(serial)

                            isAdded = true
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                loadcylinder = true
                                $('#load-cyl').trigger('click')
                                loadcylinder = false
                            })
                            console.log('gralert13')
                            alert("Loading Gasrack Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('gralert14')
                                alert("No Gasrack Found");
                                $('#barcodell').val('')
                                $('#serialll').val('')
                            }
                        }
                    }

                })
            })
        }

        // if (mainRows.length == 0 && !isAdded) {
        //     alert("No Gasrack Found");
        // }

    })
    
    $('#unload-gas').click(function () {
        let card = $(this).parents('.card')
        let date = card.find('.invoice-date')
        let driver = card.find('.driverul')
        let barcodeul = $('#barcodeul').val()
        let serialul = $('#seriaull').val();
        var isAdded = false;
        let dbRecords2 = $('#barcodeul').data('gs');
        

        if(barcodeul == ''){
            serialul = serialul.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.serial == serialul
            })[0]
        }else{
            barcodeul = barcodeul.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.barcode == barcodeul
            })[0]
        }

        let selectedBarcodeManyfor = [] 
        selectedBarcodeManyfor = selectedBarcode2.gscylinder 
        let existRows = $('#gasrackdt').find('.existing-data2')

        existRows.each(function (index) {
            let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)

                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                
                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                gasrackTableRow.each(function() {
                    let barcode = $(this).find('.barcode').val() == '' ? $(this).find('.barcode').html() : $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val() == '' ? $(this).find('.serialno').html() : $(this).find('.serialno').val()
                    let uloadingdate = $(this).find('.uloadingdt')

                    if (barcodeul == '') {
                        if(serial == serialul && uloadingdate.val() == ''){
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $(this).find('.datetime').val(date.val())
                            $('.serialul').val(serial)

                            isAdded = true;
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                                unloadcylinder = false
                            })  
                            console.log('gralert15')
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                        }
                    }else{
                        if(barcode == barcodeul && uloadingdate.val() == ''){
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $(this).find('.datetime').val(date.val())
                            $('.serialul').val(serial)

                            isAdded = true;
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                                unloadcylinder = false
                            })
                            console.log('gralert16')
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                        }
                    }

                })
        })

        if (!isAdded) {
            let mainRows = $('#gasrackdt').find('.main-row2')
            var total = mainRows.length;
            mainRows.each(function(index) {

                let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)

                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                let total_2 = gasrackTableRow.length
                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                gasrackTableRow.each(function(index_2) {
                    let barcode = $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val()
                    let uloadingdate = $(this).find('.uloadingdt')

                    if (barcodeul == '') {
                        if(serial == serialul && uloadingdate.val() == ''){
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialul').val(serial)

                            isAdded = true;
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                                unloadcylinder = false
                            })
                            console.log('gralert17')
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('gralert18')
                                alert("No Gasrack Found");
                                $('#barcodeul').val('')
                                $('#seriaull').val('')
                            }
                        }
                    }else{
                        if(barcode == barcodeul && uloadingdate.val() == ''){
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            
                            $('.serialul').val(serial)

                            isAdded = true;
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                                unloadcylinder = false
                            })
                            console.log('gralert19')
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('gralert20')
                                alert("No Gasrack Found");
                                $('#barcodeul').val('')
                                $('#seriaull').val('')
                            }
                        }
                    }

                })
            })
        }

        if (mainRows.length == 0 && !isAdded) {
            console.log('gralert21')
            alert("No Gasrack Found");
            $('#barcodeul').val('')
            $('#seriaull').val('')
        }

    })
    
    console.log($('.driver').val())

    
    let driver = $('.driver').val()

    $('.driverpr').val(driver)
    $('.driverll').val(driver)
    $('.driverul').val(driver)
    
    //Get Driver Details
    $('.driver').on('change', function(){
        console.log('hee')
        let driver = $(this).val()

        $('.driverpr').val(driver)
        $('.driverll').val(driver)
        $('.driverul').val(driver)

    })
    
})
