$(document).ready(function () {
    let sqn = 1;
    $('.sqn_no').each(function () {
        sqn += 1
    })

    let mainTableCounter = 0; // everytime click (Add) button, increment this number (used for data-row-id)
    let idRow = 0; // everytime, new barcode found, increment this number and append in inner table
    let barcodeData = $('.barcodell').data('item'); // get the existing data from the database (i.e.: BR1234)


    function innerTableTbodyRowFormat() {
        return `
          <tr class="narrow-padding new-row text-sm inner-row">
              <td>
                {!! Form::hidden("dt_sqn_no[]", null, ["class" => "form-control dt_sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("sqn_no[]", null, ["class" => "form-control sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("type[]", "cy", ["class" => "form-control type", "placeholder"=>"sqn_no", "readonly"]) !!}
                  {!! Form::text("barcode[]", null, ["class" => "form-control barcode", "placeholder"=>"Barcode", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("serialno[]", null, ["class" => "form-control serialno", "placeholder"=>"Serial No", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("driverdt[]", null, [ "class" => "form-control driverdt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("preparedt[]", null, ["class" => "form-control preparedt calculate-field", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("loadingdt[]", null, ["class" => "form-control loadingdt calculate-field", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("uloadingdt[]", null, ["class" => "form-control uloadingdt calculate-field", "readonly"]) !!}
              </td>
              <td>
                  <button type="button" class="btn btn-danger remove-row-child" data-id="0">
                      <i class="fa fa-times"></i>
                  </button>
              </td>
          </tr>
        `
    }

    $('#opiniondt').on('click', '.toggle-inner-table-button', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#opiniondt').find('.' + secondRowIdentifier)

        // now we want to show the table or hide it
        if (row.hasClass('d-none'))
            row.removeClass('d-none') // if its hidden, show it, when button is clicked
        else
            row.addClass('d-none') // if its shown, hide it, when button is clicked

    })

    $('#opiniondt').on('click', '.toggle-inner-table-button-exist', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#opiniondt').find('.' + secondRowIdentifier)

        // now we want to show the table or hide it
        if (row.hasClass('d-none')) {
            row.removeClass('d-none') // if its hidden, show it, when button is clicked
        } else {
            row.addClass('d-none') // if its shown, hide it, when button is clicked
        }
    })

    //Check if the main row have matched value with the loading list in uloading list
    let isMatched = function (selectedBarcode, mainRow) {
        if (selectedBarcode.category.code == mainRow.find('.category').val() &&
            selectedBarcode.product.code == mainRow.find('.product').val() &&
            selectedBarcode.capacity == mainRow.find('.capacity').val() &&
            selectedBarcode.workingpressure == mainRow.find('.pressure').val()) {
            return true
        }

        return false
    }

    //check if barcode or serial exist in cylinders records or not
    function filterRecords(item, route, date, driver, isAdded, isBarcode) {

        let data = {
            'item': item,
            'isBarcode': isBarcode
        }

        if (item != '') {
            $.ajax({
                url: route,
                method: "GET",
                data: data,
            }).done(function (response) {
                $('.loading-overlay').hide();
                var selectedBarcode = response[0];
                let existRows = $('#opiniondt').find('.existing-data')
                var totalR = existRows.length;

                existRows.each(function (index) {
                    let existRow = $(this)
                    let id = $(this).data('row-id')
                    let secondRow = $(this).siblings('.row-id-' + id)

                    let cylinderTable = secondRow.find('.cylinder-table')
                    let cylinderTableRow = cylinderTable.find('.new-row')
                    if (selectedBarcode && isMatched(selectedBarcode, existRow)) {

                        let newRow = $(innerTableTbodyRowFormat())
                        newRow.find('.dt_sqn_no').val(existRow.find('.sequence_no').val())
                        newRow.find('.barcode').val(selectedBarcode.barcode)
                        newRow.find('.serialno').val(selectedBarcode.serial)
                        newRow.find('.preparedt').val(date.val())
                        newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                        newRow.find('.sqn_no').val(sqn)
                        sqn += 1
                        $('.serialpr').val(selectedBarcode.serial)

                        if (cylinderTableRow.length > 0) {

                            // We need to check one condition: the table does not contain the same barcode value (barcodell)
                            let existingValues = []
                            cylinderTableRow.each(function () {
                                existingValues.push($(this).find('.barcode').val())
                                existingValues.push($(this).find('.barcode').html())
                            })

                            if ($.inArray(selectedBarcode.barcode, existingValues) == -1) {
                                console.log('alert1')
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("Prepare Cylinder Succesfully Added");
                                isAdded = true;
                                cylinderTable.find("tbody").append(newRow)

                                //readonly row
                                $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                                //disable select 2
                                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                                //put value to database hidden input form
                                let cat = $('.main-row-id-' + id).find('.category').val()
                                $('.main-row-id-' + id).find('.categorydt').val(cat)

                                let prod = $('.main-row-id-' + id).find('.product').val()
                                $('.main-row-id-' + id).find('.productdt').val(prod)

                                let cap = $('.main-row-id-' + id).find('.capacity').val()
                                $('.main-row-id-' + id).find('.capacitydt').val(cap)

                                let ps = $('.main-row-id-' + id).find('.pressure').val()
                                $('.main-row-id-' + id).find('.pressuredt').val(ps)
                                

                                $('#barcodepr').val('')
                                $('#serialpr').val('')
                            } else {
                                console.log('alert2')
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("Cylinder Already Exist");
                                isAdded = true;

                                $('#barcodepr').val('')
                                $('#serialpr').val('')
                            }
                        } else {
                            // Directly append whatever value that we found
                            cylinderTable.find("tbody").append(newRow)
                            console.log('alert3')
                            if(barcodemanyfor == '' || serialmanyfor == '')
                            alert("Prepare Cylinder Succesfully Added");
                            isAdded = true;

                            //readonly row
                            $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                            //disable select 2
                            $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                            //put value to database hidden input form
                            let cat = $('.main-row-id-' + id).find('.category').val()
                            $('.main-row-id-' + id).find('.categorydt').val(cat)

                            let prod = $('.main-row-id-' + id).find('.product').val()
                            $('.main-row-id-' + id).find('.productdt').val(prod)

                            let cap = $('.main-row-id-' + id).find('.capacity').val()
                            $('.main-row-id-' + id).find('.capacitydt').val(cap)

                            let ps = $('.main-row-id-' + id).find('.pressure').val()
                            $('.main-row-id-' + id).find('.pressuredt').val(ps)



                            $('#barcodepr').val('')
                            $('#serialpr').val('')

                            return false
                        }


                        $('#barcodepr').val('')
                        $('#serialpr').val('')
                        // exit the loop
                        return false
                    }

                })

                let mainRows = $('#opiniondt').find('.main-row')
                var total = mainRows.length;

                if (!isAdded) {
                    // this loop is to find the second row
                    mainRows.each(function (index) {

                        let mainRow = $(this)
                        let id = $(this).data('row-id')

                        let secondRow = $(this).siblings('.row-id-' + id)

                        let cylinderTable = secondRow.find('.cylinder-table')
                        let cylinderTableRow = cylinderTable.find('.new-row')
                        if (selectedBarcode && isMatched(selectedBarcode, mainRow)) {

                            // generating new element and inputting the value

                            let newRow = $(innerTableTbodyRowFormat())
                            newRow.find('.dt_sqn_no').val(mainRow.find('.sequence_no').val())
                            newRow.find('.barcode').val(selectedBarcode.barcode)
                            newRow.find('.serialno').val(selectedBarcode.serial)
                            newRow.find('.preparedt').val(date.val())
                            newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                            newRow.find('.sqn_no').val(sqn)
                            sqn += 1
                            $('.serialpr').val(selectedBarcode.serial)

                            // newRow.find('.loadingdt').val($('.invoice-date').val())     // on page load
                            // check if there's existing row in the cylinder-table
                            if (cylinderTableRow.length > 0) {

                                // We need to check one condition: the table does not contain the same barcode value (barcodell)
                                let existingValues = []
                                cylinderTableRow.each(function () {
                                    existingValues.push($(this).find('.barcode').val())
                                })

                                if ($.inArray(selectedBarcode.barcode, existingValues) == -1) {
                                    console.log('alert4')
                                    if(barcodemanyfor == '' || serialmanyfor == '')
                                    alert("Prepare Cylinder Succesfully Added");
                                    isAdded = true;
                                    cylinderTable.find("tbody").append(newRow)

                                    //readonly row
                                    $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                                    //disable select 2
                                    $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                                    //put value to database hidden input form
                                    let cat = $('.main-row-id-' + id).find('.category').val()
                                    $('.main-row-id-' + id).find('.categorydt').val(cat)

                                    let prod = $('.main-row-id-' + id).find('.product').val()
                                    $('.main-row-id-' + id).find('.productdt').val(prod)

                                    let cap = $('.main-row-id-' + id).find('.capacity').val()
                                    $('.main-row-id-' + id).find('.capacitydt').val(cap)

                                    let ps = $('.main-row-id-' + id).find('.pressure').val()
                                    $('.main-row-id-' + id).find('.pressuredt').val(ps)

                                    

                                    $('#barcodepr').val('')
                                    $('#serialpr').val('')
                                } else {
                                    console.log('alert5')
                                    if(barcodemanyfor == '' || serialmanyfor == '')
                                    alert("Cylinder Already Exist");
                                    

                                    $('#barcodepr').val('')
                                    $('#serialpr').val('')
                                }
                            } else {
                                // Directly append whatever value that we found
                                isAdded = true;
                                console.log('alert6')
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("Prepare Cylinder Succesfully Added");
                                cylinderTable.find("tbody").append(newRow)
                                
                                //readonly row
                                $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                                //disable select 2
                                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                                //put value to database hidden input form
                                let cat = $('.main-row-id-' + id).find('.category').val()
                                $('.main-row-id-' + id).find('.categorydt').val(cat)

                                let prod = $('.main-row-id-' + id).find('.product').val()
                                $('.main-row-id-' + id).find('.productdt').val(prod)

                                let cap = $('.main-row-id-' + id).find('.capacity').val()
                                $('.main-row-id-' + id).find('.capacitydt').val(cap)

                                let ps = $('.main-row-id-' + id).find('.pressure').val()
                                $('.main-row-id-' + id).find('.pressuredt').val(ps)

                                $('#barcodepr').val('')
                                $('#serialpr').val('')
                                return false
                            }

                            $('#barcodepr').val('')
                            $('#serialpr').val('')
                            // exit the loop
                            return false
                        } else {

                            if (index === total - 1) {
                                // this is the last one
                                console.log('alert7') 
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("No Cylinder Found");

                                $('#barcodepr').val('')
                                $('#serialpr').val('')
                            }

                        }


                    })
                }

                // if (totalR == 0 && !isAdded) {
                //     console.log('alert8')
                //     if(barcodemanyfor == '' || serialmanyfor == '')
                //     alert("No Cylinder Found");

                //     $('#barcodepr').val('')
                //     $('#serialpr').val('')
                // }
            }).fail(function (response) {
                console.log("Error" + response);
                $('.loading-overlay').hide();

                $('#barcodepr').val('')
                $('#serialpr').val('')
            })
        } else {
            console.log('alert9')
            if(barcodemanyfor == '' || serialmanyfor == '')
            alert("No Input Detected");
            $('.loading-overlay').hide();

            $('#barcodepr').val('')
            $('#serialpr').val('')
        }
        

    }

    /**
     * On Change Barcode,
     * 1. compare the value of barcode against database records
     * 2. if found, search the existing "category" & "product" in the main-table above
     *      3. if found, add a new-row to the inner-table of this main-table
     *
     * NOTE:
     *      - we are comparing the barcode value against the first tr (main-row)
     *      - we only use row-id to find the next tr (second-row)
     *
     *          <tr class="main-row"> </tr>
     *          <tr class="second-row"> </tr>
     * * */

    $('#prepare-cyl').click(function () {
        if(preparecylinder == true){

            let dbRecords2 = $('#barcodepr').data('gs');
            $('.loading-overlay').show();
            let card = $('#barcodepr').parents('.card');
            let date = card.find('#datepr');
            let driver = card.find('.driverpr');
            let barcode = barcodemanyfor;
            let serial = serialmanyfor;
            var isAdded = false;
            let route = $('#barcodepr').data('item');

            
            if (barcode == '') {
                serial = serial.trim()
                let isBarcode = false;
                filterRecords(serial, route, date, driver, isAdded, isBarcode);
            } else {
                barcode = barcode.trim()
                let isBarcode = true;
                filterRecords(barcode, route, date, driver, isAdded, isBarcode);
            }


        }
        else{
            $('.loading-overlay').show();
            let card = $('#barcodepr').parents('.card');
            let date = card.find('#datepr');
            let driver = card.find('.driverpr');
            let barcode = $('#barcodepr').val();
            let serial = $('#serialpr').val();
            var isAdded = false;
            let route = $('#barcodepr').data('item');
            
            if (barcode == '') {
                let isBarcode = false;
                filterRecords(serial, route, date, driver, isAdded, isBarcode);
            } else {
                let isBarcode = true;
                filterRecords(barcode, route, date, driver, isAdded, isBarcode);
            }
        }
    })

    $('#load-cyl').click(function () {
        let barcodell = ''
        let serialll = ''
        
        if(loadcylinder == true){
            
            barcodell = barcodemanyfor
            serialll = serialmanyfor
            barcodell = barcodell.trim()
            serialll = serialll.trim()

        }else{

            barcodell = $('#barcodell').val()
            serialll = $('#serialll').val()
            barcodell = barcodell.trim()
            serialll = serialll.trim()
        }
        
        let card = $('#barcodell').parents('.card')
        let date = card.find('#datell')
        let driver = card.find('.driverll')
        var isAdded = false;
        let mainRows = $('#opiniondt').find('.main-row')
        let existRows = $('#opiniondt').find('.existing-data')
        var total = mainRows.length;

            existRows.each(function (index) {
                // let existRow = $(this)
                let id = $(this).data('row-id')
                let secondRow = $(this).siblings('.row-id-' + id)
                
                let cylinderTable = secondRow.find('.cylinder-table')
                let cylinderTableRow = cylinderTable.find('.new-row')
                //readonly row
                // $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)
                
                //disable select 2
                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)
                
                //put value to database hidden input form
                let cat = $('.existing-data-id-' + id).find('.category').val()
                $('.existing-data-id-' + id).find('.categorydt').val(cat)
                
                let prod = $('.existing-data-id-' + id).find('.product').val()
                $('.existing-data-id-' + id).find('.productdt').val(prod)
                
                let cap = $('.existing-data-id-' + id).find('.capacity').val()
                $('.existing-data-id-' + id).find('.capacitydt').val(cap)
                
                let ps = $('.existing-data-id-' + id).find('.pressure').val()
                $('.existing-data-id-' + id).find('.pressuredt').val(ps)
                
                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                
                
                cylinderTableRow.each(function () {
                    let barcode = $(this).find('.barcode').val() == '' ? $(this).find('.barcode').html() : $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val() == '' ? $(this).find('.serialno').html() : $(this).find('.serialno').val()
                    let loadingdate = $(this).find('.loadingdt')
                    
                    if (barcodell == '') {
                        if (serial == serialll && loadingdate.val() == '') {
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            
                            $('.serialll').val(serial)
                            isAdded = true;
                            console.log('alert10')
                            if(barcodemanyfor == '' || serialmanyfor == '')
                            alert("Loading Cylinder Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                        }
                    } else {
                        if (barcode == barcodell && loadingdate.val() == '') {
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            
                            $('.serialll').val(serial)
                            isAdded = true;
                            console.log('alert11')
                            if(barcodemanyfor == '' || serialmanyfor == '')
                            alert("Loading Cylinder Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                    }
                }
                
            })
        })

        if (!isAdded) {
            mainRows.each(function (index) {

                let mainRow = $(this)
                let id = $(this).data('row-id')
                let secondRow = $(this).siblings('.row-id-' + id)
                
                let cylinderTable = secondRow.find('.cylinder-table')
                let cylinderTableRow = cylinderTable.find('.new-row')
                let total_2 = cylinderTableRow.length;
                
                //readonly row
                $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)
                
                //disable select 2
                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)
                
                //put value to database hidden input form
                let cat = $('.main-row-id-' + id).find('.category').val()
                $('.main-row-id-' + id).find('.categorydt').val(cat)
                
                let prod = $('.main-row-id-' + id).find('.product').val()
                $('.main-row-id-' + id).find('.productdt').val(prod)

                let cap = $('.main-row-id-' + id).find('.capacity').val()
                $('.main-row-id-' + id).find('.capacitydt').val(cap)
                
                let ps = $('.main-row-id-' + id).find('.pressure').val()
                $('.main-row-id-' + id).find('.pressuredt').val(ps)
                
                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                cylinderTableRow.each(function (index_2) {
                    let barcode = $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val()
                    let loadingdate = $(this).find('.loadingdt')
                    
                    if (barcodell == '') {
                        if (serial == serialll && loadingdate.val() == '') {
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialll').val(serial)
                            isAdded = true;
                            console.log('alert12')
                            if(barcodemanyfor == '' || serialmanyfor == '')
                            alert("Loading Cylinder Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                            return false;
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('alert13')
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("No Cylinder Found");
                                $('#barcodell').val('')
                                $('#serialll').val('')
                            }
                        }
                    } else {
                        if (barcode == barcodell && loadingdate.val() == '') {
                            $(this).find('.loadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialll').val(serial)
                            isAdded = true;
                            console.log('alert14')
                            if(barcodemanyfor == '' || serialmanyfor == '')
                            alert("Loading Cylinder Succesfully Added")
                            $('#barcodell').val('')
                            $('#serialll').val('')
                            return false;
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('alert15')
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("No Cylinder Found");
                                $('#barcodell').val('')
                                $('#serialll').val('')
                            }
                        }
                    }
                })
            })
        }
        
        if (existRows.length == 0 && !isAdded) {
            console.log('alert16')
            if(barcodemanyfor == '' || serialmanyfor == '')
            alert("No Cylinder Found");
            $('#barcodell').val('')
            $('#serialll').val('')
        }
    })
    
    $('#unload-cyl').click(function () {

        let barcodeul = ''
        let serialul = ''
        if(unloadcylinder == true){
            barcodeul = barcodemanyfor;
            serialul = serialmanyfor;
            barcodeul = barcodeul.trim()
            serialul = serialul.trim()

        }else{
            barcodeul = $('#barcodeul').val()
            serialul = $('#seriaull').val()
            barcodeul = barcodeul.trim()
            serialul = serialul.trim()
            var isAdded = false;
        }  

        let card = $('#barcodeul').parents('.card')
        let date = card.find('#dateul')
        let driver = card.find('.driverul')
        var isAdded = false;
        let mainRows = $('#opiniondt').find('.main-row')
        let existRows = $('#opiniondt').find('.existing-data')
        var total = mainRows.length;

        existRows.each(function (index) {

            let existRow = $(this)
            let id = $(this).data('row-id')
            let secondRow = $(this).siblings('.row-id-' + id)

            let cylinderTable = secondRow.find('.cylinder-table')
            let cylinderTableRow = cylinderTable.find('.new-row')
            //readonly row
            // $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

            //disable select 2
            $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

            //put value to database hidden input form
            let cat = $('.existing-data-id-' + id).find('.category').val()
            $('.existing-data-id-' + id).find('.categorydt').val(cat)

            let prod = $('.existing-data-id-' + id).find('.product').val()
            $('.existing-data-id-' + id).find('.productdt').val(prod)

            let cap = $('.existing-data-id-' + id).find('.capacity').val()
            $('.existing-data-id-' + id).find('.capacitydt').val(cap)

            let ps = $('.existing-data-id-' + id).find('.pressure').val()
            $('.existing-data-id-' + id).find('.pressuredt').val(ps)

            //Get value of unloading list barcode if match in the existing row of each inner table
            //then update the uloading date
            //


            cylinderTableRow.each(function (index) {

                let barcode = $(this).find('.barcode').val() == '' ? $(this).find('.barcode').html() : $(this).find('.barcode').val()
                let serial = $(this).find('.serialno').val() == '' ? $(this).find('.serialno').html() : $(this).find('.serialno').val()
                let uloadingdate = $(this).find('.uloadingdt')
                let loading = $(this).find('.loadingdt').val() == '' ? $(this).find('.loadingdt').text() : $(this).find('.loadingdt').val()

                // console.log(loading);
                if (barcodeul == '') {
                    if (serial == serialul && uloadingdate.val() == '' && loading != '') {

                        $(this).find('.uloadingdt').val(date.val());
                        $(this).find('.driverdt').val(driver.val());
                        $(this).find('.datetime').val(date.val());

                        $('.serialul').val(serial);
                        isAdded = true;

                        console.log('alert17')
                        if(barcodemanyfor == '' || serialmanyfor == '')
                        alert("Unloading Cylinder Succesfully Added")
                        $('#barcodeul').val('')
                        $('#seriaull').val('')
                    }
                } else {
                    if (barcode == barcodeul && uloadingdate.val() == '' && loading != '') {
                        $(this).find('.uloadingdt').val(date.val())
                        $(this).find('.driverdt').val(driver.val())
                        $(this).find('.datetime').val(date.val())

                        $('.serialul').val(serial)
                        isAdded = true;
                        console.log('alert18')
                        if(barcodemanyfor == '' || serialmanyfor == '')
                        alert("Unloading Cylinder Succesfully Added")
                        $('#barcodeul').val('')
                        $('#seriaull').val('')
                    }
                }

            })
        })

        if (!isAdded) {
            mainRows.each(function (index) {

                let mainRow = $(this)
                let id = $(this).data('row-id')
                let secondRow = $(this).siblings('.row-id-' + id)

                let cylinderTable = secondRow.find('.cylinder-table')
                let cylinderTableRow = cylinderTable.find('.new-row')
                let total_2 = cylinderTableRow.length;

                //readonly row
                $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                //disable select 2
                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                //put value to database hidden input form
                let cat = $('.main-row-id-' + id).find('.category').val()
                $('.main-row-id-' + id).find('.categorydt').val(cat)

                let prod = $('.main-row-id-' + id).find('.product').val()
                $('.main-row-id-' + id).find('.productdt').val(prod)

                let cap = $('.main-row-id-' + id).find('.capacity').val()
                $('.main-row-id-' + id).find('.capacitydt').val(cap)

                let ps = $('.main-row-id-' + id).find('.pressure').val()
                $('.main-row-id-' + id).find('.pressuredt').val(ps)

                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                cylinderTableRow.each(function (index_2) {
                    let barcode = $(this).find('.barcode').val();
                    let serial = $(this).find('.serialno').val();
                    let uloadingdate = $(this).find('.uloadingdt');
                    let loadingdate = $(this).find('.loadingdt').val();

                    if (barcodeul == '') {
                        if (serial == serialul && uloadingdate.val() == '' && loadingdate != '') {
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialul').val(serial)
                            isAdded = true;
                            console.log('alert19')
                            if(barcodemanyfor == '' || serialmanyfor == '')
                            alert("Unloading Cylinder Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                            return false;
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('alert20')
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("No Cylinder Found Inside Loading List");
                                $('#barcodeul').val('')
                                $('#seriaull').val('')
                            }
                        }
                    } else {
                        if (barcode == barcodeul && uloadingdate.val() == '' && loadingdate != '') {
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialul').val(serial)
                            isAdded = true;
                            console.log('alert21')
                            if(barcodemanyfor == '' || serialmanyfor == '')
                            alert("Unloading Cylinder Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                            return false;
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                console.log('alert22')
                                if(barcodemanyfor == '' || serialmanyfor == '')
                                alert("No Cylinder Found Inside Loading List");
                                $('#barcodeul').val('')
                                $('#seriaull').val('')
                            }
                        }
                    }
                })
            })
        }

        if (mainRows.length == 0 && !isAdded) {
            console.log('alert24')
            if(barcodemanyfor == '' || serialmanyfor == '')
            alert("No Cylinder Found Inside Loading List");
            $('#barcodeul').val('')
            $('#seriaull').val('')
        }
    })

    
    let driver = $('.driver').val()

    $('.driverpr').val(driver)
    $('.driverll').val(driver)
    $('.driverul').val(driver)

    //Get Driver Details
    $('.driver').on('change', function () {
        let driver = $(this).val()

        $('.driverpr').val(driver)
        $('.driverll').val(driver)
        $('.driverul').val(driver)

    })

})
