let systemSettings = $('.system-settings-param')
let boolRounding = systemSettings.data('rounding')
let mainTableCounter = 0;

let leftPad = function (value, length) {
    return value
    // return ('0'.repeat(length) + value).slice(-length);
}


// custom select2
function formatStateInTable(state) {
    return state.id
}

// overiding all current select2 and summernote in the table forms into using formatStateInTable
$('.form-select2').select2({
    templateSelection: formatStateInTable,
    dropdownAutoWidth: true
})

// when page load, aku always initialize rowID to 0

let taxCodes = $('.tax-container').data('taxes')

$('#item-master-datatable').DataTable()

let intTSN = 0;
let tot_seq = $('.sequence_no').length;

if (tot_seq > 1) {
    $('.sequence_no').each(function (index) {
        if (index === tot_seq - 1) {
            intTSN = parseInt($(this).val()) + 1;
        }
    })
} else {
    intTSN = 1;
}

let rowID = 0;
let tot_seq_rowID = $('.grcontent-id').length;

if (tot_seq_rowID > 1) {
    $('.grcontent-id').each(function (index) {

        if (index === tot_seq_rowID - 1) {
            rowID = parseInt($(this).val()) + 1;
        }
    })
} else {
    rowID = 1;
}

// trigger on click new button
$('#add-new').on('click', function () {

    //must fill in debtor account code before return note
    if($('#debtor-select').val() == ''){
        alert('Please Fill In Customer Details')
    }else{
        mainTableCounter = mainTableCounter + 1
        rowID++ // sini, tambah satu
        let template = $('.main-table-row-template').clone().removeClass('main-table-row-template d-none').addClass('main-row')
        let secondRowTemplate = $('.second-row-template').clone().removeClass('second-row-template').addClass('second-row')

        //initialize each main row and child table main-row,row-id
        let mainRowIdentifier = 'main-row-id-' + rowID
        let secondRowIdentifier = 'row-id-' + rowID
        template.addClass(mainRowIdentifier)
        template.attr('data-row-id', rowID)
        secondRowTemplate.addClass(secondRowIdentifier)
        secondRowTemplate.attr('data-row-id', rowID)

        let rowDetailID = 'row-detail-' + rowID
        let tSN = leftPad(intTSN, 0) // 0005

        //Category Select 2
        let cycatSelect = template.eq(0).find('.category')

        cycatSelect.select2({
            templateSelection: formatStateInTable,
            dropdownAutoWidth: true,
            tags: true
        })
        let contents = cycatSelect.data('cat')
        contents.forEach(function (item) {
            let newItem = new Option(item, item, false, false);
            cycatSelect.append(newItem)
        })
        cycatSelect.trigger('change')

        //Product Select 2
        let cyprodSelect = template.eq(0).find('.product')

        cyprodSelect.select2({
            templateSelection: formatStateInTable,
            dropdownAutoWidth: true,
            tags: true
        })
        let contents1 = cyprodSelect.data('prod')
        contents1.forEach(function (item1) {
            let newItem1 = new Option(item1, item1, false, false);
            cyprodSelect.append(newItem1)
        })
        cyprodSelect.trigger('change')

        //Capacity Select 2
        let cycapSelect = template.eq(0).find('.capacity')

        cycapSelect.select2({
            templateSelectiosn: formatStateInTable,
            dropdownAutoWidth: true,
            tags: true
        })
        let capacityObjects = cycapSelect.data('cap')
        $.each(capacityObjects, function (key, value) {
            let newItem2 = new Option(value, value, false, false);
            cycapSelect.append(newItem2)
            cycapSelect.val('nil')
        })

        cycapSelect.trigger('change')

        //Pressure Select 2
        let cyPressSelect = template.eq(0).find('.pressure')

        cyPressSelect.select2({
            templateSelection: formatStateInTable,
            dropdownAutoWidth: true,
            tags: true
        })
        let pressObjects = cyPressSelect.data('press')
        $.each(pressObjects, function (key, valuepres) {
            let newItem3 = new Option(valuepres, valuepres, false, false);
            cyPressSelect.append(newItem3)
            cyPressSelect.val('nil')
        })
        // for (let index = 0; index < pressArray.length; index++) {
        //     if($.inArray(pressArray[index].testpressure, contents3)){
        //         contents3.push(pressArray[index].testpressure)
        //     }
        // }

        // contents3.forEach(function (valuepres) {
        //     let newItem3 = new Option(valuepres, valuepres, false, false);
        //     cyPressSelect.append(newItem3)
        // })
        cyPressSelect.trigger('change')

        // start cloning
        if((preparecylinder == true)){
            let existingTrigger = []
            let triggercheck = ''

            if(($('.main-row').find('.category').val() == categorymanyfor) &&
            ($('.main-row').find('.product').val() == productmanyfor) &&
            ($('.main-row').find('.capacity').val() == capacitymanyfor) &&
            ($('.main-row').find('.pressure').val() == pressuremanyfor)){
            }else{
                let mainRows = $('.cylindertable').find('.existing-data')
                let mainRows2 = $('.cylindertable').find('.main-row')
                mainRows2.each(function (index) {
                    console.log(categorymanyfor)
                    console.log($(this).find('.category').val())
                    let id = $(this).data('row-id')
                    if(($(this).find('.category').val() == categorymanyfor) &&
                    ($(this).find('.product').val() == productmanyfor) &&
                    ($(this).find('.capacity').val() == capacitymanyfor) &&
                    ($(this).find('.pressure').val() == pressuremanyfor)){
                        triggercheck = '2'
                        existingTrigger.push('2')
                    }else{
                        triggercheck =  '1'
                    }
                })

                mainRows.each(function (index) {
                    console.log(categorymanyfor)
                    console.log($(this).find('.category').val())
                    let id = $(this).data('row-id')
                    if(($(this).find('.category').val() == categorymanyfor) &&
                    ($(this).find('.product').val() == productmanyfor) &&
                    ($(this).find('.capacity').val() == capacitymanyfor) &&
                    ($(this).find('.pressure').val() == pressuremanyfor)){
                        triggercheck = '2'
                        existingTrigger.push('2')
                    }else{
                        triggercheck =  '1'
                    }
                })

                if(triggercheck == '1' && existingTrigger == ''){
                    console.log(triggercheck)
                    template.eq(0).find('.sequence_no').val(tSN)
                    // template.eq(0).find('.category').select2('data', {id: 'cy cat', a_key: 'cy cat'})
                    template.eq(0).find('.category').val(categorymanyfor).trigger('change')
                    template.eq(0).find('.product').val(productmanyfor).trigger('change')
                    template.eq(0).find('.capacity').val(capacitymanyfor).trigger('change')
                    template.eq(0).find('.pressure').val(pressuremanyfor).trigger('change')
                    template.eq(0).find('.quantity').val(1)
                    template.eq(0).find('.price').attr("required", true);
                    template.eq(0).find('.toggle-inner-table-button').attr('data-second-row-id', secondRowIdentifier)
                    // template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)


                    $('#table-form-tbody').append(template)
                    $('#table-form-tbody').append(secondRowTemplate)

                    // after adding to the tbody, increment sequence_no for the upcoming button click
                    intTSN += 1
                    rowExistinCylinder = true
                    // triggercheck = '2'
                    existingTrigger = [];

                }else if((triggercheck != '2') && (existingTrigger == '')){

                    // if(rowExistinCylinder == false){
                        template.eq(0).find('.sequence_no').val(tSN)
                        // template.eq(0).find('.category').select2('data', {id: 'cy cat', a_key: 'cy cat'})
                        template.eq(0).find('.category').val(categorymanyfor).trigger('change')
                        template.eq(0).find('.product').val(productmanyfor).trigger('change')
                        template.eq(0).find('.capacity').val(capacitymanyfor).trigger('change')
                        template.eq(0).find('.pressure').val(pressuremanyfor).trigger('change')
                        template.eq(0).find('.quantity').val(1)
                        template.eq(0).find('.price').attr("required", true);
                        template.eq(0).find('.toggle-inner-table-button').attr('data-second-row-id', secondRowIdentifier)
                        // template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)


                        $('#table-form-tbody').append(template)
                        $('#table-form-tbody').append(secondRowTemplate)

                        // after adding to the tbody, increment sequence_no for the upcoming button click
                        intTSN += 1
                        rowExistinCylinder = true

                    // }
                }

                    // cyexistingValues2.push(categorymanyfor)
                    // pdexistingValues2.push(productmanyfor)
                    // cpexistingValues2.push(capacitymanyfor)
                    // prexistingValues2.push(pressuremanyfor)

                    // if()
                //     let id = 1
                // if(rowexist == false){
                    // cyexistingValues2.push(categorymanyfor)
                    // pdexistingValues2.push(productmanyfor)
                    // cpexistingValues2.push(capacitymanyfor)
                    // prexistingValues2.push(pressuremanyfor)

                // }
            }
            // console.log(categorymanyfor)
            // console.log(productmanyfor)
            // console.log(capacitymanyfor)
            // console.log(pressuremanyfor)
            // console.log(cyexistingValues2)
            // console.log(pdexistingValues2)
            // console.log(cpexistingValues2)
            // console.log(prexistingValues2)
            // if (($.inArray(categorymanyfor, cyexistingValues2) == -1)
            // && ($.inArray(productmanyfor, pdexistingValues2) == -1)
            // && ($.inArray(capacitymanyfor, cpexistingValues2) == -1)
            // && ($.inArray(pressuremanyfor, prexistingValues2) == -1)){

            //     rowexist = true
            // }else{
            //     rowexist = false

            // }

        }else{
            template.eq(0).find('.sequence_no').val(tSN)
            template.eq(0).find('.category').select2({
                templateSelection: formatStateInTable,
                dropdownAutoWidth: true
            })
            template.eq(0).find('.product').select2({
                templateSelection: formatStateInTable,
                dropdownAutoWidth: true
            })
            template.eq(0).find('.capacity').select2({
                templateSelection: formatStateInTable,
                dropdownAutoWidth: true
            })
            template.eq(0).find('.pressure').select2({
                templateSelection: formatStateInTable,
                dropdownAutoWidth: true
            })
            template.eq(0).find('.quantity').val(1)
            template.eq(0).find('.price').attr("required", true);
            template.eq(0).find('.toggle-inner-table-button').attr('data-second-row-id', secondRowIdentifier)
            // template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)

            $('#table-form-tbody').append(template)
            $('#table-form-tbody').append(secondRowTemplate)


            // after adding to the tbody, increment sequence_no for the upcoming button click
            intTSN += 1
        }

        // set on event handler
        // onHandler(rowDetailID, template.eq(0), template.eq(1))
    }
})

//Edit on adding new row-id-(i)

$('.existing-data').each(function () {

    rowID++

})


// predefined actions to be done
function onHandler(rowDetailID) {

    //delete the main row
    $('.remove-row').on('click', function () {
        $(this).closest('tr').next('tr').remove()
        $(this).parent().parent().remove()
        // $("#add-new").removeClass('d-none')
    })


}

//dynamically get function on click based on opinion dt, remove-row-child

$('#opiniondt').on('click', '.remove-row-child', function () {

    let row = $(this).parents('.new-row');
    let route = $(this).data('delete-routelist');
    let unloadingDate = row.find("td:eq(5)").text();
    let loadingDate = row.find("td:eq(4)").text();
    let isExistingData = row.data("isexisted");

    $('.delete-overlay').show();
    if (unloadingDate === "" && loadingDate === "") {

        if (confirm('This Action Will Remove Cylinder Data From The List, Are You Sure?')) {
            let method = "POST"
            let data = {
                'dnno': $(this).data('dnno'),
                'sqnex': $(this).data('sqnex'),
                'sqnlist': $(this).data('sqnlist'),
                'type': 'cy'
            }

            // delete from db
            $.ajax({
                url: route,
                method: method,
                data: data,
            }).done(function (response) {
                $('.delete-overlay').hide();
                alert("Loading and Unloading Data Removed");
                row.remove()
            }).fail(function (response) {
                alert("Error: " + response);
            })
        } else {
            $('.delete-overlay').hide();
            return false
        }

    } else if (unloadingDate === "") {

        let method = "POST"
        let data = {
            'dnno': $(this).data('dnno'),
            'sqnex': $(this).data('sqnex'),
            'sqnlist': $(this).data('sqnlist'),
            'type': 'cy',
            'only-LL': true
        }

        $.ajax({
            url: route,
            method: method,
            data: data,
        }).done(function (response) {
            $('.delete-overlay').hide();
            alert("loading Data Removed");
            row.find("td:eq(4)").empty();
            var input = '<input class="form-control loadingdt calculate-field" readonly="" name="edit_ll_date[]" type="text">'
            input += '<input name="edit_ll_barcode[]" type="hidden" value="' + row.find('.barcode').html() + '">';
            input += '<input name="edit_ll_serial[]" type="hidden" value="' + row.find('.serialno').html() + '">';
            input += '<input name="edit_ll_sequence_no[]" type="hidden" value="' + response.sqnex + '">';
            input += '<input name="edit_ll_sqn_no[]" type="hidden" value="' + response.sqnlist + '">';
            input += '<input name="edit_ll_datetime[]" class="datetime" type="hidden" value="">';
            input += '<input name="edit_ll_driver[]" type="hidden" value="' + row.find('.driver').val() + '">';
            input += '<input name="edit_ll_type[]" type="hidden" value="cy">';

            row.find("td:eq(4)").append(input);

        }).fail(function (response) {
            alert("Error: " + response);
            console.log(response)
        })
    } else if (isExistingData === undefined) {
        row.remove()
        $('.delete-overlay').hide();
    } else {
        let method = "POST"
        let data = {
            'dnno': $(this).data('dnno'),
            'sqnex': $(this).data('sqnex'),
            'sqnlist': $(this).data('sqnlist'),
            'type': 'cy',
            'only-UL': true
        }

        var sqnlist = row.data('sqnlist');
        $.ajax({
            url: route,
            method: method,
            data: data,
        }).done(function (response) {
            $('.delete-overlay').hide();
            alert("Unloading Data Removed");
            row.find("td:eq(5)").empty();
            var input = '<input class="form-control uloadingdt calculate-field" readonly="" name="edit_ul_date[]" type="text">'
            input += '<input name="edit_ul_barcode[]" type="hidden" value="' + row.find('.barcode').html() + '">';
            input += '<input name="edit_ul_serial[]" type="hidden" value="' + row.find('.serialno').html() + '">';
            input += '<input name="edit_ul_sequence_no[]" type="hidden" value="' + response.sqnex + '">';
            input += '<input name="edit_ul_sqn_no[]" type="hidden" value="' + response.sqnlist + '">';
            input += '<input name="edit_ul_datetime[]" class="datetime" type="hidden" value="">';
            input += '<input name="edit_ul_driver[]" type="hidden" value="' + row.find('.driver').val() + '">';
            input += '<input name="edit_ul_type[]" type="hidden" value="cy">';

            row.find("td:eq(5)").append(input);

        }).fail(function (response) {
            alert("Error: " + response);
            console.log(response)
        })
    }

})

$('#opiniondt').on('click', '.remove-row', function () {

    let row = $(this).parents('.sortable-row')
    let route = $(this).data('delete-route');
    let isExistingData = row.data("existed");
    let method = "DELETE"

    if (isExistingData === undefined) {
        $(this).closest('tr').next('tr').remove()
        $(this).parent().parent().remove()
    } else {
        if (confirm('This Action Will Remove The Cylinder, Are You Sure?')) {
            // delete from db
            $(this).closest('tr').next('tr').remove()
            $(this).parent().parent().remove()
            $.ajax({
                url: route,
                method: method,
            }).done(function (response) {
                alert("Succesfully Removed");
                console.log(response)
            }).fail(function (response) {
                alert("Error: " + response);
                console.log(response)
            })
        } else {
            // Do nothing!
            return false;
        }
    }

})

$('#opiniondt').on('click', '.add-remark', function () {
    let row = $(this).parents('.sortable-row');
    let isExistingData = row.data("existed");
    let id = $(this).closest("tr").data("row-id");
    var text = $(this).closest('tr').find('.remarkdt').val();

    if (isExistingData === undefined) {
        $(".remarkText").val(text);
        $('.remarkText').attr('data-index', id);
        $('.remarkText').data('index', id);
    } else {
        $(".remarkText").val(text);
        $('.remarkText').attr('data-index', id);
        $('.remarkText').data('index', id);
    }
})

// update remark
$('#myModal').on('click', '.update-remark', function () {

    let index = $('.remarkText').data("index");
    let remark = $('.remarkText').val();

    $('#opiniondt > tbody  > tr').each(function() {
        var currentRow=$(this);
        var id = currentRow.closest('tr.narrow-padding').data("row-id");
        if(id == index){
            $(this).closest('tr').find('.remarkdt').val(remark)
        }
    });

});

$('#rackTable').on('click', '.add-rack-detail', function () {
    $('.rack-overlay').show();
    let urlGet = $(this).closest("tr").data("get-url");
    let url = $(this).closest("tr").data("url");
    let dnno = $(this).closest("tr").data("dnno");
    let rackno = $(this).closest('tr').find('.serialno').text();
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    let data = {
        'dnno': dnno,
        'rackno':rackno,
    }

    $('#gasRack').attr('data-url', url);
    $('#gasRack').data('url', url);

    $('#gasRack').attr('data-rackno', rackno);
    $('#gasRack').data('rackno', rackno);

    $('#gasRack').attr('data-dnno', dnno);
    $('#gasRack').data('dnno', dnno);

    // get rack detail
    $.ajax({
        url: urlGet,
        method: 'GET',
        data: data,
    }).done(function (response) {
            $('.rack-overlay').hide();
            if(response != "empty"){
                $('#txtLeggedSling').val(response.sling_no);
                $('#txtGWL').val(response.gwl);
                $('#datepicker-inspDate').datepicker("setDate" , response.inspDate)
                $('#datepicker-testDate').datepicker("setDate" , response.testDate)
            }else{
                $('#txtLeggedSling').val(response.sling_no);
                $('#txtGWL').val(response.gwl);
                $('#datepicker-inspDate').datepicker("setDate" , today)
                $('#datepicker-testDate').datepicker("setDate" , today)
            }
    }).fail(function (response) {
            $('.rack-overlay').hide();
            alert("Rack Details Failed to Display (Network Error)")
    })

});

$('#gasRack').on('click', '.save-detail', function () {
    $('.rack-overlay').show();
    let url = $('#gasRack').data("url");
    let data = {
        'dnno': $('#gasRack').data("dnno"),
        'rackno': $('#gasRack').data("rackno"),
        'gwl': $('#txtGWL').val(),
        'slingno': $('#txtLeggedSling').val(),
        'inspDate': $('#datepicker-inspDate').val(),
        'testDate': $('#datepicker-testDate').val()
    }

    //update rack detail
    $.ajax({
            url: url,
            method: 'POST',
            data: data,
    }).done(function (response) {
            $('.rack-overlay').hide();
            if(response == "Saved"){
                alert("Rack Details Succesfully Updated")
            }
            $('.close-detail').click();
    }).fail(function (response) {
            $('.rack-overlay').hide();
            alert("Rack Details Failed to Update")
    })

});