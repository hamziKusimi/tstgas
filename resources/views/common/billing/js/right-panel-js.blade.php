function arrayRemove(arr, value) {

    return arr.filter(function(ele){
        return ele != value;
    });
 
 }

 let references = [] 
 
 $('#table-form-tbody').find('.reference_no').each(function(index, reference) {
     let row = $(this).parents('tr')
     let data = {
         'item_code': row.find('.item_code').val(),
         'reference_no': $(this).val(),
         'sequence_no': row.find('.sequence_no').val()
     }

     if ($(this).val()) 
        references.push(data)
 })
 

$('.search-document').on('click', function() {
    let modal = $('.search-document-modal-template').clone().removeClass('search-document-modal-template')
    let getRoute = modal.data('get-route')
    let table = modal.find('table')
    // table.clear()

    modal.modal()
    modal.on('hidden.bs.modal', function (e) { $(this).remove() })
    let search = modal.find('.search')
    modal.find('.searchdocno').on('click', function() {
        let data = {
            'search' : search.val(),
        }
        $.ajax({
            data: data,
            method: "POST",
            url: getRoute,
        }).done(function(resources) {
            resources.forEach(resource => {
                let markup = `
                    <tr class="pointer" data-route="${resource.editRoute}">
                        <td class="docNo" width="25%">${resource.docno}</td>
                        <td class="acode" width="15%">${resource.account_code}</td>
                        <td class="name" width="15%">${resource.name}</td>
                        <td class="date" width="15%">${resource.updated_at}</td>
                    </tr>
                `
                table.find('tbody').append(markup)
            });
            // init datatable
            table.on('click', 'tr', function() {
                window.open($(this).data('route'), '_blank')
            })
        })
    })
})

$('#pono-modal').on('shown.bs.modal	', function() {
    let getArraypono = []
    if ($('#pono').val())
        getArraypono = $('#pono').val().split(',')
    
    let modal = $('#pono-modal')
    let getRoute2 = modal.data('get-route2')
    let getRoute = modal.data('get-route')
    let leftSelect = modal.find('.left-select')
    let rightSelect = modal.find('.right-select')

    modal.modal()
    modal.on('hidden.bs.modal', function (e) { 
        modal.off('dblclick', '.left-option')
     }) 
    let getPonoRoute = getRoute.replace('ACC_CODE', $('#debtor-select').val())

    let leftpono = modal.find('.left-select')
    leftpono.empty()

     $.ajax({
            'url': getPonoRoute,
            'method': 'GET'
        }).done(function(response) {
            
            response.forEach(function (item) {         
                // if (still have po items not used, then append, otherwise don't append) {
                // po items dalam existing data
                    console.log(item.dt)
                if (item.dt.length > 0) {
                    let option = new Option(item.docno, item.docno, false, false);
                    leftSelect.append(option)
                    console.log(item)
                }
            })

            leftSelect.find('option').addClass('left-option')
            console.log($('.pono').val())
            modal.on('dblclick', '.left-option', function() {
                let originalPono = $(this).val()
                let pono = $(this).val().replace('/', '')
                
                
                if ($.inArray($(this).val(), getArraypono === -1) && ($('.pono').val() != $(this).val()))
                    getArraypono.push($(this).val())
                console.log(getArraypono)
                $('.pono').val(getArraypono)

                let moveOverOption = $(this).clone().addClass('moved-option').removeClass('left-option')
                
                $(this).remove()                
                let right = rightSelect.append(moveOverOption)

                let selectedItem = response.filter(item => {
                    return item.docno == $(this).val()
                })[0]
                
                selectedItem.dt.forEach(function(dt) {
                        // if dt is already in existing, dont append 
                        // Template in Modal
                        let ptemplate = modal.find('.modal-template-new').clone().removeClass('modal-template-new d-none').addClass(pono) 
                        let uomSelect = ptemplate.find('.punit_measure') 
                        let stockcode = $(".item_code").data('contents').filter(content => content.code == dt.item_code)[0]
                        
                        //Template in DT Table
                        let template = $('.template-new').clone().removeClass('template-new d-none').addClass(pono)
                        let uomSelect2 = template.find('.unit_measure')
                        let rowDetailID = 'row-detail-' + rowID
                        let tSN = leftPad(intTSN, 4)    // 0005
                        let itemCodeSelect = template.find('.item_code')
                        
                        let ponoClassInOutsideTable = dt.item_code + '-' + tSN
                        template.addClass(ponoClassInOutsideTable);

                        uomSelect.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            ptemplate.find('.punit_measure').append(uoms)
                        })
                        uomSelect.on('change', function() {
                            if (uomSelect.val() == stockcode.uom_id1) {
                                ptemplate.find('.prate').val(stockcode.volume)
                            } else if (uomSelect.val() == stockcode.uom_id2) {
                                ptemplate.find('.prate').val(stockcode.uomrate2)
                            } else if (uomSelect.val() == stockcode.uom_id3) {
                                ptemplate.find('.prate').val(stockcode.uomrate3)
                            }
                        })
                        uomSelect.trigger('change')
                                    
                        ptemplate.find('.podocno').val(dt.doc_no)
                        ptemplate.find('.pitem_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        ptemplate.find('.pitem_code').val(dt.item_code).trigger('change')
                        ptemplate.find('.psubject').val(dt.subject)
                        ptemplate.find('.psequence_no').val(dt.sequence_no)
                        ptemplate.find('.pquantity').val(dt.qty)
                        ptemplate.find('.psupp-price').val(dt.cucost)
                        ptemplate.find('.pamount').val(dt.amount)


                        modal.find('#modal-table-form-tbody').append(ptemplate)
                        rowID++
                        
                        // adding newly added item_code to select2
                        itemCodeSelect.select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true, tags: true })
                        let contents = itemCodeSelect.data('contents')
                        contents.forEach(function (item) {
                            let code = item.code
                            let name = item.descr
                            let newItem = new Option(name, code, false, false);
                            itemCodeSelect.append(newItem)
                        })


                        uomSelect2.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            template.find('.unit_measure').append(uoms)
                            if(dt.uom == uom.id)
                                template.find('.unit_measuredt').val(uom.code)
                        })
                        uomSelect2.on('change', function() {
                            if (uomSelect2.val() == stockcode.uom_id1) {
                                template.find('.rate').val(stockcode.volume)
                            } else if (uomSelect2.val() == stockcode.uom_id2) {
                                template.find('.rate').val(stockcode.uomrate2)
                            } else if (uomSelect2.val() == stockcode.uom_id3) {
                                template.find('.rate').val(stockcode.uomrate3)
                            }

                            stockcode.uoms.forEach(function (uom) {
                                if(template.find('.unit_measure').val() == uom.id)
                                    template.find('.unit_measuredt').val(uom.code)
                            })
                        })
                        
                        uomSelect2.trigger('change')
                        template.find('.item_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.item_code').val(dt.item_code).trigger('change')
                        template.find('.subject').val(dt.subject)
                        template.find('.sequence_no').val(tSN)
                        // template.find('.unit_measure').select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true })
                        // template.find('.unit_measure').val(dt.uom)
                        // template.find('.rate').val(dt.rate)  
                        template.find('.quantity').val(dt.qty)
                        ptemplate.find('.pquantity').on('change', function(){
                            let qty = $(this).val()
                            template.find('.quantity').val(qty).trigger('change')
                        })

                        template.find('.cunit_cost').val(dt.cucost)
                        ptemplate.find('.psupp-price').on('change', function(){
                            let suppprice = $(this).val()
                        template.find('.cunit_cost').val(suppprice).trigger('change')
                         })
                        // template.find('.cunit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.reference_no').val(originalPono)
                        template.find('.markup').val(dt.mkup)
                        template.find('.unit_cost').val(dt.ucost)
                        template.find('.unit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.discount').val(dt.discount)
                        template.find('.discount').inputmask({ rightAlign: true })
                        template.find('.amount').val(dt.amount)
                        template.find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.tax_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.tax_rate').val(dt.tax_rate)
                        template.find('.tax_amount').val(dt.tax_amount)
                        template.find('.tax_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').val(dt.taxed_amount)
                        template.find('.toggle-details').attr('data-target', '#' + rowDetailID)
                        template.attr('id', rowDetailID)
                        $('#table-form-tbody').append(template)
                        intTSN += 5
                        
                        //trigger calculation on subtotal
                        $('.cunit_cost').trigger('change')
                        
                        // delete from po also in table
                        ptemplate.find('.remove-row').on('click', function(){
                            $('#table-form-tbody').find('.'+ponoClassInOutsideTable).remove()
                        })
                    // set on event handler
                        onHandler(rowDetailID, template.eq(0), template.eq(1))
                })
                
            })

            modal.on('dblclick', '.moved-option', function() {   
                let val = $(this).val()
                val = val.replace('/', '')
                modal.find('.' + val).remove()  
                $('tbody').find('.' + val).remove()       

                getArraypono = arrayRemove(getArraypono, $(this).val())  
                $('.pono').val(getArraypono)
                
                $(this).remove()
                leftSelect.append($(this).removeClass('moved-option').addClass('left-option'))  
            })
            
            
        })
    

})

$('#dono-modal').on('shown.bs.modal	', function() {
    let getArraydono = []
    if ($('#dono').val())
        getArraydono = $('#dono').val().split(',')
    
    let modal = $('#dono-modal')
    let getRoute2 = modal.data('get-route2')
    let getRoute = modal.data('get-route')
    let leftSelect = modal.find('.left-select')
    let rightSelect = modal.find('.right-select')

    modal.modal()
    modal.on('hidden.bs.modal', function (e) { 
        modal.off('dblclick', '.left-option')
     }) 
    let getDonoRoute = getRoute.replace('DEB_CODE', $('#debtor-select').val())

    let leftdono = modal.find('.left-select')
    leftdono.empty()

     $.ajax({
            'url': getDonoRoute,
            'method': 'GET'
        }).done(function(response) {
            console.log(response)
            response.forEach(function (item) {         
                // if (still have po items not used, then append, otherwise don't append) {
                // po items dalam existing data
                if (item.dt.length > 0) {
                    let option = new Option(item.docno, item.docno, false, false);
                    leftSelect.append(option)
                }
            })

            leftSelect.find('option').addClass('left-option')

            modal.on('dblclick', '.left-option', function() {
                let originalDono = $(this).val()
                let dono = $(this).val().replace('/', '')
                
                if ($.inArray($(this).val(), getArraydono === -1) && ($('.dono').val() != $(this).val())) 
                    getArraydono.push($(this).val())
                console.log(getArraydono)
                $('.dono').val(getArraydono)

                let moveOverOption = $(this).clone().addClass('moved-option').removeClass('left-option')
                
                $(this).remove()                
                let right = rightSelect.append(moveOverOption)

                let selectedItem = response.filter(item => {
                    return item.docno == $(this).val()
                })[0]
                
                selectedItem.dt.forEach(function(dt) {
                        // if dt is already in existing, dont append 
                        // Template in Modal
                        let dtemplate = modal.find('.modal-template-new').clone().removeClass('modal-template-new d-none').addClass(dono) 
                        let uomSelect = dtemplate.find('.dunit_measure') 
                        let stockcode = $(".item_code").data('contents').filter(content => content.code == dt.item_code)[0]
                        
                        //Template in DT Table
                        let template = $('.template-new').clone().removeClass('template-new d-none').addClass(dono)
                        let uomSelect2 = template.find('.unit_measure')
                        let rowDetailID = 'row-detail-' + rowID
                        let tSN = leftPad(intTSN, 4)    // 0005
                        let itemCodeSelect = template.find('.item_code')
                        
                        let donoClassInOutsideTable = dt.item_code + '-' + tSN
                        template.addClass(donoClassInOutsideTable);

                        uomSelect.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            dtemplate.find('.dunit_measure').append(uoms)
                        })
                        uomSelect.on('change', function() {
                            if (uomSelect.val() == stockcode.uom_id1) {
                                dtemplate.find('.drate').val(stockcode.volume)
                            } else if (uomSelect.val() == stockcode.uom_id2) {
                                dtemplate.find('.drate').val(stockcode.uomrate2)
                            } else if (uomSelect.val() == stockcode.uom_id3) {
                                dtemplate.find('.drate').val(stockcode.uomrate3)
                            }
                        })
                        uomSelect.trigger('change')
                                    
                        dtemplate.find('.dodocno').val(dt.doc_no)
                        dtemplate.find('.ditem_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        dtemplate.find('.ditem_code').val(dt.item_code).trigger('change')
                        dtemplate.find('.dsubject').val(dt.subject)
                        dtemplate.find('.dsequence_no').val(dt.sequence_no)
                        dtemplate.find('.dquantity').val(dt.qty)
                        dtemplate.find('.dsupp-price').val(dt.uprice)
                        dtemplate.find('.damount').val(dt.amount)


                        modal.find('#modal-table-form-tbody').append(dtemplate)
                        rowID++
                        
                        // adding newly added item_code to select2
                        itemCodeSelect.select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true, tags: true })
                        let contents = itemCodeSelect.data('contents')
                        contents.forEach(function (item) {
                            let code = item.code
                            let name = item.descr
                            let newItem = new Option(code, code, false, false);
                            itemCodeSelect.append(newItem)
                        })


                        uomSelect2.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            template.find('.unit_measure').append(uoms)
                            if(dt.uom == uom.id)
                                template.find('.unit_measuredt').val(uom.code)
                        })
                        uomSelect2.on('change', function() {
                            if (uomSelect2.val() == stockcode.uom_id1) {
                                template.find('.rate').val(stockcode.volume)
                            } else if (uomSelect2.val() == stockcode.uom_id2) {
                                template.find('.rate').val(stockcode.uomrate2)
                            } else if (uomSelect2.val() == stockcode.uom_id3) {
                                template.find('.rate').val(stockcode.uomrate3)
                            }

                            stockcode.uoms.forEach(function (uom) {
                                if(template.find('.unit_measure').val() == uom.id)
                                    template.find('.unit_measuredt').val(uom.code)
                            })
                        })
                        
                        uomSelect2.trigger('change')
                        template.find('.item_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.item_code').val(dt.item_code).trigger('change')
                        template.find('.subject').val(dt.subject)
                        template.find('.sequence_no').val(tSN)
                        // template.find('.unit_measure').select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true })
                        // template.find('.unit_measure').val(dt.uom)
                        // template.find('.rate').val(dt.rate)  
                        template.find('.quantity').val(dt.qty)
                        dtemplate.find('.dquantity').on('change', function(){
                            let qty = $(this).val()
                            template.find('.quantity').val(qty).trigger('change')
                        })

                        template.find('.unit_price').val(dt.uprice)
                        dtemplate.find('.dsupp-price').on('change', function(){
                            let suppprice = $(this).val()
                        template.find('.unit_price').val(suppprice).trigger('change')
                         })
                        // template.find('.cunit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.reference_no').val(originalDono)
                        template.find('.markup').val(dt.mkup)
                        template.find('.unit_cost').val(dt.ucost)
                        template.find('.unit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.discount').val(dt.discount)
                        template.find('.discount').inputmask({ rightAlign: true })
                        template.find('.amount').val(dt.amount)
                        template.find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.tax_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.tax_rate').val(dt.tax_rate)
                        template.find('.tax_amount').val(dt.tax_amount)
                        template.find('.tax_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').val(dt.taxed_amount)
                        template.find('.toggle-details').attr('data-target', '#' + rowDetailID)
                        template.attr('id', rowDetailID)
                        $('#table-form-tbody').append(template)
                        intTSN += 5
                        
                        //trigger calculation on subtotal
                        $('.cunit_cost').trigger('change')
                        $('.unit_price').trigger('change')
                        
                        // delete from po also in table
                        dtemplate.find('.remove-row').on('click', function(){
                            $('#table-form-tbody').find('.'+donoClassInOutsideTable).remove()
                        })
                    // set on event handler
                        onHandler(rowDetailID, template.eq(0), template.eq(1))
                })
                selectedItem.dt2.forEach(function(dt2) {
                    // if dt is already in existing, dont append 
                    // Template in Modal
                    let dtemplate = modal.find('.modal-template-new').clone().removeClass('modal-template-new d-none').addClass(dono) 
                    let uomSelect = dtemplate.find('.dunit_measure') 
                    let stockcode = $(".item_code").data('contents').filter(content => content.code == dt.item_code)[0]
                    
                    //Template in DT Table
                    let template = $('.template-new').clone().removeClass('template-new d-none').addClass(dono)
                    let uomSelect2 = template.find('.unit_measure')
                    let rowDetailID = 'row-detail-' + rowID
                    let tSN = leftPad(intTSN, 4)    // 0005
                    let itemCodeSelect = template.find('.item_code')
                    
                    let donoClassInOutsideTable = dt.item_code + '-' + tSN
                    template.addClass(donoClassInOutsideTable);

                    uomSelect.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                    stockcode.uoms.forEach(function (uom) {
                        let uoms = new Option(uom.code, uom.id, false, false);
                        dtemplate.find('.dunit_measure').append(uoms)
                    })
                    uomSelect.on('change', function() {
                        if (uomSelect.val() == stockcode.uom_id1) {
                            dtemplate.find('.drate').val(stockcode.volume)
                        } else if (uomSelect.val() == stockcode.uom_id2) {
                            dtemplate.find('.drate').val(stockcode.uomrate2)
                        } else if (uomSelect.val() == stockcode.uom_id3) {
                            dtemplate.find('.drate').val(stockcode.uomrate3)
                        }
                    })
                    uomSelect.trigger('change')
                                
                    dtemplate.find('.dodocno').val(dt.doc_no)
                    dtemplate.find('.ditem_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                    dtemplate.find('.ditem_code').val(dt.item_code).trigger('change')
                    dtemplate.find('.dsubject').val(dt.subject)
                    dtemplate.find('.dsequence_no').val(dt.sequence_no)
                    dtemplate.find('.dquantity').val(dt.qty)
                    dtemplate.find('.dsupp-price').val(dt.uprice)
                    dtemplate.find('.damount').val(dt.amount)


                    modal.find('#modal-table-form-tbody').append(dtemplate)
                    rowID++
                    
                    // adding newly added item_code to select2
                    itemCodeSelect.select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true, tags: true })
                    let contents = itemCodeSelect.data('contents')
                    contents.forEach(function (item) {
                        let code = item.code
                        let name = item.descr
                        let newItem = new Option(code, code, false, false);
                        itemCodeSelect.append(newItem)
                    })


                    uomSelect2.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                    stockcode.uoms.forEach(function (uom) {
                        let uoms = new Option(uom.code, uom.id, false, false);
                        template.find('.unit_measure').append(uoms)
                        if(dt.uom == uom.id)
                            template.find('.unit_measuredt').val(uom.code)
                    })
                    uomSelect2.on('change', function() {
                        if (uomSelect2.val() == stockcode.uom_id1) {
                            template.find('.rate').val(stockcode.volume)
                        } else if (uomSelect2.val() == stockcode.uom_id2) {
                            template.find('.rate').val(stockcode.uomrate2)
                        } else if (uomSelect2.val() == stockcode.uom_id3) {
                            template.find('.rate').val(stockcode.uomrate3)
                        }

                        stockcode.uoms.forEach(function (uom) {
                            if(template.find('.unit_measure').val() == uom.id)
                                template.find('.unit_measuredt').val(uom.code)
                        })
                    })
                    
                    uomSelect2.trigger('change')
                    template.find('.item_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                    template.find('.item_code').val(dt.item_code).trigger('change')
                    template.find('.subject').val(dt.subject)
                    template.find('.sequence_no').val(tSN)
                    // template.find('.unit_measure').select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true })
                    // template.find('.unit_measure').val(dt.uom)
                    // template.find('.rate').val(dt.rate)  
                    template.find('.quantity').val(dt.qty)
                    dtemplate.find('.dquantity').on('change', function(){
                        let qty = $(this).val()
                        template.find('.quantity').val(qty).trigger('change')
                    })

                    template.find('.unit_price').val(dt.uprice)
                    dtemplate.find('.dsupp-price').on('change', function(){
                        let suppprice = $(this).val()
                    template.find('.unit_price').val(suppprice).trigger('change')
                     })
                    // template.find('.cunit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                    template.find('.reference_no').val(originalDono)
                    template.find('.markup').val(dt.mkup)
                    template.find('.unit_cost').val(dt.ucost)
                    template.find('.unit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                    template.find('.discount').val(dt.discount)
                    template.find('.discount').inputmask({ rightAlign: true })
                    template.find('.amount').val(dt.amount)
                    template.find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
                    template.find('.tax_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                    template.find('.tax_rate').val(dt.tax_rate)
                    template.find('.tax_amount').val(dt.tax_amount)
                    template.find('.tax_amount').inputmask('currency', { prefix: '', rightAlign: true })
                    template.find('.taxed_amount').inputmask('currency', { prefix: '', rightAlign: true })
                    template.find('.taxed_amount').val(dt.taxed_amount)
                    template.find('.toggle-details').attr('data-target', '#' + rowDetailID)
                    template.attr('id', rowDetailID)
                    $('#table-form-tbody').append(template)
                    intTSN += 5
                    
                    //trigger calculation on subtotal
                    $('.cunit_cost').trigger('change')
                    $('.unit_price').trigger('change')
                    
                    // delete from po also in table
                    dtemplate.find('.remove-row').on('click', function(){
                        $('#table-form-tbody').find('.'+donoClassInOutsideTable).remove()
                    })
                // set on event handler
                    onHandler(rowDetailID, template.eq(0), template.eq(1))
            })
            })

            modal.on('dblclick', '.moved-option', function() {   
                let val = $(this).val()
                val = val.replace('/', '')
                modal.find('.' + val).remove()  
                $('tbody').find('.' + val).remove()       

                getArraydono = arrayRemove(getArraydono, $(this).val())  
                $('.dono').val(getArraydono)
                
                $(this).remove()
                leftSelect.append($(this).removeClass('moved-option').addClass('left-option'))  
            })
            
            
        })
    

})


$('#qtno-modal').on('shown.bs.modal	', function() {
    let getArrayqtno = []
    if ($('#qtno').val())
    getArrayqtno = $('#qtno').val().split(',')
    
    let modal = $('#qtno-modal')
    let getRoute2 = modal.data('get-route2')
    let getRoute = modal.data('get-route')
    let leftSelect = modal.find('.left-select')
    let rightSelect = modal.find('.right-select')
    
    modal.modal()
    modal.on('hidden.bs.modal', function (e) { 
        modal.off('dblclick', '.left-option')
    }) 
    let getqtnoRoute = getRoute.replace('ACC_CODE', $('#debtor-select').val())
    
    let leftqtno = modal.find('.left-select')
    leftqtno.empty()
    
    $.ajax({
        'url': getqtnoRoute,
        'method': 'GET'
    }).done(function(response) {
            
            response.forEach(function (item) {         
                // if (still have po items not used, then append, otherwise don't append) {
                // po items dalam existing data
                    console.log(item.dt)
                if (item.dt.length > 0) {
                    let option = new Option(item.docno, item.docno, false, false);
                    leftSelect.append(option)
                    console.log(item)
                }
            })

            leftSelect.find('option').addClass('left-option')
            console.log($('.qtno').val())
            modal.on('dblclick', '.left-option', function() {
                let originalqtno = $(this).val()
                let qtno = $(this).val().replace('/', '')
                
                
                if ($.inArray($(this).val(), getArrayqtno === -1) && ($('.qtno').val() != $(this).val()))
                    getArrayqtno.push($(this).val())
                console.log(getArrayqtno)
                $('.qtno').val(getArrayqtno)

                let moveOverOption = $(this).clone().addClass('moved-option').removeClass('left-option')
                
                $(this).remove()                
                let right = rightSelect.append(moveOverOption)

                let selectedItem = response.filter(item => {
                    return item.docno == $(this).val()
                })[0]
                
                selectedItem.dt.forEach(function(dt) {
                        // if dt is already in existing, dont append 
                        // Template in Modal
                        let qtemplate = modal.find('.modal-template-new').clone().removeClass('modal-template-new d-none').addClass(qtno) 
                        let uomSelect = qtemplate.find('.qunit_measure') 
                        let stockcode = $(".item_code").data('contents').filter(content => content.code == dt.item_code)[0]
                        
                        //Template in DT Table
                        let template = $('.template-new').clone().removeClass('template-new d-none').addClass(qtno)
                        let uomSelect2 = template.find('.unit_measure')
                        let rowDetailID = 'row-detail-' + rowID
                        let tSN = leftPad(intTSN, 4)    // 0005
                        let itemCodeSelect = template.find('.item_code')
                        
                        let qtnoClassInOutsideTable = dt.item_code + '-' + tSN
                        template.addClass(qtnoClassInOutsideTable);

                        uomSelect.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            qtemplate.find('.qunit_measure').append(uoms)
                        })
                        uomSelect.on('change', function() {
                            if (uomSelect.val() == stockcode.uom_id1) {
                                qtemplate.find('.qrate').val(stockcode.volume)
                            } else if (uomSelect.val() == stockcode.uom_id2) {
                                qtemplate.find('.qrate').val(stockcode.uomrate2)
                            } else if (uomSelect.val() == stockcode.uom_id3) {
                                qtemplate.find('.qrate').val(stockcode.uomrate3)
                            }
                        })
                        uomSelect.trigger('change')
                                    
                        qtemplate.find('.qodocno').val(dt.doc_no)
                        qtemplate.find('.qitem_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        qtemplate.find('.qitem_code').val(dt.item_code).trigger('change')
                        qtemplate.find('.qsubject').val(dt.subject)
                        qtemplate.find('.qsequence_no').val(dt.sequence_no)
                        qtemplate.find('.qquantity').val(dt.qty)
                        qtemplate.find('.qsupp-price').val(dt.cucost)
                        qtemplate.find('.qamount').val(dt.amount)


                        modal.find('#modal-table-form-tbody').append(qtemplate)
                        rowID++
                        
                        // adding newly added item_code to select2
                        itemCodeSelect.select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true, tags: true })
                        let contents = itemCodeSelect.data('contents')
                        contents.forEach(function (item) {
                            let code = item.code
                            let name = item.descr
                            let newItem = new Option(name, code, false, false);
                            itemCodeSelect.append(newItem)
                        })


                        uomSelect2.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            template.find('.unit_measure').append(uoms)
                            if(dt.uom == uom.id)
                                template.find('.unit_measuredt').val(uom.code)
                        })
                        uomSelect2.on('change', function() {
                            if (uomSelect2.val() == stockcode.uom_id1) {
                                template.find('.rate').val(stockcode.volume)
                            } else if (uomSelect2.val() == stockcode.uom_id2) {
                                template.find('.rate').val(stockcode.uomrate2)
                            } else if (uomSelect2.val() == stockcode.uom_id3) {
                                template.find('.rate').val(stockcode.uomrate3)
                            }

                            stockcode.uoms.forEach(function (uom) {
                                if(template.find('.unit_measure').val() == uom.id)
                                    template.find('.unit_measuredt').val(uom.code)
                            })
                        })
                        
                        uomSelect2.trigger('change')
                        template.find('.item_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.item_code').val(dt.item_code).trigger('change')
                        template.find('.subject').val(dt.subject)
                        template.find('.sequence_no').val(tSN)
                        // template.find('.unit_measure').select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true })
                        // template.find('.unit_measure').val(dt.uom)
                        // template.find('.rate').val(dt.rate)  
                        template.find('.quantity').val(dt.qty)
                        qtemplate.find('.qquantity').on('change', function(){
                            let qty = $(this).val()
                            template.find('.quantity').val(qty).trigger('change')
                        })

                        template.find('.cunit_cost').val(dt.cucost)
                        qtemplate.find('.qsupp-price').on('change', function(){
                            let suppprice = $(this).val()
                        template.find('.cunit_cost').val(suppprice).trigger('change')
                         })
                        // template.find('.cunit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.reference_no').val(originalqtno)
                        template.find('.markup').val(dt.mkup)
                        template.find('.unit_cost').val(dt.ucost)
                        template.find('.unit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.discount').val(dt.discount)
                        template.find('.discount').inputmask({ rightAlign: true })
                        template.find('.amount').val(dt.amount)
                        template.find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.tax_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.tax_rate').val(dt.tax_rate)
                        template.find('.tax_amount').val(dt.tax_amount)
                        template.find('.tax_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').val(dt.taxed_amount)
                        template.find('.toggle-details').attr('data-target', '#' + rowDetailID)
                        template.attr('id', rowDetailID)
                        $('#table-form-tbody').append(template)
                        intTSN += 5
                        
                        //trigger calculation on subtotal
                        $('.cunit_cost').trigger('change')
                        
                        // delete from po also in table
                        qtemplate.find('.remove-row').on('click', function(){
                            $('#table-form-tbody').find('.'+qtnoClassInOutsideTable).remove()
                        })
                    // set on event handler
                        onHandler(rowDetailID, template.eq(0), template.eq(1))
                })
                
            })

            modal.on('dblclick', '.moved-option', function() {   
                let val = $(this).val()
                val = val.replace('/', '')
                modal.find('.' + val).remove()  
                $('tbody').find('.' + val).remove()       

                getArrayqtno = arrayRemove(getArrayqtno, $(this).val())  
                $('.qtno').val(getArrayqtno)
                
                $(this).remove()
                leftSelect.append($(this).removeClass('moved-option').addClass('left-option'))  
            })
            
            
        })
    

})


$('#dnno-modal').on('shown.bs.modal	', function() {
    let getArraydnno = []
    if ($('#dnno').val())
        getArraydnno = $('#dnno').val().split(',')
    
    let modal = $('#dnno-modal')
    let getRoute2 = modal.data('get-route2')
    let getRoute = modal.data('get-route')
    let leftSelect = modal.find('.left-select')
    let rightSelect = modal.find('.right-select')

    modal.modal()
    modal.on('hidden.bs.modal', function (e) { 
        modal.off('dblclick', '.left-option')
     }) 
    let getDnnoRoute = getRoute.replace('DEB_CODE', $('#debtor-select').val())

    let leftdnno = modal.find('.left-select')
    leftdnno.empty()

     $.ajax({
            'url': getDnnoRoute,
            'method': 'GET'
        }).done(function(response) {
            console.log(response)
            response.forEach(function (item) { 
                // if (still have po items not used, then append, otherwise don't append) {
                // dn items dalam existing data
                if (item.dt3.length > 0) {
                    let option = new Option(item.dn_no, item.dn_no, false, false);
                    leftSelect.append(option)
                }
            })

            leftSelect.find('option').addClass('left-option')

            modal.on('dblclick', '.left-option', function() {
                console.log('here')
                let originalDnno = $(this).val()
                let dnno = $(this).val().replace('/', '')
                
                if ($.inArray($(this).val(), getArraydnno === -1) && ($('.dnno').val() != $(this).val())) 
                    getArraydnno.push($(this).val())
                $('.dnno').val(getArraydnno)

                let moveOverOption = $(this).clone().addClass('moved-option').removeClass('left-option')
                
                $(this).remove()                
                let right = rightSelect.append(moveOverOption)

                let selectedItem = response.filter(item => {
                    return item.dn_no == $(this).val()
                })[0]
                console.log(selectedItem)
                selectedItem.dt3.forEach(function(dt3) {
                        // if dt is already in existing, dont append 
                        // Template in Modal
                        let dtemplate = modal.find('.modal-template-new').clone().removeClass('modal-template-new d-none').addClass(dnno) 
                        let uomSelect = dtemplate.find('.dunit_measure') 
                        let cylinder = $(".serial").data('contents').filter(content => content.serial == dt3.serial)[0]
                        
                        //Template in DT Table
                        let template = $('.template-new').clone().removeClass('template-new d-none').addClass(dnno)
                        let uomSelect2 = template.find('.unit_measure')
                        let rowDetailID = 'row-detail-' + rowID
                        let tSN = leftPad(intTSN, 4)    // 0005
                        let itemCodeSelect = template.find('.serial')
                        
                        let dnnoClassInOutsideTable = dt3.serial + '-' + tSN
                        template.addClass(dnnoClassInOutsideTable);
                                    
                        dtemplate.find('.dndn_no').val(dt3.dn_no)
                        dtemplate.find('.dserial').val(dt3.serial)
                        dtemplate.find('.dbarcode').val(dt3.barcode)
                        dtemplate.find('.dsubject').val(dt3.descr)
                        dtemplate.find('.dsequence_no').val(dt3.sequence_no)
                        dtemplate.find('.dquantity').val(1)
                        dtemplate.find('.dsupp-price').val(dt3.daily_price)
                        dtemplate.find('.damount').val(dt3.daily_price)


                        modal.find('#modal-table-form-tbody').append(dtemplate)
                        rowID++
                                                
                        let dnroute = "{{ route('deliverynote.get.cylinder', 'SERIAL') }}"
                        let dnroute2 = "{{ route('deliverynote.get.gasrack', 'SERIAL') }}"
                        let dngetroute = dnroute.replace('SERIAL', dt3.serial)
                        let data = ''

                        if(dt3.type == 'cy'){
                            $.ajax({
                                'url': dngetroute,
                                'method': 'GET'
                            }).done(function(response) {
                                    dtemplate.find('.dsubject').val(response.descr)
                                    template.find('.subject').val(response.descr)
                                    template.find('.product').val(response.product.code)
                                    template.find('.type').val(response.category.code)
                            
                            
                                
                            })
                        }else if(dt3.type == 'gr'){
                            let dngetroute2 = dnroute2.replace('SERIAL', dt3.serial)
                            $.ajax({
                                'url': dngetroute2,
                                'method': 'GET'
                            }).done(function(resource) {
                                console.log(resource)    
                                    dtemplate.find('.dsubject').val(resource.description)
                                    template.find('.subject').val(resource.description)
                                    template.find('.type').val(resource.gstype.code)    
                            })
                        }
                        
                        template.find('.serial').val(dt3.serial)
                        template.find('.barcode').val(dt3.barcode)
                        template.find('.subject').val(dt3.subject)
                        template.find('.sequence_no').val(tSN)
                        template.find('.quantity').val(1)
                        dtemplate.find('.dquantity').on('change', function(){
                            let qty = $(this).val()
                            template.find('.quantity').val(qty).trigger('change')
                        })

                        template.find('.unit_price').val(dt3.daily_price)
                        dtemplate.find('.dsupp-price').on('change', function(){
                            let suppprice = $(this).val()
                        template.find('.unit_price').val(suppprice).trigger('change')
                         })
                        // template.find('.cunit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.reference_no').val(originalDnno)
                        template.find('.uom').val(dt3.uom)
                        // template.find('.product').val(dt3.product)
                        // template.find('.type').val(dt3.category)
                        template.find('.discount').val(dt3.discount)
                        template.find('.discount').inputmask({ rightAlign: true })
                        template.find('.amount').val(dt3.daily_price)
                        template.find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.toggle-details').attr('data-target', '#' + rowDetailID)
                        template.attr('id', rowDetailID)
                        
                        template.find('#search-cyl-btn').prop('hidden', true)
                        template.find('.sequence_no').trigger('change')
                        template.find('.sequence_no').on('change', function(){
                            console.log('sequnce trigger')
                        })
                        intTSN += 5
                        $('#table-form-tbody').append(template)
                        
                        //trigger calculation on subtotal
                        $('.cunit_cost').trigger('change')
                        $('.unit_price').trigger('change')
                        
                        // delete from po also in table
                        dtemplate.find('.remove-row').on('click', function(){
                            $('#table-form-tbody').find('.'+dnnoClassInOutsideTable).remove()
                        })
                    // set on event handler
                        onHandler(rowDetailID, template.eq(0), template.eq(1))
                })
            })

            modal.on('dblclick', '.moved-option', function() {   
                let val = $(this).val()
                val = val.replace('/', '')
                modal.find('.' + val).remove()  
                $('tbody').find('.' + val).remove()       

                getArraydnno = arrayRemove(getArraydnno, $(this).val())  
                $('.dnno').val(getArraydnno)
                
                $(this).remove()
                leftSelect.append($(this).removeClass('moved-option').addClass('left-option'))  
            })
            
            
        })
    

})