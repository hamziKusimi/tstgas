let systemSettings = $('.system-settings-param')
let boolRounding = systemSettings.data('rounding')
let mainTableCounter = 0;

let leftPad = function (value, length) {
    return value
    // return ('0'.repeat(length) + value).slice(-length);
}


// custom select2
function formatStateInTable(state) {
    return state.id
}

// overiding all current select2 and summernote in the table forms into using formatStateInTable
$('.form-select2').select2({
    templateSelection: formatStateInTable,
    dropdownAutoWidth: true
})

// when page load, aku always initialize rowID to 0

let taxCodes = $('.tax-container').data('taxes')

$('#item-master-datatable').DataTable()

let intTSN = 0;
let tot_seq = $('.sequence_no').length;

if (tot_seq > 1) {
    $('.sequence_no').each(function (index) {
        if (index === tot_seq - 1) {
            intTSN = parseInt($(this).val()) + 1;
        }
    })
} else {
    intTSN = 1;
}

let rowID = 0;
let tot_seq_rowID = $('.grcontent-id').length;

if (tot_seq_rowID > 1) {
    $('.grcontent-id').each(function (index) {

        if (index === tot_seq_rowID - 1) {
            rowID = parseInt($(this).val()) + 1;
        }
    })
} else {
    rowID = 1;
}

// trigger on click new button
$('#add-new').on('click', function () {

    //must fill in debtor account code before return note
    if($('#debtor-select').val() == ''){
        alert('Please Fill In Customer Details')
        location.reload();
    }else{
    mainTableCounter = mainTableCounter + 1
    rowID++ // sini, tambah satu
    let template = $('.main-table-row-template').clone().removeClass('main-table-row-template d-none').addClass('main-row')
    let secondRowTemplate = $('.second-row-template').clone().removeClass('second-row-template').addClass('second-row')

    //initialize each main row and child table main-row,row-id
    let mainRowIdentifier = 'main-row-id-' + rowID
    let secondRowIdentifier = 'row-id-' + rowID
    template.addClass(mainRowIdentifier)
    template.attr('data-row-id', rowID)
    secondRowTemplate.addClass(secondRowIdentifier)
    secondRowTemplate.attr('data-row-id', rowID)

    let rowDetailID = 'row-detail-' + rowID
    let tSN = leftPad(intTSN, 0) // 0005

   
    // start cloning
    if((preparecylinder == true) || (loadcylinder == true)){

        if(($('.main-row').find('.category').val() == categorymanyfor) && 
        ($('.main-row').find('.product').val() == productmanyfor) &&
        ($('.main-row').find('.capacity').val() == capacitymanyfor) &&
        ($('.main-row').find('.pressure').val() == pressuremanyfor)){
        }else{
            
            template.eq(0).find('.sequence_no').val(tSN)
            // template.eq(0).find('.category').select2('data', {id: 'cy cat', a_key: 'cy cat'})
            // console.log(categorymanyfor)
            // template.eq(0).find('.category').val(categorymanyfor).trigger('change')
            // template.eq(0).find('.product').val(productmanyfor).trigger('change')
            // template.eq(0).find('.capacity').val(capacitymanyfor).trigger('change')
            // template.eq(0).find('.pressure').val(pressuremanyfor).trigger('change')
            // template.eq(0).find('.quantity').val(1)
            // template.eq(0).find('.price').attr("required", true);
            template.eq(0).find('.toggle-inner-table-button').attr('data-second-row-id', secondRowIdentifier)
            if(triggerexist == false){
                console.log('gni')
                template.eq(0).find('.exist-cylinder').addClass('has-cylinder')
                template.eq(0).find('.exist-cylinder').html('Exist Cylinder')
                triggerexist == true
            }else{
                console.log('gni1')
                template.eq(0).find('.exist-cylinder').addClass('no-cylinder')
                template.eq(0).find('.exist-cylinder').html('Not Exist Cylinder')
                triggerexist == true
            }
            // template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)
            
            $('#table-form-tbody').append(template)
            $('#table-form-tbody').append(secondRowTemplate)
            
            // after adding to the tbody, increment sequence_no for the upcoming button click
            intTSN += 1
        }

    }else{
        template.eq(0).find('.sequence_no').val(tSN)
        // template.eq(0).find('.category').select2({
        //     templateSelection: formatStateInTable,
        //     dropdownAutoWidth: true
        // })
        // template.eq(0).find('.product').select2({
        //     templateSelection: formatStateInTable,
        //     dropdownAutoWidth: true
        // })
        // template.eq(0).find('.capacity').select2({
        //     templateSelection: formatStateInTable,
        //     dropdownAutoWidth: true
        // })
        // template.eq(0).find('.pressure').select2({
        //     templateSelection: formatStateInTable,
        //     dropdownAutoWidth: true
        // })
        // template.eq(0).find('.quantity').val(1)
        // template.eq(0).find('.price').attr("required", true);
        template.eq(0).find('.toggle-inner-table-button').attr('data-second-row-id', secondRowIdentifier)
        if(triggerexist == false){
            console.log('gni2')
            template.eq(0).find('.exist-cylinder').addClass('has-cylinder')
            template.eq(0).find('.exist-cylinder').html('Exist Cylinder')
        }else{
            console.log('gni3')
            template.eq(0).find('.exist-cylinder').addClass('no-cylinder')
            template.eq(0).find('.exist-cylinder').html('Not Exist Cylinder')
        }
        // template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)
        
        $('#table-form-tbody').append(template)
        $('#table-form-tbody').append(secondRowTemplate)
        
        // after adding to the tbody, increment sequence_no for the upcoming button click
        intTSN += 1
    }

    // set on event handler
    // onHandler(rowDetailID, template.eq(0), template.eq(1))
    }
})

//Edit on adding new row-id-(i)

$('.existing-data').each(function () {

    rowID++

})


// predefined actions to be done
function onHandler(rowDetailID) {

    //delete the main row
    $('.remove-row').on('click', function () {
        $(this).closest('tr').next('tr').remove()
        $(this).parent().parent().remove()
        // $("#add-new").removeClass('d-none')
    })


}

//dynamically get function on click based on opinion dt, remove-row-child

$('#opiniondt').on('click', '.remove-row-child', function () {

    let row = $(this).parents('.new-row');
    let route = $(this).data('delete-routelist');
    let unloadingDate = row.find("td:eq(7)").text();
    let loadingDate = row.find("td:eq(6)").text();
    let isExistingData = row.data("isexisted");

    $('.delete-overlay').show();
    if (unloadingDate === "" && loadingDate === "") {

        if (confirm('This Action Will Remove Cylinder Data From The List, Are You Sure?')) {
            let method = "POST"
            let data = {
                'dnno': $(this).data('dnno'),
                'sqnex': $(this).data('sqnex'),
                'sqnlist': $(this).data('sqnlist'),
                'type': 'cy'
            }

            // delete from db
            $.ajax({
                url: route,
                method: method,
                data: data,
            }).done(function (response) {
                $('.delete-overlay').hide();
                alert("Loading and Unloading Data Removed");
                row.remove()
            }).fail(function (response) {
                alert("Error: " + response);
            })
        } else {
            $('.delete-overlay').hide();
            return false
        }

    } else if (unloadingDate === "") {

        let method = "POST"
        let data = {
            'dnno': $(this).data('dnno'),
            'sqnex': $(this).data('sqnex'),
            'sqnlist': $(this).data('sqnlist'),
            'type': 'cy',
            'only-LL': true
        }

        $.ajax({
            url: route,
            method: method,
            data: data,
        }).done(function (response) {
            $('.delete-overlay').hide();
            alert("loading Data Removed");
            row.find("td:eq(6)").empty();
            var input = '<input class="form-control loadingdt calculate-field" readonly="" name="edit_ll_date[]" type="text">'
            input += '<input name="edit_ll_barcode[]" type="hidden" value="' + row.find('.barcode').html() + '">';
            input += '<input name="edit_ll_serial[]" type="hidden" value="' + row.find('.serialno').html() + '">';
            input += '<input name="edit_ll_sequence_no[]" type="hidden" value="' + response.sqnex + '">';
            input += '<input name="edit_ll_sqn_no[]" type="hidden" value="' + response.sqnlist + '">';
            input += '<input name="edit_ll_datetime[]" class="datetime" type="hidden" value="">';
            input += '<input name="edit_ll_driver[]" type="hidden" value="' + row.find('.driver').val() + '">';
            input += '<input name="edit_ll_type[]" type="hidden" value="cy">';

            row.find("td:eq(6)").append(input);

        }).fail(function (response) {
            alert("Error: " + response);
            console.log(response)
        })
    } else if (isExistingData === undefined) {
        row.remove()
        $('.delete-overlay').hide();
    } else {
        let method = "POST"
        let data = {
            'dnno': $(this).data('dnno'),
            'sqnex': $(this).data('sqnex'),
            'sqnlist': $(this).data('sqnlist'),
            'type': 'cy',
            'only-UL': true
        }

        var sqnlist = row.data('sqnlist');
        $.ajax({
            url: route,
            method: method,
            data: data,
        }).done(function (response) {
            $('.delete-overlay').hide();
            alert("Unloading Data Removed");
            row.find("td:eq(7)").empty();
            var input = '<input class="form-control uloadingdt calculate-field" readonly="" name="edit_ul_date[]" type="text">'
            input += '<input name="edit_ul_barcode[]" type="hidden" value="' + row.find('.barcode').html() + '">';
            input += '<input name="edit_ul_serial[]" type="hidden" value="' + row.find('.serialno').html() + '">';
            input += '<input name="edit_ul_sequence_no[]" type="hidden" value="' + response.sqnex + '">';
            input += '<input name="edit_ul_sqn_no[]" type="hidden" value="' + response.sqnlist + '">';
            input += '<input name="edit_ul_datetime[]" class="datetime" type="hidden" value="">';
            input += '<input name="edit_ul_driver[]" type="hidden" value="' + row.find('.driver').val() + '">';
            input += '<input name="edit_ul_type[]" type="hidden" value="cy">';

            row.find("td:eq(7)").append(input);

        }).fail(function (response) {
            alert("Error: " + response);
            console.log(response)
        })
    }

})

$('#opiniondt').on('click', '.remove-row', function () {

    let row = $(this).parents('.sortable-row')
    let route = $(this).data('delete-route');
    let isExistingData = row.data("existed");
    let method = "DELETE"

    if (isExistingData === undefined) {
        $(this).closest('tr').next('tr').remove()
        $(this).parent().parent().remove()
    } else {
        if (confirm('This Action Will Remove The Cylinder, Are You Sure?')) {
            // delete from db
            $(this).closest('tr').next('tr').remove()
            $(this).parent().parent().remove()
            $.ajax({
                url: route,
                method: method,
            }).done(function (response) {
                alert("Succesfully Removed");
                console.log(response)
            }).fail(function (response) {
                alert("Error: " + response);
                console.log(response)
            })
        } else {
            // Do nothing!
            return false;
        }
    }

})
