
$(document).ready(function () {
    // let sqn = $('.newrow').find('.sqn_no').val()
    // $('.sqn_no').each(function () { sqn += 1 })
    
    let sqn = 1;
    $('.sqn_no').each(function () { sqn += 1 })

    function innerTableTbodyRowFormat2() {
      return `
          <tr class="narrow-padding new-row2 text-sm inner-row">
              <td>
                {!! Form::hidden("dt_sqn_no[]", null, ["class" => "form-control dt_sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("sqn_no[]", null, ["class" => "form-control sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("type[]", "gr", ["class" => "form-control type", "placeholder"=>"sqn_no", "readonly"]) !!}
                  {!! Form::text("barcode[]", null, ["class" => "form-control barcode", "placeholder"=>"Barcode", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("serialno[]", null, ["class" => "form-control serialno", "placeholder"=>"Serial No", "readonly"]) !!}
              </td>
              <td>
                 {!! Form::text("descr[]", null, [ "class" => "form-control descr" , "readonly"]) !!}
             </td>
              <td>
                  {!! Form::text("driverdt[]", null, [ "class" => "form-control driverdt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("dnnodt[]", null, [ "class" => "form-control dnnodt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("dndatedt[]", null, [ "class" => "form-control dndatedt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("loadingdt[]", null, ["class" => "form-control loadingdt calculate-field", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("uloadingdt[]", null, ["class" => "form-control uloadingdt calculate-field", "readonly"]) !!}
              </td>
              <td>
                  <button type="button" class="btn btn-danger remove-row-child" data-id2="0">
                      <i class="fa fa-times"></i>
                  </button>
              </td>
          </tr>
        `
    }

    $('#gasrackdt').on('click', '.toggle-inner-table-button', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#gasrackdt').find('.'+secondRowIdentifier)

        // now we want to show the table or hide it
        if (row.hasClass('d-none'))
            row.removeClass('d-none')     // if its hidden, show it, when button is clicked
        else
            row.addClass('d-none')        // if its shown, hide it, when button is clicked

    })

    $('#gasrackdt').on('click', '.toggle-inner-table-button-exist', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#gasrackdt').find('.'+secondRowIdentifier)
        // now we want to show the table or hide it
        if (row.hasClass('d-none'))
            row.removeClass('d-none')     // if its hidden, show it, when button is clicked
        else
            row.addClass('d-none')        // if its shown, hide it, when button is clicked

    })

    //Check if the main row have matched value with the loading list in uloading list
    let isMatched = function(selectedBarcode2, mainRow) {
        let barcodell = $('#barcodell').val()
        barcodell = barcodell.trim()
        let serialll = $('#serialll').val()
        serialll = serialll.trim()

        if(barcodell == ''){
            if (selectedBarcode2.serial == serialll)
            {
                return true    
            }
        }else{
            if (selectedBarcode2.barcode == barcodell)
            {
                return true    
            }
        }

        return false
    }

    /**
     * On Change Barcode,
     * 1. compare the value of barcode against database records
     * 2. if found, search the existing "category" & "product" in the main-table above
     *      3. if found, add a new-row to the inner-table of this main-table
     * 
     * NOTE: 
     *      - we are comparing the barcode value against the first tr (main-row)
     *      - we only use row-id to find the next tr (second-row)
     * 
     *          <tr class="main-row"> </tr> 
     *          <tr class="second-row"> </tr>
     * * */
    
    $('#load-gas').click(function () {
        let existingTrigger = []
        let isAdded = false

        let existRows2 = $('#table-form-tbody-gasrack').find('.existing-data2')
        existRows2.each(function (index) {
            grtriggerexist = true
            grtriggernotexist = true
            existingTrigger.push('2')
            isAdded = true
        })

        triggerformany = true
        if(grtriggerexist == false){
            
            $('#add-new2').trigger('click')
            grtriggerexist = true
        }

        if(grtriggernotexist == false){
            console.log('msuk sni')
            $('#add-new2').trigger('click')
            grtriggernotexist = true
        }
        let barcode = $('#barcodell').val()
        let serial = $('#serialll').val()
        let dbRecords2 = $('#barcodell').data('gs');
        let card = $(this).parents('.card')
        let date = card.find('.invoice-date')
        let driver = card.find('.driverll') 
        console.log(serial)
        console.log(barcode)
        
        if(barcode == ''){
            serial = serial.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.serial == serial
            })[0]
        }else{
            barcode = barcode.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.barcode == barcode
            })[0]
        }
        let selectedBarcodeManyfor = [] 
        if(selectedBarcode2){
            
            selectedBarcodeManyfor = selectedBarcode2.gscylinder
        }

        
        let existRows = $('#gasrackdt').find('.existing-data2');

        existRows.each(function (index) {
            console.log('mmmaaaa')
            let mainRow = $(this)
            let id =  $(this).data('row-id2')
            let secondRow = $(this).siblings('.row-id2-' + id)
          
            let gasrackTable = secondRow.find('.gasrack-table')
            let gasrackTableRow = gasrackTable.find('.new-row2')
            if (selectedBarcode2 && isMatched(selectedBarcode2, mainRow) ) {
                // generating new element and inputting the value
                let broute = $('#barcodell').data('item4');
                let route = broute.replace('ACC_CODE', $('#debtor-select').val())
                let barcodecy = selectedBarcode2.barcode
                let serialcy = selectedBarcode2.serial
                console.log(barcodecy)
                console.log(serialcy)
                let item = ''
                let dnno = ''
                let dndate =''
                if (barcodecy == '') {
                    item = serialcy
                } else {
                    item = barcodecy
        
                }
                let data = {
                    'item': item,
                }
                if (item != '') {
                $.ajax({
                    url: route,
                    method: "GET",
                    data: data,
                    }).done(function (response) {
                        var itemresponse = response[0]
                        // dnno = itemresponse.
                        

                    }).fail(function (response) {
                        console.log("Error" + response);
                        $('.loading-overlay').hide();
                    })
                }
                console.log('snn')
                let newRow = $(innerTableTbodyRowFormat2())
                newRow.find('.dt_sqn_no').val(mainRow.find('.grsequence_no').val())
                newRow.find('.barcode').val(selectedBarcode2.barcode)
                newRow.find('.serialno').val(selectedBarcode2.serial)
                newRow.find('.descr').val(selectedBarcode2.description)
                newRow.find('.loadingdt').val(date.val())       
                newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                newRow.find('.sqn_no').val(sqn)
                sqn += 1
                $('.serialll').val(selectedBarcode2.serial)
            
                // newRow.find('.loadingdt').val($('.invoice-date').val())     // on page load
                // check if there's existing row in the gasrack-table
                if (gasrackTableRow.length > 0) {
                    // We need to check one condition: the table does not contain the same barcode value (barcodell)
                    let existingValues = []
                    gasrackTableRow.each(function() {
                        existingValues.push($(this).find('.barcode').html())
                    })

                    if ($.inArray(selectedBarcode2.barcode, existingValues) == -1){
                        alert("Loading Gasrack Succesfully Added");
                        isAdded = true;

                        gasrackTable.find(".gasrack").append(newRow)
                        $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                        $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)

                        let type = $('.main-row2-id-' + id).find('.grtype').val()
                        $('.main-row2-id-' + id).find('.grtypedt').val(type)
                                                 
                        //add cylinder automatically if exist

                        
                        $.each(selectedBarcodeManyfor, function(index, value){
                            barcodemanyfor = value.cy_barcode
                            serialmanyfor = value.cy_serial
                            categorymanyfor = value.cy_category
                            productmanyfor = value.cy_product
                            descrmanyfor = value.cy_descr
                            capacitymanyfor = value.cy_capacity
                            pressuremanyfor = value.cy_pressure
                            loadcylinder = true
                            if(triggerexist == false){
                                // $('#add-new').trigger('click')
                                triggerexist = true
                                triggerformany = false
                            }
                            $('#load-cyl').trigger('click')
                            loadcylinder = false
                        })  
                        $('#barcodell').val('')
                        $('#serialll').val('')

                    }else{  let newRow = $(innerTableTbodyRowFormat2())
                        newRow.find('.dt_sqn_no').val('2')
                        newRow.find('.barcode').val(selectedBarcode2.barcode)
                        newRow.find('.serialno').val(selectedBarcode2.serial)
                        newRow.find('.loadingdt').val(date.val())       
                        newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                        newRow.find('.sqn_no').val(sqn)
                        sqn += 1
                        gasrackTable.find(".gasrack").append(newRow)
                        alert("Gasrack Already Exist");
                        isAdded = true;
                        $('#barcodell').val('')
                        $('#serialll').val('')
                    }

    
                } else {
                    $.each(selectedBarcodeManyfor, function(index, value){
                        console.log('kkk')
                        barcodemanyfor = value.cy_barcode
                        serialmanyfor = value.cy_serial
                        categorymanyfor = value.cy_category
                        productmanyfor = value.cy_product
                        descrmanyfor = value.cy_descr
                        capacitymanyfor = value.cy_capacity
                        pressuremanyfor = value.cy_pressure
                        loadcylinder = true
                        
                        let CyexistRows = $('#opiniondt').find('.existing-data')
                        CyexistRows.each(function (index) {
                            triggerexist = true
                            triggernotexist = true
                            existingTrigger.push('2')
                            console.log('satu')
                        })
                        
                        if(triggerexist == false){
                            console.log('inin')
                            $('#add-new').trigger('click')
                            triggerexist = true
                            triggerformany = false
                        }
                        $('#load-cyl').trigger('click')
                        loadcylinder = false


                    })

                    // Directly append whatever value that we found/
                    alert("Loading Gasrack Succesfully Added");
                    isAdded = true;

                    gasrackTable.find(".gasrack").append(newRow)
                    $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                    $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)
                    
                    let type = $('.main-row2-id-' + id).find('.grtype').val()
                    $('.main-row2-id-' + id).find('.grtypedt').val(type)
                    $('#barcodell').val('')
                    $('#serialll').val('')
                
                    return false
                }

                // exit the loop
                return false
            }else{
                console.log('llll')
                let existingValues = []
                let existingValuesSerial = []
                gasrackTableRow.each(function () {
                    existingValues.push($(this).find('.barcode').val())
                    existingValues.push($(this).find('.barcode').html())
                    existingValuesSerial.push($(this).find('.serialno').val())
                    existingValuesSerial.push($(this).find('.serialno').html())
                })

                if (($.inArray(barcode, existingValues) == -1) || ($.inArray(serial, existingValuesSerial) == -1)){
                    console.log('check barcode ndak sma klu sdh exist dlam row bru append')
                    // if(maintable.find('.sequence_no').val() == '2'){
                    let Notexistgasrack = $('#table-form-tbody-gasrack').find('.Notexistgasrack')
                    let td = Notexistgasrack.parent('td')
                    let tr = td.parent('tr')
                    let id = tr.data('row-id2')
                    let appendRow = $('#table-form-tbody-gasrack').find(".row-id2-" + id)
                    

                    let newRow = $(innerTableTbodyRowFormat2())
                    newRow.find('.dt_sqn_no').val('2')
                    newRow.find('.barcode').val($('#barcodell').val())
                    newRow.find('.serialno').val($('#serialll').val())
                    newRow.find('.descr').val(null)
                    newRow.find('.loadingdt').val(date.val())       
                    newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                    newRow.find('.sqn_no').val(sqn)
                    sqn += 1
                    appendRow.find(".gasrack").append(newRow)
                    isAdded = true;
                    $('#barcodell').val('')
                    $('#serialll').val('')
                    return false
                }
            }
            isAdded = true
            return false
        })

        if (!isAdded) {
            console.log('fffffff')
            let mainRows = $('#gasrackdt').find('.main-row2')
            // this loop is to find the second row
            total_row = mainRows.length;
            
            mainRows.each(function(index) {
                let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)
                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                
                if (selectedBarcode2 && isMatched(selectedBarcode2, mainRow) ) {
                    // generating new element and inputting the value
                    let broute = $('#barcodell').data('item4');
                    let route = broute.replace('ACC_CODE', $('#debtor-select').val())
                    let barcodecy = selectedBarcode2.barcode
                    let serialcy = selectedBarcode2.serial
                    let item = ''
                    let newRow = $(innerTableTbodyRowFormat2())
                    if (barcodecy == '') {
                        item = serialcy
                        isBarcode = false
                    } else {
                        item = barcodecy
                        isBarcode = true
            
                    }
                    let data = {
                        'item': item,
                        'isBarcode': isBarcode
                    }
                    if (item != '') {
                    $.ajax({
                        url: route,
                        method: "GET",
                        data: data,
                        }).done(function (response) {
                            console.log('gasrack sni 2')
                            var itemresponse = response[0]
                            newRow.find('.dnnodt').val(itemresponse.dnno)
                            newRow.find('.dndatedt').val(itemresponse.dnno_date)
                            dnno = itemresponse.dnno
                            dndate = itemresponse.dnno_date
                            
    
                        }).fail(function (response) {
                            console.log("Error" + response);
                            $('.loading-overlay').hide();
                        })
                    }
                    newRow.find('.dt_sqn_no').val(mainRow.find('.grsequence_no').val())
                    newRow.find('.barcode').val(selectedBarcode2.barcode)
                    newRow.find('.serialno').val(selectedBarcode2.serial)
                    newRow.find('.descr').val(selectedBarcode2.description)
                    newRow.find('.loadingdt').val(date.val())       
                    newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                    newRow.find('.sqn_no').val(sqn)
                    sqn += 1
                    $('.serialll').val(selectedBarcode2.serial)

                    // newRow.find('.loadingdt').val($('.invoice-date').val())     // on page load
                    // check if there's existing row in the gasrack-table
                    if (gasrackTableRow.length > 0) {
                        // We need to check one condition: the table does not contain the same barcode value (barcodell)
                        let existingValues = []
                        gasrackTableRow.each(function() {
                            existingValues.push($(this).find('.barcode').val())
                            existingValues.push($(this).find('.barcode').html())
                        })

                        if ($.inArray(selectedBarcode2.barcode, existingValues) == -1){
                            alert("Loading Gasrack Succesfully Added");
                            isAdded = true;

                            gasrackTable.find(".gasrack").append(newRow)
                            $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                            $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)

                            let type = $('.main-row2-id-' + id).find('.grtype').val()
                            $('.main-row2-id-' + id).find('.grtypedt').val(type)

                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                loadcylinder = true
                                if(triggerexist == false){
                                    $('#add-new').trigger('click')
                                    triggerexist = true
                                    triggerformany = false
                                }
                                $('#load-cyl').trigger('click')
                            })
                            $('#barcodell').val('')
                            $('#serialll').val('')
                            return false

                        }else{  
                            let newRow = $(innerTableTbodyRowFormat2())
                            newRow.find('.dt_sqn_no').val('2')
                            newRow.find('.barcode').val(selectedBarcode2.barcode)
                            newRow.find('.serialno').val(selectedBarcode2.serial)
                            newRow.find('.descr').val(selectedBarcode2.description)
                            newRow.find('.loadingdt').val(date.val())       
                            newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                            newRow.find('.sqn_no').val(sqn)
                            sqn += 1
                            gasrackTable.find(".gasrack").append(newRow)
                            alert("Gasrack Already Exist");
                            isAdded = true;
                            $('#barcodell').val('')
                            $('#serialll').val('')
                            return false
                        }
                        return false

        
                    } else {

                        $.each(selectedBarcodeManyfor, function(index, value){
                            barcodemanyfor = value.cy_barcode
                            serialmanyfor = value.cy_serial
                            categorymanyfor = value.cy_category
                            productmanyfor = value.cy_product
                            descrmanyfor = value.cy_descr
                            capacitymanyfor = value.cy_capacity
                            pressuremanyfor = value.cy_pressure
                            loadcylinder = true
                            if(triggerexist == false){
                                $('#add-new').trigger('click')
                                triggerexist = true
                                triggerformany = false
                            }
                            $('#load-cyl').trigger('click')
                        })

                        // Directly append whatever value that we found/
                        alert("Loading Gasrack Succesfully Added");
                        isAdded = true;

                        gasrackTable.find(".gasrack").append(newRow)
                        $('.main-row2-id-' + id).find('.exist-val-gr').prop('readonly', true)
                        $('.main-row2-id-' + id).find('.exist-val-gr-select').prop('disabled', true)
                        
                        let type = $('.main-row2-id-' + id).find('.grtype').val()
                        $('.main-row2-id-' + id).find('.grtypedt').val(type)
                        $('#barcodell').val('')
                        $('#serialll').val('')

                        return false
                    }

                    // exit the loop
                    return false
                }else{
                    let noGasrack = $('#table-form-tbody-gasrack').find('.no-gasrack')
                    console.log(noGasrack.html())
                    console.log('Nda Sama Msuk Not Exist Cylnder')

                    let existingValues = []
                    let existingValuesSerial = []
                    gasrackTableRow .each(function () {
                        existingValues.push($(this).find('.barcode').val())
                        existingValues.push($(this).find('.barcode').html())
                        existingValuesSerial.push($(this).find('.serialno').val())
                        existingValuesSerial.push($(this).find('.serialno').html())
                    })
                    console.log('c')
                    if ($.inArray(barcode, existingValues) == -1 || ($.inArray(serial, existingValuesSerial) == -1)) {
                        console.log('jj')
                        let NotexistGasrack = $('#table-form-tbody-gasrack').find('.no-gasrack')
                        let td = NotexistGasrack.parent('td')
                        let tr = NotexistGasrack.parent('tr')
                        let id = tr.data('row-id2')
                        let appendRow = $('#table-form-tbody-gasrack').find(".row-id2-" + id)
                        // this is the last one
                        alert("No Gasrack Found"); 

                        let newRow = $(innerTableTbodyRowFormat2())
                        newRow.find('.dt_sqn_no').val('2')
                        newRow.find('.descr').val('null')
                        newRow.find('.dnnodt').val('null')
                        newRow.find('.dndatedt').val('null')
                        newRow.find('.barcode').val($('.barcodell').val())
                        newRow.find('.serialno').val($('.serialll').val())
                        newRow.find('.loadingdt').val(date.val())       
                        newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                        newRow.find('.sqn_no').val(sqn)
                        sqn += 1
                        appendRow.find(".gasrack").append(newRow)
                        $('#barcodell').val('')
                        $('#serialll').val('')
                        return false
                    }
                    return false
                }
                
                
            })

        }

    })

    // $('#load-gas').click(function () {
    //     let card = $(this).parents('.card')
    //     let date = card.find('.invoice-date')
    //     let driver = card.find('.driverll')
    //     let barcodell = $('#barcodell').val()
    //     let serialll = $('#serialll').val();
    //     var isAdded = false;

    //     let existRows = $('#gasrackdt').find('.existing-data2')

    //     existRows.each(function (index) {
    //         let mainRow = $(this)
    //             let id =  $(this).data('row-id2')
    //             let secondRow = $(this).siblings('.row-id2-' + id)

    //             let gasrackTable = secondRow.find('.gasrack-table')
    //             let gasrackTableRow = gasrackTable.find('.new-row2')
                
    //             //Get value of loading list barcode if match in the existing row of each inner table
    //             //then update the loading date
    //             //
    //             gasrackTableRow.each(function() {
    //                 let barcode = $(this).find('.barcode').val() == '' ? $(this).find('.barcode').html() : $(this).find('.barcode').val()
    //                 let serial = $(this).find('.serialno').val() == '' ? $(this).find('.serialno').html() : $(this).find('.serialno').val()
    //                 let loadingdate = $(this).find('.loadingdt')

    //                 if (barcodell == '') {
    //                     if(serial == serialll && loadingdate.val() == ''){
    //                         $(this).find('.loadingdt').val(date.val())
    //                         $(this).find('.driverdt').val(driver.val())
    //                         $(this).find('.datetime').val(date.val())
    //                         $('.serialll').val(serial)

    //                         isAdded = true;
    //                         alert("Loading Gasrack Succesfully Added")
    //                     }
    //                 }else{
    //                     if(barcode == barcodell && loadingdate.val() == ''){
    //                         $(this).find('.loadingdt').val(date.val())
    //                         $(this).find('.driverdt').val(driver.val())
    //                         $(this).find('.datetime').val(date.val())
    //                         $('.serialll').val(serial)

    //                         isAdded = true;
    //                         alert("Loading Gasrack Succesfully Added")
    //                     }
    //                 }

    //             })
    //     })

    //     if (!isAdded) {
    //         let mainRows = $('#gasrackdt').find('.main-row2')
    //         var total = mainRows.length;
    //         mainRows.each(function(index) {

    //             let mainRow = $(this)
    //             let id =  $(this).data('row-id2')
    //             let secondRow = $(this).siblings('.row-id2-' + id)

    //             let gasrackTable = secondRow.find('.gasrack-table')
    //             let gasrackTableRow = gasrackTable.find('.new-row2')
    //             let total_2 = gasrackTableRow.length
    //             //Get value of loading list barcode if match in the existing row of each inner table
    //             //then update the loading date
    //             //
    //             gasrackTableRow.each(function(index_2) {
    //                 let barcode = $(this).find('.barcode').val()
    //                 let serial = $(this).find('.serialno').val()
    //                 let loadingdate = $(this).find('.loadingdt')

    //                 if (barcodell == '') {
    //                     if(serial == serialll && loadingdate.val() == ''){
    //                         $(this).find('.loadingdt').val(date.val())
    //                         $(this).find('.driverdt').val(driver.val())
    //                         $('.serialll').val(serial)

    //                         isAdded = true;
    //                         alert("Loading Gasrack Succesfully Added")
    //                     } else {
    //                         if (index === total - 1 && index_2 === total_2 - 1) {
    //                             // this is the last one
    //                             alert("No Gasrack Found");
    //                         }
    //                     }
    //                 }else{
    //                     if(barcode == barcodell && loadingdate.val() == ''){
    //                         $(this).find('.loadingdt').val(date.val())
    //                         $(this).find('.driverdt').val(driver.val())
                            
    //                         $('.serialll').val(serial)

    //                         isAdded = true;
    //                         alert("Loading Gasrack Succesfully Added")
    //                     } else {
    //                         if (index === total - 1 && index_2 === total_2 - 1) {
    //                             // this is the last one
    //                             alert("No Gasrack Found");
    //                         }
    //                     }
    //                 }

    //             })
    //         })
    //     }

    //     if (mainRows.length == 0 && !isAdded) {
    //         alert("No Gasrack Found");
    //     }

    // })
    
    $('#unload-gas').click(function () {
        let card = $(this).parents('.card')
        let date = card.find('.invoice-date')
        let driver = card.find('.driverul')
        let barcodeul = $('#barcodeul').val()
        let serialul = $('#seriaull').val();
        var isAdded = false;
        let dbRecords2 = $('#barcodeul').data('gs');

        if(barcodeul == ''){
            serialul = serialul.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.serial == serialul
            })[0]
        }else{
            barcodeul = barcodeul.trim()
            var selectedBarcode2 = dbRecords2.filter(recordbr => {
                return recordbr.barcode == barcodeul
            })[0]
        }

        let selectedBarcodeManyfor = [] 
        if(selectedBarcode2){

            selectedBarcodeManyfor = selectedBarcode2.gscylinder 
        }

        let existRows = $('#gasrackdt').find('.existing-data2')
        let mainRows = $('#gasrackdt').find('.existing-data2')
        var total = mainRows.length;

        existRows.each(function (index) {
            let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)

                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                
                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                gasrackTableRow.each(function() {
                    let barcode = $(this).find('.barcode').val() == '' ? $(this).find('.barcode').html() : $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val() == '' ? $(this).find('.serialno').html() : $(this).find('.serialno').val()
                    let uloadingdate = $(this).find('.uloadingdt')

                    if (barcodeul == '') {
                        if(serial == serialul && uloadingdate.val() == ''){
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $(this).find('.datetime').val(date.val())
                            $('.serialul').val(serial)

                            isAdded = true; 
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                            })  
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                            return false
                        }
                    }else{
                        if(barcode == barcodeul && uloadingdate.val() == ''){
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $(this).find('.datetime').val(date.val())
                            $('.serialul').val(serial)

                            isAdded = true;
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                            })  
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                            return false
                        }
                    }

                })
        })

        if (!isAdded) {
            let mainRows = $('#gasrackdt').find('.main-row2')
            var total = mainRows.length;
            mainRows.each(function(index) {
            console.log('msuk sni')
                let mainRow = $(this)
                let id =  $(this).data('row-id2')
                let secondRow = $(this).siblings('.row-id2-' + id)

                let gasrackTable = secondRow.find('.gasrack-table')
                let gasrackTableRow = gasrackTable.find('.new-row2')
                let total_2 = gasrackTableRow.length
                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                gasrackTableRow.each(function(index_2) {
                    let barcode = $(this).find('.barcode').val()
                    let serial = $(this).find('.serialno').val()
                    let uloadingdate = $(this).find('.uloadingdt')
                    console.log(barcode)

                    if (barcodeul == '') {
                        // if(serial == serialul && uloadingdate.val() == ''){
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialul').val(serial)

                            isAdded = true;
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                            })  
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                        // } else {
                        //     if (index === total - 1 && index_2 === total_2 - 1) {
                        //         // this is the last one
                        //         alert("No Gasrack Found");
                        //     }
                        // }
                        return false
                    }else{
                        console.log("unloading ni")
                        // if(barcode == barcodeul && uloadingdate.val() == ''){
                            console.log('sni')
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            
                            $('.serialul').val(serial)

                            isAdded = true;
                            $.each(selectedBarcodeManyfor, function(index, value){
                                barcodemanyfor = value.cy_barcode
                                serialmanyfor = value.cy_serial
                                categorymanyfor = value.cy_category
                                productmanyfor = value.cy_product
                                descrmanyfor = value.cy_descr
                                capacitymanyfor = value.cy_capacity
                                pressuremanyfor = value.cy_pressure
                                unloadcylinder = true
                                $('#unload-cyl').trigger('click')
                            })  
                            alert("Unloading Gasrack Succesfully Added")
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                        // } else {
                        //     if (index === total - 1 && index_2 === total_2 - 1) {
                        //         // $(this).find('.uloadingdt').val(date.val())
                        //         // $(this).find('.driverdt').val(driver.val())
                                
                        //         // $('.serialul').val(serial)
                        //         // // this is the last one
                        //         alert("No Gasrack Found");
                        //     }
                        // }
                        return false
                    }

                })
            })
            if (mainRows.length == 0 && !isAdded) {
                // let barcode = $(this).find('.barcode').val()
                // let serial = $(this).find('.serialno').val()
                // let uloadingdate = $(this).find('.uloadingdt')
                // $(this).find('.uloadingdt').val(date.val())
                // $(this).find('.driverdt').val(driver.val())
                
                // $('.serialul').val(serial)
                console.log('sni kh')
                // alert("No Gasrack Found");
                mainRows.find('.uloadingdt').val(date.val())
                mainRows.find('.driverdt').val(driver.val())
                
                $('.serialul').val(serial)
    
                isAdded = true;
                $.each(selectedBarcodeManyfor, function(index, value){
                    barcodemanyfor = value.cy_barcode
                    serialmanyfor = value.cy_serial
                    categorymanyfor = value.cy_category
                    productmanyfor = value.cy_product
                    descrmanyfor = value.cy_descr
                    capacitymanyfor = value.cy_capacity
                    pressuremanyfor = value.cy_pressure
                    unloadcylinder = true
                    $('#unload-cyl').trigger('click')
                })  
                $('#barcodeul').val('')
                $('#seriaull').val('')
                alert("Unloading Gasrack Succesfully Added")
                return false
            }
        }

     

    })

    
    let driver = $('.driver').val()

    $('.driverpr').val(driver)
    $('.driverll').val(driver)
    $('.driverul').val(driver)
    
    //Get Driver Details
    $('.driver').on('change', function(){
        let driver = $(this).val()

        $('.driverpr').val(driver)
        $('.driverll').val(driver)
        $('.driverul').val(driver)

    })
    
})
