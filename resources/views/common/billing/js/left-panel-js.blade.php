$('.search-debtor').on('click', function () {
    let modal = $('.search-debtor-modal-template').clone().removeClass('search-document-modal-template')
    let getRoute = modal.data('get-route')
    let table = modal.find('table')
    // show modal
    modal.modal()
    modal.on('hidden.bs.modal', function (e) { $(this).remove() })
    $.ajax({
        url: getRoute,
        method: 'GET',
    }).done(function (resources) {
        resources.forEach(resource => {
            let markup = `
            <tr class="pointer" data-choice="${resource.accountcode}">
                <td class="acode" width="25%">${resource.accountcode}</td>
                <td class="name" width="75%">${resource.name}</td>
            </tr>
            `

            table.find('tbody').append(markup)
        });

        // init datatable
        table.DataTable()
        table.on('click', 'tr', function () {
            // first we check if the select2 exist (in billing, uses debtor-select, in vouchers using account_code)
            // then triggers the change depending on which one exist - by default check for debtor-select first
            if ($('#debtor-select').val()) {
                $('#debtor-select').val($(this).data('choice')).trigger('change')
            } else {
                $('#account_code').val($(this).data('choice')).trigger('change')
            }
            $('#debtor-select').val($(this).data('choice')).trigger('change')
            modal.modal('hide')
        })
    })
})
