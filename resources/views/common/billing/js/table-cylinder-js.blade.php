let systemSettings = $('.system-settings-param')
let boolRounding = systemSettings.data('rounding')

let leftPad = function (value, length) {
    return ('0'.repeat(length) + value).slice(-length);
}

$('#rounding-adjustment').change(function () {
    boolRounding = $(this).prop('checked') // update the value

    // if the rounding is allowed, unhide the Rounding in totaling blade
    if (boolRounding)
        $('#rounding-container').hasClass('d-none') ? $('#rounding-container').removeClass('d-none') : ''
    else
        $('#rounding-container').hasClass('d-none') ? '' : $('#rounding-container').addClass('d-none')
})

// custom select2
function formatStateInTable(state) { return state.id }

// overiding all current select2 and summernote in the table forms into using formatStateInTable
$('.form-select2').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })

let rowID = 0;
let taxCodes = $('.tax-container').data('taxes')

$('#item-master-datatable').DataTable()

let intTSN = 0
$('.sequence_no').each(function () { intTSN += 5 })

// trigger on click new button
$('#add-new').on('click', function () {
    rowID++
    let template = $('.template-new').clone().removeClass('template-new d-none').addClass('bill')
    let rowDetailID = 'row-detail-' + rowID
    let tSN = leftPad(intTSN, 4)    // 0005
    let itemCodeSelect = template.eq(0).find('.item_code')

    // adding newly added item_code to select2
    itemCodeSelect.select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true, tags: true })
    let contents = itemCodeSelect.data('contents')
    contents.forEach(function (item) {
        let code = item.code
        let name = item.descr
        let newItem = new Option(code, code, false, false);
        itemCodeSelect.append(newItem)
    })
    itemCodeSelect.trigger('change')

    let uomSelect = template.eq(0).find('.unit_measure')

    // adding newly added uom to select2
    uomSelect.select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true, tags: true })
    let contents1 = uomSelect.data('uoms')
    contents1.forEach(function (item1) {
        let code1 = item1.code
        let newItem1 = new Option(code1, code1, false, false);
        uomSelect.append(newItem1)
    })
    uomSelect.trigger('change')

    // disabling the form submit button since the item code still empty
    $('.btn-form-submit').prop('disabled', true)

    // start cloning
    template.eq(0).find('.item_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
    template.eq(0).find('.item_code').attr('required', true)
    template.eq(0).find('.subject').val()
    template.eq(0).find('.sequence_no').val(tSN)
    template.eq(0).find('.quantity').val(1)
    template.eq(0).find('.unit_measure').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
    template.eq(0).find('.rate').val(null)
    template.eq(0).find('.cunit_cost').val(0)
    template.eq(0).find('.cunit_cost').inputmask('currency', { prefix: '', rightAlign: true })
    template.eq(0).find('.unit_price').val(0)
    template.eq(0).find('.unit_price').inputmask('currency', { prefix: '', rightAlign: true })
    template.eq(0).find('.markup').val(null)
    template.eq(0).find('.unit_cost').val(0)
    template.eq(0).find('.unit_cost').inputmask('currency', { prefix: '', rightAlign: true })
    template.eq(0).find('.discount').val(0)
    template.eq(0).find('.discount').inputmask({ rightAlign: true })
    template.eq(0).find('.amount').val(0)
    template.eq(0).find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
    template.eq(0).find('.tax_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
    template.eq(0).find('.tax_rate').val(0)
    template.eq(0).find('.tax_amount').val(0)
    template.eq(0).find('.tax_amount').inputmask('currency', { prefix: '', rightAlign: true })
    template.eq(0).find('.taxed_amount').inputmask('currency', { prefix: '', rightAlign: true })
    template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)
    template.eq(1).attr('id', rowDetailID)
    $('#table-form-tbody').append(template)

    // after adding to the tbody, increment sequence_no for the upcoming button click
    intTSN += 5

    // hide this button temporarily
    $("#add-new").addClass('d-none')
    $("#add-new-item").addClass('d-none')

    // set on event handler
    onHandler(rowDetailID, template.eq(0), template.eq(1))
})

$('.existing-data').each(function () {
    let rowID = $(this).data('item-id')
    let deleteRoute = $(this).data('delete-route')
    deleteRoute = deleteRoute.replace('id', rowID)
    let firstRow = $(this)
    let rowDetailID = 'details_' + rowID

    firstRow.find('.item_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
    firstRow.find('.unit_measure').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })

    // normal operations handler
    onHandler(rowDetailID)

    // delete handler
    firstRow.find('.remove-row').on('click', function () {
        $.ajax({
            'method': 'DELETE',
            'url': deleteRoute
        }).done(function (response) { console.log(response) })
    })
})

// postToAccount is set from create/edit page (because every transactions uses different accounts)
function findItemAccount(postToAccount, content) {
    // only apply if account is found
    switch (postToAccount) {
        case 'cashSalesAcc':
            if (content.cashSalesAcc)
                return content.cashSalesAcc.accountCode
            break;
        case 'cashSalesReturnAcc':
            if (content.cashSalesReturnAcc)
                return content.cashSalesReturnAcc.accountCode
            break;
        case 'creditSalesAcc':
            if (content.creditSalesAcc)
                return content.creditSalesAcc.accountCode
            break;
        case 'creditSalesReturnAcc':
            if (content.creditSalesReturnAcc)
                return content.creditSalesReturnAcc.accountCode
            break;
        case 'cashPurchaseAcc':
            if (content.cashPurchaseAcc)
                return content.cashPurchaseAcc.accountCode
            break;
        case 'cashPurchaseReturnAcc':
            if (content.cashPurchaseReturnAcc)
                return content.cashPurchaseReturnAcc.accountCode
            break;
        case 'creditPurchaseAcc':
            if (content.creditPurchaseAcc)
                return content.creditPurchaseAcc.accountCode
            break;
        case 'creditPurchaseReturnAcc':
            if (content.creditPurchaseReturnAcc)
                return content.creditPurchaseReturnAcc.accountCode
            break;
        default:
            break;
    }

    return '';
}

// predefined actions to be done
function onHandler(rowDetailID) {
    $('.item_code').on('change', function () {
        let firstRow = $(this).parents('tr')
        let secondRow = firstRow.next()

        if (!$(this).val()) {
            // keep the form submit button disabled and hide the add-new button
            $('.btn-form-submit').prop('disabled', true)
            $("#add-new").addClass('d-none')
            $("#add-new-item").addClass('d-none')
        } else {
            // re-enable the form submit button since the item code is now filled
            $('.btn-form-submit').prop('disabled', false)

            let row = $(this).parent().parent()
            let content = $(".item_code").data('contents').filter(content => content.code == $(this).val())[0]

            // console.log(content.uom.code)
            let postToAccountCode = findItemAccount(postToAccount, content)
            firstRow.find('.account_code').val(postToAccountCode)
            firstRow.find('.subject').val(content.descr)
            firstRow.find('.quantity').val(1)
            firstRow.find('.unit_measure').val(content.uom)
            firstRow.find('.rate').val(content.rate)
            firstRow.find('.cucost').val(content.cucost)
            firstRow.find('.markup').val(content.markup)
            firstRow.find('.unit_price').val(content.uprice)
            firstRow.find('.unit_cost').val(content.ucost)
            firstRow.find('.discount').val(0)
            firstRow.find('.amount').val(content.amount)
            firstRow.find('.tax_code').val(null)
            firstRow.find('.tax_rate').val(0)
            firstRow.find('.tax_amount').val(0)
            firstRow.find('.taxed_amount').val(content.tax_amount)
            firstRow.find('.toggle-details').attr('data-target', '#' + rowDetailID)
            secondRow.find('.t_detail').summernote({
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                },
                placeholder: '',
                height: 100,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']]
                ],
                followingToolbar: false
            })

            $("#add-new").removeClass('d-none')
            // $("#add-new-item").removeClass('d-none')
        }
    })

    $('.subject').on('keyup', function (e) {
        if ($('#add-new').hasClass('d-none')) {
            $("#add-new").removeClass('d-none')
        }

        if ($(this).val() === '') {
            if (!$('#add-new').hasClass('d-none')) {
                $("#add-new").addClass('d-none')
            }
        }
    })

    $('.tax_code').on('change', function () {
        let row = $(this).parent().parent()
        let selectedTax = row.find('.tax_code').find(':selected').val()
        let tax = taxCodes.filter(tax => tax.t_code == selectedTax)[0]

        row.find('.tax_rate').val(tax.t_rate)
        row.find('.tax_rate_dt').val(tax.t_rate)
    })

    $('.calculate-field').change(function () {
        let row = $(this).parent().parent()
        let amount = row.find('.amount').val()
        let uprice = row.find('.unit_price').val().replace(',', '')
        let qty = row.find('.quantity').val()
        let discount = row.find('.discount')
        let theDiscount = (discount.val().indexOf('%') > -1) ? discount.val().replace('%', '') : discount.val()
        let taxRate = row.find('.tax_rate').val()

        // calculate discount
        let discountAmount = 0.00
        if (discount.val().indexOf('%') > -1) {
            discountAmount = uprice * qty * theDiscount / 100
            row.find('.discount').val(discountAmount)
        } else {
            discountAmount = row.find('.discount').val()
        }

        let price = (uprice * qty) - discountAmount
        row.find('.amount').val(price)
        row.find('.amount_dt').val(price)

        // calculate tax
        let taxamount = 0.00
        if (taxRate > 0)
            taxamount = price / taxRate
        row.find('.tax_amount').val(taxamount)
        row.find('.tax_amount_dt').val(taxamount)

        // final price
        let finalPrice = price + taxamount
        row.find('.taxed_amount').val(finalPrice)
        row.find('.taxed_amount_dt').val(finalPrice)

        // update the subtotal and grandtotal fields
        updateTotals()
    })

    $('.calculate-field2').change(function () {
        let row = $(this).parent().parent()
        let amount = row.find('.amount').val()
        let ucost = row.find('.unit_cost').val().replace(',', '')
        // row.find('.cunit_cost').val(ucost)
        let qty = row.find('.quantity').val()
        let discount = row.find('.discount')
        let theDiscount = (discount.val().indexOf('%') > -1) ? discount.val().replace('%', '') : discount.val()
        let taxRate = row.find('.tax_rate').val()

        // calculate discount
        let discountAmount = 0.00
        if (discount.val().indexOf('%') > -1) {
            discountAmount = ucost * qty * theDiscount / 100
            row.find('.discount').val(discountAmount)
        } else {
            discountAmount = row.find('.discount').val()
        }

        let price = (ucost * qty) - discountAmount
        row.find('.amount').val(price)
        row.find('.amount_dt').val(price)

        // calculate tax
        let taxamount = 0.00
        if (taxRate > 0)
            taxamount = price / taxRate
        row.find('.tax_amount').val(taxamount)
        row.find('.tax_amount_dt').val(taxamount)

        // final price
        let finalPrice = price + taxamount
        row.find('.taxed_amount').val(finalPrice)
        row.find('.taxed_amount_dt').val(finalPrice)

        // update the subtotal and grandtotal fields
        updateTotals()
    })

    $('.calculate-field3').change(function () {
        let row = $(this).parent().parent()
        let amount = row.find('.amount').val()
        let uprice = row.find('.unit_price').val().replace(',', '')
        // row.find('.cunit_cost').val(ucost)
        let qty = row.find('.quantity').val()
        let discount = row.find('.discount')
        let theDiscount = (discount.val().indexOf('%') > -1) ? discount.val().replace('%', '') : discount.val()
        let taxRate = row.find('.tax_rate').val()

        // calculate discount
        let discountAmount = 0.00
        if (discount.val().indexOf('%') > -1) {
            discountAmount = uprice * qty * theDiscount / 100
            row.find('.discount').val(discountAmount)
        } else {
            discountAmount = row.find('.discount').val()
        }

        let price = (uprice * qty) - discountAmount
        row.find('.amount').val(price)
        row.find('.amount_dt').val(price)

        // calculate tax
        let taxamount = 0.00
        if (taxRate > 0)
            taxamount = price / taxRate
        row.find('.tax_amount').val(taxamount)
        row.find('.tax_amount_dt').val(taxamount)

        // final price
        let finalPrice = price + taxamount
        row.find('.taxed_amount').val(finalPrice)
        row.find('.taxed_amount_dt').val(finalPrice)

        // update the subtotal and grandtotal fields
        updateTotals()
    })


    $('.discount_mt').on('change', function () {
        updateTotals()
    })

    $('.taxed_amount').on('change', function () {
        let row = $(this).parents('tr')
        row.find('.amount').val($(this).val())
        row.find('.amount_dt').val($(this).val())

        updateTotals()
    })

    $('.remove-row').on('click', function () {
        $(this).closest('tr').next('tr').remove()
        $(this).parent().parent().remove()
        $("#add-new").removeClass('d-none')
        // $("#add-new-item").removeClass('d-none')

        updateTotals()
    })

    function updateTotals() {
        let amountFields = $('.amount')
        let taxFields = $('.tax_amount')
        let discountFields = $('.discount')
        let globalDiscount = $('.discount_mt')

        let subtotal = 0.00
        for (let i = 1; i < amountFields.length; i++) {
            subtotal += parseFloat(amountFields.eq(i).val().replace(',', ''))
        }
        console.log(subtotal)
        let taxes = 0.00
        for (let i = 1; i < taxFields.length; i++)
            taxes += parseFloat(taxFields.eq(i).val().replace(',', ''))

        // calculate subtotal
        let sumOfAmountNTaxes = subtotal + taxes

        // calculate discount
        let globalDiscountAmount = 0.00
        let theGlobalDiscount = (globalDiscount.val().indexOf('%') > -1) ? globalDiscount.val().replace('%', '') : globalDiscount.val()
        if (globalDiscount.val().indexOf('%') > -1)
            globalDiscountAmount = sumOfAmountNTaxes * theGlobalDiscount / 100
        else
            globalDiscountAmount = theGlobalDiscount

        let discountedPrice = sumOfAmountNTaxes - globalDiscountAmount;
        let grandTotal = (sumOfAmountNTaxes) - globalDiscountAmount

        // show subtotal and tax
        $('#subtotal').val(subtotal.toFixed(2))
        $('#tax').val(taxes.toFixed(2))

        // show rounding if allowed
        if (boolRounding) {
            let rounding = calculateRounding(discountedPrice)
            let roundedPrice = calculateRoundedPrice(discountedPrice, rounding)
            $('#rounding').val(rounding)
            $('#grand_total').val(roundedPrice.toFixed(2))
            $('.amount_mt').val(roundedPrice.toFixed(2))    // for credit note, need to put the subtotal at the top
            $('.remaining_amount_mt').val(roundedPrice.toFixed(2))    // for credit note, need to put the subtotal at the top
            amount = roundedPrice
            remainingAmount = roundedPrice

        } else {
            $('#grand_total').val(grandTotal.toFixed(2))
            $('.amount_mt').val(grandTotal.toFixed(2))      // for credit note, need to put the subtotal at the top
            $('.remaining_amount_mt').val(grandTotal.toFixed(2))    // for credit note, need to put the subtotal at the top
            amount = grandTotal
            remainingAmount = grandTotal
        }
    }

    function calculateRounding(price) {
        if (boolRounding) {
            price = price.toFixed(2)
            let lastDecimal = price.toString().slice(-1);
            switch (lastDecimal) {
                case '1': return -0.01; break;
                case '2': return -0.02; break;
                case '3': return 0.02; break;
                case '4': return 0.01; break;
                case '5': return 0.00; break;
                case '6': return -0.01; break;
                case '7': return -0.02; break;
                case '8': return 0.02; break
                case '9': return 0.01; break;
                default: return 0.00; break;
            }
        } else {
            return 0.00
        }
    }

    function calculateRoundedPrice(price, rounding) {
        return roundedPrice = parseFloat(price) + parseFloat(rounding)
    }
}
