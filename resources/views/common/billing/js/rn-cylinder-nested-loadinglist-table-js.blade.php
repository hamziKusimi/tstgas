
$(document).ready(function () {
    let sqn = 1;
    $('.sqn_no').each(function () { sqn += 1 })

    let mainTableCounter = 0;                           // everytime click (Add) button, increment this number (used for data-row-id)
    let idRow = 0;                       // everytime, new barcode found, increment this number and append in inner table
    let barcodeData = $('.barcodell').data('item');     // get the existing data from the database (i.e.: BR1234)


    function innerTableTbodyRowFormat() {
        return `
          <tr class="narrow-padding new-row text-sm inner-row">
              <td>
                {!! Form::hidden("dt_sqn_no[]", null, ["class" => "form-control dt_sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("sqn_no[]", null, ["class" => "form-control sqn_no", "placeholder"=>"sqn_no", "readonly"]) !!}
                {!! Form::hidden("type[]", "cy", ["class" => "form-control type", "placeholder"=>"sqn_no", "readonly"]) !!}
                  {!! Form::text("barcode[]", null, ["class" => "form-control barcode", "placeholder"=>"Barcode", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("serialno[]", null, ["class" => "form-control serialno", "placeholder"=>"Serial No", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("descr[]", null, [ "class" => "form-control descr" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("driverdt[]", null, [ "class" => "form-control driverdt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("dnnodt[]", null, [ "class" => "form-control dnnodt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("dndatedt[]", null, [ "class" => "form-control dndatedt" , "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("loadingdt[]", null, ["class" => "form-control loadingdt calculate-field", "readonly"]) !!}
              </td>
              <td>
                  {!! Form::text("uloadingdt[]", null, ["class" => "form-control uloadingdt calculate-field", "readonly"]) !!}
              </td>
              <td>
                  <button type="button" class="btn btn-danger remove-row-child" data-id="0">
                      <i class="fa fa-times"></i>
                  </button>
              </td>
          </tr>
        `
    }

    $('#opiniondt').on('click', '.toggle-inner-table-button', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#opiniondt').find('.' + secondRowIdentifier)

        // now we want to show the table or hide it
        if (row.hasClass('d-none'))
            row.removeClass('d-none')     // if its hidden, show it, when button is clicked
        else
            row.addClass('d-none')        // if its shown, hide it, when button is clicked

    })

    $('#opiniondt').on('click', '.toggle-inner-table-button-exist', function () {
        // <tr class="narrow-padding main-table-row-template d-none" data-row-id="0">
        let secondRowIdentifier = $(this).data('second-row-id')
        let row = $('#opiniondt').find('.' + secondRowIdentifier)
      
        // now we want to show the table or hide it
        if (row.hasClass('d-none')) {
            row.removeClass('d-none')  // if its hidden, show it, when button is clicked
        } else {
            row.addClass('d-none')        // if its shown, hide it, when button is clicked
        }
    })

    //Check if the main row have matched value with the loading list in uloading list
    let isMatched = function (selectedBarcode, mainRow) {
        if (
            selectedBarcode.account_code == $('#debtor-select').val()) {
            return true
        }

        return false
    }

    //check if barcode or serial exist in cyliners records or not
    function filterRecords(item, route, date, driver, isAdded, isBarcode) {

        let data = {
            'item': item,
            'isBarcode': isBarcode
        }

        console.log(item)
        if (item != '') {
            $.ajax({
                url: route,
                method: "GET",
                data: data,
            }).done(function (response) {
                $('.loading-overlay').hide();
                var selectedBarcode = response[0];
                let existRows = $('#opiniondt').find('.existing-data')
                var totalR = existRows.length;

                let maintable = $('#table-form-tbody').find('.exist-row-cylinder')
                maintable.each(function (index) {
                    console.log(maintable.find('.sequence_no').val())
                })

                existRows.each(function (index) {
                    console.log('Exist Row')
                    let existRow = $(this)
                    let id = $(this).data('row-id')
                    let secondRow = $(this).siblings('.row-id-' + id)
                   
                    let cylinderTable = secondRow.find('.cylinder-table')
                    let cylinderTable2 = cylinderTable.data('row-id')
                    console.log(cylinderTable2)
                    let cylinderTableRow = cylinderTable.find('.new-row')
                    if (selectedBarcode && isMatched(selectedBarcode, existRow)) {
                        console.log('check klu barcode ada dlam DN')
                        let newRow = $(innerTableTbodyRowFormat())
                        newRow.find('.dt_sqn_no').val(existRow.find('.sequence_no').val())
                        newRow.find('.barcode').val(selectedBarcode.barcode)
                        newRow.find('.serialno').val(selectedBarcode.serial)
                        newRow.find('.descr').val(selectedBarcode.descr)
                        newRow.find('.loadingdt').val(date.val())
                        newRow.find('.dnnodt').val(selectedBarcode.dnno)
                        newRow.find('.dndatedt').val(selectedBarcode.dnno_date)
                        newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                        newRow.find('.sqn_no').val(sqn)
                        sqn += 1
                        $('.serialll').val(selectedBarcode.serial)
                        // We need to check one condition: the table does not contain the same barcode value (barcodell)
                        let existingValues = []
                        let existingValuesSerial = []
                        cylinderTableRow.each(function () {
                            existingValues.push($(this).find('.barcode').val())
                            existingValues.push($(this).find('.barcode').html())
                            existingValuesSerial.push($(this).find('.serialno').val())
                            existingValuesSerial.push($(this).find('.serialno').html())
                        })


                        if (cylinderTableRow.length > 0) {
                            console.log('check klu ada row')

                            if (($.inArray(selectedBarcode.barcode, existingValues) == -1) || ($.inArray(selectedBarcode.serial, existingValuesSerial) == -1)) {
                                
                                console.log('check klu tiada exist apeend cylinder')
                                if(barcodemanyfor == '' || serialmanyfor == ''){
                                    alert("Loading Cylinder Succesfully Added");
                                }
                                isAdded = true;
                                cylinderTable.find("tbody").append(newRow)

                                //readonly row
                                $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                                //disable select 2
                                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                                //put value to database hidden input form
                                let cat = $('.main-row-id-' + id).find('.category').val()
                                $('.main-row-id-' + id).find('.categorydt').val(cat)

                                let prod = $('.main-row-id-' + id).find('.product').val()
                                $('.main-row-id-' + id).find('.productdt').val(prod)

                                let cap = $('.main-row-id-' + id).find('.capacity').val()
                                $('.main-row-id-' + id).find('.capacitydt').val(cap)

                                let ps = $('.main-row-id-' + id).find('.pressure').val()
                                $('.main-row-id-' + id).find('.pressuredt').val(ps)
                                $('#barcodell').val('')
                                $('#serialll').val('')
                            }else{
                                
                                console.log('check klu ada exist apeend cylinder')
                                
                        // let existingValues = []
                        // let existingValuesSerial = []
                        // cylinderTableRow.each(function () {
                        //     existingValues.push($(this).find('.barcode').val())
                        //     existingValues.push($(this).find('.barcode').html())
                        //     existingValuesSerial.push($(this).find('.serialno').val())
                        //     existingValuesSerial.push($(this).find('.serialno').html())
                        // })

                        if (($.inArray($('.barcodell').val(), existingValues) == -1) || ($.inArray($('.serialll').val(), existingValuesSerial) == -1)){
                            console.log('check barcode ndak sma klu sdh exist dlam row bru append')
                            // if(maintable.find('.sequence_no').val() == '2'){
                            let Notexistcylinder = $('#table-form-tbody').find('.Notexistcylinder')
                            let td = Notexistcylinder.parent('td')
                            let tr = td.parent('tr')
                            let id = tr.data('row-id')
                            let appendRow = $('#table-form-tbody').find(".row-id-" + id)
                            // if (index === totalR - 1) {
                                console.log("msukkk snii")   
                                let newRow = $(innerTableTbodyRowFormat())
                                newRow.find('.dt_sqn_no').val('2')
                                newRow.find('.barcode').val($('.barcodell').val())
                                newRow.find('.serialno').val($('.serialll').val())
                                newRow.find('.descr').val('null')
                                newRow.find('.loadingdt').val(date.val())
                                newRow.find('.dnnodt').val('null')
                                newRow.find('.dndatedt').val('null')
                                newRow.find('.driverdt').val($('.driver').val()) // keep track of changing values
                                newRow.find('.sqn_no').val(sqn)
                                sqn += 1
                                
                                // triggerformany =  true
                                appendRow.find('tbody').append(newRow)
                                    
                                    // }else {
                                    //     console.log('msuk sni2')
                                //     if(barcodemanyfor == '' || serialmanyfor == '')    {
                                //     alert("No Input Detected");
                                // }
                                    //     $('.loading-overlay').hide();
                                    // }
                                $('#barcodell').val('')
                                $('#serialll').val('')
                                isAdded = true
                                return false
                            // }
                            // }
                        }else{
                            console.log('cylinder ndak wujud')
                            
                        }

                       
                        isAdded = true
                        return false
                            }
                        } else {
                            // Directly append whatever value that we found
                            cylinderTable.find("tbody").append(newRow)
                            console.log('check klu  tiada row exist apeend cylinder')
                            if(barcodemanyfor == '' || serialmanyfor == ''){
                                alert("Loading Cylinder Succesfully Added");
                            }
                            isAdded = true;

                            //readonly row
                            $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                            //disable select 2
                            $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                            //put value to database hidden input form
                            let cat = $('.main-row-id-' + id).find('.category').val()
                            $('.main-row-id-' + id).find('.categorydt').val(cat)

                            let prod = $('.main-row-id-' + id).find('.product').val()
                            $('.main-row-id-' + id).find('.productdt').val(prod)

                            let cap = $('.main-row-id-' + id).find('.capacity').val()
                            $('.main-row-id-' + id).find('.capacitydt').val(cap)

                            let ps = $('.main-row-id-' + id).find('.pressure').val()
                            $('.main-row-id-' + id).find('.pressuredt').val(ps)

                            $('#barcodell').val('')
                            $('#serialll').val('')

                            return false
                        }

                        isAdded = true;
                        // exit the loop
                        return false
                    }else{
                    
                        console.log('tiada dlam DN append jk ke not exist cylinder')
                        let existingValues = []
                        let existingValuesSerial = []
                        cylinderTableRow.each(function () {
                            existingValues.push($(this).find('.barcode').val())
                            existingValues.push($(this).find('.barcode').html())
                            existingValuesSerial.push($(this).find('.serialno').val())
                            existingValuesSerial.push($(this).find('.serialno').html())
                        })

                        if (($.inArray($('.barcodell').val(), existingValues) == -1) || ($.inArray($('.serialll').val(), existingValuesSerial) == -1)){
                            console.log('check barcode ndak sma klu sdh exist dlam row bru append')
                            // if(maintable.find('.sequence_no').val() == '2'){
                            let Notexistcylinder = $('#table-form-tbody').find('.Notexistcylinder')
                            let td = Notexistcylinder.parent('td')
                            let tr = td.parent('tr')
                            let id = tr.data('row-id')
                            let appendRow = $('#table-form-tbody').find(".row-id-" + id)
                            // if (index === totalR - 1) {
                                console.log("msukkk snii")   
                                let newRow = $(innerTableTbodyRowFormat())
                                newRow.find('.dt_sqn_no').val('2')
                                newRow.find('.barcode').val($('.barcodell').val())
                                newRow.find('.serialno').val($('.serialll').val())
                                newRow.find('.descr').val('null')
                                newRow.find('.loadingdt').val(date.val())
                                newRow.find('.dnnodt').val('null')
                                newRow.find('.dndatedt').val('null')
                                newRow.find('.driverdt').val($('.driver').val()) // keep track of changing values
                                newRow.find('.sqn_no').val(sqn)
                                sqn += 1
                                
                                // triggerformany =  true
                                appendRow.find('tbody').append(newRow)
                                    
                                    // }else {
                                    //     console.log('msuk sni2')
                                //     if(barcodemanyfor == '' || serialmanyfor == '')    {
                                //     alert("No Input Detected");
                                // }
                                    //     $('.loading-overlay').hide();
                                    // }
                                $('#barcodell').val('')
                                $('#serialll').val('')
                                isAdded = true
                                return false
                            // }
                            // }
                        }else{
                            console.log('cylinder ndak wujud')
                            
                        }

                       
                        isAdded = true
                        return false
                    }

                })

                let mainRows = $('#opiniondt').find('.main-row')
                var total = mainRows.length;

                if (!isAdded) {
                    console.log('New Row')
                    // this loop is to find the second row
                    mainRows.each(function (index) {

                        let mainRow = $(this)
                        let id = $(this).data('row-id')

                        let secondRow = $(this).siblings('.row-id-' + id)

                        let cylinderTable = secondRow.find('.cylinder-table')
                        let cylinderTableRow = cylinderTable.find('.new-row')
                        if (selectedBarcode && isMatched(selectedBarcode, mainRow)) {
                            console.log('Sama Msuk Exist Cylnder')

                            // generating new element and inputting the value

                            let newRow = $(innerTableTbodyRowFormat())
                            newRow.find('.dt_sqn_no').val(mainRow.find('.sequence_no').val())
                            newRow.find('.barcode').val(selectedBarcode.barcode)
                            newRow.find('.serialno').val(selectedBarcode.serial)
                            newRow.find('.descr').val(selectedBarcode.descr)
                            newRow.find('.loadingdt').val(date.val())
                            newRow.find('.driverdt').val(driver.val()) // keep track of changing values
                            newRow.find('.dnnodt').val(selectedBarcode.dnno)
                            newRow.find('.dndatedt').val(selectedBarcode.dnno_date)
                            newRow.find('.sqn_no').val(sqn)
                            sqn += 1
                            $('.serialll').val(selectedBarcode.serial)

                            // newRow.find('.loadingdt').val($('.invoice-date').val())     // on page load
                            // check if there's existing row in the cylinder-table

                                // We need to check one condition: the table does not contain the same barcode value (barcodell)
                                let existingValues = []
                                let existingValuesSerial = []
                                cylinderTableRow.each(function () {
                                    existingValues.push($(this).find('.barcode').val())
                                    existingValues.push($(this).find('.barcode').html())
                                    existingValuesSerial.push($(this).find('.serialno').val())
                                    existingValuesSerial.push($(this).find('.serialno').html())
                                })

                            if (cylinderTableRow.length > 0) {
                                console.log('check klu table ada row')
                                if ($.inArray(selectedBarcode.barcode, existingValues) == -1 || ($.inArray(selectedBarcode.serial, existingValuesSerial) == -1)) {
                                    console.log('check klu table tiada existing value')
                                    isAdded = true;
                                    cylinderTable.find("tbody").append(newRow)
                                    if(barcodemanyfor == '' || serialmanyfor == ''){
                                        alert("Loading Cylinder Succesfully Added");
                                        $('#barcodell').val('')
                                        $('#serialll').val('')
                                    }

                              
                                } else {
                                    console.log('check klu table ada existing value')
                                    if(barcodemanyfor == '' || serialmanyfor == ''){
                                        alert("Cylinder Already Exist");  
                                    }
                                    loadcylinder = false
                                    $('#barcodell').val('')
                                    $('#serialll').val('')
                                  
                                }
                            } else {
                                console.log('just append apa2 value klu tiada row')
                                // Directly append whatever value that we found
                                cylinderTable.find("tbody").append(newRow)
                                if(barcodemanyfor == '' || serialmanyfor == ''){
                                alert("Loading Cylinder Succesfully Added");   
                                } 
                                isAdded = true;
                                //readonly row
                                $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                                //disable select 2
                                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                                //put value to database hidden input form
                                let cat = $('.main-row-id-' + id).find('.category').val()
                                $('.main-row-id-' + id).find('.categorydt').val(cat)

                                let prod = $('.main-row-id-' + id).find('.product').val()
                                $('.main-row-id-' + id).find('.productdt').val(prod)

                                let cap = $('.main-row-id-' + id).find('.capacity').val()
                                $('.main-row-id-' + id).find('.capacitydt').val(cap)

                                let ps = $('.main-row-id-' + id).find('.pressure').val()
                                $('.main-row-id-' + id).find('.pressuredt').val(ps)

                                $('#barcodell').val('')
                                $('#serialll').val('')

                                return false
                            }

                            // exit the loop
                            return false
                        } else {
                            let noCylinder = $('#table-form-tbody').find('.no-cylinder')
                            console.log(noCylinder.html())
                            console.log('Nda Sama Msuk Not Exist Cylnder')

                            let existingValues = []
                            let existingValuesSerial = []
                            cylinderTableRow.each(function () {
                                existingValues.push($(this).find('.barcode').val())
                                existingValues.push($(this).find('.barcode').html())
                                existingValuesSerial.push($(this).find('.serialno').val())
                                existingValuesSerial.push($(this).find('.serialno').html())
                            })
                            if ($.inArray($('.barcodell').val(), existingValues) == -1 || ($.inArray($('.serialll').val(), existingValuesSerial) == -1)) {
                                    console.log('jj')
                                let Notexistcylinder = $('#table-form-tbody').find('.no-cylinder')
                                let td = Notexistcylinder.parent('td')
                                let tr = Notexistcylinder.parent('tr')
                                let id = tr.data('row-id')
                                let appendRow = $('#table-form-tbody').find(".row-id-" + id)
                                console.log(tr.html())
                                // if (index === total - 1) {
                                    // this is the last one
                                    // if(triggerformany ==  false){
                                        if(barcodemanyfor == '' || serialmanyfor == ''){
                                        alert("No Cylinder Found");
                                        }
                                        let newRow = $(innerTableTbodyRowFormat())
                                        newRow.find('.dt_sqn_no').val('2')
                                        newRow.find('.barcode').val($('.barcodell').val())
                                        newRow.find('.serialno').val($('.serialll').val())
                                        newRow.find('.descr').val('null')
                                        newRow.find('.loadingdt').val(date.val())
                                        newRow.find('.dnnodt').val('null')
                                        newRow.find('.dndatedt').val('null')
                                        newRow.find('.driverdt').val($('.driver').val()) // keep track of changing values
                                        newRow.find('.sqn_no').val(sqn)
                                        sqn += 1
                                        
                                        // triggerformany =  true
                                        appendRow.find("tbody").append(newRow)
                                            // }else {
                                            //     console.log('msuk sni2')
                                        //     if(barcodemanyfor == '' || serialmanyfor == '')    {
                                        //     alert("No Input Detected");
                                        // }
                                            //     $('.loading-overlay').hide();
                                            // }
                                    $('#barcodell').val('')
                                    $('#serialll').val('')
                                // }
                                return false
                            }else{
                                console.log('cylinder h wjud')
                                return false
                            }
                            return false

                        }


                    })
                }

                // if (total == 0 && !isAdded) {  
                //     console.log('alamak sni')
                //     let existRows = $('#opiniondt').find('.existing-data')
                //     let id = existRows.data('row-id')
                //     let secondRow = existRows.siblings('.row-id-' + id)
                //     let cylinderTable = secondRow.find('.cylinder-table')
                //     let cylinderTableRow = cylinderTable.find('.new-row')
                   
                //     if(triggerformany ==  false){
                //         console.log('msuk sni8')
                //         let newRow = $(innerTableTbodyRowFormat())
                //         newRow.find('.dt_sqn_no').val('2')
                //         newRow.find('.barcode').val($('.barcodell').val())
                //         newRow.find('.serialno').val($('.serialll').val())
                //         newRow.find('.descr').val('null')
                //         newRow.find('.loadingdt').val('null')
                //         newRow.find('.dnnodt').val('null')
                //         newRow.find('.dndatedt').val('null')
                //         newRow.find('.driverdt').val($('.driver').val()) // keep track of changing values
                //         newRow.find('.sqn_no').val(sqn)
                //         sqn += 1
                        
                //         triggerformany =  true
                //         cylinderTable.find("tbody").append(newRow)
                //     }
                // }else{ 
                //     let existRows = $('#opiniondt').find('.existing-data')
                //     let id = existRows.data('row-id')
                //     let secondRow = existRows.siblings('.row-id-' + id)
                //     let cylinderTable = secondRow.find('.cylinder-table')
                //     let cylinderTableRow = cylinderTable.find('.new-row')
                    
                //     if(triggerformany ==  false){
                //         console.log('msuk sni10')
            //     if(barcodemanyfor == '' || serialmanyfor == '')         {
            //     alert("No Cylinder Found");
            // }
                //         let newRow = $(innerTableTbodyRowFormat())
                //         newRow.find('.dt_sqn_no').val('2')
                //         newRow.find('.barcode').val($('.barcodell').val())
                //         newRow.find('.serialno').val($('.serialll').val())
                //         newRow.find('.descr').val('null')
                //         newRow.find('.loadingdt').val('null')
                //         newRow.find('.dnnodt').val('null')
                //         newRow.find('.dndatedt').val('null')
                //         newRow.find('.driverdt').val($('.driver').val()) // keep track of changing values
                //         newRow.find('.sqn_no').val(sqn)
                //         sqn += 1
                        
                //         triggerformany =  true
                //         cylinderTable.find("tbody").append(newRow)
                //     }
                // }

            }).fail(function (response) {
                console.log("Error" + response);
                $('.loading-overlay').hide();
            })
        } else {
            console.log('msuk sni11')
            if(barcodemanyfor == '' || serialmanyfor == ''){
            alert("No Input Detected");
        }
            $('.loading-overlay').hide();
        }

    }

    /**
     * On Change Barcode,
     * 1. compare the value of barcode against database records
     * 2. if found, search the existing "category" & "product" in the main-table above
     *      3. if found, add a new-row to the inner-table of this main-table
     * 
     * NOTE: 
     *      - we are comparing the barcode value against the first tr (main-row)
     *      - we only use row-id to find the next tr (second-row)
     * 
     *          <tr class="main-row"> </tr> 
     *          <tr class="second-row"> </tr>
     * * */

    $('#load-cyl').click(function () {

        let barcode = $('#barcodell').val()
        let serial = $('#serialll').val()

        let existingTrigger = []

        let existRows = $('#opiniondt').find('.existing-data')
        existRows.each(function (index) {
            triggerexist = true
            triggernotexist = true
            existingTrigger.push('2')
        })

        if(triggerexist == false){

            $('#add-new').trigger('click')
            triggerexist = true
            existingTrigger = [];
        }   

        if(triggernotexist == false){
            
            $('#add-new').trigger('click')
            triggernotexist = true
            existingTrigger = [];
        }

        if(loadcylinder == true){
            if(barcode == ''){
                serial = serialmanyfor
                serial = serial.trim()
            }else{
                barcode = barcodemanyfor
                barcode = barcode.trim()
            }

        }else{
            if(barcode = ''){
                serial = $('#serialll').val()
                serial = serial.trim()
            }else{
                barcode = $('#barcodell').val()
                barcode = barcode.trim()
            }
            var isAdded = false;
        } 


        $('.loading-overlay').show();
        let card = $('#barcodell').parents('.card');
        let date = card.find('#datell'); 
        let driver = card.find('.driverll');
        var isAdded = false;
        let broute = $('#barcodell').data('item2');
        let route = broute.replace('ACC_CODE', $('#debtor-select').val())

        if (barcode == '') {
            let isBarcode = false;
            filterRecords(serial, route, date, driver, isAdded, isBarcode);
        } else {
            let isBarcode = true;
            filterRecords(barcode, route, date, driver, isAdded, isBarcode);
        }

    })

    $('#unload-cyl').click(function () {
        let barcodeul = ''
        let serialul = ''
        if(unloadcylinder == true){
            barcodeul = barcodemanyfor;
            serialul = serialmanyfor;
            barcodeul = barcodeul.trim()
            serialul = serialul.trim()

        }else{
            barcodeul = $('#barcodeul').val()
            serialul = $('#seriaull').val()
            barcodeul = barcodeul.trim()
            serialul = serialul.trim()
            var isAdded = false;
        }  

        let card = $('#barcodeul').parents('.card')
        let date = card.find('#dateul')
        let driver = card.find('.driverul')
        var isAdded = false;
        let mainRows = $('#opiniondt').find('.main-row')
        let existRows = $('#opiniondt').find('.existing-data')
        var total = mainRows.length;

        console.log('msuk sni')
        existRows.each(function (index) {

            let existRow = $(this)
            let id = $(this).data('row-id')
            let secondRow = $(this).siblings('.row-id-' + id)

            let cylinderTable = secondRow.find('.cylinder-table')
            let cylinderTableRow = cylinderTable.find('.new-row')
            //readonly row
            // $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

            //disable select 2
            $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

            //put value to database hidden input form
            let cat = $('.existing-data-id-' + id).find('.category').val()
            $('.existing-data-id-' + id).find('.categorydt').val(cat)

            let prod = $('.existing-data-id-' + id).find('.product').val()
            $('.existing-data-id-' + id).find('.productdt').val(prod)

            let cap = $('.existing-data-id-' + id).find('.capacity').val()
            $('.existing-data-id-' + id).find('.capacitydt').val(cap)

            let ps = $('.existing-data-id-' + id).find('.pressure').val()
            $('.existing-data-id-' + id).find('.pressuredt').val(ps)

            //Get value of unloading list barcode if match in the existing row of each inner table
            //then update the uloading date
            //


            cylinderTableRow.each(function () {

                let barcode = $(this).find('.barcode').val() == '' ? $(this).find('.barcode').html() : $(this).find('.barcode').val()
                let serial = $(this).find('.serialno').val() == '' ? $(this).find('.serialno').html() : $(this).find('.serialno').val()
                let uloadingdate = $(this).find('.uloadingdt')
                let loading = $(this).find('.loadingdt').val() == '' ? $(this).find('.loadingdt').text() : $(this).find('.loadingdt').val();


                if (barcodeul == '') {
                    if (serial == serialul && uloadingdate.val() == '') {
                        $(this).find('.uloadingdt').val(date.val())
                        $(this).find('.driverdt').val(driver.val())
                        $(this).find('.datetime').val(date.val())

                        $('.serialul').val(serial)
                        isAdded = true;

                        if(barcodemanyfor == '' || serialmanyfor == ''){
                        alert("Unloading Cylinder Succesfully Added")
                        }
                        $('#barcodeul').val('')
                        $('#seriaull').val('')
                    }
                } else {
                    if (barcode == barcodeul && uloadingdate.val() == '') {
                        $(this).find('.uloadingdt').val(date.val())
                        $(this).find('.driverdt').val(driver.val())
                        $(this).find('.datetime').val(date.val())

                        $('.serialul').val(serial)
                        isAdded = true;
                        if(barcodemanyfor == '' || serialmanyfor == ''){
                        alert("Unloading Cylinder Succesfully Added")
                        }
                        $('#barcodeul').val('')
                        $('#seriaull').val('')
                    }
                }

            })
        })

        if (!isAdded) {
            mainRows.each(function (index) {

                let mainRow = $(this)
                let id = $(this).data('row-id')
                let secondRow = $(this).siblings('.row-id-' + id)

                let cylinderTable = secondRow.find('.cylinder-table')
                let cylinderTableRow = cylinderTable.find('.new-row')
                let total_2 = cylinderTableRow.length;

                //readonly row
                $('.main-row-id-' + id).find('.exist-val-cy').prop('readonly', true)

                //disable select 2
                $('.main-row-id-' + id).find('.exist-val-cy-select').prop('disabled', true)

                //put value to database hidden input form
                let cat = $('.main-row-id-' + id).find('.category').val()
                $('.main-row-id-' + id).find('.categorydt').val(cat)

                let prod = $('.main-row-id-' + id).find('.product').val()
                $('.main-row-id-' + id).find('.productdt').val(prod)

                let cap = $('.main-row-id-' + id).find('.capacity').val()
                $('.main-row-id-' + id).find('.capacitydt').val(cap)

                let ps = $('.main-row-id-' + id).find('.pressure').val()
                $('.main-row-id-' + id).find('.pressuredt').val(ps)

                //Get value of unloading list barcode if match in the existing row of each inner table
                //then update the uloading date
                //
                cylinderTableRow.each(function (index_2) {
                    let barcode = $(this).find('.barcode').val();
                    let serial = $(this).find('.serialno').val();
                    let uloadingdate = $(this).find('.uloadingdt');
                    let loadingdate = $(this).find('.loadingdt').val();

                    if (barcodeul == '') {
                        if (serial == serialul && uloadingdate.val() == '') {
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialul').val(serial)
                            isAdded = true;
                            if(barcodemanyfor == '' || serialmanyfor == ''){
                            alert("Unloading Cylinder Succesfully Added")
                            }
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                            return false;
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                if(barcodemanyfor == '' || serialmanyfor == ''){
                                alert("No Cylinder Found");
                            }
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                            }
                        }
                    } else {
                        if (barcode == barcodeul && uloadingdate.val() == '') {
                            $(this).find('.uloadingdt').val(date.val())
                            $(this).find('.driverdt').val(driver.val())
                            $('.serialul').val(serial)
                            isAdded = true;
                            if(barcodemanyfor == '' || serialmanyfor == ''){
                            alert("Unloading Cylinder Succesfully Added")
                            }
                            $('#barcodeul').val('')
                            $('#seriaull').val('')
                            return false;
                        } else {
                            if (index === total - 1 && index_2 === total_2 - 1) {
                                // this is the last one
                                if(barcodemanyfor == '' || serialmanyfor == ''){
                                alert("No Cylinder Found");
                                }
                                $('#barcodeul').val('')
                                $('#seriaull').val('')
                            }
                        }
                    }
                })
            })
        }

        if (mainRows.length == 0 && !isAdded) {
            if(barcodemanyfor == '' || serialmanyfor == ''){
            alert("No Cylinder Found");
            }
            $('#barcodeul').val('')
            $('#seriaull').val('')
        }
    })

    
    let driver = $('.driver').val()

    $('.driverpr').val(driver)
    $('.driverll').val(driver)
    $('.driverul').val(driver)
    
    //Get Driver Details
    $('.driver').on('change', function () {
        let driver = $(this).val()

        $('.driverll').val(driver)
        $('.driverul').val(driver)

    })

})
