$('#memo_div').on('hidden.bs.collapse', function () { $('#memo_button').addClass('d-none') })
$('#memo_div').on('shown.bs.collapse', function () { $('#memo_button').removeClass('d-none') })

$('#add-header-footer').on('click', function() {
    $('#add-on').hasClass('d-none') ? $('#add-on').removeClass('d-none') : $('#add-on').addClass('d-none');
})

$('#memo').summernote({ placeholder: 'Memo', height: 100, followingToolbar: false, toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
    ],
})