let systemSettings = $('.system-settings-param')
let boolRounding = systemSettings.data('rounding')

let leftPad = function (value, length) {
    return ('0'.repeat(length) + value).slice(-length);
}

$('#rounding-adjustment').change(function () {
    boolRounding = $(this).prop('checked') // update the value

    // if the rounding is allowed, unhide the Rounding in totaling blade
    if (boolRounding)
        $('#rounding-container').hasClass('d-none') ? $('#rounding-container').removeClass('d-none') : ''
    else
        $('#rounding-container').hasClass('d-none') ? '' : $('#rounding-container').addClass('d-none')
})

// custom select2
function formatStateInTable(state) {
    return state.id
}

// overiding all current select2 and summernote in the table forms into using formatStateInTable
$('.form-select2').select2({
    templateSelection: formatStateInTable,
    dropdownAutoWidth: true
})

let rowID = 0;
let taxCodes = $('.tax-container').data('taxes')

$('#item-master-datatable').DataTable()

$('.sequence_no').on('change', function(){
    console.log('sequnce trigger')
})
let intTSN = 0
$('.sequence_no').each(function () {
    intTSN += 5
})

// trigger on click new button
$('#add-new').on('click', function () {
    rowID++
    let template = $('.template-new').clone().removeClass('template-new d-none').addClass('bill')
    let rowDetailID = 'row-detail-' + rowID
    let tSN = leftPad(intTSN, 4) // 0005
    template.addClass(tSN)
    let itemCodeSelect = template.eq(0).find('.serial')

    // adding newly added serial to select2
    // itemCodeSelect.select2({
    //     templateSelection: formatStateInTable,
    //     dropdownAutoWidth: true,
    //     tags: true
    // })
    // let contents = itemCodeSelect.data('contents')
    // // console.log(contents)
    // contents.forEach(function (item) {
    //     let newItem = new Option(item, item, false, false);
    //     itemCodeSelect.append(newItem)
    // })
    // itemCodeSelect.trigger('change')
    // disabling the form submit button since the item code still empty

    // $('.btn-form-submit').prop('disabled', true)

    // start cloning
    // template.eq(0).find('.serial').select2({
    //     templateSelection: formatStateInTable,
    //     dropdownAutoWidth: true
    // })
    template.eq(0).find('.serial').attr('required', true)
    template.eq(0).find('.serial').val('')
    template.eq(0).find('.barcode').val('')
    template.eq(0).find('.subject').val('')
    template.eq(0).find('.product').val('')

    template.eq(0).find('.subject').val()
    template.eq(0).find('.sequence_no').val(tSN)
    template.eq(0).find('.quantity').val(1)
    template.eq(0).find('.unit_measure').select2({
        templateSelection: formatStateInTable,
        dropdownAutoWidth: true
    })
    template.eq(0).find('.type').val(null)
    template.eq(0).find('.unit_price').val(0)
    template.eq(0).find('.unit_price').inputmask('currency', {
        prefix: '',
        rightAlign: true
    })
    template.eq(0).find('.discount').val(0)
    template.eq(0).find('.discount').inputmask({
        rightAlign: true
    })
    template.eq(0).find('.amount').val(0)
    template.eq(0).find('.amount').inputmask('currency', {
        prefix: '',
        rightAlign: true
    })
    template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)
    template.eq(1).attr('id', rowDetailID)
    $('#table-form-tbody').append(template)

    // after adding to the tbody, increment sequence_no for the upcoming button click
    intTSN += 5

    // hide this button temporarily
    $("#add-new").addClass('d-none')
    $("#add-new-item").addClass('d-none')

    // set on event handler
    onHandler(rowDetailID, template.eq(0), template.eq(1))
})

$('.existing-data').each(function () {
    let rowID = $(this).data('item-id')
    let deleteRoute = $(this).data('delete-route')
    deleteRoute = deleteRoute.replace('id', rowID)
    let row = $(this)
    let rowDetailID = 'details_' + rowID

    // row.find('.serial').select2({
    //     templateSelection: formatStateInTable,
    //     dropdownAutoWidth: true
    // })
    row.find('.unit_measure').select2({
        templateSelection: formatStateInTable,
        dropdownAutoWidth: true
    })

    // normal operations handler
    onHandler(rowDetailID)

    // delete handler
    row.find('.remove-row').on('click', function () {
        console.log(deleteRoute)
        console.log('here')
        $.ajax({
            'method': 'DELETE',
            'url': deleteRoute
        }).done(function (response) {
            console.log(response)
        })
    })
})

// predefined actions to be done
function onHandler(rowDetailID) {

    $('.serial').on('change', function () {
        let getCylinderRoute = $(this).data('get-cylinder-route')
        let row = $(this).parents('tr')
        // console.log("test")
        if (!$(this).val()) {
            // keep the form submit button disabled and hide the add-new button
            $('.btn-form-submit').prop('disabled', true)
            $("#add-new").addClass('d-none')
            $("#add-new-item").addClass('d-none')
        } else {
            // re-enable the form submit button since the item code is now filled
            $('.btn-form-submit').prop('disabled', false)
            $("#add-new").removeClass('d-none')
            getCylinderRoute = getCylinderRoute.replace('SERIAL', $(this).val())

            row.find('.toggle-details').attr('data-target', '#' + rowDetailID)

        }
    })

    $('.calculate-field').change(function () {
        let row = $(this).parent().parent()
        let amount = row.find('.amount').val()
        let uprice = row.find('.unit_price').val().replace(',', '')
        let qty = row.find('.quantity').val()
        let discount = row.find('.discount')
        let theDiscount = (discount.val().indexOf('%') > -1) ? discount.val().replace('%', '') : discount.val()
        let taxRate = row.find('.tax_rate').val()

        // calculate discount
        let discountAmount = 0.00
        if (discount.val().indexOf('%') > -1) {
            discountAmount = uprice * qty * theDiscount / 100
            row.find('.discount').val(discountAmount)
        } else {
            discountAmount = row.find('.discount').val()
        }

        let price = (uprice * qty) - discountAmount
        row.find('.amount').val(price)
        row.find('.amount_dt').val(price)

        // calculate tax
        let taxamount = 0.00
        if (taxRate > 0)
            taxamount = price / taxRate
        row.find('.tax_amount').val(taxamount)
        row.find('.tax_amount_dt').val(taxamount)

        // final price
        let finalPrice = price + taxamount
        row.find('.taxed_amount').val(finalPrice)
        row.find('.taxed_amount_dt').val(finalPrice)

        // update the subtotal and grandtotal fields
        updateTotals()
    })

    $('.subject').on('keyup', function (e) {
        if ($('#add-new').hasClass('d-none')) {
            $("#add-new").removeClass('d-none')
        }

        if ($(this).val() === '') {
            if (!$('#add-new').hasClass('d-none')) {
                $("#add-new").addClass('d-none')
            }
        }
    })

    $('.remove-row').on('click', function () {
        $(this).closest('tr').next('tr').remove()
        $(this).parent().parent().remove()
        $("#add-new").removeClass('d-none')
        // $("#add-new-item").removeClass('d-none')

        updateTotals()
    })

    $('.discount_mt').on('change', function () {
        updateTotals()
    })

    function updateTotals() {
        let amountFields = $('.amount')
        let taxFields = $('.tax_amount')
        let discountFields = $('.discount')
        let globalDiscount = $('.discount_mt')

        let subtotal = 0.00
        for (let i = 1; i < amountFields.length; i++) {
            subtotal += parseFloat(amountFields.eq(i).val().replace(',', ''))
        }
        
        let taxes = 0.00
        for (let i = 1; i < taxFields.length; i++)
            taxes += parseFloat(taxFields.eq(i).val().replace(',', ''))

        // calculate subtotal
        let sumOfAmountNTaxes = subtotal + taxes

        // calculate discount
        let globalDiscountAmount = 0.00
        let theGlobalDiscount = (globalDiscount.val().indexOf('%') > -1) ? globalDiscount.val().replace('%', '') : globalDiscount.val()
        if (globalDiscount.val().indexOf('%') > -1)
            globalDiscountAmount = sumOfAmountNTaxes * theGlobalDiscount / 100
        else
            globalDiscountAmount = theGlobalDiscount

        let discountedPrice = sumOfAmountNTaxes - globalDiscountAmount;
        let grandTotal = (sumOfAmountNTaxes) - globalDiscountAmount

        // show subtotal and tax
        $('#subtotal').val(subtotal.toFixed(2))
        $('#tax').val(taxes.toFixed(2))

        // show rounding if allowed
        if (boolRounding) {
            let rounding = calculateRounding(discountedPrice)
            let roundedPrice = calculateRoundedPrice(discountedPrice, rounding)
            $('#rounding').val(rounding)
            $('#grand_total').val(roundedPrice.toFixed(2))
            $('.amount_mt').val(roundedPrice.toFixed(2)) // for credit note, need to put the subtotal at the top
            $('.remaining_amount_mt').val(roundedPrice.toFixed(2)) // for credit note, need to put the subtotal at the top
            amount = roundedPrice
            remainingAmount = roundedPrice

        } else {
            $('#grand_total').val(grandTotal.toFixed(2))
            $('.amount_mt').val(grandTotal.toFixed(2)) // for credit note, need to put the subtotal at the top
            $('.remaining_amount_mt').val(grandTotal.toFixed(2)) // for credit note, need to put the subtotal at the top
            amount = grandTotal
            remainingAmount = grandTotal
        }
    }
}
