// prevent # from jumping to top of the screen
$('a').on('click', function(e) {
    if ($(this).attr('href') == '#') {
        e.preventDefault();
    }
})