let leftPad = function(value, length) {
    return ('0'.repeat(length) + value).slice(-length);
}

// let transformString = function(prefix, separator, last_no, zeroes) {
//     let runningNumberString = ''
//     let nextNumber = last_no;
//     let leftPadNumbersString = leftPad(nextNumber, zeroes);
//     runningNumberString = prefix + separator + leftPadNumbersString

//     return runningNumberString
// }