let id = 0;

$('.productcoy').on('change', function(){ 
    let productcoy = $(this).val()
    $('.product').val(productcoy)
})

$('.editBarcode').on('click', function () {
    if ($('.barcodecoy').val() == '') {
        $('.barcodecoy').prop('disabled', true)
    } else {
        $('.barcodecoy').prop('disabled', false)
    }

});
$('.editSerial').on('click', function () {
    if ($('.serialcoy').val() == '') {
        $('.serialcoy').prop('disabled', true);
    } else {
        $('.serialcoy').prop('disabled', false);
    }
});
$('.editCategory').on('click', function () {
    if ($('.categorycoy').val() == '') {
        $('.categorycoy').prop('disabled', true);
    } else {
        $('.categorycoy').prop('disabled', false);
    }
});
$('.editGroup').on('click', function () {
    if ($('.groupcoy').val() == '') {
        $('.groupcoy').prop('disabled', true);
    } else {
        $('.groupcoy').prop('disabled', false);
    }
});
$('.editProduct').on('click', function () {
    if ($('.productcoy').val() == '') {
        $('.productcoy').prop('disabled', true);
    } else {
        $('.productcoy').prop('disabled', false);
    }
});
$('.editOwnership').on('click', function () {
    if ($('.ownercoy').val() == '') {
        $('.ownercoy').prop('disabled', true);
    } else {
        $('.ownercoy').prop('disabled', false);
    }
});
$('.editHwtype').on('click', function () {
    if ($('.hwtypecoy').val() == '') {
        $('.hwtypecoy').prop('disabled', true);
    } else {
        $('.hwtypecoy').prop('disabled', false);
    }
});
$('.editMfr').on('click', function () {
    if ($('.mfrcoy').val() == '') {
        $('.mfrcoy').prop('disabled', true);
    } else {
        $('.mfrcoy').prop('disabled', false);
    }
});
$('.editValve').on('click', function () {
    if ($('.valvetypecoy').val() == '') {
        $('.valvetypecoy').prop('disabled', true);
    } else {
        $('.valvetypecoy').prop('disabled', false);
    }
});


$('.barcodecoy').on('change', function(){
    let barcodecoy = $(this).val()
    $('.barcode').val(barcodecoy)
})
$('.serialcoy').on('change', function(){
    let serialcoy = $(this).val()
    $('.serial').val(serialcoy)
})
$('.categorycoy').on('change', function(){
    let categorycoy = $(this).val()
    $('.category').val(categorycoy)
})
$('.groupcoy').on('change', function(){ 
    let groupcoy = $(this).val()
    $('.group').val(groupcoy)
})
$('.ownercoy').on('change', function(){ 
    let ownercoy = $(this).val()
    $('.owner').val(ownercoy)
})
$('.hwtypecoy').on('change', function(){ 
    let hwtypecoy = $(this).val()
    $('.hwtype').val(hwtypecoy)
})
$('.mfrcoy').on('change', function(){ 
    let mfrcoy = $(this).val()
    $('.mfr').val(mfrcoy)
})
$('.valvetypecoy').on('change', function(){ 
    let valvetypecoy = $(this).val()
    $('.valvetype').val(valvetypecoy)
})


// function appendReasons() {
    
//     return `
//         <label for="" class="col-md-2">Reasons</label>
//         <div class="col-md-5">
//             <input name="reasons" type="text" maxlength="100" class="form-control reasons"
//                 value="">
//         </div>
//     `
// }

// $('.check-status').on('click',function(){
//     alert('Please Provide Reasons')
//     let newRow = $(appendReasons())
//     $('form').find(".cylinder-status").empty().append(newRow)
//     // $('form').find(".reasons").removeAttr('disabled')
//     $('.reasons').on('change',function(){
//         // $(this).attr('disabled','disabled')
//     })

// })