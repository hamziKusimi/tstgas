// select2 on debtor select
let acode = $('.system-settings-param').data('acode')
let acodeName = $('.system-settings-param').data('acode-name')
let existingAcodes = $('#debtor-select').data('acodes')
let search = ''

// for the acode, use a different template to display back results on the select box
function formatState (state) {
    // put the name (only - without the id) values into the next textbox
    let name = state.text.replace(state.id + ' ', '')
    $('#m_drac_name').val(name)

    // put the name (only id) value into the select2 dropdown
    return state.id
}

// get the value from select2 search to transfer to the ajax_acode field on the modal
// on press enter to register key and trigger modal
$(document).on('keyup', '.select2-search__field', function (event) {
    search = $(this).val()
    // let keycode = event.which
    // if (keycode === 13)
    //     $('#createNewAccountOption').modal('toggle')

})

$('#debtor-select').select2({
    templateSelection: formatState,
    dropdownAutoWidth : true,
    tags: true,
})
$('#debtor-select').on('select2:open', () => {
    $('.select2-results:not(:has(a))').append($('#createNewAccountOption')).trigger('change')
    $('#createNewAccountOption').on('click', function() {
        $('#debtor-select').select2('close')
        $('#ajax_acode').val(search)
        $('#create-new-account').on('click', function() {
            let code = $('#ajax_acode').val()
            let name = $('#ajax_name').val()

            let newAccount = new Option(name, code, true, true);
            $('#debtor-select').append(newAccount).trigger('change')
            // $('#m_drac_name').val(name)
            $('#m_detail').val($('#ajax_address').val())
            $('#m_cptel').val($('#ajax_phone').val())
            $('#m_fax').val($('#ajax_fax').val())
            $('#m_cterm').val($('#ajax_credit_term').val())
            let address = $('#ajax_address').val().split('\n')

            // AJAX call
            let data = {
                'D_DC'      : 'D',
                'D_ACODE'   : $('#ajax_acode').val(),
                'D_TYPE'    : $('#ajax_type').val(),
                'D_NAME'    : $('#ajax_name').val(),
                'D_ADDR1'   : typeof(address[0]) !== 'undefined' ? address[0] : '',
                'D_ADDR2'   : typeof(address[1]) !== 'undefined' ? address[1] : '',
                'D_ADDR3'   : typeof(address[2]) !== 'undefined' ? address[2] : '',
                'D_ADDR4'   : typeof(address[3]) !== 'undefined' ? address[3] : '',
                'D_TEL'     : $('#ajax_phone').val(),
                'D_HP'      : $('#ajax_mobile').val(),
                'D_FAX'     : $('#ajax_fax').val(),
                'D_EMAIL'   : $('#ajax_email').val(),
                'D_CTERM'   : $('#ajax_credit_term').val(),
                'D_CLIMIT'  : $('#ajax_credit_limit').val(),
                'd_gstno'   : $('#ajax_gst_no').val(),
                'd_brn'     : $('#ajax_brn_no').val(),
                'D_MEMO'    : $('#ajax_memo').val(),
            }

            $.ajax({
                data: data,
                method: "POST",
                url: "{{ route('debtors.api.store') }}",
            }).done(function(response) {
                $('#success-message').find('.custom-message').text('Successfully added new account')
                $('#success-message').fadeIn(1000).removeClass('d-none').delay(5000).fadeOut(1000, function() {
                    $(this).addClass('d-none')
                })

            }).fail(function(response) {
                $('#failure-message').find('.custom-message').text('Failed adding new account')
                $('#failure-message').fadeIn(1000).removeClass('d-none').delay(5000).fadeOut(1000, function() {
                    $(this).addClass('d-none')
                })
            })

            toggleDetails()
        })
    })
})
$('#debtor-select').on('change', function() {
    let acodes = $(this).data('acodes')
    // if this value exist in array, then update the address
    if ($.inArray($(this).val(), acodes) !== -1) {
        let data = $(this).data('master')
        let value = $(this).find(':selected').val()
        let selectedData = data.filter(record => record.d_acode == value)[0]
        let address = `${selectedData.d_addr1} \n${selectedData.d_addr2} \n${selectedData.d_addr3} \n${selectedData.d_addr4}`
        let creditTerm = selectedData.d_cterm.replace('DAYS', '')
        if (creditTerm === '') creditTerm = 0
        let dueDate = moment(invoiceDate).add(creditTerm, 'days')
        def_price = selectedData.def_price

        $('#m_detail').text(address)
        $('#m_cptel').val(selectedData.d_cptel)
        $('#m_fax').val(selectedData.d_fax)
        $('#m_cterm').val(selectedData.d_cterm)
        $('#m_due_date-picker').datetimepicker('destroy')
        $('#m_due_date-picker').datetimepicker({ date: dueDate, format: 'DD/MM/YYYY' })
        $('#suppinvdate-picker').datetimepicker('destroy')
        $('#suppinvdate-picker').datetimepicker({ date: dueDate, format: 'DD/MM/YYYY' })

        toggleDetails()
    }
})

let toggleDetails = function() {
    $('#edit-address-button').removeClass('d-none').on('click', function() {
        $('#m_detail').hasClass('form-control-plaintext') ?
            $('#m_detail').removeClass('form-control-plaintext', false).addClass('form-control').prop('readonly', false) :
            $('#m_detail').removeClass('form-control', true).addClass('form-control-plaintext').prop('readonly', true)

        $('#m_cptel').prop('readonly') ? $('#m_cptel').prop('readonly', false) : $('#m_cptel').prop('readonly', true)
        $('#m_fax').prop('readonly') ? $('#m_fax').prop('readonly', false) : $('#m_fax').prop('readonly', true)
    })
}
