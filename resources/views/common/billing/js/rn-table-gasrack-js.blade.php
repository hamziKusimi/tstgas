let intTSN2 = 0
let tot_seq2 = $('.grsequence_no').length;

if (tot_seq2 > 1) {
    $('.grsequence_no').each(function (index) {

        if (index === tot_seq2 - 1) {
            intTSN2 = parseInt($(this).val()) + 1;
        }
    })
} else {
    intTSN2 = 1;
}


let rowID2 = 0;
let tot_seq2_rowID = $('.grcontent-id').length;

if (tot_seq2_rowID > 1) {
    $('.grcontent-id').each(function (index) {

        if (index === tot_seq2_rowID - 1) {
            rowID2 = parseInt($(this).val()) + 1;
        }
    })
} else {
    rowID2 = 1;
}


// trigger on click new button
$('#add-new2').on('click', function () {
    
    //must fill in debtor account code before return note
    if($('#debtor-select').val() == ''){
        alert('Please Fill In Customer Details')
        location.reload();
    }else{
        mainTableCounter = mainTableCounter + 1
        rowID2++ // sini, tambah satu
        let template = $('.main-table-row-template2').clone().removeClass('main-table-row-template2 d-none').addClass('main-row2')
        let secondRowTemplate = $('.second-row-template2').clone().removeClass('second-row-template2').addClass('second-row2')

        //initialize each main row and child table main-row2,row-id
        let mainRowIdentifier = 'main-row2-id-' + rowID2
        let secondRowIdentifier = 'row-id2-' + rowID2
        template.addClass(mainRowIdentifier)
        template.attr('data-row-id2', rowID2)
        secondRowTemplate.addClass(secondRowIdentifier)
        secondRowTemplate.attr('data-row-id2', rowID2)

        // console.log(secondRowTemplate)
        let rowDetailID = 'row-detail-' + rowID2
        let tSN = leftPad(intTSN2, 0) // 0005]

        // start cloning
        // if((preparegasrack == true) || (loadgasrack == true)){

        //     template.eq(0).find('.grsequence_no').val(tSN)
        //     template.eq(0).find('.grtype').select2({
        //         templateSelection: formatStateInTable,
        //         dropdownAutoWidth: true
        //     })
        //     template.eq(0).find('.quantity').val(1)
        //     template.eq(0).find('.price').attr("required", true);
        //     template.eq(0).find('.toggle-inner-table-button').attr('data-second-row-id', secondRowIdentifier)
        //     // template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)

        //     $('#table-form-tbody-gasrack').append(template)
        //     $('#table-form-tbody-gasrack').append(secondRowTemplate)

        //     // after adding to the tbody, increment sequence_no for the upcoming button click
        //     intTSN2 += 1

        // }else{\

            template.eq(0).find('.grsequence_no').val(tSN)
            // template.eq(0).find('.grtype').select2({
            //     templateSelection: formatStateInTable,
            //     dropdownAutoWidth: true
            // })
            // template.eq(0).find('.quantity').val(1)
            // template.eq(0).find('.price').attr("required", true);
            template.eq(0).find('.toggle-inner-table-button').attr('data-second-row-id', secondRowIdentifier)  
            if(grtriggerexist == false){
                template.eq(0).find('.exist-gasrack').addClass('has-gasrack')
                template.eq(0).find('.exist-gasrack').html('Exist Gas Rack')
            }else{
                template.eq(0).find('.exist-gasrack').addClass('no-gasrack')
                template.eq(0).find('.exist-gasrack').html('Not Exist Gas Rack')
            }
            // template.eq(0).find('.toggle-details').attr('data-target', '#' + rowDetailID)

            $('#table-form-tbody-gasrack').append(template)
            $('#table-form-tbody-gasrack').append(secondRowTemplate)

            // after adding to the tbody, increment sequence_no for the upcoming button click
            intTSN2 += 1

        // }

        // set on event handler
        onHandler(rowDetailID, template.eq(0), template.eq(1))
    }
})

// Edit on adding new row-id-(i)

$('.existing-data').each(function () {

    rowID++

})


// predefined actions to be done
function onHandler(rowDetailID) {

    //delete the main row
    $('.remove-row').on('click', function () {
        $(this).closest('tr').next('tr').remove()
        $(this).parent().parent().remove()
        // $("#add-new").removeClass('d-none')
    })


}

// //dynamically get function on click based on opinion dt, remove-row-child

$('#gasrackdt').on('click', '.remove-row-child', function () {

    let row = $(this).parents('.new-row2')
    let route = $(this).data('delete-routelist');
    let loadingDate = row.find("td:eq(4)").text();
    let unloadingDate = row.find("td:eq(5)").text();
    let isExistingData = row.data("isexisted");
    let method = "POST"

    $('.delete-overlay2').show();
    if (unloadingDate === "" && loadingDate === "") {

        if (confirm('This Action Will Remove Gasrack Data From The List, Are You Sure?')) {
            let data = {
                'dnno': $(this).data('dnno'),
                'sqnex': $(this).data('sqndt'),
                'sqnlist': $(this).data('sqnlist'),
                'type': 'gr'
            }

            // delete from db
            $.ajax({
                url: route,
                method: method,
                data: data,
            }).done(function (response) {
                alert("Loading and Unloading Data Removed");
                $('.delete-overlay2').hide();
                row.remove()
            }).fail(function (response) {
                alert("Error: " + response);
            })
        } else {
            $('.delete-overlay2').hide();
            return false
        }
    } else if (unloadingDate === "") {
        let data = {
            'dnno': $(this).data('dnno'),
            'sqnex': $(this).data('sqndt'),
            'sqnlist': $(this).data('sqnlist'),
            'type': 'gr',
            'only-LL': true
        }

        $.ajax({
            url: route,
            method: method,
            data: data
        }).done(function (response) {
            alert("loading Data Removed");
            $('.delete-overlay2').hide();
            row.find("td:eq(4)").empty();
            var input = '<input class="form-control loadingdt calculate-field" readonly="" name="edit_ll_date[]" type="text">'
            input += '<input name="edit_ll_barcode[]" type="hidden" value="' + row.find('.barcode').html() + '">';
            input += '<input name="edit_ll_serial[]" type="hidden" value="' + row.find('.serialno').html() + '">';
            input += '<input name="edit_ll_sequence_no[]" type="hidden" value="' + response.sqnex + '">';
            input += '<input name="edit_ll_sqn_no[]" type="hidden" value="' + response.sqnlist + '">';
            input += '<input name="edit_ll_datetime[]" class="datetime" type="hidden" value="">';
            input += '<input name="edit_ll_driver[]" type="hidden" value="' + row.find('.driver').val() + '">';
            input += '<input name="edit_ll_type[]" type="hidden" value="gr">';

            row.find("td:eq(4)").append(input);

        }).fail(function (response) {
            alert("Error: " + response);
            console.log(response)
        })

    } else if (isExistingData === undefined) {
        $('.delete-overlay2').hide();
        row.remove()
    } else {
        let data = {
            'dnno': $(this).data('dnno'),
            'sqnex': $(this).data('sqndt'),
            'sqnlist': $(this).data('sqnlist'),
            'type': 'gr',
            'only-UL': true
        }

        $.ajax({
            url: route,
            method: method,
            data: data
        }).done(function (response) {
            alert("Unloading Data Removed");
            $('.delete-overlay2').hide();
            row.find("td:eq(5)").empty();
            var input = '<input class="form-control uloadingdt calculate-field" readonly="" name="edit_ul_date[]" type="text">'
            input += '<input name="edit_ul_barcode[]" type="hidden" value="' + row.find('.barcode').html() + '">';
            input += '<input name="edit_ul_serial[]" type="hidden" value="' + row.find('.serialno').html() + '">';
            input += '<input name="edit_ul_sequence_no[]" type="hidden" value="' + response.sqnex + '">';
            input += '<input name="edit_ul_sqn_no[]" type="hidden" value="' + response.sqnlist + '">';
            input += '<input name="edit_ul_datetime[]" class="datetime" type="hidden" value="">';
            input += '<input name="edit_ul_driver[]" type="hidden" value="' + row.find('.driver').val() + '">';
            input += '<input name="edit_ul_type[]" type="hidden" value="gr">';

            row.find("td:eq(5)").append(input);

        }).fail(function (response) {
            alert("Error: " + response);
            console.log(response)
        })
    }

})

$('#gasrackdt').on('click', '.remove-row', function () {
    console.log('test')
    let row = $(this).parents('.sortable-row')
    let route = $(this).data('delete-route');
    let method = "DELETE"

    row.remove()

    // delete from db
    $.ajax({
        url: route,
        method: method,
    }).done(function (response) {}).fail(function (response) {})
})
