@foreach (['danger', 'info', 'warning', 'success'] as $msg)
	@if(Session::has('alert-' . $msg))
		<div class="alert alert-{{ $msg }} alert-dismissible mt-2 mb-0">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		@if($msg == 'danger')
			<h5><i class="icon fa fa-ban"></i> Error</h5>
		@elseif ($msg == 'info')
			<h5><i class="icon fa fa-info"></i> Info</h5>
		@elseif ($msg == 'warning')
			<h5><i class="icon fa fa-warning"></i> Warning</h5>
		@elseif ($msg == 'success')
			<h5><i class="icon fa fa-check"></i> Success</h5>
		@endif

		{!! Session::get('alert-' . $msg) !!}

		</div>
	@endif
@endforeach
