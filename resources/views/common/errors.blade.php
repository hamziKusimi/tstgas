@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissible mt-2 mb-0">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        <h5><i class="icon fa fa-ban"></i> Error</h5>

        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
