<!-- Main Header -->
<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    {{-- <!-- Logo --> {{ SystemSetup::table('system_setups')->where('id',1)->pluck('name') }} --}}
    <a href="{{ route('home') }}" class="logo"><b>Seafood Station</b></a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" style="height: 50px;" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle pt-0 pb-0" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <!-- Right Side Of Navbar -->

        <ul class="nav navbar-nav mr-auto">
        </ul>
        <div class="navbar-custom-menu" style="margin-right:30px">
            <ul class="nav navbar-nav">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                                                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </nav>

</header>