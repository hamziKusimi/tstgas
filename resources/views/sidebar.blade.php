<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="padding-top: 30px;">

        <!-- Sidebar user panel (optional) -->
        {{-- <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset("vendor/adminlte/dist/img/user2-160x160.jpg")}}" class="img-circle" alt="User Image"
        />
        </div>
        <div class="pull-left info">
            <p>Alexander Pierce</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
        </div> --}}

        <!-- search form (Optional) -->
        {{-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
                </span>
            </div>
        </form> --}}
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            {{-- <li class="header"></li> --}}
            <!-- Optionally, you can add icons to the links -->
            <li class="treeview">
            <li class=""><a href="{{ route('home') }}"><span>Dashboard </span></a></li>
            </li>
            <li class="treeview">
                <a href="#"><span>Inventory</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                    @if(Auth::user()->hasPermissionTo('PURCHASE_OD_VR'))
                    <li class=""><a href="{{ route('porders.index') }}"><span>Purchase Order</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('GOOD_RC_VR'))
                    <li class=""><a href="{{ route('goods.index') }}"><span>Goods Received</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('PURCHASE_RT_VR'))
                    <li class=""><a href="{{ route('preturns.index') }}"><span>Purchase Returns</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('DELIVERY_OD_VR'))
                    <li class=""><a href="{{ route('deliveryorders.index') }}"><span>Delivery Order</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('CASH_BILL_VR'))
                    <li class=""><a href="{{ route('cashbills.index') }}"><span>Cash Bill</span></a></li>
                    @endif

                    {{-- @if(Auth::user()->hasPermissionTo('CASH_SALES_VR'))
                    <li class=""><a href="{{ route('cashsales.index') }}"><span>Cash Sales</span></a></li>
                    @endif --}}

                    @if(Auth::user()->hasPermissionTo('INVOICE_VR'))
                    <li class=""><a href="{{ route('invoices.index') }}"><span>Invoices</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('SALES_OD_VR'))
                    <li class=""><a href="{{ route('salesorders.index') }}"><span>Sales Order</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('QUOTATION_VR'))
                    <li class=""><a href="{{ route('quotations.index') }}"><span>Quotation</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('ADJUSTMENT_IN_VR'))
                    <li class=""><a href="{{ route('adjustmentis.index') }}"><span>Adjustment In</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('ADJUSTMENT_OUT_VR'))
                    <li class=""><a href="{{ route('adjustmentos.index') }}"><span>Adjustment Out</span></a></li>
                    @endif

                    @if(Auth::user()->hasPermissionTo('SALES_RETURN_VR'))
                    <li class=""><a href="{{ route('salesreturns.index') }}"><span>Sales Return</span></a></li>
                    @endif
                </ul>
            </li>
            {{-- {{ dd(Config::get('config.cylinder.cylinder')) }} --}}
            @if(config('config.cylinder.cylinder') == "true")
            <li class="treeview">
                <a href="#"><span>Cylinder</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                    <li class=""><a href="{{ route('cylindercashbills.index') }}"><span>Cash Bill</span></a></li>
                    <li class=""><a href="{{ route('cylinderinvoices.index') }}"><span>Invoice</span></a></li>
                    <li class=""><a href="{{ route('proformainvoices.index') }}"><span>Pro-Forma Invoice</span></a></li>
                    <li class=""><a href="{{ route('cylinderquotations.index') }}"><span>Quotation</span></a></li>
                    <li class=""><a href="{{ route('cylinderporders.index') }}"><span>Purchase Order</span></a></li>

                    @if(Auth::user()->hasPermissionTo('DELIVERY_NT_VR'))
                    <li class=""><a href="{{ route('deliverynotes.index') }}"><span>Delivery Note</span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('RETURN_NT_VR'))
                    <li class=""><a href="{{ route('returnnotes.index') }}"><span>Return Note</span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('REFILL_VR'))
                    <li class=""><a href="{{ route('refills.index') }}"><span>Refill</span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('REPAIR_VR'))
                    <li class=""><a href="{{ route('repairs.index') }}"><span>Repair</span></a></li>
                    @endif
                </ul>
            </li>
            @endif
            <li class="treeview">
                <a href="#"><span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @if(Auth::user()->hasPermissionTo('IV_MD_CREDITOR_VR'))
                    <li class=""><a href="{{ route('creditors.index') }}"><span>Creditor </span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('IV_MD_DEBTOR_VR'))
                    <li class=""><a href="{{ route('debtors.index') }}"><span>Debtor </span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('IV_MD_AREA_VR'))
                    <li class=""><a href="{{ route('areas.index') }}"><span>Area </span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('IV_MD_CURRENCY_VR'))
                    <li class=""><a href="{{ route('currencies.index') }}"><span>Currency </span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('IV_MD_CUSTOMER_VR'))
                    <li class=""><a href="{{ route('customertypes.index') }}"><span>Customer Type </span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('IV_MD_SALESMAN_VR'))
                    <li class=""><a href="{{ route('salesman.index') }}"><span>Salesman </span></a></li>
                    @endif
                    <li class="treeview">
                        <a href="#"><span>Inventory</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            @if(Auth::user()->hasPermissionTo('IV_MD_STOCK_VR'))
                            <li class=""><a href="{{ route('stockcodes.index') }}"><span>Stock</span></a></li>
                            @endif
                            @if(Auth::user()->hasPermissionTo('IV_MD_CATEGORY_VR'))
                            <li class=""><a href="{{ route('category.index') }}"><span>Category </span></a></li>
                            @endif
                            @if(Auth::user()->hasPermissionTo('IV_MD_PRODUCT_VR'))
                            <li class=""><a href="{{ route('products.index') }}"><span>Product </span></a></li>
                            @endif
                            @if(Auth::user()->hasPermissionTo('IV_MD_BRAND_VR'))
                            <li class=""><a href="{{ route('brands.index') }}"><span>Brand </span></a></li>
                            @endif
                            @if(Auth::user()->hasPermissionTo('IV_MD_LOCATION_VR'))
                            <li class=""><a href="{{ route('locations.index') }}"><span>Location </span></a></li>
                            @endif
                            @if(Auth::user()->hasPermissionTo('IV_MD_UOM_VR'))
                            <li class=""><a href="{{ route('uom.index') }}"><span>U.O.M </span></a></li>
                            @endif

                            @if(Auth::user()->hasPermissionTo('IV_MD_ADJUSTMENT_I_VR'))
                            <li class=""><a href="{{ route('adjustmentits.index') }}"><span>Adjustment In
                                        Type</span></a></li>
                            @endif
                            @if(Auth::user()->hasPermissionTo('IV_MD_ADJUSTMENT_O_VR'))
                            <li class=""><a href="{{ route('adjustmentots.index') }}"><span>Adjustment Out
                                        Type</span></a></li>
                            @endif
                            @if(Auth::user()->hasPermissionTo('IV_MD_REASON_VR'))
                            <li class=""><a href="{{ route('reasons.index') }}"><span>Reason </span></a></li>
                            @endif

                            {{-- <li class=""><a href="{{ route('equipments.index') }}"><span>Equipment </span></a>
                    </li> --}}
                </ul>
            </li>

            @if(config('config.cylinder.cylinder') == "true")
            <li class="treeview">
                <a href="#"><span>Cylinder</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class=""><a href="{{ route('cylinders.index') }}"><span>Cylinder </span></a></li>

                    @if(Auth::user()->hasPermissionTo('CY_CATEGORY_VR'))
                    <li class=""><a href="{{ route('cylindercategory.index') }}"><span>Cylinder Category</span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('CY_GROUP_VR'))
                    <li class=""><a href="{{ route('cylindergroups.index') }}"><span>Cylinder Group</span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('CY_PRODUCT_VR'))
                    <li class=""><a href="{{ route('cylinderproducts.index') }}"><span>Cylinder Product</span></a></li>
                    @endif
                    @if(Auth::user()->hasPermissionTo('CY_TYPE_VR'))
                    <li class=""><a href="{{ route('cylindertypes.index') }}"><span>Cylinder Type</span></a></li>
                    @endif
                    {{-- <li class=""><a href="{{ route('cylindercontainertypes.index') }}"><span>Cylinder Container
                        Type</span></a>
            </li> --}}
            @if(Auth::user()->hasPermissionTo('CY_HANDWHEEL_VR'))
            <li class=""><a href="{{ route('cylinderhandwheeltypes.index') }}"><span>Cylinder Handwheel Type</span></a>
            </li>
            @endif
            @if(Auth::user()->hasPermissionTo('CY_VALVE_VR'))
            <li class=""><a href="{{ route('cylindervalvetypes.index') }}"><span>Cylinder Valve Type</span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('CY_MANUFACTURER_VR'))
            <li class=""><a href="{{ route('manufacturers.index') }}"><span>Manufacturer</span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('CY_OWNERSHIP_VR'))
            <li class=""><a href="{{ route('ownerships.index') }}"><span>Ownership</span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('GS_RACK_VR'))
            <li class=""><a href="{{ route('gasracks.index') }}"><span>Gas Rack </span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('GS_RACK_TYPE_VR'))
            <li class=""><a href="{{ route('gasracktypes.index') }}"><span>Gas Rack Type</span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('SLING_VR'))
            <li class=""><a href="{{ route('slings.index') }}"><span>Sling </span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('SLING_TYPE_VR'))
            <li class=""><a href="{{ route('slingtypes.index') }}"><span>Sling Type</span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('SHACKLE_VR'))
            <li class=""><a href="{{ route('shackles.index') }}"><span>Shackle </span></a></li>
            @endif
            @if(Auth::user()->hasPermissionTo('SHACKLE_TYPE_VR'))
            <li class=""><a href="{{ route('shackletypes.index') }}"><span>Shackle Type</span></a></li>
            @endif
            <li class=""><a href="{{ route('drivers.index') }}"><span>Driver</span></a></li>
            {{-- <li class=""><a href="{{ route('manyfor.index') }}"><span>Many For</span></a></li> --}}
        </ul>
        </li>
        @endif
        <li class="treeview">
            <a href="#"><span>Code Replacement</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li class=""><a href="{{ route('codereplacement.stockcode') }}"><span>Stock Code</span></a></li>
                <li class=""><a href="{{ route('codereplacement.debtorcode') }}"><span>Debtor Code</span></a></li>
                <li class=""><a href="{{ route('codereplacement.creditorcode') }}"><span>Creditor Code</span></a></li>
            </ul>
        </li>
        </ul>
        </li>
        <li class="treeview">
            <a href="#"><span>Report Printing</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li class="treeview">
                    <a href="#"><span>Inventory Module</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        {{-- <li class=""><a href="{{ route('reportprinting.transaction') }}"><span>Transaction
                                    Printing</span></a></li> --}}
                        @if(Auth::user()->hasPermissionTo('DEBTOR_LISTING_VR'))
                        <li class=""><a href="{{ route('reportprinting.debtormasterlisting') }}"><span>Debtor Master
                                    Listing</span></a></li>
                        @endif
                        @if(Auth::user()->hasPermissionTo('CREDITOR_LISTING_VR'))
                        <li class=""><a href="{{ route('reportprinting.creditormasterlisting') }}"><span>Creditor Master
                                    Listing</span></a></li>
                        @endif
                        @if(Auth::user()->hasPermissionTo('STOCK_LISTING_VR'))
                        <li class=""><a href="{{ route('reportprinting.stockmasterlisting') }}"><span>Stock Master
                                    Listing</span></a></li>
                        @endif
                        @if(Auth::user()->hasPermissionTo('STOCK_BALANCE_VR'))
                        <li class=""><a href="{{ route('reportprinting.stockbalance') }}"><span>Stock Balance / Take
                                    List</span></a></li>
                        @endif
                        @if(Auth::user()->hasPermissionTo('STOCK_LEDGER_VR'))
                        <li class=""><a href="{{ route('reportprinting.stockledger') }}"><span>Stock Ledger</span></a>
                        </li>
                        @endif
                        @if(Auth::user()->hasPermissionTo('STOCK_VAL_FIFO_VR'))
                        <li class=""><a href="{{ route('reportprinting.stockvalue_fifo') }}"><span>Stock
                                    Value(FIFO)</span></a></li>
                        @endif
                        @if(Auth::user()->hasPermissionTo('STOCK_VAL_ASAT_VR'))
                        <li class=""><a href="{{ route('reportprinting.stockvalue_asat') }}"><span>Stock Value(As
                                    at)</span></a></li>
                        @endif
                        @if(Auth::user()->hasPermissionTo('STOCK_MOVEMENT_VR'))
                        <li class=""><a href="{{ route('reportprinting.stockmovement') }}"><span>Stock
                                    Movement</span></a></li>
                        @endif
                        {{-- @if(config('config.cylinder.cylinder') == "false")
                        <li class="treeview">
                            <a href="#"><span>KPDNHEP</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                @if(Auth::user()->hasPermissionTo('PROCESSING_VR'))
                                <li class=""><a
                                        href="{{ route('reportprinting.KPDNHEP_BELIAN') }}"><span>Belian</span></a>
                                </li>
                                @endif
                                @if(Auth::user()->hasPermissionTo('PEMBEKAL_VR'))
                                <li class=""><a
                                        href="{{ route('reportprinting.KPDNHEP_SUPPLIER') }}"><span>Pembekal/Pemborong</span></a>
                                </li>
                                @endif
                                @if(Auth::user()->hasPermissionTo('PERUNCIT_VR'))
                                <li class=""><a
                                        href="{{ route('reportprinting.KPDNHEP_RETAILER') }}"><span>Peruncit</span></a>
                                </li>
                                @endif
                                @if(Auth::user()->hasPermissionTo('PROCESSING_VR'))
                                <li class=""><a
                                        href="{{ route('reportprinting.KPDNHEP_Process') }}"><span>Processing</span></a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        @endif --}}
                    </ul>
                </li>
                @if(config('config.cylinder.cylinder') == "true")
                <li class="treeview">
                    <a href="#"><span>Cylinder Module</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class=""><a href="{{ route('reportprinting.cylindermasterlisting') }}"><span>Cylinder Master
                                    Listing</span></a></li>
                        <li class=""><a href="{{ route('cylinders.cylinderactivity') }}"><span>Cylinder
                                    Tracking</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.cylindermovementreport') }}"><span>Cylinder
                                    Movement Report</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.cylinderledgerreport') }}"><span>Cylinder Ledger
                                    Report</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.cylinderRentalreport') }}"><span>Cylinder Rental
                                    Statement</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.cylinderLocationreport') }}"><span>Cylinder
                                    Location</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.CylinderSummaryHolding') }}"><span>Holding
                                    Summary</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.CylinderActivity') }}"><span>Cust. Cylinder
                                    Activity</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.CylinderInvalid') }}"><span>Invalid Cylinder
                                    Return</span></a></li>
                        <li class=""><a href="{{ route('reportprinting.RackActivity') }}"><span>Customer Rack
                                    Activity</span></a></li>
                    </ul>
                </li>
                @endif
                <li class="treeview">
                    <a href="#"><span>Purchase & Sales Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="">
                            <a href="{{ route('reportprinting.purchasebybill') }}">
                                <span>Daily & Monthly Purchase by <br>Bill No.</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('reportprinting.purchasebystockcode') }}">
                                <span>Daily & Monthly Purchase by <br>Stock Code</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('reportprinting.salesbybill') }}">
                                <span>Daily & Monthly Sales by <br>Bill No.</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('reportprinting.salesbystockcode') }}">
                                <span>Daily & Monthly Sales by <br>Stock Code</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
      {{--  <li class="treeview">
            <a href="#"><span>Enquiry</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                @if(Auth::user()->hasPermissionTo('STOCK_CUST_ENQ_VR'))
                <li class=""><a href="{{ route('customers.index') }}"><span>Stock Customer History Enquiry</span></a>
                </li>
                @endif
                @if(Auth::user()->hasPermissionTo('STOCK_SUPP_ENQ_VR'))
                <li class=""><a href="{{ route('suppliers.index') }}"><span>Stock Supplier History Enquiry</span></a>
                </li>
                @endif
            </ul>
        </li> --}}
        <li class="treeview">
            <a href="#"><span>Setup</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                @if(Auth::user()->hasPermissionTo('SYSTEM_SETUP_VR'))
                <li class=""><a href="{{ route('systemsetups.create') }}"><span>System Setup</span></a></li>
                @endif
                @if(Auth::user()->hasPermissionTo('DOC_SETUP_VR'))
                <li class=""><a href="{{ route('documentsetups.index') }}"><span>Document Setup</span></a></li>
                @endif
                {{-- @if(Auth::user()->hasPermissionTo('USER_SETUP_VR')) --}}
                <li class=""><a href="{{ route('usersetups.index') }}"><span>User Setup</span></a></li>
                {{-- @endif --}}
                @if(Auth::user()->hasPermissionTo('UG_SETUP_VR'))
                <li class=""><a href="{{ route('usergroups.index') }}"><span>User Group Setup</span></a></li>
                @endif
                {{-- @if(Auth::user()->hasPermissionTo('AP_SETUP_VR'))
                <li class=""><a href="{{ route('accountposting.index') }}"><span>Account Posting</span></a></li>
                @endif  --}}
            </ul>
        </li>
        <!-- Optionally, you can add icons to the links -->

        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>