$.fn.dataTable.moment('DD/MM/YYYY');
var dataTable;

function fill_datatable()
{
    var dataInput = $('#formEnquiry').serialize();
    var rows_selected = [];

    dataTable = $('#enquiry_data').DataTable({
        pageLength: 5,
        scrollX: true,
        ajax:{
            url: "{{ route('accountposting.query') }}",
            data:{ formdata : dataInput } ,
            type: "POST",
            dataFilter: function(response) {
                var json_response = JSON.parse(response);
                if (json_response.data.length) {
                    $("#enquiry_data").css({
                        fontSize: 11
                    });
                    $(".btn-post").prop("disabled", false);
                }
                $(".overlay").hide();
                return response;
            },
            error: function (xhr) {
                console.error(xhr.responseJSON);
            }
        },
        'columnDefs': [
           {
              'targets': 0,
              'checkboxes': {
                 'selectRow': true
              }
           }
        ],
        'select': {
           'style': 'multi'
        },
        'order': [[1, 'asc']]
    });

    return dataTable;
}

$("#formEnquiry").submit(function(event) {

    var btn = $(document.activeElement).val();

    if(btn == "search"){
        event.preventDefault();
        $(".table-result").show();
        $('#enquiry_data').DataTable().destroy();

        dataTable = fill_datatable();

        $('html, body').animate({
            scrollTop: $("#target").offset().top
        }, 2000);
    }else{
        event.preventDefault();
        var form = this;
        var docsNo = [];
        var dataInput = $('#formEnquiry').serialize();
        var isValid = true;

        if($('#batchNo').val() === ""){
            alert("Please insert batch no.")
            return false;
        }

        dataTable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            var data = this.data();
            var node = this.node();

            if($(node).find('input').prop('checked')){
                docsNo.push(data[2])
                if(data[11] == 'Y' && !$('.allowRePosted').prop('checked')){
                    alert("Some posted document detected, please tick Re-Post Posted Data to confirm this process")
                    isValid = false;
                    return false;
                }
            }
        } );

        if(docsNo.length == 0){
            alert("There is no document selected")
            return false;
        }else if(docsNo.length >= 5000){
            alert("The number of document selected reach maximum value(5000), if you want to post more document please do it on the next batch.")
            $('.btn').prop("disabled", true);
            $(".table-result").show();
            $(".search-result").show();
        }else {
            $('.btn').prop("disabled", true);
            $(".table-result").show();
            $(".search-result").show();
        }

        if(isValid){
            $.ajax({
                url: '{{route("accountposting.post")}}',
                type: 'POST',
                data:{ docno : docsNo, formdata : dataInput },
                success: function(data) {
                            if(data == "success"){
                                alert("Selected document(s) posted succesfully ")
                                $('.btn').prop("disabled", false);
                                $(".table-result").hide();
                                $(".search-result").hide();
                                $('#enquiry_data').DataTable().ajax.reload();
                            }else{
                                alert("Posting process failed, please re-try again");
                                $('.btn').prop("disabled", false);
                                $(".table-result").hide();
                                $(".search-result").hide();
                                $('#enquiry_data').DataTable().ajax.reload();
                            }

                        },
                error: function (request, status, error) {
                            alert("Process failed, please refresh page");
                            $('.btn').prop("disabled", false);
                            $(".table-result").hide();
                            $(".search-result").hide();
                            $('#enquiry_data').DataTable().ajax.reload();
                        }
            });
        }else{
            $('.btn').prop("disabled", false);
            $(".table-result").hide();
            $(".search-result").hide();
        }
    }
});

$( ".form-input" ).change(function() {
    if ( $.fn.DataTable.isDataTable('#enquiry_data') ) {
        $('#enquiry_data').DataTable().destroy();
        $('#enquiry_data').DataTable().clear().draw();
        $('.btn-print').prop("disabled", true);
    }
});

$('#enquiry_data th ').click(function() {

    var sortValue = $(this).attr("class");
    $('#orderBy').attr('data-sorting',sortValue);
    $('#orderBy').data('sorting',sortValue);
    $('#orderBy').val($(this).index());

})

