// script for datatable addline
    let leftPad = function(value, length) {
    return ('0'.repeat(length) + value).slice(-length);
    }

    let intTSN = 0
    $('.sn').each(function() { intTSN += 5 })

    let tSN = leftPad(intTSN, 4)    // 0005

    // if ($('.stockcode').val())
        // $('.btn-form-submit').prop('disabled', false)
   // trigger on click new button
    $('#add-new').on('click', function() {

        // alert('yes');
        let template = $('.template-new').clone().removeClass('template-new d-none')
        
        let tSN = leftPad(intTSN, 4)    // 0005
        // disabling the form submit button since the item code still empty
        $('.btn-form-submit').prop('disabled', true)
        
        // start cloning
        template.eq(0).find('.sn').val(tSN)
        template.eq(0).find('.stockcode').select2({theme: 'bootstrap4', width: 'style',})
        // template.eq(0).find('.stockcode').val()
        template.eq(0).find('.descr').val()
        template.eq(0).find('.qty').val()
        template.eq(0).find('.uom').val(null).select2({theme: 'bootstrap4', width: 'style',})
        template.eq(0).find('.rate').val()
        template.eq(0).find('.cucost').val()
        template.eq(0).find('.mkup').val()
        template.eq(0).find('.ucost').val()
        template.eq(0).find('.uprice').val()
        template.eq(0).find('.discount').val()
        template.eq(0).find('.amount').val()
        template.eq(0).find('.reason').select2({theme: 'bootstrap4', width: 'style',})
        $('.table-form-tbody').append(template)

        intTSN += 5

        // hide this button temporarily
        $(this).addClass('d-none')

    })

    $('.table-form-tbody').on('change', '.stockcode', function() {

        if (!$(this).val()) {
            // keep the form submit button disabled and hide the add-new button
            $('.btn-form-submit').prop('disabled', true)
            $("#add-new").addClass('d-none')
        } else {
            // re-enable the form submit button since the item code is now filled
            $('.btn-form-submit').prop('disabled', false)
            $("#add-new").removeClass('d-none')
        }
    })