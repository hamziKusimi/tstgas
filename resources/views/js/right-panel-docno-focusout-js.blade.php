// After user type something on document no, after focus out check for existing data
// if exist go to that page
// if not exist, just refresh with same doc no they type just now
$(function () {
$(".overlay").hide();
    $("#doc_no").focusout(function () {

    $(".overlay").show();
    let route = $(this).data("url");
    let docno = $("#doc_no").val();
    let data = {
        'docno': docno
        }
    $.ajax({
        url: route,
        method: "POST",
        data: data,
            }).done(function (response) {
                console.log(response);
                window.location.replace(response + '?docno=' + docno);
            }).fail(function (response) {
                console.log(response);
        });
    });



});