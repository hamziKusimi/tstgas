
    //check if barcode or serial exist in cylinders records or not
    function filterRecords1(item, route, isAdded, isBarcode) {

        let data = {
            'item': item,
            'isBarcode': isBarcode
        }

        if (item != '') {
            $.ajax({
                url: route,
                method: "GET",
                data: data,
            }).done(function (response) {
                var selectedBarcode = response[0];

                if(selectedBarcode){
                    $('.cyid').val(selectedBarcode.id)
                    $('.cybarcode').val(selectedBarcode.barcode)
                    $('.cyserial').val(selectedBarcode.serial)
                    $('.cycat').val(selectedBarcode.category.code)
                    $('.cyprod').val(selectedBarcode.product.code)
                    $('.cydescr').val(selectedBarcode.descr)
                    $('.cycapacity').val(selectedBarcode.capacity)
                    $('.cypressure').val(selectedBarcode.workingpressure)
    
                    $('.save').trigger('click')              

                }else{
                    
                    alert('Cylinder not exist')
                }

            }).fail(function (response) {
                console.log("Error" + response);
                $('.loading-overlay').hide();
            })
        } else {
            alert("No Input Detected");
            $('.loading-overlay').hide();
        }

    }

$('.add').click(function () {

    let barcode = $('.cybarcode').val()
    let serial = $('.cyserial').val()

    var isAdded = false;
    let route = "{{ route('cylinders.search.filter') }}"
    let route2 = "{{ route('cylinders.check.filter') }}"
    let isBarcode2 = ''
    if (barcode == '') {
        isBarcode2 = false;
        value = serial
    } else {
        isBarcode2 = true;
        value = barcode
    }
    let val = {
        'item': value,
        'isBarcode': isBarcode2
    }

    $.ajax({
        url: route2,
        method: "GET",
        data: val,
    }).done(function (response2) {
        console.log(route2)
        var selectedBarcode2 = response2[0];
        console.log(selectedBarcode2)
        console.log(value)
        if (selectedBarcode2) {

            console.log('dd')
            if(selectedBarcode2.cy_serial == serial){
                alert("Duplicate Serial No");
                // $('.btn-form-submit').prop('disabled', true)
            }else if(selectedBarcode2.cy_barcode == barcode){
                alert("Duplicate Barcode");
                // $('.btn-form-submit').prop('disabled', false)
            }else{
                alert("Input Not Detected");
            }

        } else {
            console.log('ss')
            if (barcode == '') {
                let isBarcode = false;
                filterRecords1(serial, route, isAdded, isBarcode);
            } else {
                let isBarcode = true;
                filterRecords1(barcode, route, isAdded, isBarcode);
            }
        } 

    }).fail(function (response) {

        console.log("Error" + response);
        $('.loading-overlay').hide();
    })



})