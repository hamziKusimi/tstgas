// script for select2
    $(".stockcode2").select2({theme: 'bootstrap4', width: 'style',});
    $(".uom2").select2({theme: 'bootstrap4', width: 'style',});
    $(".reason2").select2({theme: 'bootstrap4', width: 'style',});
    $(".myselect").select2(); 


// creditor fill in

$(function() {
    $('#creditor_id').change(function() {
        let contents = $(this).data('values')
        let creditor = $(this).val()  

        let selectedCreditor = contents.filter(content => {
            return content.id == creditor
        }) [0]

        // console.log(selectedCreditor)
        $('.address').val(selectedCreditor.address)
        $('.name').val(selectedCreditor.name)
        $('.fax').val(selectedCreditor.fax)
        $('.tel').val(selectedCreditor.tel)
        $('.hp').val(selectedCreditor.hp)
    })
});

// debtor fill in

$(function() {
    $('#debtor_id').change(function() {
        let contents = $(this).data('values')
        let debtors = $(this).val()  

        let selectedDebtors = contents.filter(content => {
            return content.id == debtors
        }) [0]

        // console.log(selectedDebtors)
        $('.address').val(selectedDebtors.address)
        $('.name').val(selectedDebtors.name)
        $('.fax').val(selectedDebtors.fax)
        $('.tel').val(selectedDebtors.tel)
        $('.hp').val(selectedDebtors.hp)
    })
});


$(function() {
    $('#add-new').on('click', function() {
        $('.stockcode').change(function() {
        let row = $(this).parent().parent()
            let contents = $(this).data('values')
            let stockcode = $(this).val()  
            let selectedStockcode = contents.filter(content => {
                return content.id == stockcode
            }) [0]
    
            // console.log(selectedStockcode)
            row.find('.descr').val(selectedStockcode.descr)
            row.find('.uom').val(selectedStockcode.uom_id1).select2({theme: 'bootstrap4', width: 'style',})
            row.find('.stockcode_dt').val(selectedStockcode.id)
            
        })
   
    })
    
}); 
$(function() {
    $('.stockcode2').change(function() {
        let row = $(this).parent().parent()
            let contents = $(this).data('values')
            let stockcodedt = $(this).val()  
            let selectedStockcodedt = contents.filter(content => {
                return content.id == stockcodedt
            }) [0]
    
            row.find('.descr').val(selectedStockcodedt.descr)
            row.find('.uom').val(selectedStockcodedt.uom_id1).select2({theme: 'bootstrap4', width: 'style',})
            row.find('.stockcode_dt').val(selectedStockcodedt.id)
            
        })
   
   
}); 

//initiate float value
    
    $('#dataTable').on('change', '.qty', function() {
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    }); 
    
    $('#dataTable').on('change', '.rate', function() {
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    });

    $('#dataTable').on('change', '.cucost', function() {
        if(isNaN(this.value = parseFloat(this.value).toFixed(4))){
            this.value = parseFloat(0).toFixed(4);
        }else{
            this.value = parseFloat(this.value).toFixed(4);
        }
    }); 

    $('#dataTable').on('change', '.mkup', function() {
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    }); 

    $('#dataTable').on('change', '.ucost', function() {
        if(isNaN(this.value = parseFloat(this.value).toFixed(4))){
            this.value = parseFloat(0).toFixed(4);
        }else{
            this.value = parseFloat(this.value).toFixed(4);
        }
    }); 

    $('#dataTable').on('change', '.amount', function() {
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    }); 
    
    $('#dataTable').on('change', '.uprice', function() {
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    }); 

//calculate table
    
    $('#dataTable').on('change', '.calculate-field', function() {
        let row = $(this).parent().parent()
        let amount = row.find('.amount').val()
        let ucost = row.find('.ucost').val().replace(',','')
        let qty = row.find('.qty').val()

        let price = (ucost * qty)

        row.find('.amount').val(price)

        let discount = row.find('.discount')
        let theDiscount = (discount.val().indexOf('%') > -1) ? discount.val().replace('%', '') : discount.val()
        // let taxRate = row.find('.tax_rate').val()

        // calculate discount
        let discountAmount = 0.00
        if (discount.val().indexOf('%') > -1) {
            discountAmount = ucost * qty * theDiscount / 100
            row.find('.discount').val(discountAmount)
        } else {
            discountAmount = row.find('.discount').val()
        }

        let price2 = (ucost * qty) - discountAmount
        row.find('.amount').val(price2)
        // row.find('.amount_dt').val(price)

    })
    
    $('#dataTable').on('change', '.calculate-field2', function() {
        let row = $(this).parent().parent()
        let amount = row.find('.amount').val()
        let uprice = row.find('.uprice').val().replace(',','')
        let qty = row.find('.qty').val()

        let price = (uprice * qty)

        row.find('.amount').val(price)
        
        let discount = row.find('.discount')
        let theDiscount = (discount.val().indexOf('%') > -1) ? discount.val().replace('%', '') : discount.val()
        // let taxRate = row.find('.tax_rate').val()

        // calculate discount
        let discountAmount = 0.00
        if (discount.val().indexOf('%') > -1) {
            discountAmount = uprice * qty * theDiscount / 100
            row.find('.discount').val(discountAmount)
        } else {
            discountAmount = row.find('.discount').val()
        }

        let price2 = (uprice * qty) - discountAmount
        row.find('.amount').val(price2)
        // row.find('.amount_dt').val(price)

    })
    
   
    //Clear Input Tables
    // function ClearFields(){
    //         $(".sn").val("");
    //         $(".stockcode").val("");
    //         $(".descr").val("");
    //         $(".qty").val("");
    //         $(".uom").val("");
    //         $(".rate").val("");
    //         $(".cucost").val("");
    //         $(".mkup").val("");
    //         $(".ucost").val("");
    //         $(".discount").val("");
    //         $(".amount").val("");
     //   $(".reason").val("");
      //  $(".gr").val("");
     // $(".uprice").val("");
     // $(".ty").val("");
    // }

