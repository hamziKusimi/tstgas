$(document).ready(function () {
    
    $('.unitcost').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(4))){
            this.value = parseFloat(0).toFixed(4);
        }else{
            this.value = parseFloat(this.value).toFixed(4);
        }
    })   
    $('#unitcost1').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#unitcost2').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#unitcost3').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#unitcost4').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    }) 
    $('#unitcost5').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#unitcost6').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#unitcost7').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#minstkqty').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#maxstkqty').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#volume').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(4))){
            this.value = parseFloat(0).toFixed(4);
        }else{
            this.value = parseFloat(this.value).toFixed(4);
        }
    })
    $('#stkbal').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#prevcost').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(4))){
            this.value = parseFloat(0).toFixed(4);
        }else{
            this.value = parseFloat(this.value).toFixed(4);
        }
    })
    $('#avecost').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(4))){
            this.value = parseFloat(0).toFixed(4);
        }else{
            this.value = parseFloat(this.value).toFixed(4);
        }
    })
    $('#minprice').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#wsprice').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })

    $('#unitprice').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#retprice').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#price1').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#price2').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })
    $('#price3').on('change', function(){
        if(isNaN(this.value = parseFloat(this.value).toFixed(2))){
            this.value = parseFloat(0).toFixed(2);
        }else{
            this.value = parseFloat(this.value).toFixed(2);
        }
    })

    $(document).on('click', '#testButton', function () {
    //     alert('clicked!');
        var unitcost = document.getElementById('unitcost').value;
        var unitcost1 = document.getElementById('unitcost1').value;
        var unitcost2 = document.getElementById('unitcost2').value;
        var unitcost3 = document.getElementById('unitcost3').value;
        var unitcost4 = document.getElementById('unitcost4').value;
        var unitcost5 = document.getElementById('unitcost5').value;
        var unitcost6 = document.getElementById('unitcost6').value;
        var unitcost7 = document.getElementById('unitcost7').value;
            
        document.getElementById('minprice').value = ((parseFloat(unitcost) * parseFloat(unitcost1)) / 100).toFixed(2); 
        document.getElementById('wsprice').value = ((parseFloat(unitcost) * parseFloat(unitcost2)) / 100).toFixed(2);       
        document.getElementById('unitprice').value = ((parseFloat(unitcost) * parseFloat(unitcost3)) / 100).toFixed(2);
        document.getElementById('retprice').value = ((parseFloat(unitcost) * parseFloat(unitcost4)) / 100).toFixed(2);
        document.getElementById('price1').value = ((parseFloat(unitcost) * parseFloat(unitcost5)) / 100).toFixed(2);       
        document.getElementById('price2').value = ((parseFloat(unitcost) * parseFloat(unitcost6)) / 100).toFixed(2);         
        document.getElementById('price3').value = ((parseFloat(unitcost) * parseFloat(unitcost7)) / 100).toFixed(2);     
    })

})
