
$('#qtno-modal').on('shown.bs.modal	', function() {
    let getArrayqtno = []
    if ($('#qtno').val())
        getArrayqtno = $('#qtno').val().split(',')
    
    let modal = $('#qtno-modal')
    let getRoute2 = modal.data('get-route2')
    let getRoute = modal.data('get-route')
    let leftSelect = modal.find('.left-select')
    let rightSelect = modal.find('.right-select')

    modal.modal()
    modal.on('hidden.bs.modal', function (e) { 
        modal.off('dblclick', '.left-option')
     }) 
    let getqtnoRoute = getRoute.replace('ACC_CODE', $('#debtor-select').val())

    let leftqtno = modal.find('.left-select')
    leftqtno.empty()

     $.ajax({
            'url': getqtnoRoute,
            'method': 'GET'
        }).done(function(response) {
            
            response.forEach(function (item) {         
                // if (still have po items not used, then append, otherwise don't append) {
                // po items dalam existing data
                    console.log(item.dt)
                if (item.dt.length > 0) {
                    let option = new Option(item.docno, item.docno, false, false);
                    leftSelect.append(option)
                    console.log(item)
                }
            })

            leftSelect.find('option').addClass('left-option')
            console.log($('.qtno').val())
            modal.on('dblclick', '.left-option', function() {
                let originalqtno = $(this).val()
                let qtno = $(this).val().replace('/', '')
                
                
                if ($.inArray($(this).val(), getArrayqtno === -1) && ($('.qtno').val() != $(this).val()))
                    getArrayqtno.qush($(this).val())
                console.log(getArrayqtno)
                $('.qtno').val(getArrayqtno)

                let moveOverOption = $(this).clone().addClass('moved-option').removeClass('left-option')
                
                $(this).remove()                
                let right = rightSelect.append(moveOverOption)

                let selectedItem = response.filter(item => {
                    return item.docno == $(this).val()
                })[0]
                
                selectedItem.dt.forEach(function(dt) {
                        // if dt is already in existing, dont append 
                        // Template in Modal
                        let qtemplate = modal.find('.modal-template-new').clone().removeClass('modal-template-new d-none').addClass(qtno) 
                        let uomSelect = qtemplate.find('.qunit_measure') 
                        let stockcode = $(".item_code").data('contents').filter(content => content.code == dt.item_code)[0]
                        
                        //Template in DT Table
                        let template = $('.template-new').clone().removeClass('template-new d-none').addClass(qtno)
                        let uomSelect2 = template.find('.unit_measure')
                        let rowDetailID = 'row-detail-' + rowID
                        let tSN = leftPad(intTSN, 4)    // 0005
                        let itemCodeSelect = template.find('.item_code')
                        
                        let qtnoClassInOutsideTable = dt.item_code + '-' + tSN
                        template.addClass(qtnoClassInOutsideTable);

                        uomSelect.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            qtemplate.find('.qunit_measure').append(uoms)
                        })
                        uomSelect.on('change', function() {
                            if (uomSelect.val() == stockcode.uom_id1) {
                                qtemplate.find('.qrate').val(stockcode.volume)
                            } else if (uomSelect.val() == stockcode.uom_id2) {
                                qtemplate.find('.qrate').val(stockcode.uomrate2)
                            } else if (uomSelect.val() == stockcode.uom_id3) {
                                qtemplate.find('.qrate').val(stockcode.uomrate3)
                            }
                        })
                        uomSelect.trigger('change')
                                    
                        qtemplate.find('.qodocno').val(dt.doc_no)
                        qtemplate.find('.qitem_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        qtemplate.find('.qitem_code').val(dt.item_code).trigger('change')
                        qtemplate.find('.qsubject').val(dt.subject)
                        qtemplate.find('.qsequence_no').val(dt.sequence_no)
                        qtemplate.find('.qquantity').val(dt.qty)
                        qtemplate.find('.qsupp-price').val(dt.cucost)
                        qtemplate.find('.qamount').val(dt.amount)


                        modal.find('#modal-table-form-tbody').append(qtemplate)
                        rowID++
                        
                        // adding newly added item_code to select2
                        itemCodeSelect.select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true, tags: true })
                        let contents = itemCodeSelect.data('contents')
                        contents.forEach(function (item) {
                            let code = item.code
                            let name = item.descr
                            let newItem = new Option(name, code, false, false);
                            itemCodeSelect.append(newItem)
                        })


                        uomSelect2.select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true, tags: true }).empty()
                        stockcode.uoms.forEach(function (uom) {
                            let uoms = new Option(uom.code, uom.id, false, false);
                            template.find('.unit_measure').append(uoms)
                            if(dt.uom == uom.id)
                                template.find('.unit_measuredt').val(uom.code)
                        })
                        uomSelect2.on('change', function() {
                            if (uomSelect2.val() == stockcode.uom_id1) {
                                template.find('.rate').val(stockcode.volume)
                            } else if (uomSelect2.val() == stockcode.uom_id2) {
                                template.find('.rate').val(stockcode.uomrate2)
                            } else if (uomSelect2.val() == stockcode.uom_id3) {
                                template.find('.rate').val(stockcode.uomrate3)
                            }

                            stockcode.uoms.forEach(function (uom) {
                                if(template.find('.unit_measure').val() == uom.id)
                                    template.find('.unit_measuredt').val(uom.code)
                            })
                        })
                        
                        uomSelect2.trigger('change')
                        template.find('.item_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.item_code').val(dt.item_code).trigger('change')
                        template.find('.subject').val(dt.subject)
                        template.find('.sequence_no').val(tSN)
                        // template.find('.unit_measure').select2({ templateSelection: formatStateInTableText, dropdownAutoWidth: true })
                        // template.find('.unit_measure').val(dt.uom)
                        // template.find('.rate').val(dt.rate)  
                        template.find('.quantity').val(dt.qty)
                        qtemplate.find('.qquantity').on('change', function(){
                            let qty = $(this).val()
                            template.find('.quantity').val(qty).trigger('change')
                        })

                        template.find('.cunit_cost').val(dt.cucost)
                        qtemplate.find('.qsupp-price').on('change', function(){
                            let suppprice = $(this).val()
                        template.find('.cunit_cost').val(suppprice).trigger('change')
                         })
                        // template.find('.cunit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.reference_no').val(originalqtno)
                        template.find('.markup').val(dt.mkup)
                        template.find('.unit_cost').val(dt.ucost)
                        template.find('.unit_cost').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.discount').val(dt.discount)
                        template.find('.discount').inputmask({ rightAlign: true })
                        template.find('.amount').val(dt.amount)
                        template.find('.amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.tax_code').select2({ templateSelection: formatStateInTable, dropdownAutoWidth: true })
                        template.find('.tax_rate').val(dt.tax_rate)
                        template.find('.tax_amount').val(dt.tax_amount)
                        template.find('.tax_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').inputmask('currency', { prefix: '', rightAlign: true })
                        template.find('.taxed_amount').val(dt.taxed_amount)
                        template.find('.toggle-details').attr('data-target', '#' + rowDetailID)
                        template.attr('id', rowDetailID)
                        $('#table-form-tbody').append(template)
                        intTSN += 5
                        
                        //trigger calculation on subtotal
                        $('.cunit_cost').trigger('change')
                        
                        // delete from po also in table
                        qtemplate.find('.remove-row').on('click', function(){
                            $('#table-form-tbody').find('.'+qtnoClassInOutsideTable).remove()
                        })
                    // set on event handler
                        onHandler(rowDetailID, template.eq(0), template.eq(1))
                })
                
            })

            modal.on('dblclick', '.moved-option', function() {   
                let val = $(this).val()
                val = val.replace('/', '')
                modal.find('.' + val).remove()  
                $('tbody').find('.' + val).remove()       

                getArrayqtno = arrayRemove(getArrayqtno, $(this).val())  
                $('.qtno').val(getArrayqtno)
                
                $(this).remove()
                leftSelect.append($(this).removeClass('moved-option').addClass('left-option'))  
            })
            
            
        })
    

})