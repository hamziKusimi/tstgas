
$(document).ready(function () {
    function formatStateInTable(state) { return state.id }
    function formatStateInTableText(state) { return state.text }

    //select2 for category
    let categorySelect = $('.box-body').find('.category')
    let categorySelect2 = $('.box-body').find('.categorycoy')
    categorySelect.select2({ dropdownAutoWidth: true, tags: true })
    let cat = categorySelect.data('cat')
    cat.forEach(function (item) {
        let code = item.id
        let name = `${item.code} ${item.descr}`
        let newItem = new Option(name, code, false, false);
        categorySelect.append(name)

        // categorySelect.on('change', function(){
        //     let newItem2 = new Option(name, item.id, false, false);  
        //     categorySelect2.append(newItem2)
        //     console.log(categorySelect2.val())
        // }) 
    })
    categorySelect.trigger('change')

    //select2 for product
    // let productSelect = $('.box-body').find('.product')
    // productSelect.select2({ dropdownAutoWidth: true, tags: true })
    // let prod = productSelect.data('prod')
    // prod.forEach(function (item2) {
    //     let code2 = item2.code
    //     let name2 = `${code2} ${item2.descr}`
    //     let newItem2 = new Option(name2, code2, false, false);
    //     productSelect.append(newItem2)
    // })
    // productSelect.trigger('change')

    //select2 for brand
    // let brandSelect = $('.box-body').find('.brand')
    // brandSelect.select2({ dropdownAutoWidth: true, tags: true })
    // let brand = brandSelect.data('brand')
    // brand.forEach(function (item3) {
    //     let code3 = item3.code
    //     let name3 = `${code3} ${item3.descr}`
    //     let newItem3 = new Option(name3, code3, false, false);
    //     brandSelect.append(newItem3)
    // })
    // brandSelect.trigger('change')
})