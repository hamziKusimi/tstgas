
    //check if barcode or serial exist in sglinders records or not
    function filterRecords2(item, route, isAdded, isBarcode) {

        let data = {
            'item': item,
            'isBarcode': isBarcode
        }
        if (item != '') {
            $.ajax({
                url: route,
                method: "GET",
                data: data,
            }).done(function (response) {
                var selectedBarcode = response[0];

                if(selectedBarcode){
                    $('.sgbarcode').val(selectedBarcode.barcode)
                    $('.sgserial').val(selectedBarcode.serial)
    
                    $('.save2').trigger('click')              

                }else{
                    
                    alert('Sling Not Exist')
                }

            }).fail(function (response) {
                console.log("Error" + response);
                $('.loading-overlay').hide();
            })
        } else {
            alert("No Input Detected");
            $('.loading-overlay').hide();
        }

    }

$('.add2').click(function () {

    console.log('masuk sini')

    let barcode = $('.sgbarcode').val()
    let serial = $('.sgserial').val()

    var isAdded = false;
    let route = "{{ route('slings.search.filter') }}"
    let route2 = "{{ route('slings.check.filter') }}"
    let isBarcode2 = ''
    if (barcode == '') {
        isBarcode2 = false;
        value = serial
    } else {
        isBarcode2 = true;
        value = barcode
    }
    let val = {
        'item': value,
        'isBarcode': isBarcode2
    }

    $.ajax({
        url: route2,
        method: "GET",
        data: val,
    }).done(function (response2) {
        console.log(route2)
        var selectedBarcode2 = response2[0];
        console.log(selectedBarcode2)
        console.log(value)
        if (selectedBarcode2) {

            console.log('dd')
            if(selectedBarcode2.sg_serial == serial){
                alert("Duplicate Serial No");
                // $('.btn-form-submit').prop('disabled', true)
            }else if(selectedBarcode2.sg_barcode == barcode){
                alert("Duplicate Barcode");
                // $('.btn-form-submit').prop('disabled', false)
            }else{
                alert("Input Not Detected");
            }

        } else {
            console.log('ss')
            if (barcode == '') {
                let isBarcode = false;
                filterRecords2(serial, route, isAdded, isBarcode);
            } else {
                let isBarcode = true;
                filterRecords2(barcode, route, isAdded, isBarcode);
            }
        } 

    }).fail(function (response) {

        console.log("Error" + response);
        $('.loading-overlay').hide();
    })


})