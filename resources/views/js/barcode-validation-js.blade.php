      $(function() {
          let originalBarcode = $('.barcodecoy').val()
        $('.barcodecoy').on('blur', function() {
            let contents = $(this).data('values')
            let barcode = $(this).val()  
            console.log(barcode)
            console.log(contents)

            let selectedBarcode = contents.filter(content => {
                return (content == barcode && content != originalBarcode)
            }) [0]

            if (selectedBarcode) {
                if(selectedBarcode.barcode == barcode){
                    $('.alert-exist').text('Barcode Exist! Create a New Barcode')
                    $(".red-alert").removeClass('d-none')
                    $('.btn-form-submit').prop('disabled', true)
                }else{
                    
                    $(".red-alert").addClass('d-none')
                    $('.btn-form-submit').prop('disabled', false)
                }
            } else {
                    $(".red-alert").addClass('d-none')
                    $('.btn-form-submit').prop('disabled', false)
            }
            
        })
        
    });