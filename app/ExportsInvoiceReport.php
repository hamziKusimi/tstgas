<?php
namespace App\ExportsInvoiceReport;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\WithEvents;
// use Maatwebsite\Excel\Events\AfterSheet;

class ExportInvoiceReport implements FromView
{
    public $data;
    public $type;

    public function __construct($data, $type)
    {
        $this->data = $data;
        $this->type = $type;
    }

    public function view(): View
    {
        if($this->type == "listing"){
            return view('dailypro.invoices.listing-excel', ['data' => $this->data]);
        }else{
            return view('dailypro.invoices.summary-excel', ['data' => $this->data]);
        }
    }

    // public function registerEvents(): array
    // {

    //     return [
    //         AfterSheet::class    => function(AfterSheet $event) {
    //             $styleArray = [
    //                 'borders' => [
    //                     'outline' => [
    //                         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                         'color' => ['argb' => '#000000'],
    //                     ],
    //                 ],
    //             ];

    //             $event->sheet->getDelegate()->mergeCells('A1:H1');
    //             $event->sheet->getStyle('A1:H1')->ApplyFromArray($styleArray);
    //         },
    //     ];
    // }

}