<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Model\SystemSetup;
use DB;
use Auth;
use App\Model\CustomObject\DatabaseConnection;
use Carbon\Carbon;

trait AccountPosting {

    public function postAccount(Request $request)
    {
        $connection = DatabaseConnection::setConnection();
        $docNoArray = $request->input('docno');
        $inputs = $request->input('formdata');

        $inValue=  array();
        parse_str($inputs, $inValue);

        if(isset($inValue['itemType'])){
            $postGRData =  $inValue['itemType'] != "gr" ? true : $this::postGRData($docNoArray, $connection, $inValue['batchNo']);
            $postCBData =  $inValue['itemType'] != "cb" ? true : $this::postCBData($docNoArray, $connection, $inValue['batchNo']);
            $postINVData = $inValue['itemType'] != "inv" ? true : $this::postINVData($docNoArray, $connection, $inValue['batchNo']);
            // $postPRData = $inValue['itemType'] != "pr" ? true : $this::postPRData($docNoArray, $connection, $inValue['batchNo']);
            // $postSRData = $inValue['itemType'] != "sr" ? true : $this::postSRData($docNoArray, $connection, $inValue['batchNo']);
        } else{
            $postGRData = $this::postGRData($docNoArray, $connection, $inValue['batchNo']);
            $postCBData = $this::postCBData($docNoArray, $connection, $inValue['batchNo']);
            $postINVData = $this::postINVData($docNoArray, $connection, $inValue['batchNo']);
            // $postPRData = $this::postPRData($docNoArray, $connection, $inValue['batchNo']);
            // $postSRData = $this::postSRData($docNoArray, $connection, $inValue['batchNo']);
        }

        if($postGRData && $postCBData && $postINVData){
            return "success";
        }else{
            return "failed";
        }
    }

    public function postGRData($docNoArray, $connection, $batchNo)
    {
        try {
            $docNo = implode("', '", $docNoArray);
            $dataMt = DB::select(DB::raw("
                SELECT *
                FROM goods
                WHERE deleted_at IS NULL AND docno IN ('$docNo')
            "), []);

            if(count($dataMt)>0){
                $dataDt = DB::select(DB::raw("
                    SELECT a.*, c.code AS 'catCode', c.dr_creditpurcase_acc AS 'creditPurch', c.dr_cashpurchase_acc AS 'cashPurch',
                            d.ptype, a.created_by AS 'keyby',
                            a.created_at AS 'keyat', a.updated_by AS 'modiby',
                            a.updated_at AS 'modiat'
                    FROM gooddts a
                    INNER JOIN stockcodes b ON b.code = a.item_code
                    INNER JOIN categories c ON c.id = b.cat_id
                    INNER JOIN goods d ON d.docno = a.doc_no
                    WHERE a.deleted_at IS NULL AND
                    b.deleted_at IS NULL AND c.deleted_at IS NULL AND d.deleted_at IS NULL
                    AND doc_no IN ('$docNo')
                "), []);

                DB::statement("
                    UPDATE goods
                    SET posted = 'Y'
                    WHERE deleted_at IS NULL AND docno IN ('$docNo')
                ");
            }

            $grMt=[];
            foreach($dataMt as $gr) {
                $connection->table('acc_pinvmt')->where('m_docno', '=', $gr->docno)->delete();
                $connection->table('acc_pinvdt')->where('t_docno', '=', $gr->docno)->delete();
                $grMt[] = [
                    'm_docno' => $gr->docno == null ? '' :  $gr->docno,
                    'm_date' => $gr->date == null ? '' : $gr->date,
                    'm_batch' => $batchNo == null ? '' :  $batchNo,
                    'm_tamt' => $gr->amount == null ? '' : $gr->amount,
                    'm_ttaxamt' => $gr->tax_amount == null ? '' : $gr->tax_amount,
                    'm_tamtt' => $gr->taxed_amount == null ? '' : $gr->taxed_amount,
                    'm_crac' => $gr->account_code == null ? '' : $gr->account_code,
                    'm_name' => $gr->name == null ? '' : $gr->name,
                    'm_addr1' => $gr->addr1 == null ? '' : $gr->addr1,
                    'm_addr2' => $gr->addr2 == null ? '' :  $gr->addr2,
                    'm_addr3' => $gr->addr3 == null ? '' : $gr->addr3,
                    'm_addr4' => $gr->addr4 == null ? '' : $gr->addr4,
                    'm_tel' => $gr->tel_no == null ? '' : $gr->tel_no,
                    'm_fax' => $gr->fax_no == null ? '' : $gr->fax_no,
                    'm_header' => $gr->header == null ? '' : $gr->header,
                    'm_footer' => $gr->footer == null ? '' : $gr->footer,
                    'm_spinvno' => $gr->suppinv == null ? '' : $gr->suppinv,
                    'm_spinvdate' => $gr->suppinvdate == null ? '' :  $gr->suppinvdate,
                    'm_ref1' => $gr->ref == null ? '' : $gr->ref,
                    'm_post' => $gr->posted == null ? '' : $gr->posted,
                    'm_keyuser' => $gr->created_by == null ? '' :  $gr->created_by,
                    'm_modiuser' => $gr->updated_by == null ? '' : $gr->updated_by,
                    'm_keydate' =>  $gr->created_at  == null ? '' :  $gr->created_at ,
                    'm_modidate' => $gr->updated_at == null ? '' : $gr->updated_at,
                ];

                $grData[]=[
                    'strdocno'=> $gr->docno == null?'' :  $gr->docno,
                    'stracode'=>$gr->account_code == null?'' : $gr->account_code
                ];
            }

            foreach (array_chunk($grMt,1000) as $gr)
            {
                $connection->table('acc_pinvmt')->insert($gr);
            }

            $isCategorySet = SystemSetup::select(
                'dr_cashpurchase_acc',
                'dr_creditpurchase_acc',
                'set_category1',
                'set_category2')->first();
            $res = [];
            $prevDoc = "";
            $prevPtype = "";
            $prevCatCode = "";
            $prevCreditPurch = "";
            $prevCashPurch = "";
            $tSeq = "005";
            $isNewDoc = false;
            $amt = 0;
            $amtTaxed = 0;
            $count = 1;

            if(isset($dataDt)){
                $grDt=[];
                foreach($dataDt as $gr) {
                        if($prevDoc != $gr->doc_no || count($dataDt) == $count){

                            if($prevPtype == 'Cash'){
                                if($isCategorySet->set_category1 == 'true'){
                                    if($prevDoc == $gr->doc_no && $prevCatCode == $gr->catCode){
                                        $grDt[] = [
                                            't_docno' =>  $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' =>  $prevCashPurch,
                                            't_subject' => "$prevCatCode CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt + $gr->amount,
                                            't_fullamt' => $amt + $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed + $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }elseif($prevDoc == $gr->doc_no){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' =>  $prevCashPurch,
                                            't_subject' => "$prevCatCode CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                        $grDt[] = [
                                            't_docno' =>  $gr->doc_no,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $gr->cashPurch,
                                            't_subject' => "$gr->catCode CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$gr->amount,
                                            't_fullamt' => $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }elseif(count($dataDt) == $count){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $prevCashPurch,
                                            't_subject' => "$prevCatCode CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                        $grDt[] = [
                                            't_docno' =>  $gr->doc_no,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' =>  $gr->ptype == 'Cash' ? $gr->cashPurch :  $gr->creditPurch,
                                            't_subject' => $gr->ptype == 'Cash' ? "$gr->catCode CASH PURCHASES" : "$gr->catCode CREDIT PURCHASES" ,
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$gr->amount,
                                            't_fullamt' => $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }else{
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $prevCashPurch,
                                            't_subject' => "$prevCatCode CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                    }

                                }else{
                                    if($prevDoc == $gr->doc_no){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $isCategorySet->dr_cashpurchase_acc,
                                            't_subject' => "CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>   $amt + $gr->amount ,
                                            't_fullamt' =>   $amt + $gr->amount ,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' =>  $amtTaxed + $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }elseif(count($dataDt) == $count){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $isCategorySet->dr_cashpurchase_acc,
                                            't_subject' => "CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>  $amt,
                                            't_fullamt' =>  $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                        $grDt[] = [
                                            't_docno' => $gr->doc_no,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $gr->ptype == 'Cash'? $isCategorySet->dr_cashpurchase_acc : $isCategorySet->dr_creditpurchase_acc,
                                            't_subject' => $gr->ptype == 'Cash'?"CASH PURCHASES":"CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' => $gr->amount ,
                                            't_fullamt' => $gr->amount ,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];

                                    }else{
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $isCategorySet->dr_cashpurchase_acc,
                                            't_subject' => "CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>  $amt,
                                            't_fullamt' =>  $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                    }
                                }

                            }elseif($prevPtype == 'Credit'){
                                if($isCategorySet->set_category2 == 'true'){
                                    if($prevDoc == $gr->doc_no && $prevCatCode == $gr->catCode){
                                        $grDt[] = [
                                            't_docno' =>  $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' =>  $prevCreditPurch,
                                            't_subject' => "$prevCatCode CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt + $gr->amount,
                                            't_fullamt' => $amt + $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed + $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }elseif($prevDoc == $gr->doc_no){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' =>  $prevCreditPurch,
                                            't_subject' => "$prevCatCode CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                        $grDt[] = [
                                            't_docno' =>  $gr->doc_no,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $gr->creditPurch,
                                            't_subject' => "$gr->catCode CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$gr->amount,
                                            't_fullamt' => $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }elseif(count($dataDt) == $count){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $prevCreditPurch,
                                            't_subject' => "$prevCatCode CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                        $grDt[] = [
                                            't_docno' =>  $gr->doc_no,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' =>  $gr->ptype == 'Cash' ? $gr->cashPurch : $gr->creditPurch,
                                            't_subject' => $gr->ptype == 'Cash' ? "$gr->catCode CASH PURCHASES" : "$gr->catCode CREDIT PURCHASES" ,
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$gr->amount,
                                            't_fullamt' => $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }else{
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $prevCreditPurch,
                                            't_subject' => "$prevCatCode CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                    }

                                }else{
                                    if($prevDoc == $gr->doc_no){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $isCategorySet->dr_creditpurchase_acc,
                                            't_subject' => "CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>   $amt + $gr->amount ,
                                            't_fullamt' =>   $amt + $gr->amount ,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' =>  $amtTaxed + $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }elseif(count($dataDt) == $count){
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $isCategorySet->dr_creditpurchase_acc,
                                            't_subject' => "CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>  $amt,
                                            't_fullamt' =>  $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                        $grDt[] = [
                                            't_docno' => $gr->doc_no,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $gr->ptype == 'Cash'? $isCategorySet->dr_cashpurchase_acc : $isCategorySet->dr_creditpurchase_acc,
                                            't_subject' => $gr->ptype == 'Cash'?"CASH PURCHASES":"CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' => $gr->amount ,
                                            't_fullamt' => $gr->amount ,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];

                                    }else{
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $isCategorySet->dr_creditpurchase_acc,
                                            't_subject' => "CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>  $amt,
                                            't_fullamt' =>  $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = "005";
                                    }
                                }
                            }else{
                                if(count($dataDt) == $count){
                                    if($gr->ptype == 'Cash' && $isCategorySet->set_category1 == 'true'){
                                        $grDt[] = [
                                            't_docno' =>  $gr->doc_no,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $gr->cashPurch,
                                            't_subject' => "$gr->catCode CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' => $gr->amount,
                                            't_fullamt' =>  $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }elseif($gr->ptype == 'Credit' && $isCategorySet->set_category2 == 'true'){
                                        $grDt[] = [
                                            't_docno' =>  $gr->doc_no,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $gr->creditPurch,
                                            't_subject' => "$gr->catCode CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' => $gr->amount,
                                            't_fullamt' =>  $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }else{
                                        $grDt[] = [
                                            't_docno' =>  $gr->doc_no,
                                            't_sn' =>  $tSeq,
                                            't_itemno' => $tSeq,
                                            't_drac' => $gr->ptype == 'Cash'? $isCategorySet->dr_cashpurchase_acc : $isCategorySet->dr_creditpurchase_acc,
                                            't_subject' => $gr->ptype == 'Cash'?"CASH PURCHASES":"CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' => $gr->amount,
                                            't_fullamt' =>  $gr->amount,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $gr->amount,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    }
                                }
                            }

                            $amt = 0;
                            $amtTaxed = 0;
                            $amt = $amt + $gr->amount;
                            $amtTaxed =  $amtTaxed  + $gr->amount;
                        }else{
                            if($gr->ptype == 'Cash'){
                                if($isCategorySet->set_category1 == 'true'){
                                    if($prevCatCode == $gr->catCode){
                                        $amt = $amt + $gr->amount;
                                        $amtTaxed =  $amtTaxed  + $gr->amount;
                                    }else{
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => null,
                                            't_drac' =>  $prevCashPurch,
                                            't_subject' => "$prevCatCode CASH PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );

                                        $amt = 0;
                                        $amtTaxed = 0;
                                        $amt = $amt + $gr->amount;
                                        $amtTaxed =  $amtTaxed  + $gr->amount;
                                    }

                                }else{
                                    $amt = $amt + $gr->amount;
                                    $amtTaxed =  $amtTaxed  + $gr->amount;
                                }
                            }else{
                                if($isCategorySet->set_category2 == 'true'){
                                    if($prevCatCode == $gr->catCode){
                                        $amt = $amt + $gr->amount;
                                        $amtTaxed =  $amtTaxed  + $gr->amount;
                                    }else{
                                        $grDt[] = [
                                            't_docno' => $prevDoc,
                                            't_sn' => $tSeq,
                                            't_itemno' => null,
                                            't_drac' =>  $prevCreditPurch,
                                            't_subject' => "$prevCatCode CREDIT PURCHASES",
                                            't_details' => '',
                                            't_qty' => 0,
                                            't_uom' =>'',
                                            't_idiscamt' => null,
                                            't_amt' =>$amt,
                                            't_fullamt' => $amt,
                                            't_taxcode' => '',
                                            't_taxrate' => 0,
                                            't_taxamt' => 0,
                                            't_amtt' => $amtTaxed,
                                            't_batchno' => $batchNo,
                                            't_keyuser' => Auth::user()->usercode,
                                            't_modiuser' => Auth::user()->usercode,
                                            't_keydate' => Carbon::now(),
                                            't_modidate' => Carbon::now(),
                                        ];
                                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );

                                        $amt = 0;
                                        $amtTaxed = 0;
                                        $amt = $amt + $gr->amount;
                                        $amtTaxed =  $amtTaxed  + $gr->amount;
                                    }

                                }else{
                                    $amt = $amt + $gr->amount;
                                    $amtTaxed =  $amtTaxed  + $gr->amount;
                                }
                            }
                        }

                    $grDt[] = [
                        't_docno' => $gr->doc_no == ''?null :  $gr->doc_no,
                        't_sn' => $tSeq,
                        't_itemno' => $tSeq, //$gr->item_code == ''?null : $gr->item_code,
                        't_drac' => null,
                        't_subject' => $gr->subject == ''?null : $gr->subject,
                        't_details' => $gr->details == ''?null : $gr->details,
                        't_qty' => $gr->qty == ''?null : $gr->qty,
                        't_uom' => $gr->uom == ''?null : $gr->uom,
                        't_idiscamt' => $gr->discount == ''?null : $gr->discount,
                        't_amt' => $gr->amount == ''?null : $gr->amount,
                        't_fullamt' => $gr->amount == ''?null :  $gr->amount,
                        't_taxcode' => $gr->tax_code == ''?null : $gr->tax_code,
                        't_taxrate' => $gr->tax_rate == ''?null : $gr->tax_rate,
                        't_taxamt' => $gr->tax_amount == ''?null : $gr->tax_amount,
                        't_amtt' => $gr->amount == ''?null : $gr->amount,
                        't_batchno' => $batchNo,
                        't_keyuser' => $gr->keyby == ''?null :  $gr->keyby,
                        't_modiuser' => $gr->modiby == ''?null : $gr->modiby,
                        't_keydate' =>  $gr->keyat  == ''?null :  $gr->keyat ,
                        't_modidate' => $gr->modiat == ''?null : $gr->modiat,
                    ];

                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                    $prevDoc = $gr->doc_no;
                    $prevPtype =  $gr->ptype;
                    $prevCatCode =  $gr->catCode;
                    $prevCreditPurch = $gr->creditPurch;
                    $prevCashPurch = $gr->cashPurch;
                    $count++;
                }
                $result = array_reduce($grDt, function($arr, $item) {
                    if(!isset($arr[$item['t_docno'].$item['t_subject']])) {
                        $arr[$item['t_docno'].$item['t_subject']] = $item;
                    } else {
                        $arr[$item['t_docno'].$item['t_subject']]['t_amt'] += $item['t_amt'];
                        $arr[$item['t_docno'].$item['t_subject']]['t_fullamt'] += $item['t_fullamt'];
                        $arr[$item['t_docno'].$item['t_subject']]['t_amtt'] += $item['t_amtt'];
                    }
                    return $arr;
                });
                $result = array_values($result);

                foreach (array_chunk($result,1000) as $pr)
                {
                    $connection->table('acc_pinvdt')->insert($pr);
                }

                // $client = new \GuzzleHttp\Client();
                // $response = $client->request('POST', 'http://localhost:7388/postpiv.php', [
                //     'form_params' => [
                //         'data' =>  json_encode($grData),
                //     ]
                // ]);

                $client = new \GuzzleHttp\Client();
                $response = $client->request('POST', 'http://localhost/web_acc_tstgas/postpiv.php', [
                    'form_params' => [
                        'data' =>  json_encode($grData),
                    ]
                ]);
            }

            return true;
        }catch(\Exception $e){
            dd($e);
            return false;
        }

    }

    public function postCBData($docNoArray, $connection, $batchNo)
    {
        try{
            $isPost = false;
            $docNo = implode("', '", $docNoArray);
            $dataMt = DB::select(DB::raw("
                SELECT *
                FROM cashbills
                WHERE deleted_at IS NULL AND docno IN ('$docNo')
            "), []);

            $isCategorySet = SystemSetup::leftjoin('debtors', 'debtors.accountcode', '=', 'system_setups.dr_cashsales_acc')
            ->selectRaw("
                cr_cashsales_acc,
                set_category5,
                dr_cashsales_acc,
                set_category4, debtors.name AS dr_name,
                debtors.address1 AS dr_addr1,
                debtors.address2 AS dr_addr2,
                debtors.address3 AS dr_addr3,
                debtors.address4 AS dr_addr4,
                debtors.tel AS dr_tel,
                debtors.fax AS dr_fax")->first();

            if(count($dataMt)>0){
                $dataDt = DB::select(DB::raw("
                    SELECT a.*, c.code AS 'catCode', c.cr_cashsales_acc AS 'crAcc', a.created_by AS 'keyby',
                            a.created_at AS 'keyat', a.updated_by AS 'modiby',
                            a.updated_at AS 'modiat', d.name as C_NAME, d.discount as m_discount
                    FROM cashbilldts a
                    LEFT JOIN stockcodes b ON b.code = a.item_code AND b.deleted_at IS NULL
                    LEFT JOIN categories c ON c.id = b.cat_id AND c.deleted_at IS NULL
                    INNER JOIN cashbills d ON d.docno = a.doc_no AND d.deleted_at IS NULL
                    WHERE a.deleted_at IS NULL
                        AND doc_no IN ('$docNo')
                    ORDER BY a.doc_no, a.sequence_no
                "), []);
                $isPost = true;
            }

            $cbMt=[];
            foreach($dataMt as $cb) {
                $connection->table('acc_cbmt')->where('m_docno', '=', $cb->docno)->delete();
                $connection->table('acc_cbdt')->where('t_docno', '=', $cb->docno)->delete();
                $cbMt[] = [
                    'm_docno' => $cb->docno == null ? '' :  $cb->docno,
                    'm_date' => $cb->date == null ? '' : $cb->date,
                    'm_batch' => $batchNo == null ? '' :  $batchNo,
                    'm_tamt' => $cb->amount == null ? '' : $cb->amount,
                    'm_ttaxamt' => $cb->tax_amount == null || $cb->amount == '' ? 0.00  : $cb->tax_amount,
                    'm_tamtt' => $cb->amount == null || $cb->amount == '' ? 0.00 : $cb->amount,
                    'm_drac' => $isCategorySet->set_category4 != 'true'?$isCategorySet->dr_cashsales_acc : $cb->account_code,
                    'm_name' => $isCategorySet->set_category4 != 'true' ? $isCategorySet->dr_name : $cb->name,
                    'm_addr1' => $isCategorySet->set_category4 != 'true' ? $isCategorySet->dr_addr1 : $cb->addr1,
                    'm_addr2' => $isCategorySet->set_category4 != 'true' ? $isCategorySet->dr_addr2 : $cb->addr2,
                    'm_addr3' =>  $isCategorySet->set_category4 != 'true' ? $isCategorySet->dr_addr3 : $cb->addr3,
                    'm_addr4' =>  $isCategorySet->set_category4 != 'true' ? $isCategorySet->dr_addr4 : $cb->addr4,
                    'm_tel' => $isCategorySet->set_category4 != 'true' ? $isCategorySet->dr_tel :$cb->tel_no,
                    'm_fax' => $isCategorySet->set_category4 != 'true' ? $isCategorySet->dr_fax :$cb->fax_no,
                    'm_header' => $cb->header == null ? '' : $cb->header,
                    'm_footer' => $cb->footer == null ? '' : $cb->footer,
                    'm_ref1' => $cb->ref1 == ''?'' : $cb->ref1,
                    'm_ref2' => $cb->lpono == ''?'' : $cb->lpono,
                    'm_post' => $cb->posted == null ? '' : $cb->posted,
                    'm_keyuser' => $cb->created_by == null ? '' :  $cb->created_by,
                    'm_modiuser' => $cb->created_by == null ? '' : $cb->created_by,
                    'm_keydate' =>  $cb->created_at  == null ? '' :  $cb->created_at ,
                    'm_modidate' => $cb->updated_at == null ? '' : $cb->updated_at
                ];
                $cbData[]=[
                    'strdocno'=>  $cb->docno == null?'' :  $cb->docno,
                    'stracode'=>$isCategorySet->set_category4 != 'true'?$isCategorySet->dr_cashsales_acc : $cb->account_code
                ];
            }

            foreach (array_chunk($cbMt,1000) as $cb)
            {
                $connection->table('acc_cbmt')->insert($cb);
            }

            $res = [];
            $prevDoc = "";
            $prevCatCode = "";
            $prevCrAcc = "";
            $prevSubject = "";
            $tSeq = "005";
            $isNewDoc = false;
            $isSetDiscount = false;
            $amt = 0;
            $amtTaxed = 0;
            $count = 1;

            $cbDt=[];
            if(isset($dataDt)){
                foreach($dataDt as $cb) {
                    if($prevDoc != $cb->doc_no || count($dataDt) == $count){
                        if($prevDoc != ''){
                            if($isCategorySet->set_category5 == 'true'){
                                if($prevDoc == $cb->doc_no && $prevCatCode == $cb->catCode){
                                    $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevCrAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt + $cb->amount : ($amt + $cb->amount) - $cb->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt + $cb->amount : ($amt + $cb->amount) - $cb->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed + $cb->amount : ($amt + $cb->amount) - $cb->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                }elseif($prevDoc == $cb->doc_no){
                                    $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevCrAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $cb->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $cb->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed : $amtTaxed - $cb->m_discount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                    $cbDt[] = [
                                        't_docno' =>  $cb->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $cb->crAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $isCategorySet->set_category4 != 'true'?$isCategorySet->dr_name : $cb->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>0,
                                        't_amt' =>$cb->amount ,
                                        't_fullamt'=>$cb->amount ,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$cb->amount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );

                                }elseif(count($dataDt) == $count){
                                    $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevCrAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $cb->m_discount ,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $cb->m_discount ,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed : $amtTaxed - $cb->m_discount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "005";
                                    $cbDt[] = [
                                        't_docno' =>  $cb->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $cb->crAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $isCategorySet->set_category4 != 'true'?$isCategorySet->dr_name : $cb->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>0,
                                        't_amt' =>$cb->amount ,
                                        't_fullamt'=>$cb->amount ,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$cb->amount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                }else{
                                    $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevCrAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $cb->m_discount ,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $cb->m_discount ,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed : $amtTaxed - $cb->m_discount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "005";
                                }

                            }else{
                                if($prevDoc == $cb->doc_no){
                                    $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_cashsales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt + $cb->amount : ($amt + $cb->amount) - $cb->m_discount ,
                                        't_fullamt'=>$isSetDiscount ? $amt + $cb->amount : ($amt + $cb->amount) - $cb->m_discount ,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed + $cb->amount : ($amtTaxed + $cb->amount) - $cb->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                }elseif(count($dataDt) == $count){
                                    $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_cashsales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt: $amt - $cb->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt: $amt - $cb->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed:  $amtTaxed - $cb->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "005";
                                    $cbDt[] = [
                                        't_docno' =>  $cb->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_cashsales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $isCategorySet->set_category4 != 'true'?$isCategorySet->dr_name : $cb->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>0,
                                        't_amt' =>$cb->amount,
                                        't_fullamt'=>$cb->amount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$cb->amount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];

                                }else{
                                    $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_cashsales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt: $amt - $cb->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt: $amt - $cb->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed: $amtTaxed - $cb->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "005";
                                }

                            }

                        }else{
                            if(count($dataDt) == $count){
                                if($isCategorySet->set_category5 == 'true'){
                                    $cbDt[] = [
                                        't_docno' =>  $cb->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $cb->crAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $isCategorySet->set_category4 != 'true'?$isCategorySet->dr_name : $cb->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $cb->amount: $cb->amount - $cb->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $cb->amount: $cb->amount - $cb->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $cb->amount: $cb->amount - $cb->m_discount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                }else{
                                    $cbDt[] = [
                                        't_docno' =>  $cb->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_cashsales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $isCategorySet->set_category4 != 'true'?$isCategorySet->dr_name : $cb->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $cb->amount: $cb->amount - $cb->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $cb->amount: $cb->amount - $cb->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $cb->amount: $cb->amount - $cb->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                }
                            }
                        }

                        $amt = 0;
                        $amtTaxed = 0;
                        $amt = $amt + $cb->amount;
                        $amtTaxed =  $amtTaxed  + $cb->amount;
                    }else{
                        if($isCategorySet->set_category5 == 'true'){
                            if($prevCatCode == $cb->catCode){
                                $amt = $amt + $cb->amount;
                                $amtTaxed =  $amtTaxed  + $cb->amount;
                            }else{
                                $cbDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevCrAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $cb->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt: $amt - $cb->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt: $amt - $cb->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed: $amtTaxed - $cb->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                ];
                                $isSetDiscount = true;
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );

                                $amt = 0;
                                $amtTaxed = 0;
                                $amt = $amt + $cb->amount;
                                $amtTaxed =  $amtTaxed  + $cb->amount;
                            }
                        }else{
                            $amt = $amt + $cb->amount;
                            $amtTaxed =  $amtTaxed  + $cb->amount;
                        }
                    }

                    $cbDt[] = [
                        't_docno' => $cb->doc_no == null?'' :  $cb->doc_no,
                        't_sn' => $tSeq,
                        't_crac' => "-",
                        't_itemno' => $tSeq,
                        't_subject' => $cb->subject == null?'' : $cb->subject,
                        't_details' => $cb->details == '' || $cb->details == NULL?'-' : $cb->details,
                        't_qty' => $cb->qty == ''?null : $cb->qty,
                        't_uom' => $cb->uom == ''?null : $cb->uom,
                        't_uprice' => $cb->uprice == ''?null : $cb->uprice,
                        't_idiscamt' => $cb->discount == ''|| $cb->discount == NULL? 0 : $cb->discount,
                        't_amt' => $cb->amount == ''?null : $cb->amount,
                        't_fullamt' => $cb->amount == ''?null :  $cb->amount,
                        't_taxcode' => $cb->tax_code == ''|| $cb->tax_code == NULL? '' : $cb->tax_code,
                        't_taxrate' => $cb->tax_rate == ''|| $cb->tax_rate == NULL? 0 : $cb->tax_rate,
                        't_taxamt' => $cb->tax_amount == ''|| $cb->discount == NULL? 0 : $cb->tax_amount,
                        't_amtt' => $cb->amount == ''?null : $cb->amount,
                        't_batchno' => $batchNo,
                        't_keyuser' => $cb->created_by == ''?null :  $cb->created_by,
                        't_modiuser' => $cb->updated_by == ''|| $cb->updated_by == null ? 'PHPSYS' : $cb->updated_by,
                        't_keydate' =>  $cb->created_at  == ''?null :  $cb->created_at,
                        't_modidate' => $cb->updated_at == '' || $cb->updated_at == null ? Carbon::now() : $cb->updated_at,
                    ];

                    $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                    $prevDoc = $cb->doc_no;
                    $prevCatCode =  $cb->catCode;
                    $prevCrAcc = $cb->crAcc;
                    $prevSubject = $isCategorySet->set_category4 != 'true'?$isCategorySet->dr_name : $cb->C_NAME;
                    $count++;
                }
                $result = array_reduce($cbDt, function($arr, $item) {
                    if(!isset($arr[$item['t_docno'].$item['t_subject'].$item['t_crac']])) {
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']] = $item;
                    } else {
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']]['t_amt'] += $item['t_amt'];
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']]['t_fullamt'] += $item['t_fullamt'];
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']]['t_amtt'] += $item['t_amtt'];
                    }
                    return $arr;
                });
                $result = array_values($result);

                foreach (array_chunk($result,1000) as $cb)
                {
                    $connection->table('acc_cbdt')->insert($cb);
                }

                if($isPost){
                        DB::statement("
                        UPDATE cashbills
                        SET posted = 'Y'
                        WHERE deleted_at IS NULL AND docno IN ('$docNo')
                    ");
                }

                $client = new \GuzzleHttp\Client();
                $response = $client->request('POST', 'http://tstgas.com/webacc/postcb.php', [
                    'form_params' => [
                        'data' =>  json_encode($cbData),
                    ]
                ]);
                // $client = new \GuzzleHttp\Client();
                // $response = $client->request('POST', 'http://localhost/web_acc_tstgas/postcb.php', [
                //     'form_params' => [
                //         'data' =>  json_encode($cbData),
                //     ]
                // ]);
            }
            return true;

        }catch(\Exception $e){
            dd($e);
            return false;
        }
    }

    public function postINVData($docNoArray, $connection, $batchNo)
    {
        try{
            $isPost = false;
            $docNo = implode("', '", $docNoArray);
            $dataMt = DB::select(DB::raw("
                SELECT *
                FROM invoices
                WHERE deleted_at IS NULL AND docno IN ('$docNo')
            "), []);

            if(count($dataMt)>0){
                $dataDt = DB::select(DB::raw("
                    SELECT a.*, c.code AS 'catCode', c.cr_invoicesales_acc AS 'invAcc', a.created_by AS 'keyby',
                            a.created_at AS 'keyat', a.updated_by AS 'modiby',
                            a.updated_at AS 'modiat', d.name AS C_NAME, d.discount as m_discount
                    FROM invoice_data a
                    LEFT JOIN stockcodes b ON b.code = a.item_code AND b.deleted_at IS NULL
                    LEFT JOIN categories c ON c.id = b.cat_id AND c.deleted_at IS NULL
                    INNER JOIN invoices d ON d.docno = a.doc_no AND d.deleted_at IS NULL
                    WHERE a.deleted_at IS NULL
                        AND doc_no IN ('$docNo')
                    ORDER BY a.doc_no, a.sequence_no
                "), []);
                $isPost = true;
            }

            $invMt=[];
            foreach($dataMt as $inv) {
                $connection->table('acc_invmt')->where('m_docno', '=', $inv->docno)->delete();
                $connection->table('acc_invdt')->where('t_docno', '=', $inv->docno)->delete();

                $invMt[] = [
                    'm_docno' => $inv->docno == null?'' :  $inv->docno,
                    'm_date' => $inv->date == null?'' : $inv->date,
                    'm_batch' => $batchNo == null?'' :  $batchNo,
                    'm_tamt' => $inv->amount == null?'' : $inv->amount,
                    'm_ttaxamt' => $inv->tax_amount == null?'' : $inv->tax_amount,
                    'm_tamtt' => $inv->amount == null?0.00 : $inv->amount,
                    'm_drac' => $inv->account_code == null?'' : $inv->account_code,
                    'm_name' => $inv->name == null?'' : $inv->name,
                    'm_addr1' => $inv->addr1 == null?'' : $inv->addr1,
                    'm_addr2' => $inv->addr2 == null?'' :  $inv->addr2,
                    'm_addr3' => $inv->addr3 == null?'' : $inv->addr3,
                    'm_addr4' => $inv->addr4 == null?'' : $inv->addr4,
                    'm_tel' => $inv->tel_no == null?'' : $inv->tel_no,
                    'm_fax' => $inv->fax_no == null?'' : $inv->fax_no,
                    'm_header' => $inv->header == null?'' : $inv->header,
                    'm_footer' => $inv->footer == null?'' : $inv->footer,
                    'm_cterm' => $inv->credit_term == null?'' : $inv->credit_term,
                    'm_ref1' => $inv->refno == null?'' : $inv->refno,
                    'm_ref2' => $inv->do_no == null?'' : $inv->do_no,
                    'm_post' => $inv->posted == null?'' : $inv->posted,
                    'm_keyuser' => $inv->created_by == null?'' :  $inv->created_by,
                    'm_modiuser' => $inv->created_by == null?'' : $inv->created_by,
                    'm_keydate' =>  $inv->updated_at  == null?'' :  $inv->updated_at ,
                    'm_modidate' => $inv->updated_at == null?'' : $inv->updated_at,
                ];

                $ivData[]=[
                    'strdocno'=> $inv->docno == ''?null :  $inv->docno,
                    'stracode'=>$inv->account_code == ''?null : $inv->account_code
                ];
            }
            foreach (array_chunk($invMt,1000) as $inv)
            {
                $connection->table('acc_invmt')->insert($inv);
            }

            $isCategorySet = SystemSetup::select(
                'cr_invoicesales_acc',
                'set_category7')->first();
            $res = [];
            $prevDoc = "";
            $prevCatCode = "";
            $prevInvAcc = "";
            $prevSubject = "";
            $tSeq = "0005";
            $isNewDoc = false;
            $isSetDiscount = false;
            $amt = 0;
            $amtTaxed = 0;
            $count = 1;

            if(isset($dataDt)){
                $invDt=[];
                foreach($dataDt as $inv) {
                    if($prevDoc != $inv->doc_no || count($dataDt) == $count){
                        if($prevDoc != ''){
                            if($isCategorySet->set_category7 == 'true'){
                                if($prevDoc == $inv->doc_no && $prevCatCode == $inv->catCode){
                                    $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevInvAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt + $inv->amount : ($amt + $inv->amount) - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt + $inv->amount : ($amt + $inv->amount) - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $amtTaxed + $inv->amount : ($amtTaxed + $inv->amount) - $inv->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                                }elseif($prevDoc == $inv->doc_no){
                                    $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevInvAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed : $amtTaxed - $inv->m_discount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                                    $invDt[] = [
                                        't_docno' =>  $inv->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $inv->invAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $inv->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>0,
                                        't_amt' =>$inv->amount ,
                                        't_fullamt'=>$inv->amount ,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$inv->amount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                                }elseif(count($dataDt) == $count){
                                    $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevInvAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $amtTaxed : $amtTaxed - $inv->m_discount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "0005";
                                    $invDt[] = [
                                        't_docno' =>  $inv->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $inv->invAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $inv->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>0,
                                        't_amt' =>$inv->amount ,
                                        't_fullamt'=>$inv->amount ,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$inv->amount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                                }else{
                                    $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevInvAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $amtTaxed : $amtTaxed - $inv->m_discount ,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "0005";
                                }
                            }else{
                                if($prevDoc == $inv->doc_no){
                                    $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_invoicesales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt + $inv->amount : ($amt + $inv->amount) - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt + $inv->amount : ($amt + $inv->amount) - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed + $inv->amount : ($amtTaxed + $inv->amount) - $inv->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                                }elseif(count($dataDt) == $count){
                                    $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_invoicesales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed : $amtTaxed - $inv->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "0005";
                                    $invDt[] = [
                                        't_docno' =>  $inv->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_invoicesales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $inv->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>0,
                                        't_amt' =>$inv->amount,
                                        't_fullamt'=>$inv->amount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$inv->amount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                }else{
                                    $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_invoicesales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' => $isSetDiscount ? $amtTaxed : $amtTaxed - $inv->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = "0005";
                                }
                            }
                        }else{
                            if(count($dataDt) == $count){
                                if($isCategorySet->set_category7 == 'true'){
                                    $invDt[] = [
                                        't_docno' =>  $inv->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $inv->invAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $inv->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $inv->amount : $inv->amount - $inv->m_discount ,
                                        't_fullamt'=>$isSetDiscount ? $inv->amount : $inv->amount - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $inv->amount : $inv->amount - $inv->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                                }else{
                                    $invDt[] = [
                                        't_docno' =>  $inv->doc_no,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $isCategorySet->cr_invoicesales_acc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $inv->C_NAME,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $inv->amount : $inv->amount - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $inv->amount : $inv->amount - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $inv->amount : $inv->amount - $inv->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                    ];
                                    $isSetDiscount = true;
                                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                                }
                            }
                        }

                        $amt = 0;
                        $amtTaxed = 0;
                        $amt = $amt + $inv->amount;
                        $amtTaxed =  $amtTaxed  + $inv->amount;
                    }else{
                        if($isCategorySet->set_category7 == 'true'){
                            if($prevCatCode == $inv->catCode){
                                $amt = $amt + $inv->amount;
                                $amtTaxed =  $amtTaxed  + $inv->amount;
                            }else{
                                $invDt[] = [
                                        't_docno' =>  $prevDoc,
                                        't_sn' => $tSeq,
                                        't_crac' =>  $prevInvAcc,
                                        't_itemno' => $tSeq,
                                        't_subject' => $prevSubject,
                                        't_details' => '',
                                        't_qty' => 0,
                                        't_uom' =>'',
                                        't_uprice'=>0,
                                        't_idiscamt'=>$isSetDiscount ? 0 : $inv->m_discount,
                                        't_amt' =>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_fullamt'=>$isSetDiscount ? $amt : $amt - $inv->m_discount,
                                        't_taxcode' => '',
                                        't_taxrate' => 0,
                                        't_taxamt' => 0,
                                        't_amtt' =>$isSetDiscount ? $amtTaxed : $amtTaxed - $inv->m_discount,
                                        't_batchno' => $batchNo,
                                        't_keyuser' => Auth::user()->usercode,
                                        't_modiuser' => Auth::user()->usercode,
                                        't_keydate' => Carbon::now(),
                                        't_modidate' => Carbon::now(),
                                ];
                                $isSetDiscount = true;
                                $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );

                                $amt = 0;
                                $amtTaxed = 0;
                                $amt = $amt + $inv->amount;
                                $amtTaxed =  $amtTaxed  + $inv->amount;
                            }
                        }else{
                            $amt = $amt + $inv->amount;
                            $amtTaxed =  $amtTaxed + $inv->amount;
                        }
                    }

                    $invDt[] = [
                        't_docno' => $inv->doc_no == ''?null :  $inv->doc_no,
                        't_sn' => $tSeq,
                        't_crac' => "-",
                        't_itemno' => $tSeq,
                        't_subject' => $inv->subject == ''?null : $inv->subject,
                        't_details' => $inv->details == ''|| $inv->discount == null ? '-': $inv->details,
                        't_qty' => $inv->quantity == ''?null : $inv->quantity,
                        't_uom' => $inv->uom == ''?null : $inv->uom,
                        't_uprice' => $inv->unit_price == ''?null : $inv->unit_price,
                        't_idiscamt' => $inv->discount == '' || $inv->discount == null ? 0: $inv->discount,
                        't_amt' => $inv->amount == ''?null : $inv->amount,
                        't_fullamt' => $inv->amount == ''?null :  $inv->amount,
                        't_taxcode' => '',
                        't_taxrate' => 0,
                        't_taxamt' => 0,
                        't_amtt' => $inv->amount == ''?null : $inv->amount,
                        't_batchno' => $batchNo,
                        't_keyuser' => $inv->created_by == ''?null :  $inv->created_by,
                        't_modiuser' => $inv->updated_by == ''|| $inv->updated_by == null? 'PHPSYS' : $inv->updated_by,
                        't_keydate' =>  $inv->created_at  == ''?null :  $inv->created_at ,
                        't_modidate' => $inv->updated_at == ''|| $inv->updated_at== null? Carbon::now() : $inv->updated_at,
                    ];

                    $tSeq = str_pad( $tSeq + 5, 4, "0", STR_PAD_LEFT );
                    $prevDoc = $inv->doc_no;
                    $prevCatCode =  $inv->catCode;
                    $prevInvAcc = $inv->invAcc;
                    $prevSubject = $inv->C_NAME;
                    $count++;
                }
                $result = array_reduce($invDt, function($arr, $item) {
                    if(!isset($arr[$item['t_docno'].$item['t_subject'].$item['t_crac']])) {
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']] = $item;
                    } else {
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']]['t_amt'] += $item['t_amt'];
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']]['t_fullamt'] += $item['t_fullamt'];
                        $arr[$item['t_docno'].$item['t_subject'].$item['t_crac']]['t_amtt'] += $item['t_amtt'];
                    }
                    return $arr;
                });
                $result = array_values($result);
                foreach (array_chunk($result,1000) as $inv)
                {
                    $connection->table('acc_invdt')->insert($inv);
                }

                if($isPost){
                        DB::statement("
                        UPDATE invoices
                        SET posted = 'Y'
                        WHERE deleted_at IS NULL AND docno IN ('$docNo')
                    ");
                }

                $client = new \GuzzleHttp\Client();
                $response = $client->request('POST', 'http://tstgas.com/webacc/postiv.php', [
                    'form_params' => [
                        'data' =>  json_encode($ivData),
                    ]
                ]);
                // $client = new \GuzzleHttp\Client();
                // $response = $client->request('POST', 'http://localhost/web_acc_tstgas/postiv.php', [
                //     'form_params' => [
                //         'data' =>  json_encode($ivData),
                //     ]
                // ]);
            }
            return true;
        }catch(\Exception $e){
            dd($e);
            return false;
        }
    }

    public function postPRData($docNoArray, $connection, $batchNo)
    {
        try{
            $isPost = false;
            $docNo = implode("', '", $docNoArray);
            $dataMt = DB::select(DB::raw("
                SELECT *
                FROM preturns
                WHERE deleted_at IS NULL AND docno IN ('$docNo')
            "), []);

            if(count($dataMt)>0){
                $dataDt = DB::select(DB::raw("
                    SELECT a.*, c.code AS 'catCode', c.cr_purchasereturn_acc AS 'purchRet',a.created_by AS 'keyby',
                            a.created_at AS 'keyat', a.updated_by AS 'modiby',
                            a.updated_at AS 'modiat'
                    FROM preturndts a
                    INNER JOIN stockcodes b ON b.code = a.item_code
                    INNER JOIN categories c ON c.id = b.cat_id
                    INNER JOIN preturns d ON d.docno = a.doc_no
                    WHERE a.deleted_at IS NULL AND
                          b.deleted_at IS NULL AND c.deleted_at IS NULL AND d.deleted_at IS NULL
                          AND doc_no IN ('$docNo')
                "), []);
                $isPost = true;
            }else{
                $dataDt = [];
            }

            $prMt=[];
            foreach($dataMt as $pr) {
                $prMt[] = [
                    'm_docno' => $pr->docno == ''?null :  $pr->docno,
                    'M_DATE' => $pr->date == ''?null : $pr->date,
                    'M_BATCH' => $batchNo == ''?null :  $batchNo,
                    'M_TAMT' => $pr->amount == ''?null : $pr->amount,
                    'm_ttaxamt' => $pr->tax_amount == ''?null : $pr->tax_amount,
                    'm_tamtt' => $pr->amount == ''?null : $pr->amount,
                    'M_DRAC' => $pr->account_code == ''?null : $pr->account_code,
                    'M_NAME' => $pr->name == ''?null : $pr->name,
                    'M_ADDR1' => $pr->addr1 == ''?null : $pr->addr1,
                    'M_ADDR2' => $pr->addr2 == ''?null :  $pr->addr2,
                    'M_ADDR3' => $pr->addr3 == ''?null : $pr->addr3,
                    'M_ADDR4' => $pr->addr4 == ''?null : $pr->addr4,
                    'M_TEL' => $pr->tel_no == ''?null : $pr->tel_no,
                    'M_FAX' => $pr->fax_no == ''?null : $pr->fax_no,
                    'M_HEADER' => $pr->header == ''?null : $pr->header,
                    'M_FOOTER' => $pr->footer == ''?null : $pr->footer,
                    'M_REF1' => $pr->ref1 == ''?null : $pr->ref1,
                    'M_REF2' => $pr->ref2 == ''?null : $pr->ref2,
                    'M_POST' => $pr->posted == ''?null : $pr->posted,
                    'M_KEYUSER' => $pr->created_by == ''?null :  $pr->created_by,
                    'M_MODIUSER' => $pr->updated_by == ''?null : $pr->updated_by,
                    'M_KEYDATE' =>  $pr->created_at  == ''?null :  $pr->created_at ,
                    'M_MODIDATE' => $pr->updated_at == ''?null : $pr->updated_at,
                ];
            }
            foreach (array_chunk($prMt,1000) as $pr)
            {
                $connection->table('acc_dnmt')->insert($pr);
            }

            $isCategorySet = SystemSetup::select(
                'cr_purchasereturn_acc',
                'set_category3')->first();
            $res = [];
            $prevDoc = "";
            $prevCatCode = "";
            $prevPurchRet = "";
            $tSeq = "005";
            $isNewDoc = false;
            $amt = 0;
            $amtTaxed = 0;
            $count = 1;

            $prDt=[];
            foreach($dataDt as $pr) {

                if($prevDoc != $pr->doc_no || count($dataDt) == $count){
                    if($prevDoc != ''){
                        if($isCategorySet->set_category3 == 'true'){
                            if($prevDoc == $pr->doc_no && $prevCatCode == $pr->catCode){
                                $prDt[] = [
                                    't_docno' =>  $prevDoc,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => $tSeq,
                                    'T_CRAC' =>  $prevPurchRet,
                                    'T_SUBJECT' => "$prevCatCode PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$amt + $pr->amount,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $amtTaxed + $pr->amount,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                            }elseif($prevDoc == $pr->doc_no){
                                $prDt[] = [
                                    't_docno' =>  $prevDoc,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $prevPurchRet,
                                    'T_SUBJECT' => "$prevCatCode PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$amt,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $amtTaxed,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                                $prDt[] = [
                                    't_docno' =>  $pr->doc_no,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $pr->purchRet,
                                    'T_SUBJECT' => "$pr->catCode PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$pr->amount,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $pr->amount,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );

                            }elseif(count($dataDt) == $count){
                                $prDt[] = [
                                    't_docno' =>  $prevDoc,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $prevPurchRet,
                                    'T_SUBJECT' => "$prevCatCode PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$amt,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $amtTaxed,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = "005";
                                $prDt[] = [
                                    't_docno' =>  $pr->doc_no,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $pr->purchRet,
                                    'T_SUBJECT' => "$pr->catCode PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$pr->amount,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $pr->amount,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                            }else{
                                $prDt[] = [
                                    't_docno' =>  $prevDoc,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $prevPurchRet,
                                    'T_SUBJECT' => "$prevCatCode PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$amt,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $amtTaxed,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = "005";
                            }

                        }else{
                            if($prevDoc == $pr->doc_no){
                                $prDt[] = [
                                    't_docno' =>  $prevDoc,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $isCategorySet->cr_purchasereturn_acc,
                                    'T_SUBJECT' => "PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$amt + $pr->amount,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $amtTaxed + $pr->amount,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                            }elseif(count($dataDt) == $count){
                                $prDt[] = [
                                    't_docno' =>  $prevDoc,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $isCategorySet->cr_purchasereturn_acc,
                                    'T_SUBJECT' => "PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$amt,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $amtTaxed,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = "005";
                                $prDt[] = [
                                    't_docno' =>  $pr->doc_no,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $isCategorySet->cr_purchasereturn_acc,
                                    'T_SUBJECT' => "PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$pr->amount,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $pr->amount,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];

                            }else{
                                $prDt[] = [
                                    't_docno' =>  $prevDoc,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $isCategorySet->cr_purchasereturn_acc,
                                    'T_SUBJECT' => "PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$amt,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $amtTaxed,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = "005";
                            }

                        }
                    }else{
                        if(count($dataDt) == $count){
                            if($isCategorySet->set_category3 == 'true'){
                                $prDt[] = [
                                    't_docno' =>  $pr->doc_no,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $pr->purchRet,
                                    'T_SUBJECT' => "$pr->catCode PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$pr->amount,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' =>$pr->amount,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                            }else{
                                $prDt[] = [
                                    't_docno' =>  $pr->doc_no,
                                    'T_SN' => $tSeq,
                                    'T_ITEMNO' => null,
                                    'T_CRAC' =>  $isCategorySet->cr_purchasereturn_acc,
                                    'T_SUBJECT' => "PURCHASES",
                                    'T_DETAILS' => '',
                                    'T_QTY' => 0,
                                    'T_UOM' =>'',
                                    'T_UPRICE'=>0,
                                    'T_AMT' =>$pr->amount,
                                    't_taxcode' => '',
                                    't_taxrate' => 0,
                                    't_taxamt' => 0,
                                    't_amtt' => $pr->amount,
                                    't_batchno' => $batchNo,
                                    'T_KEYUSER' => Auth::user()->usercode,
                                    'T_MODIUSER' => Auth::user()->usercode,
                                    'T_KEYDATE' => Carbon::now(),
                                    'T_MODIDATE' => Carbon::now(),
                                ];
                                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                            }
                        }
                    }

                    $amt = 0;
                    $amtTaxed = 0;
                    $amt = $amt + $pr->amount;
                    $amtTaxed =  $amtTaxed  + $pr->amount;
                }else{
                    if($isCategorySet->set_category3 == 'true'){
                        if($prevCatCode == $pr->catCode){
                            $amt = $amt + $pr->amount;
                            $amtTaxed =  $amtTaxed  + $pr->amount;
                        }else{
                            $prDt[] = [
                                't_docno' =>  $prevDoc,
                                'T_SN' => $tSeq,
                                'T_ITEMNO' => null,
                                'T_CRAC' =>  $prevPurchRet,
                                'T_SUBJECT' => "$prevCatCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );

                            $amt = 0;
                            $amtTaxed = 0;
                            $amt = $amt + $pr->amount;
                            $amtTaxed =  $amtTaxed  + $pr->amount;
                        }

                    }else{
                        $amt = $amt + $pr->amount;
                        $amtTaxed =  $amtTaxed  + $pr->amount;
                    }
                }

                $prDt[] = [
                    't_docno' => $pr->doc_no == ''?null :  $pr->doc_no,
                    'T_SN' =>  $tSeq,
                    'T_CRAC' => null,
                    'T_ITEMNO' => $pr->item_code == ''?null : $pr->item_code,
                    'T_SUBJECT' => $pr->subject == ''?null : $pr->subject,
                    'T_DETAILS' => $pr->details == ''?null : $pr->details,
                    'T_QTY' => $pr->qty == ''?null : $pr->qty,
                    'T_UOM' => $pr->uom == ''?null : $pr->uom,
                    'T_UPRICE' => $pr->ucost == ''?null : $pr->ucost,
                    'T_AMT' => $pr->amount == ''?null : $pr->amount,
                    't_taxcode' => $pr->tax_code == ''?null : $pr->tax_code,
                    't_taxrate' => $pr->tax_rate == ''?null : $pr->tax_rate,
                    't_taxamt' => $pr->tax_amount == ''?null : $pr->tax_amount,
                    't_amtt' => $pr->amount == ''?null : $pr->amount,
                    't_batchno' => $batchNo,
                    'T_KEYUSER' => $pr->created_by == ''?null :  $pr->created_by,
                    'T_MODIUSER' => $pr->updated_by == ''?null : $pr->updated_by,
                    'T_KEYDATE' =>  $pr->created_at  == ''?null :  $pr->created_at ,
                    'T_MODIDATE' => $pr->updated_at == ''?null : $pr->updated_at,
                ];

                $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                $prevDoc = $pr->doc_no;
                $prevCatCode =  $pr->catCode;
                $prevPurchRet = $pr->purchRet;
                $count++;
            }
            $result = array_reduce($prDt, function($arr, $item) {
                if(!isset($arr[$item['t_docno'].$item['T_SUBJECT']])) {
                    $arr[$item['t_docno'].$item['T_SUBJECT']] = $item;
                } else {
                    $arr[$item['t_docno'].$item['T_SUBJECT']]['T_AMT'] += $item['T_AMT'];
                    $arr[$item['t_docno'].$item['T_SUBJECT']]['t_amtt'] += $item['t_amtt'];
                }
                return $arr;
            });
            $result = array_values($result);
            foreach (array_chunk($result,1000) as $pr)
            {
                $connection->table('acc_dndt')->insert($pr);
            }

            if($isPost){
                    DB::statement("
                    UPDATE preturns
                    SET posted = 'Y'
                    WHERE deleted_at IS NULL AND docno IN ('$docNo')
                ");
            }
            return true;

        }catch(\Exception $e){
            dd($e);
            return false;
        }
    }

    public function postSRData($docNoArray, $connection, $batchNo)
    {
        try{
            $isPost = false;
        $docNo = implode("', '", $docNoArray);
        $dataMt = DB::select(DB::raw("
            SELECT *
            FROM salesreturns
            WHERE deleted_at IS NULL AND docno IN ('$docNo')
        "), []);

        if(count($dataMt)>0){
            $dataDt = DB::select(DB::raw("
                SELECT a.*, c.code AS 'catCode', c.dr_cashsales_return_acc AS 'salesRet',a.created_by AS 'keyby',
                        a.created_at AS 'keyat', a.updated_by AS 'modiby',
                        a.updated_at AS 'modiat'
                FROM salesreturndts a
                INNER JOIN stockcodes b ON b.code = a.item_code
                INNER JOIN categories c ON c.id = b.cat_id
                INNER JOIN salesreturns d ON d.docno = a.doc_no
                WHERE a.deleted_at IS NULL AND
                      b.deleted_at IS NULL AND c.deleted_at IS NULL AND d.deleted_at IS NULL
                      AND doc_no IN ('$docNo')
            "), []);
            $isPost = true;
        }else{
            $dataDt = [];
        }

        $srMt=[];
        foreach($dataMt as $sr) {
            $srMt[] = [
                'm_docno' => $sr->docno == ''?null :  $sr->docno,
                'M_DATE' => $sr->date == ''?null : $sr->date,
                'M_BATCH' => $batchNo == ''?null :  $batchNo,
                'M_TAMT' => $sr->amount == ''?null : $sr->amount,
                'm_ttaxamt' => $sr->tax_amount == ''?null : $sr->tax_amount,
                'm_tamtt' => $sr->amount == ''?null : $sr->amount,
                'M_CRAC' => $sr->account_code == ''?null : $sr->account_code,
                'M_NAME' => $sr->name == ''?null : $sr->name,
                'M_ADDR1' => $sr->addr1 == ''?null : $sr->addr1,
                'M_ADDR2' => $sr->addr2 == ''?null :  $sr->addr2,
                'M_ADDR3' => $sr->addr3 == ''?null : $sr->addr3,
                'M_ADDR4' => $sr->addr4 == ''?null : $sr->addr4,
                'M_TEL' => $sr->tel_no == ''?null : $sr->tel_no,
                'M_FAX' => $sr->fax_no == ''?null : $sr->fax_no,
                'M_HEADER' => $sr->header == ''?null : $sr->header,
                'M_FOOTER' => $sr->footer == ''?null : $sr->footer,
                'M_REF1' => $sr->ref1 == ''?null : $sr->ref1,
                'M_REF2' => $sr->ref2 == ''?null : $sr->ref2,
                'M_POST' => $sr->posted == ''?null : $sr->posted,
                'M_KEYUSER' => $sr->created_by == ''?null :  $sr->created_by,
                'M_MODIUSER' => $sr->updated_by == ''?null : $sr->updated_by,
                'M_KEYDATE' =>  $sr->created_at  == ''?null :  $sr->created_at ,
                'M_MODIDATE' => $sr->updated_at == ''?null : $sr->updated_at,
            ];
        }
        foreach (array_chunk($srMt,1000) as $sr)
        {
            $connection->table('acc_cnmt')->insert($sr);
        }

        $isCategorySet = SystemSetup::select(
            'dr_cashsales_return_acc',
            'set_category6')->first();
        $res = [];
        $prevDoc = "";
        $prevCatCode = "";
        $prevSalesRet = "";
        $dtType = "";
        $prevInvPurch = "";
        $prevCbPurch = "";
        $tSeq = "005";
        $isNewDoc = false;
        $amt = 0;
        $amtTaxed = 0;
        $count = 1;

        $srDt=[];
        foreach($dataDt as $sr) {
            if($prevDoc != $sr->doc_no || count($dataDt) == $count){
                if($prevDoc != ''){
                    if($isCategorySet->set_category6 == 'true'){
                        if($prevDoc == $sr->doc_no && $prevCatCode == $sr->catCode){
                            $srDt[] = [
                                't_docno' =>  $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $prevSalesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$prevCatCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt + $sr->amount,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed + $sr->amount,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                        }elseif($prevDoc == $sr->doc_no){
                            // dd($sr->subject." ".$sr->catCode);
                            $srDt[] = [
                                't_docno' => $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $prevSalesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$prevCatCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                            $srDt[] = [
                                't_docno' =>  $sr->doc_no,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>   $sr->salesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$sr->catCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$sr->amount,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $sr->amount,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                        }elseif(count($dataDt) == $count){
                            $srDt[] = [
                                't_docno' => $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $prevSalesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$prevCatCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = "005";
                            $srDt[] = [
                                't_docno' =>  $sr->doc_no,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>   $sr->salesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$sr->catCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$sr->amount,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $sr->amount,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                        }else{
                            $srDt[] = [
                                't_docno' => $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $prevSalesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$prevCatCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = "005";
                        }
                    }else{
                        if($prevDoc == $sr->doc_no){
                            $srDt[] = [
                                't_docno' => $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $isCategorySet->dr_cashsales_return_acc,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt + $sr->amount,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed + $sr->amount,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                        }elseif(count($dataDt) == $count){
                            $srDt[] = [
                                't_docno' => $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $isCategorySet->dr_cashsales_return_acc,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = "005";
                            $srDt[] = [
                                't_docno' =>  $sr->doc_no,
                                'T_SN' => $tSeq,
                                'T_DRAC' => $isCategorySet->dr_cashsales_return_acc,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$sr->amount,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $sr->amount,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                        }else{
                            $srDt[] = [
                                't_docno' => $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $isCategorySet->dr_cashsales_return_acc,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = "005";
                        }
                    }
                }else{
                    if(count($dataDt) == $count){
                        if($isCategorySet->set_category6 == 'true'){
                            $srDt[] = [
                                't_docno' =>  $sr->doc_no,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>   $sr->salesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$sr->catCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$sr->amount,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $sr->amount,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                        }else{
                            $srDt[] = [
                                't_docno' =>  $sr->doc_no,
                                'T_SN' => $tSeq,
                                'T_DRAC' => $isCategorySet->dr_cashsales_return_acc,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$sr->amount,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $sr->amount,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                            ];
                            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
                        }
                    }
                }

                $amt = 0;
                $amtTaxed = 0;
                $amt = $amt + $sr->amount;
                $amtTaxed =  $amtTaxed  + $sr->amount;
            }else{
                if($isCategorySet->set_category6 == 'true'){
                    if($prevCatCode == $sr->catCode){
                        $amt = $amt + $sr->amount;
                        $amtTaxed =  $amtTaxed  + $sr->amount;
                    }else{
                        $srDt[] = [
                                't_docno' => $prevDoc,
                                'T_SN' => $tSeq,
                                'T_DRAC' =>  $prevSalesRet,
                                'T_ITEMNO' => null,
                                'T_SUBJECT' => "$prevCatCode PURCHASES",
                                'T_DETAILS' => '',
                                'T_QTY' => 0,
                                'T_UOM' =>'',
                                'T_UPRICE'=>0,
                                'T_AMT' =>$amt,
                                't_taxcode' => '',
                                't_taxrate' => 0,
                                't_taxamt' => 0,
                                't_amtt' => $amtTaxed,
                                't_batchno' => $batchNo,
                                'T_KEYUSER' => Auth::user()->usercode,
                                'T_MODIUSER' => Auth::user()->usercode,
                                'T_KEYDATE' => Carbon::now(),
                                'T_MODIDATE' => Carbon::now(),
                        ];
                        $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );

                        $amt = 0;
                        $amtTaxed = 0;
                        $amt = $amt + $sr->amount;
                        $amtTaxed =  $amtTaxed  + $sr->amount;
                    }
                }else{
                    $amt = $amt + $sr->amount;
                    $amtTaxed =  $amtTaxed  + $sr->amount;
                }
            }

            $srDt[] = [
                't_docno' => $sr->doc_no == ''?null :  $sr->doc_no,
                'T_SN' =>$tSeq,
                'T_DRAC' => null,
                'T_ITEMNO' => $sr->item_code == ''?null : $sr->item_code,
                'T_SUBJECT' => $sr->subject == ''?null : $sr->subject,
                'T_DETAILS' => $sr->details == ''?null : $sr->details,
                'T_QTY' => $sr->qty == ''?null : $sr->qty,
                'T_UOM' => $sr->uom == ''?null : $sr->uom,
                'T_UPRICE' => $sr->uprice == ''?null : $sr->uprice,
                'T_AMT' => $sr->amount == ''?null : $sr->amount,
                't_taxcode' => $sr->tax_code == ''?null : $sr->tax_code,
                't_taxrate' => $sr->tax_rate == ''?null : $sr->tax_rate,
                't_taxamt' => $sr->tax_amount == ''?null : $sr->tax_amount,
                't_amtt' => $sr->amount == ''?null : $sr->amount,
                't_batchno' => $batchNo,
                'T_KEYUSER' => $sr->created_by == ''?null :  $sr->created_by,
                'T_MODIUSER' => $sr->updated_by == ''?null : $sr->updated_by,
                'T_KEYDATE' =>  $sr->created_at  == ''?null :  $sr->created_at ,
                'T_MODIDATE' => $sr->updated_at == ''?null : $sr->updated_at,
            ];

            $tSeq = str_pad( $tSeq + 5, 3, "0", STR_PAD_LEFT );
            $prevDoc = $sr->doc_no;
            $prevCatCode =  $sr->catCode;
            $prevSalesRet = $sr->salesRet;
            $count++;
        }
        $result = array_reduce($srDt, function($arr, $item) {
            if(!isset($arr[$item['t_docno'].$item['T_SUBJECT']])) {
                $arr[$item['t_docno'].$item['T_SUBJECT']] = $item;
            } else {
                $arr[$item['t_docno'].$item['T_SUBJECT']]['T_AMT'] += $item['T_AMT'];
                $arr[$item['t_docno'].$item['T_SUBJECT']]['t_amtt'] += $item['t_amtt'];
            }
            return $arr;
        });
        $result = array_values($result);
        foreach (array_chunk($result,1000) as $sr)
        {
            $connection->table('acc_cndt')->insert($sr);
        }

        if($isPost){
                DB::statement("
                UPDATE salesreturns
                SET posted = 'Y'
                WHERE deleted_at IS NULL AND docno IN ('$docNo')
            ");
        }
        return true;

        }catch(\Exception $e){
            return false;
        }
    }

}