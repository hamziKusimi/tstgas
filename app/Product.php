<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Stockcode;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code',
        'descr',
        'active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
