<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\DeliveryNote;
use App\Model\View\CylinderInvProcessing;
use App\Model\DeliveryNotedt;
use App\Model\CylinderInvoice;
use App\Model\CylinderInvoicedt;
use App\Model\DocumentSetup;
use Auth;

class CreateGasInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:creategasinvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $output = "No Invoice Generated";
        $deliveryNotes = DeliveryNote::join('debtors', 'delivery_notes.account_code', '=', 'debtors.accountcode')
            ->where(function ($query) {
                $query->whereNull('gas_invoice_date');
                $query->whereRaw('DATE_ADD(`date`, INTERVAL 3 DAY) >=NOW()');
            })->get([
                'dn_no',
                'account_code',
                'delivery_notes.name',
                'delivery_notes.addr1',
                'delivery_notes.addr2',
                'delivery_notes.addr3',
                'delivery_notes.addr4',
                'delivery_notes.tel_no',
                'delivery_notes.fax_no',
                'debtors.customval3',
                'debtors.cterm'
            ]);

        if (!$deliveryNotes->isEmpty()) {
            foreach ($deliveryNotes as $DN) {
                $amount = 0;
                $sequence = 1;

                //find DNdt
                $deliveryNotesDT = DeliveryNotedt::where('dn_no', '=', $DN->dn_no)->get();

                if (!$deliveryNotesDT->isEmpty()) {
                    $runningNumber = DocumentSetup::findByName('Sales Invoice');
                    $InvDoc = $runningNumber->nextRunningNoString;

                    foreach ($deliveryNotesDT as $DNDT) {
                        // make invoice child
                        $dt = CylinderInvoicedt::create([
                            'doc_no' => $InvDoc,
                            'sequence_no' => $sequence,
                            'account_code' =>  $DN->account_code,
                            // 'serial' =>  $cyl->serial,
                            // 'barcode' => $cyl->barcode,
                            'subject' => $DNDT->category . " " . $DNDT->product,
                            'details' => "Refer D.N. " .  $DNDT->dn_no,
                            'product' => $DNDT->product,
                            'type' => "gas",
                            'quantity' => $DNDT->qty,
                            'uom' => "Cyl",
                            'unit_price' =>  $DNDT->gas_price,
                            // 'discount' => $request['discount'][$i],
                            'amount' =>  $DNDT->gas_price * $DNDT->qty,
                            // 'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => "server"
                        ]);
                        $output = "Latest Invoice Generated at " . date("Y/m/d");
                        $amount = $amount + $DNDT->gas_price * $DNDT->qty;
                        $sequence++;
                    }

                    //make invoice master
                    $invoice = CylinderInvoice::create([
                        'docno' =>  $InvDoc,
                        'date' => date("Y-m-d"),
                        // 'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                        // 'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                        // 'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                        'amount' =>  $amount,
                        // 'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                        // 'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                        'account_code' => $DN->account_code,
                        'name' => $DN->name,
                        'addr1' => $DN->addr1,
                        'addr2' => $DN->addr2,
                        'addr3' => $DN->addr3,
                        'addr4' => $DN->addr4,
                        'tel_no' => $DN->tel_no,
                        'fax_no' => $DN->fax_no,
                        'credit_term' => $DN->cterm,
                        // 'header' => !empty($request['header']) ? $request['header'] : '',
                        'footer' => "Late payment charges at 1.5% P.M after due date",
                        // 'summary' => !empty($request['summary']) ? $request['summary'] : '',
                        // 'currency' => !empty($request['currency']) ? $request['currency'] : '',
                        'exchange_rate' =>  0.00,
                        'rounding' =>  0.00,
                        'created_by' => "server"
                    ]);

                    $invoiceDoc = DocumentSetup::findByName('Sales Invoice');
                    $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);
                    $delNotes = DeliveryNote::where('dn_no', '=', $DN->dn_no);
                    $delNotes->update(['gas_invoice_date' => date("Y-m-d")]);
                }
            }
        }

        \Log::info("working fine");
        $this->info("Command Run successfully $output ");
    }
}
