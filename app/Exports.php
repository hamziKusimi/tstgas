<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class Export implements FromView, WithEvents
{
    public $date;
    public $data;

    public function __construct($data,$date)
    {
        $this->date = $date;
        $this->data = $data;
    }

    public function view(): View
    {

        return view('reportprinting.inventory.kpdnhep.belian',[ 'item'=>$this->data, 'date'=>$this->date]);
    }

    public function registerEvents(): array
    {


        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '#000000'],
                        ],
                    ],
                ];

                $event->sheet->getDelegate()->mergeCells('A1:H1');
                $event->sheet->getStyle('A1:H1')->ApplyFromArray($styleArray);
            },
        ];
    }

}