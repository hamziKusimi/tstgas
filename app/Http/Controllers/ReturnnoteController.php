<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\View\ReturnNotePrintCylinderView;
use App\Model\View\ReturnNotePrintRackView;
use App\Model\ReturnNote;
use App\Model\ReturnNotedt;
use App\Model\ReturnNoteGRDT;
use App\Model\ReturnNoteLL;
use App\Model\ReturnNoteUL;
use App\Model\View\ReturnnoteMaster;
use App\Model\View\ReturnnotegrMaster;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylinderproduct;
use App\Model\Gasrack;
use App\Model\Gasracktype;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\DocumentSetup;
use App\Model\Uom;
use App\Model\Driver;
use App\Model\Salesman;
use Auth;
use PHPJasper\PHPJasper;
use DateTime;
use App\Model\CustomObject\Transaction;
use Illuminate\Http\Request;
use App\Model\SystemSetup;
use App\Model\DeliveryNotedt;
use App\Model\DeliveryNoteUL;
use App\Model\DeliveryNotepr;
use DB;
use App\Model\View\DeliveryNotePrintRackView;
use App\Model\Area;

class ReturnNoteController extends Controller
{
    public function index()
    {
        $data["returnnotes"] = ReturnNote::orderBy('dn_no', 'desc')->get();
        $data["docno_select"] = ReturnNote::pluck('dn_no', 'dn_no');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["page_title"] = "Return Note Listing";
        $data["bclvl1"] = "Return Note Listing";
        $data["bclvl1_url"] = route('returnnotes.index');
        return view('dailypro/returnnotes.index', $data);
    }

    public function api_get()
    {
        $resources = ReturnNote::orderBy('date')->get();

        return response()->json($resources);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $capacities = Cylinder::whereNotNull('capacity')->pluck('capacity', 'capacity');
        $pressures = Cylinder::whereNotNull('testpressure')->pluck('testpressure', 'testpressure');
        $serials = Cylinder::pluck('serial');
        $barcodes = Cylinder::whereNotNull('barcode')->get(['barcode', 'serial', 'cat_id', 'prod_id', 'capacity', 'testpressure']);
        $runningNumber = DocumentSetup::findByName('Return Note');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $cylindercat = Cylindercategory::pluck('code');
        $cylinderprod = Cylinderproduct::pluck('code');
        $gsbarcodes = Gasrack::get();
        $gasracktype = Gasracktype::pluck('code');
        $drivers = Driver::pluck('code', 'code');

        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['masterCodes'] = $masterCodes;
        $data['capacities'] = $capacities;
        $data['pressures'] = $pressures;
        $data['taxCodes'] = $taxCodes;
        $data["serials"] = $serials;
        $data["barcodes"] = $barcodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["cylindercat"] = $cylindercat;
        $data["cylinderprod"] = $cylinderprod;
        $data["gsbarcodes"] = $gsbarcodes;
        $data["gasracktype"] = $gasracktype;
        $data["drivers"] = $drivers;
        $data["page_title"] = "Add Return Note";
        $data["bclvl1"] = "Return Note Received Listing";
        $data["bclvl1_url"] = route('returnnotes.index');
        $data["bclvl2"] = "Add Return Note";
        $data["bclvl2_url"] = route('returnnotes.create');

        // return response()->json($data);

        return view('dailypro/returnnotes.create', $data);
    }

    public function store(Request $request)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';
        // dd($request['date']);
        $returnnoteid = ReturnNote::create([
            'dn_no' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'lpono' => $request['lpono'],
            'driver' => $request['driver'],
            'charge_type' => "default",
            // 'start_from' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'vehicle_no' => !empty($request['vehicleNo']) ? $request['vehicleNo'] : '',
            'created_by' => Auth::user()->id
        ]);

        if (count($request['sequence_no']) > 1) {
            for ($i = 1; $i < count($request['sequence_no']); $i++) {
                ReturnNotedt::create([
                    'dn_no' => $returnnoteid->dn_no,
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'sequence_no' => $request['sequence_no'][$i],
                    'category' => !empty($request['category'][$i]) ? $request['category'][$i] : $request['categorydt'][$i],
                    'product' => !empty($request['product'][$i]) ? $request['product'][$i] : $request['productdt'][$i],
                    'capacity' => !empty($request['capacity'][$i]) ? $request['capacity'][$i] : $request['capacitydt'][$i],
                    'pressure' => !empty($request['pressure'][$i]) ? $request['pressure'][$i] : $request['pressuredt'][$i],
                    'qty' => $request['quantity'][$i],
                    'price' => $request['price'][$i],
                    'created_by' => Auth::user()->id
                ]);
            }
        }

        if (count($request['grsequence_no']) > 1) {
            for ($i = 1; $i < count($request['grsequence_no']); $i++) {
                ReturnNoteGRDT::create([
                    'dn_no' => $returnnoteid->dn_no,
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'sequence_no' => $request['grsequence_no'][$i],
                    'type' => !empty($request['grtype'][$i]) ? $request['grtype'][$i] : $request['grtypedt'][$i],
                    'qty' => $request['grquantity'][$i],
                    'price' => $request['grprice'][$i],
                    'created_by' => Auth::user()->id
                ]);
            }
        }

        if (isset($request['sqn_no'])) {
            for ($i = 0; $i < count($request['sqn_no']); $i++) {
                $date = Carbon::createFromFormat('d/m/Y', $request['loadingdt'][$i]);
                $debtor = $request['account_code_mt'];
                $serial = $request['serialno'][$i];
                $barcode = $request['barcode'][$i];
                $DNset = $this->findDN($date, $serial, $barcode, $request['doc_no']);
                $rlll = ReturnNoteLL::create([
                    'dn_no' => $returnnoteid->dn_no,
                    'sequence_no' => $request['dt_sqn_no'][$i],
                    'sqn_no' => $request['sqn_no'][$i],
                    'datetime' => $request['loadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['loadingdt'][$i]) : null,
                    'driver' => $request['driverdt'][$i],
                    'serial' => $request['serialno'][$i],
                    'type' => $request['type'][$i],
                    'barcode' => $request['barcode'][$i],
                    'descr' => $request['descr'][$i],
                    'created_by' => Auth::user()->id
                ]);


                if (isset($request['uloadingdt'][$i])) {
                    // $date = Carbon::createFromFormat('d/m/Y', $request['uloadingdt'][$i]);
                    // $debtor = $request['account_code_mt'];
                    // $barcode = $request['barcode'][$i];
                    // $DNset = $this->findDN($date, $barcode, $debtor,  $request['doc_no']);
                    $rlul = ReturnNoteUL::create([
                        'dn_no' => $returnnoteid->dn_no,
                        'sequence_no' => $request['dt_sqn_no'][$i],
                        'sqn_no' => $request['sqn_no'][$i],
                        'datetime' => $request['uloadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['uloadingdt'][$i]) : null,
                        'driver' => $request['driverdt'][$i],
                        'serial' => $request['serialno'][$i],
                        'barcode' => $request['barcode'][$i],
                        'descr' => $request['descr'][$i],
                        'type' => $request['type'][$i],
                        'created_by' => Auth::user()->id
                    ]);
                }
            }
        }

        $invoiceDoc = DocumentSetup::findByName('Return Note');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        return redirect()->route('returnnotes.edit', ['id' => $returnnoteid])->with('Success', 'Return Note added sucessfully');
    }

    public function show(Type $var = null)
    {
        return view('dailypro/returnnotes.invoices', $data);
    }

    public function edit($id)
    {
        $returnnote = ReturnNote::find($id);
        // $masters = ReturnnoteMaster::where('dn_no', $returnnote->dn_no)->get();
        $contents = ReturnNote::where('dn_no', $returnnote->dn_no)->get();
        // return response()->json($returnnote->docno);
        $selectedDebitor = Debtor::findByAcode($returnnote->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $capacities = Cylinder::whereNotNull('capacity')->pluck('capacity', 'capacity');
        $pressures = Cylinder::whereNotNull('testpressure')->pluck('testpressure', 'testpressure');
        $barcodes = Cylinder::whereNotNull('barcode')->get(['barcode', 'serial', 'cat_id', 'prod_id', 'capacity', 'testpressure']);
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $cylindercat = Cylindercategory::pluck('code');
        $cylinderprod = Cylinderproduct::pluck('code');
        $gsbarcodes = Gasrack::get();
        $gasracktype = Gasracktype::pluck('code');
        $drivers = Driver::pluck('code', 'code');
        // $grcontents = ReturnNoteGRDT::where('dn_no', $returnnote->dn_no)->get();
        $grcontents = ReturnNote::where('dn_no', $returnnote->dn_no)->get();

        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['item'] = $returnnote;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['capacities'] = $capacities;
        $data['pressures'] = $pressures;
        $data['barcodes'] = $barcodes;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["cylindercat"] = $cylindercat;
        $data["cylinderprod"] = $cylinderprod;
        $data["gsbarcodes"] = $gsbarcodes;
        $data["gasracktype"] = $gasracktype;
        $data["drivers"] = $drivers;
        $data["grcontents"] = $grcontents;
        $data["page_title"] = "Edit Return Note";
        $data["bclvl1"] = "Return Note Received Listing";
        $data["bclvl1_url"] = route('returnnotes.index');
        $data["bclvl2"] = "Edit Return Note";
        $data["bclvl2_url"] = route('returnnotes.edit', ['id' => $id]);

        // return response()->json($grcontents);

        return view('dailypro/returnnotes.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $returnnote = Returnnote::find($id);
        if (isset($returnnote)) {
            $returnnote->update([
                'dn_no' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'lpono' => $request['lpono'],
                'driver' => $request['driver'],
                'charge_type' => $request['charge_type'],
                // 'start_from' => Carbon::createFromFormat('d/m/Y', $request['startfrom']),
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'vehicle_no' => !empty($request['vehicleNo']) ? $request['vehicleNo'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['sequence_no']) > 1) {
                for ($i = 1; $i < count($request['sequence_no']); $i++) {

                    $returnnotedt_id = Returnnotedt::where('dn_no', $request['doc_no'])
                        ->where('sequence_no', $request['sequence_no'][$i])->first();

                    if ($returnnotedt_id == null) {
                        Returnnotedt::create([
                            'dn_no' => $returnnote->dn_no,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'category' => !empty($request['category'][$i]) ? $request['category'][$i] : $request['categorydt'][$i],
                            'product' => !empty($request['product'][$i]) ? $request['product'][$i] : $request['productdt'][$i],
                            'capacity' => !empty($request['capacity'][$i]) ? $request['capacity'][$i] : $request['capacitydt'][$i],
                            'pressure' => !empty($request['pressure'][$i]) ? $request['pressure'][$i] : $request['pressuredt'][$i],
                            'qty' => $request['quantity'][$i],
                            'price' => $request['price'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }


            if (count($request['grsequence_no']) > 1) {
                for ($i = 1; $i < count($request['grsequence_no']); $i++) {

                    $returnnotegrdt_id = ReturnnoteGRDT::where('dn_no', $request['doc_no'])
                        ->where('sequence_no', $request['grsequence_no'][$i])->first();

                    if ($returnnotegrdt_id == null) {
                        ReturnnoteGRDT::create([
                            'dn_no' => $returnnote->dn_no,
                            'sequence_no' => $request['grsequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'type' => !empty($request['grtype'][$i]) ? $request['grtype'][$i] : $request['grtypedt'][$i],
                            'qty' => $request['grquantity'][$i],
                            'price' => $request['grprice'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }

            $data = ReturnNoteLL::where('dn_no', $request['doc_no'])
                ->orderBy('sqn_no', 'DESC')
                ->first('sqn_no');

            if ($data === null) {
                $data_sqn = 0;
            } else {
                $data_sqn = $data->sqn_no;
            }

            if (isset($request['sqn_no'])) {
                for ($i = 0; $i < count($request['sqn_no']); $i++) {

                    if (isset($request['loadingdt'][$i])) {
                        $date = Carbon::createFromFormat('d/m/Y', $request['loadingdt'][$i]);
                        $serial = $request['serialno'][$i];
                        $barcode = $request['barcode'][$i];
                        $debtor = $request['account_code_mt'];
                        $DNset = $this->findDN($date, $serial, $barcode, $returnnote->dn_no);
                        $rlll = ReturnNoteLL::create([
                            'dn_no' => $returnnote->dn_no,
                            'sequence_no' => $request['dt_sqn_no'][$i],
                            'sqn_no' => $request['sqn_no'][$i],
                            'datetime' => $request['loadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['loadingdt'][$i]) : null,
                            'driver' => $request['driverdt'][$i],
                            'serial' => $request['serialno'][$i],
                            'barcode' => $request['barcode'][$i],
                            'descr' => $request['descr'][$i],
                            'type' => $request['type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }

                    if (isset($request['uloadingdt'][$i])) {
                        // $date = $request['uloadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['uloadingdt'][$i]) : null;
                        // $barcode = $request['barcode'][$i];
                        // $debtor = $request['account_code_mt'];
                        // $DNset = $this->findDN($date, $barcode, $debtor, $returnnote->dn_no);

                        $rlul = ReturnNoteUL::create([
                            'dn_no' => $returnnote->dn_no,
                            'sequence_no' => $request['dt_sqn_no'][$i],
                            'sqn_no' => $request['sqn_no'][$i],
                            'datetime' => $request['uloadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['uloadingdt'][$i]) : null,
                            'driver' => $request['driverdt'][$i],
                            'serial' => $request['serialno'][$i],
                            'barcode' => $request['barcode'][$i],
                            'descr' => $request['descr'][$i],
                            'type' => $request['type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }

            if (isset($request['edit_ul_barcode']) && isset($request['edit_ul_date'])) {

                for ($i =  0; $i < count($request['edit_ul_barcode']); $i++) {

                    if ($request['edit_ul_date'][$i] != '' || $request['edit_ul_date'][$i] != null) {
                        // $date = Carbon::createFromFormat('d/m/Y', $request['edit_ul_datetime'][$i]);
                        // $barcode =  $request['edit_ul_barcode'][$i];
                        // $debtor = $request['account_code_mt'];
                        // $DNset = $this->findDN($date, $barcode, $debtor, $returnnote->dn_no);
                        $dlul = ReturnNoteUL::create([
                            'dn_no' => $returnnote->dn_no,
                            'sequence_no' => $request['edit_ul_sequence_no'][$i],
                            'sqn_no' => $request['edit_ul_sqn_no'][$i],
                            'datetime' => $request['edit_ul_datetime'][$i] ? Carbon::createFromFormat('d/m/Y', $request['edit_ul_date'][$i]) : null,
                            'driver' => $request['edit_ul_driver'][$i],
                            'serial' => $request['edit_ul_serial'][$i],
                            'barcode' => $request['edit_ul_barcode'][$i],
                            'descr' => $request['edit_ul_descr'][$i],
                            'type' => $request['edit_ul_type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }
        // dd($returnnotedt);
        return redirect()->route('returnnotes.edit', ['id' => $id])->with('Success', 'Return Note updated sucessfully');
    }

    public function destroyData($id)
    {
        $data = ReturnNotedt::find($id);
        $data_select = ReturnNotedt::select('dn_no', 'sequence_no')->where('id', $id)->first();
        $docno = $data_select->dn_no;
        $seq_no = $data_select->sequence_no;

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();

            $data_ul = ReturnNoteUL::where('dn_no', $docno)
                ->where('sequence_no', $seq_no);
            $data_ll = ReturnNoteLL::where('dn_no', $docno)
                ->where('sequence_no', $seq_no);

            if (isset($data_ul)) {
                $data_ul->update([
                    'deleted_by' => Auth::user()->id
                ]);

                $data_ul->delete();
            }

            if (isset($data_ll)) {
                $data_ll->update([
                    'deleted_by' => Auth::user()->id
                ]);

                $data_ll->delete();
            }

            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyDatalist(Request $request)
    {
        if ($request['only-UL']) {
            $data2 = ReturnNoteUL::where('dn_no', $request['dnno'])
                ->where('sequence_no', $request['sqnex'])
                ->where('sqn_no', $request['sqnlist'])
                ->first();
            $data2->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data2->delete();
        } else {
            $data = ReturnNoteLL::where('dn_no', $request['dnno'])
                ->where('sequence_no', $request['sqnex'])
                ->where('sqn_no', $request['sqnlist'])
                ->first();
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();

            $data3 = DeliveryNotepr::where('return_note', $request['dnno'])
                    ->where('serial', $request['serial'])
                    ->update(['return_note' => null,
                    'return_note_date' => null]);
        }

        return $request;
    }

    public function destroy($id)
    {

        $returnnote = ReturnNote::find($id);
        $returnnote->update([
            'deleted_by' => Auth::user()->id
        ]);
        $returnnote->delete();

        $returnnotedt = ReturnNotedt::where('dn_no', $returnnote->dn_no);
        $returnnotedt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $returnnotedt->delete();

        $returnnotegrdt = ReturnNoteGRDT::where('dn_no', $returnnote->dn_no);
        $returnnotegrdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $returnnotegrdt->delete();

        $returnnotell = ReturnNoteLL::where('dn_no', $returnnote->dn_no);
        $returnnotell->update([
            'deleted_by' => Auth::user()->id
        ]);
        $returnnotell->delete();

        $returnnoteul = ReturnNoteUL::where('dn_no', $returnnote->dn_no);
        $returnnoteul->update([
            'deleted_by' => Auth::user()->id
        ]);
        $returnnoteul->delete();
        return redirect()->route('returnnotes.index')->with('Success', 'Return Note deleted successfully');
    }

    public function destroyreturnnotedt($id)
    {
        $returnnotedt = ReturnNotedt::find($id);
        $returnnotedt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $returnnotedt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper(Request $request)
    {
        $Resources = ReturnNote::where('id', $request->id)->get();
        $ResourcesJsonList = $this->getReturnNoteListingJSON($Resources);

        $formattedResource = ReturnNote::select(
            'dn_no as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'charge_type as CH_TYPE',
            'lpono as LPO_NO',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'driver as DRIVER',
            'start_from as START_FROM',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $request->id)->first();
        $CTERM = Debtor::Select('cterm')
            ->where('accountcode', '=',  $formattedResource->ACC_CODE)->first();

            $source = 'ReturnNote';
            $jrxml = 'general-bill-Return-Notes.jrxml';
            $reportTitle = 'CYLINDER RETURN RECORD';

        Transaction::generateBillReturnNote(
            $formattedResource,
            $CTERM->cterm,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList),
            $request->type
        );
    }

    public function print(Request $request)
    {
        $ReturnNote = $this->getReturnNoteQuery($request);

        if (empty($ReturnNote)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $ReturnNoteJSON = $this->getReturnNoteListingJSON($ReturnNote);

        // check if JSON empty \\
        $RNJSON = json_decode($ReturnNoteJSON);
        if (empty($RNJSON->data)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        // check if JSON empty \\

        $JsonFileName = "ReturnNote" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/DailyProcess/ReturnNotes/' . $JsonFileName . '.json'), $ReturnNoteJSON);
        $JasperFileName = "ReturnNote" . date("Ymdhisa") . Auth::user()->id;

        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/ReturnNotes/ReturnNoteListing.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/ReturnNotes/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/ReturnNotes/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/ReturnNotes/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getReturnNoteQuery(Request $request)
    {
        return ReturnNote::select('dn_no', 'date', 'account_code', 'name')
            ->where(function ($query) use ($request) {

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('dn_no', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $query->where('lpono', "=", $request->LPO_frm_1);
                    } else {
                        $query->whereBetween('lpono', [$request->LPO_frm_1, $request->LPO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('dn_no')
            ->get();
    }

    public function getReturnNoteListingJSON($ReturnNote)
    {
        $dataArray = array();
        foreach ($ReturnNote as $RN) {

            $RNTrans = DB::select(DB::raw("
                SELECT GROUP_CONCAT(a.`serial`) AS s_no, a.type AS `type`,
                CASE
                    WHEN a.type = 'cy' THEN CONCAT(IFNULL(e.code,''), ' ',IFNULL(d.code,''))
                    ELSE 'Rack'
                END AS `subject`,
                COUNT(a.`serial`) AS total
                FROM return_notells a
                LEFT JOIN cylinders b ON b.`serial` = a.`serial`
                LEFT JOIN gasracks c ON c.`serial` = a.`serial`
                LEFT JOIN cylindercategories d ON b.cat_id = d.id
                LEFT JOIN cylindergroups e ON b.group_id = e.id
                WHERE dn_no = '$RN->dn_no'
                GROUP BY `subject`, a.type;
            "), []);

            $s_n = 1;
            foreach ($RNTrans as $trans) {

                $objectJSON['s_n'] = $s_n;
                $objectJSON['dn_no'] = $RN->dn_no;
                $objectJSON['subject'] = str_replace(' ', '', $trans->subject) == '' ? "Undefined" : $trans->subject;
                $objectJSON['details'] =  "<pre>" . str_replace(',', '          ', $trans->s_no) . "</pre>";
                $objectJSON['quantity'] = $trans->total;
                $objectJSON['type'] =  $trans->subject == "Rack" ? "RACK" : "CYL";

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function findDN($date, $serial, $barcode, $returnNote)
    {
        $Cylinder = DeliveryNotepr::whereRaw("(`serial` = '$serial' or barcode = '$barcode') ")
        ->whereNull('return_note')
        ->get();

        if (!$Cylinder->isEmpty()) {
            $Cyl = DeliveryNotepr::whereRaw("(`serial` = '$serial' or barcode = '$barcode') ")
            ->whereNull('return_note')
                ->update([
                    'return_note' => $returnNote,
                    'return_note_date' => $date
                ]);
            return true;
        }
        return true;
    }
}
