<?php

namespace App\Http\Controllers;

use App\Model\Debtor;
use App\Model\View\Gasrack;
use App\Model\View\RackCustomerActivity;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;

class GasrackActivityController extends Controller
{
    public function print(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $RackActivity = $this->getRackActivityQuery($request, $dates_from, $dates_to);

        if ($RackActivity->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $RackJSON = $this->getRackActivityJSON($RackActivity);

        $JsonFileName = "RackCustomerActivity" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/RackCustomerActivity/' . $JsonFileName . '.json'), $RackJSON);
        $JasperFileName = "RackCustomerActivity" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to)
    {
        $input = base_path() . '/resources/reporting/RackCustomerActivity/RackCustomerActivity.jrxml';
        $output = base_path() . '/resources/reporting/RackCustomerActivity/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/RackCustomerActivity/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date_from" => date('d/m/Y', strtotime($dates_from)),
                "date_to" => date('d/m/Y', strtotime($dates_to))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/RackCustomerActivity/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getRackActivityQuery($request, $dates_from, $dates_to)
    {
        return RackCustomerActivity::where(function ($query) use ($request, $dates_from, $dates_to) {
            if ($dates_from == $dates_to) {
                $query->whereDate('cdate', '=', $dates_from);
            } else {
                $query->whereBetween('cdate', [$dates_from, $dates_to]);
            }

            if ($request->docno_Chkbx == "on") {
                $query->whereBetween('doc_no', [$request->docno_frm, $request->docno_to])
                    ->get();
            }

            if ($request->grserial_Chkbx == "on") {
                $query->whereBetween('s_no', [$request->grserial_frm, $request->grserial_to])
                    ->get();
            }

            if ($request->debCode_Chkbx == "on") {
                $query->whereBetween('deb_code', [$request->debCode_frm, $request->debCode_to])
                    ->get();
            }

            if($request->printtype == "outstanding"){
                
                $query->where('returncy', '=', NULL);

            }elseif($request->printtype == "returned"){

                $query->where('returncy', '<>', NULL);

            }
            
        })->orderBy('cdate')->get();
    }

    public function getRackActivityJSON($RackActivity)
    {
        $dataArray = [];
        $totDelivery = 0;
        $totReturn = 0;
        foreach ($RackActivity as $rack) {
            $objectJSON =   [];

            $objectJSON['rack_no'] = $rack->s_no;
            $objectJSON['doc_type'] = $rack->doc_type;
            $objectJSON['doc_no'] = $rack->doc_no;
            $objectJSON['debtor'] = $rack->deb_code . " " . $rack->deb_name;
            $objectJSON['date'] = date('d/m/Y', strtotime($rack->cdate));

            $totDelivery += $rack->doc_type == 'DN' ? 1 : 0;
            $totReturn += $rack->doc_type == 'RN' ? 1 : 0;
            $objectJSON['tot_dn'] = $totDelivery;
            $objectJSON['tot_rn'] = $totReturn;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
