<?php

namespace App\Http\Controllers;

use App\Model\Stockcode;
use App\Model\Category;
use App\Model\Product;
use App\Model\Brand;
use App\Model\Location;
use App\Model\Uom;
use App\Model\View\StockBalance;
use App\Model\View\StockIn;
use App\Model\PrintedIndexView;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;
use Carbon\Carbon;

class StockValueController extends Controller
{
    //
    public function printFIFO(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Stock Value FIFO')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Stock Value FIFO')->update([
                'index' => "Stock Value FIFO",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Stock Value FIFO",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        $stockValue = $this->getFIFOQuery($request);
        if ($stockValue->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $stocksJSON = $this->getStockValJSON($stockValue, $request);

        // check if JSON empty \\
        $stkJSON = json_decode($stocksJSON);
        if (empty($stkJSON->data)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        // check if JSON empty \\

        $JsonFileName = "StockValue" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/StockValue/FIFO/' . $JsonFileName . '.json'), $stocksJSON);
        $JasperFileName = "StockValue" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($request, $JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($request, $JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockValue/FIFO/StockValue.jrxml';
        $output = base_path() . '/resources/reporting/StockValue/FIFO/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockValue/FIFO/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date" => date('d/m/Y', strtotime($request->date))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockValue/FIFO/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getFIFOQuery($request)
    {
        return StockIn::join('stockcodes', 'view_stock_in.item_code', '=', 'stockcodes.code')
            ->select(
                DB::raw('item_code, SUM(totalqty) as stock_bal')
            )
            ->whereDate('t_date', '<=', date('Y-m-d', strtotime($request->date)))
            ->where(function ($query) use ($request) {

                if ($request->Active_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 0);
                }
                if ($request->Inactive_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 1);
                }
                if ($request->STK_Code_Chkbx == "on") {
                    $query->whereBetween('item_code', [$request->STK_Code_frm, $request->STK_Code_to]);
                }
                if ($request->Category_Chkbx == "on") {
                    $categoryidfrm = Category::select('id')->where('code', $request->Category_frm)->first();
                    $categoryidto = Category::select('id')->where('code', $request->Category_to)->first();
                    if ($categoryidfrm != null && $categoryidto != null) {
                        $cat_id_frm = $categoryidfrm->id;
                        $cat_id_to = $categoryidto->id;
                    } else {
                        $cat_id_frm = null;
                        $cat_id_to = null;
                    }
                    $query->whereBetween('stockcodes.cat_id', [$cat_id_frm, $cat_id_to]);
                }
                if ($request->Product_Chkbx == "on") {
                    $productidfrm = Product::select('id')->where('code', $request->Product_frm)->first();
                    $productidto = Product::select('id')->where('code', $request->Product_to)->first();
                    if ($productidfrm  != null && $productidto != null) {
                        $prod_id_frm = $productidfrm->id;
                        $prod_id_to = $productidto->id;
                    } else {
                        $prod_id_frm = null;
                        $prod_id_to = null;
                    }
                    $query->whereBetween('stockcodes.prod_id', [$prod_id_frm, $prod_id_to]);
                }
                if ($request->Brand_Chkbx == "on") {
                    $brandidfrm = Brand::select('id')->where('code', $request->Brand_frm)->first();
                    $brandidto = Brand::select('id')->where('code', $request->Brand_to)->first();
                    if ($brandidfrm  != null && $brandidto != null) {
                        $brand_id_frm = $brandidfrm->id;
                        $brand_id_to = $brandidto->id;
                    } else {
                        $brand_id_frm = null;
                        $brand_id_to = null;
                    }
                    $query->whereBetween('stockcodes.brand_id', [$brand_id_frm, $brand_id_to]);
                }
                if ($request->Location_Chkbx == "on") {
                    $locationidfrm = Location::select('id')->where('code', $request->Location_frm)->first();
                    $locationidto = Location::select('id')->where('code', $request->Location_to)->first();
                    if ($locationidfrm != null && $locationidto != null) {
                        $location_id_frm = $locationidfrm->id;
                        $location_id_to = $locationidto->id;
                    } else {
                        $location_id_frm = null;
                        $location_id_to = null;
                    }
                    $query->whereBetween('stockcodes.loc_id', [$location_id_frm, $location_id_to]);
                }
            })
            ->groupBy('item_code')
            ->get();
    }

    public function getStockValJSON($stockLedger, $request)
    {
        $dataArray = array();
        $s_n = 1;

        foreach ($stockLedger as $stockLed) {

            $totalOut = StockBalance::selectRaw('SUM(totalqty) as totalout')
                ->whereDate('t_date', '<=', date('Y-m-d', strtotime($request->date)))
                ->where('item_code',   $stockLed->item_code)
                ->Where(function ($query) {
                    $query->orWhere('t_type',   "INV")
                        ->orWhere('t_type',   "DO")
                        ->orWhere('t_type',   "PR")
                        ->orWhere('t_type',   "AOUT")
                        ->orWhere('t_type',   "CB");
                })
                ->groupBy('item_code')
                ->first();

            if ($totalOut == null) {
                $OutStock = 0;
            } else {
                $OutStock = $totalOut->totalout;
            }

            $stockTransaction = StockIn::selectRaw('item_code, doc_no, t_date, uom, ucost, SUM(totalqty) as totalquantity')
                ->whereDate('t_date', '<=', date('Y-m-d', strtotime($request->date)))
                ->where('item_code',   $stockLed->item_code)
                ->groupBy('item_code', 't_date', 'type_seq', 'doc_no', 'ucost', 'uom')
                ->orderBy('t_date')
                ->orderBy('type_seq')
                ->orderBy('doc_no')
                ->get();

            $item_no = 1;
            foreach ($stockTransaction as $stockTrans) {
                if ($item_no == 1) {
                    $totOut = $stockTrans->totalquantity +  $OutStock;
                    if ($totOut > 0) {
                        $objectJSON = [];
                        $objectJSON['stock_code'] =  $stockTrans->item_code;
                        $objectJSON['s_n'] = $s_n;
                        $stockCodes = Stockcode::select('descr', 'loc_id')->where('code', $stockTrans->item_code)->first();
                        $objectJSON['description'] = $stockCodes->descr;
                        $objectJSON['doc_no'] = $stockTrans->doc_no;
                        $objectJSON['date'] = date('d/m/Y', strtotime($stockTrans->t_date));
                        $stockLoc = Location::select('code')->where('id', $stockCodes->loc_id)->first();
                        $objectJSON['location'] = $stockLoc->code;
                        $objectJSON['uom'] = $stockTrans->uom;
                        $objectJSON['unit_cost'] = $stockTrans->ucost;
                        $objectJSON['in'] = $stockTrans->totalquantity;
                        $objectJSON['out'] = abs($OutStock);
                        $objectJSON['balance'] = $objectJSON['in'] - $objectJSON['out'];
                        $objectJSON['stock_value'] = round(($objectJSON['unit_cost'] * $objectJSON['balance']), 2);

                        $s_n =  $s_n + 1;

                        $dataArray[] = collect($objectJSON);
                    }
                } else {
                    if ($totOut <= 0) {
                        $OutStock = $totOut;
                        $totOut = $stockTrans->totalquantity + $totOut;
                    } else {
                        $OutStock = 0;
                    }
                    if ($totOut > 0) {
                        $objectJSON['stock_code'] =  $stockTrans->item_code;
                        $objectJSON['s_n'] = $s_n;
                        $stockCodes = Stockcode::select('descr', 'loc_id')->where('code', $stockTrans->item_code)->first();
                        $objectJSON['description'] = $stockCodes->descr;
                        $objectJSON['doc_no'] = $stockTrans->doc_no;
                        $objectJSON['date'] = date('d/m/Y', strtotime($stockTrans->t_date));
                        $stockLoc = Location::select('code')->where('id', $stockCodes->loc_id)->first();
                        $objectJSON['location'] = $stockLoc->code;
                        $objectJSON['uom'] = $stockTrans->uom;
                        $objectJSON['unit_cost'] = $stockTrans->ucost;
                        $objectJSON['in'] = $stockTrans->totalquantity;
                        $objectJSON['out'] = abs($OutStock);
                        $objectJSON['balance'] = $objectJSON['in'] - $objectJSON['out'];
                        $objectJSON['stock_value'] = round(($objectJSON['unit_cost'] * $objectJSON['balance']), 2);

                        $s_n =  $s_n + 1;

                        $dataArray[] = collect($objectJSON);
                    }
                }
                $item_no =  $item_no + 1;
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function printASAT(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Stock Value ASAT')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Stock Value ASAT')->update([
                'index' => "Stock Value ASAT",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Stock Value ASAT",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        if ($request->method == "LC") {
            $dateFirstDay = "1/" . $request->date;
            $dateLastDay = date('Y-m-t', strtotime(date_format(date_create($dateFirstDay), 'd/m/Y')));

            $stockValue = $this->getASATQuery($request, $dateLastDay);
            if ($stockValue->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }
            $stocksJSON = $this->getStockValJSON_asat($stockValue, $request, $dateLastDay, $dateFirstDay);
        } else {

            $dateDay = date('Y-m-d', strtotime($request->dateasat));
            $stockValue = $this->getASATQuery($request, $dateDay);
            if ($stockValue->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }

            $stocksJSON = $this->getStockValJSON_asat_wac($stockValue, $request, $dateDay, $dateDay);
        }

        $JsonFileName = "StockValue" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/StockValue/Asat/' . $JsonFileName . '.json'), $stocksJSON);
        $JasperFileName = "StockValue" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReportAsat($request, $JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReportAsat($request, $JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockValue/Asat/StockValue.jrxml';
        $output = base_path() . '/resources/reporting/StockValue/Asat/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockValue/Asat/' . $JsonFileName . '.json';
        if ($request->method == "LC") {
            $report_date = "term " . $request->date;
        } else {
            $report_date = date('d/m/Y', strtotime($request->dateasat));
        }
        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date" =>  $report_date
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockValue/Asat/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getASATQuery($request, $dateDay)
    {
        return StockBalance::join('stockcodes', 'view_stock_balance.item_code', '=', 'stockcodes.code')
            ->select(
                DB::raw('item_code, SUM(totalqty) as stock_bal')
            )
            ->whereDate('t_date', '<=',  $dateDay)
            ->where(function ($query) use ($request) {

                if ($request->Active_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 0);
                }
                if ($request->Inactive_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 1);
                }
                if ($request->STK_Code_Chkbx == "on") {
                    $query->whereBetween('item_code', [$request->STK_Code_frm, $request->STK_Code_to]);
                }
                if ($request->Category_Chkbx == "on") {
                    $categoryidfrm = Category::select('id')->where('code', $request->Category_frm)->first();
                    $categoryidto = Category::select('id')->where('code', $request->Category_to)->first();
                    if ($categoryidfrm != null && $categoryidto != null) {
                        $cat_id_frm = $categoryidfrm->id;
                        $cat_id_to = $categoryidto->id;
                    } else {
                        $cat_id_frm = null;
                        $cat_id_to = null;
                    }
                    $query->whereBetween('stockcodes.cat_id', [$cat_id_frm, $cat_id_to]);
                }
                if ($request->Product_Chkbx == "on") {
                    $productidfrm = Product::select('id')->where('code', $request->Product_frm)->first();
                    $productidto = Product::select('id')->where('code', $request->Product_to)->first();
                    if ($productidfrm  != null && $productidto != null) {
                        $prod_id_frm = $productidfrm->id;
                        $prod_id_to = $productidto->id;
                    } else {
                        $prod_id_frm = null;
                        $prod_id_to = null;
                    }
                    $query->whereBetween('stockcodes.prod_id', [$prod_id_frm, $prod_id_to]);
                }
                if ($request->Brand_Chkbx == "on") {
                    $brandidfrm = Brand::select('id')->where('code', $request->Brand_frm)->first();
                    $brandidto = Brand::select('id')->where('code', $request->Brand_to)->first();
                    if ($brandidfrm  != null && $brandidto != null) {
                        $brand_id_frm = $brandidfrm->id;
                        $brand_id_to = $brandidto->id;
                    } else {
                        $brand_id_frm = null;
                        $brand_id_to = null;
                    }
                    $query->whereBetween('stockcodes.brand_id', [$brand_id_frm, $brand_id_to]);
                }
                if ($request->Location_Chkbx == "on") {
                    $locationidfrm = Location::select('id')->where('code', $request->Location_frm)->first();
                    $locationidto = Location::select('id')->where('code', $request->Location_to)->first();
                    if ($locationidfrm != null && $locationidto != null) {
                        $location_id_frm = $locationidfrm->id;
                        $location_id_to = $locationidto->id;
                    } else {
                        $location_id_frm = null;
                        $location_id_to = null;
                    }
                    $query->whereBetween('stockcodes.loc_id', [$location_id_frm, $location_id_to]);
                }
            })
            ->groupBy('item_code')
            ->get();
    }

    public function getStockValJSON_asat($stockLedger, $request, $dateDay, $dateFirstDay)
    {
        $dataArray = array();
        $s_n = 1;
        foreach ($stockLedger as $stockLed) {
            $objectJSON = [];

            //Get s/n Column
            $objectJSON['s_n'] = $s_n;

            //Get Code Column
            $objectJSON['stock_code'] =  $stockLed->item_code;

            //Get description Column
            $stockCodes = Stockcode::select('descr', 'loc_id')->where('code', $stockLed->item_code)->first();
            $objectJSON['description'] = $stockCodes->descr;

            //Get location Column
            $stockLoc = Location::select('code')->where('id', $stockCodes->loc_id)->first();
            $objectJSON['location'] = $stockLoc->code;

            $term = explode("/", $request->date);
            $month = $term[0];
            $year = $term[1];
            $stk = StockIn::selectRaw('uom, ucost')
                ->where(function ($query) use ($stockLed, $dateDay) {
                    $query->where('item_code',   $stockLed->item_code);
                    $query->where('t_type',  'GR');
                    $query->whereDate('t_date', '<=',  $dateDay);
                })
                ->orderBy('t_date','DESC')
                ->first();

            $objectJSON['unit_cost'] = $stk->ucost;
            $objectJSON['uom'] = $stk->uom;

            $stockOpening = StockBalance::selectRaw('SUM(totalqty) as opening')
                ->whereraw("YEAR(t_date) = YEAR(STR_TO_DATE('$dateFirstDay', '%d/%m/%Y') - INTERVAL 1 MONTH) AND MONTH(t_date) = MONTH(STR_TO_DATE('$dateFirstDay', '%d/%m/%Y') - INTERVAL 1 MONTH)")
                ->where('item_code',   $stockLed->item_code)
                ->groupBy('item_code')
                ->first();

            if ($stockOpening == null) {
                $objectJSON['opening'] = 0;
            } else {
                $objectJSON['opening'] = $stockOpening->opening;
            }

            $TotalIn = StockIn::selectRaw('SUM(totalqty) as totalin')
                ->whereRaw("MONTH(t_date)='$month' AND YEAR(t_date) ='$year'")
                ->where('item_code',   $stockLed->item_code)
                ->groupBy('item_code')
                ->first();

            if ($TotalIn == null) {
                $objectJSON['total_in'] = 0;
            } else {
                $objectJSON['total_in'] = $TotalIn->totalin;
            }

            $TotalOut = StockBalance::selectRaw('SUM(totalqty) as totalout')
                ->whereRaw("MONTH(t_date)='$month' AND YEAR(t_date) ='$year'")
                ->where('item_code',   $stockLed->item_code)
                ->Where(function ($query) {
                    $query->orWhere('t_type',   "INV")
                        ->orWhere('t_type',   "DO")
                        ->orWhere('t_type',   "PR")
                        ->orWhere('t_type',   "AOUT")
                        ->orWhere('t_type',   "CB");
                })
                ->groupBy('item_code')
                ->first();

            if ($TotalOut == null) {
                $objectJSON['total_out'] = 0;
            } else {
                $objectJSON['total_out'] = abs($TotalOut->totalout);
            }

            $objectJSON['balance'] = $objectJSON['opening'] + $objectJSON['total_in'] - $objectJSON['total_out'];

            $objectJSON['stock_value'] =  round(($objectJSON['unit_cost'] * $objectJSON['balance']), 2);

            $s_n =  $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getStockValJSON_asat_wac($stockLedger, $request, $dateDay)
    {
        $dataArray = array();
        $s_n = 1;
        foreach ($stockLedger as $stockLed) {
            $objectJSON = [];

            //Get s/n Column
            $objectJSON['s_n'] = $s_n;

            //Get Code Column
            $objectJSON['stock_code'] =  $stockLed->item_code;

            //Get description Column
            $stockCodes = Stockcode::select('descr', 'loc_id', 'uom_id1')->where('code', $stockLed->item_code)->first();
            $objectJSON['description'] = $stockCodes->descr;

            //Get location Column
            $stockLoc = Location::select('code')->where('id', $stockCodes->loc_id)->first();
            $objectJSON['location'] = $stockLoc->code;

            //Get uom Column
            $stockLoc = Uom::select('code')->where('id', $stockCodes->uom_id1)->first();
            $objectJSON['uom'] = $stockLoc->code;

            $StockIn = StockIn::selectRaw('item_code, ucost, t_date, sum(totalqty) as totalQuantity')
                ->whereDate('t_date', '<=',  $dateDay)
                ->where('item_code',   $stockLed->item_code)
                ->groupBy('item_code', 't_type', 't_date', 'ucost')
                ->orderBy('t_date')
                ->get();

            $i = 0;
            $tot_qty = 0;
            $cost_multply_qty = [];
            foreach ($StockIn as $stk) {
                $tot_qty = $tot_qty + $stk->totalQuantity;
                $cost_multply_qty[$i] = $stk->totalQuantity * $stk->ucost;
                $i = $i + 1;
            }

            $objectJSON['unit_cost'] = round(array_sum($cost_multply_qty) / $tot_qty, 2);

            $stockOpening = StockBalance::selectRaw('sum(totalqty) as totalQuantity')
                ->whereDate('t_date', '<',  $dateDay)
                ->where('item_code',   $stockLed->item_code)
                ->groupBy('item_code')
                ->first();

            if ($stockOpening == null) {
                $objectJSON['opening'] = 0;
            } else {
                $objectJSON['opening'] = $stockOpening->totalQuantity;
            }


            $TotalIn = StockIn::selectRaw('SUM(totalqty) as totalin')
                ->whereDate('t_date', '=',  $dateDay)
                ->where('item_code',   $stockLed->item_code)
                ->groupBy('item_code')
                ->first();

            if ($TotalIn == null) {
                $objectJSON['total_in'] = 0;
            } else {
                $objectJSON['total_in'] = $TotalIn->totalin;
            }

            $TotalOut = StockBalance::selectRaw('SUM(totalqty) as totalout')
                ->whereDate('t_date', '=',  $dateDay)
                ->where('item_code',   $stockLed->item_code)
                ->Where(function ($query) {
                    $query->orWhere('t_type',   "INV")
                        ->orWhere('t_type',   "DO")
                        ->orWhere('t_type',   "PR")
                        ->orWhere('t_type',   "AOUT")
                        ->orWhere('t_type',   "CB");
                })
                ->groupBy('item_code')
                ->first();

            if ($TotalOut == null) {
                $objectJSON['total_out'] = 0;
            } else {
                $objectJSON['total_out'] = abs($TotalOut->totalout);
            }

            $objectJSON['balance'] = $objectJSON['opening'] + $objectJSON['total_in'] - $objectJSON['total_out'];

            $objectJSON['stock_value'] =  round(($objectJSON['unit_cost'] * $objectJSON['balance']), 2);

            $s_n =  $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
