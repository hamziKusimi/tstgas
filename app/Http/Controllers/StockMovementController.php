<?php

namespace App\Http\Controllers;

use App\Model\Stockcode;
use App\Model\Category;
use App\Model\Product;
use App\Model\Brand;
use App\Model\Location;
use App\Model\Uom;
use App\Model\View\StockBalance;
use App\Model\PrintedIndexView;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;
use Carbon\Carbon;

class StockMovementController extends Controller
{
    //
    public function printStkMovement(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Stock Movement')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Stock Movement')->update([
                'index' => "Stock Movement",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Stock Movement",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $stockMovement = $this->getStkMoveQuery($request, $dates_from, $dates_to);
        if ($stockMovement->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $stocksJSON = $this->getStockMoveJSON($stockMovement, $request, $dates_from, $dates_to);

        $JsonFileName = "StockMovement" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/StockMovement/' . $JsonFileName . '.json'), $stocksJSON);
        $JasperFileName = "StockMovement" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to)
    {
        $input = base_path() . '/resources/reporting/StockMovement/StockMovement.jrxml';
        $output = base_path() . '/resources/reporting/StockMovement/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockMovement/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date_from" => date('d/m/Y', strtotime($dates_from)),
                "date_to" => date('d/m/Y', strtotime($dates_to))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockMovement/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getStkMoveQuery($request, $dates_from, $dates_to)
    {
        return StockBalance::join('stockcodes', 'view_stock_balance.item_code', '=', 'stockcodes.code')
            ->select(DB::raw('item_code, uom, count(view_stock_balance.t_type) as countt_type, SUM(totalqty) as stock_bal'))
            ->where(function ($query) use ($request, $dates_from, $dates_to) {

                if ($request->Active_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 0);
                }
                if ($request->Inactive_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 1);
                }
                if ($request->STK_Code_Chkbx == "on") {
                    $query->whereBetween('item_code', [$request->STK_Code_frm, $request->STK_Code_to]);
                }
                if ($request->Category_Chkbx == "on") {
                    $categoryidfrm = Category::select('id')->where('code', $request->Category_frm)->first();
                    $categoryidto = Category::select('id')->where('code', $request->Category_to)->first();
                    if ($categoryidfrm != null && $categoryidto != null) {
                        $cat_id_frm = $categoryidfrm->id;
                        $cat_id_to = $categoryidto->id;
                    } else {
                        $cat_id_frm = null;
                        $cat_id_to = null;
                    }
                    $query->whereBetween('stockcodes.cat_id', [$cat_id_frm, $cat_id_to]);
                }
                if ($request->Product_Chkbx == "on") {
                    $productidfrm = Product::select('id')->where('code', $request->Product_frm)->first();
                    $productidto = Product::select('id')->where('code', $request->Product_to)->first();
                    if ($productidfrm  != null && $productidto != null) {
                        $prod_id_frm = $productidfrm->id;
                        $prod_id_to = $productidto->id;
                    } else {
                        $prod_id_frm = null;
                        $prod_id_to = null;
                    }
                    $query->whereBetween('stockcodes.prod_id', [$prod_id_frm, $prod_id_to]);
                }
                if ($request->Brand_Chkbx == "on") {
                    $brandidfrm = Brand::select('id')->where('code', $request->Brand_frm)->first();
                    $brandidto = Brand::select('id')->where('code', $request->Brand_to)->first();
                    if ($brandidfrm  != null && $brandidto != null) {
                        $brand_id_frm = $brandidfrm->id;
                        $brand_id_to = $brandidto->id;
                    } else {
                        $brand_id_frm = null;
                        $brand_id_to = null;
                    }
                    $query->whereBetween('stockcodes.brand_id', [$brand_id_frm, $brand_id_to]);
                }
                if ($request->Location_Chkbx == "on") {
                    $locationidfrm = Location::select('id')->where('code', $request->Location_frm)->first();
                    $locationidto = Location::select('id')->where('code', $request->Location_to)->first();
                    if ($locationidfrm != null && $locationidto != null) {
                        $location_id_frm = $locationidfrm->id;
                        $location_id_to = $locationidto->id;
                    } else {
                        $location_id_frm = null;
                        $location_id_to = null;
                    }
                    $query->whereBetween('stockcodes.loc_id', [$location_id_frm, $location_id_to]);
                }
            })
            ->groupBy('item_code')
            ->get();
    }

    public function getStockMoveJSON($stockMovement, $request, $dates_from, $dates_to)
    {
        $dataArray = array();

        $s_n = 1;
        foreach ($stockMovement as $stockMove) {
            $objectJSON = [];

            //Get S/N Column
            $objectJSON['s_n'] = $s_n;

            //Get Code Column
            $objectJSON['stock_code'] =  $stockMove->item_code;

            //Get description Column
            $stockCodes = Stockcode::select('descr')->where('code', $stockMove->item_code)->first();
            $objectJSON['description'] = $stockCodes->descr;

            $stock = Stockcode::select('descr', 'loc_id', 'uom_id1')
                ->where('code', $stockMove->item_code)
                ->first();

            //Get Location Column
            $locationQuery = Location::select('code')->where('id', $stock->loc_id)->first();
            $objectJSON['location'] = $locationQuery->code;

            $stockOpening = StockBalance::selectRaw('SUM(totalqty) as opening')
                ->whereDate('t_date', '<', $dates_from)
                ->where('item_code',   $stockMove->item_code)
                ->groupBy('item_code')
                ->first();

            //Get opening Column
            if ($stockOpening == null) {
                $objectJSON['opening'] = 0;
            } else {
                $objectJSON['opening'] = $stockOpening->opening;
            }

            $stockTransaction = StockBalance::selectRaw('item_code, t_type, doc_no, SUM(totalqty) as totalquantity, t_date')
                ->where(function ($query) use ($stockMove, $dates_from, $dates_to) {
                    if ($dates_from == $dates_to) {
                        $query->where('t_date', '=', $dates_from);
                    } else {
                        $query->whereBetween('t_date', [$dates_from, $dates_to]);
                    }
                    $query->where('item_code',   $stockMove->item_code);
                })
                ->groupBy('item_code', 't_type', 'doc_no', 'type_seq', 't_date')
                ->orderBy('type_seq')
                ->get();

            //Get in/out
            $i = 1;
            $objectJSON['in'] = 0;
            $objectJSON['adj_in'] = 0;
            $objectJSON['s_r'] = 0;
            $objectJSON['out'] = 0;
            $objectJSON['p_r'] = 0;
            $objectJSON['adj_in'] = 0;
            $objectJSON['adj_out'] = 0;
            foreach ($stockTransaction as $stockTrans) {
                if ($stockTrans->t_type == "GR") {
                    $objectJSON['in'] = $objectJSON['in'] + $stockTrans->totalquantity;
                } elseif ($stockTrans->t_type == "AIN") {
                    $objectJSON['adj_in'] =  $objectJSON['adj_in'] + $stockTrans->totalquantity;
                } elseif ($stockTrans->t_type == "SR") {
                    $objectJSON['s_r'] = $objectJSON['s_r'] + $stockTrans->totalquantity;
                } elseif ($stockTrans->t_type == "INV" || $stockTrans->t_type == "DO" || $stockTrans->t_type == "CB") {
                    $objectJSON['out'] = $objectJSON['out'] + abs($stockTrans->totalquantity);
                } elseif ($stockTrans->t_type == "PR") {
                    $objectJSON['p_r'] = $objectJSON['p_r'] + abs($stockTrans->totalquantity);
                } elseif ($stockTrans->t_type == "AOUT") {
                    $objectJSON['adj_out'] = $objectJSON['adj_out'] + abs($stockTrans->totalquantity);
                }
            }

            $InStock = $objectJSON['in'] + $objectJSON['adj_in'] + $objectJSON['s_r'];
            $OutStock = $objectJSON['out'] + $objectJSON['p_r'] + $objectJSON['adj_out'];

            //Get Stock bal.
            $objectJSON['stock_balance'] = $objectJSON['opening']  + $InStock - $OutStock;

            $s_n = $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
