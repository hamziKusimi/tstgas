<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Good;
use App\Model\View\PurchaseStock;
use App\Model\Gooddt;
use App\Model\Creditor;
use App\Model\Debtor;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Uom;
use App\Model\Porder;
use App\Model\Porderdt;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Location;
use PHPJasper\PHPJasper;
use Auth;
use Redirect;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use PDF;

class PurchaseReportController extends Controller
{
    public function processbybill(Request $request)
    {
        switch ($request->input('Btn')) {
            case 'search':
                return $this->search($request);
                break;

            case 'print':
                return $this->print($request);
                break;
        }
    }

    public function search(Request $request)
    {
        $GoodReceived = Good::selectRaw(
            'docno,
            date,
            account_code,
            account_code as code,
            name,
            amount,
            taxed_amount,
            CONCAT(MONTHNAME(`date`), " " , YEAR(`date`)) as Month'
        )
            ->where(function ($query) use ($request) {
                $dates = explode("-", $request->dates);
                $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

                if ($request->DocNo_Chkbx == "on") {
                    if ($request->DocNo_frm == $request->DocNo_to) {
                        $query->where('docno', '=', $request->DocNo_frm);
                    } else {
                        $query->whereBetween('docno', [$request->DocNo_frm, $request->DocNo_to]);
                    }
                }

                if ($request->AC_Code_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->AC_Code_frm, $request->AC_Code_to]);
                }
            })->get();

        if ($GoodReceived->isEmpty()) {
            return Redirect::back()->with([
                'Error' => 'No Data Found',
                'ac_chk' => $request->AC_Code_Chkbx, 'docno_chk' => $request->DocNo_Chkbx,
                'docno_from' => $request->DocNo_frm, 'docno_to' => $request->DocNo_to,
                'ac_from' => $request->AC_Code_frm, 'ac_to' => $request->AC_Code_to,
                'date' =>  $request->dates
            ]);
        }

        return redirect()->route('reportprinting.purchasebybill')
            ->with([
                'GoodReceived' => $GoodReceived, 'ac_chk' => $request->AC_Code_Chkbx,
                'docno_chk' => $request->DocNo_Chkbx,
                'docno_from' => $request->DocNo_frm, 'docno_to' => $request->DocNo_to,
                'ac_from' => $request->AC_Code_frm, 'ac_to' => $request->AC_Code_to,
                'date' =>  $request->dates
            ]);
    }

    public function print(Request $request)
    {
        $grpCount = isset($request->grp) ? count($request->grp) : 0;

        $grouping = [];
        for ($i = 0; $i < $grpCount; $i++) {

            for ($j = 1; $j <= 3; $j++) {
                if ($request->grp[$i] == "grp" . $j) {
                    $grouping[$i] = $request['grp_' . $j];
                }
            }
        }
        $GoodReceived = $request->session()->get('GR');
        switch ($grpCount) {
            case 1:
                $JasperName = "group_1";
                $group_1 = $grouping[0] != "month" ? $grouping[0] : 'Month';
                $GoodReceived =  $GoodReceived->groupBy($group_1);
                break;
            case 2:
                $JasperName = "group_2";
                $group_1 = $grouping[0] != "month" ? $grouping[0] : 'Month';
                $group_2 = $grouping[1] != "month" ? $grouping[1] : 'Month';
                $GoodReceived =  $GoodReceived->groupBy([$group_1, $group_2]);
                break;
            case 3:
                $JasperName = "group_3";
                $group_1 = $grouping[0] != "month" ? $grouping[0] : 'Month';
                $group_2 = $grouping[1] != "month" ? $grouping[1] : 'Month';
                $group_3 = $grouping[2] != "month" ? $grouping[2] : 'Month';
                $GoodReceived = $GoodReceived->groupBy([$group_1, $group_2, $group_3]);
                break;
            default:
                $JasperName = "no_group";
        }

        $dates = explode("-", $request->dates);
        $dates_from = $dates[0];
        $dates_to = $dates[1];

        $dates = explode("-", $request->dates);
        if(str_replace(' ', '', $dates[0]) == str_replace(' ', '', $dates[1])){
            $date = "at " . str_replace(' ', '', $dates[0]);
        }else{
            $date = $request->dates;
        }

        $systemsetup = SystemSetup::pluck('qty_decimal')->first();

        $dataArray = array();
        $s_n = 1;

        if ($JasperName == "no_group") {
            foreach ($GoodReceived as $GR) {
                $objectJSON = [];
                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $GR->docno;
                $objectJSON['date'] = date("d/m/Y", strtotime($GR->date));
                $objectJSON['code'] = $GR->account_code;
                $objectJSON['name'] = $GR->name;
                $objectJSON['total'] = $GR->amount;
                $objectJSON['gross'] = $GR->taxed_amount;
                $objectJSON['month'] =  $GR->year . "/" . $GR->month;
                $objectJSON['group_1'] = isset($grouping[0]) ? $grouping[0] : null;
                $objectJSON['group_2'] = isset($grouping[1]) ? $grouping[1] : null;
                $objectJSON['group_3'] = isset($grouping[2]) ? $grouping[2] : null;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }

            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bybill', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'purchaseArray' =>  $dataArray
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();

        } elseif ($JasperName == "group_1") {
            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bybill_grp1', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'group_1' =>  $group_1,
                'purchaseArray' =>  $GoodReceived
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();
            // return view('reportprinting.purchase.viewreport_bybill_grp1', $data);

        } elseif ($JasperName == "group_2") {
            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bybill_grp2', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'group_1' =>  $group_1,
                'group_2' =>  $group_2,
                'purchaseArray' =>  $GoodReceived
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();
            // return view('reportprinting.purchase.viewreport_bybill_grp2', $data);

        } else {
            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bybill_grp3', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'group_1' =>  $group_1,
                'group_2' =>  $group_2,
                'group_3' =>  $group_3,
                'purchaseArray' =>  $GoodReceived
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();
            // return view('reportprinting.purchase.viewreport_bybill_grp3', $data);
        }
    }

    public function processbystockcode(Request $request)
    {
        switch ($request->input('Btn')) {
            case 'search':
                return $this->searchByStockcode($request);
                break;

            case 'print':
                return $this->printByStockcode($request);
                break;
        }
    }

    public function searchByStockcode(Request $request)
    {
        $GoodReceived = PurchaseStock::selectRaw(
            'item_code, `subject`,uom, SUM(quantity) AS quantity, 
            SUM(amount) AS amount, SUM(t_amount) AS t_amount,
            category,brand,product,location,uom'
        )
            ->where(function ($query) use ($request) {
                $dates = explode("-", $request->dates);
                $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

                if ($request->STK_Code_Chkbx == "on") {
                    if ($request->STK_Code_frm == $request->STK_Code_to) {
                        $query->where('item_code', '=', $request->STK_Code_frm);
                    } else {
                        $query->whereBetween('item_code', [$request->STK_Code_frm, $request->STK_Code_to]);
                    }
                }

                if ($request->Category_Chkbx == "on") {
                    if ($request->Category_frm == $request->Category_to) {
                        $query->where('category', '=', $request->Category_frm);
                    } else {
                        $query->whereBetween('category', [$request->Category_frm, $request->Category_to]);
                    }
                }

                if ($request->Product_Chkbx == "on") {
                    if ($request->Product_frm == $request->Product_to) {
                        $query->where('product', '=', $request->Product_frm);
                    } else {
                        $query->whereBetween('product', [$request->Product_frm, $request->Product_to]);
                    }
                }

                if ($request->Brand_Chkbx == "on") {
                    if ($request->Brand_frm == $request->Brand_to) {
                        $query->where('brand', '=', $request->Brand_frm);
                    } else {
                        $query->whereBetween('brand', [$request->Brand_frm, $request->Brand_to]);
                    }
                }

                if ($request->Location_Chkbx == "on") {
                    if ($request->Location_frm == $request->Location_to) {
                        $query->where('location', '=', $request->Location_frm);
                    } else {
                        $query->whereBetween('location', [$request->Location_frm, $request->Location_to]);
                    }
                }
            })->groupBy('item_code', 'subject', 'uom','category','brand','product','location')->get();

        if ($GoodReceived->isEmpty()) {
            return Redirect::back()->with([
                'Error' => 'No Data Found',
                'Category_Chkbx' => $request->Category_Chkbx, 'STK_Code_Chkbx' => $request->STK_Code_Chkbx,
                'Product_Chkbx' => $request->Product_Chkbx, 'Brand_Chkbx' => $request->Brand_Chkbx,
                'Location_Chkbx' => $request->Location_Chkbx,
                'STK_Code_from' => $request->STK_Code_frm, 'STK_Code_to' => $request->STK_Code_to,
                'Category_from' => $request->Category_frm, 'Category_to' => $request->Category_to,
                'Product_from' => $request->Product_frm, 'Product_to' => $request->Product_to,
                'Brand_from' => $request->Brand_frm, 'Brand_to' => $request->Brand_to,
                'Location_from' => $request->Location_frm, 'Location_to' => $request->Location_to,
                'date' =>  $request->dates
            ]);
        }

        return redirect()->route('reportprinting.purchasebystockcode')
            ->with([
                'GoodReceived' => $GoodReceived,
                'Category_Chkbx' => $request->Category_Chkbx, 'STK_Code_Chkbx' => $request->STK_Code_Chkbx,
                'Product_Chkbx' => $request->Product_Chkbx, 'Brand_Chkbx' => $request->Brand_Chkbx,
                'Location_Chkbx' => $request->Location_Chkbx,
                'STK_Code_from' => $request->STK_Code_frm, 'STK_Code_to' => $request->STK_Code_to,
                'Category_from' => $request->Category_frm, 'Category_to' => $request->Category_to,
                'Product_from' => $request->Product_frm, 'Product_to' => $request->Product_to,
                'Brand_from' => $request->Brand_frm, 'Brand_to' => $request->Brand_to,
                'Location_from' => $request->Location_frm, 'Location_to' => $request->Location_to,
                'date' =>  $request->dates
            ]);
    }

    public function printByStockcode(Request $request)
    {
        $grpCount = isset($request->grp) ? count($request->grp) : 0;

        $grouping = [];
        for ($i = 0; $i < $grpCount; $i++) {

            for ($j = 1; $j <= 3; $j++) {
                if ($request->grp[$i] == "grp" . $j) {
                    $grouping[$i] = $request['grp_' . $j];
                }
            }
        }
        $GoodReceived = $request->session()->get('GR');
        switch ($grpCount) {
            case 1:
                $JasperName = "group_1";
                $group_1 = $grouping[0];
                $GoodReceived =  $GoodReceived->groupBy($group_1);
                break;
            case 2:
                $JasperName = "group_2";
                $group_1 = $grouping[0];
                $group_2 = $grouping[1];
                $GoodReceived =  $GoodReceived->groupBy([$group_1, $group_2]);
                break;
            case 3:
                $JasperName = "group_3";
                $group_1 = $grouping[0];
                $group_2 = $grouping[1];
                $group_3 = $grouping[2];
                $GoodReceived = $GoodReceived->groupBy([$group_1, $group_2, $group_3]);
                break;
            default:
                $JasperName = "no_group";
        }

        $dates = explode("-", $request->dates);
        if(str_replace(' ', '', $dates[0]) == str_replace(' ', '', $dates[1])){
            $date = "at " . str_replace(' ', '', $dates[0]);
        }else{
            $date = $request->dates;
        }

        if ($JasperName == "no_group") {
            $systemsetup = SystemSetup::pluck('qty_decimal')->first();

            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bystockcode', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'purchaseArray' =>  $GoodReceived
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();
            // return view('reportprinting.purchase.viewreport_bystockcode', $data);

        } elseif ($JasperName == "group_1") {
            $systemsetup = SystemSetup::pluck('qty_decimal')->first();

            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bystockcode_grp1', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'group_1' =>  $group_1,
                'purchaseArray' =>  $GoodReceived
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();
            // return view('reportprinting.purchase.viewreport_bystockcode_grp1', $data);
 
        } elseif ($JasperName == "group_2") {
            $systemsetup = SystemSetup::pluck('qty_decimal')->first();

            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bystockcode_grp2', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'group_1' =>  $group_1,
                'group_2' =>  $group_2,
                'purchaseArray' =>  $GoodReceived
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();
            // return view('reportprinting.purchase.viewreport_bystockcode_grp2', $data);
        
        } else {
            $systemsetup = SystemSetup::pluck('qty_decimal')->first();

            $pdf = PDF::loadView('reportprinting.purchase.viewreport_bystockcode_grp3', [
                'date' =>  $date,
                'systemsetup' =>  $systemsetup,
                'group_1' =>  $group_1,
                'group_2' =>  $group_2,
                'group_3' =>  $group_3,
                'purchaseArray' =>  $GoodReceived
            ])->setPaper('A4', 'landscape');

            return $pdf->inline();
            // return view('reportprinting.purchase.viewreport_bystockcode_grp3', $data);


        }
    }
    
}
