<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Adjustmenti;
use App\Model\Adjustmentidt;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Uom;
use App\Model\Location;
use App\Model\Salesman;
use App\Model\PrintedIndexView;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use App\Model\Area;

class AdjustmentiController extends Controller
{
    public function index()
    {
        $data["adjustmentis"] = Adjustmenti::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Adjustmenti::pluck('docno', 'docno');
        $data["print"] = PrintedIndexView::where('index', 'Adjustment In')->pluck('printed_by');
        $data["page_title"] = "Adjustment In Listing";
        $data["bclvl1"] = "Adjustment In Listing";
        $data["bclvl1_url"] = route('adjustmentis.index');
        return view('dailypro/adjustmentis.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["adjustmentisearch"] = Adjustmenti::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Adjustmenti::pluck('docno', 'docno');
        $data["page_title"] = "Adjustment In Listing";
        $data["bclvl1"] = "Adjustment In Listing";
        $data["bclvl1_url"] = route('adjustmentis.index');
        return view('dailypro.adjustmentis.index', $data);
    }

    public function adjustmentiDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'taxed_amount',
                        );

        $totalData = Adjustmenti::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $adjustmentis = Adjustmenti::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $adjustmentis =  Adjustmenti::Where('docno', 'LIKE',"%{$search}%")
                            ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                            ->orWhere('date', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Adjustmenti::Where('docno', 'LIKE',"%{$search}%")
                             ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                             ->orWhere('date', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($adjustmentis))
        {
            foreach ($adjustmentis as $key => $adjustmenti)
            {
                $delete =  route('adjustmentis.destroy', $adjustmenti->id);
                $edit =  route('adjustmentis.edit', $adjustmenti->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('ADJUSTMENT_IN_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$adjustmenti->docno.'</a>';
                }else{
                    $nestedData['docno'] = $adjustmenti->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($adjustmenti->date));
                $nestedData['taxed_amount'] = $adjustmenti->taxed_amount;
                if(Auth::user()->hasPermissionTo('ADJUSTMENT_IN_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }

                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function api_store(Request $request)
    {
        // return $request['search'];
        $data = Adjustmenti::select('id', 'docno', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();


        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Adjustment In');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Adjustment In";
        $data["bclvl1"] = "Adjustment In Listing";
        $data["bclvl1_url"] = route('adjustmentis.index');
        $data["bclvl2"] = "Add Adjustment In";
        $data["bclvl2_url"] = route('adjustmentis.create');
        // return response()->json($data);

        return view('dailypro/adjustmentis.create', $data);
    }

    public function store(Request $request)
    {
        $adjustmentiid = Adjustmenti::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $adjustmentiDoc = DocumentSetup::findByName('Adjustment In');
        $adjustmentiDoc->update(['D_LAST_NO' => $adjustmentiDoc->D_LAST_NO + 1]);

        for ($i = 1; $i < count($request['item_id']); $i++) {
            $dt = Adjustmentidt::create([
                'doc_no' => $request['doc_no'],
                'sequence_no' => $request['sequence_no'][$i],
                'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                'item_code' => $request['item_code'][$i],
                'subject' => $request['subject'][$i],
                'details' => $request['details'][$i],
                'qty' => $request['quantity'][$i],
                'uom' => $request['unit_measuredt'][$i],
                'rate' => $request['rate'][$i],
                'ucost' => $request['unit_cost'][$i],
                'type' => ($request['type'][$i] == 0) ? 0 : 1,
                'discount' => $request['discount'][$i],
                'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                'created_by' => Auth::user()->id
            ]);

            // Update stockabalance
            $stock = Stockcode::where('code', $dt->item_code)->first();
            $stkBalance = $stock->stkbal + $dt->totalqty;
            $stock->update(['stkbal' => $stkBalance]);
        }

        return redirect()->route('adjustmentis.edit', ['id' => $adjustmentiid])->with('Success', 'Adjustment In added sucessfully');
    }

    public function show(Type $var = null)
    {
        return view('dailypro/adjustmentis.adjustmentis', $data);
    }

    public function edit($id)
    {
        $adjustmenti = Adjustmenti::find($id);
        $contents = Adjustmentidt::where('doc_no', $adjustmenti->docno)->get();
        $selectedDebitor = Debtor::findByAcode($adjustmenti->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $adjustmenti;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Adjustment In";
        $data["bclvl1"] = "Adjustment In Listing";
        $data["bclvl1_url"] = route('adjustmentis.index');
        $data["bclvl2"] = "Edit Adjustment In";
        $data["bclvl2_url"] = route('adjustmentis.edit', ['id' => $id]);

        // return response()->json( count($data["adjustmentidts"]));
        // return response()->json($adjustmentidts);
        return view('dailypro/adjustmentis.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $adjustmenti = Adjustmenti::find($id);
        if (isset($adjustmenti)) {
            $adjustmenti->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $adjustmentidt_id = Adjustmentidt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;
                    if ($adjustmentidt_id != null) {
                        $stkBalance = $stock->stkbal - $adjustmentidt_id->totalqty;
                        $adjustmentidt_id->update([
                            'doc_no' => $request['doc_no'],
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'type' => ($request['type'][$i] == 0) ? 0 : 1,
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal + $adjustmentidt_id->totalqty;
                    } else {
                        $dt = Adjustmentidt::create([
                            'doc_no' => $request['doc_no'],
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'type' => ($request['type'][$i] == 0) ? 0 : 1,
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal + $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
            // return response()->json($request['adjustmentidts_id']);

        }
        // dd($adjustmentidt);
        return redirect()->route('adjustmentis.edit', ['id' => $id])->with('Success', 'Adjustment In updated sucessfully');
    }

    public function destroy($id)
    {

        $adjustmenti = Adjustmenti::find($id);
        $adjustmenti->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmenti->delete();
        // return redirect()->route('adjustmentis.index')->with('Success', 'Adjustment In deleted successfully');

        $adjustmentidt = Adjustmentidt::where('doc_no', $adjustmenti->docno);
        $adjustmentidt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmentidt->delete();
        return redirect()->route('adjustmentis.index')->with('Success', 'Adjustment In deleted successfully');
    }


    public function destroyData($id)
    {
        $data = Adjustmentidt::find($id);

        if (isset($data)) {
            $Adjustmenti = Adjustmenti::where('docno','=',$data->doc_no)->first();
            $Adjustmenti->update([
                'amount' => $Adjustmenti->amount - $data->amount,
                'taxed_amount' => $Adjustmenti->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyAdjustmentidt($id)
    {
        $adjustmentidt = Adjustmentidt::find($id);
        $adjustmentidt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmentidt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper($id)
    {
        //update printed details
        $adjustmenti = Adjustmenti::find($id);
        $getprinted = Adjustmenti::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($adjustmenti)) {
            $adjustmenti->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $Resources = Adjustmenti::where('id', $id)->get();
        $ResourcesJsonList = $this->getAdjustmentInListingJSON($Resources);

        $formattedResource = Adjustmenti::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'AdjustmentIn';
        $jrxml = 'general-bill-Adjustment.jrxml';
        $reportTitle = 'Adjustment In';
        Transaction::generateBillAdjustment(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        $AdjustmentIn = $this->getAdjustmentInQuery($request);

        if ($AdjustmentIn->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->AdjustmentIn_rcv == "listing") {
            $AdjustmentInJSON = $this->getAdjustmentInListingJSON($AdjustmentIn);
            $reportType = "AdjustmentInListing";
        } else {
            $AdjustmentInJSON = $this->getAdjustmentInSummaryJSON($AdjustmentIn);
            $reportType = "AdjustmentInSummary";
        }

        // check if JSON empty \\
        $AdjtJSON = json_decode($AdjustmentInJSON);
        if (empty($AdjtJSON->data)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        // check if JSON empty \\

        $JsonFileName = "AdjustmentIn" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/DailyProcess/AdjustmentIn/' . $JsonFileName . '.json'), $AdjustmentInJSON);
        $JasperFileName = "AdjustmentIn" . date("Ymdhisa") . Auth::user()->id;

        $file = $this->printReport($JsonFileName, $JasperFileName, $reportType);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/AdjustmentIn/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/AdjustmentIn/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/AdjustmentIn/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/AdjustmentIn/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getAdjustmentInQuery(Request $request)
    {
        return Adjustmenti::select('docno', 'date')
            ->where(function ($query) use ($request) {

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

    public function getAdjustmentInListingJSON($AdjustmentIn)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Adjustment In')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Adjustment In')->update([
                'index' => "Adjustment In",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Adjustment In",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $dataArray = array();
        foreach ($AdjustmentIn as $AdjtIn) {

            $AdjtInTrans = Adjustmentidt::select(
                'item_code',
                'subject',
                'qty',
                'uom',
                'ucost',
                'amount',
                'details',
                'taxed_amount'
            )
                ->where("doc_no", "=", $AdjtIn->docno)
                ->get();

            $lastElement = count($AdjtInTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($AdjtInTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($AdjtIn->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $AdjtIn->docno . ", " . $date;
                $objectJSON['description'] = $trans->subject;
                $objectJSON['item_code'] = $trans->item_code;
                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_cost'] = $trans->ucost;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $objectJSON['amount'];
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getAdjustmentInSummaryJSON($AdjustmentIn)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Adjustment In')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Adjustment In')->update([
                'index' => "Adjustment In",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Adjustment In",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $dataArray = array();
        $s_n = 1;
        foreach ($AdjustmentIn as $AdjtIn) {
            $AdjtInTrans = Adjustmentidt::select('amount', 'updated_at')
                ->where("doc_no", "=", $AdjtIn->docno)
                ->get();

            foreach ($AdjtInTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $AdjtIn->docno;
                $objectJSON['date'] = $date;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }



    public function getRouteByDocno(Request $request)
    {
        $Adjustmenti = Adjustmenti::where('docno', $request->docno)->first('id');

        if (!$Adjustmenti) {

            $url = action('AdjustmentiController@create');
        } else {

            $url = action('AdjustmentiController@edit', ['id' => $Adjustmenti->id]);
        }

        return $url;
    }
}
