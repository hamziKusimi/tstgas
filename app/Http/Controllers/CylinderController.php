<?php

namespace App\Http\Controllers;

use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Cylindervalvetype;
use App\Model\Cylindercontainertypes;
use App\Model\Cylinderhandwheeltype;
use App\Model\Gasrackcylinder;
use App\Model\Manufacturer;
use App\Model\Ownership;
use App\Model\CylinderActivity;
use App\Model\Debtor;
use App\Model\View\CylinderMaster;
use App\Model\view\CylinderTrackingView;
use Illuminate\Http\Request;
use Auth;
use PHPJasper\PHPJasper;
use PDF;
use DB;
use DataTables;
use DateTime;

class CylinderController extends Controller
{
    public function index()
    {
        $data["cylinders"] = Cylinder::orderBy('barcode', 'desc')->paginate(30);
        $data["page_title"] = "Cylinder Item Listings";
        $data["bclvl1"] = "Cylinder Item Listings";
        $data["bclvl1_url"] = route('cylinders.index');

        return view('cylindermaster.cylinders.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["cylindersearch"] = Cylinder::where(function ($query) use ($request) {
            $query->orWhere('serial', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('barcode', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('descr', 'LIKE', '%' . $request['search'] . '%');
        })
            ->orderBy('serial')
            ->paginate(30);

        $data["cylinders"] = $data["cylindersearch"];
        $data["page_title"] = "Cylinder Item Listings";
        $data["bclvl1"] = "Cylinder Item Listings";
        $data["bclvl1_url"] = route('cylinders.index');
        return view('cylindermaster.cylinders.index', $data);
    }

    public function docnoList()
    {
        // $query = CylinderTrackingView::query()
        //     ->selectRaw('dn_no, DATETIME as date, accountcode as account_code')->groupBy('dn_no');
        $query =  DB::select(DB::raw("
            SELECT dn_no, date, account_code FROM return_notes GROUP BY dn_no
            UNION ALL
            SELECT dn_no, date, account_code FROM delivery_notes GROUP BY dn_no
        "), []);

        return DataTables::of($query)->make(true);
    }

    public function cylinderList()
    {
        $query = Cylinder::query()
            ->selectRaw('serial, descr')->orderBy('serial');

        return DataTables::of($query)->make(true);
    }

    public function debtorList()
    {
        $query = Debtor::query()
            ->selectRaw('accountcode, name')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }


    public function cylinderactivity()
    {
        // return response()->json("test");
        // $data["cylinderactivities"] = CylinderTrackingView::paginate(15);
        $data["doctype"] = CylinderTrackingView::get();
        $data["page_title"] = "Cylinder Asset Tracking";
        $data["bclvl1"] = "Cylinder Asset Tracking";
        $data["bclvl1_url"] = route('cylinders.cylinderactivity');
        // dd($data);
        return view('cylindermaster.cylinders.cylinderactivity', $data);
    }

    public function cylinderActivityListing(Request $request)
    {
        // dd($request['dates']);
        $data["cylinderactivities"] = CylinderTrackingView::paginate(15);
        $data["page_title"] = "Cylinder Asset Tracking";
        $data["bclvl1"] = "Cylinder Asset Tracking";
        $data["bclvl1_url"] = route('cylinders.cylinderactivity');
        $data["bclvl2"] = "Cylinder Asset Tracking";
        $data["bclvl2_url"] = route('cylinders.cylinderactivity.listing');
        $data["docno_Chkbx"] = $request->docno_Chkbx;
        $data["docno_frm"] = $request->docno_frm;
        $data["docno_to"] = $request->docno_to;
        $data["serial_Chkbx"] = $request->serial_Chkbx;
        $data["serial_frm"] = $request->serial_frm;
        $data["serial_to"] = $request->serial_to;
        $data["grserial_Chkbx"] = $request->grserial_Chkbx;
        $data["grserial_frm"] = $request->grserial_frm;
        $data["grserial_to"] = $request->grserial_to;
        $data["debCode_Chkbx"] = $request->debCode_Chkbx;
        $data["debCode_frm"] = $request->debCode_frm;
        $data["debCode_to"] = $request->debCode_to;
        $data["dates_Chkbx"] = $request->dates_Chkbx;
        if(($request->dates_Chkbx) == 'on'){
            $data["dates"] = $request->dates;
            $dates = explode("-", $request->dates);

            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');
            $data["dates_from"] = $dates_from;
            $data["dates_to"] = $dates_to;
        }else{
            $data["dates"] = '';
            $data["dates_from"] = '';
            $data["dates_to"] = '';
        }
        // return response($data);
        // dd($data["dates_Chkbx"]);
        return view('cylindermaster.cylinders.cylinderactivitylisting', $data);

    }

    public function cylindertrackingDatatableList(Request $request)
    {

        if ($request->dates_Chkbx == 'on') {
            if ($request->dates_from == $request->dates_to) {
                $date = 'AND cylinder_tracking_view.datetime = "' . $request->dates_from . '"';
            } else {
                $date = 'AND cylinder_tracking_view.datetime >= "' . $request->dates_from . '" AND cylinder_tracking_view.datetime <= "' . $request->dates_to . '"';
            }
        }else{
            $date = '';
        }

        if ($request->docno_Chkbx == 'on') {
            $docno = 'AND (cylinder_tracking_view.dn_no >= "' . $request->docno_frm . '" AND cylinder_tracking_view.dn_no <= "' . $request->docno_to . '")';
        }else{
            $docno = '';
        }

        if ($request->debCode_Chkbx == 'on') {
            $debtor = 'AND (cylinder_tracking_view.accountcode >= "' . $request->debCode_frm . '" AND cylinder_tracking_view.accountcode <= "' . $request->debCode_to . '")';
        }else{
            $debtor = '';
        }

        if ($request->serial_Chkbx == 'on') {
            $serial = 'AND (cylinder_tracking_view.serial >= "' . $request->serial_frm . '" AND cylinder_tracking_view.serial <= "' . $request->serial_to . '")';
        }else{
            $serial = '';
        }

        if ($request->grserial_Chkbx == 'on') {
            $grserial = 'AND (cylinder_tracking_view.serial >= "' . $request->grserial_frm . '" AND cylinder_tracking_view.serial <= "' . $request->grserial_to . '")';
        }else{
            $grserial = '';
        }

        $query = DB::select(DB::raw('

                SELECT
                cylinder_tracking_view.model AS model,
                cylinder_tracking_view.barcode AS barcode,
                cylinder_tracking_view.serial AS serial,
                cylinder_tracking_view.action AS action,
                cylinder_tracking_view.description AS description,
                cylinder_tracking_view.datetime AS datetime,
                cylinder_tracking_view.dn_no AS dn_no,
                cylinder_tracking_view.TYPE AS TYPE,
                cylinder_tracking_view.created_at AS created_at,
                cylinder_tracking_view.updated_at AS updated_at,
                cylinder_tracking_view.accountcode AS accountcode,
                cylinder_tracking_view.name AS NAME
                FROM cylinder_tracking_view
                where 1=1 '.$docno.' '.$date.' '.$serial.' '.$debtor.' '.$grserial.'
                order by updated_at DESC
        '), [
            ]);

        return DataTables::of($query)->make(true);
    }

    public function create()
    {
        $data["serials"] = Cylinder::pluck('serial');
        $data["barcodes"] = Cylinder::pluck('barcode');
        $data["categories"] = Cylindercategory::all();
        $data["groups"] = Cylindergroup::all();
        $data["products"] = Cylinderproduct::all();
        $data["types"] = Cylindertype::all();
        $data["containertypes"] = Cylindercontainertypes::all();
        $data["handwheeltypes"] = Cylinderhandwheeltype::all();
        $data["valvetypes"] = Cylindervalvetype::all();
        $data["manufacturers"] = Manufacturer::all();
        $data["owners"] = Ownership::all();
        // $data["categories"] = Cylindercategory::pluck('code', 'id');
        // $data["groups"] = Cylindergroup::pluck('code', 'id');
        // $data["products"] = Cylinderproduct::pluck('code', 'id');
        // $data["types"] = Cylindertype::pluck('code', 'id');
        // $data["containertypes"] = Cylindercontainertypes::pluck('code', 'id');
        // $data["handwheeltypes"] = Cylinderhandwheeltype::pluck('code', 'id');
        // $data["valvetypes"] = Cylindervalvetype::pluck('code', 'id');
        // $data["manufacturers"] = Manufacturer::pluck('code', 'id');
        // $data["owners"] = Ownership::pluck('code', 'id');
        $data["page_title"] = "Add Cylinder";
        $data["bclvl1"] = "Cylinder Item Listings";
        $data["bclvl1_url"] = route('cylinders.index');
        $data["bclvl2"] = "Add Cylinder";
        $data["bclvl2_url"] = route('cylinders.create');
        return view('cylindermaster.cylinders.create', $data);
    }

    public function store(Request $request)
    {
        $cylID = cylinder::create([
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'cat_id' => $request['category'],
            'group_id' => $request['group'],
            'type_id' => $request['type_id'],
            'prod_id' => $request['product'],
            'descr' => $request['descr'],
            'owner' => $request['owner'],
            'mass' => $request['mass'],
            'approval' => $request['approval'],
            'approved' => $request['approved'],
            'capacity' => $request['capacity']?$request['capacity']:"nil",
            'conttype' => $request['conttype'],
            'hwtype' => $request['hwtype'],
            'testdate' => $request['testdate'] ? date('Y-m-d', strtotime($request['testdate'])) : null,
            'mfgdate' => $request['mfgdate'] ? date('Y-m-d', strtotime($request['mfgdate'])) : null,
            'mfr' => $request['mfr'],
            'material' => $request['material'],
            'neckcode' => $request['neckcode'],
            'oriowner' => $request['oriowner'],
            'oritareweight' => $request['oritareweight'],
            'ramplot' => $request['ramplot'],
            'solvent' => $request['solvent'],
            'valveguard' => $request['valveguard'],
            'valveoutlet' => $request['valveoutlet'],
            'testpressure' => $request['testpressure']?$request['testpressure']:"nil",
            'workingpressure' => $request['workingpressure']?$request['workingpressure']:"nil",
            'valvetype' => $request['valvetype'],
            'valvemfr' => $request['valvemfr'],
            'created_by' => Auth::user()->id,
            'status' => $request['Status'],
            'reasons' => $request['reasons']
        ]);

        return redirect()->route('cylinders.edit', ['id' => $cylID])->with('Success', 'Cylinder created successfully');
    }

    public function edit($id)
    {
        $data["serials"] = Cylinder::pluck('serial');
        $data["barcodes"] = Cylinder::pluck('barcode');
        $data["cylinder"] = Cylinder::find($id);
        $data["categories"] = Cylindercategory::all();
        $data["groups"] = Cylindergroup::all();
        $data["products"] = Cylinderproduct::all();
        $data["types"] = Cylindertype::all();
        $data["containertypes"] = Cylindercontainertypes::all();
        $data["handwheeltypes"] = Cylinderhandwheeltype::all();
        $data["valvetypes"] = Cylindervalvetype::all();
        $data["manufacturers"] = Manufacturer::all();
        $data["owners"] = Ownership::all();
        // $data["categories"] = Cylindercategory::pluck('code', 'id');
        // $data["groups"] = Cylindergroup::pluck('code', 'id');
        // $data["products"] = Cylinderproduct::pluck('code', 'id');
        // $data["types"] = Cylindertype::pluck('code', 'id');
        // $data["containertypes"] = Cylindercontainertypes::pluck('code', 'id');
        // $data["handwheeltypes"] = Cylinderhandwheeltype::pluck('code', 'id');
        // $data["valvetypes"] = Cylindervalvetype::pluck('code', 'id');
        // $data["manufacturers"] = Manufacturer::pluck('code', 'id');
        // $data["owners"] = Ownership::pluck('code', 'id');
        $data["page_title"] = "Edit Cylinder";
        $data["bclvl1"] = "Cylinder Item Listings";
        $data["bclvl1_url"] = route('cylinders.index');
        $data["bclvl2"] = "Edit Cylinder";
        $data["bclvl2_url"] = route('cylinders.edit', ['id' => $id]);

        // return response()->json($data);

        return view('cylindermaster.cylinders.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cylinder = Cylinder::find($id);
        $cylinder->update([
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'cat_id' => $request['category'],
            'group_id' => $request['group'],
            'type_id' => $request['type_id'],
            'prod_id' => $request['product'],
            'descr' => $request['descr'],
            'owner' => $request['owner'],
            'mass' => $request['mass'],
            'approval' => $request['approval'],
            'approved' => $request['approved'],
            'capacity' => $request['capacity']?$request['capacity']:"nil",
            'conttype' => $request['conttype'],
            'hwtype' => $request['hwtype'],
            'testdate' => $request['testdate'] ? date('Y-m-d', strtotime($request['testdate'])) : null,
            'mfgdate' => $request['mfgdate'] ? date('Y-m-d', strtotime($request['mfgdate'])) : null,
            'mfr' => $request['mfr'],
            'material' => $request['material'],
            'neckcode' => $request['neckcode'],
            'oriowner' => $request['oriowner'],
            'oritareweight' => $request['oritareweight'],
            'ramplot' => $request['ramplot'],
            'solvent' => $request['solvent'],
            'valveguard' => $request['valveguard'],
            'valveoutlet' => $request['valveoutlet'],
            'testpressure' => $request['testpressure']?$request['testpressure']:"nil",
            'workingpressure' => $request['workingpressure']?$request['workingpressure']:"nil",
            'valvetype' => $request['valvetype'],
            'valvemfr' => $request['valvemfr'],
            'updated_by' => Auth::user()->id,
            'status' => $request['Status'],
            'reasons' => $request['reasons']
        ]);

        return redirect()->route('cylinders.edit', ['id' => $id])->with('Success', 'Cylinder updated successfully');
    }

    public function cylinderDatatableList()
    {
        $query = CylinderMaster::query()
            ->selectRaw('serial, barcode, descr, ctype, product, id')
            ->whereNotNUll('barcode');

        return DataTables::of($query)->make(true);
    }

    public function destroy(cylinder $cylinder)
    {
        $cylinder->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylinder->delete();
        return redirect()->route('cylinders.index')->with('Success', 'Cylinder deleted successfully.');
    }

    public function print(Request $request)
    {
        $Cylinder = CylinderMaster::where(function ($query) use ($request) {

            if ($request->BarcodeFilter == "withBc") {
                $query->whereNotNull('barcode')
                    ->where('barcode', '!=', '')->get();
            } elseif ($request->BarcodeFilter == "withoutBc") {
                $query->whereNull('barcode')
                    ->get();
            }

            if ($request->Capacity_Chkbx == "on") {
                if ($request->Capacity_to == "") {
                    $query->where('capacity', '=', $request->Capacity_frm)
                        ->get();
                } else {
                    $query->whereBetween('capacity', [$request->Capacity_frm, $request->Capacity_to])
                        ->get();
                }
            }

            if ($request->Type_Chkbx == "on") {
                $query->whereBetween('ctype', [$request->Type_frm, $request->Type_to])
                    ->get();
            }

            if ($request->Category_Chkbx == "on") {
                $query->whereBetween('category', [$request->Category_frm, $request->Category_to])
                    ->get();
            }

            if ($request->Product_Chkbx == "on") {
                $query->whereBetween('product', [$request->Product_frm, $request->Product_to])
                    ->get();
            }

            if ($request->Group_Chkbx == "on") {
                $query->whereBetween('cgroup', [$request->Group_frm, $request->Group_to])
                    ->get();
            }

            if ($request->HWtype_Chkbx == "on") {
                $query->whereBetween('htype', [$request->HWtype_frm, $request->HWtype_to])
                    ->get();
            }

            if ($request->ValveType_Chkbx == "on") {
                $query->whereBetween('vtype', [$request->ValveType_frm, $request->ValveType_to])
                    ->get();
            }

            if ($request->Manufacturer_Chkbx == "on") {
                $query->whereBetween('manu', [$request->Manufacturer_frm, $request->Manufacturer_to])
                    ->get();
            }
        })->get();

        if ($Cylinder->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $data = [];
        $data['CylinderJSON'] = $Cylinder;
        $data['request'] = $request;
        $pdf = PDF::loadView('cylindermaster/cylinders.masterlisting', ['CylinderJSON' => $data['CylinderJSON'], 'request' => $data['request']])->setPaper('A4');
        return $pdf->inline();
        // return view('cylindermaster/cylinders.masterlisting', $data);
    }

    public function convert_customer_data_to_html($request, $cylinders)
    {
        $no = 1;

        $output = '
        <h3 align="center">Customer Data</h3>
        <table width="100%" style="border-collapse: collapse; border: 0px;">
         <tr>
       <th style="border: 1px solid; padding:12px;" width="20%">Barcode</th>
       <th style="border: 1px solid; padding:12px;" width="30%">serial</th>
       <th style="border: 1px solid; padding:12px;" width="15%">category</th>
       <th style="border: 1px solid; padding:12px;" width="15%">product Code</th>
       <th style="border: 1px solid; padding:12px;" width="20%">owner</th>
      </tr>
        ';
        foreach ($cylinders as $cylinder) {
            $output .= '
            <tr>
             <td style="border: 1px solid; padding:12px;">' . $cylinder->barcode . '</td>
             <td style="border: 1px solid; padding:12px;">' . $cylinder->serial . '</td>
             <td style="border: 1px solid; padding:12px;">' . $cylinder->category . '</td>
             <td style="border: 1px solid; padding:12px;">' . $cylinder->product . '</td>
             <td style="border: 1px solid; padding:12px;">' . $cylinder->own . '</td>
            </tr>
            ';
        }
        $output .= '</table>';
        return $output;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/CylinderMasterListing/CylinderMasterListing.jrxml';
        $output = base_path() . '/resources/reporting/CylinderMasterListing/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/CylinderMasterListing/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;
        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/CylinderMasterListing/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getCylinderJSON($request, $cylinders)
    {
        $dataArray = array();
        $no = 1;

        foreach ($cylinders as $cylinder) {
            $objectJSON = [];

            $objectJSON['no'] = $no;
            $objectJSON['barcode'] = $cylinder->barcode;
            $objectJSON['serial_no'] = $cylinder->serial;
            $objectJSON['category'] = $cylinder->category;
            $objectJSON['product'] = $cylinder->product;
            $objectJSON['ownership'] = $cylinder->own;
            $objectJSON['hdw_type'] = $cylinder->htype;
            $objectJSON['manufact'] = $cylinder->manu;
            $objectJSON['material'] = $cylinder->material;
            $objectJSON['type'] = $cylinder->ctype;
            $objectJSON['group'] = $cylinder->cgroup;

            $objectJSON['description'] = $cylinder->descr . "<br><b>Valve Guard:</b>" . ($cylinder->valveguard ? "Yes" : "No");
            if ($request->OriOwn_Checkbox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Ori. Owner:</b><br>" . $cylinder->oriowner;
            }
            if ($request->OriWeight_Checkbox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Ori. TareWeight:</b><br>" . $cylinder->oritareweight;
            }
            if ($request->VOut_checkBox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Valve Outlet:</b><br>" . $cylinder->valveoutlet;
            }
            if ($request->TestPress_Checkbox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Test Pressure:</b><br>" . $cylinder->testpressure;
            }
            if ($request->VType_Checkbox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Valve Type:</b><br>" . $cylinder->vtype;
            }
            if ($request->ValManu_Checkbox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Value Manufact:</b><br>" . $cylinder->valvemfr;
            }
            if ($request->TestDate_Checkbox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Test Date:</b><br>" . date("d/m/Y", strtotime($cylinder->testdate));
            }
            if ($request->ManuDate_Checkbox == "on") {
                $objectJSON['description'] = $objectJSON['description'] . "<br><b>Manufac. Date:</b><br>" . date("d/m/Y", strtotime($cylinder->mfgdate));
            }

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }
        return '{"data" :' . json_encode($dataArray) . '}';
    }

    public function filter_records(Request $request)
    {

        $result = Cylinder::where(function ($query) use ($request) {

            if ($request['isBarcode'] == 'true') {
                $query->Where('barcode', '=', $request['item']);
            } else {
                $query->Where('serial', '=', $request['item']);
            }
            $query->whereNotNull('serial');
        })
            ->get(['id', 'barcode', 'serial', 'descr', 'cat_id', 'prod_id', 'capacity', 'workingpressure']);

        return $result;
    }

    public function rn_filter_records(Request $request, $accountcode)
    {

        // $result = Cylinder::where(function ($query) use ($request) {

        //     $query->orWhere('serial', '=', $request['item'])
        //         ->orWhere('barcode', '=', $request['item']);

        //     $query->whereNotNull('barcode');
        // })
        //     ->get(['barcode', 'serial', 'cat_id', 'prod_id', 'capacity', 'testpressure']);
        if ($request['isBarcode'] == 'true') {

                $result = DB::select(DB::raw("

                    SELECT
                    cylinders.serial AS serial,
                    cylinders.barcode AS barcode,
                    cylinders.cat_id AS cat_id,
                    cylinders.prod_id AS prod_id,
                    cylinders.capacity AS capacity,
                    cylinders.descr AS descr,
                    cylinders.workingpressure AS workingpressure,
                    delivery_notes.dn_no AS dnno,
                    delivery_notes.date AS dnno_date,
                    delivery_notes.account_code AS account_code,
                    delivery_noteprs.dn_no AS pr_dnno,
                    delivery_noteprs.barcode AS pr_barcode,
                    delivery_noteprs.serial AS pr_serial
                    FROM cylinders
                    INNER JOIN delivery_noteprs ON delivery_noteprs.serial = cylinders.serial
                    INNER JOIN delivery_notes ON delivery_notes.dn_no = delivery_noteprs.dn_no

                    where delivery_noteprs.return_note is null and delivery_notes.account_code = :accountcode and cylinders.barcode = :item
            "), [
                    'accountcode' => $accountcode,
                    'item' => $request['item']
                ]);
        } else {
                $result = DB::select(DB::raw("

                SELECT
                cylinders.serial AS serial,
                cylinders.barcode AS barcode,
                cylinders.cat_id AS cat_id,
                cylinders.prod_id AS prod_id,
                cylinders.capacity AS capacity,
                cylinders.descr AS descr,
                cylinders.workingpressure AS workingpressure,
                delivery_notes.dn_no AS dnno,
                delivery_notes.date AS dnno_date,
                delivery_notes.account_code AS account_code,
                delivery_noteprs.dn_no AS pr_dnno,
                delivery_noteprs.barcode AS pr_barcode,
                delivery_noteprs.serial AS pr_serial
                FROM cylinders
                INNER JOIN delivery_noteprs ON delivery_noteprs.serial = cylinders.serial
                INNER JOIN delivery_notes ON delivery_notes.dn_no = delivery_noteprs.dn_no

                where delivery_noteprs.return_note is null and delivery_notes.account_code = :accountcode and cylinders.serial = :item
        "), [
                'accountcode' => $accountcode,
                'item' => $request['item']
            ]);
        }

        return $result;
    }

    public function check_cylinders(Request $request)
    {

        $result = Gasrackcylinder::where(function ($query) use ($request) {

            if ($request['isBarcode'] == 'true') {
                $query->Where('cy_barcode', '=', $request['item']);
            } else {
                $query->Where('cy_serial', '=', $request['item']);
            }
            $query->whereNotNull('cy_barcode');
        })
            ->get(['cy_barcode', 'cy_serial']);

        return $result;
    }

    public function getCylinderDetail(Request $request)
    {
        if ($request['isBarcode'] == 'true'){

            $result = Cylinder::where('barcode', $request['item'])->first();

        }else{

            $result = Cylinder::where('serial', $request['item'])->first();

        }

    }
}
