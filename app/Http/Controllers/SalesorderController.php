<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Salesorder;
use App\Model\Salesorderdt;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\PrintedIndexView;
use App\Model\Uom;
use App\Model\Quotation;
use App\Model\Location;
use App\Model\Salesman;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use App\Model\Area;
use PDF;

class salesorderController extends Controller
{
    public function index()
    {
        $data["salesorders"] = Salesorder::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Salesorder::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Sales Order')->pluck('printed_by');
        $data["page_title"] = "Sales Order Listing";
        $data["bclvl1"] = "Sales Order Listing";
        $data["bclvl1_url"] = route('salesorders.index');
        return view('dailypro/salesorders.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["deliverysearch"] = Salesorder::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('lpono', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref1', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('cbinvno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('quotno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Salesorder::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["page_title"] = "Sales Order Listing";
        $data["bclvl1"] = "Sales Order Listing";
        $data["bclvl1_url"] = route('salesorders.index');
        return view('dailypro.salesorders.index', $data);
    }
    public function qtno_get($debtor)
    {
        $response = Quotation::where('account_code', $debtor)->get();
        return response()->json($response);
    }

    public function salesorderDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'lpono',
                            6 => 'ref1',
                            7 => 'cbinvno',
                            8 => 'quotno',
                            9 => 'taxed_amount',
                        );

        $totalData = Salesorder::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $salesorders = Salesorder::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $salesorders =  Salesorder::Where('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('lpono', 'LIKE',"%{$search}%")
                            ->orWhere('ref1', 'LIKE',"%{$search}%")
                            ->orWhere('cbinvno', 'LIKE',"%{$search}%")
                            ->orWhere('quotno', 'LIKE',"%{$search}%")
                            ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                            ->orWhere('date', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Salesorder::Where('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->orWhere('lpono', 'LIKE',"%{$search}%")
                             ->orWhere('ref1', 'LIKE',"%{$search}%")
                             ->orWhere('cbinvno', 'LIKE',"%{$search}%")
                             ->orWhere('quotno', 'LIKE',"%{$search}%")
                             ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                             ->orWhere('date', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($salesorders))
        {
            foreach ($salesorders as $key => $salesorder)
            {
                $delete =  route('salesorders.destroy', $salesorder->id);
                $edit =  route('salesorders.edit', $salesorder->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('SALES_OD_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$salesorder->docno.'</a>';
                }else{
                    $nestedData['docno'] = $salesorder->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($salesorder->date));
                $nestedData['account_code'] = $salesorder->account_code;
                $nestedData['name'] = $salesorder->name;
                $nestedData['lpono'] = $salesorder->lpono;
                $nestedData['ref1'] = $salesorder->ref1;
                $nestedData['cbinvno'] = $salesorder->cbinvno;
                $nestedData['quotno'] = $salesorder->quotno;
                $nestedData['taxed_amount'] = $salesorder->taxed_amount;
                if(Auth::user()->hasPermissionTo('SALES_OD_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function api_store(Request $request)
    {
        // return $request['search'];
        $data = Salesorder::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('lpono', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('cbinvno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('quotno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Sales Order');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Sales Order";
        $data["bclvl1"] = "Sales Order Listing";
        $data["bclvl1_url"] = route('salesorders.index');
        $data["bclvl2"] = "Add Sales Order";
        $data["bclvl2_url"] = route('salesorders.create');
        // return response()->json($data);

        return view('dailypro/salesorders.create', $data);
    }

    public function store(Request $request)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $salesorderid = Salesorder::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'lpono' => $request['lpono'],
            'ref1' => $request['ref1'],
            'cbinvno' => $request['cbinvno'],
            'quotno' => $request['quotno'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $salesorderDoc = DocumentSetup::findByName('Sales Order');
        $salesorderDoc->update(['D_LAST_NO' => $salesorderDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Salesorderdt::create([
                    'doc_no' => $salesorderid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'uprice' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal - $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }

        return redirect()->route('salesorders.edit', ['id' => $salesorderid])->with('Success', 'Sales Order added sucessfully');
    }

    public function show(Type $var = null)
    {
        return view('dailypro/salesorders.salesorders', $data);
    }

    public function edit($id)
    {
        $salesorder = Salesorder::find($id);
        $contents = Salesorderdt::where('doc_no', $salesorder->docno)->get();
        $selectedDebitor = Debtor::findByAcode($salesorder->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $salesorder;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Sales Order";
        $data["bclvl1"] = "Sales Order Listing";
        $data["bclvl1_url"] = route('salesorders.index');
        $data["bclvl2"] = "Edit Sales Order";
        $data["bclvl2_url"] = route('salesorders.edit', ['id' => $id]);

        // return response()->json( count($data["salesorderdts"]));
        return view('dailypro/salesorders.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // return response()->json($request);
        $salesorder = Salesorder::find($id);
        if (isset($salesorder)) {
            $salesorder->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'lpono' => $request['lpono'],
                'ref1' => $request['ref1'],
                'cbinvno' => $request['cbinvno'],
                'quotno' => $request['quotno'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $salesorderdt_id = Salesorderdt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if ($salesorderdt_id != null) {
                        $stkBalance = $stock->stkbal + $salesorderdt_id->totalqty;
                        $salesorderdt_id->update([
                            'doc_no' => $salesorder->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id

                        ]);

                        $stkBalance = $stock->stkbal - $salesorderdt_id->totalqty;
                    } else {
                        $dt = Salesorderdt::create([
                            'doc_no' => $salesorder->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal - $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
            // return response()->json($request['salesorderdts_id']);

        }
        // dd($salesorder);
        return redirect()->route('salesorders.edit', ['id' => $id])->with('Success', 'Sales Order updated sucessfully');
    }

    public function destroy($id)
    {

        $salesorder = Salesorder::find($id);
        $salesorder->update([
            'deleted_by' => Auth::user()->id
        ]);
        $salesorder->delete();
        $docno = $salesorder->docno;
        // return redirect()->route('salesorders.index')->with('Success', 'Sales Order deleted successfully');

        $salesorderdt = Salesorderdt::where('doc_no', $docno);
        $salesorderdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $salesorderdt->delete();
        return redirect()->route('salesorders.index')->with('Success', 'Sales Order deleted successfully');
    }


    public function destroyData($id)
    {
        $data = Salesorderdt::find($id);

        if (isset($data)) {
            $Salesorder = Salesorder::where('docno','=',$data->doc_no)->first();
            $Salesorder->update([
                'amount' => $Salesorder->amount - $data->amount,
                'taxed_amount' => $Salesorder->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroysalesorderdt($id)
    {
        $salesorder = Salesorderdt::find($id);
        $salesorder->update([
            'deleted_by' => Auth::user()->id
        ]);
        $salesorder->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper($id, Request $request)
    {
        //update printed details
        $salesorder = Salesorder::find($id);
        $getprinted = Salesorder::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($salesorder)) {
            $salesorder->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $Resources = Salesorder::where('id', $id)->get();
        $ResourcesJsonList = $this->getsalesorderListingJSON($Resources);

        $formattedResource = Salesorder::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'cbinvno as REF_NO',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'salesorder';
        if ($request->Print_radio == "with_amount") {
            $jrxml = 'general-bill-DO.jrxml';
        } else {
            $jrxml = 'general-bill-DO-NoAmount.jrxml';
        }

        $reportTitle = 'Sales Order';
        Transaction::generateBillDebtor(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Sales Order')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Sales Order')->update([
                'index' => "Sales Order",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Sales Order",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $dates = $request->dates;
        $SalesOrder = $this->getsalesorderQuery($request);

        if ($SalesOrder->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->DeliveryOrder_rcv == "listing") {
            foreach ($SalesOrder as $SO) {

                $amount = $amount + $SO->amount;
            }

            $pdf = PDF::loadView('dailypro/salesorders.listing', [
                'SalesOrderJSON' => $SalesOrder,
                'date' =>  $dates,
                'totalamount' => $amount
            ])->setPaper('A4', 'landscape');
            return $pdf->inline();
        } else {
            $pdf = PDF::loadView('dailypro/salesorders.summary', [
                'date' =>  $dates,
                'SalesOrderJSON' => $SalesOrder
            ])->setPaper('A4');
            return $pdf->inline();
        }
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/salesorder/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/salesorder/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/salesorder/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/salesorder/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getsalesorderQuery(Request $request)
    {
        return Salesorder::join('salesorderdts', 'salesorders.docno', '=', 'salesorderdts.doc_no')
            ->join('stockcodes', 'salesorderdts.item_code', '=', 'stockcodes.code')
            ->selectRaw("
        `salesorders`.`docno`          AS `docno`,
        `salesorders`.`date`           AS `date`,
        `salesorders`.`ref1`          AS `ref1`,
        `salesorders`.`lpono`          AS `lpono`,
        `salesorders`.`quotno`          AS `quotno`,
        `salesorders`.`account_code`   AS `account_code`,
        `salesorders`.`account_code`   AS `debtor`,
        `salesorders`.`name`           AS `name`,
        CONCAT(CONCAT(`salesorders`.`docno`),', ',DATE_FORMAT(`salesorders`.`date`,'%d-%m-%Y'),', ',`salesorders`.`account_code`,', ',`salesorders`.`name`) AS `details`,
        `salesorderdts`.`subject`      AS `description`,
        `salesorderdts`.`id`           AS `id`,
        `salesorderdts`.`doc_no`       AS `doc_no`,
        `salesorderdts`.`item_code`    AS `item_code`,
        `salesorderdts`.`qty`          AS `quantity`,
        `salesorderdts`.`uom`          AS `uom`,
        `salesorderdts`.`uprice`       AS `uprice`,
        `salesorderdts`.`amount`       AS `amount`,
        `salesorderdts`.`taxed_amount` AS `tax_amount`,
        `salesorderdts`.`subject`      AS `subject`,
        `salesorderdts`.`details`      AS `DETAIL`,
        `stockcodes`.`loc_id`        AS `loc_id`,
        salesorders.amount AS totalamt,
        (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location")
            ->where(function ($query) use ($request) {

                $query->whereNull('salesorders.deleted_at');
                $query->whereNull('salesorderdts.deleted_at');

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $query->where('lpono', "=", $request->LPO_frm_1);
                    } else {
                        $query->whereBetween('lpono', [$request->LPO_frm_1, $request->LPO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }
    public function getsalesorderListingJSON($salesorder)
    {
        $dataArray = array();
        foreach ($salesorder as $DO) {

            $DOTrans = Salesorderdt::select(
                'item_code',
                'subject',
                'qty',
                'uom',
                'uprice',
                'amount',
                'details',
                'taxed_amount'
            )
                ->where("doc_no", "=", $DO->docno)
                ->get();

            $lastElement = count($DOTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($DOTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($DO->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $DO->docno . ", " . $date . ", " .
                    $DO->account_code . ", " . $DO->name;
                $objectJSON['description'] = $trans->subject;
                $objectJSON['item_code'] = $trans->item_code;
                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_price'] = $trans->uprice;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $objectJSON['amount'];
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getsalesorderSummaryJSON($salesorder)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Sales Order')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Sales Order')->update([
                'index' => "Sales Order",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Sales Order",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $dataArray = array();
        $s_n = 1;
        foreach ($salesorder as $DO) {
            $DOTrans = Salesorderdt::select('amount', 'updated_at')
                ->where("doc_no", "=", $DO->docno)
                ->get();

            foreach ($DOTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $DO->docno;
                $objectJSON['date'] = $date;
                if ($DO->cbinvno != "") {
                    $objectJSON['cbinv_no'] = $DO->cbinvno;
                } else {
                    $objectJSON['cbinv_no'] = "   -";
                }
                $objectJSON['debtor'] = $DO->account_code;
                $objectJSON['name'] = $DO->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getRouteByDocno(Request $request)
    {
        $Salesorder = Salesorder::where('docno', $request->docno)->first('id');

        if (!$Salesorder) {

            $url = action('SalesorderController@create');
        } else {

            $url = action('SalesorderController@edit', ['id' => $Salesorder->id]);
        }

        return $url;
    }
}
