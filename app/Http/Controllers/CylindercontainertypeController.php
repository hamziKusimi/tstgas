<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Cylindercontainertypes;

class CylindercontainertypeController extends Controller
{
    public function index(){

        $data["cylindercontainertypes"] = Cylindercontainertypes::get();
        $data["page_title"] = "Container Type Item Listings";
        $data["bclvl1"] = "Container Type Item Listings";
        $data["bclvl1_url"] = route('cylindercontainertypes.index');

        return view('cylindermaster.cylindercontainertypes.index', $data);
    }

    public function create(){
        
        $data["page_title"] = "Add Container Type";
        $data["bclvl1"] = "Container Type Item Listings";
        $data["bclvl1_url"] = route('cylindercontainertypes.index');
        $data["bclvl2"] = "Add Container Type";
        $data["bclvl2_url"] = route('cylindercontainertypes.create');

        return view('cylindermaster.cylindercontainertypes.create', $data);
    }

    public function store(Request $request){

        $request->validate([
            'code' => 'required',
            'descr' => 'required',
            'active' => 'required'
        ]);

        Cylindercontainertypes::create($request->all());
        return redirect()->route('cylindercontainertypes.index')->with('Success', 'Cylinder Container Types created successfully.');

    }

    public function edit($id){

        $data["cylindercontainertype"] = Cylindercontainertypes::find($id);
        $data["page_title"] = "Edit Container Type";
        $data["bclvl1"] = "Container Type Item Listings";
        $data["bclvl1_url"] = route('cylindercontainertypes.index');
        $data["bclvl2"] = "Edit Container Type";
        $data["bclvl2_url"] = route('cylindercontainertypes.edit', ['id'=>$id]);

        return view('cylindermaster.cylindercontainertypes.edit', $data);
    }

    public function update(Request $request, cylindercontainertypes $cylindercontainertype){
        
        $request->validate([
            'code' => 'required',
            'descr' => 'required',
            'active' => 'required'
        ]);

        $cylindercontainertype->update($request->all());
        return redirect()->route('cylindercontainertypes.index')->with('Success', 'Cylinder Container Types updated successfully.');
    }

    public function destroy(cylindercontainertypes $cylindercontainertype)
    {
        $cylindercontainertype->delete();
        return redirect()->route('cylindercontainertypes.index')->with('Success', 'Cylinder Container Types deleted successfully.');
    }
}
