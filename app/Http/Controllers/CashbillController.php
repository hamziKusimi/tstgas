<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Cashbill;
use App\Model\Cashbilldt;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\DocumentSetup;
use App\Model\Deliveryorder;
use App\Model\Invoice;
use App\Model\Deliveryorderdt;
use App\Model\SystemSetup;
use App\Model\PrintedIndexView;
use App\Model\Salesman;
use App\Model\Area;
use App\Model\Uom;
use App\Model\ViewTransaction;
use App\Model\Location;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use DB;
use DataTables;
use Illuminate\Support\Facades\Input;
use PDF;
use App\ExportsCashbillReport\ExportCashbillReport;
use Excel;

class CashbillController extends Controller
{
    public function get_print_details()
    {
        $data = [];
        $data["docno_select"] = Cashbill::pluck('docno', 'docno')->toArray();
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode')->toArray();

        return response()->json($data);
    }

    public function editRoute($id)
    {
        $response1 = Cashbill::where('id', $id)->pluck('docno')->toArray();
        $response2 = Cashbill::where('id', $id)->pluck('docno')->toArray();

        $responses = array_merge($responses, $response1, $response2);


        return response()->json($responses);
    }

    public function index()
    {
        $data["debtor_select"] = Debtor::where('active', '<>', '0')->orderBy('accountcode')->pluck('accountcode', 'accountcode')->toArray();
        $data["salesman_select"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->toArray();
        $data["print"] = PrintedIndexView::where('index', 'Cash Bill')->pluck('printed_by');
        $data["page_title"] = "Cash Bill Listing";
        $data["bclvl1"] = "Cash Bill Listing";
        $data["bclvl1_url"] = route('cashbills.index');

        return view('dailypro/cashbills.index', $data);
    }

    public function cashbillDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'lpono',
                            6 => 'dono',
                            7 => 'quotno',
                            8 => 'taxed_amount',
                        );

        $totalData = Cashbill::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Cashbill::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Cashbill::where('id','LIKE',"%{$search}%")
                            ->orWhere('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('lpono', 'LIKE',"%{$search}%")
                            ->orWhere('dono', 'LIKE',"%{$search}%")
                            ->orWhere('quotno', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Cashbill::where('id','LIKE',"%{$search}%")
                             ->orWhere('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->orWhere('lpono', 'LIKE',"%{$search}%")
                             ->orWhere('dono', 'LIKE',"%{$search}%")
                             ->orWhere('quotno', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $key => $post)
            {
                $delete =  route('cashbills.destroy', $post->id);
                $edit =  route('cashbills.edit', $post->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('CASH_BILL_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$post->docno.'</a>';
                }else{
                    $nestedData['docno'] = $post->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($post->date));
                $nestedData['account_code'] = $post->account_code;
                $nestedData['name'] = $post->name;
                $nestedData['lpono'] = $post->lpono;
                $nestedData['dono'] = $post->dono;
                $nestedData['quotno'] = $post->quotno;
                $nestedData['taxed_amount'] = $post->taxed_amount;

                if(Auth::user()->hasPermissionTo('CASH_BILL_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }

                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function docnoList()
    {
        $query = Cashbill::query()
            ->selectRaw('docno, date, name');

        return DataTables::of($query)->make(true);
    }

    public function debtorList()
    {
        $query = Debtor::query()
            ->selectRaw('accountcode, name')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }

    public function salesmanList()
    {
        $query = Salesman::query()
            ->selectRaw('code, descr')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }

    public function searchDocno(Request $request)
    {
        $data = Cashbill::select('id', 'docno', 'date', 'name')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('docno');

        // $data2 = Invoice::select('id', 'docno', 'date', 'name')
        //     ->where(function ($query) use ($request) {
        //         $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%');
        //     })
        //     ->orderBy('date');

        // $results = $data->union($data2)->get();

        $results = $data->get();

        return response()->json($results);
    }

    public function searchindex(Request $request)
    {

        $cashbills = Cashbill::searchindex($request->search)
                    ->latest()
                    ->paginate(30)
                    ->appends(['search'=>$request->search]);

        $data["cashbills"] = $cashbills;
        // $data["docno_select"] = Cashbill::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::where('active', '<>', '0')->orderBy('accountcode')->pluck('accountcode', 'accountcode');
        $data["salesman_select"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->toArray();
        $data["print"] = PrintedIndexView::where('index', 'Cash Bill')->pluck('printed_by');
        $data["page_title"] = "Cash Bill  Listing";
        $data["bclvl1"] = "Cash Bill  Listing";
        $data["bclvl1_url"] = route('cashbills.index');
        return view('dailypro.cashbills.index', $data);
    }

    public function getAuthenticateAjax(Request $request)
    {
        if (Auth::guard('admin')->attempt($credentials)) {
            return true;
        }
    }

    public function dono_get($debtor)
    {
        $response = Deliveryorder::where('account_code', $debtor)->get();

        return response()->json($response);
    }

    public function getDOrderDt(Request $request)
    {
        $dorders = Deliveryorderdt::where('doc_no', $request['docno'])->orderBy('sequence_no')->get();

        return response()->json($dorders);
    }

    public function api_store(Request $request)
    {
        $data = Cashbill::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('lpono', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('dono', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('quotno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Cash Bill');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();
        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->get()->pluck('Detail', 'code');
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Cash Bill";
        $data["bclvl1"] = "Cash Bill Listing";
        $data["bclvl1_url"] = route('cashbills.index');
        $data["bclvl2"] = "Add Cash Bill";
        $data["bclvl2_url"] = route('cashbills.create');

        // $result1 = DB::table('invoice_data')->where('account_code', $latestprice)->where('item_code', $request["item"])->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        // dd($result1);
        return view('dailypro.cashbills.create', $data);
    }

    public function store(Request $request)
    {
        $cbNo = $request['doc_no'];
        DB::statement("
            DELETE FROM cashbills
            WHERE docno='$cbNo' AND deleted_at IS NOT NULL;
        ");

        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $cashbillid = Cashbill::updateOrCreate([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'lpono' => $request['lpono'],
            'ref1' => $request['ref1'],
            'dono' => $request['dono'],
            'quotno' => $request['quotno'],
            'salesman' => $request['salesman'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            // 'currency' => !empty($request['currency']) ? $request['currency'] : '',
            // 'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
            // 'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
            'created_by' => Auth::user()->id
        ],[]);
        $CashbillDoc = DocumentSetup::findByName('Cash Bill');
        $CashbillDoc->update(['D_LAST_NO' => $CashbillDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Cashbilldt::create([
                    'doc_no' => $cashbillid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'reference_no' => $request['reference_no'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'uprice' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal - $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }

        // dd($cashbillid->id);
        return redirect()->route('cashbills.edit', ['id' => $cashbillid->id])->with('Success', 'Cash Bill added sucessfully');
    }

    public function show($id)
    { }

    public function edit($id)
    {
        $cashbill = Cashbill::find($id);
        $contents = CashbillDt::where('doc_no', $cashbill->docno)->get();

        $selectedDebitor = Debtor::findByAcode($cashbill->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $cashbill;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->get()->pluck('Detail', 'code');
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Cash Bill";
        $data["bclvl1"] = "Cash Bill Listing";
        $data["bclvl1_url"] = route('cashbills.index');
        $data["bclvl2"] = "Edit Cash Bill";
        $data["bclvl2_url"] = route('cashbills.edit', ['id' => $id]);

        // return response()->json( count($data["cashbilldts"]));
        // return response()->json($contents);
        return view('dailypro/cashbills.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);

        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $cashbill = Cashbill::find($id);
        if (isset($cashbill)) {
            $cashbill->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'lpono' => $request['lpono'],
                'ref1' => $request['ref1'],
                'dono' => $request['dono'],
                'quotno' => $request['quotno'],
                'salesman' => $request['salesman'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                // 'currency' => !empty($request['currency']) ? $request['currency'] : '',
                // 'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                // 'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $cashbilldt_id = Cashbilldt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if ($cashbilldt_id != null) {
                        $stkBalance = $stock->stkbal + $cashbilldt_id->totalqty;
                        $cashbilldt_id->update([
                            'doc_no' => $cashbill->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stkBalance - $cashbilldt_id->totalqty;
                    } else {
                        $dt = Cashbilldt::create([
                            'doc_no' => $cashbill->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal - $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
            // return response()->json($request['cashbilldts_id']);

        }

        return redirect()->route('cashbills.edit', ['id' => $id])->with('Success', 'Cash Bill updated sucessfully');
    }

    public function destroy($id)
    {

        $cashbill = Cashbill::find($id);
        $cashbill->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cashbill->delete();
        $docno = $cashbill->docno;

        $cashbilldt = Cashbilldt::where('doc_no', $docno);
        $cashbilldt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cashbilldt->delete();
        return redirect()->route('cashbills.index')->with('Success', 'Cash Bill deleted successfully');
    }

    public function destroyData($id)
    {
        $data = Cashbilldt::find($id);

        if (isset($data)) {
            $cashbill = Cashbill::where('docno','=',$data->doc_no)->first();
            $cashbill->update([
                'amount' => $cashbill->amount - $data->amount,
                'taxed_amount' => $cashbill->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyCashbilldt($id)
    {
        $cashbilldt = Cashbilldt::find($id);
        $cashbilldt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cashbilldt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper(Request $request)
    {
        //update printed details
        $cashbill = Cashbill::find($request->id);
        $getprinted = Cashbill::where('id', $request->id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($cashbill)) {
            $cashbill->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $Resources = Cashbill::where('id', $request->id)->get();
        $ResourcesJsonList = $this->getCashbillListingJSON($Resources);

        $formattedResource = Cashbill::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'dono as DO_NO',
            'lpono as REF_NO',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'discount as DISCOUNT',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $request->id)->first();

        if($request->docType == 'A11'){
            return $this->printReceipt($formattedResource, json_decode($ResourcesJsonList));
        }

        $source = 'CashBill';
        $jrxml = 'general-bill-two.jrxml';
        $reportTitle = 'Cash Bill';
        $jasperData = Transaction::generateBillDebtor(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Cash Bill')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Cash Bill')->update([
                'index' => "Cash Bill",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Cash Bill",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;

        if($request->cashbill_rcv =='reprint'){

            return $this->reprint($request);
        }

        $CashBill = $this->getcashbillQuery($request);

        $dates = $request->dates;
        $data['date'] = $dates;

        $systemsetup = SystemSetup::pluck('qty_decimal')->first();
        $systemsetupstock = SystemSetup::pluck('stock_item')->first();
        $data['systemsetup'] = $systemsetup;
        // $data['systemsetupstock'] = $systemsetupstock;
        $decimal = ($request->decimal== "decimal")?'decimal':'notdecimal';
        $data['decimal'] = $decimal;
        if (empty($CashBill)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->cashbill_rcv == "listing") {
            $reportType = "CashBillListing";
            foreach ($CashBill as $cb) {

                $amount = $amount + $cb->amount;
            }

            if($request->submitBtn == "pdf"){
                $pdf = PDF::loadView('dailypro/cashbills.listing', [
                    'reportType' => $reportType,
                    'totalqty' => "",
                    'cashbillJSON' => $CashBill,
                    'finaltotalamount' => $amount,
                    'date' =>  $dates,
                    'systemsetup' =>  $systemsetup,
                ])->setPaper('A4', 'landscape');
                return $pdf->inline();
            }else{
                $data['reportType'] = $reportType;
                $data['cashbillJSON'] = $CashBill;
                $data['finaltotalamount'] = $amount;
                $data['date'] = $dates;
                $data['systemsetup'] = $systemsetup;

                return Excel::download(new ExportCashbillReport($data, "listing"), 'CashbillReportListing.xlsx');
            }

        }else{
            foreach ($CashBill as $cb) {

                $amount = $amount + $cb->totalamount;
            }

            if($request->submitBtn == "pdf"){
                $reportType = "CashBillSummary";
                $pdf = PDF::loadView('dailypro/cashbills.summary', [
                    'reportType' => $reportType,
                    'cashbillJSON' => $CashBill,
                    'finaltotalamount' => $amount,
                    'date' =>  $dates,
                    'systemsetup' =>  $systemsetup,
                ])->setPaper('A4');
                return $pdf->inline();
            }else{
                $data['cashbillJSON'] = $CashBill;
                $data['finaltotalamount'] = $amount;
                $data['date'] = $dates;
                $data['systemsetup'] = $systemsetup;

                return Excel::download(new ExportCashbillReport($data, "summary"), 'CashbillReportSummary.xlsx');
            }

        }
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/CashBill/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/CashBill/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/CashBill/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        // dd(file_get_contents($data_file));

        $jasper = new PHPJasper;

        $jasper->process($input, $output, $options)->execute();
        // $jasper->process($input,$output,$options)->output();
        // dd($jasper);
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/CashBill/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function printReceipt($formattedResource, $ResourcesJsonList){
        $systemSetup = SystemSetup::first();

        $data['coyaddr1'] =  $systemSetup->address1;
        $data['coyaddr2'] =  $systemSetup->address2;
        $data['coyaddr3'] =  $systemSetup->address3;
        $data['coyaddr4'] =  $systemSetup->address4;

        $data['coytel'] =  $systemSetup->tel;
        $data['coyfax'] =  $systemSetup->fax;
        $data['coyemail'] =  $systemSetup->email;
        $data['coysstno'] =  "";

        $data['cashbillmt'] = $formattedResource->toArray();
        $data['cashbilldt'] = $ResourcesJsonList->data;

        return view('dailypro/cashbills.receipt', $data);
    }

    public function getcashbillQuery(Request $request)
    {
        $systemsetupstock = SystemSetup::pluck('stock_item')->first();

        if($request->cashbill_rcv == "summary"){
            $type = "GROUP BY docno";
            if($systemsetupstock == '1'){

                // $stock = "AND stockcodes.type = 'Stock Item'";
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM cashbilldts INNER JOIN stockcodes on ((cashbilldts.item_code = stockcodes.code) and (stockcodes.type = 'Stock Item'))
                WHERE cashbilldts.item_code = item_code and cashbilldts.doc_no = docno and cashbilldts.deleted_at IS NULL) AS totalqty,";

            }else{
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM cashbilldts WHERE cashbilldts.item_code = item_code and cashbilldts.doc_no = docno and cashbilldts.deleted_at IS NULL) AS totalqty,";

            }
        }else{
            $type = "GROUP BY docno, subject, item_code";
            $stock = "";
            $totalqty = "";
        }

        if ($request->dates_Chkbx == "on") {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

            if ($dates_from == $dates_to) {
                $date = "AND cashbills.date = '" . $dates_from . "'";
            } else {
                $date = "AND cashbills.date >= '" . $dates_from . "' AND cashbills.date <= '" . $dates_to . "'";
            }

            if ($request->docno_Chkbx == "on") {
                $docno = "AND (cashbills.docno >= '" . $request->docno_frm . "' AND cashbills.docno <= '" . $request->docno_to . "')";

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cashbills.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cashbills.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }

                        $debtor = "AND (cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "')";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                }
            } else {
                $docno = '';

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cashbills.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cashbills.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "')";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "')";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                }
            }
        } else {
            $date = '';
            if ($request->docno_Chkbx == "on") {
                $docno = "AND cashbills.docno >= '" . $request->docno_frm . "' AND cashbills.docno <= '" . $request->docno_to . "'";

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cashbills.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cashbills.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "')";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "')";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                }
            } else {
                $docno = '';

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND cashbills.lpono = '" . $request->LPO_frm_1 . "'";
                    } else {
                        $lpono = "AND cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cashbills.lpono <= '" . $request->LPO_to_1 . "'";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "')";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND cashbills.account_code >= '" . $request->debCode_frm . "' AND cashbills.account_code <= '" . $request->debCode_to . "'";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (cashbills.salesman >= '" . $request->salesman_frm . "' AND cashbills.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                }
            }
        }


        $CashBill = DB::select(DB::raw("

                SELECT
                `cashbills`.`docno`          AS `docno`,
                `cashbills`.`date`           AS `date`,
                `cashbills`.`lpono`          AS `lpono`,
                `cashbills`.`account_code`   AS `account_code`,
                `cashbills`.`account_code`   AS `debtor`,
                `cashbills`.`name`           AS `name`,
                IF(`cashbills`.`amount` IS NULL,'0',`cashbills`.`amount`) AS `totalamount`,
                CONCAT(CONCAT(`cashbills`.`docno`),', ',DATE_FORMAT(`cashbills`.`date`,'%d-%m-%Y'),', ',`cashbills`.`account_code`,', ',`cashbills`.`name`) AS `details`,
                `cashbilldts`.`subject`      AS `description`,
                `cashbilldts`.`id`           AS `id`,
                `cashbilldts`.`doc_no`       AS `doc_no`,
                `cashbilldts`.`item_code`    AS `item_code`,
                IF(`cashbilldts`.`qty` IS NULL,'0',`cashbilldts`.`qty`)   AS `quantity`,
                `cashbilldts`.`uom`          AS `uom`,
                IF(`cashbilldts`.`uprice` IS NULL,'0',`cashbilldts`.`uprice`) AS `unit_price`,
                IF(`cashbilldts`.`amount` IS NULL,'0',`cashbilldts`.`amount`)  AS `amount`,
                IF(`cashbilldts`.`taxed_amount` IS NULL,'0',`cashbilldts`.`taxed_amount`) AS `tax_amount`,
                `cashbilldts`.`subject`      AS `subject`,
                `cashbilldts`.`details`      AS `DETAIL`,
                `stockcodes`.`loc_id`        AS `loc_id`,
                IF(cashbills.amount IS NULL,'0',cashbills.amount) AS totalamt,
                $totalqty
                (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location
                FROM ((`cashbills`
                JOIN `cashbilldts`
                    ON ((`cashbills`.`docno` = `cashbilldts`.`doc_no`)))
                JOIN `stockcodes`
                ON ((`cashbilldts`.`item_code` = `stockcodes`.`code`)))
                WHERE `cashbilldts`.`deleted_at` IS NULL AND `cashbills`.`deleted_at` IS NULL
                $stock $date $docno $lpono $debtor $salesman
                $type
                order by docno


        "), []);

        return $CashBill;
    }

    public function getcashbillListingJSON($CashBills)
    {
        $dataArray = array();
        foreach ($CashBills as $cashbill) {

            $cashbillTrans = Cashbilldt::select(
                'item_code',
                'subject',
                'qty',
                'rate',
                'uom',
                'uprice',
                'discount',
                'amount',
                'details',
                'taxed_amount'
            )->where("doc_no", "=", $cashbill->docno)
            ->get();

            $lastElement = count($cashbillTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;


            foreach ($cashbillTrans as $trans) {

                $objectJSON = [];
                $date = date("d/m/Y", strtotime($cashbill->date));
                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $cashbill->docno . ", " . $date . ", " .
                $cashbill->account_code . ", " . $cashbill->name;
                $objectJSON['description'] = $trans->subject;
                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['item_code'] = $trans->item_code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['rate'] = $trans->rate;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_price'] = $trans->uprice;
                $objectJSON['discount'] = $trans->discount;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $trans->amount;
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getCashBillSummaryJSON($cashbills)
    {
        // return response()->json($data);
        $dataArray = array();
        $s_n = 1;
        foreach ($cashbills as $cbill) {

            $cbillTrans = Cashbilldt::select('amount', 'updated_at')
                ->where("doc_no", "=", $cbill->docno)
                ->get();

            foreach ($cbillTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $cbill->docno;
                $objectJSON['date'] = $date;
                $objectJSON['lpono'] = $cbill->lpono;
                $objectJSON['debtor'] = $cbill->account_code;
                $objectJSON['name'] = $cbill->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';

        // return $dataArray;
    }

    public function getLatestPrice(Request $request, $latestprice)
    {

        // $result = DB::table('debtors')
        //     ->leftJoin('cashbilldts', 'debtors.accountcode', '=', 'cashbilldts.account_code')
        //     ->leftJoin('cashsalesdts', 'debtors.accountcode', '=', 'cashsalesdts.account_code')
        //     ->leftJoin('invoice_data', 'debtors.accountcode', '=', 'invoice_data.account_code')
        //     ->leftJoin('deliveryorderdts', 'debtors.accountcode', '=', 'deliveryorderdts.account_code')
        //     ->leftJoin('salesorderdts', 'debtors.accountcode', '=', 'salesorderdts.account_code')
        //     ->leftJoin('quotationdts', 'debtors.accountcode', '=', 'quotationdts.account_code')
        //     ->leftJoin('adjustmentidts', 'debtors.accountcode', '=', 'adjustmentidts.account_code')
        //     ->leftJoin('adjustmentodts', 'debtors.accountcode', '=', 'adjustmentodts.account_code')
        //     ->leftJoin('salesreturndts', 'debtors.accountcode', '=', 'salesreturndts.account_code')
        //     ->where('debtors.accountcode', $latestprice)
        //     ->select('cashbilldts.uprice AS cashbill', 'invoice_data.unit_price AS invoice', 'cashsalesdts.uprice AS cashsale',
        //             'deliveryorderdts.uprice AS deliveryorder', 'salesorderdts.uprice    AS salesorder', 'quotationdts.uprice AS quotation',
        //             'adjustmentidts.ucost AS adjustmenti', 'adjustmentodts.ucost AS adjustmento', 'salesreturndts.uprice AS salesreturn',
        //             'cashbilldts.updated_at AS cashbilldate',
        //             'cashsalesdts.updated_at AS cashsalesdate', 'invoice_data.updated_at AS invoicedate',
        //             'deliveryorderdts.updated_at AS deliveryorderdate', 'salesorderdts.updated_at AS salesorderdate',
        //             'quotationdts.updated_at AS quotationdate',
        //             'adjustmentidts.updated_at AS adjustmentidate', 'adjustmentodts.updated_at AS adjustmentodate',
        //             'salesreturndts.updated_at AS salesreturndate')
        //     ->get();

        $result1 = DB::table('invoice_data')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result1 != '') && ($result1 != null)) {
            $invoice = $result1->updated_at;
            $invoiceprice = $result1->unit_price;
        } else {
            $invoice = '';
            $invoiceprice = '';
        }

        $result2 = DB::table('cashbilldts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result2 != '') && ($result2 != null)) {
            $cashbill = $result2->updated_at;
            $cashbillprice = $result2->uprice;
        } else {
            $cashbill = '';
            $cashbillprice = '';
        }

        $result3 = DB::table('cashsalesdts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result3 != '') && ($result3 != null)) {
            $cashsales = $result3->updated_at;
            $cashsaleprice = $result3->uprice;
        } else {
            $cashsales = '';
            $cashsaleprice = '';
        }

        $result4 = DB::table('deliveryorderdts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result4 != '') && ($result4 != null)) {
            $deliveryorder = $result4->updated_at;
            $deliveryorderprice = $result4->uprice;
        } else {
            $deliveryorder = '';
            $deliveryorderprice = '';
        }

        $result5 = DB::table('salesorderdts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result5 != '') && ($result5 != null)) {
            $salesorder = $result5->updated_at;
            $salesorderprice = $result5->uprice;
        } else {
            $salesorder = '';
            $salesorderprice = '';
        }

        $result6 = DB::table('quotationdts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result6 != '') && ($result6 != null)) {
            $quotation = $result6->updated_at;
            $quotationprice = $result6->uprice;
        } else {
            $quotation = '';
            $quotationprice = '';
        }

        $result7 = DB::table('adjustmentidts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result7 != '') && ($result7 != null)) {
            $adjustmenti = $result7->updated_at;
            $adjustmentiprice = $result7->ucost;
        } else {
            $adjustmenti = '';
            $adjustmentiprice = '';
        }

        $result8 = DB::table('adjustmentodts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result8 != '') && ($result8 != null)) {
            $adjustmento = $result8->updated_at;
            $adjustmentoprice = $result8->ucost;
        } else {
            $adjustmento = '';
            $adjustmentoprice = '';
        }

        $result9 = DB::table('salesreturndts')->where('account_code', $latestprice)->where('item_code', $request["item"])->orderBy('updated_at', 'desc')->first();

        if (($result9 != '') && ($result9 != null)) {
            $salesreturn = $result9->updated_at;
            $salesreturnprice = $result9->uprice;
        } else {
            $salesreturn = '';
            $salesreturnprice = '';
        }

        $array = [];
        $array = array(
            $cashbill => $cashbillprice, $cashsales => $cashsaleprice, $invoice => $invoiceprice, $deliveryorder => $deliveryorderprice,
            $salesorder => $salesorderprice, $quotation => $quotationprice, $adjustmenti => $adjustmentiprice, $adjustmento => $adjustmentoprice,
            $salesreturn => $salesreturnprice
        );

        $result = max($array);


        return $result;
    }

    public function getRouteByDocno(Request $request)
    {
        $cashBill = Cashbill::where('docno', $request->docno)->first('id');

        if (!$cashBill) {

            // Session::flash('message', 'This is a message!');
            $url = action('CashbillController@create');
        } else {

            $url = action('CashbillController@edit', ['id' => $cashBill->id]);
        }

        return $url;
    }

    public function reprint($request){

        $cashbills = Cashbill::join('cashbilldts', 'cashbilldts.doc_no', '=', 'cashbills.docno')
        ->selectRaw("
                cashbills.docno as DOC_NO,
                cashbills.date as DOC_DATE,
                cashbills.account_code as ACC_CODE,
                cashbills.name as ACC_HOLDER,
                cashbills.dono as DO_NO,
                cashbills.lpono as REF_NO,
                cashbills.addr1 as ADDR1,
                cashbills.addr2 as ADDR2,
                cashbills.addr3 as ADDR3,
                cashbills.addr4 as ADDR4,
                cashbills.tel_no as TEL,
                cashbills.fax_no as FAX,
                cashbills.amount as STOTAL,
                cashbills.taxed_amount as GRANDTOTAL,
                cashbills.header as HEADER,
                cashbills.footer as FOOTER,
                cashbills.summary as SUMMARY,
                cashbilldts.item_code,
                cashbilldts.subject,
                cashbilldts.qty,
                cashbilldts.uom,
                cashbilldts.uprice,
                cashbilldts.amount as amounts,
                cashbilldts.details,
                cashbilldts.taxed_amount as taxed_amounts
        ")
        ->whereRaw("
        cashbilldts.deleted_at IS NULL AND cashbills.deleted_at IS NULL")
        ->where(function ($query) use ( $request) {

            if($request->dates_Chkbx =='on'){
                $dates = explode("-", $request->dates);
                $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }
            }

            if($request->docno_Chkbx =='on'){
                if ($request->docno_frm == $request->docno_to) {
                    $query->where('docno', '=',$request->docno_frm);
                } else {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }
            }

            if($request->debCode_Chkbx == 'on'){
                if ($request->debCode_frm == $request->debCode_to) {
                    $query->where('cashbills.account_code', '=',$request->debCode_frm);
                } else {
                    $query->whereBetween('cashbills.account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            }

            if($request->salesman_Chkbx == 'on'){
                if ($request->salesman_frm == $request->salesman_to) {
                    $query->where('salesman', '=',$request->salesman_frm);
                } else {
                    $query->whereBetween('salesman', [$request->salesman_frm, $request->salesman_to]);
                }
            }

            if($request->LPO_Chkbx_1 == 'on'){
                if ($request->LPO_frm_1 == $request->LPO_to_1) {
                    $query->where('salesman', '=',$request->LPO_frm_1);
                } else {
                    $query->whereBetween('salesman', [$request->LPO_frm_1, $request->LPO_to_1]);
                }
            }

        })->orderBy('docno')
        ->get();

        $source = 'CashBill';
        $jrxml = 'general-bill-two-reprint.jrxml';

        Transaction::generateBillReprint(
            $cashbills,
            $source,
            $jrxml
        );
    }
}
