<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ManyFor;

class ManyforController extends Controller
{
   public function index()
   {
        $data["page_title"] = "Many For Item Listing";
        $data["bclvl1"] = "Many For Item Listing";
        $data['bclvl1_url'] = route('shackles.index');

        return view('cylindermaster.manyfor.index', $data);
   }

   public function create()
   {
      $data["page_title"] = "Add Many For";
      $data["bclvl1"] = "Many For Item Listing";
      $data['bclvl1_url'] = route('shackles.index');
      $data["bclvl2"] = "Add Many For";
      $data['bclvl2_url'] = route('shackles.create');
   
      return view('cylindermaster.manyfor.create', $data);
   }
}
