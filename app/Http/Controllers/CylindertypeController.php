<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Cylindertype;
use Auth;

class CylindertypeController extends Controller
{
    public function index()
    {
        $data["cylindertypes"] = Cylindertype::get();
        $data["page_title"] = "Type Item Listing";
        $data["bclvl1"] = "Type Item Listing";
        $data["bclvl1_url"] = route('cylindertypes.index');

        return view('cylindermaster.cylindertypes.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Type";
        $data["bclvl1"] = "Type Item Listing";
        $data["bclvl1_url"] = route('cylindertypes.index');
        $data["bclvl2"] = "Add Type";
        $data["bclvl2_url"] = route('cylindertypes.create');

        return view('cylindermaster.cylindertypes.create', $data);
    }

    public function store(Request $request)
    {
        $cylindertype = Cylindertype::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('cylindertypes.index')->with('Success', 'Types created successfully');
    }

    public function edit($id)
    {
        $data["cylindertype"] = Cylindertype::find($id);
        $data["page_title"] = "Edit Type";
        $data["bclvl1"] = "Type Item Listing";
        $data["bclvl1_url"] = route('cylindertypes.index');
        $data["bclvl2"] = "Edit Type";
        $data["bclvl2_url"] = route('cylindertypes.edit', ['id'=>$id]);

        return view('cylindermaster.cylindertypes.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cylindertype = Cylindertype::find($id);
        if (isset($cylindertype)) {
            $cylindertype->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('cylindertypes.index')->with('Success', 'Types updated successfully');
    }

    public function destroy(cylindertype $cylindertype)
    {
        $cylindertype->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylindertype->delete();
        return redirect()->route('cylindertypes.index')->with('Success', 'Types deleted successfully');
    }
}
