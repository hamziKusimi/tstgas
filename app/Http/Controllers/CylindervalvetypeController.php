<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Cylindervalvetype;
use Auth;

class CylindervalvetypeController extends Controller
{
    public function index()
    {
        $data["cylindervalvetypes"] = Cylindervalvetype::get();
        $data["page_title"] = "Cylinder Valve Type Item Listing";
        $data["bclvl1"] = "Cylinder Valve Type Item Listing";
        $data["bclvl1_url"] = route('cylindervalvetypes.index');

        return view('cylindermaster.cylindervalvetypes.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Cylinder Valve";
        $data["bclvl1"] = "Cylinder Valve Type Item Listing";
        $data["bclvl1_url"] = route('cylindervalvetypes.index');
        $data["bclvl2"] = "Add Cylinder Valve";
        $data["bclvl2_url"] = route('cylindervalvetypes.create');

        return view('cylindermaster.cylindervalvetypes.create', $data);
    }

    public function store(Request $request)
    {
        $cylindervalvetype = Cylindervalvetype::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('cylindervalvetypes.index')->with('Success', 'Cylinder Valve created successfully');
    }

    public function edit($id)
    {
        $data["cylindervalvetype"] = Cylindervalvetype::find($id);
        $data["page_title"] = "Edit Cylinder Valve";
        $data["bclvl1"] = "Cylinder Valve Type Item Listing";
        $data["bclvl1_url"] = route('cylindervalvetypes.index');
        $data["bclvl2"] = "Edit Cylinder Valve";
        $data["bclvl2_url"] = route('cylindervalvetypes.edit', ['id'=>$id]);

        return view('cylindermaster.cylindervalvetypes.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cylindervalvetype = Cylindervalvetype::find($id);
        if (isset($cylindervalvetype)) {
            $cylindervalvetype->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('cylindervalvetypes.index')->with('Success', 'Cylinder Valve updated successfully');
    }

    public function destroy(cylindervalvetype $cylindervalvetype)
    {
        $cylindervalvetype->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylindervalvetype->delete();
        return redirect()->route('cylindervalvetypes.index')->with('Success', 'Cylinder Valve deleted successfully');
    }
}
