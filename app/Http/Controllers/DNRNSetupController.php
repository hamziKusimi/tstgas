<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\DNRNSetup;

class DNRNSetupController extends Controller
{
    public function index()
    {
        $data["dnrnSetups"] = DNRNSetup::get();
        $dnrnsetups =  $data["dnrnSetups"];
        $data["page_title"] = "Delivery/Return Note Setup";
        $data["bclvl1"] = "Delivery/Return Note Setup Item Listing";
        $data["bclvl1_url"] = route('dnrnsetups.index');
       
        return view('settings.dnrnsetups.index', $data);
    }

    public function create()
    {
        $data["dnrnSetups"] = DNRNSetup::get();
        $data["page_title"] = "Edit Delivery/Return Note Setup";
        $data["bclvl1"] = "Delivery/Return Note Setup Item Listing";
        $data["bclvl1_url"] = route('dnrnsetups.index');
        $data["bclvl2"] = "Edit Delivery/Return Note Setup";
        $data["bclvl2_url"] = route('dnrnsetups.create');

        return view('settings.dnrnsetups.create', $data);
    }

    public function update(Request $request, $id)
    {
        // $id = $request['RN_ID'];
        // return response()->json($request);
        if (count($request['RN_D_PREFIX']) > 0) {   
            // return response()->json($request['RN_D_PREFIX']);
            for ($i = 0; $i < count($request['RN_D_PREFIX']); $i++) {
                $docsetup = DNRNSetup::find($request['RN_ID'][$i]);
                $docsetup->update([
                    'D_PREFIX'=>$request['RN_D_PREFIX'][$i],
                    'D_SEPARATOR'=>$request['RN_D_SEPARATOR'][$i],
                    'D_LAST_NO'=>$request['RN_D_LAST_NO'][$i],
                    'D_ZEROES'=>$request['RN_D_ZEROES'][$i],
                    'D_ACTIVE'=>isset($request['RN_ACTIVE'])? $request['RN_ACTIVE'][$i]: 0,
                ]);
            }
        }

        return redirect()->route('dnrnsetups.index')->with('Success', 'Delivery/Return Note Setups successfully');
    }
}
