<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Ownership;
use Auth;

class OwnershipController extends Controller
{
    public function index()
    {
        $data["ownerships"] = Ownership::get();
        $data["page_title"] = "Ownership Item Listings";
        $data["bclvl1"] = "Ownership Item Listings";
        $data["bclvl1_url"] = route('ownerships.index');
        return view('cylindermaster.ownerships.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Ownership";
        $data["bclvl1"] = "Ownership Item Listings";
        $data["bclvl1_url"] = route('ownerships.index');
        $data["bclvl2"] = "Add Ownership";
        $data["bclvl2_url"] = route('ownerships.create');

        return view('cylindermaster.ownerships.create', $data);
    }

    public function store(Request $request)
    {
        $ownership = Ownership::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('ownerships.index')->with('Success', 'Ownership created successfully.');
    }

    public function edit($id)
    {
        $data["ownership"] = Ownership::find($id);
        $data["page_title"] = "Edit Ownership";
        $data["bclvl1"] = "Ownership Item Listings";
        $data["bclvl1_url"] = route('ownerships.index');
        $data["bclvl2"] = "Edit Ownership";
        $data["bclvl2_url"] = route('ownerships.edit', ['id'=>$id]);

        return view('cylindermaster.ownerships.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $ownership = Ownership::find($id);
        if (isset($ownership)) {
            $ownership->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('ownerships.index')->with('Success', 'Ownership updated successfully.');
    }

    public function destroy(ownership $ownership)
    {
        $ownership->update([
            'deleted_by' => Auth::user()->id
        ]);
        $ownership->delete();
        return redirect()->route('ownerships.index')->with('Success', 'Ownership deleted successfully.');
    }
}
