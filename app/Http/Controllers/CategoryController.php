<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\SystemSetup;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use App\Exports\CategoryExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class CategoryController extends Controller
{
    public function Index()
    {
        $data["category"] = Category::get();
        $data["systemSetup"] = SystemSetup::first();
        $data["page_title"] = "Category Item Listing";
        $data["bclvl1"] = "Category Item Listing";
        $data["bclvl1_url"] = route('category.index');

        return view('stockmaster.category.index', $data);
    }

    public function create()
    {
        $data["systemSetup"] = SystemSetup::first();
        $data["page_title"] = "Add Category";
        $data["bclvl1"] = "Category Item Listing";
        $data["bclvl1_url"] = route('category.index');
        $data["bclvl2"] = "Add Category";
        $data["bclvl2_url"] = route('category.create');

        return view('stockmaster.category.create', $data);
    }

    public function store(Request $request)
    {
        $categoryid = Category::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'dr_cashpurchase_acc' => $request['dr_cashpurchase_acc'],
            'dr_creditpurcase_acc' => $request['dr_creditpurchase_acc'],
            'cr_purchasereturn_acc' => $request['cr_purchasereturn_acc'],
            'cr_cashsales_acc' => $request['cr_cashsales_acc'],
            'dr_cashsales_return_acc' => $request['dr_cashsales_return_acc'],
            'cr_invoicesales_acc' => $request['cr_invoicesales_acc'],
            'dr_creditsales_return_acc' => $request['dr_creditsales_return_acc'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('category.index')->with('Success', 'Category created successfully.');
    }

    public function edit($id)
    {
        $data["category"] = Category::find($id);
        $data["systemSetup"] = SystemSetup::first();
        $data["page_title"] = "Edit Category";
        $data["bclvl1"] = "Category Item Listing";
        $data["bclvl1_url"] = route('category.index');
        $data["bclvl2"] = "Edit Category";
        $data["bclvl2_url"] = route('category.edit', ['id' => $id]);

        return view('stockmaster.category.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        if (isset($category)) {
            $category->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'dr_cashpurchase_acc' => $request['dr_cashpurchase_acc'],
                'dr_creditpurcase_acc' => $request['dr_creditpurchase_acc'],
                'cr_purchasereturn_acc' => $request['cr_purchasereturn_acc'],
                'cr_cashsales_acc' => $request['cr_cashsales_acc'],
                'dr_cashsales_return_acc' => $request['dr_cashsales_return_acc'],
                'cr_invoicesales_acc' => $request['cr_invoicesales_acc'],
                'dr_creditsales_return_acc' => $request['dr_creditsales_return_acc'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('category.index')->with('Success', 'Category updated successfully.');
    }

    public function destroy(category $category)
    {
        $category->update([
            'deleted_by' => Auth::user()->id
        ]);
        $category->delete();
        return redirect()->route('category.index')->with('Success', 'Category deleted successfully.');
    }

    public function print()
    {
        $Categories = Category::all();

        $dataArray = array();
        $no = 1;
        foreach ($Categories as $Category) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Category->code;
            $objectJSON["description"] = $Category->descr;
            $objectJSON["status"] = $Category->active ? "Active":"Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $CategoriesJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintCategory" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $CategoriesJSON);
        $JasperFileName = "ReportPrintCategory" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Category Listing Report (" .  date('d/m/Y') .")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return Excel::download(new CategoryExport, 'Category.xlsx');
    }
}
