<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use Auth;

class UserSetupController extends Controller
{
    public function index()
    {
        $data["userSetups"] = User::get();
        $data["page_title"] = "User Setup Item Listing";
        $data["bclvl1"] = "User Setup Item Listing";
        $data["bclvl1_url"] = route('usersetups.index');

        return view('settings.usersetups.index', $data);
    }

    public function create()
    {
        $data["userSetups"] = User::get();
        $data["usergroups"] = Role::get();
        $data["page_title"] = "User Setup Item Listing";
        $data["bclvl1"] = "User Setup Item Listing";
        $data["bclvl1_url"] = route('usersetups.index');
        $data["bclvl2"] = "Add User Setup";
        $data["bclvl2_url"] = route('usersetups.create');

        return view('settings.usersetups.create', $data);
    }

    public function store(Request $request)
    {

        $delete = User::withTrashed()->where('email', $request['email'])->first();

        if($delete){

            $delete->forceDelete();
        }

        $user = User::create([
            'usercode' => $request['usercode'],
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['passwordcoy']),
            'password_h' => md5($request['passwordcoy']),
            'role' => $request['role'],
            'active' => $request['active'],
            'created_by' => Auth::user()->name,
        ]);

        $user->assignRole($request['role']);
        // $user->roles()->sync([$request['role']]);

        return redirect()->route('usersetups.index')->with('Success', 'User Setups created successfully.');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $role = Role::get();
        $roles = $user->roles()->pluck('name');
        // return response()->json($roles);
        $data["userSetup"] = $user;
        $data["usergroups"] = $role;
        $data["page_title"] = "User Setup Item Listing";
        $data["bclvl1"] = "User Setup Item Listing";
        $data["bclvl1_url"] = route('usersetups.index');
        $data["bclvl2"] = "Add User Setup";
        $data["bclvl2_url"] = route('usersetups.create');

        // return response()->json($data);
        return view('settings.usersetups.edit', $data);
    }

    public function update(Request $request, $id)
    {
        if($request['email'] != $request['emailcoy']){
            
            $delete = User::withTrashed()->where('email', $request['email'])->first();

            if($delete){
    
                $delete->forceDelete();
            }

        }
        
        
        $user = User::find($id);
        if (isset($user)) {
            // dd(md5($request['passwordcoy']));
            if (!empty($request['passwordcoy'])) {
                $user->update([
                    'password' => bcrypt($request['passwordcoy']),
                    'password_h' => md5($request['passwordcoy']),
                ]);
            }

            $user->update([
                'usercode' => $request['usercode'],
                'name' => $request['name'],
                'email' => $request['email'],
                'role' => $request['role'],
                'active' => $request['active'],
                'updated_by' => Auth::user()->name,
            ]);
            $user->syncRoles($user->role);
            // $user->assignRole($user->role);
        }


        // $user->assignRole($request['role']);
        // $user->roles()->sync([$request['role']]);

        return redirect()->route('usersetups.index')->with('Success', 'User Setups Updated successfully.');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if (isset($user)) {
            $user->update([
                'deleted_by' => Auth::user()->id,
            ]);

            $user->delete();
        }

        return redirect()->route('usersetups.index')->with('Success', 'User Group deleted successfully.');
    }

    public function getEmail($email)
    {
        $user = User::where('email', $email)->where('deleted_at', NULL)->get();
        return $user;
    }

    public function getUsercode($usercode)
    {
        $user = User::where('usercode', $usercode)->where('deleted_at', NULL)->get();
        return $user;
    }
}
