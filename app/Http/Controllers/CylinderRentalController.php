<?php

namespace App\Http\Controllers;

use App\Model\Debtor;
use App\Model\View\CylinderRental;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;

class CylinderRentalController extends Controller
{
    public function print(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $CylRental = $this->getCylRentalQuery($request, $dates_from, $dates_to);

        if ($CylRental->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        $cylJSON = $this->getCylRentalJSON($CylRental, $dates_from, $dates_to);

        $JsonFileName = "CylinderRentalStatement" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/CylinderRentalStatement/' . $JsonFileName . '.json'), $cylJSON);
        $JasperFileName = "CylinderRentalStatement" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName, $dates_from, $request);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $dates_from, $request)
    {
        $debtor = Debtor::where('accountcode', '=', $request->Debtor_frm)->first();

        $mt_ADDR =  $debtor['address1'];
        $mt_ADDR .= (str_replace(' ', '', $debtor['address2']) == 'null' ? "" : $debtor['address2']);
        $mt_ADDR .= (str_replace(' ', '', $debtor['address3']) == 'null' ? "" : $debtor['address3']);
        $mt_ADDR .= (str_replace(' ', '', $debtor['address4']) == 'null' ? "" : $debtor['address4']);

        $input = base_path() . '/resources/reporting/CylinderRentalStatement/CylinderRentalStatement.jrxml';
        $output = base_path() . '/resources/reporting/CylinderRentalStatement/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/CylinderRentalStatement/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => "Cylinder Rental Statement ",
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYNO' => config('config.company.company_no'),
                'mt_ACC_HOLDER' =>  $debtor['name'],
                'mt_ADDR1' => $mt_ADDR,
                'mt_TEL' => $debtor['tel'],
                'mt_FAX' => $debtor['fax'],
                'mt_DOC_DATE' => date('d/m/Y', strtotime($dates_from)),
                'mt_ACC_CODE' => $debtor['accountcode'],
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/CylinderRentalStatement/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getCylRentalQuery($request, $dates_from, $dates_to)
    {
        return CylinderRental::where(function ($query) use ($request, $dates_from, $dates_to) {

            $query->where('debtor', '=', $request->Debtor_frm);
        })->orderBy('doc_no')
            ->get();
    }

    public function getCylRentalJSON($CylRental, $dates_from, $dates_to)
    {

        $dataArray = [];
        $objectJSON =   [];

        $cylOpening = CylinderRental::selectRaw('SUM(qty) as opening')
            ->whereDate('cdate', '<', $dates_from)
            ->where('debtor',  $CylRental[0]->debtor)
            ->groupBy('debtor')
            ->first();

        $objectJSON['date_ref_doc'] = date('d/m/Y', strtotime($dates_from));
        $objectJSON['no_ref_doc'] = "OPENING";
        $objectJSON['balance_ref'] = $cylOpening == null ? 0:$cylOpening->opening;

        $CylinderRental = CylinderRental::selectRaw('cdate, sum(qty) as quantity, doc_no, doc_type')
            ->where(function ($query) use ($CylRental, $dates_from, $dates_to) {
                if ($dates_from == $dates_to) {
                    $query->where('cdate', '=', $dates_from);
                } else {
                    $query->whereBetween('cdate', [$dates_from, $dates_to]);
                }
                $query->where('debtor',   $CylRental[0]->debtor);
            })
            ->groupBy('doc_type', 'doc_no')
            ->orderBy('cdate')
            ->get();

        $closingBalance = $objectJSON['balance_ref'];
        $dataArray[] = collect($objectJSON);
        foreach ($CylinderRental as $cyl) {
            $objectJSON =   [];

            $objectJSON['date_ref_doc'] = date('d/m/Y', strtotime($cyl->cdate));
            $objectJSON['no_ref_doc'] = $cyl->doc_no;

            if ($cyl->doc_type == "DN") {
                $objectJSON['delivery_ref'] = $cyl->quantity;
                $closingBalance = $closingBalance + $cyl->quantity;
            } elseif ($cyl->doc_type == "RN") {
                $objectJSON['return_ref'] = abs($cyl->quantity);
                $closingBalance = $closingBalance + $cyl->quantity;
            }
            $dataArray[] = collect($objectJSON);
        }
        $objectJSON =   [];
        $objectJSON['date_ref_doc'] = date('d/m/Y', strtotime($dates_to));
        $objectJSON['no_ref_doc'] = "CLOSING";
        $objectJSON['balance_ref'] = $closingBalance;
        $dataArray[] = collect($objectJSON);

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
