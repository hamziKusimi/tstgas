<?php

namespace App\Http\Controllers;

use App\Model\Reason;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class ReasonController extends Controller
{
    public function index()
    {
        $data["reasons"] = Reason::get();
        $data["page_title"] = "Reason Item Listing";
        $data["bclvl1"] = "Reason Item Listing";
        $data["bclvl1_url"] = route('reasons.index');

        return view('stockmaster.reasons.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Reason";
        $data["bclvl1"] = "Reason Item Listing";
        $data["bclvl1_url"] = route('reasons.index');
        $data["bclvl2"] = "Add Reason";
        $data["bclvl2_url"] = route('reasons.create');

        return view('stockmaster.reasons.create', $data);
    }

    public function store(Request $request)
    {
        $reason = Reason::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('reasons.index')->with('Success', 'Reason created successfully.');
    }

    public function edit($id)
    {
        $data["reason"] = Reason::find($id);
        $data["page_title"] = "Edit Reason";
        $data["bclvl1"] = "Reason Item Listing";
        $data["bclvl1_url"] = route('reasons.index');
        $data["bclvl2"] = "Edit Reason";
        $data["bclvl2_url"] = route('reasons.edit', ['id'=>$id]);

        return view('stockmaster.reasons.edit', $data);
    }

    public function update(Request $request, $id){

        $reason = Reason::find($id);
        if (isset($reason)) {
            $reason->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('reasons.index')->with('Success', 'Reason updated successfully.');
    }

    public function destroy(reason $reason)
    {
        $reason->update([
            'deleted_by' => Auth::user()->id
        ]);
        $reason->delete();
        return redirect()->route('reasons.index')->with('Success', 'Reason deleted successfully.');
    }

    public function print()
    {
        $Reasons = Reason::all();

        if ($Reasons->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($Reasons as $Reason) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Reason->code;
            $objectJSON["description"] = $Reason->descr;
            $objectJSON["status"] = $Reason->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $ReasonsJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintReason" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $ReasonsJSON);
        $JasperFileName = "ReportPrintReason" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Reason Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }

}
