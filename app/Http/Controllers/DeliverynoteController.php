<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Model\DeliveryNote;
use App\Model\DeliveryNotedt;
use App\Model\DeliveryNoteGRDT;
use App\Model\DeliveryNotepr;
use App\Model\DeliveryNoteLL;
use App\Model\DeliveryNoteUL;
use App\Model\View\DeliverynoteMaster;
use App\Model\View\DeliverynotegrMaster;
use App\Model\View\DeliverynoteStatus;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Cylinder;
use App\Model\Debtor;
use App\Model\DocumentSetup;
use App\Model\Cylindercategory;
use App\Model\Cylinderproduct;
use App\Model\Gasrack;
use App\Model\Gasracktype;
use App\Model\Driver;
use App\Model\Salesman;
use App\Model\DeliveryNoteRack;
use Auth;
use App\Model\Location;
use PHPJasper\PHPJasper;
use DateTime;
use App\Model\CustomObject\Transaction;
use App\Model\SystemSetup;
use DB;
use App\Model\View\DeliveryNotePrintRackView;
use App\Model\Area;
use DataTables;

class DeliverynoteController extends Controller
{
    public function index()
    {
        $deliverynotes = DeliveryNote::orderBy('dn_no', 'desc')->get();
        $data["deliverynotes"] = $deliverynotes;
        $data["docno_select"] = DeliveryNote::pluck('dn_no', 'dn_no');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        // $data["status"] = DeliverynoteStatus::get();
        $data["page_title"] = "Delivery Note Listing";
        $data["bclvl1"] = "Delivery Note Listing";
        $data["bclvl1_url"] = route('deliverynotes.index');
        return view('dailypro/deliverynotes.index', $data);
    }

    public function api_get()
    {
        $resources = DeliveryNote::noteBy('date')->get();

        return response()->json($resources);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $capacities = Cylinder::whereNotNull('capacity')->pluck('capacity', 'capacity');
        $pressures = Cylinder::whereNotNull('workingpressure')->pluck('workingpressure', 'workingpressure');
        $serials = Cylinder::pluck('serial');
        $barcodes = Cylinder::whereNotNull('barcode')->get(['barcode', 'serial', 'cat_id', 'prod_id', 'capacity', 'workingpressure']);
        $runningNumber = DocumentSetup::findByName('Delivery Note');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $cylindercat = Cylindercategory::pluck('code');
        $cylinderprod = Cylinderproduct::pluck('code');
        // $gsbarcodes = Gasrack::get(['barcode', 'serial', 'type', 'gscylinder']);
        $gsbarcodes = Gasrack::get();
        $gasracktype = Gasracktype::pluck('code');
        $drivers = Driver::pluck('code', 'code');

        $data = [];
        // $data["uom"] = $uom;
        // $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['masterCodes'] = $masterCodes;
        $data['capacities'] = $capacities;
        $data['pressures'] = $pressures;
        $data['taxCodes'] = $taxCodes;
        $data["serials"] = $serials;
        $data["barcodes"] = $barcodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["cylindercat"] = $cylindercat;
        $data["cylinderprod"] = $cylinderprod;
        $data["gsbarcodes"] = $gsbarcodes;
        $data["gasracktype"] = $gasracktype;
        $data["drivers"] = $drivers;
        $data["page_title"] = "Add Delivery Note";
        $data["bclvl1"] = "Delivery Note Received Listing";
        $data["bclvl1_url"] = route('deliverynotes.index');
        $data["bclvl2"] = "Add Delivery Note";
        $data["bclvl2_url"] = route('deliverynotes.create');

        return view('dailypro/deliverynotes.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $deliverynoteid = DeliveryNote::create([
            'dn_no' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'lpono' => $request['lpono'],
            'driver' => $request['driver'],
            // 'charge_type' => $request['charge_type'],
            // 'start_from' => Carbon::createFromFormat('d/m/Y', $request['startfrom']),
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'vehicle_no' => !empty($request['vehicleNo']) ? $request['vehicleNo'] : '',
            'created_by' => Auth::user()->id
        ]);


        if (count($request['sequence_no']) > 1) {
            for ($i = 1; $i < count($request['sequence_no']); $i++) {

                DeliveryNotedt::create([
                    'dn_no' => $deliverynoteid->dn_no,
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'sequence_no' => $request['sequence_no'][$i],
                    'category' => !empty($request['category'][$i]) ? $request['category'][$i] : $request['categorydt'][$i],
                    'product' => !empty($request['product'][$i]) ? $request['product'][$i] : $request['productdt'][$i],
                    'capacity' => !empty($request['capacity'][$i]) ? $request['capacity'][$i] : $request['capacitydt'][$i],
                    'pressure' => !empty($request['pressure'][$i]) ? $request['pressure'][$i] : $request['pressuredt'][$i],
                    'qty' => $request['quantity'][$i],
                    'daily_price' => $request['daily_price'][$i],
                    'monthly_price' => $request['monthly_price'][$i],
                    'gas_price' => $request['gas_price'][$i],
                    'remarks' => $request['remarkdt'][$i],
                    'bill_date' => !empty($request['bill_date'][$i]) ? Carbon::createFromFormat('d/m/Y', $request['bill_date'][$i]) : null,
                    'created_by' => Auth::user()->id
                ]);
            }
        }

        if (count($request['grsequence_no']) > 1) {
            for ($i = 1; $i < count($request['grsequence_no']); $i++) {
                DeliveryNoteGRDT::create([
                    'dn_no' => $deliverynoteid->dn_no,
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'sequence_no' => $request['grsequence_no'][$i],
                    'type' => !empty($request['grtype'][$i]) ? $request['grtype'][$i] : $request['grtypedt'][$i],
                    'qty' => $request['grquantity'][$i],
                    'daily_price' => $request['grdaily_price'][$i],
                    'monthly_price' => $request['grmonthly_price'][$i],
                    'bill_date' => !empty($request['grbill_date'][$i]) ? Carbon::createFromFormat('d/m/Y', $request['grbill_date'][$i]) : null,
                    'created_by' => Auth::user()->id
                ]);
            }
        }

        if (isset($request['sqn_no'])) {
            for ($i = 0; $i < count($request['sqn_no']); $i++) {

                if (isset($request['preparedt'][$i])) {
                    $dlpr = DeliveryNotepr::create([
                        'dn_no' => $deliverynoteid->dn_no,
                        'sequence_no' => $request['dt_sqn_no'][$i],
                        'sqn_no' => $request['sqn_no'][$i],
                        'datetime' => $request['preparedt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['preparedt'][$i]) : null,
                        'driver' => $request['driverdt'][$i],
                        'serial' => $request['serialno'][$i],
                        'type' => $request['type'][$i],
                        'barcode' => $request['barcode'][$i],
                        'created_by' => Auth::user()->id
                    ]);
                }

                if (isset($request['loadingdt'][$i])) {
                    $dlll = DeliveryNoteLL::create([
                        'dn_no' => $deliverynoteid->dn_no,
                        'sequence_no' => $request['dt_sqn_no'][$i],
                        'sqn_no' => $request['sqn_no'][$i],
                        'datetime' => $request['loadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['loadingdt'][$i]) : null,
                        'driver' => $request['driverdt'][$i],
                        'serial' => $request['serialno'][$i],
                        'type' => $request['type'][$i],
                        'barcode' => $request['barcode'][$i],
                        'created_by' => Auth::user()->id
                    ]);
                }
                if (isset($request['uloadingdt'][$i])) {
                    $dlul = DeliveryNoteUL::create([
                        'dn_no' => $deliverynoteid->dn_no,
                        'sequence_no' => $request['dt_sqn_no'][$i],
                        'sqn_no' => $request['sqn_no'][$i],
                        'datetime' => $request['uloadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['uloadingdt'][$i]) : null,
                        'driver' => $request['driverdt'][$i],
                        'serial' => $request['serialno'][$i],
                        'barcode' => $request['barcode'][$i],
                        'type' => $request['type'][$i],
                        'created_by' => Auth::user()->id
                    ]);
                }
            }
        }

        $invoiceDoc = DocumentSetup::findByName('Delivery Note');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        return redirect()->route('deliverynotes.edit', ['id' => $deliverynoteid])->with('Success', 'Delivery Note created sucessfully');
    }

    public function edit($id)
    {
        $deliverynote = DeliveryNote::find($id);
        $contents = DeliveryNotedt::where('dn_no', $deliverynote->dn_no)->get();
        $deliverynotells = DeliveryNoteLL::where('dn_no', $deliverynote->dn_no)->where('sequence_no', $deliverynote->sequence_no)->get();
        $deliverynoteuls = DeliveryNoteUL::where('dn_no', $deliverynote->dn_no)->where('sequence_no', $deliverynote->sequence_no)->get();
        $deliverynoteprs = DeliveryNotepr::where('dn_no', $deliverynote->dn_no)->where('sequence_no', $deliverynote->sequence_no)->get();
        $selectedDebitor = Debtor::findByAcode($deliverynote->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $capacities = Cylinder::whereNotNull('capacity')->pluck('capacity', 'capacity');
        $pressures = Cylinder::whereNotNull('workingpressure')->pluck('workingpressure', 'workingpressure');
        $barcodes = Cylinder::whereNotNull('barcode')->get(['barcode', 'serial', 'cat_id', 'prod_id', 'capacity', 'workingpressure']);
        $taxCodes = TaxCode::all();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $cylindercat = Cylindercategory::pluck('code');
        $cylinderprod = Cylinderproduct::pluck('code');
        $gsbarcodes = Gasrack::get();
        $gasracktype = Gasracktype::pluck('code');
        $drivers = Driver::pluck('code', 'code');
        $grcontents = DeliveryNoteGRDT::where('dn_no', $deliverynote->dn_no)->get();
        // return response()->json($contents);

        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['item'] = $deliverynote;
        $data['deliverynotells'] = $deliverynotells;
        $data['deliverynoteuls'] = $deliverynoteuls;
        $data['deliverynoteprs'] = $deliverynoteprs;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['capacities'] = $capacities;
        $data['pressures'] = $pressures;
        $data['barcodes'] = $barcodes;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["cylindercat"] = $cylindercat;
        $data["cylinderprod"] = $cylinderprod;
        $data["gsbarcodes"] = $gsbarcodes;
        $data["gasracktype"] = $gasracktype;
        $data["drivers"] = $drivers;
        $data["grcontents"] = $grcontents;
        $data["page_title"] = "Edit Delivery Note";
        $data["bclvl1"] = "Delivery Note Received Listing";
        $data["bclvl1_url"] = route('deliverynotes.index');
        $data["bclvl2"] = "Edit Delivery Note";
        $data["bclvl2_url"] = route('deliverynotes.edit', ['id' => $id]);
        // return response()->json($grcontents);

        return view('dailypro/deliverynotes.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $deliverynote = Deliverynote::find($id);
        if (isset($deliverynote)) {
            $deliverynote->update([
                'dn_no' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'lpono' => $request['lpono'],
                'driver' => $request['driver'],
                // 'charge_type' => $request['charge_type'],
                // 'start_from' => Carbon::createFromFormat('d/m/Y', $request['startfrom']),
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'vehicle_no' => !empty($request['vehicleNo']) ? $request['vehicleNo'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['sequence_no']) > 1) {
                for ($i = 1; $i < count($request['sequence_no']); $i++) {
                    $deliverynotedt_id = Deliverynotedt::where('dn_no', $request['doc_no'])
                        ->where('sequence_no', $request['sequence_no'][$i])->first();
                    if ($deliverynotedt_id == null) {
                        Deliverynotedt::create([
                            'dn_no' => $deliverynote->dn_no,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'category' => !empty($request['category'][$i]) ? $request['category'][$i] : $request['categorydt'][$i],
                            'product' => !empty($request['product'][$i]) ? $request['product'][$i] : $request['productdt'][$i],
                            'capacity' => !empty($request['capacity'][$i]) ? $request['capacity'][$i] : $request['capacitydt'][$i],
                            'pressure' => !empty($request['pressure'][$i]) ? $request['pressure'][$i] : $request['pressuredt'][$i],
                            'qty' => $request['quantity'][$i],
                            'daily_price' => $request['daily_price'][$i],
                            'monthly_price' => $request['monthly_price'][$i],
                            'gas_price' => $request['gas_price'][$i],
                            'remarks' => $request['remarkdt'][$i],
                            'bill_date' => !empty($request['bill_date'][$i]) ? Carbon::createFromFormat('d/m/Y', $request['bill_date'][$i]) : null,
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }

            if (isset($request['content_cyl_id'])) {
                for ($i = 0; $i < count($request['content_cyl_id']); $i++) {
                    $Deliverynotedt = Deliverynotedt::where('id',  $request['content_cyl_id'][$i]);
                    $Deliverynotedt->update([
                        'remarks' => $request['remark_exist'][$i]
                    ]);
                }
            }

            if (count($request['grsequence_no']) > 1) {
                for ($i = 1; $i < count($request['grsequence_no']); $i++) {

                    $deliverynotegrdt_id = DeliverynoteGRDT::where('dn_no', $request['doc_no'])
                        ->where('sequence_no', $request['grsequence_no'][$i])->first();

                    if ($deliverynotegrdt_id == null) {
                        DeliverynoteGRDT::create([
                            'dn_no' => $deliverynote->dn_no,
                            'sequence_no' => $request['grsequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'type' => !empty($request['grtype'][$i]) ? $request['grtype'][$i] : $request['grtypedt'][$i],
                            'qty' => $request['grquantity'][$i],
                            'daily_price' => $request['grdaily_price'][$i],
                            'monthly_price' => $request['grmonthly_price'][$i],
                            'bill_date' => !empty($request['grbill_date'][$i]) ? Carbon::createFromFormat('d/m/Y', $request['grbill_date'][$i]) : null,
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }

            $data = DeliveryNoteLL::where('dn_no', $request['doc_no'])
                ->orderBy('sqn_no', 'DESC')
                ->first('sqn_no');
            if ($data === null) {
                $data_sqn = 0;
            } else {
                $data_sqn = $data->sqn_no;
            }

            if (isset($request['sqn_no'])) {
                for ($i =  0; $i < count($request['sqn_no']); $i++) {

                    if (isset($request['preparedt'][$i])) {
                        $dlpr = DeliveryNotepr::create([
                            'dn_no' => $deliverynote->dn_no,
                            'sequence_no' => $request['dt_sqn_no'][$i],
                            'sqn_no' => $request['sqn_no'][$i] + $data_sqn,
                            'datetime' => $request['preparedt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['preparedt'][$i]) : null,
                            'driver' => $request['driverdt'][$i],
                            'serial' => $request['serialno'][$i],
                            'barcode' => $request['barcode'][$i],
                            'type' => $request['type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }

                    if (isset($request['loadingdt'][$i])) {
                        $dlll = DeliveryNoteLL::create([
                            'dn_no' => $deliverynote->dn_no,
                            'sequence_no' => $request['dt_sqn_no'][$i],
                            'sqn_no' => $request['sqn_no'][$i] + $data_sqn,
                            'datetime' => $request['loadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['loadingdt'][$i]) : null,
                            'driver' => $request['driverdt'][$i],
                            'serial' => $request['serialno'][$i],
                            'barcode' => $request['barcode'][$i],
                            'type' => $request['type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }

                    if (isset($request['uloadingdt'][$i])) {
                        $dlul = DeliveryNoteUL::create([
                            'dn_no' => $deliverynote->dn_no,
                            'sequence_no' => $request['dt_sqn_no'][$i],
                            'sqn_no' => $request['sqn_no'][$i] + $data_sqn,
                            'datetime' => $request['uloadingdt'][$i] ? Carbon::createFromFormat('d/m/Y', $request['uloadingdt'][$i]) : null,
                            'driver' => $request['driverdt'][$i],
                            'serial' => $request['serialno'][$i],
                            'barcode' => $request['barcode'][$i],
                            'type' => $request['type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }

            // dd($request['edit_ul_date']);
            if (isset($request['edit_ll_barcode']) && isset($request['edit_ll_date'])) {
                for ($i =  0; $i < count($request['edit_ll_barcode']); $i++) {
                    if ($request['edit_ll_date'][$i] != '' || $request['edit_ll_date'][$i] != null) {
                        $dlul = DeliveryNoteLL::create([
                            'dn_no' => $deliverynote->dn_no,
                            'sequence_no' => $request['edit_ll_sequence_no'][$i],
                            'sqn_no' => $request['edit_ll_sqn_no'][$i],
                            'datetime' => $request['edit_ll_date'][$i] ? Carbon::createFromFormat('d/m/Y', $request['edit_ll_date'][$i]) : null,
                            'driver' => $request['edit_ll_driver'][$i],
                            'serial' => $request['edit_ll_serial'][$i],
                            'barcode' => $request['edit_ll_barcode'][$i],
                            'type' => $request['edit_ll_type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }

            if (isset($request['edit_ul_barcode']) && isset($request['edit_ul_date'])) {
                for ($i =  0; $i < count($request['edit_ul_barcode']); $i++) {
                    if ($request['edit_ul_date'][$i] != '' || $request['edit_ul_date'][$i] != null) {
                        $dlul = DeliveryNoteUL::create([
                            'dn_no' => $deliverynote->dn_no,
                            'sequence_no' => $request['edit_ul_sequence_no'][$i],
                            'sqn_no' => $request['edit_ul_sqn_no'][$i],
                            'datetime' => $request['edit_ul_date'][$i] ? Carbon::createFromFormat('d/m/Y', $request['edit_ul_date'][$i]) : null,
                            'driver' => $request['edit_ul_driver'][$i],
                            'serial' => $request['edit_ul_serial'][$i],
                            'barcode' => $request['edit_ul_barcode'][$i],
                            'type' => $request['edit_ul_type'][$i],
                            'created_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }
        return redirect()->route('deliverynotes.edit', ['id' => $id])->with('Success', 'Delivery Note updated sucessfully');
    }

    public function destroy($id)
    {
        $delivery = DeliveryNote::find($id);
        $delivery->update([
            'deleted_by' => Auth::user()->id
        ]);
        $delivery->delete();

        $deliverydt = DeliveryNotedt::where('dn_no', $delivery->dn_no);
        $deliverydt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $deliverydt->delete();

        $deliverygrdt = DeliveryNoteGRDT::where('dn_no', $delivery->dn_no);
        $deliverygrdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $deliverygrdt->delete();

        $deliverypr = DeliveryNotepr::where('dn_no', $delivery->dn_no);
        $deliverypr->update([
            'deleted_by' => Auth::user()->id
        ]);
        $deliverypr->delete();

        $deliveryll = DeliveryNoteLL::where('dn_no', $delivery->dn_no);
        $deliveryll->update([
            'deleted_by' => Auth::user()->id
        ]);
        $deliveryll->delete();

        $deliveryul = DeliveryNoteUL::where('dn_no', $delivery->dn_no);
        $deliveryul->update([
            'deleted_by' => Auth::user()->id
        ]);
        $deliveryul->delete();

        return redirect()->route('deliverynotes.index')->with('Success', 'Delivery Note deleted successfully');
    }

    public function destroyData($id)
    {
        $data = Deliverynotedt::find($id);
        $data_select = Deliverynotedt::select('dn_no', 'sequence_no')->where('id', $id)->first();
        $docno = $data_select->dn_no;
        $seq_no = $data_select->sequence_no;

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();

            $data_ul = DeliveryNoteUL::where('dn_no', $docno)
                ->where('sequence_no', $seq_no);
            $data_ll = DeliveryNoteLL::where('dn_no', $docno)
                ->where('sequence_no', $seq_no);

            if (isset($data_ul)) {
                $data_ul->update([
                    'deleted_by' => Auth::user()->id
                ]);

                $data_ul->delete();
            }

            if (isset($data_ll)) {
                $data_ll->update([
                    'deleted_by' => Auth::user()->id
                ]);

                $data_ll->delete();
            }

            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyData2($id)
    {
        $data = DeliveryNoteGRDT::find($id);
        $data_select = DeliveryNoteGRDT::select('dn_no', 'sequence_no')->where('id', $id)->first();
        $docno = $data_select->dn_no;
        $seq_no = $data_select->sequence_no;

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();

            $data_ul = DeliveryNoteUL::where('dn_no', $docno)
                ->where('sequence_no', $seq_no);
            $data_ll = DeliveryNoteLL::where('dn_no', $docno)
                ->where('sequence_no', $seq_no);

            if (isset($data_ul)) {
                $data_ul->update([
                    'deleted_by' => Auth::user()->id
                ]);

                $data_ul->delete();
            }

            if (isset($data_ll)) {
                $data_ll->update([
                    'deleted_by' => Auth::user()->id
                ]);

                $data_ll->delete();
            }

            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyDatalist(Request $request)
    {
        if ($request['only-UL']) {
            $data = DeliveryNoteUL::where('dn_no', $request['dnno'])
                ->where('sequence_no', $request['sqnex'])
                ->where('sqn_no', $request['sqnlist'])
                ->where('type', "$request->type")
                ->first();

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
        } elseif ($request['only-LL']) {
            $data2 = DeliveryNoteLL::where('dn_no', $request['dnno'])
                ->where('sequence_no', $request['sqnex'])
                ->where('sqn_no', $request['sqnlist'])
                ->where('type', "$request->type")
                ->first();

            $data2->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data2->delete();
        } else {
            $data3 = DeliveryNotepr::where('dn_no', $request['dnno'])
                ->where('sequence_no', $request['sqnex'])
                ->where('sqn_no', $request['sqnlist'])
                ->where('type', $request['type'])
                ->first();
            $data3->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data3->delete();
        }
        return $request;
    }


    public function destroyDeliverynotedt($id)
    {
        $deliverynote = Deliverynotedt::find($id);
        $deliverynote->update([
            'deleted_by' => Auth::user()->id
        ]);
        $deliverynote->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper(Request $request)
    {
        $Resources = DeliveryNote::where('id', $request->id)->get();
        $ResourcesJsonList = $request->type == "cyl" ? $this->getDeliveryNoteListingJSON($Resources)
                                : $this->getDeliveryNoteGRListingJSON($Resources);


        $formattedResource = DeliveryNote::select(
            'dn_no as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'lpono as LPO_NO',
            'charge_type as CH_TYPE',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'driver as DRIVER',
            'start_from as START_FROM',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $request->id)->first();
        $CTERM = Debtor::Select('cterm')
            ->where('accountcode', '=',  $formattedResource->ACC_CODE)->first();
        $source = 'DeliveryNote';
        if($request->type == "cyl"){
            $jrxml = 'general-bill-Notes.jrxml';
            $reportTitle = 'DELIVERY ORDER';
        }else{
            $jrxml = 'general-bill-Notes-rack.jrxml';
            $reportTitle = 'GAS RACK - DELIVERY ORDER';
        }

        $dataArr = json_decode($ResourcesJsonList);

        if(empty($dataArr->data)){
            return "<script>alert('No Item Found');window.close();</script>";
        }

        Transaction::generateBillNote(
            $formattedResource,
            $CTERM->cterm,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList),
            $request->type
        );
    }

    public function print(Request $request)
    {
        $DeliveryNote = $this->getDeliveryNoteQuery($request);

        if ($DeliveryNote->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $DeliveryNoteJSON = $this->getDeliveryNoteListingJSON($DeliveryNote);

        // check if JSON empty \\
        $DNJSON = json_decode($DeliveryNoteJSON);
        if (empty($DNJSON->data)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        // check if JSON empty \\

        $JsonFileName = "DeliveryNote" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/DailyProcess/DeliveryNotes/' . $JsonFileName . '.json'), $DeliveryNoteJSON);
        $JasperFileName = "DeliveryNote" . date("Ymdhisa") . Auth::user()->id;

        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/DeliveryNotes/DeliveryNoteListing.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/DeliveryNotes/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/DeliveryNotes/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/DeliveryNotes/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getDeliveryNoteQuery(Request $request)
    {
        return DeliveryNote::select('dn_no', 'date', 'account_code', 'name')
            ->where(function ($query) use ($request) {

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('dn_no', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $query->where('lpono', "=", $request->LPO_frm_1);
                    } else {
                        $query->whereBetween('lpono', [$request->LPO_frm_1, $request->LPO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('dn_no')
            ->get();
    }

    public function getDeliveryNoteListingJSON($DeliveryNote)
    {

        $category = "";
        $dataArray = array();
        foreach ($DeliveryNote as $DN) {

            // $DNTrans = DeliveryNotedt::select('dn_no', 'sequence_no', 'qty', 'capacity', 'remarks')
            //     ->where("dn_no", "=", $DN->dn_no)
            //     ->get();

            // $DNTrans = DB::select(DB::raw("
            // SELECT delivery_noteprs.dn_no, gr_serial, descr, delivery_notedts.remarks, cylinders.`descr`
            // FROM delivery_noteprs
            // INNER JOIN delivery_notedts ON delivery_noteprs.dn_no = delivery_notedts.dn_no
            //     AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no
            // LEFT JOIN view_delivery_note_manifold ON delivery_noteprs.`serial`= view_delivery_note_manifold.cy_serial
            //     AND view_delivery_note_manifold.dn_no = delivery_noteprs.dn_no
            // INNER JOIN cylinders ON delivery_noteprs.`serial` = cylinders.`serial`
            // WHERE delivery_noteprs.`deleted_at` IS NULL AND `type` = 'cy' AND delivery_notedts.`deleted_at` IS NULL
            //     AND delivery_noteprs.dn_no = '$DN->dn_no'
            // GROUP BY gr_serial, cylinders.`descr`
            // ORDER BY gr_serial DESC;
            // "));

            $DNTrans = DB::select(DB::raw("
            SELECT delivery_noteprs.dn_no,
            delivery_notes.account_code as account_code, delivery_notes.name as name, delivery_notes.date as date,
            gr_serial, delivery_notedts.remarks, delivery_notedts.category, cylinders.`descr` as cydescr, view_delivery_note_manifold.descr as descr, view_delivery_note_manifold.cy_serial cy_serial
            FROM delivery_noteprs
            INNER JOIN delivery_notedts ON delivery_noteprs.dn_no = delivery_notedts.dn_no
                AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no
            LEFT JOIN view_delivery_note_manifold ON delivery_noteprs.`serial`= view_delivery_note_manifold.cy_serial
                AND view_delivery_note_manifold.dn_no = delivery_noteprs.dn_no
            INNER JOIN cylinders ON delivery_noteprs.`serial` = cylinders.`serial`
            INNER JOIN delivery_notes ON (delivery_notes.dn_no = delivery_noteprs.dn_no)
            WHERE delivery_noteprs.`deleted_at` IS NULL AND `type` = 'cy' AND delivery_notedts.`deleted_at` IS NULL
                AND delivery_noteprs.dn_no = '$DN->dn_no'
            GROUP BY gr_serial, view_delivery_note_manifold.`descr`, delivery_notedts.category
            ORDER BY gr_serial DESC;
            "));

            $lastElement = count($DNTrans);
            $s_n = 1;
            $t_qty = 0;
            $t_price = 0;
            foreach ($DNTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($DN->date));
                $new_arr = array();
                if($trans->gr_serial == null){
                    $Cylinders = DB::select(DB::raw("
                    SELECT delivery_noteprs.`serial` AS cy_serials
                    FROM delivery_noteprs
                    LEFT JOIN view_delivery_note_manifold ON delivery_noteprs.`serial`= view_delivery_note_manifold.cy_serial
                        AND view_delivery_note_manifold.dn_no = delivery_noteprs.dn_no
                    INNER JOIN delivery_notedts on (delivery_noteprs.dn_no = delivery_notedts.dn_no
                    AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no)
                    WHERE delivery_noteprs.`deleted_at` IS NULL AND `type` = 'cy'
                        AND delivery_noteprs.dn_no = '$trans->dn_no'
                        AND delivery_notedts.category = '$trans->category'
                        AND view_delivery_note_manifold.gr_serial IS NULL
                        GROUP BY delivery_noteprs.created_at, delivery_noteprs.serial;
                    "));

                    foreach($Cylinders as $cyl)
                    {
                        $new_arr[] = $cyl->cy_serials;
                    }
                }else{
                    $Cylinders = DB::select(DB::raw("
                    SELECT delivery_noteprs.`serial` AS cy_serials
                    FROM delivery_noteprs
                    LEFT JOIN view_delivery_note_manifold ON delivery_noteprs.`serial`= view_delivery_note_manifold.cy_serial
                        AND view_delivery_note_manifold.dn_no = delivery_noteprs.dn_no
                    WHERE delivery_noteprs.`deleted_at` IS NULL AND `type` = 'cy'
                        AND delivery_noteprs.dn_no = '$trans->dn_no'
                        AND view_delivery_note_manifold.gr_serial = '$trans->gr_serial';
                    "));

                    foreach($Cylinders as $cyl)
                    {
                        $new_arr[] = $cyl->cy_serials;
                    }
                }


                // $DNLoading = DeliveryNotepr::join('cylinders', 'delivery_noteprs.barcode', '=', 'cylinders.barcode')
                //     ->select('descr')
                //     ->where(function ($query) use ($trans) {
                //         $query->where('dn_no', '=', $trans->dn_no);
                //         $query->where('sequence_no', '=', $trans->sequence_no);
                //     })->first();

                // $category = $trans->category;
                // dd($category);
                if ($trans->cy_serial == null) {
                    // if($trans->category == $category){

                        $descr = $trans->cydescr;
                    // }else{

                        // $descr = "-";
                    // }
                } else {
                    // $descr =$trans->descr;

                    if($trans->descr == null){

                        $descr = $trans->cydescr." IN MANIFOLD PALLET X ".count($Cylinders)." CYLS";
                    }else{
                        $descr = $trans->descr;
                    }
                }


                $objectJSON['s_n'] = $s_n;
                $objectJSON['dn_no'] = $trans->dn_no;
                $objectJSON['uom'] = "";
                $objectJSON['discount'] = "0.00";
                $objectJSON['amount'] = "0.00";
                $objectJSON['subject'] = $trans->dn_no.", ".$trans->date.", ".$trans->account_code.", ".$trans->name."";
                $objectJSON['details'] = count($Cylinders) > 0 ? "Cylinders: " . implode(', ', $new_arr) : null;
                $objectJSON['quantity'] = count($Cylinders);
                $objectJSON['description'] = $descr;
                if($trans->cy_serial == null){
                    $objectJSON['remark'] = $trans->remarks;
                }else{
                    $objectJSON['remark'] = $trans->gr_serial;
                }

                $objectJSON['price'] = "0.00";
                $t_qty =  $t_qty + count($Cylinders);
                $t_price =  $t_price +  $objectJSON['price'];

                if ($s_n == $lastElement) {
                    $objectJSON['t_qty'] = $t_qty;
                    $objectJSON['t_price'] = $t_price;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }

        // dd($dataArray);
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getDeliveryNoteGRListingJSON($DeliveryNote)
    {
        $dataArray = array();
        foreach ($DeliveryNote as $DN) {

            $DNTrans = DeliveryNotePrintRackView::leftjoin('delivery_note_racks', function ($join) {
                $join->on('view_delivery_note_print_rack.serial', '=', 'delivery_note_racks.rack_no');
                $join->on('view_delivery_note_print_rack.dn_no', '=', 'delivery_note_racks.dn_no');
            })
            ->selectRaw('serial, type,
                        delivery_note_racks.sling_no,
                        delivery_note_racks.gwl,
                        DATE_FORMAT(next_inspect_date, "%d/%m/%Y") as inspDate,
                        DATE_FORMAT(re_test_date, "%d/%m/%Y") as testDate')
            ->where("view_delivery_note_print_rack.dn_no", "=", $DN->dn_no)
            ->groupBy('view_delivery_note_print_rack.serial')
            ->get();

            $lastElement = count($DNTrans);
            $s_n = 1;
            $t_qty = 0;
            $t_price = 0;

            foreach ($DNTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($DN->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['serial_no'] = $trans->serial;
                $objectJSON['quantity'] = 0;
                $objectJSON['rack_type'] = $trans->type;
                $objectJSON['sling_no'] = $trans->sling_no;
                $objectJSON['gwl'] = $trans->gwl;
                $objectJSON['inspDate'] = $trans->inspDate;
                $objectJSON['testDate'] = $trans->testDate;
                $objectJSON['price'] = 0;
                $t_qty =  $t_qty + 0;
                $t_price =  $t_price +  $objectJSON['price'];

                if ($s_n == $lastElement) {
                    $objectJSON['t_qty'] = $t_qty;
                    $objectJSON['t_price'] = $t_price;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getCyDt($serial)
    {

            $result = Cylinder::where('serial', $serial)->first();
        return $result;

    }

    public function getGrDt($serial)
    {
        // if($request['isCylinder'] == false){

            $result = Gasrack::where('serial', $serial)->first();
        // }
        return $result;

    }

    public function saveRackDetail(Request $request)
    {
        $date1 = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $request['inspDate']))->format('Y-m-d');
        $date2 = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $request['testDate']))->format('Y-m-d');

        $DoNo = isset($request['dnno']) ? $request['dnno'] : '' ;
        $rackNo = isset($request['rackno']) ? $request['rackno'] : '' ;
        $slingNo = isset($request['slingno']) ? $request['slingno'] : '' ;
        $GWL = isset($request['gwl']) ? $request['gwl'] : '' ;
        $inspDate = isset($request['inspDate']) ? $date1 : '' ;
        $testDate = isset($request['testDate']) ? $date2 : '' ;

        $RackDetail = DeliveryNoteRack::where(function ($query) use ($rackNo,  $DoNo) {
                        $query->Where('dn_no', '=', $DoNo);
                        $query->Where('rack_no', '=', $rackNo );
                        })->get();


        if ($RackDetail->isEmpty()) {
            DeliveryNoteRack::create([
                'dn_no' => $DoNo,
                'rack_no' => $rackNo,
                'sling_no' => $slingNo,
                'gwl' => $GWL,
                'next_inspect_date' => $inspDate,
                're_test_date' => $testDate,
            ]);

        }else{
            $rack = DeliveryNoteRack::where(function ($query) use ($rackNo,  $DoNo) {
                $query->Where('dn_no', '=', $DoNo);
                $query->Where('rack_no', '=', $rackNo );
                });
            $rack->update([
                'sling_no' => $slingNo,
                'gwl' => $GWL,
                'next_inspect_date' => $inspDate,
                're_test_date' => $testDate
            ]);
        }


        return "Saved";
    }

    public function getRackDetail(Request $request)
    {
        $DoNo = isset($request['dnno']) ? $request['dnno'] : '' ;
        $rackNo = isset($request['rackno']) ? $request['rackno'] : '' ;

        $RackDetail = DeliveryNoteRack::selectRaw('sling_no, gwl, DATE_FORMAT(next_inspect_date, "%d/%m/%Y") as inspDate, DATE_FORMAT(re_test_date, "%d/%m/%Y") as testDate')
                        ->where(function ($query) use ($rackNo,  $DoNo) {
                        $query->Where('dn_no', '=', $DoNo);
                        $query->Where('rack_no', '=', $rackNo );
                        })->first();


        if ($RackDetail) {
            return ($RackDetail->toArray());
        }else{
            return "empty";
        }
    }

    public function docnoList()
    {
        $query = DeliveryNote::query()
            ->selectRaw('dn_no, date, name');

        return DataTables::of($query)->make(true);
    }
}
