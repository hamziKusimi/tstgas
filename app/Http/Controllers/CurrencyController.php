<?php

namespace App\Http\Controllers;

use App\Model\Currency;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class CurrencyController extends Controller
{
    public function index()
    {
        $data["currencies"] = Currency::get();
        $data["page_title"] = "Currency Item Listing";
        $data["bclvl1"] = "Currency Item Listing";
        $data["bclvl1_url"] = route('currencies.index');

        return view('stockmaster.currencies.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Currency";
        $data["bclvl1"] = "Currency Item Listing";
        $data["bclvl1_url"] = route('currencies.index');
        $data["bclvl2"] = "Add Currency";
        $data["bclvl2_url"] = route('currencies.create');

       return view('stockmaster.currencies.create', $data);
    }

    public function store(Request $request)
    {
        $currency = Currency::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('currencies.index')->with('Success', 'Currency created successfully.');
    }

    public function edit($id)
    {
        $data["currency"] = Currency::find($id);
        $data["page_title"] = "Edit Currency";
        $data["bclvl1"] = "Currency Item Listing";
        $data["bclvl1_url"] = route('currencies.index');
        $data["bclvl2"] = "Edit Currency";
        $data["bclvl2_url"] = route('currencies.edit', ['id'=>$id]);

       return view('stockmaster.currencies.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $currency = Currency::find($id);
        if (isset($currency)) {
            $currency->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('currencies.index')->with('Success', 'Currency updated successfully.');

    }

    public function destroy(currency $currency)
    {
        $currency->update([
            'deleted_by' => Auth::user()->id
        ]);
        $currency->delete();
        return redirect()->route('currencies.index')->with('Success', 'Currency deleted successfully.');
    }

    public function print()
    {
        $Currencies = Currency::all();

        if ($Currencies->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($Currencies as $Currency) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Currency->code;
            $objectJSON["description"] = $Currency->descr;
            $objectJSON["status"] = $Currency->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $CurrenciesJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintCurrency" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $CurrenciesJSON);
        $JasperFileName = "ReportPrintCurrency" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Currency Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }

}
