<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Slingtype;
use Auth;

class SlingtypeController extends Controller
{
    public function index(){

        $data["slingtypes"] = Slingtype::get();
        $data["page_title"] = "Sling Type Item Listings";
        $data["bclvl1"] = "Sling Type Item Listings";
        $data["bclvl1_url"] = route('slingtypes.index');

        return view('cylindermaster.slingtypes.index', $data);
    }

    public function create(){
        
        $data["page_title"] = "Add Sling Type";
        $data["bclvl1"] = "Sling Type Item Listings";
        $data["bclvl1_url"] = route('slingtypes.index');
        $data["bclvl2"] = "Add Sling Type";
        $data["bclvl2_url"] = route('slingtypes.create');

        return view('cylindermaster.slingtypes.create', $data);
    }

    public function store(Request $request){

        $slingtype = Slingtype::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('slingtypes.index')->with('Success', 'Sling Type created successfully.');

    }

    public function edit($id){

        $data["slingtype"] = Slingtype::find($id);
        $data["page_title"] = "Edit Sling Type";
        $data["bclvl1"] = "Sling Type Item Listings";
        $data["bclvl1_url"] = route('slingtypes.index');
        $data["bclvl2"] = "Edit Sling Type";
        $data["bclvl2_url"] = route('slingtypes.edit', ['id'=>$id]);

        return view('cylindermaster.slingtypes.edit', $data);
    }

    public function update(Request $request, $id){
        
        $slingtype = Slingtype::find($id);
        if (isset($slingtype)) {
            $slingtype->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('slingtypes.index')->with('Success', 'Sling Type updated successfully.');
    }

    public function destroy(slingtype $slingtype)
    {
        $slingtype->update([
            'deleted_by' => Auth::user()->id
        ]);
        $slingtype->delete();
        return redirect()->route('slingtypes.index')->with('Success', 'Sling Type deleted successfully.');
    }
}
