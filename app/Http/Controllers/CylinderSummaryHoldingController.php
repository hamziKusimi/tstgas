<?php

namespace App\Http\Controllers;

use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Cylindervalvetype;
use App\Model\Cylindercontainertypes;
use App\Model\Cylinderhandwheeltype;
use App\Model\Manufacturer;
use App\Model\Ownership;
use App\Model\CylinderActivity;
use App\Model\View\CylinderMovement;
use App\Model\Debtor;
use App\Model\View\CylinderHolding;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;

class CylinderSummaryHoldingController extends Controller
{
    public function print(Request $request)
    {
        $CylHolding = $this->getCylHoldingQuery($request);

        if ($CylHolding->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $cylJSON = $this->getCylRentalJSON($CylHolding, $request);

        $JsonFileName = "CylinderSummaryHolding" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/CylinderSummaryHolding/' . $JsonFileName . '.json'), $cylJSON);
        $JasperFileName = "CylinderSummaryHolding" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/CylinderSummaryHolding/CylinderSummaryHolding.jrxml';
        $output = base_path() . '/resources/reporting/CylinderSummaryHolding/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/CylinderSummaryHolding/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/CylinderSummaryHolding/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getCylHoldingQuery($request)
    {
        return CylinderHolding::where(function ($query) use ($request) {
            // $query->where('doc_type', "=", "DN");

            if ($request->date_radio == "date_picker") {
                $dates_to = DateTime::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');
                $query->whereDate('cdate', '<=', $dates_to);
            } else {
                $dates = explode("-", $request->dates);
                $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');
                if ($dates_from == $dates_to) {
                    $query->whereRaw("(cdate ='$dates_from' or RNdate = '$dates_from')");
                } else {
                    $query->whereRaw("((cdate >='$dates_from' AND cdate <='$dates_to') or
                                    (RNdate >='$dates_from' AND RNdate <='$dates_to'))");
                }
            }

            if ($request->Product_Chkbx == "on") {
                $query->whereBetween('product', [$request->Product_frm, $request->Product_to])
                    ->get();
            }

            if ($request->Debtor_Chkbx == "on") {
                $query->whereBetween('debtor', [$request->Debtor_frm, $request->Debtor_to])
                    ->get();
            }

            if ($request->DeliveryNote_Chkbx == "on") {
                $query->whereBetween('doc_no', [$request->DeliveryNote_frm, $request->DeliveryNote_to])
                    ->get();
            }

            if ($request->LPO_Chkbx == "on") {
                if ($request->LPO_to == null) {
                    $query->where('lpono', "=", $request->LPO_frm);
                } else {
                    $query->whereBetween('lpono', [$request->LPO_frm, $request->LPO_to]);
                }
            }

            if ($request->Area_Chkbx == "on") {
                if ($request->Area_to == null) {
                    $query->where('area', "=", $request->Area_frm);
                } else {
                    $query->whereBetween('area', [$request->Area_frm, $request->Area_to]);
                }
            }

            if ($request->include_radio == "wo_invalid") {
                $query->whereNull('remark');
            }
        })->get();

    }

    public function getCylRentalJSON($CylHolding, $request)
    {
        $dataArray = [];

        foreach ($CylHolding as $cylHold) {
            $objectJSON =  [];

            $objectJSON['s_no'] = $cylHold->s_no;
            $objectJSON['DO_No'] = $cylHold->doc_no;
            $objectJSON['DO_Date'] = date('d/m/Y', strtotime($cylHold->cdate));
            $objectJSON['PO'] = $cylHold->lpono;
            $objectJSON['product'] = $cylHold->type;
            $objectJSON['location'] = $cylHold->area;

            if ($request->date_radio == "date_picker") {
                $dates_to = DateTime::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');
                $objectJSON['date'] = $request->date;
            } else {
                $dates = explode("-", $request->dates);
                $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');
                $objectJSON['date_from'] = date('d/m/Y', strtotime($dates_from));
                $objectJSON['date_to'] = date('d/m/Y', strtotime($dates_to));
            }

            $objectJSON['name'] = $cylHold->name ." " . "(". $cylHold->debtor.")";
            $objectJSON['RN_No'] = $cylHold->RNdoc == null ? " " :$cylHold->RNdoc;
            $objectJSON['RN_Date'] = $cylHold->RNdate == null ? " " : date('d/m/Y', strtotime($cylHold->RNdate));
            $objectJSON['Remark'] = $cylHold->remark == null ? " " :$cylHold->remark ;

            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
