<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use App\Model\Refill;
use App\Model\DocumentSetup;
use Carbon\Carbon;
use App\Model\CustomObject\Transaction;

class RefillController extends Controller
{
    public function index()
    {
        $data["refills"] = Refill::orderBy('wo_no', 'desc')->get();
        $data["wo_select"] = Refill::pluck('wo_no', 'wo_no');
        $data["page_title"] = "Refill Item Listings";
        $data["bclvl1"] = "Refill Item Listings";
        $data["bclvl1_url"] = route('refills.index');
        return view('dailypro.refills.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Refill";
        $runningNumber = DocumentSetup::findByName('Refill');
        $data['runningNumber'] = $runningNumber;
        $data["bclvl1"] = "Refill Item Listings";
        $data["bclvl1_url"] = route('refills.index');
        $data["bclvl2"] = "Add Refill";
        $data["bclvl2_url"] = route('refills.create');

        return view('dailypro.refills.create', $data);
    }

    public function store(Request $request)
    {
        $refill = Refill::create([
            'wo_no' => $request['wo_no'],
            'tech' => $request['tech'],
            'datetime' => $request['datetime'] ? date('Y-m-d', strtotime($request['datetime'])) : null,
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'charge_in' => $request['charge_in'],
            'charge_out' => $request['charge_out'],
            'created_by' => Auth::user()->id
        ]);

        $RefillDoc = DocumentSetup::findByName('Refill');
        $RefillDoc->update(['D_LAST_NO' => $RefillDoc->D_LAST_NO + 1]);


        return redirect()->route('refills.index')->with('Success', 'Refill created successfully.');
    }

    public function edit($id)
    {
        $data["refill"] = Refill::find($id);
        $data["page_title"] = "Edit Refill";
        $data["bclvl1"] = "Refill Item Listings";
        $data["bclvl1_url"] = route('refills.index');
        $data["bclvl2"] = "Edit Refill";
        $data["bclvl2_url"] = route('refills.edit', ['id' => $id]);

        return view('dailypro.refills.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $refill = Refill::find($id);
        $refill->update([
            'wo_no' => $request['wo_no'],
            'tech' => $request['tech'],
            'datetime' => $request['datetime'] ? date('Y-m-d', strtotime($request['datetime'])) : null,
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'charge_in' => $request['charge_in'],
            'charge_out' => $request['charge_out'],
            'updated_by' => Auth::user()->id
        ]);

        return redirect()->route('refills.index')->with('Success', 'Refill updated successfully.');
    }

    public function destroy(refill $refill)
    {
        $refill->update([
            'deleted_by' => Auth::user()->id
        ]);
        $refill->delete();
        return redirect()->route('refills.index')->with('Success', 'Refill deleted successfully.');
    }

    public function jasper($id)
    {
        $formattedResource = Refill::select(
            'wo_no as DOC_NO',
            'datetime as DOC_DATE',
            'tech',
            'barcode',
            'serial',
            'charge_in',
            'charge_out'
        )->where('id', $id)->first();
        $source = 'Refill';
        $jrxml = 'general-bill-Refill.jrxml';
        $reportTitle = 'Refill Report';
        Transaction::generateReportRefill(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle
        );
    }

    public function print(Request $request)
    {
        $Refill = $this->getRefillQuery($request);
        if ($Refill->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $RefillJSON = $this->getRefillJSON($Refill);

        $JsonFileName = "Refills" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/DailyProcess/Refill/' . $JsonFileName . '.json'), $RefillJSON);
        $JasperFileName = "Refills" . date("Ymdhisa") . Auth::user()->id;

        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/Refill/RefillReport.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/Refill/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/Refill/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/Refill/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getRefillQuery(Request $request)
    {
        return Refill::select('wo_no', 'datetime', 'tech', 'barcode', 'serial', 'charge_in', 'charge_out')
            ->where(function ($query) use ($request) {

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('datetime', '=', $dates_from);
                    } else {
                        $query->whereBetween('datetime', [$dates_from, $dates_to]);
                    }
                }

                if ($request->wo_Chkbx == "on") {
                    $query->whereBetween('wo_no', [$request->wo_frm, $request->wo_to]);
                }
            })
            ->orderBy('wo_no')
            ->get();
    }

    public function getRefillJSON($Refills)
    {
        $dataArray = array();
        $s_n = 1;
        foreach ($Refills as $refill) {
            $objectJSON = [];
            $date = date("d/m/Y", strtotime($refill->datetime));

            $objectJSON['s_n'] = $s_n;
            $objectJSON['date'] = $date;
            $objectJSON['technician'] = $refill->tech;
            $objectJSON['barcode'] = $refill->barcode;
            $objectJSON['serial_no'] = $refill->serial;
            $objectJSON['details'] = "<b>Charge in:</b><br>" . $refill->charge_in .
                "<br><b>Charge out:</b><br>" . $refill->charge_out;

            $s_n = $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
