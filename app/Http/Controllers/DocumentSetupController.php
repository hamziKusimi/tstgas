<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\DocumentSetup;

class DocumentSetupController extends Controller
{
    public function index()
    {
        $data["documentSetups"] = DocumentSetup::get();
        $documentsetups =  $data["documentSetups"];
        $data["page_title"] = "Document Setup";
        $data["bclvl1"] = "Document Setup Item Listing";
        $data["bclvl1_url"] = route('documentsetups.index');
       
        return view('settings.documentsetups.index', $data);
    }

    public function create()
    {
        $data["documentSetups"] = DocumentSetup::get();
        $data["page_title"] = "Edit Document Setup";
        $data["bclvl1"] = "Document Setup Item Listing";
        $data["bclvl1_url"] = route('documentsetups.index');
        $data["bclvl2"] = "Edit Document Setup";
        $data["bclvl2_url"] = route('documentsetups.create');

        return view('settings.documentsetups.create', $data);
    }

    public function update(Request $request, $id)
    {
        if (count($request['RN_ID']) > 0) {   
            for ($i = 0; $i < count($request['RN_ID']); $i++) {
                $docsetup = DocumentSetup::where('ID', $request['RN_ID'][$i]);
                $docsetup->update([
                    'D_PREFIX'=>$request['RN_D_PREFIX'][$i],
                    'D_SEPARATOR'=>$request['RN_D_SEPARATOR'][$i],
                    'D_LAST_NO'=>$request['RN_D_LAST_NO'][$i],
                    'D_ZEROES'=>$request['RN_D_ZEROES'][$i],
                    'D_ACTIVE'=>isset($request['RN_ACTIVE'][$i])? 1: 0,
                ]);
            }
        }
        return redirect()->route('documentsetups.create', $id)->with('Success', 'Document Setups successfully');
    }
}
