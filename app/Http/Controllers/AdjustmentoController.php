<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Adjustmento;
use App\Model\Adjustmentodt;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Uom;
use App\Model\Location;
use App\Model\PrintedIndexView;
use App\Model\Salesman;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use App\Model\Area;

class AdjustmentoController extends Controller
{
    public function index()
    {
        $data["adjustmentos"] = Adjustmento::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Adjustmento::pluck('docno', 'docno');
        $data["print"] = PrintedIndexView::where('index', 'Adjustment Out')->pluck('printed_by');
        $data["page_title"] = "Adjustment Out Listing";
        $data["bclvl1"] = "Adjustment Out Listing";
        $data["bclvl1_url"] = route('adjustmentos.index');
        return view('dailypro/adjustmentos.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["adjustmentosearch"] = Adjustmento::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Adjustmento::pluck('docno', 'docno');
        $data["page_title"] = "Adjustment Out Listing";
        $data["bclvl1"] = "Adjustment Out Listing";
        $data["bclvl1_url"] = route('adjustmentos.index');
        return view('dailypro.adjustmentos.index', $data);
    }

    public function adjustmentoDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'taxed_amount',
                        );

        $totalData = Adjustmento::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $adjustmentos = Adjustmento::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $adjustmentos =  Adjustmento::Where('docno', 'LIKE',"%{$search}%")
                            ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                            ->orWhere('date', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Adjustmento::Where('docno', 'LIKE',"%{$search}%")
                             ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                             ->orWhere('date', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($adjustmentos))
        {
            foreach ($adjustmentos as $key => $adjustmento)
            {
                $delete =  route('adjustmentos.destroy', $adjustmento->id);
                $edit =  route('adjustmentos.edit', $adjustmento->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('ADJUSTMENT_IN_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$adjustmento->docno.'</a>';
                }else{
                    $nestedData['docno'] = $adjustmento->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($adjustmento->date));
                $nestedData['taxed_amount'] = $adjustmento->taxed_amount;
                if(Auth::user()->hasPermissionTo('ADJUSTMENT_IN_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }

                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }
    public function api_store(Request $request)
    {
        // return $request['search'];
        $data = Adjustmento::select('id', 'docno', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();


        return response()->json($data);
    }

    public function create()
    {

        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Adjustment Out');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Adjustment Out";
        $data["bclvl1"] = "Adjustment Out Listing";
        $data["bclvl1_url"] = route('adjustmentos.index');
        $data["bclvl2"] = "Add Adjustment Out";
        $data["bclvl2_url"] = route('adjustmentos.create');
        // return response()->json($data);

        return view('dailypro/adjustmentos.create', $data);
    }

    public function store(Request $request)
    {
        $adjustmentoid = Adjustmento::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $adjustmentoDoc = DocumentSetup::findByName('Adjustment Out');
        $adjustmentoDoc->update(['D_LAST_NO' => $adjustmentoDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Adjustmentodt::create([
                    'doc_no' => $request['doc_no'],
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                    'item_code' => $request['item_code'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'ucost' => $request['unit_cost'][$i],
                    'type' => ($request['type'][$i] == 0) ? 0 : 1,
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal + $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }

        return redirect()->route('adjustmentos.edit', ['id' => $adjustmentoid])->with('Success', 'Adjustment Out added sucessfully');
    }

    public function show(Type $var = null)
    {
        return view('dailypro/adjustmentos.adjustmentos', $data);
    }

    public function edit($id)
    {
        $adjustmento = Adjustmento::find($id);
        $contents = Adjustmentodt::where('doc_no', $adjustmento->docno)->get();
        $selectedDebitor = Debtor::findByAcode($adjustmento->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $adjustmento;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Adjustment Out";
        $data["bclvl1"] = "Adjustment Out Listing";
        $data["bclvl1_url"] = route('adjustmentos.index');
        $data["bclvl2"] = "Edit Adjustment Out";
        $data["bclvl2_url"] = route('adjustmentos.edit', ['id' => $id]);

        // return response()->json( count($data["adjustmentodts"]));
        // return response()->json($adjustmentodts);
        return view('dailypro/adjustmentos.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $adjustmento = Adjustmento::find($id);
        if (isset($adjustmento)) {
            $adjustmento->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $adjustmentodt_id = Adjustmentodt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if ($adjustmentodt_id != null) {
                        $stkBalance = $stock->stkbal + $adjustmentodt_id->totalqty;
                        $adjustmentodt_id->update([
                            'doc_no' => $request['doc_no'],
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'type' => ($request['type'][$i] == 0) ? 0 : 1,
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal - $adjustmentodt_id->totalqty;
                    } else {
                        $dt = Adjustmentodt::create([
                            'doc_no' => $request['doc_no'],
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'type' => ($request['type'][$i] == 0) ? 0 : 1,
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal + $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
        }
        return redirect()->route('adjustmentos.edit', ['id' => $id])->with('Success', 'Adjustment Out updated sucessfully');
    }

    public function destroy($id)
    {

        $adjustmento = Adjustmento::find($id);
        $adjustmento->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmento->delete();
        // return redirect()->route('adjustmentos.index')->with('Success', 'Adjustment Out deleted successfully');

        $adjustmentodt = Adjustmentodt::where('doc_no', $adjustmento->docno);
        $adjustmentodt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmentodt->delete();
        return redirect()->route('adjustmentos.index')->with('Success', 'Adjustment Out deleted successfully');
    }


    public function destroyData($id)
    {
        $data = Adjustmentodt::find($id);

        if (isset($data)) {
            $Adjustmento = Adjustmento::where('docno','=',$data->doc_no)->first();
            $Adjustmento->update([
                'amount' => $Adjustmento->amount - $data->amount,
                'taxed_amount' => $Adjustmento->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyAdjustmentodt($id)
    {
        $adjustmentodt = Adjustmentodt::find($id);
        $adjustmentodt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmentodt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper($id)
    {
        //update printed details
        $adjustmento = Adjustmento::find($id);
        $getprinted = Adjustmento::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($adjustmento)) {
            $adjustmento->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $Resources = Adjustmento::where('id', $id)->get();
        $ResourcesJsonList = $this->getAdjustmentOutListingJSON($Resources);

        $formattedResource = Adjustmento::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'AdjustmentOut';
        $jrxml = 'general-bill-Adjustment.jrxml';
        $reportTitle = 'Adjustment Out';
        Transaction::generateBillAdjustment(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        $AdjustmentOut = $this->getAdjustmentOutQuery($request);

        if ($AdjustmentOut->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->AdjustmentOut_rcv == "listing") {
            $AdjustmentOutJSON = $this->getAdjustmentOutListingJSON($AdjustmentOut);
            $reportType = "AdjustmentOutListing";
        } else {
            $AdjustmentOutJSON = $this->getAdjustmentOutSummaryJSON($AdjustmentOut);
            $reportType = "AdjustmentOutSummary";
        }

        // check if JSON empty \\
        $AdjtJSON = json_decode($AdjustmentOutJSON);
        if (empty($AdjtJSON->data)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        // check if JSON empty \\

        $JsonFileName = "AdjustmentOut" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/DailyProcess/AdjustmentOut/' . $JsonFileName . '.json'), $AdjustmentOutJSON);
        $JasperFileName = "AdjustmentOut" . date("Ymdhisa") . Auth::user()->id;

        $file = $this->printReport($JsonFileName, $JasperFileName, $reportType);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/AdjustmentOut/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/AdjustmentOut/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/AdjustmentOut/' . $JsonFileName . '.json';

        $company_no = config('config.company.show_company_no') == 'true' ? config('config.company.company_no') : '';
        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => $company_no
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/AdjustmentOut/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getAdjustmentOutQuery(Request $request)
    {
        return Adjustmento::select('docno', 'date')
            ->where(function ($query) use ($request) {

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

    public function getAdjustmentOutListingJSON($AdjustmentOut)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Adjustment Out')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Adjustment Out')->update([
                'index' => "Adjustment Out",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Adjustment Out",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $dataArray = array();
        foreach ($AdjustmentOut as $AdjtOut) {

            $AdjtOutTrans = Adjustmentodt::select(
                'item_code',
                'subject',
                'qty',
                'uom',
                'ucost',
                'amount',
                'details',
                'taxed_amount'
            )
                ->where("doc_no", "=", $AdjtOut->docno)
                ->get();

            $lastElement = count($AdjtOutTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($AdjtOutTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($AdjtOut->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $AdjtOut->docno . ", " . $date;
                $objectJSON['description'] = $trans->subject;
                $objectJSON['item_code'] = $trans->item_code;
                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_cost'] = $trans->ucost;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $objectJSON['amount'];
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getAdjustmentOutSummaryJSON($AdjustmentOut)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Adjustment Out')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Adjustment Out')->update([
                'index' => "Adjustment Out",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Adjustment Out",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $dataArray = array();
        $s_n = 1;
        foreach ($AdjustmentOut as $AdjtOut) {
            $AdjtOutTrans = Adjustmentodt::select('amount', 'updated_at')
                ->where("doc_no", "=", $AdjtOut->docno)
                ->get();

            foreach ($AdjtOutTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $AdjtOut->docno;
                $objectJSON['date'] = $date;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getRouteByDocno(Request $request)
    {
        $Adjustmento = Adjustmento::where('docno', $request->docno)->first('id');

        if (!$Adjustmento) {

            $url = action('AdjustmentoController@create');
        } else {

            $url = action('AdjustmentoController@edit', ['id' => $Adjustmento->id]);
        }

        return $url;
    }
}
