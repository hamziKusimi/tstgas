<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SupplierEnquiry;
use App\Model\Creditor;
use App\Model\Stockcode;
use App\Model\Category;
use App\Model\Product;
use App\Model\Brand;
use App\Model\Location;
use App\Model\Uom;
use App\Model\View\CustomerView;


class SupplierEnqController extends Controller
{
    public function index()
    {
        // $data["suppliers"] = CustomerView::paginate(15);
        // $data["page_title"] = "Supplier Enquiry Item Listing";
        // $data["bclvl1"] = "Supplier Enquiry Item Listing";
        // $data["bclvl1_url"] = route('suppliers.index');
        
        // return view('enquiry.suppliers.index', $data);
        
        $data["creditors"] = Creditor::pluck('accountcode');
        $data["stockcodes"] = Stockcode::pluck('code');
        $data["categories"] = Category::pluck('code');
        $data["products"] = Product::pluck('code');
        $data["brands"] = Brand::pluck('code');
        $data["locations"] = Location::pluck('code');
        $data["uoms"] = Uom::pluck('code');
        // return response()->json($data);
        $data["page_title"] = "Supplier Enquiry Item Listing";
        $data["bclvl1"] = "Supplier Enquiry Item Listing";
        $data["bclvl1_url"] = route('suppliers.index');
        $data["bclvl2"] = "Supplier Enquiry Search";
        $data["bclvl2_url"] = route('suppliers.search');

        return view('enquiry.suppliers.search', $data);
    }

    public function search(Request $request)
    {
        $data["creditors"] = Creditor::pluck('accountcode');
        $data["stockcodes"] = Stockcode::pluck('code');
        $data["categories"] = Category::pluck('code');
        $data["products"] = Product::pluck('code');
        $data["brands"] = Brand::pluck('code');
        $data["locations"] = Location::pluck('code');
        $data["uoms"] = Uom::pluck('code');
        // return response()->json($data);
        $data["page_title"] = "Supplier Enquiry Item Listing";
        $data["bclvl1"] = "Supplier Enquiry Item Listing";
        $data["bclvl1_url"] = route('suppliers.index');
        $data["bclvl2"] = "Supplier Enquiry Search";
        $data["bclvl2_url"] = route('suppliers.search');

        return view('enquiry.suppliers.search', $data);
    }

    public function query(Request $request)
    {
        // return response()->json($request);
        $data["suppliers"] = CustomerView::select('*')
        ->where(function ($query) use ($request) {
            if ($request->docno == "1") {
                $query->whereBetween('doc_no', [$request->docnofrm, $request->docnoto]);
                // dd($request->docnofrm);
            }

            if($request->creditor == "1"){
                $query->whereBetween('account_code', [$request->creditorfrm, $request->creditorto]);
            }

            if($request->stockcode == "1"){
                $query->whereBetween('item_code', [$request->stockcodefrm, $request->stockcodeto]);
            }

            if($request->category == "1"){
                $query->whereBetween('category', [$request->categoryfrm, $request->categoryto]);
            }

            if($request->product == "1"){
                $query->whereBetween('product', [$request->productfrm, $request->productto]);
            }

            if($request->brand == "1"){
                $query->whereBetween('brand', [$request->brandfrm, $request->brandto]);
            }

            if($request->location == "1"){
                $query->whereBetween('location', [$request->locationfrm, $request->locationto]);
            }

            $lefttextname1 = $request->lefttextname1;
            if(!empty($lefttextname1)){
                $query->where($lefttextname1, 'LIKE', $request->lefttextval1 . '%');
            }

            $ctxtextname1 = $request->ctxtextname1;
            if(!empty($ctxtextname1)){
                $query->where($ctxtextname1, 'LIKE', '%' . $request->ctxtextval1 . '%');
            }

            $lefttextname2 = $request->lefttextname2;
            if(!empty($lefttextname2)){
                $query->where($lefttextname2, 'LIKE', $request->lefttextval2 . '%');
            }

            $ctxtextname2 = $request->ctxtextname2;
            if(!empty($ctxtextname2)){
                $query->where($ctxtextname2, 'LIKE', '%' . $request->ctxtextval2 . '%');
            }

            if($request->active == "1"){
                $query->where('inactive', '0');
            }
            
            if($request->stock == "1"){
                $query->where('type', 'Stock Item');
            }
            
            if($request->nonstock == "1"){
                $query->where('type', 'None Stock Item');
            }

            if($request->inactive == "1"){
                $query->where('inactive', '1');
            }

            if($request->service == "1"){
                $query->where('type', 'Service');
            }

            if($request->inactivestock == "1"){
                $query->where('type', 'inActive Stock Item');
            }
                         
        })
        ->orderBy('date')
        ->paginate(15);

        $data["page_title"] = "Supplier Enquiry Item Listing";
        $data["bclvl1"] = "Supplier Enquiry Item Listing";
        $data["bclvl1_url"] = route('suppliers.index');
        $data["bclvl2"] = "Supplier Search Item Listing";
        $data["bclvl2_url"] = route('suppliers.index');
        return view('enquiry.suppliers.index', $data);
    }
}
