<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Good;
use App\Model\Gooddt;
use App\Model\Creditor;
use App\Model\Debtor;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Uom;
use App\Model\Porder;
use App\Model\Porderdt;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Location;
use App\Model\Salesman;
use App\Model\PrintedIndexView;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use DB;
use DataTables;
use App\Model\Area;
use PDF;

class GoodController extends Controller
{
    public function index()
    {
        $data["goods"] = Good::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Good::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Goods Received')->pluck('printed_by');
        $data["page_title"] = "Goods Received Listing";
        $data["bclvl1"] = "Goods Received Listing";
        $data["bclvl1_url"] = route('goods.index');

        // return response()->json($data);

        return view('dailypro.goods.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["goodsearch"] = Good::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppdo', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppinv', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ptype', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Good::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Goods Received')->pluck('printed_by');
        $data["page_title"] = "Goods Received Listing";
        $data["bclvl1"] = "Goods Received Listing";
        $data["bclvl1_url"] = route('goods.index');
        return view('dailypro.goods.index', $data);
    }

    public function goodDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'suppdo',
                            6 => 'suppinv',
                            7 => 'ref',
                            8 => 'ptype',
                            9 => 'taxed_amount',
                        );

        $totalData = Good::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $goods = Good::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $goods =  Good::Where('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('suppdo', 'LIKE',"%{$search}%")
                            ->orWhere('suppinv', 'LIKE',"%{$search}%")
                            ->orWhere('ref', 'LIKE',"%{$search}%")
                            ->orWhere('ptype', 'LIKE',"%{$search}%")
                            ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                            ->orWhere('date', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Good::Where('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->orWhere('suppdo', 'LIKE',"%{$search}%")
                             ->orWhere('suppinv', 'LIKE',"%{$search}%")
                             ->orWhere('ref', 'LIKE',"%{$search}%")
                             ->orWhere('ptype', 'LIKE',"%{$search}%")
                             ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                             ->orWhere('date', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($goods))
        {
            foreach ($goods as $key => $good)
            {
                $delete =  route('goods.destroy', $good->id);
                $edit =  route('goods.edit', $good->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('GOOD_RC_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$good->docno.'</a>';
                }else{
                    $nestedData['docno'] = $good->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($good->date));
                $nestedData['account_code'] = $good->account_code;
                $nestedData['name'] = $good->name;
                $nestedData['suppdo'] = $good->suppdo;
                $nestedData['suppinv'] = $good->suppinv;
                $nestedData['ref'] = $good->ref;
                $nestedData['ptype'] = $good->ptype;
                $nestedData['taxed_amount'] = $good->taxed_amount;

                if(Auth::user()->hasPermissionTo('GOOD_RC_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function api_store(Request $request)
    {
        $data = Good::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppdo', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppinv', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ptype', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function docnoList()
    {
        $query = Good::query()
            ->selectRaw('docno, date, name');

        return DataTables::of($query)->make(true);
    }

    public function debtorList()
    {
        $query = Creditor::query()
            ->selectRaw('accountcode, name')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }


    public function editRoute($id)
    {
        $resources = Stockcode::where('id', $id)->pluck('code');

        return response()->json($resources);
    }

    public function editAllRoute($id)
    {
        $responses = Stockcode::where('id', $id)->get();

        return response()->json($responses);
    }

    public function pono_get($creditor)
    {
        $response = Porder::where('account_code', $creditor)->get();

        return response()->json($response);
    }

    public function getPOrderDt(Request $request)
    {
        $goods = Porderdt::where('doc_no', $request['docno'])->orderBy('sequence_no')->get();

        return response()->json($goods);
    }

    public function search(Request $request)
    {
        $data = [];
        $data['accounts'] = Debtor::where('D_NAME', 'LIKE', '%' . $request['name_search'] . '%')
            ->when(isset($request['acode_search']), function ($query) use ($request) {
                return $query->where('D_ACODE', 'LIKE', '%' . $request['acode_search'] . '%');
            })
            ->paginate($request['pageNumbers']);

        return view('file-maintenance.debtors.index', $data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isCreditor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Goods Received');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountcode');
        $systemsetup = SystemSetup::first();
        $uom = Uom::get();
        $uom1 = Stockcode::pluck('uom_id1')->toArray();
        $uom2 = Stockcode::pluck('uom_id2')->toArray();
        $uom3 = Stockcode::pluck('uom_id3')->toArray();
        $uoms = json_encode(array_unique(array_merge($uom1, $uom2, $uom3)));

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data['systemsetup'] = $systemsetup;
        $data["uom"] = $uom;
        $data['uoms'] = $uoms;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Goods Received";
        $data["bclvl1"] = "Goods Received Listing";
        $data["bclvl1_url"] = route('goods.index');
        $data["bclvl2"] = "Add Goods Received";
        $data["bclvl2_url"] = route('goods.create');
        // return response()->json($data);

        return view('dailypro.goods.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $Goodid = Good::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'suppdo' => $request['suppdo'],
            'suppinv' => $request['suppinv'],
            'suppinvdate' => Carbon::createFromFormat('d/m/Y', $request['suppinvdate']),
            'ref' => $request['ref'],
            'ptype' => $request['ptype'],
            'pono' => $request['pono'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'currency' => $request['currency'],
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $GoodDoc = DocumentSetup::findByName('Goods Received');
        $GoodDoc->update(['D_LAST_NO' => $GoodDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Gooddt::create([
                    'doc_no' => $Goodid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'reference_no' => $request['reference_no'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'cucost' => $request['cunit_cost'][$i],
                    'mkup' => $request['markup'][$i],
                    'ucost' => $request['unit_cost'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                if (isset($stock)) {
                    $stkBalance = $stock->stkbal + $dt->totalqty;
                    $stock->update(['stkbal' => $stkBalance]);
                }
            }
        }
        return redirect()->route('goods.edit', ['id' => $Goodid])->with('Success', 'Goods added sucessfully');
    }

    public function show($id)
    { }

    public function edit($id)
    {
        $good = Good::find($id);
        $contents = Gooddt::where('doc_no', $good->docno)->get();
        // return response()->json($contents);

        $selectedCreditor = Creditor::findByAcode($good->account_code);
        $masterCodes = AccountMastercode::isCreditor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();
        $uom1 = Stockcode::pluck('uom_id1')->toArray();
        $uom2 = Stockcode::pluck('uom_id2')->toArray();
        $uom3 = Stockcode::pluck('uom_id3')->toArray();
        // $uoms = Stockcode::pluck('uoms.code')->get();
        // return response()->json($stockcode->uoms);
        // $uoms = array_unique(array_merge($uom1, $uom2, $uom3));
        $uoms = Stockcode::get()->pluck('uoms');


        $data = [];
        $data['item'] = $good;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedCreditor'] = $selectedCreditor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['uoms'] = $uoms;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Goods Received";
        $data["bclvl1"] = "Goods Received Listing";
        $data["bclvl1_url"] = route('goods.index');
        $data["bclvl2"] = "Edit Goods Received";
        $data["bclvl2_url"] = route('goods.edit', ['id' => $id]);
        return view('dailypro.goods.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $good = Good::find($id);
        if (isset($good)) {
            // return response()->json(!empty($request['tax']));
            // $good->tax_amount = $request['tax'];
            // return response()->json($good->tax_amount);

            $good->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'suppdo' => $request['suppdo'],
                'suppinv' => $request['suppinv'],
                'suppinvdate' => Carbon::createFromFormat('d/m/Y', $request['suppinvdate']),
                'ref' => $request['ref'],
                'ptype' => $request['ptype'],
                'pono' => $request['pono'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'currency' => $request['currency'],
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {
                    $gooddt_id = Gooddt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if ($gooddt_id != null) {
                        $stkBalance = $stock->stkbal - $gooddt_id->totalqty;
                        $gooddt_id->update([
                            'doc_no' => $good->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'cucost' => $request['cunit_cost'][$i],
                            'mkup' => $request['markup'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'discount' => $request['discount'][$i],
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id

                        ]);

                        $stkBalance = $stkBalance + $gooddt_id->totalqty;
                    } else {
                        $dt = Gooddt::create([
                            'doc_no' => $good->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'cucost' => $request['cunit_cost'][$i],
                            'mkup' => $request['markup'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id

                        ]);

                        $stkBalance = $stock->stkbal + $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
        }
        return redirect()->route('goods.edit', ['id' => $id])->with('Success', 'Goods updated sucessfully');
    }

    public function destroy($id)
    {
        $good = Good::find($id);
        $good->update([
            'deleted_by' => Auth::user()->id
        ]);
        $good->delete();
        $docno = $good->docno;

        $gooddt = Gooddt::where('doc_no', $docno);
        $gooddt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $gooddt->delete();
        return redirect()->route('goods.index')->with('Success', 'Goods Received deleted successfully');
    }

    public function destroyData($id)
    {
        $data = Gooddt::find($id);

        if (isset($data)) {
            $Good = Good::where('docno','=',$data->doc_no)->first();
            $Good->update([
                'amount' => $Good->amount - $data->amount,
                'taxed_amount' => $Good->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function jasper($id)
    {
        //update printed details
        $good = Good::find($id);
        $getprinted = Good::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($good)) {
            $good->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $originalResource = Good::find($id);
        $Resources = Good::where('id', $id)->get();
        $ResourcesJsonList = $this->getGoodsListingJSON($Resources);

        $formattedResource = Good::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'GoodReceived';
        $jrxml = 'general-bill-one.jrxml';
        $reportTitle = 'Goods Received';
        $jasperData = Transaction::generateBill(
            $formattedResource,
            $originalResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Goods Received')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Goods Received')->update([
                'index' => "Goods Received",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Goods Received",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $systemsetup = SystemSetup::pluck('qty_decimal')->first();

        $dates = $request->dates;
        $GoodsReceived = $this->getGoodsQuery($request);

        if (empty($GoodsReceived)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->goods_rcv == "listing") {
            foreach ($GoodsReceived as $GR) {

                $amount = $amount + $GR->amount;
            }
            $pdf = PDF::loadView('dailypro/goods.listing', [
                'GoodsReceivedJSON' => $GoodsReceived,
                'finaltotalamount' => $amount,
                'date' =>  $dates,
                'systemsetup' => $systemsetup
            ])->setPaper('A4', 'landscape');
            return $pdf->inline();
        } else {

            foreach ($GoodsReceived as $GR) {

                $amount = $amount + $GR->totalamount;
            }

            $pdf = PDF::loadView('dailypro/goods.summary', [
                'GoodsReceivedJSON' => $GoodsReceived,
                'finaltotalamount' => $amount,
                'date' =>  $dates,
                'systemsetup' => $systemsetup
            ])->setPaper('A4');
            return $pdf->inline();
        }
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/GoodsReceived/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/GoodsReceived/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/GoodsReceived/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/GoodsReceived/' . $JasperFileName . '.pdf';
        return $file;
    }


    public function getGoodsQuery(Request $request)
    {
        $systemsetupstock = SystemSetup::pluck('stock_item')->first();

        if($request->goods_rcv == "summary"){
            $type = "GROUP BY docno";
            if($systemsetupstock == '1'){

                // $stock = "AND stockcodes.type = 'Stock Item'";
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM gooddts INNER JOIN stockcodes on ((gooddts.item_code = stockcodes.code) and (stockcodes.type = 'Stock Item'))
                WHERE gooddts.item_code = item_code and gooddts.doc_no = docno and gooddts.deleted_at IS NULL) AS totalqty,";
            }else{
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM gooddts WHERE gooddts.item_code = item_code and gooddts.doc_no = docno and gooddts.deleted_at IS NULL) AS totalqty,";

            }
        }else{
            $type = "GROUP BY docno, item_code";
            $stock = "";
            $totalqty = "";
        }

        if ($request->dates_Chkbx == "on") {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');


            if ($dates_from == $dates_to) {
                $date = "AND goods.date = '" . $dates_from . "'";
            } else {
                $date = "AND goods.date >= '" . $dates_from . "' AND goods.date <= '" . $dates_to . "'";
            }

            // if ($dates_from == $dates_to) {
            //     $query->where('goods.date', '=', $dates_from);
            // } else {
            //     $query->whereBetween('goods.date', [$dates_from, $dates_to]);
            // }
        }else{
            $date = "";
        }

        if ($request->docno_Chkbx == "on") {

            $docno = "AND (goods.docno >= '" . $request->docno_frm . "' AND goods.docno <= '" . $request->docno_to . "')";

            // $query->whereBetween('goods.docno', [$request->docno_frm, $request->docno_to]);
        }else{
            $docno = "";
        }

        if ($request->suppDo_Chkbx == "on") {

            if ($request->suppDo_to == null) {
                $suppdo = "AND (goods.suppdo = '" . $request->suppDo_frm . "')";
            } else {
                $suppdo = "AND (goods.suppdo >= '" . $request->suppDo_frm . "' AND goods.suppdo <= '" . $request->suppDo_to . "')";
            }

            // if ($request->suppDo_to == null) {
            //     $query->where('goods.suppdo', "=", $request->suppDo_frm);
            // } else {
            //     $query->whereBetween('goods.suppdo', [$request->suppDo_frm, $request->suppDo_to]);
            // }
        }else{
            $suppdo = "";
        }

        if ($request->suppInv_Chkbx == "on") {

            if ($request->suppInv_to == null) {
                $suppinv = "AND (goods.suppinv = '" . $request->suppInv_frm . "')";
            } else {
                $suppinv = "AND (goods.suppinv >= '" . $request->suppInv_frm . "' AND goods.suppinv <= '" . $request->suppInv_to . "')";
            }

            // if ($request->suppInv_to == null) {
            //     $query->where('goods.suppinv', "=", $request->suppInv_frm);
            // } else {
            //     $query->whereBetween('goods.suppinv', [$request->suppInv_frm, $request->suppInv_to]);
            // }
        }else{
            $suppinv = "";
        }

        if ($request->refNo_Chkbx == "on") {

            if ($request->refNo_to == null) {
                $ref = "AND (goods.ref = '" . $request->refNo_frm . "')";
            } else {
                $ref = "AND (goods.ref >= '" . $request->refNo_frm . "' AND goods.ref <= '" . $request->refNo_to . "')";
            }

            // if ($request->refNo_to == null) {
            //     $query->where('goods.ref', "=", $request->refNo_frm);
            // } else {
            //     $query->whereBetween('goods.ref', [$request->refNo_frm, $request->refNo_to]);
            // }
        }else{
            $ref = "";
        }

        if ($request->credCode_Chkbx == "on") {

            $creditor = "AND goods.account_code >= '" . $request->credCode_frm . "' AND goods.account_code <= '" . $request->credCode_to . "'";

            // $query->whereBetween('goods.account_code', [$request->credCode_frm, $request->credCode_to]);
        }else{
            $creditor = "";
        }

        $Good = DB::select(DB::raw("
                SELECT
                `goods`.`docno`          AS `docno`,
                `goods`.`date`           AS `date`,
                `goods`.`suppdo`          AS `suppdo`,
                `goods`.`suppinv`          AS `suppinv`,
                `goods`.`ref`          AS `ref`,
                `goods`.`account_code`   AS `account_code`,
                `goods`.`account_code`   AS `debtor`,
                `goods`.`name`           AS `name`,
                `goods`.`amount`           AS `totalamount`,
                CONCAT(CONCAT(`goods`.`docno`),', ',DATE_FORMAT(`goods`.`date`,'%d-%m-%Y'),', ',`goods`.`account_code`,', ',`goods`.`name`) AS `details`,
                `gooddts`.`subject`      AS `description`,
                `gooddts`.`id`           AS `id`,
                `gooddts`.`doc_no`       AS `doc_no`,
                `gooddts`.`item_code`    AS `item_code`,
                `gooddts`.`qty`          AS `quantity`,
                `gooddts`.`uom`          AS `uom`,
                `gooddts`.`ucost`       AS `ucost`,
                `gooddts`.`amount`       AS `amount`,
                `gooddts`.`taxed_amount` AS `tax_amount`,
                `gooddts`.`subject`      AS `subject`,
                `gooddts`.`details`      AS `DETAIL`,
                `stockcodes`.`loc_id`        AS `loc_id`,
                goods.amount AS totalamt,
                $totalqty
                (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location
            FROM ((`goods`
                JOIN `gooddts`
                    ON ((`goods`.`docno` = `gooddts`.`doc_no`)))
                JOIN `stockcodes`
                ON ((`gooddts`.`item_code` = `stockcodes`.`code`)))
                WHERE `gooddts`.`deleted_at` IS NULL AND `goods`.`deleted_at` IS NULL
                $stock $date $docno $suppdo $suppinv $ref $creditor
                $type
                order by docno

        "), []);

        return $Good;

    }

    public function getGoodsListingJSON($goodsReceived)
    {

        $dataArray = array();
        foreach ($goodsReceived as $goods) {

            $goodsTrans = Gooddt::select(
                'item_code',
                'subject',
                'qty',
                'taxed_amount',
                'uom',
                'ucost',
                'amount',
                'details'
            )
                ->where("doc_no", "=", $goods->docno)
                ->get();

            $lastElement = count($goodsTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($goodsTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($goods->date));
                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $goods->docno . ", " . $date . ", " .
                    $goods->account_code . ", " . $goods->name;
                $objectJSON['description'] = $trans->subject;

                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['item_code'] = $trans->item_code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_cost'] = $trans->ucost;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $trans->amount;
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getGoodsSummaryJSON($goodsReceived)
    {
        //update printing details
        // $pr = substr($goodsReceived->docno, 0, strpos($goodsReceived->docno, "/"));
        // if($pr == "GR"){

        $getprinted = PrintedIndexView::where('index', 'Goods Received')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Goods Received')->update([
                'index' => "Goods Received",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Goods Received",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        // }

        $dataArray = array();
        $s_n = 1;
        foreach ($goodsReceived as $goods) {

            $goodsTrans = Gooddt::select('amount', 'updated_at')
                ->where("doc_no", "=", $goods->docno)
                ->get();

            foreach ($goodsTrans as $trans) {


                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $goods->docno;
                $objectJSON['date'] = $date;
                $objectJSON['suppdo'] = $goods->suppdo;
                $objectJSON['suppinv'] = $goods->suppinv;
                $objectJSON['creditor'] = $goods->account_code;
                $objectJSON['name'] = $goods->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getRouteByDocno(Request $request)
    {
        $Good = Good::where('docno', $request->docno)->first('id');

        if (!$Good) {

            // Session::flash('message', 'This is a message!');
            $url = action('GoodController@create');
        } else {

            $url = action('GoodController@edit', ['id' => $Good->id]);
        }

        return $url;
    }
}
