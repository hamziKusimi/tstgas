<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$products = product::latest()->paginate(10);
        $data["products"] = Product::get();
        // $data["products"] = Product::orderBy('created_at', 'asc')->latest()->paginate(10);
        // $data["pagetitle"] = "Add Product";
        // $data["products"] = product::latest()->paginate(10);
        $data["page_title"] = "Product Item Listing";
        $data["bclvl1"] = "Product Item Listing";
        $data["bclvl1_url"] = route('products.index');
        //return view("products.index", compact('products'))->with('i', (request()->input('page', 1) - 1) * 10);
        //return response()->json($data);
        // dd('test');
        return view('stockmaster.products.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["page_title"] = "Add Product";
        $data["bclvl1"] = "Product Item Listing";
        $data["bclvl1_url"] = route('products.index');
        $data["bclvl2"] = "Add Product";
        $data["bclvl2_url"] = route('products.create');

        return view('stockmaster.products.create',$data);
    }

    public function store(Request $request)
    {
        $product = Product::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('products.index')->with('Success', 'Product created successfully.');
    }

    public function show(product $product)
    {
        $product = Product::where('id', $product->id)->first();
        $data = [];
        $data['product'] = $product;

        return view('stockmaster.products.show', compact('product'));
    }

    public function edit($id)
    {
        $data["product"] = Product::find($id);
        $data["bclvl1"] = "Product Item Listing";
        $data["bclvl1_url"] = route('products.index');
        $data["page_title"] = "Edit Product";
        $data["bclvl2"] = "Edit Product";
        $data["bclvl2_url"] = route('products.edit', ['id'=>$id]);
        return view('stockmaster.products.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        // return response()->json($request);
        if (isset($product)) {
            $product->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('products.index')->with('Success', 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(product $product)
    {
        $product->update([
            'deleted_by' => Auth::user()->id
        ]);
        $product->delete();
        return redirect()->route('products.index')->with('Success', 'Product deleted successfully.');
    }

    public function print()
    {
        $Products = Product::all();

        $dataArray = array();
        $no = 1;
        foreach ($Products as $Product) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Product->code;
            $objectJSON["description"] = $Product->descr;
            $objectJSON["status"] = $Product->active ? "Active":"Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $ProductsJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintProduct" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $ProductsJSON);
        $JasperFileName = "ReportPrintProduct" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Product Listing Report (" .  date('d/m/Y') .")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }
    public function download()
    {
        return "hi";
    }
}
