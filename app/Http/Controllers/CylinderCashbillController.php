<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CylinderCashbill;
use App\Model\CylinderCashbilldt;
use App\Model\DocumentSetup;
use App\Model\Cylinder;
use Carbon\Carbon;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Debtor;
use App\Model\Location;
use App\Model\Cylindercategory;
use App\Model\SystemSetup;
use App\Model\Stockcode;
use App\Model\Gasrack;
use App\Model\DeliveryNote;
use App\Model\DeliveryNotedt;
use App\Model\Salesman;
use App\Model\PrintedIndexView;
use App\Model\CustomObject\Transaction;
use PDF;
use Storage;
use DB;
use App\Model\Area;

class CylinderCashbillController extends Controller
{
    public function index()
    {
        $data["cashbills"] = CylinderCashbill::orderBy('docno', 'desc')->get();
        $data["customers"] = CylinderCashbill::pluck('name', 'account_code');
        $data["dates"] = CylinderCashbill::pluck('date', 'date');
        $data["docno_select"] = CylinderCashbill::orderBy('docno')->pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode')->toArray();
        $data["page_title"] = "Cash Bill Listing";
        $data["bclvl1"] = "Cash Bill Listing";
        $data["bclvl1_url"] = route('cylindercashbills.index');
        return view('dailypro.cylindercashbills.index', $data);
    }

    public function api_store(Request $request)
    {
        // return $request['search'];
        $data = CylinderCashbill::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('lpono', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('dnno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('quotno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $runningNumber = DocumentSetup::findByName('Cylinder Cashbill');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $systemsetup = SystemSetup::first();
        // $product = Cylinder::whereNotNull('serial')->get(['barcode','serial','cat_id', 'prod_id', 'capacity', 'testpressure']);
        $product = Cylinder::whereNotNull('barcode')->pluck('serial');

        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['masterCodes'] = $masterCodes;
        $data['runningNumber'] = $runningNumber;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;
        $data['cylinder'] = $product;

        // return response()->json($data);
        $data["page_title"] = "Add Cash Bill";
        $data["bclvl1"] = "Cash Bill Listing";
        $data["bclvl1_url"] = route('cylindercashbills.index');
        $data["bclvl2"] = "Add Cash Bill";
        $data["bclvl2_url"] = route('cylindercashbills.create');
        return view('dailypro.cylindercashbills.create', $data);
    }

    public function editRoute($id)
    {
        $resources = Cylinder::where('id', $id)->first();

        return response()->json($resources);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $cashbill = CylinderCashbill::create([
            'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'lpono' => !empty($request['lpono']) ? $request['lpono'] : '',
            'dnno' => !empty($request['dnno']) ? $request['dnno'] : '',
            'ref1' => !empty($request['ref1']) ? $request['ref1'] : '',
            'quotno' => !empty($request['quotno']) ? $request['quotno'] : '',
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'currency' => !empty($request['currency']) ? $request['currency'] : '',
            'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
            'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
            'created_by' => Auth::user()->id
        ]);

        // dd(count($request['subject']));
        if (count($request['subject']) > 1) {
            for ($i = 1; $i < count($request['subject']); $i++) {
                $dt = CylinderCashbilldt::create([
                    'doc_no' => $cashbill->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'serial' => $request['serial'][$i],
                    'barcode' => $request['barcode'][$i],
                    'reference_no' => $request['reference_no'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'product' => $request['product'][$i],
                    'type' => $request['type'][$i],
                    'quantity' => $request['quantity'][$i],
                    'uom' => $request['uom'][$i],
                    'unit_price' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }


        $cashbillDoc = DocumentSetup::findByName('Cylinder Cashbill');
        $cashbillDoc->update(['D_LAST_NO' => $cashbillDoc->D_LAST_NO + 1]);

        return redirect()->route('cylindercashbills.edit', ['id' => $cashbill->id])->with('Success', 'Cashbill added successfully');
    }

    public function edit($id)
    {
        $cashbill = CylinderCashbill::find($id);
        $contents = CylinderCashbilldt::where('doc_no', $cashbill->docno)->get();

        // return response()->json($cashbill->doc_no);
        $selectedDebitor = Debtor::findByAcode($cashbill->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();;
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $systemsetup = SystemSetup::first();
        $productval = Cylinder::pluck('serial', 'serial');
        $product = Cylinder::pluck('serial');
        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['item'] = $cashbill;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;
        $data['productval'] = $productval;
        $data['cylinder'] = $product;
        $data["page_title"] = "Edit Cash Bill";
        $data["bclvl1"] = "Cash Bill Listing";
        $data["bclvl1_url"] = route('cylindercashbills.index');
        // $data["bclvl2"] = "Edit cashbills";
        // $data["bclvl2_url"] = route('cashbills.edit');

        // return response()->json($data);

        return view('dailypro.cylindercashbills.edit', $data);
    }


    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // update acc_invmt row
        $cashbill = CylinderCashbill::find($id);
        if (isset($cashbill)) {

            // update cashbill
            $cashbill->update([
                'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'lpono' => !empty($request['lpono']) ? $request['lpono'] : '',
                'dnno' => !empty($request['dnno']) ? $request['dnno'] : '',
                'ref1' => !empty($request['ref1']) ? $request['ref1'] : '',
                'quotno' => !empty($request['quotno']) ? $request['quotno'] : '',
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['serial']) > 1) {
                for ($i = 1; $i < count($request['serial']); $i++) {

                    $dt = CylinderCashbilldt::find($request['item_id'][$i]);

                    if (isset($dt)) {

                        $dt->update([
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        $dt = CylinderCashbilldt::create([
                            'doc_no' => $cashbill->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }


        return redirect()->route('cylindercashbills.edit', ['id' => $id])->with('Success', 'Cash Bill updated successfully');
    }

    public function destroy($id)
    {

        $cashbill = CylinderCashbill::find($id);
        $cashbill->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cashbill->delete();
        $docno = $cashbill->docno;

        $cylinderCashbilldt = CylinderCashbilldt::where('doc_no', $docno);
        $cylinderCashbilldt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylinderCashbilldt->delete();
        return redirect()->route('cylindercashbills.index')->with('Success', 'Cash Bill deleted successfully');
    }

    public function destroyData($id)
    {
        $data = CylinderCashbilldt::find($id);

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyCashbilldt($id)
    {
        $cashbilldt = CylinderCashbilldt::find($id);
        $cashbilldt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cashbilldt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper($id)
    {
        $Resources = CylinderCashbill::where('id', $id)->get();
        $invoice = CylinderCashbill::select(
            'docno',
            'date',
            'account_code',
            'name',
            'addr1',
            'addr2',
            'addr3',
            'addr4',
            'tel_no',
            'fax_no',
            'amount',
            'taxed_amount',
            'header',
            'footer',
            'summary'
        )->where('id', $id)->first();

        $CTERM = Debtor::Select('cterm')
            ->where('accountcode', '=',  $invoice->ACC_CODE)->first();

        $data = [];
        $data['invoice'] = $invoice;
        $data['invdata'] = CylinderCashbilldt::where("doc_no", "=", $invoice->docno)
            ->get();
        $data['cterm'] = $CTERM;
        $systemSetup = SystemSetup::first(['bank_detail', 'bank_account_no']);

        $pdf = PDF::loadView('dailypro.cylindercashbills.cashbills-pdf', ['inv' => $data, 'systemSetup' => $systemSetup])->setPaper('A4');
        // $pdf = PDF::loadView('dailypro.cylinderinvoices.invoices-pdf', ['inv' => $data, 'systemSetup' => $systemSetup])->setPaper('A4');

        // $datass["inv"] = $data;
        // $datass["systemSetup"] = $systemSetup;
        // return view('dailypro.cylinderinvoices.invoices-pdf', $datass);
        return $pdf->inline();

    }


    public function getcashbillListingJSON($CashBill)
    {
        $dataArray = array();

        foreach ($CashBill as $cashbill) {
            $cashbillTrans = CylinderCashbilldt::select(
                'serial',
                'barcode',
                'subject',
                'barcode',
                'serial',
                'product',
                'unit_price',
                'details',
                'amount',
                'taxed_amount'
            )
                ->where("doc_no", "=", $cashbill->docno)
                ->get();

            $lastElement = count($cashbillTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($cashbillTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($cashbill->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $cashbill->docno . ", " . $date . ", " .
                    $cashbill->account_code . ", " . $cashbill->name;
                $objectJSON['description'] = $trans->subject;

                $objectJSON['barcode'] = $trans->barcode;
                $objectJSON['serial'] = $trans->serial;
                $objectJSON['product'] = $trans->product;
                $objectJSON['unit_price'] = $trans->unit_price;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $objectJSON['amount'];
                $t_qty =  $t_qty + $trans->quantity;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }


    public function dnno_get($debtor)
    {
        $response = DeliveryNote::where('account_code', $debtor)->get();

        return response()->json($response);
    }

    public function getDnDt(Request $request)
    {
        $dorders = DeliveryNotedt::where('dn_no', $request['docno'])->orderBy('sequence_no')->get();

        return response()->json($dorders);
    }

    public function printIndex(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Cylinder Cashbill')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Cylinder Cashbill')->update([
                'index' => "Cylinder Cashbill",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Cylinder Cashbill",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;

        $CashBill = $this->getcashbillQuery($request);
        if (empty($CashBill)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->Cashbill_rcv == "listing") {
            $reportType = "CashBillListing";

            foreach ($CashBill as $cb) {

                $amount = $amount + $cb->amount;
            }

            $data['cashbillJSON'] = $CashBill;
            $data['totalamount'] = $amount;
            return view('dailypro/cylindercashbills.listing', $data);
        } else {

            foreach ($CashBill as $cb) {

                $amount = $amount + $cb->amount;
            }

            $reportType = "cylinderCashBillSummary";


            $data['cashbillJSON'] = $CashBill;
            $data['totalamount'] = $amount;
            // return $data;
            return view('dailypro/cylindercashbills.summary', $data);
        }
    }

    public function getcashbillQuery(Request $request)
    {
        if ($request->dates_Chkbx == "on") {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

            if ($dates_from == $dates_to) {
                $date = "AND cylinder_cashbills.date = '" . $dates_from . "'";
            } else {
                $date = "AND cylinder_cashbills.date >= '" . $dates_from . "' AND cylinder_cashbills.date <= '" . $dates_to . "'";
            }

            if ($request->docno_Chkbx == "on") {
                $docno = "AND (cylinder_cashbills.docno >= '" . $request->docno_frm . "' AND cylinder_cashbills.docno <= '" . $request->docno_to . "')";

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cylinder_cashbills.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cylinder_cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cylinder_cashbills.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                }
            } else {
                $docno = '';

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cylinder_cashbills.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cylinder_cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cylinder_cashbills.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                }
            }
        } else {
            $date = '';
            if ($request->docno_Chkbx == "on") {
                $docno = "AND cylinder_cashbills.docno >= '" . $request->docno_frm . "' AND cylinder_cashbills.docno <= '" . $request->docno_to . "'";

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cylinder_cashbills.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cylinder_cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cylinder_cashbills.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                }
            } else {
                $docno = '';

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND cylinder_cashbills.lpono = '" . $request->LPO_frm_1 . "'";
                    } else {
                        $lpono = "AND cylinder_cashbills.lpono >= '" . $request->LPO_frm_1 . "' AND cylinder_cashbills.lpono <= '" . $request->LPO_to_1 . "'";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND cylinder_cashbills.account_code >= '" . $request->debCode_frm . "' AND cylinder_cashbills.account_code <= '" . $request->debCode_to . "'";
                    } else {
                        $debtor = '';
                    }
                }
            }
        }



        $CashBill = DB::select(DB::raw("

                SELECT
                `cylinder_cashbills`.`docno`          AS `docno`,
                `cylinder_cashbills`.`date`           AS `date`,
                `cylinder_cashbills`.`lpono`          AS `lpono`,
                `cylinder_cashbills`.`account_code`   AS `account_code`,
                `cylinder_cashbills`.`account_code`   AS `debtor`,
                `cylinder_cashbills`.`name`           AS `name`,
                CONCAT(CONCAT(`cylinder_cashbills`.`docno`),', ',DATE_FORMAT(`cylinder_cashbills`.`date`,'%d-%m-%Y'),', ',`cylinder_cashbills`.`account_code`,', ',`cylinder_cashbills`.`name`) AS `details`,
                `cylinder_cashbilldts`.`subject`      AS `description`,
                `cylinder_cashbilldts`.`id`           AS `id`,
                `cylinder_cashbilldts`.`doc_no`       AS `doc_no`,
                `cylinder_cashbilldts`.`serial`    AS `serial`,
                `cylinder_cashbilldts`.`quantity`          AS `quantity`,
                `cylinder_cashbilldts`.`uom`          AS `uom`,
                `cylinder_cashbilldts`.`unit_price`       AS `unit_price`,
                `cylinder_cashbilldts`.`amount`       AS `amount`,
                `cylinder_cashbilldts`.`taxed_amount` AS `tax_amount`,
                `cylinder_cashbilldts`.`subject`      AS `subject`,
                `cylinder_cashbilldts`.`details`      AS `DETAIL`,
                cylinder_cashbills.amount AS totalamt
                FROM ((`cylinder_cashbills`
                JOIN `cylinder_cashbilldts`
                    ON ((`cylinder_cashbills`.`docno` = `cylinder_cashbilldts`.`doc_no`))))
                WHERE `cylinder_cashbilldts`.`deleted_at` IS NULL AND `cylinder_cashbills`.`deleted_at` IS NULL
                $date $docno $lpono $debtor
                order by docno


        "), []);

        return $CashBill;
    }


}
