<?php

namespace App\Http\Controllers;

use App\Model\Sling;
use App\Model\Slingtype;
use App\Model\Manufacturer;
use Auth;

use Illuminate\Http\Request;

class SlingController extends Controller
{
    public function index()
    {
        $data["slings"]= Sling::get();
        $data["page_title"] = "Sling Item Listing";
        $data["bclvl1"] = "Sling Item Listing";
        $data["bclvl1_url"] = route('slings.index');

        return view('cylindermaster.slings.index', $data);
    }

    public function create()
    {
        $data["slings"]= Sling::get();
        $data["slingtypes"]= Slingtype::get();
        $data["manufacturers"]= Manufacturer::get();
        $data["page_title"] = "Add Sling";
        $data["bclvl1"] = "Sling Item Listing";
        $data["bclvl1_url"] = route('slings.index');
        $data["bclvl2"] = "Add Sling";
        $data["bclvl2_url"] = route('slings.create');

        return view('cylindermaster.slings.create', $data);
    }

    public function store(Request $request)
    {
        Sling::create([
            'barcode'=>$request['barcode'],
            'serial'=>$request['serial'],
            'mfr'=>$request['mfr'],
            'owner'=>$request['owner'],
            'type'=>$request['type'],
            'loadlimit'=>$request['loadlimit'],
            'dimension'=>$request['dimension'],
            'legged'=>$request['legged'],
            'certno'=>$request['certno'],
            'testdate'=>$request['testdate']?date('Y-m-d', strtotime($request['testdate'])):null,
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('slings.index')->with('Success', 'Sling created successfully');
    }

    public function edit($id)
    {
        $data["slings"]= Sling::get();
        $data["sling"] = Sling::find($id);
        $data["slingtypes"]= Slingtype::get();
        $data["manufacturers"]= Manufacturer::get();
        $data["page_title"] = "Edit Sling";
        $data["bclvl1"] = "Sling Item Listing";
        $data["bclvl1_url"] = route('slings.index');
        $data["bclvl2"] = "Edit Sling";
        $data["bclvl2_url"] = route('slings.edit', ['id'=>$id]);

        return view('cylindermaster.slings.edit', $data);        
    }

    public function update(Request $request, $id)
    {
        $sling = Sling::find($id);
        $sling->update([
            'barcode'=>$request['barcode'],
            'serial'=>$request['serial'],
            'mfr'=>$request['mfr'],
            'owner'=>$request['owner'],
            'type'=>$request['type'],
            'loadlimit'=>$request['loadlimit'],
            'dimension'=>$request['dimension'],
            'legged'=>$request['legged'],
            'certno'=>$request['certno'],
            'testdate'=>$request['testdate']?date('Y-m-d', strtotime($request['testdate'])):null,
            'updated_by'=> Auth::user()->id
        ]);

        return redirect()->route('slings.index')->with('Success', 'Sling updated successfully');
    }

    public function destroy(sling $sling)
    {
        $sling->update([
            'deleted_by' => Auth::user()->id
        ]);
        $sling->delete();
        return redirect()->route('slings.index')->with('Success', 'Sling deleted successfully');
    }
    

    public function filter_records(Request $request)
    {

        $result = Sling::where(function ($query) use ($request) {

            if ($request['isBarcode'] == 'true') {
                $query->Where('barcode', '=', $request['item']);
            } else {
                $query->Where('serial', '=', $request['item']);
            }
            $query->whereNotNull('barcode');
        })
            ->get(['barcode', 'serial']);

        return $result;
    }
}
