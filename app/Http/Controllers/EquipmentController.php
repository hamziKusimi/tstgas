<?php

namespace App\Http\Controllers;

use App\Model\Equipment;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    public function index()
    {
        $data["equipment"] = Equipment::get();

        return view('stockmaster.equipments.index', $data);
    }

    public function create()
    {
        return view('stockmaster.equipments.create');
    }

    public function store(Request $request)
    {
        $request->validate([

        ]);
    }
}
