<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Salesreturn;
use App\Model\Salesreturndt;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Uom;
use App\Model\Salesman;
use App\Model\Location;
use App\Model\PrintedIndexView;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use App\Model\Area;
use PDF;

class SalesreturnController extends Controller
{
    public function index()
    {
        $data["salesreturns"] = Salesreturn::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Salesreturn::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Sales Return')->pluck('printed_by');
        $data["page_title"] = "Sales Return Received Listing";
        $data["bclvl1"] = "Sales Return Received Listing";
        $data["bclvl1_url"] = route('salesreturns.index');
        return view('dailypro/salesreturns.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["salesreturnsearch"] = Salesreturn::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref1', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref2', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Salesreturn::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["page_title"] = "Sales Returns Listing";
        $data["bclvl1"] = "Sales Returns Listing";
        $data["bclvl1_url"] = route('salesreturns.index');
        return view('dailypro.salesreturns.index', $data);
    }

    public function salesreturnDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'ref1',
                            6 => 'ref2',
                            7 => 'amount',
                        );

        $totalData = Salesreturn::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $salesreturns = Salesreturn::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $salesreturns =  Salesreturn::Where('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('ref1', 'LIKE',"%{$search}%")
                            ->orWhere('ref2', 'LIKE',"%{$search}%")
                            ->orWhere('date', 'LIKE',"%{$search}%")
                            ->orWhere('amount', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Salesreturn::Where('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->orWhere('ref1', 'LIKE',"%{$search}%")
                             ->orWhere('ref2', 'LIKE',"%{$search}%")
                             ->orWhere('date', 'LIKE',"%{$search}%")
                             ->orWhere('amount', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($salesreturns))
        {
            foreach ($salesreturns as $key => $salesreturn)
            {
                $delete =  route('salesreturns.destroy', $salesreturn->id);
                $edit =  route('salesreturns.edit', $salesreturn->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('SALES_RETURN_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$salesreturn->docno.'</a>';
                }else{
                    $nestedData['docno'] = $salesreturn->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($salesreturn->date));
                $nestedData['account_code'] = $salesreturn->account_code;
                $nestedData['name'] = $salesreturn->name;
                $nestedData['ref1'] = $salesreturn->ref1;
                $nestedData['ref2'] = $salesreturn->ref2;
                $nestedData['amount'] = $salesreturn->amount;
                if(Auth::user()->hasPermissionTo('SALES_RETURN_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function api_store(Request $request)
    {
        // return $request['search'];
        $data = Salesreturn::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref1', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref2', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Sales Return');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Sales Return";
        $data["bclvl1"] = "Sales Return Received Listing";
        $data["bclvl1_url"] = route('salesreturns.index');
        $data["bclvl2"] = "Add Sales Return";
        $data["bclvl2_url"] = route('salesreturns.create');
        // return response()->json($data);

        return view('dailypro/salesreturns.create', $data);
    }

    public function store(Request $request)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $salesreturnid = Salesreturn::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'ref1' => $request['ref1'],
            'ref2' => $request['ref2'],
            'salesman' => $request['salesman'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $invoiceDoc = DocumentSetup::findByName('Sales Return');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Salesreturndt::create([
                    'doc_no' => $salesreturnid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'ucost' => $request['unit_cost'][$i],
                    'uprice' => $request['unit_price'][$i],
                    'reason' => $request['reason'][$i],
                    'cbinv' => $request['cbinv'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => $request['amount_dt'][$i],
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal + $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }

        return redirect()->route('salesreturns.edit', ['id' => $salesreturnid])->with('Success', 'Sales Return added sucessfully');
    }

    public function show(Type $var = null)
    {
        return view('dailypro/salesreturns.invoices', $data);
    }

    public function edit($id)
    {
        $salesreturn = Salesreturn::find($id);
        $contents = Salesreturndt::where('doc_no', $salesreturn->docno)->get();
        $selectedDebitor = Debtor::findByAcode($salesreturn->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $salesreturn;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["salesreturn"] = $salesreturn;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Sales Return";
        $data["bclvl1"] = "Sales Return Received Listing";
        $data["bclvl1_url"] = route('salesreturns.index');
        $data["bclvl2"] = "Edit Sales Return";
        $data["bclvl2_url"] = route('salesreturns.edit', ['id' => $id]);

        // return response()->json( count($data["Salesreturndts"]));
        // return response()->json($salesreturndts);
        return view('dailypro/salesreturns.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // return response()->json($request);
        $salesreturn = Salesreturn::find($id);
        if (isset($salesreturn)) {
            $salesreturn->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'lpono' => $request['lpono'],
                'ref1' => $request['ref1'],
                'ref2' => $request['ref2'],
                'salesman' => $request['salesman'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $salesreturndt_id = Salesreturndt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;
                    if ($salesreturndt_id != null) {
                        $stkBalance = $stock->stkbal - $salesreturndt_id->totalqty;
                        $salesreturndt_id->update([
                            'doc_no' => $salesreturn->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'reason' => $request['reason'][$i],
                            'cbinv' => $request['cbinv'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => $request['amount_dt'][$i],
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal + $salesreturndt_id->totalqty;
                    } else {
                        $dt = Salesreturndt::create([
                            'doc_no' => $salesreturn->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'reason' => $request['reason'][$i],
                            'cbinv' => $request['cbinv'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => $request['amount_dt'][$i],
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal + $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
            // return response()->json($request['Salesreturndts_id']);

        }
        // dd($salesreturndt);
        return redirect()->route('salesreturns.edit', ['id' => $id])->with('Success', 'Sales Return updated sucessfully');
    }

    public function destroy($id)
    {

        $salesreturn = Salesreturn::find($id);
        $salesreturn->update([
            'deleted_by' => Auth::user()->id
        ]);
        $salesreturn->delete();
        // return redirect()->route('salesreturns.index')->with('Success', 'Sales Return deleted successfully');

        $salesreturndt = Salesreturndt::where('doc_no', $salesreturn->docno);
        $salesreturndt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $salesreturndt->delete();
        return redirect()->route('salesreturns.index')->with('Success', 'Sales Return deleted successfully');
    }


    public function destroyData($id)
    {
        $data = Salesreturndt::find($id);

        if (isset($data)) {
            $Salesreturn = Salesreturn::where('docno','=',$data->doc_no)->first();
            $Salesreturn->update([
                'amount' => $Salesreturn->amount - $data->amount,
                'taxed_amount' => $Salesreturn->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroySalesreturndt($id)
    {
        $salesreturndt = Salesreturndt::find($id);
        $salesreturndt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $salesreturndt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper($id)
    {
        $Resources = Salesreturn::where('id', $id)->get();
        $ResourcesJsonList = $this->getSalesreturnListingJSON($Resources);

        $formattedResource = Salesreturn::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'ref1 as REF_NO',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'SalesReturn';
        $jrxml = 'general-bill-two.jrxml';
        $reportTitle = 'Sales Return';
        Transaction::generateBillDebtor(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Sales Return')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Sales Return')->update([
                'index' => "Sales Return",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Sales Return",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $dates = $request->dates;
        $SalesReturn = $this->getSalesReturnsQuery($request);

        if ($SalesReturn->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->salesreturn_rcv == "listing") {
            foreach ($SalesReturn as $SReturn) {

                $amount = $amount + $SReturn->amount;
            }

            $pdf = PDF::loadView('dailypro/salesreturns.listing', [
                'SalesReturnJSON' => $SalesReturn,
                'date' =>  $dates,
                'totalamount' => $amount
            ])->setPaper('A4', 'landscape');
            return $pdf->inline();

        } else {

            $pdf = PDF::loadView('dailypro/salesreturns.summary', [
                'date' =>  $dates,
                'SalesReturnJSON' => $SalesReturn,
            ])->setPaper('A4');
            return $pdf->inline();
        }
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/SalesReturns/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/SalesReturns/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/SalesReturns/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/SalesReturns/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getSalesReturnsQuery(Request $request)
    {
        return Salesreturn::join('salesreturndts', 'salesreturns.docno', '=', 'salesreturndts.doc_no')
            ->join('stockcodes', 'salesreturndts.item_code', '=', 'stockcodes.code')
            ->selectRaw("
        `salesreturns`.`docno`          AS `docno`,
        `salesreturns`.`date`           AS `date`,
        `salesreturns`.`ref1`          AS `ref1`,
        `salesreturns`.`account_code`   AS `account_code`,
        `salesreturns`.`account_code`   AS `debtor`,
        `salesreturns`.`name`           AS `name`,
        CONCAT(CONCAT(`salesreturns`.`docno`),', ',DATE_FORMAT(`salesreturns`.`date`,'%d-%m-%Y'),', ',`salesreturns`.`account_code`,', ',`salesreturns`.`name`) AS `details`,
        `salesreturndts`.`subject`      AS `description`,
        `salesreturndts`.`id`           AS `id`,
        `salesreturndts`.`doc_no`       AS `doc_no`,
        `salesreturndts`.`item_code`    AS `item_code`,
        `salesreturndts`.`qty`          AS `quantity`,
        `salesreturndts`.`uom`          AS `uom`,
        `salesreturndts`.`uprice`       AS `uprice`,
        `salesreturndts`.`amount`       AS `amount`,
        `salesreturndts`.`taxed_amount` AS `tax_amount`,
        `salesreturndts`.`subject`      AS `subject`,
        `salesreturndts`.`details`      AS `DETAIL`,
        `stockcodes`.`loc_id`        AS `loc_id`,
        salesreturns.amount AS totalamt,
        (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location")
            ->where(function ($query) use ($request) {

                $query->whereNull('salesreturns.deleted_at');
                $query->whereNull('salesreturndts.deleted_at');

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->refNo_Chkbx_1 == "on") {
                    if ($request->refNo_to_1 == null) {
                        $query->where('ref1', "=", $request->refNo_frm_1);
                    } else {
                        $query->whereBetween('ref1', [$request->refNo_frm_1, $request->refNo_to_1]);
                    }
                }

                if ($request->refNo_Chkbx_2 == "on") {
                    if ($request->refNo_to_2 == null) {
                        $query->where('ref2', "=", $request->refNo_frm_2);
                    } else {
                        $query->whereBetween('ref2', [$request->refNo_frm_2, $request->refNo_to_2]);
                    }
                }

                if ($request->DebtorCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->DebtorCode_frm, $request->DebtorCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

    public function getSalesReturnListingJSON($SalesReturns)
    {
        $dataArray = array();
        foreach ($SalesReturns as $SalesReturn) {

            $SalesReturnTrans = Salesreturndt::select(
                'item_code',
                'subject',
                'qty',
                'uom',
                'details',
                'ucost',
                'uprice',
                'amount',
                'taxed_amount'
            )
                ->where("doc_no", "=", $SalesReturn->docno)
                ->get();

            $lastElement = count($SalesReturnTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($SalesReturnTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($SalesReturn->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $SalesReturn->docno . ", " . $date . ", " .
                    $SalesReturn->account_code . ", " . $SalesReturn->name;
                $objectJSON['description'] = $trans->subject;

                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['item_code'] = $trans->item_code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_cost'] = $trans->ucost;
                $objectJSON['unit_price'] = $trans->uprice;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $trans->amount;
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getSalesReturnSummaryJSON($SalesReturns)
    {
        $dataArray = array();
        $s_n = 1;
        foreach ($SalesReturns as $SalesReturn) {
            $SalesReturnTrans = Salesreturndt::select('amount', 'updated_at')
                ->where("doc_no", "=", $SalesReturn->docno)
                ->get();

            foreach ($SalesReturnTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $SalesReturn->docno;
                $objectJSON['date'] = $date;
                $objectJSON['ref1'] = $SalesReturn->ref1;
                $objectJSON['ref2'] = $SalesReturn->ref2;
                $objectJSON['debtor'] = $SalesReturn->account_code;
                $objectJSON['name'] = $SalesReturn->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getRouteByDocno(Request $request)
    {
        $Salesreturn = Salesreturn::where('docno', $request->docno)->first('id');

        if (!$Salesreturn) {

            $url = action('SalesreturnController@create');
        } else {

            $url = action('SalesreturnController@edit', ['id' => $Salesreturn->id]);
        }

        return $url;
    }
}
