<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Quotation;
use App\Model\Quotationdt;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Uom;
use App\Model\Location;
use App\Model\Salesman;
use App\Model\PrintedIndexView;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use App\Model\Area;
use PDF;

class QuotationController extends Controller
{
    public function index()
    {
        $data["quotations"] = Quotation::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Quotation::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Quotation')->pluck('printed_by');
        $data["page_title"] = "Quotation Listing";
        $data["bclvl1"] = "Quotation Listing";
        $data["bclvl1_url"] = route('quotations.index');
        return view('dailypro/quotations.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["quotationsearch"] = Quotation::select('*')
        ->where(function ($query) use ($request) {
            $query->orWhere('docno', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('account_code', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('name', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('attention', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('contact', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('cbinvdono', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('quot', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('taxed_amount', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('date', 'LIKE', '%'.$request['search'].'%');
        })
        ->orderBy('date')
        ->get();

        $data["docno_select"] = Quotation::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["page_title"] = "Quotation Listing";
        $data["bclvl1"] = "Quotation Listing";
        $data["bclvl1_url"] = route('quotations.index');
        return view('dailypro.quotations.index', $data);

    }

    public function quotationDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'attention',
                            6 => 'contact',
                            7 => 'cbinvdono',
                            8 => 'quot',
                            9 => 'taxed_amount',
                        );

        $totalData = Quotation::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $quotations = Quotation::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $quotations =  Quotation::Where('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('attention', 'LIKE',"%{$search}%")
                            ->orWhere('contact', 'LIKE',"%{$search}%")
                            ->orWhere('cbinvdono', 'LIKE',"%{$search}%")
                            ->orWhere('quot', 'LIKE',"%{$search}%")
                            ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                            ->orWhere('date', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Quotation::Where('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->orWhere('attention', 'LIKE',"%{$search}%")
                             ->orWhere('contact', 'LIKE',"%{$search}%")
                             ->orWhere('cbinvdono', 'LIKE',"%{$search}%")
                             ->orWhere('quot', 'LIKE',"%{$search}%")
                             ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                             ->orWhere('date', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($quotations))
        {
            foreach ($quotations as $key => $quotation)
            {
                $delete =  route('quotations.destroy', $quotation->id);
                $edit =  route('quotations.edit', $quotation->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('QUOTATION_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$quotation->docno.'</a>';
                }else{
                    $nestedData['docno'] = $quotation->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($quotation->date));
                $nestedData['account_code'] = $quotation->account_code;
                $nestedData['name'] = $quotation->name;
                $nestedData['attention'] = $quotation->attention;
                $nestedData['contact'] = $quotation->contact;
                $nestedData['cbinvdono'] = $quotation->cbinvdono;
                $nestedData['quot'] = $quotation->quot;
                $nestedData['taxed_amount'] = $quotation->taxed_amount;
                if(Auth::user()->hasPermissionTo('QUOTATION_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function api_store(Request $request)
    {
        // return $request['search'];
        $data = Quotation::select('id', 'docno', 'account_code', 'name', 'updated_at')
        ->where(function ($query) use ($request) {
            $query->orWhere('docno', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('account_code', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('name', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('attention', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('contact', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('cbinvdono', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('quot', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('date', 'LIKE', '%'.$request['search'].'%');
        })
        ->orderBy('date')
        ->get();

        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Quotation');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Quotation";
        $data["bclvl1"] = "Quotation Received Listing";
        $data["bclvl1_url"] = route('quotations.index');
        $data["bclvl2"] = "Add Quotation";
        $data["bclvl2_url"] = route('quotations.create');
        // return response()->json($data);

        return view('dailypro/quotations.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $quotationid = Quotation::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'attention' => $request['attention'],
            'contact' => $request['contact'],
            'cbinvdono' => $request['cbinvdono'],
            'quot' => $request['quotno'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);

        $quotationDoc = DocumentSetup::findByName('Quotation');
        $quotationDoc->update(['D_LAST_NO' => $quotationDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Quotationdt::create([
                    'doc_no' => $quotationid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'uprice' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                // $stock = Stockcode::where('code', $dt->item_code)->first();
                // $stkBalance = $stock->stkbal + $dt->totalqty;
                // $stock->update(['stkbal' => $stkBalance]);
            }
        }

        return redirect()->route('quotations.edit', ['id' => $quotationid])->with('Success', 'Quotation added sucessfully');
    }

    public function show(Type $var = null)
    {
        return view('dailypro/quotations.quotations', $data);
    }

    public function edit($id)
    {

        $quotation = Quotation::find($id);
        $contents = Quotationdt::where('doc_no', $quotation->docno)->get();
        $selectedDebitor = Debtor::findByAcode($quotation->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $quotation;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Quotation";
        $data["bclvl1"] = "Quotation Received Listing";
        $data["bclvl1_url"] = route('quotations.index');
        $data["bclvl2"] = "Edit Quotation";
        $data["bclvl2_url"] = route('quotations.edit', ['id' => $id]);

        // return response()->json( count($data["quotationdts"]));
        // return response()->json($quotationdts);
        return view('dailypro/quotations.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // return response()->json($request);
        $quotation = Quotation::find($id);
        if (isset($quotation)) {
            $quotation->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'attention' => $request['attention'],
                'contact' => $request['contact'],
                'cbinvdono' => $request['cbinvdono'],
                'quot' => $request['quotno'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $quotationdt_id = Quotationdt::find($request['item_id'][$i]);
                    if ($quotationdt_id != null) {
                        $quotationdt_id->update([
                            'doc_no' => $quotation->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        $dt = Quotationdt::create([
                            'doc_no' => $quotation->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        // // Update stockabalance
                        // $stock = Stockcode::where('code', $dt->item_code)->first();
                        // $stkBalance = $stock->stkbal + $dt->totalqty;
                        // $stock->update(['stkbal' => $stkBalance]);
                    }
                }
            }
            // return response()->json($request['Quotationdts_id']);

        }
        // dd($quotation);
        return redirect()->route('quotations.edit', ['id' => $id])->with('Success', 'Quotation updated sucessfully');
    }

    public function destroy($id)
    {

        $quotation = Quotation::find($id);
        $quotation->update([
            'deleted_by' => Auth::user()->id
        ]);
        $quotation->delete();
        // return redirect()->route('quotations.index')->with('Success', 'Quotation deleted successfully');

        $quotationdt = Quotationdt::where('doc_no', $quotation->docno);
        $quotationdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $quotationdt->delete();
        return redirect()->route('quotations.index')->with('Success', 'Quotation deleted successfully');
    }


    public function destroyData($id)
    {
        $data = Quotationdt::find($id);

        if (isset($data)) {
            $Quotation = Quotation::where('docno','=',$data->doc_no)->first();
            $Quotation->update([
                'amount' => $Quotation->amount - $data->amount,
                'taxed_amount' => $Quotation->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyQuotationdt($id)
    {
        $quotationdt = Quotationdt::find($id);
        $quotationdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $quotationdt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper($id, Request $request)
    {
        //update printed details
        $quotation = Quotation::find($id);
        $getprinted = Quotation::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($quotation)) {
            $quotation->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);

        }

        $Resources = Quotation::where('id', $id)->get();
        $ResourcesJsonList = $this->getQuotationListingJSON($Resources);

        $formattedResource = Quotation::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'cbinvdono as REF_NO',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'Quotation';
        $jrxml = 'general-bill-Quot.jrxml';
        $reportTitle = 'Quotation';
        Transaction::generateBillDebtor(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Quotation')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Quotation')->update([
                'index' => "Quotation",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Quotation",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $dates = $request->dates;
        $Quotation = $this->getQuotationQuery($request);

        if ($Quotation->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->Quotation_rcv == "listing") {
            foreach ($Quotation as $Quot) {

                $amount = $amount + $Quot->amount;
            }

            $pdf = PDF::loadView('dailypro/quotations.listing', [
                'QuotationJSON' => $Quotation,
                'date' =>  $dates,
                'totalamount' => $amount
            ])->setPaper('A4', 'landscape');
            return $pdf->inline();
        } else {
            $pdf = PDF::loadView('dailypro/quotations.summary', [
                'date' =>  $dates,
                'QuotationJSON' => $Quotation
            ])->setPaper('A4');
            return $pdf->inline();
        }

    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/Quotation/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/Quotation/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/Quotation/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/Quotation/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getQuotationQuery(Request $request)
    {
        return Quotation::join('quotationdts', 'quotations.docno', '=', 'quotationdts.doc_no')
        ->join('stockcodes', 'quotationdts.item_code', '=', 'stockcodes.code')
        ->selectRaw("
        `quotations`.`docno`          AS `docno`,
        `quotations`.`date`           AS `date`,
        `quotations`.`cbinvdono`          AS `cbinvdono`,
        `quotations`.`account_code`   AS `account_code`,
        `quotations`.`account_code`   AS `debtor`,
        `quotations`.`name`           AS `name`,
        CONCAT(CONCAT(`quotations`.`docno`),', ',DATE_FORMAT(`quotations`.`date`,'%d-%m-%Y'),', ',`quotations`.`account_code`,', ',`quotations`.`name`) AS `details`,
        `quotationdts`.`subject`      AS `description`,
        `quotationdts`.`id`           AS `id`,
        `quotationdts`.`doc_no`       AS `doc_no`,
        `quotationdts`.`item_code`    AS `item_code`,
        `quotationdts`.`qty`          AS `quantity`,
        `quotationdts`.`uom`          AS `uom`,
        `quotationdts`.`uprice`       AS `uprice`,
        `quotationdts`.`amount`       AS `amount`,
        `quotationdts`.`taxed_amount` AS `tax_amount`,
        `quotationdts`.`subject`      AS `subject`,
        `quotationdts`.`details`      AS `DETAIL`,
        `stockcodes`.`loc_id`        AS `loc_id`,
        quotations.amount AS totalamt,
        (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location")
            ->where(function ($query) use ($request) {

                $query->whereNull('quotations.deleted_at');
                $query->whereNull('quotationdts.deleted_at');

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->CB_INV_DO_Chkbx_1 == "on") {
                    if ($request->CB_INV_DO_to_1 == null) {
                        $query->where('cbinvdono', "=", $request->CB_INV_DO_frm_1);
                    } else {
                        $query->whereBetween('CB_INV_DOno', [$request->CB_INV_DO_frm_1, $request->CB_INV_DO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

    public function getQuotationListingJSON($Quotation)
    {

        $dataArray = array();
        foreach ($Quotation as $Quot) {

            $QuotTrans = Quotationdt::select(
                'item_code',
                'subject',
                'qty',
                'uom',
                'uprice',
                'amount',
                'details',
                'taxed_amount'
            )
                ->where("doc_no", "=", $Quot->docno)
                ->get();

            $lastElement = count($QuotTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($QuotTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($Quot->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $Quot->docno . ", " . $date . ", " .
                    $Quot->account_code . ", " . $Quot->name;
                $objectJSON['description'] = $trans->subject;
                $objectJSON['item_code'] = $trans->item_code;
                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_price'] = $trans->uprice;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $objectJSON['amount'];
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getQuotationSummaryJSON($Quotation)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Quotation')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Quotation')->update([
                'index' => "Quotation",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Quotation",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        $dataArray = array();
        $s_n = 1;
        foreach ($Quotation as $Quot) {
            $QuotTrans = Quotationdt::select('amount', 'updated_at')
                ->where("doc_no", "=", $Quot->docno)
                ->get();

            foreach ($QuotTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $Quot->docno;
                $objectJSON['date'] = $date;
                if ($Quot->cbinvdono != "") {
                    $objectJSON['cbinvdo_no'] = $Quot->cbinvdono;
                } else {
                    $objectJSON['cbinvdo_no'] = "   -";
                }
                $objectJSON['debtor'] = $Quot->account_code;
                $objectJSON['name'] = $Quot->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getRouteByDocno(Request $request)
    {
        $Quotation = Quotation::where('docno', $request->docno)->first('id');

        if (!$Quotation) {

            $url = action('QuotationController@create');
        } else {

            $url = action('QuotationController@edit', ['id' => $Quotation->id]);
        }

        return $url;
    }
}
