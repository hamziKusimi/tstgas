<?php

namespace App\Http\Controllers;

use App\Model\Location;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class LocationController extends Controller
{
    public function index()
    {
        $data["locations"] = Location::get();
        $data["page_title"] = "Location Item Listing";
        $data["bclvl1"] = "Location Item Listing";
        $data["bclvl1_url"] = route('locations.index');

        return view('stockmaster.locations.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Location";
        $data["bclvl1"] = "Location Item Listing";
        $data["bclvl1_url"] = route('locations.index');
        $data["bclvl2"] = "Add Location";
        $data["bclvl2_url"] = route('locations.create');

        return view('stockmaster.locations.create', $data);
    }

    public function store(Request $request)
    {
        $location = Location::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('locations.index')->with('Success', 'Location created successfully.');
    }

    public function edit($id)
    {
        $data["location"] = Location::find($id);
        $data["page_title"] = "Edit Location";
        $data["bclvl1"] = "Location Item Listing";
        $data["bclvl1_url"] = route('locations.index');
        $data["bclvl2"] = "Edit Location";
        $data["bclvl2_url"] = route('locations.edit', ['id' => $id]);

        return view('stockmaster.locations.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $category = Location::find($id);
        if (isset($category)) {
            $category->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('locations.index')->with('Success', 'Location updated successfully.');
    }

    public function destroy(location $location)
    {
        $location->update([
            'deleted_by' => Auth::user()->id
        ]);
        $location->delete();
        return redirect()->route('locations.index')->with('Success', 'Location deleted successfully.');
    }

    public function print()
    {
        $Locations = Location::all();

        $dataArray = array();
        $no = 1;
        foreach ($Locations as $Location) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Location->code;
            $objectJSON["description"] = $Location->descr;
            $objectJSON["status"] = $Location->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $LocationsJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintLocation" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $LocationsJSON);
        $JasperFileName = "ReportPrintLocation" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }
    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Location Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }
}
