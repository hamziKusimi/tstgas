<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Invoice;
use App\Model\InvoiceData;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\Location;
use App\Model\Deliveryorder;
use App\Model\Deliveryorderdt;
use App\Model\PrintedIndexView;
use App\Model\Salesman;
use App\Model\Area;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Uom;
use App\Model\CustomObject\Transaction;
use DB;
use DataTables;
use PDF;
use App\ExportsInvoiceReport\ExportInvoiceReport;
use Excel;

class InvoiceController extends Controller
{
    public function index()
    {
        // $data["invoices"] = Invoice::orderBy('docno', 'desc')->paginate(30);
        $data["docno_select"] = Invoice::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["salesman_select"] = Salesman::pluck('code', 'code')->toArray();
        $data["print"] = PrintedIndexView::where('index', 'Invoice Sales')->pluck('printed_by');
        $data["page_title"] = "Invoices Listing";
        $data["bclvl1"] = "Invoices Listing";
        $data["bclvl1_url"] = route('invoices.index');
        return view('dailypro.invoices.index', $data);
    }

    public function invoiceDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'terms',
                            6 => 'do_no',
                            8 => 'taxed_amount',
                            // 9 => 'more',
                        );

        $totalData = Invoice::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = Invoice::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  Invoice::where('id','LIKE',"%{$search}%")
                            ->orWhere('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Invoice::where('id','LIKE',"%{$search}%")
                             ->orWhere('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $key => $post)
            {
                $delete =  route('invoices.destroy', $post->id);
                $edit =  route('invoices.edit', $post->id);


                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('INVOICE_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$post->docno.'</a>';
                }else{
                    $nestedData['docno'] = $post->docno;
                }
                $nestedData['docno'] = '<a href="'.$edit.'">'.$post->docno.'</a>';
                $nestedData['date'] = date('d-m-Y',strtotime($post->date));
                $nestedData['account_code'] = $post->account_code;
                $nestedData['name'] = $post->name;
                $nestedData['terms'] = $post->credit_term;
                $nestedData['do_no'] = $post->dono;
                $nestedData['taxed_amount'] = $post->taxed_amount;
                if(Auth::user()->hasPermissionTo('INVOICE_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);

    }

    public function searchindex(Request $request)
    {
        $data["invoicesearch"] = Invoice::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('credit_term', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('do_no', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Invoice::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["salesman_select"] = Salesman::pluck('code', 'code')->toArray();
        $data["print"] = PrintedIndexView::where('index', 'Invoice Sales')->pluck('printed_by');
        $data["page_title"] = "Invoice Listing";
        $data["bclvl1"] = "Invoice Listing";
        $data["bclvl1_url"] = route('invoices.index');
        return view('dailypro.invoices.index', $data);
    }

    public function api_store(Request $request)
    {
        // return $request['search'];
        $data = Invoice::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('credit_term', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('do_no', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function docnoList()
    {
        $query = Invoice::query()
            ->selectRaw('docno, date, name');

        return DataTables::of($query)->make(true);
    }

    public function debtorList()
    {
        $query = Debtor::query()
            ->selectRaw('accountcode, name')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }

    public function salesmanList()
    {
        $query = Salesman::query()
            ->selectRaw('code, descr')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }

    public function dono_get($debtor)
    {
        $response = Deliveryorder::where('account_code', $debtor)->get();

        return response()->json($response);
    }

    public function getDOrderDt(Request $request)
    {
        $dorders = Deliveryorderdt::where('doc_no', $request['docno'])->orderBy('sequence_no')->get();

        return response()->json($dorders);
    }


    public function api_get()
    {
        $resources = Invoice::orderBy('date')->get();

        return response()->json($resources);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        // $masterCodes = AccountMastercode::get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        // $templates = TemplateMaster::all();
        $runningNumber = DocumentSetup::findByName('Invoice Sales');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        // $data['templates'] = $templates;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->get()->pluck('Detail', 'code');
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Invoices";
        $data["bclvl1"] = "Invoice Sales Listing";
        $data["bclvl1_url"] = route('invoices.index');
        $data["bclvl2"] = "Add Invoices";
        $data["bclvl2_url"] = route('invoices.create');
        // return response()->json($stockcode);
        return view('dailypro.invoices.create', $data);
    }

    public function store(Request $request)
    {
        $invNo = $request['doc_no'];
        DB::statement("
            DELETE FROM invoices
            WHERE docno='$invNo' AND deleted_at IS NOT NULL;
        ");


        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // create acc_invmt row
        $invoice = Invoice::updateOrCreate([
            'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
            'refinvdate' => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
            'refno' => !empty($request['ref_no']) ? $request['ref_no'] : '',
            'salesman' => $request['salesman'],
            'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
            'credit_limit' => !empty($request['credit_limit']) ? $request['credit_limit'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'currency' => !empty($request['currency']) ? $request['currency'] : '',
            'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
            'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
            'created_by' => Auth::user()->id
        ]);

        // adding to opening balance
        // $debtor = Debtor::findByAcode($invoice->accountcode);
        // $debtor->update([ 'yopen' => $debtor->D_YOPEN + $invoice->taxed_amount ]);

        // update the next value in billing settings
        $invoiceDoc = DocumentSetup::findByName('Invoice Sales');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        // populate data for acc_invdt, first row is a template, so we start with the second row
        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = InvoiceData::create([
                    'doc_no' => $invoice->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'reference_no' => $request['reference_no'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'quantity' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'unit_price' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'exchange_rate' => $invoice->exchange_rate,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal - $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }


        // session()->flash('alert-success', config('kusimi.form.flash.new').'Invoice');
        // return redirect()->route('invoices.edit', $invoice->id);

        return redirect()->route('invoices.edit', ['id' => $invoice])->with('Success', 'Invoice added successfully');
        // return redirect()->route('invoices.create');
    }

    public function show($id)
    {
        // $invoice = Invoice::find($id);
        // $contents = $invoice->data;
        // $selectedDebitor = self::getSelectedMastercode($invoice->account_code)[0];
        // $masterCodes = self::getMastercode();

        // $data = [];
        // $data['item'] = $invoice;
        // $data['contents'] = $contents;
        // $data['masterCodes'] = $masterCodes;
        // $data['selectedDebitor'] = $selectedDebitor;

        // return view ('dailypro.invoices.show', $data);
    }

    public function edit($id)
    {
        $invoice = Invoice::where('id', $id)->first();
        $contents = InvoiceData::where('doc_no', $invoice->docno)->get();

        // return response()->json($invoice);
        $selectedDebitor = Debtor::findByAcode($invoice->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $invoice;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->get()->pluck('Detail', 'code');
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Invoice Sales Listing";
        $data["bclvl1"] = "Invoice Sales Listing";
        $data["bclvl1_url"] = route('invoices.index');
        // $data["bclvl2"] = "Edit Invoices";
        // $data["bclvl2_url"] = route('invoices.edit');

        return view('dailypro.invoices.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // update acc_invmt row
        $invoice = Invoice::find($id);
        if (isset($invoice)) {
            // updating to opening balance
            // $debtor = Debtor::findByAcode($invoice->account_code);
            // $debtor->update([ 'D_YOPEN' => $debtor->D_YOPEN - $invoice->taxed_amount ]);

            // update invoice
            $invoice->update([
                'doc_no' => !empty($request['doc_no']) ? $request['doc_no'] : '',
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                'refinvdate' => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
                'refno' => !empty($request['ref_no']) ? $request['ref_no'] : '',
                'salesman' => $request['salesman'],
                'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
                'credit_limit' => !empty($request['credit_limit']) ? $request['credit_limit'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'updated_by' => Auth::user()->id
            ]);

            // after update, then re-add the new value
            // $debtor->update([ 'D_YOPEN' => $debtor->D_YOPEN + $invoice->taxed_amount ]);

            // populate data for acc_invdt, first row is a template, so we start with the second row
            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $dt = InvoiceData::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if (isset($dt)) {
                        $stkBalance = $stock->stkbal + $dt->totalqty;

                        $dt->update([
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'exchange_rate' => $invoice->exchange_rate,
                            'updated_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal - $dt->totalqty;
                    } else {
                        $dt = InvoiceData::create([
                            'doc_no' => $invoice->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'exchange_rate' => $invoice->exchange_rate,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal - $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
        }


        // session()->flash('alert-success', config('kusimi.form.flash.update').'Invoice');
        // return redirect()->route('invoices.edit', $invoice->id);
        return redirect()->route('invoices.edit', ['id' => $id])->with('Success', 'Invoice updated successfully');
    }

    public function destroy($id)
    {
        $invoice = Invoice::find($id);
        $invoice->update([
            'deleted_by' => Auth::user()->id
        ]);
        $invoice->delete();
        // session()->flash('alert-success', config('kusimi.form.flash.delete').'Invoice');
        // return redirect()->back();
        return redirect()->route('invoices.index')->with('Success', 'Invoice deleted successfully');
    }

    public function destroyData($id)
    {
        $data = InvoiceData::find($id);

        if (isset($data)) {
            $Invoice = Invoice::where('docno','=',$data->doc_no)->first();
            $Invoice->update([
                'amount' => $Invoice->amount - $data->amount,
                'taxed_amount' => $Invoice->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    /**
     * return Invoice array
     * */
    public function getInvoicesByAcode($acode)
    {
        $invoices = Invoice::where('account_code', $acode)->get();

        return response()->json($invoices);
    }

    /**
     * return array containing item_code only
     * */
    public function getInvoiceItems($invoice_no)
    {
        $invoice = Invoice::where('docno', $invoice_no)->first();

        $foundItemCodes = $invoice->data->pluck('item_code')->unique();
        $items = [];
        foreach ($foundItemCodes as $itemCode) {
            $item = ItemMaster::where('I_CODE', $itemCode)->first();
            if (isset($item)) array_push($items, $item);
        }
        return response()->json(collect($items));
    }

    public function jasper($id)
    {
        //update printed details
        $invoice = Invoice::find($id);
        $getprinted = Invoice::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($invoice)) {
            $invoice->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $Resources = Invoice::where('id', $id)->get();
        $ResourcesJsonList = $this->getInvoiceListingJSON($Resources);

        $formattedResource = Invoice::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'do_no as REF_NO',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'refno as REF_NO',
            'do_no as DO_NO',
            'credit_term as TERMS',
            'm_duedate as INV_Due_Date',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'Invoice';
        $jrxml = 'general-bill-two.jrxml';
        $reportTitle = 'Invoice';
        Transaction::generateBillDebtor(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        //update printed details

        $getprinted = PrintedIndexView::where('index', 'Invoice Sales')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Invoice Sales')->update([
                'index' => "Invoice Sales",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Invoice Sales",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;
        $totalqty = 0;

        if($request->cashbill_rcv =='reprint'){

            return $this->reprint($request);
        }

        $Invoice = $this->getInvoiceQuery($request);

        $dates = $request->dates;
        $data['date'] = $dates;
        $systemsetup = SystemSetup::pluck('qty_decimal')->first();
        $data['systemsetup'] = $systemsetup;

        if (empty($Invoice)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        if ($request->cashbill_rcv == "listing") {
            // dd('here');
            // if($request->stockitem == "stockitem"){
            //     foreach ($Invoice as $iv) {

            //         $amount = $amount + $iv->totalamount;
            //     }

            //     $data['totalqty'] = $cb->totalqty;
            // }else{

                foreach ($Invoice as $iv) {

                    $amount = $amount + $iv->amount;
                }
                $data['totalqty'] = "";

            // }

            $reportType = "InvoiceListing";

            if($request->submitBtn == "pdf"){
                $pdf = PDF::loadView('dailypro/invoices.listing', [
                    'invoiceJSON' => $Invoice,
                    'finaltotalamount' => $amount,
                    'totalqty' => "",
                    'systemsetup' => $systemsetup,
                    'date' => $dates
                ])->setPaper('A4');
                return $pdf->inline();
            }else{
                $data['reportType'] = $reportType;
                $data['invoiceJSON'] = $Invoice;
                $data['finaltotalamount'] = $amount;
                $data['date'] = $dates;
                $data['systemsetup'] = $systemsetup;

                return Excel::download(new ExportInvoiceReport($data, "listing"), 'InvoiceReportListing.xlsx');
            }

        } else {
            foreach ($Invoice as $iv) {

                $amount = $amount + $iv->totalamount;
            }

            if($request->submitBtn == "pdf"){
                $pdf = PDF::loadView('dailypro/invoices.summary', [
                    'invoiceJSON' => $Invoice,
                    'finaltotalamount' => $amount,
                    'systemsetup' => $systemsetup,
                    'date' => $dates
                ])->setPaper('A4');
                return $pdf->inline();
            }else{
                $data['invoiceJSON'] = $Invoice;
                $data['finaltotalamount'] = $amount;
                $data['date'] = $dates;
                $data['systemsetup'] = $systemsetup;

                return Excel::download(new ExportInvoiceReport($data, "summary"), 'InvoiceReportSummary.xlsx');
            }
        }

        // check if JSON empty \\
        // $InvJSON = json_decode($InvoiceJSON);
        // if (empty($InvJSON->data)) {
        //     return "<script>alert('No Record Found');window.close();</script>";
        // }
        // // check if JSON empty \\

        // $JsonFileName = "Invoice" . date("Ymdhisa") . Auth::user()->id;
        // file_put_contents(base_path('resources/reporting/DailyProcess/Invoices/' . $JsonFileName . '.json'), $InvoiceJSON);
        // $JasperFileName = "Invoice" . date("Ymdhisa") . Auth::user()->id;

        // $file = $this->printReport($JsonFileName, $JasperFileName, $reportType);

        // header('Content-Description: application/pdf');
        // header('Content-Type: application/pdf');
        // header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        // readfile($file);
        // unlink($file);
        // flush();
        // exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/Invoices/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/Invoices/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/Invoices/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/Invoices/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getInvoiceQuery(Request $request)
    {

        $systemsetupstock = SystemSetup::pluck('stock_item')->first();

        if($request->cashbill_rcv == "summary"){
            $type = "GROUP BY docno";
            if($systemsetupstock == '1'){

                // $stock = "AND stockcodes.type = 'Stock Item'";
                $stock = "";
                $totalqty = "(SELECT sum(quantity) FROM invoice_data INNER JOIN stockcodes on ((invoice_data.item_code = stockcodes.code) and (stockcodes.type = 'Stock Item'))
                WHERE invoice_data.item_code = item_code and invoice_data.doc_no = docno and invoice_data.deleted_at IS NULL) AS totalqty,";

            }else{
                $stock = "";
                $totalqty = "(SELECT sum(quantity) FROM invoice_data WHERE invoice_data.item_code = item_code and invoice_data.doc_no = docno and invoice_data.deleted_at IS NULL) AS totalqty,";

            }
        }else{
            $type = "GROUP BY docno, subject, item_code";
            $stock = "";
            $totalqty = "";
        }

        // if($request->cashbill_rcv == "summary"){

        //     $type = "GROUP BY docno";
        //     if($request->stockitem == "stockitem"){

        //         $stock = "";
        //         $totalqty = "(SELECT sum(quantity) FROM invoice_data INNER JOIN stockcodes on ((invoice_data.item_code = stockcodes.code) and (stockcodes.type = 'Stock Item'))
        //         WHERE invoice_data.item_code = item_code and invoice_data.doc_no = docno) AS totalqty,";
        //     }else{
        //         $stock = "";
        //         $totalqty = "(SELECT sum(quantity) FROM invoice_data WHERE invoice_data.item_code = item_code and invoice_data.doc_no = docno) AS totalqty,";


        //     }

        // }else{

        //     $type = "";
        //     $stock = "";
        //     $totalqty = "";

        // }



        if ($request->dates_Chkbx == "on") {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

            if ($dates_from == $dates_to) {
                $date = "AND invoices.date = '" . $dates_from . "'";
            } else {
                $date = "AND invoices.date >= '" . $dates_from . "' AND invoices.date <= '" . $dates_to . "'";
            }

            if ($request->docno_Chkbx == "on") {
                $docno = "AND (invoices.docno >= '" . $request->docno_frm . "' AND invoices.docno <= '" . $request->docno_to . "')";

                if ($request->DO_Chkbx_1 == "on") {
                    if ($request->DO_to_1 == null) {
                        $do_no = "AND (invoices.do_no = '" . $request->DO_frm_1 . "')";
                    } else {
                        $do_no = "AND (invoices.do_no >= '" . $request->DO_frm_1 . "' AND invoices.do_no <= '" . $request->DO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "')";
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $do_no = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "')";
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                }
            } else {
                $docno = '';

                if ($request->DO_Chkbx_1 == "on") {
                    if ($request->DO_to_1 == null) {
                        $do_no = "AND (invoices.do_no = '" . $request->DO_frm_1 . "')";
                    } else {
                        $do_no = "AND (invoices.do_no >= '" . $request->DO_frm_1 . "' AND invoices.do_no <= '" . $request->DO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "')";
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $do_no = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "')";
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                }
            }
        } else {
            $date = '';
            if ($request->docno_Chkbx == "on") {
                $docno = "AND invoices.docno >= '" . $request->docno_frm . "' AND invoices.docno <= '" . $request->docno_to . "'";

                if ($request->DO_Chkbx_1 == "on") {
                    if ($request->DO_to_1 == null) {
                        $do_no = "AND (invoices.do_no = '" . $request->DO_frm_1 . "')";
                    } else {
                        $do_no = "AND (invoices.do_no >= '" . $request->DO_frm_1 . "' AND invoices.do_no <= '" . $request->DO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "')";
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $do_no = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "'";
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                }
            } else {
                $docno = '';

                if ($request->DO_Chkbx_1 == "on") {
                    if ($request->DO_to_1 == null) {
                        $do_no = "AND invoices.do_no = '" . $request->DO_frm_1 . "'";
                    } else {
                        $do_no = "AND invoices.do_no >= '" . $request->DO_frm_1 . "' AND invoices.do_no <= '" . $request->DO_to_1 . "'";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "')";
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';
                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    }
                } else {
                    $do_no = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND invoices.account_code >= '" . $request->debCode_frm . "' AND invoices.account_code <= '" . $request->debCode_to . "'";

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";
                        } else {
                            $salesman = '';
                        }
                    } else {
                        $debtor = '';

                        if ($request->salesman_Chkbx == "on") {
                            $salesman = "AND (invoices.salesman >= '" . $request->salesman_frm . "' AND invoices.salesman <= '" . $request->salesman_to . "')";

                        } else {
                            $salesman = '';
                        }
                    }
                }
            }
        }



        $Invoice = DB::select(DB::raw("
                    SELECT
                    `invoices`.`docno`          AS `docno`,
                    `invoices`.`date`           AS `date`,
                    `invoices`.`do_no`          AS `do_no`,
                    `invoices`.`account_code`   AS `account_code`,
                    `invoices`.`account_code`   AS `debtor`,
                    `invoices`.`name`           AS `name`,
                    `invoices`.`amount`           AS `totalamount`,
                    CONCAT(CONCAT(`invoices`.`docno`),', ',DATE_FORMAT(`invoices`.`date`, '%d-%m-%Y'),', ',`invoices`.`account_code`,', ',`invoices`.`name`) AS `details`,
                    `invoice_data`.`subject`      AS `description`,
                    `invoice_data`.`id`           AS `id`,
                    `invoice_data`.`doc_no`       AS `doc_no`,
                    `invoice_data`.`item_code`    AS `item_code`,
                    `invoice_data`.`quantity`     AS `quantity`,
                    `invoice_data`.`uom`          AS `uom`,
                    `invoice_data`.`unit_price`   AS `unit_price`,
                    `invoice_data`.`amount`       AS `amount`,
                    `invoice_data`.`taxed_amount` AS `tax_amount`,
                    `invoice_data`.`subject`      AS `subject`,
                    `invoice_data`.`details`      AS `DETAIL`,
                    `stockcodes`.`loc_id`        AS `loc_id`,
                    invoices.amount AS totalamt,
                    $totalqty
                    (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location
                FROM ((`invoices`
                    JOIN `invoice_data`
                        ON ((`invoices`.`docno` = `invoice_data`.`doc_no`)))
                    JOIN `stockcodes`
                    ON ((`invoice_data`.`item_code` = `stockcodes`.`code`)))
                    WHERE `invoice_data`.`deleted_at` IS NULL AND `invoices`.`deleted_at` IS NULL
                    $stock $date $docno $do_no $debtor $salesman
                    $type
                    order by docno

            "), []);

        return $Invoice;
    }

    public function getInvoiceListingJSON($Invoices)
    {
        $dataArray = array();
        foreach ($Invoices as $invoice) {

            $invoiceTrans = InvoiceData::select('item_code', 'subject', 'quantity', 'uom', 'unit_price', 'details', 'amount', 'taxed_amount')
                ->where("doc_no", "=", $invoice->docno)
                ->get();

            $lastElement = count($invoiceTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($invoiceTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($invoice->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $invoice->docno . ", " . $date . ", " .
                    $invoice->account_code . ", " . $invoice->name;
                $objectJSON['description'] = $trans->subject;

                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['item_code'] = $trans->item_code;
                $objectJSON['quantity'] = $trans->quantity;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_price'] = $trans->unit_price;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $objectJSON['amount'];
                $t_qty =  $t_qty + $trans->quantity;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getInvoiceSummaryJSON($Invoices)
    {
        $dataArray = array();
        $s_n = 1;
        foreach ($Invoices as $invoice) {
            // $invoiceTrans = InvoiceData::select('amount', 'updated_at')
            //     ->where("doc_no", "=", $invoice->docno)
            //     ->get();

            $objectJSON = [];
            $date = date("d/m/Y", strtotime($invoice->date));

            $objectJSON['s_n'] = $s_n;
            $objectJSON['doc_no'] = $invoice->docno;
            $objectJSON['date'] = $date;
            if ($invoice->do_no != "") {
                $objectJSON['do_no'] = $invoice->do_no;
            } else {
                $objectJSON['do_no'] = "   -";
            }
            $objectJSON['debtor'] = $invoice->account_code;
            $objectJSON['name'] = $invoice->name;
            $objectJSON['t_amount'] = $invoice->taxed_amount;

            $s_n = $s_n + 1;
            $dataArray[] = collect($objectJSON);

            // foreach ($invoiceTrans as $trans) {
            //     $objectJSON = [];
            //     $date = date("d/m/Y", strtotime($trans->updated_at));

            //     $objectJSON['s_n'] = $s_n;
            //     $objectJSON['doc_no'] = $invoice->docno;
            //     $objectJSON['date'] = $date;
            //     if ($invoice->do_no != "") {
            //         $objectJSON['do_no'] = $invoice->do_no;
            //     } else {
            //         $objectJSON['do_no'] = "   -";
            //     }
            //     $objectJSON['debtor'] = $invoice->account_code;
            //     $objectJSON['name'] = $invoice->name;
            //     $objectJSON['t_amount'] = $trans->amount;

            //     $s_n = $s_n + 1;
            //     $dataArray[] = collect($objectJSON);
            // }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getRouteByDocno(Request $request)
    {
        $Invoice = Invoice::where('docno', $request->docno)->first('id');

        if (!$Invoice) {

            // Session::flash('message', 'This is a message!');
            $url = action('InvoiceController@create');
        } else {

            $url = action('InvoiceController@edit', ['id' => $Invoice->id]);
        }

        return $url;
    }

// NOTE: reprint invoice----------
    public function reprint($request){

        $cashbills = Invoice::join('invoice_data', 'invoice_data.doc_no', '=', 'invoices.docno')
        ->selectRaw("
                invoices.docno as DOC_NO,
                invoices.date as DOC_DATE,
                invoices.account_code as ACC_CODE,
                invoices.name as ACC_HOLDER,
                invoices.do_no as DO_NO,
                invoices.refno as REF_NO,
                invoices.addr1 as ADDR1,
                invoices.addr2 as ADDR2,
                invoices.addr3 as ADDR3,
                invoices.addr4 as ADDR4,
                invoices.tel_no as TEL,
                invoices.fax_no as FAX,
                invoices.credit_term as TERMS,
                invoices.credit_limit as LIMITS,
                invoices.m_duedate as INV_Due_Date,
                invoices.amount as STOTAL,
                invoices.taxed_amount as GRANDTOTAL,
                invoices.header as HEADER,
                invoices.footer as FOOTER,
                invoices.summary as SUMMARY,
                invoice_data.item_code,
                invoice_data.subject,
                invoice_data.quantity as qty,
                invoice_data.uom,
                invoice_data.unit_price as uprice,
                invoice_data.amount as amounts,
                invoice_data.details,
                invoice_data.taxed_amount as taxed_amounts
        ")
        ->whereRaw("
        invoice_data.deleted_at IS NULL AND invoices.deleted_at IS NULL")
        ->where(function ($query) use ( $request) {

            if($request->dates_Chkbx =='on'){
                $dates = explode("-", $request->dates);
                $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }
            }

            if($request->docno_Chkbx =='on'){
                if ($request->docno_frm == $request->docno_to) {
                    $query->where('docno', '=',$request->docno_frm);
                } else {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }
            }

            if($request->DO_Chkbx_1 =='on'){
                if ($request->DO_frm_1 == $request->DO_to_1) {
                    $query->where('invoices.do_no', '=',$request->DO_frm_1);
                } else {
                    $query->whereBetween('invoices.do_no', [$request->DO_frm_1, $request->DO_to_1]);
                }
            }

            if($request->debCode_Chkbx == 'on'){
                if ($request->debCode_frm == $request->debCode_to) {
                    $query->where('invoices.account_code', '=',$request->debCode_frm);
                } else {
                    $query->whereBetween('invoices.account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            }

            if($request->salesman_Chkbx == 'on'){
                if ($request->salesman_frm == $request->salesman_to) {
                    $query->where('salesman', '=',$request->salesman_frm);
                } else {
                    $query->whereBetween('salesman', [$request->salesman_frm, $request->salesman_to]);
                }
            }

            if($request->LPO_Chkbx_1 == 'on'){
                if ($request->LPO_frm_1 == $request->LPO_to_1) {
                    $query->where('salesman', '=',$request->LPO_frm_1);
                } else {
                    $query->whereBetween('salesman', [$request->LPO_frm_1, $request->LPO_to_1]);
                }
            }

        })->orderBy('docno')
        ->get();

        $source = 'Invoice';
        $jrxml = 'general-bill-two-reprint.jrxml';

        Transaction::generateBillReprint(
            $cashbills,
            $source,
            $jrxml
        );
    }

}
