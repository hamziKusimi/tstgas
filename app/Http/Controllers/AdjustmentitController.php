<?php

namespace App\Http\Controllers;

use App\Model\Adjustmentit;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class AdjustmentitController extends Controller
{
    public function index()
    {
        $data["adjustmentits"] = Adjustmentit::get();
        $data["page_title"] = "Adjustment In Type Item Listing";
        $data["bclvl1"] = "Adjustment In Type Item Listing";
        $data["bclvl1_url"] = route('adjustmentits.index');

        return view('stockmaster.adjustmentits.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Adjustment In Type";
        $data["bclvl1"] = "Adjustment In Type Item Listing";
        $data["bclvl1_url"] = route('adjustmentits.index');
        $data["bclvl2"] = "Add Adjustment In Type";
        $data["bclvl2_url"] = route('adjustmentits.create');

        return view('stockmaster.adjustmentits.create', $data);
    }

    public function store(Request $request)
    {
        $adjustmentit = Adjustmentit::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('adjustmentits.index')->with('Success', 'Adjustment In Type created successfully');
    }

    public function edit($id)
    {
        $data["adjustmentit"] = Adjustmentit::find($id);
        $data["page_title"] = "Edit Adjustment In Type";
        $data["bclvl1"] = "Adjustment In Type Item Listing";
        $data["bclvl1_url"] = route('adjustmentits.index');
        $data["bclvl2"] = "Edit Adjustment In Type";
        $data["bclvl2_url"] = route('adjustmentits.edit', ['id'=>$id]);

        return view('stockmaster.adjustmentits.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $adjustmentit = Adjustmentit::find($id);
        if (isset($adjustmentit)) {
            $adjustmentit->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }

        return redirect()->route('adjustmentits.index')->with('Success', 'Adjustment In Type updated successfully');
    }

    public function destroy(adjustmentit $adjustmentit)
    {
        $adjustmentit->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmentit->delete();
        return redirect()->route('adjustmentits.index')->with('Success', 'Adjustment In Type deleted successfully');
    }

    public function print()
    {
        $Adjustmentits = Adjustmentit::all();

        if ($Adjustmentits->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($Adjustmentits as $Adjustmentit) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Adjustmentit->code;
            $objectJSON["description"] = $Adjustmentit->descr;
            $objectJSON["status"] = $Adjustmentit->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $AdjustmentitsJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintAdjustmentit" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $AdjustmentitsJSON);
        $JasperFileName = "ReportPrintAdjustmentit" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $company_no = config('config.company.show_company_no') == 'true' ? config('config.company.company_no') : '';
        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => $company_no,
                "title" => "Adjustment In Type Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }
}
