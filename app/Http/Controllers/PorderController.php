<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Porder;
use App\Model\Porderdt;
use App\Model\Creditor;
use App\Model\Debtor;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Uom;
use App\Model\Location;
use App\Model\PrintedIndexView;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Salesman;
use Auth;
use DateTime;
use PHPJasper\PHPJasper;
use App\Model\CustomObject\Transaction;
use Illuminate\Http\Request;
use DB;
use DataTables;
use App\Model\Area;
use PDF;

class PorderController extends Controller
{
    public function index()
    {
        $data["porders"] = Porder::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Porder::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Purchase Order')->pluck('printed_by');
        $data["page_title"] = "Purchase Orders Listing";
        $data["bclvl1"] = "Purchase Orders Listing";
        $data["bclvl1_url"] = route('porders.index');
        return view('dailypro.porders.index', $data);
    }

    public function porderDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'suppdo',
                            6 => 'suppinv',
                            7 => 'ref',
                            8 => 'ptype',
                            9 => 'taxed_amount',
                        );

        $totalData = Porder::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $porders = Porder::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $porders =  Porder::where('id','LIKE',"%{$search}%")
                            ->orWhere('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('suppdo', 'LIKE',"%{$search}%")
                            ->orWhere('suppinv', 'LIKE',"%{$search}%")
                            ->orWhere('ref', 'LIKE',"%{$search}%")
                            ->orWhere('ptype', 'LIKE',"%{$search}%")
                            ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Porder::where('id','LIKE',"%{$search}%")
                             ->orWhere('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->orWhere('suppdo', 'LIKE',"%{$search}%")
                             ->orWhere('suppinv', 'LIKE',"%{$search}%")
                             ->orWhere('ref', 'LIKE',"%{$search}%")
                             ->orWhere('ptype', 'LIKE',"%{$search}%")
                             ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($porders))
        {
            foreach ($porders as $key => $porder)
            {
                $delete =  route('porders.destroy', $porder->id);
                $edit =  route('porders.edit', $porder->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('PURCHASE_OD_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$porder->docno.'</a>';
                }else{
                    $nestedData['docno'] = $porder->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($porder->date));
                $nestedData['account_code'] = $porder->account_code;
                $nestedData['name'] = $porder->name;
                $nestedData['suppdo'] = $porder->suppdo;
                $nestedData['suppinv'] = $porder->suppinv;
                $nestedData['ref'] = $porder->ref;
                $nestedData['ptype'] = $porder->ptype;
                $nestedData['taxed_amount'] = $porder->taxed_amount;

                if(Auth::user()->hasPermissionTo('PURCHASE_OD_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }

                $data[] = $nestedData;
            }
        }

        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function searchindex(Request $request)
    {
        $data["pordersearch"] = Porder::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppdo', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppinv', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ptype', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Porder::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Purchase Order')->pluck('printed_by');
        $data["page_title"] = "Purchase Orders Listing";
        $data["bclvl1"] = "Purchase Orders Listing";
        $data["bclvl1_url"] = route('porders.index');
        return view('dailypro.porders.index', $data);
    }

    public function api_store(Request $request)
    {
        $data = Porder::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppdo', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppinv', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ptype', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function docnoList()
    {
        $query = Porder::query()
            ->selectRaw('docno, date, name');

        return DataTables::of($query)->make(true);
    }

    public function debtorList()
    {
        $query = Creditor::query()
            ->selectRaw('accountcode, name')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }

    public function editRoute($id)
    {
        $resources = Stockcode::where('id', $id)->pluck('code');

        return response()->json($resources);
    }

    public function editAllRoute($id)
    {
        $responses = Stockcode::where('id', $id)->get();

        return response()->json($responses);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isCreditor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Purchase Order');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountcode');
        $systemsetup = SystemSetup::first();
        $uom = Uom::get();
        $uom1 = Stockcode::pluck('uom_id1')->toArray();
        $uom2 = Stockcode::pluck('uom_id2')->toArray();
        $uom3 = Stockcode::pluck('uom_id3')->toArray();
        $uoms = array_unique(array_merge($uom1, $uom2, $uom3));

        // return response()->json($uoms);

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data['systemsetup'] = $systemsetup;
        $data["uom"] = $uom;
        $data['uoms'] = $uoms;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Purchase Orders";
        $data["bclvl1"] = "Purchase Orders Listing";
        $data["bclvl1_url"] = route('porders.index');
        $data["bclvl2"] = "Add Purchase Orders";
        $data["bclvl2_url"] = route('porders.create');
        // return response()->json($data);

        return view('dailypro.porders.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $Porderid = Porder::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'suppdo' => $request['suppdo'],
            'suppinv' => $request['suppinv'],
            'suppinvdate' => Carbon::createFromFormat('d/m/Y', $request['suppinvdate']),
            'ref' => $request['ref'],
            'ptype' => $request['ptype'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'currency' => $request['currency'],
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $PorderDoc = DocumentSetup::findByName('Purchase Order');
        $PorderDoc->update(['D_LAST_NO' => $PorderDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Porderdt::create([
                    'doc_no' => $Porderid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'cucost' => $request['cunit_cost'][$i],
                    'mkup' => $request['markup'][$i],
                    'ucost' => $request['unit_cost'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal + $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }
        return redirect()->route('porders.edit', ['id' => $Porderid])->with('Success', 'Purchase orders added sucessfully');
    }

    public function show($id)
    { }

    public function edit($id)
    {
        $porder = Porder::find($id);
        $contents = Porderdt::where('doc_no', $porder->docno)->get();

        $selectedCreditor = Creditor::findByAcode($porder->account_code);
        $masterCodes = AccountMastercode::isCreditor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $porder;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedCreditor'] = $selectedCreditor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Purchase Orders";
        $data["bclvl1"] = "Purchase Orders Listing";
        $data["bclvl1_url"] = route('porders.index');
        $data["bclvl2"] = "Edit Purchase Orders";
        $data["bclvl2_url"] = route('porders.edit', ['id' => $id]);

        return view('dailypro.porders.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $porder = Porder::find($id);
        if (isset($porder)) {
            $porder->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'suppdo' => $request['suppdo'],
                'suppinv' => $request['suppinv'],
                'suppinvdate' => Carbon::createFromFormat('d/m/Y', $request['suppinvdate']),
                'ref' => $request['ref'],
                'ptype' => $request['ptype'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'currency' => $request['currency'],
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {
                    $porderdt_id = Porderdt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if ($porderdt_id != null) {
                        $stkBalance = $stock->stkbal - $porderdt_id->totalqty;
                        $porderdt_id->update([
                            'doc_no' => $porder->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'cucost' => $request['cunit_cost'][$i],
                            'mkup' => $request['markup'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'discount' => $request['discount'][$i],
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id

                        ]);

                        $stkBalance = $stkBalance + $porderdt_id->totalqty;
                    } else {
                        $dt = Porderdt::create([
                            'doc_no' => $porder->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'cucost' => $request['cunit_cost'][$i],
                            'mkup' => $request['markup'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id

                        ]);

                        $stkBalance = $stock->stkbal + $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
        }
        return redirect()->route('porders.edit', ['id' => $id])->with('Success', 'Purchase orders updated sucessfully');
    }

    public function destroy($id)
    {
        $porder = Porder::find($id);
        $porder->update([
            'deleted_by' => Auth::user()->id
        ]);
        $porder->delete();
        $docno = $porder->docno;

        $porderdt = Porderdt::where('doc_no', $docno);
        $porderdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $porderdt->delete();
        return redirect()->route('porders.index')->with('Success', 'Purchase orders Received deleted successfully');
    }

    public function destroyData($id)
    {
        $data = Porderdt::find($id);

        if (isset($data)) {
            $Porder = Porder::where('docno','=',$data->doc_no)->first();
            $Porder->update([
                'amount' => $Porder->amount - $data->amount,
                'taxed_amount' => $Porder->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function jasper($id)
    {
        //update printed details
        $porder = Porder::find($id);
        $getprinted = Porder::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($porder)) {
            $porder->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);

        }

        $originalResource = Porder::find($id);
        $Resources = Porder::where('id', $id)->get();
        $ResourcesJsonList = $this->getPOListingJSON($Resources);

        $formattedResource = Porder::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'PurchasedOrder';
        $jrxml = 'general-bill-one.jrxml';
        $reportTitle = 'Purchased Order';
        $jasperData = Transaction::generateBill(
            $formattedResource,
            $originalResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    // public function print(Request $request)
    // {
    //     $PurchasedOrder = $this->getPOQuery($request);

    //     if ($PurchasedOrder->isEmpty()) {
    //         return "<script>alert('No Record Found');window.close();</script>";
    //     }
    //     if ($request->porders_rcv == "listing") {
    //         $POJSON = $this->getPOListingJSON($PurchasedOrder);
    //         $reportType = "PurchasedOrderListing";
    //     } else {
    //         $POJSON = $this->getPOSummaryJSON($PurchasedOrder);
    //         $reportType = "PurchasedOrderSummary";
    //     }

    //     // check if JSON empty \\
    //     $gdJSON = json_decode($POJSON);
    //     if (empty($gdJSON->data)) {
    //         return "<script>alert('No Record Found');window.close();</script>";
    //     }
    //     // check if JSON empty \\

    //     $JsonFileName = "PurchasedOrder" . date("Ymdhisa") . Auth::user()->id;
    //     file_put_contents(base_path('resources/reporting/DailyProcess/PurchasedOrder/' . $JsonFileName . '.json'), $POJSON);
    //     $JasperFileName = "PurchasedOrder" . date("Ymdhisa") . Auth::user()->id;

    //     $file = $this->printReport($JsonFileName, $JasperFileName, $reportType);

    //     header('Content-Description: application/pdf');
    //     header('Content-Type: application/pdf');
    //     header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
    //     readfile($file);
    //     unlink($file);
    //     flush();
    //     exit;
    // }

    public function print(Request $request)
    {
        //update printed details
         $getprinted = PrintedIndexView::where('index', 'Purchase Order')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Purchase Order')->update([
                'index' => "Purchase Order",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Purchase Order",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $systemsetup = SystemSetup::pluck('qty_decimal')->first();

        $dates = $request->dates;
        $PurchaseOrder = $this->getPOQuery($request);

        if (empty($PurchaseOrder)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->porders_rcv == "listing") {
            foreach($PurchaseOrder as $PO){

                $amount = $amount + $PO->amount;
            }

            $pdf = PDF::loadView('dailypro/porders.listing', [
                'PurchaseOrderJSON' => $PurchaseOrder,
                'finaltotalamount' =>  $amount,
                'date' =>  $dates,
                'systemsetup' =>  $systemsetup
            ])->setPaper('A4', 'landscape');
            return $pdf->inline();
        } else {

            foreach($PurchaseOrder as $PO){

                $amount = $amount + $PO->totalamount;
            }

            $pdf = PDF::loadView('dailypro/porders.summary', [
                'PurchaseOrderJSON' => $PurchaseOrder,
                'finaltotalamount' =>  $amount,
                'date' =>  $dates,
                'systemsetup' =>  $systemsetup
            ])->setPaper('A4');
            return $pdf->inline();
        }
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/PurchasedOrder/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/PurchasedOrder/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/PurchasedOrder/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/PurchasedOrder/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getPOQuery(Request $request)
    {
        $systemsetupstock = SystemSetup::pluck('stock_item')->first();

        if($request->porders_rcv == "summary"){
            $type = "GROUP BY docno";
            if($systemsetupstock == '1'){

                // $stock = "AND stockcodes.type = 'Stock Item'";
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM porderdts INNER JOIN stockcodes on ((porderdts.item_code = stockcodes.code) and (stockcodes.type = 'Stock Item'))
                WHERE porderdts.item_code = item_code and porderdts.doc_no = docno and porderdts.deleted_at IS NULL) AS totalqty,";
            }else{
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM porderdts WHERE porderdts.item_code = item_code and porderdts.doc_no = docno and porderdts.deleted_at IS NULL) AS totalqty,";

            }
        }else{
            $type = "GROUP BY docno, item_code";
            $stock = "";
            $totalqty = "";
        }

        if ($request->dates_Chkbx == "on") {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');


            if ($dates_from == $dates_to) {
                $date = "AND porders.date = '" . $dates_from . "'";
            } else {
                $date = "AND porders.date >= '" . $dates_from . "' AND porders.date <= '" . $dates_to . "'";
            }

            // if ($dates_from == $dates_to) {
            //     $query->where('porders.date', '=', $dates_from);
            // } else {
            //     $query->whereBetween('porders.date', [$dates_from, $dates_to]);
            // }
        }else{
            $date = "";
        }

        if ($request->docno_Chkbx == "on") {

            $docno = "AND (porders.docno >= '" . $request->docno_frm . "' AND porders.docno <= '" . $request->docno_to . "')";

            // $query->whereBetween('porders.docno', [$request->docno_frm, $request->docno_to]);
        }else{
            $docno = "";
        }

        if ($request->suppDo_Chkbx == "on") {

            if ($request->suppDo_to == null) {
                $suppdo = "AND (porders.suppdo = '" . $request->suppDo_frm . "')";
            } else {
                $suppdo = "AND (porders.suppdo >= '" . $request->suppDo_frm . "' AND porders.suppdo <= '" . $request->suppDo_to . "')";
            }

            // if ($request->suppDo_to == null) {
            //     $query->where('porders.suppdo', "=", $request->suppDo_frm);
            // } else {
            //     $query->whereBetween('porders.suppdo', [$request->suppDo_frm, $request->suppDo_to]);
            // }
        }else{
            $suppdo = "";
        }

        if ($request->suppInv_Chkbx == "on") {

            if ($request->suppInv_to == null) {
                $suppinv = "AND (porders.suppinv = '" . $request->suppInv_frm . "')";
            } else {
                $suppinv = "AND (porders.suppinv >= '" . $request->suppInv_frm . "' AND porders.suppinv <= '" . $request->suppInv_to . "')";
            }

            // if ($request->suppInv_to == null) {
            //     $query->where('porders.suppinv', "=", $request->suppInv_frm);
            // } else {
            //     $query->whereBetween('porders.suppinv', [$request->suppInv_frm, $request->suppInv_to]);
            // }
        }else{
            $suppinv = "";
        }

        if ($request->refNo_Chkbx == "on") {

            if ($request->refNo_to == null) {
                $ref = "AND (porders.ref = '" . $request->refNo_frm . "')";
            } else {
                $ref = "AND (porders.ref >= '" . $request->refNo_frm . "' AND porders.ref <= '" . $request->refNo_to . "')";
            }

            // if ($request->refNo_to == null) {
            //     $query->where('porders.ref', "=", $request->refNo_frm);
            // } else {
            //     $query->whereBetween('porders.ref', [$request->refNo_frm, $request->refNo_to]);
            // }
        }else{
            $ref = "";
        }

        if ($request->credCode_Chkbx == "on") {

            $creditor = "AND porders.account_code >= '" . $request->credCode_frm . "' AND porders.account_code <= '" . $request->credCode_to . "'";

            // $query->whereBetween('porders.account_code', [$request->credCode_frm, $request->credCode_to]);
        }else{
            $creditor = "";
        }

        $Porder = DB::select(DB::raw("
                SELECT
                `porders`.`docno`          AS `docno`,
                `porders`.`date`           AS `date`,
                `porders`.`suppdo`          AS `suppdo`,
                `porders`.`suppinv`          AS `suppinv`,
                `porders`.`ref`          AS `ref`,
                `porders`.`account_code`   AS `account_code`,
                `porders`.`account_code`   AS `debtor`,
                `porders`.`name`           AS `name`,
                `porders`.`amount`           AS `totalamount`,
                CONCAT(CONCAT(`porders`.`docno`),', ',DATE_FORMAT(`porders`.`date`,'%d-%m-%Y'),', ',`porders`.`account_code`,', ',`porders`.`name`) AS `details`,
                `porderdts`.`subject`      AS `description`,
                `porderdts`.`id`           AS `id`,
                `porderdts`.`doc_no`       AS `doc_no`,
                `porderdts`.`item_code`    AS `item_code`,
                `porderdts`.`qty`          AS `quantity`,
                `porderdts`.`uom`          AS `uom`,
                `porderdts`.`ucost`       AS `ucost`,
                `porderdts`.`amount`       AS `amount`,
                `porderdts`.`taxed_amount` AS `tax_amount`,
                `porderdts`.`subject`      AS `subject`,
                `porderdts`.`details`      AS `DETAIL`,
                `stockcodes`.`loc_id`        AS `loc_id`,
                porders.amount AS totalamt,
                $totalqty
                (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location
            FROM ((`porders`
                JOIN `porderdts`
                    ON ((`porders`.`docno` = `porderdts`.`doc_no`)))
                JOIN `stockcodes`
                ON ((`porderdts`.`item_code` = `stockcodes`.`code`)))
                WHERE `porderdts`.`deleted_at` IS NULL AND `porders`.`deleted_at` IS NULL
                $stock $date $docno $suppdo $suppinv $ref $creditor
                $type
                order by docno

        "), []);

        return $Porder;

    }

    public function getPOListingJSON($PurchasedOrder)
    {
        //update printed details
        // $getprinted = PrintedIndexView::where('index', 'Purchase Order')->pluck('printed');
        // if(!$getprinted->isEmpty()){
        //     $print = $getprinted[0];
        //     PrintedIndexView::where('index', 'Purchase Order')->update([
        //         'index' => "Purchase Order",
        //         'printed' => $print + 1,
        //         'printed_at' => Carbon::now(),
        //         'printed_by' => Auth::user()->name
        //         ]);
        // }else{
        //     PrintedIndexView::create([
        //     'index' => "Purchase Order",
        //     'printed' => 1,
        //     'printed_at' => Carbon::now(),
        //     'printed_by' => Auth::user()->name
        //     ]);

        // }


        $dataArray = array();
        foreach ($PurchasedOrder as $PO) {

            $POTrans = Porderdt::select(
                'item_code',
                'subject',
                'qty',
                'taxed_amount',
                'uom',
                'ucost',
                'amount',
                'details'
            )
                ->where("doc_no", "=", $PO->docno)
                ->get();

            $lastElement = count($POTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($POTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($PO->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $PO->docno . ", " . $date . ", " .
                    $PO->account_code . ", " . $PO->name;
                $objectJSON['description'] = $trans->subject;

                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['item_code'] = $trans->item_code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_cost'] = $trans->ucost;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $trans->amount;
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getPOSummaryJSON($PurchasedOrder)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Purchase Order')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Purchase Order')->update([
                'index' => "Purchase Order",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Purchase Order",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        $dataArray = array();
        $s_n = 1;
        foreach ($PurchasedOrder as $PO) {
            $POTrans = Porderdt::select('amount', 'updated_at')
                ->where("doc_no", "=", $PO->docno)
                ->get();

            foreach ($POTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $PO->docno;
                $objectJSON['date'] = $date;
                $objectJSON['suppdo'] = $PO->suppdo;
                $objectJSON['suppinv'] = $PO->suppinv;
                $objectJSON['creditor'] = $PO->account_code;
                $objectJSON['name'] = $PO->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getRouteByDocno(Request $request)
    {
        $Porder = Porder::where('docno', $request->docno)->first('id');

        if (!$Porder) {

            // Session::flash('message', 'This is a message!');
            $url = action('PorderController@create');
        } else {

            $url = action('PorderController@edit', ['id' => $Porder->id]);
        }

        return $url;
    }
}
