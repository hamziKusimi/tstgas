<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SystemSetup;
use Auth;

class SystemSetupController extends Controller
{
    public function index()
    {
        $data["systemSetups"] = SystemSetup::get();
        $systemsetups =  $data["systemSetups"];
        $data["page_title"] = "System Setup";
        $data["bclvl1"] = "System Setup Item Listing";
        $data["bclvl1_url"] = route('systemsetups.index');

        return view('settings.systemsetups.index', $data);
    }

    public function create()
    {
        $data["systemSetup"] = SystemSetup::first();
        $data["page_title"] = "System Setup";
        $data["bclvl1"] = "System Setup";
        $data["bclvl1_url"] = route('systemsetups.create');
        // return response()->json($data);
        return view('settings.systemsetups.create', $data);
    }

    public function store(Request $request)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['address']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';


        if($request['DR_Cash_Purchase_box'] == "on"){$DR_Cash_Purchase_cbox = "true";}
        else{$DR_Cash_Purchase_cbox = "false";}

        if($request['DR_Credit_Purchase_box'] == "on"){$DR_Credit_Purchase_cbox = "true";}
        else{$DR_Credit_Purchase_cbox = "false";}

        if($request['CR_Purchase_Return_box'] == "on"){$CR_Purchase_Return_cbox = "true";}
        else{$CR_Purchase_Return_cbox = "false";}

        if($request['DR_Cash_Sales_box'] == "on"){$DR_Cash_Sales_cbox = "true";}
        else{$DR_Cash_Sales_cbox = "false";}

        if($request['CR_Cash_Sales_box'] == "on"){$CR_Cash_Sales_cbox = "true";}
        else{$CR_Cash_Sales_cbox = "false";}

        if($request['DR_Cash_Sales_Return_box'] == "on"){$DR_Cash_Sales_Return_cbox = "true";}
        else{$DR_Cash_Sales_Return_cbox = "false";}

        if($request['CR_Invoice_Sales_box'] == "on"){$CR_Invoice_Sales_cbox = "true";}
        else{$CR_Invoice_Sales_cbox = "false";}

        if($request['DR_Credit_Sales_Return_box'] == "on"){$DR_Credit_Sales_Return_cbox = "true";}
        else{$DR_Credit_Sales_Return_cbox = "false";}

        SystemSetup::create([
            'code' => $request['code'],
            'name' => $request['name'],
            'company_no' => $request['company_no'],
            'address1' => $request['addr1'],
            'address2' => $request['addr2'],
            'address3' => $request['addr3'],
            'address4' => $request['addr4'],
            'tel' => $request['tel'],
            'fax' => $request['fax'],
            'email' => $request['email'],
            'name1' => $request['name1'],
            'name2' => $request['name2'],
            'licenseno' => $request['licenseno'],
            'salestaxno' => $request['salestaxno'],
            'servtaxno' => $request['servtaxno'],
            'use_tax' => $request['use_tax'] == 0 ? 0 : 1,
            'qty_decimal' => $request['qty_decimal'] == 0 ? 0 : 1,
            'qty_decimal_place' => $request['qty_decimal_place'],
            'rate_decimal' => $request['rate_decimal'] == 0 ? 0 : 1,
            'rate_decimal_place' => $request['rate_decimal_place'],
            'stock_item' => $request['stock_item'] == 0 ? 0 : 1,
            'uom1' => $request['uom1'] == 0 ? 0 : 1,
            'uom2' => $request['uom2'] == 0 ? 0 : 1,
            'uom3' => $request['uom3'] == 0 ? 0 : 1,
            'updoc_gasrack' => $request['updoc_gasrack'] == 0 ? 0 : 1,
            'free_cy' => $request['free_cy'],
            'custom1' => $request['custom1'],
            'custom2' => $request['custom2'],
            'custom3' => $request['custom3'],
            'custom4' => $request['custom4'],
            'custom5' => $request['custom5'],
            'custom1_type' => $request['custom1_type'],
            'custom2_type' => $request['custom2_type'],
            'custom3_type' => $request['custom3_type'],
            'custom4_type' => $request['custom4_type'],
            'custom5_type' => $request['custom5_type'],
            'price1' => $request['price1'],
            'price2' => $request['price2'],
            'price3' => $request['price3'],
            'price4' => $request['price4'],
            'price5' => $request['price5'],
            'price6' => $request['price6'],
            'bank_detail' => $request['bankDetail'],
            'bank_account_no' => $request['BankAccNo'],
            'warning_min' => $request['warning_min'] == 0 ? 0 : 1,
            'req_pass' => $request['req_pass'] == 0 ? 0 : 1,
            'password_min' => $request['password_min'],
            'db_server' => $request['db_server'],
            'db_name' => $request['db_name'],
            'dr_cashpurchase_acc' => $request['DR_Cash_Purchase'],
            'dr_creditpurchase_acc' => $request['DR_Credit_Purchase'],
            'cr_purchasereturn_acc' => $request['CR_Purchase_Return'],
            'dr_cashsales_acc' => $request['DR_Cash_Sales'],
            'cr_cashsales_acc' => $request['CR_Cash_Sales'],
            'dr_cashsales_return_acc' => $request['DR_Cash_Sales_Return'],
            'cr_invoicesales_acc' => $request['CR_Invoice_Sales'],
            'dr_creditsales_return_acc' => $request['DR_Credit_Sales_Return'],

            'set_category1' => $DR_Cash_Purchase_cbox,
            'set_category2' => $DR_Credit_Purchase_cbox,
            'set_category3' => $CR_Purchase_Return_cbox,
            'set_category4' => $DR_Cash_Sales_cbox,
            'set_category5' => $CR_Cash_Sales_cbox,
            'set_category6' => $DR_Cash_Sales_Return_cbox,
            'set_category7' => $CR_Invoice_Sales_cbox,
            'set_category8' => $DR_Credit_Sales_Return_cbox,

            'direct_posting' =>  $request['direct_posting'] == 'on'? '1':'0',

            'created_by' => Auth::user()->id
        ]);

        return redirect()->route('systemsetups.create')->with('Success', 'System Setup updated successfully');
    }

    public function edit()
    {
        $data["systemSetup"] = SystemSetup::first();
        $data["page_title"] = "Edit System Setup";
        $data["bclvl1"] = "System Setup Item Listing";
        $data["bclvl1_url"] = route('systemsetups.index');
        $data["bclvl2"] = "Edit System Setup";
        $data["bclvl2_url"] = route('systemsetups.edit', ['id' => $id]);

        return view('settings.systemsetups.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['address']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        if($request['DR_Cash_Purchase_box'] == "on"){$DR_Cash_Purchase_cbox = "true";}
        else{$DR_Cash_Purchase_cbox = "false";}

        if($request['DR_Credit_Purchase_box'] == "on"){$DR_Credit_Purchase_cbox = "true";}
        else{$DR_Credit_Purchase_cbox = "false";}

        if($request['CR_Purchase_Return_box'] == "on"){$CR_Purchase_Return_cbox = "true";}
        else{$CR_Purchase_Return_cbox = "false";}

        if($request['DR_Cash_Sales_box'] == "on"){$DR_Cash_Sales_cbox = "true";}
        else{$DR_Cash_Sales_cbox = "false";}

        if($request['CR_Cash_Sales_box'] == "on"){$CR_Cash_Sales_cbox = "true";}
        else{$CR_Cash_Sales_cbox = "false";}

        if($request['DR_Cash_Sales_Return_box'] == "on"){$DR_Cash_Sales_Return_cbox = "true";}
        else{$DR_Cash_Sales_Return_cbox = "false";}

        if($request['CR_Invoice_Sales_box'] == "on"){$CR_Invoice_Sales_cbox = "true";}
        else{$CR_Invoice_Sales_cbox = "false";}

        if($request['DR_Credit_Sales_Return_box'] == "on"){$DR_Credit_Sales_Return_cbox = "true";}
        else{$DR_Credit_Sales_Return_cbox = "false";}

        $systemsetup = SystemSetup::find($id);
        $systemsetup->update([
            'code' => $request['code'],
            'name' => $request['name'],
            'company_no' => $request['company_no'],
            'address1' => $request['addr1'],
            'address2' => $request['addr2'],
            'address3' => $request['addr3'],
            'address4' => $request['addr4'],
            'tel' => $request['tel'],
            'fax' => $request['fax'],
            'email' => $request['email'],
            'name1' => $request['name1'],
            'name2' => $request['name2'],
            'licenseno' => $request['licenseno'],
            'salestaxno' => $request['salestaxno'],
            'servtaxno' => $request['servtaxno'],
            'use_tax' => $request['use_tax'] == 0 ? 0 : 1,
            'qty_decimal' => $request['qty_decimal'] == 0 ? 0 : 1,
            'qty_decimal_place' => $request['qty_decimal_place'],
            'rate_decimal' => $request['rate_decimal'] == 0 ? 0 : 1,
            'rate_decimal_place' => $request['rate_decimal_place'],
            'stock_item' => $request['stock_item'] == 0 ? 0 : 1,
            'uom1' => $request['uom1'] == 0 ? 0 : 1,
            'uom2' => $request['uom2'] == 0 ? 0 : 1,
            'uom3' => $request['uom3'] == 0 ? 0 : 1,
            'updoc_gasrack' => $request['updoc_gasrack'] == 0 ? 0 : 1,
            'free_cy' => $request['free_cy'],
            'custom1' => $request['custom1'],
            'custom2' => $request['custom2'],
            'custom3' => $request['custom3'],
            'custom4' => $request['custom4'],
            'custom5' => $request['custom5'],
            'custom1_type' => $request['custom1_type'],
            'custom2_type' => $request['custom2_type'],
            'custom3_type' => $request['custom3_type'],
            'custom4_type' => $request['custom4_type'],
            'custom5_type' => $request['custom5_type'],
            'price1' => $request['price1'],
            'price2' => $request['price2'],
            'price3' => $request['price3'],
            'price4' => $request['price4'],
            'price5' => $request['price5'],
            'price6' => $request['price6'],
            'bank_detail' => $request['bankDetail'],
            'bank_account_no' => $request['BankAccNo'],
            'warning_min' => $request['warning_min'] == 0 ? 0 : 1,
            'req_pass' => $request['req_pass'] == 0 ? 0 : 1,
            'password_min' => $request['password_min'],
            'db_server' => $request['db_server'],
            'db_name' => $request['db_name'],
            'dr_cashpurchase_acc' => $request['DR_Cash_Purchase'],
            'dr_creditpurchase_acc' => $request['DR_Credit_Purchase'],
            'cr_purchasereturn_acc' => $request['CR_Purchase_Return'],
            'dr_cashsales_acc' => $request['DR_Cash_Sales'],
            'cr_cashsales_acc' => $request['CR_Cash_Sales'],
            'dr_cashsales_return_acc' => $request['DR_Cash_Sales_Return'],
            'cr_invoicesales_acc' => $request['CR_Invoice_Sales'],
            'dr_creditsales_return_acc' => $request['DR_Credit_Sales_Return'],

            'set_category1' => $DR_Cash_Purchase_cbox,
            'set_category2' => $DR_Credit_Purchase_cbox,
            'set_category3' => $CR_Purchase_Return_cbox,
            'set_category4' => $DR_Cash_Sales_cbox,
            'set_category5' => $CR_Cash_Sales_cbox,
            'set_category6' => $DR_Cash_Sales_Return_cbox,
            'set_category7' => $CR_Invoice_Sales_cbox,
            'set_category8' => $DR_Credit_Sales_Return_cbox,

            'direct_posting' =>  $request['direct_posting'] == 'on'? '1':'0',

            'updated' => Auth::user()->id
        ]);
        return redirect()->route('systemsetups.create')->with('Success', 'System Setup updated successfully');
    }

    public function destroy(systemsetup $systemsetup)
    {
        $systemsetup->delete();
        return redirect()->route('systemsetups.index')->with('Success', 'System Setup deleted successfully.');
    }
}
