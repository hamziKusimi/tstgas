<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class CustomerController extends Controller
{
    public function index()
    {
        $data["customers"] = Customer::get();
        $data["page_title"] = "Customer Item Listings";
        $data["bclvl1"] = "Customer Item Listings";
        $data["bclvl1_url"] = route('customertypes.index');

        return view('stockmaster.customertypes.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Customer";
        $data["bclvl1"] = "Customer Item Listings";
        $data["bclvl1_url"] = route('customertypes.index');
        $data["bclvl2"] = "Add Customer";
        $data["bclvl2_url"] = route('customertypes.create');

        return view('stockmaster.customertypes.create', $data);
    }

    public function store(Request $request)
    {
        $customer = Customer::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('customertypes.index')->with('Success','Customer created successfully.');
    }

    public function edit($id)
    {
        $data["customer"] = Customer::find($id);
        $data["page_title"] = "Edit Customer";
        $data["bclvl1"] = "Customer Item Listings";
        $data["bclvl1_url"] = route('customertypes.index');
        $data["bclvl2"] = "Edit Customer";
        $data["bclvl2_url"] = route('customertypes.edit', ['id'=>$id]);

        return view('stockmaster.customertypes.edit', $data);
    }

    public function update(request $request, $id)
    {
        $customer = Customer::find($id);
        if (isset($customer)) {
            $customer->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('customertypes.index')->with('Success', 'Customer updated successfully.');
    }

    public function destroy(customer $customer)
    {
        $customer->update([
            'deleted_by' => Auth::user()->id
        ]);
        $customer->delete();
        return redirect()->route('customertypes.index')->with('Success', 'Customer deleted successfully.');
    }

    public function print()
    {
        $Customers = Customer::all();

        if ($Customers->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($Customers as $Customer) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Customer->code;
            $objectJSON["description"] = $Customer->descr;
            $objectJSON["status"] = $Customer->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $CustomersJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintCustomer" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $CustomersJSON);
        $JasperFileName = "ReportPrintCustomer" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Customer Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }
    
}
