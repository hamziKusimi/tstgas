<?php

namespace App\Http\Controllers;

use App\Model\Cylinderproduct;
use Illuminate\Http\Request;
use Auth;

class CylinderproductController extends Controller
{
    public function index()
    {
        $data["cylinderproducts"] = Cylinderproduct::get();
        $data["page_title"] = "Product Item Listing";
        $data["bclvl1"] = "Product Item Listing";
        $data["bclvl1_url"] = route('cylinderproducts.index');

        return view('cylindermaster.cylinderproducts.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Product";
        $data["bclvl1"] = "Product Item Listing";
        $data["bclvl1_url"] = route('cylinderproducts.index');
        $data["bclvl2"] = "Add Product";
        $data["bclvl2_url"] = route('cylinderproducts.create');

        return view('cylindermaster.cylinderproducts.create', $data);
    }

    public function store(Request $request)
    {
        $cylinderproduct = Cylinderproduct::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('cylinderproducts.index')->with('Success', 'Products created successfully');
    }

    public function edit($id)
    {
        $data["cylinderproduct"] = Cylinderproduct::find($id);
        $data["page_title"] = "Edit Product";
        $data["bclvl1"] = "Product Item Listing";
        $data["bclvl1_url"] = route('cylinderproducts.index');
        $data["bclvl2"] = "Edit Product";
        $data["bclvl2_url"] = route('cylinderproducts.edit', ['id'=>$id]);

        return view('cylindermaster.cylinderproducts.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cylinderproduct = Cylinderproduct::find($id);
        if (isset($cylinderproduct)) {
            $cylinderproduct->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('cylinderproducts.index')->with('Success', 'Products updated successfully');
    }

    public function destroy(cylinderproduct $cylinderproduct)
    {
        $cylinderproduct->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylinderproduct->delete();
        return redirect()->route('cylinderproducts.index')->with('Success', 'Products deleted successfully');
    }
}
