<?php

namespace App\Http\Controllers;

use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Cylindervalvetype;
use App\Model\Cylindercontainertypes;
use App\Model\Cylinderhandwheeltype;
use App\Model\Manufacturer;
use App\Model\Ownership;
use App\Model\CylinderActivity;
use App\Model\View\CylinderMovement;
use App\Model\Debtor;
use App\Model\View\CylinderHolding;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;

class CylinderInvalidController extends Controller
{
    public function print(Request $request)
    {
        $CylInvalid = $this->getCylInvalidQuery($request);

        if (count($CylInvalid)== 0) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $cylJSON = $this->getCylInvalidJSON($CylInvalid);
        $JsonFileName = "InvalidCylinderReturn" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/InvalidCylinderReturn/' . $JsonFileName . '.json'), $cylJSON);
        $JasperFileName = "InvalidCylinderReturn" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/InvalidCylinderReturn/InvalidCylinderReturn.jrxml';
        $output = base_path() . '/resources/reporting/InvalidCylinderReturn/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/InvalidCylinderReturn/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/InvalidCylinderReturn/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getCylInvalidQuery($request)
    {
        if($request->DateCRR_Chkbx == "on"){
        $dates = explode("-", $request->DateCRR_Chkbx);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

            $condition_1 =" AND (crr_date BETWEEN '$dates_from' AND '$dates_to') ";
        }else{
            $condition_1 = " ";
        }

        if($request->DateDO_Chkbx == "on"){
            $dates = explode("-", $request->DateDO_Chkbx);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

            $condition_2 =" AND (rt_dndate BETWEEN '$dates_from' AND '$dates_to') ";
        }else{
            $condition_2 = " ";
        }

        if($request->Debtor_Chkbx == "on"){

            $condition_3 =" AND (debtor BETWEEN '$request->Debtor_frm' AND '$request->Debtor_TO') ";
        }else{
            $condition_3 = " ";
        }

        if($request->serial_Chkbx == "on"){

            $condition_4 =" AND (`serial` BETWEEN '$request->serial_frm' AND '$request->serial_to') ";
        }else{
            $condition_4 = " ";
        }

        switch($request->type){
            case "invalidCrrDebtor":
                $condition_5 = " AND (invalid = 'true' AND invalid_debtor = 'true') ";
            break;

            case "invalidCrrCyl":
                $condition_5 = " AND (invalid = 'true'  AND invalid_cylinder = 'true') ";
            break;

            case "noMatchDo":
                $condition_5 = " AND (invalid = 'true'  AND invalid_debtor IS NULL AND invalid_cylinder IS NULL) ";
            break;

            default:
                $condition_5 = " AND (invalid = 'true' ) ";
            break;
        }

        switch($request->orderby){
            case "cylNo":
                $orderBy = " ORDER BY `serial`";
            break;

            case "docno":
                $orderBy = " ORDER BY `dn_no`";
            break;

            case "dateCRR":
                $orderBy = " ORDER BY crr_date";
            break;

            case "dateDO":
                $orderBy = " ORDER BY rt_dndate";
            break;
        }

        return DB::select(DB::raw("
            SELECT *
            FROM view_crr_invalid
            WHERE invalid IS NOT NULL
            $condition_1
            $condition_2
            $condition_3
            $condition_4
            $condition_5
            $orderBy

        "), []);
    }

    public function getCylInvalidJSON($CylInvalid)
    {
        $dataArray = [];
        $s_n = 1;


        foreach ($CylInvalid as $cyl) {

                $objectJSON =   [];

                $objectJSON['s_n'] = $s_n;
                $objectJSON['cylinder_sn'] = $cyl->serial;
                $objectJSON['doc_no'] = $cyl->rn_no;
                $objectJSON['date'] = date('d/m/Y', strtotime($cyl->crr_date));
                $objectJSON['debtor'] = $cyl->rn_debtor . " " . $cyl->rn_name;

                if($cyl->invalid_debtor == "true"){
                    $objectJSON['dn_no'] =  $cyl->rt_dnno;
                    $objectJSON['dn_date'] = date('d/m/Y', strtotime($cyl->rt_dndate));
                    $objectJSON['del_to'] = $cyl->dn_debtor . " " . $cyl->dn_name;
                }else{
                    $objectJSON['dn_no'] =  "-";
                    $objectJSON['dn_date'] = "-";
                    $objectJSON['del_to'] = "-";
                }


                $s_n =  $s_n + 1;
                $dataArray[] = collect($objectJSON);

        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
