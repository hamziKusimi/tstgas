<?php

namespace App\Http\Controllers;

use App\Model\Adjustmentot;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class AdjustmentotController extends Controller
{
    public function index()
    {
        $data["adjustmentots"] =  Adjustmentot::get();
        $data["page_title"] = "Adjustment Out Type Item Listing";
        $data["bclvl1"] = "Adjustment Out Type Item Listing";
        $data["bclvl1_url"] = route('adjustmentots.index');

        return view('stockmaster.adjustmentots.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Adjustment Out Type";
        $data["bclvl1"] = "Adjustment Out Type Item Listing";
        $data["bclvl1_url"] = route('adjustmentots.index');
        $data["bclvl2"] = "Add Adjustment Out Type";
        $data["bclvl2_url"] = route('adjustmentots.create');

        return view('stockmaster.adjustmentots.create', $data);
    }

    public function store(Request $request)
    {
        $adjustmentot = Adjustmentot::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('adjustmentots.index')->with('Success', 'Adjustment Out Type created successfully');
    }

    public function edit($id)
    {
        $data["adjustmentot"] = Adjustmentot::find($id);
        $data["page_title"] = "Edit Adjustment Out Type";
        $data["bclvl1"] = "Adjustment Out Type Item Listing";
        $data["bclvl1_url"] = route('adjustmentots.index');
        $data["bclvl2"] = "Edit Adjustment Out Type";
        $data["bclvl2_url"] = route('adjustmentots.edit', ['id' => $id]);

        return view('stockmaster.adjustmentots.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $adjustmentot = Adjustmentot::find($id);
        if (isset($adjustmentot)) {
            $adjustmentot->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }

        return redirect()->route('adjustmentots.index')->with('Success', 'Adjustment Out Type updated successfully');
    }

    public function destroy(adjustmentot $adjustmentot)
    {
        $adjustmentot->update([
            'deleted_by' => Auth::user()->id
        ]);
        $adjustmentot->delete();
        return redirect()->route('adjustmentots.index')->with('Success', 'Adjustment Out Type deleted successfully');
    }

    public function print()
    {
        $adjustmentots = Adjustmentot::all();

        if ($adjustmentots->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($adjustmentots as $adjustmentot) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $adjustmentot->code;
            $objectJSON["description"] = $adjustmentot->descr;
            $objectJSON["status"] = $adjustmentot->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $adjustmentotsJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintadjustmentot" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $adjustmentotsJSON);
        $JasperFileName = "ReportPrintadjustmentot" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Adjustment Out Type Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }
}
