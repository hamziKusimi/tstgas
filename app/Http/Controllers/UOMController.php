<?php

namespace App\Http\Controllers;

Use App\Model\Uom;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class UOMController extends Controller
{
    public function index()
    {
        $data["uoms"] = Uom::get();
        $data["page_title"] = "U.O.M Item Listing";
        $data["bclvl1"] = "U.O.M Item Listing";
        $data["bclvl1_url"] = route('uom.index');

        return view('stockmaster.uom.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add U.O.M";
        $data["bclvl1"] = "U.O.M Item Listing";
        $data["bclvl1_url"] = route('uom.index');
        $data["bclvl2"] = "Add U.O.M";
        $data["bclvl2_url"] = route('uom.create');

        return view('stockmaster.uom.create', $data);
    }

    public function store(Request $request)
    {
        
        $uom = Uom::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('uom.index')->with('Success', 'U.O.M created successfully.');
    }

    public function edit($id)
    {
        $data["uom"] = Uom::find($id);
        $data["page_title"] = "Edit U.O.M";
        $data["bclvl1"] = "U.O.M Item Listing";
        $data["bclvl1_url"] = route('uom.index');
        $data["bclvl2"] = "Edit U.O.M";
        $data["bclvl2_url"] = route('uom.edit', ['id'=>$id]);

        return view('stockmaster.uom.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $uom = Uom::find($id);
        if (isset($uom)) {
            $uom->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('uom.index')->with('Success', 'U.O.M updated successfully.');
    }

    public function destroy(Uom $uom)
    {
        $uom->update([
            'deleted_by' => Auth::user()->id
        ]);
        $uom->delete();
        return redirect()->route('uom.index')->with('Success', 'U.O.M deleted successfully.');
    }

    public function print()
    {
        $Uoms = Uom::all();
        
        if ($Uoms->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($Uoms as $Uom) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Uom->code;
            $objectJSON["description"] = $Uom->descr;
            $objectJSON["status"] = $Uom->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $UomsJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintUom" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $UomsJSON);
        $JasperFileName = "ReportPrintUom" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Uom Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }
    
}
