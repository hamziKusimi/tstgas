<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Manufacturer;
use Auth;

class ManufacturerController extends Controller
{
    public function index()
    {
        $data["manufacturers"] = Manufacturer::get();
        $data["page_title"] = "Manufacturer Item Listings";
        $data["bclvl1"] = "Manufacturer Item Listings";
        $data["bclvl1_url"] = route('manufacturers.index');
        return view('cylindermaster.manufacturers.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Manufacturer";
        $data["bclvl1"] = "Manufacturer Item Listings";
        $data["bclvl1_url"] = route('manufacturers.index');
        $data["bclvl2"] = "Add Manufacturer";
        $data["bclvl2_url"] = route('manufacturers.create');

        return view('cylindermaster.manufacturers.create', $data);
    }

    public function store(Request $request)
    {
        $manufacturer = Manufacturer::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('manufacturers.index')->with('Success', 'Manufacturer created successfully.');
    }

    public function edit($id)
    {
        $data["manufacturer"] = Manufacturer::find($id);
        $data["page_title"] = "Edit Manufacturer";
        $data["bclvl1"] = "Manufacturer Item Listings";
        $data["bclvl1_url"] = route('manufacturers.index');
        $data["bclvl2"] = "Edit Manufacturer";
        $data["bclvl2_url"] = route('manufacturers.edit', ['id'=>$id]);

        return view('cylindermaster.manufacturers.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $manufacturer = Manufacturer::find($id);
        if (isset($manufacturer)) {
            $manufacturer->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('manufacturers.index')->with('Success', 'Manufacturer updated successfully.');
    }

    public function destroy(manufacturer $manufacturer)
    {
        $manufacturer->update([
            'deleted_by' => Auth::user()->id
        ]);
        $manufacturer->delete();
        return redirect()->route('manufacturers.index')->with('Success', 'Manufacturer deleted successfully.');
    }
}
