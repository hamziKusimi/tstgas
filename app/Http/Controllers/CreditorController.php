<?php

namespace App\Http\Controllers;

use App\Model\Creditor;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;
use App\Model\PrintedIndexView;
use Carbon\Carbon;
use DB;
use Redirect;
use DataTables;

class CreditorController extends Controller
{
    public function api_get()
    {
        $resources = Creditor::get();

        return response()->json($resources);
    }

    public function index()
    {
        $data["creditors"] = Creditor::get();
        $data["page_title"] = "Creditor";
        $data["bclvl1"] = "Creditor";
        $data["bclvl1_url"] = route('creditors.index');
        return view('stockmaster.creditors.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Creditor";
        $data["bclvl1"] = "Creditor";
        $data["bclvl1_url"] = route('creditors.index');
        $data["bclvl2"] = "Add Creditor";
        $data["bclvl2_url"] = route('creditors.create');
        return view('stockmaster.creditors.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        Creditor::create([
            'accountcode' => $request['C_ACODE'],
            'name' => $request["C_NAME"],
            'address1' => $request["C_ADDRESS"],
            'tel' => $request["C_PHONE"],
            'fax' => $request["C_FAX"],
            'hp' => $request["C_HP"],
            'email' => $request["C_EMAIL"],
            'cterm' => $request["C_CTERM"],
            'climit' => $request["C_CLIMIT"],
            'gstno' => $request["d_gstno"],
            'brnno' => $request["d_brn"],
            'memo' => $request["C_MEMO"],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('creditors.index')->with('Success', 'Creditor created successfully.');
    }

    public function api_store(Request $request)
    {
        $data = [];
        foreach ($request->toArray() as $key => $req)
            $data[$key] = is_null($req) ? '' : $req;

        $climit = !empty($request['D_CLIMIT']) ? $request['D_CLIMIT'] : 0.00;
        if (strpos($climit, ',') !== false)
            $climit = (float) str_replace(',', '', $climit);

        $creditor = Creditor::create([
            'C_DC' => 'C',
            'accountcode' => $request['D_ACODE'],
            'name' => $request["D_NAME"],
            'address1' => $request['D_ADDR1'],
            'address2' => $request['D_ADDR2'],
            'address3' => $request['D_ADDR3'],
            'address4' => $request['D_ADDR4'],
            'tel' => $request["D_TEL"],
            'fax' => $request["D_FAX"],
            'email' => $request["D_EMAIL"],
            'cterm' => $request["C_CTERM"],
            'climit' => $climit,
            'gstno' => $request["d_gstno"],
            'brnno' => $request["d_brn"],
            'memo' => $request["D_MEMO"],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id

        ]);

        return $creditor;
    }

    public function edit($id)
    {
        $data["creditor"] = Creditor::find($id);
        $data["page_title"] = "Edit Creditor";
        $data["bclvl1"] = "Creditor Item Listing";
        $data["bclvl1_url"] = route('creditors.index');
        $data["bclvl2"] = "Edit Creditor";
        $data["bclvl2_url"] = route('creditors.edit', ['id' => $id]);

        return view('stockmaster.creditors.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $creditor = Creditor::find($id);
        $creditor->update([
            'accountcode' => $request['C_ACODE'],
            'name' => $request["C_NAME"],
            'address1' => $request["C_ADDRESS"],
            'tel' => $request["C_PHONE"],
            'fax' => $request["C_FAX"],
            'hp' => $request["C_HP"],
            'email' => $request["C_EMAIL"],
            'cterm' => $request["C_CTERM"],
            'climit' => $request["C_CLIMIT"],
            'gstno' => $request["d_gstno"],
            'brnno' => $request["d_brn"],
            'memo' => $request["C_MEMO"],
            'active' => $request['active'],
            'updated_by'=> Auth::user()->id

        ]);

        return redirect()->route('creditors.index')->with('Success', 'Creditor updated successfully.');
    }

    public function destroy(creditor $creditor)
    {
        $creditor->update([
            'deleted_by' => Auth::user()->id
        ]);
        $creditor->delete();
        return redirect()->route('creditors.index')->with('Success', 'Creditor deleted successfully.');
    }

    public function print(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Creditor Master Listing')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Creditor Master Listing')->update([
                'index' => "Creditor Master Listing",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Creditor Master Listing",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        if ($request->AC_Code_Chkbx == "on") {
            $creditors = Creditor::whereBetween('accountcode', [$request->AC_Code_frm, $request->AC_Code_to])
                ->get();
        } else {
            $creditors = Creditor::all();
        }

        if ($creditors->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        foreach ($creditors as $creditor) {
            $creditor->details = $creditor->name;
            if ($request->Address_Checkbox == "on") {
                $creditor->details = $creditor->details . PHP_EOL . $creditor->address1;
            }
            if ($request->Tel_Fax == "on") {
                $creditor->details = $creditor->details . PHP_EOL . "Tel:" . $creditor->tel;
                $creditor->details = $creditor->details . "  Fax:" . $creditor->fax;
            }
        }

        $creditorJSON = '{"data" :' . json_encode($creditors) . '}';

        $JsonFileName = "CreditorMasterListing" . date("Ymdhisa") . Auth::user()->id;

        file_put_contents(base_path('resources/reporting/Creditor/' . $JsonFileName . '.json'), $creditorJSON);
        $JasperFileName = "CreditorMasterListing" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/Creditor/CreditorMasterListing.jrxml';
        $output = base_path() . '/resources/reporting/Creditor/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/Creditor/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/Creditor/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function codereplacement(Request $request)
    {
        $Creditor = Creditor::where('accountcode','=', $request->old_code)->get();

        if($Creditor->isEmpty()){
            return Redirect::back()->with([
                'Error' => 'No Creditor Code Found', 'old_code' => $request->old_code,
                'new_code' => $request->new_code
            ]);
        }

        DB::table('creditors')->where('accountcode', $request->old_code)->update(['accountcode' => $request->new_code]);
        // DB::table('cashsalesdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('cashbilldts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('invoice_data')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('gooddts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('preturndts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('deliveryorderdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('salesorderdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('quotationdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('salesreturndts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('porderdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);

        // DB::table('cashsales')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('cashbills')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('invoices')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('goods')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('preturns')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('deliveryorders')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('salesorders')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('quotations')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        // DB::table('salesreturns')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('porders')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);

        return redirect()->route('codereplacement.creditorcode')
        ->with([
            'Success' => 'Creditor Code Sucessfully Updated', 'old_code' => $request->old_code,
            'new_code' => $request->new_code]);
    }

    public function creditorList()
    {
        $query = Creditor::query()
            ->selectRaw('accountcode, name')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }
}
