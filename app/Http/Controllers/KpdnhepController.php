<?php

namespace App\Http\Controllers;

use App\Model\Invoice;
use App\Model\View\StockIn;
use App\Model\Debtor;
use App\Model\View\kpdnhep_purchase;
use App\Model\PurchasedTable;
use DateTime;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Model\Cashsales;
use App\Model\Cashsalesdt;
use App\Model\Cashbill;
use App\Model\DocumentSetup;
use Excel;
use App\Exports\Export;
use App\ExportsSupplier\ExportSupplier;
use App\ExportsRetailer\ExportRetailer;
use App\Model\Creditor;

class KpdnhepController extends Controller
{
    public function generate_supp(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $Data = $this->getDataQuery($request);

        if ($Data->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $stockOpening = StockIn::selectRaw('SUM(totalqty) as opening')
            ->whereDate('t_date', '<', $dates_to)
            ->where('item_code',   $request->Stockcode)
            ->groupBy('item_code')
            ->first();

        $opening = $stockOpening == null ? 0 : $stockOpening->opening;
        $arr = [];
        $no = 1;
        foreach ($Data as $data) {
            $json = [];
            $json['no'] = $no;
            $json['doc_date'] =  date("d.m.Y", strtotime($data->doc_date));
            $json['doc_no'] =  $data->doc_no;
            $json['quantity'] =  $data->Tquantity;
            $json['price'] =  $data->price;

            if ($data->account_code != '') {
                $debtor = Debtor::leftJoin('salesmen', 'salesmen.code', '=', 'debtors.salesman')
                ->select('address1', 'address2', 'address3', 'address4', 'customval1', 'salesmen.vehicle_no')
                    ->where('accountcode', '=', $data->account_code)->first();

                $json['name'] = $data->customer;
                $json['addr1'] = str_replace(' ', '', $debtor->address1) != '' || $debtor->address1 != null ?
                    $debtor->address1 : '';
                $json['addr2'] = str_replace(' ', '', $debtor->address2) != '' || $debtor->address2 != null ?
                    $debtor->address2 : '';
                $json['addr3'] = str_replace(' ', '', $debtor->address3) != '' || $debtor->address3 != null ?
                    $debtor->address3 : '';
                $json['addr4'] = str_replace(' ', '', $debtor->address4) != '' || $debtor->address4 != null ?
                    $debtor->address4 : '';
                $json['licenseNo'] =  "LESEN NO: " . $debtor->customval1;
                $json['vehicle_no'] =  $debtor->vehicle_no;
            } else {
                $json['name'] = null;
                $json['addr1'] = null;
                $json['addr2'] = null;
                $json['addr3'] = null;
                $json['addr4'] = null;
                $json['licenseNo'] = null;
                $json['vehicle_no'] = null;
            }
            $json['closing_stock'] =  $opening - $json['quantity'];
            $opening = $json['closing_stock'];
            $no = $no + 1;
            $arr[] = $json;
        }

        switch($request->Stockcode){
            case "LPG14":
                $item = "14KG";
                break;
            case "LPG12":
                $item = "12KG";
                break;
            case "LPG50":
                $item = "50KG";
                break;
            default:
                $item = "14KG (C)";
        }

        $data["page_title"] =  config('config.company.name');
        $data["inv"] = collect($arr);
        $data["item"] = $item; return Excel::download(new ExportSupplier($data,$dates_to), 'Pembekal.xlsx');
     }

    public function process_belian(Request $request){

        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        switch($request->Stockcode){
            case "LPG14":
                $item = "14KG";
                break;
            case "LPG12":
                $item = "12KG";
                break;
            case "LPG50":
                $item = "50KG";
                break;
            default:
                $item = "14KG (C)";
        }
        $b_date = strtoupper(date("d",strtotime( $dates_to))). "." . date("m",strtotime( $dates_to)) . "." . date("y",strtotime( $dates_to));
        return Excel::download(new Export($item,$b_date), 'Belian.xlsx');
    }

    public function getDataQuery(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        // return kpdnhep_1::where(function ($query) use ($request) {

        //     $dates = explode("-", $request->dates);
        //     $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        //     $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        //     if ($dates_from == $dates_to) {
        //         $query->where('doc_date', '=', $dates_from);
        //     } else {
        //         $query->whereBetween('doc_date', [$dates_from, $dates_to]);
        //     }

        //     $query->where('item_code', '=', $request->Stockcode);
        // })->orderBy('seq')
        //     ->orderBy('doc_date')
        //     ->get();

    $first = Cashbill::join('cashbilldts', 'cashbilldts.doc_no', '=', 'cashbills.docno')
            ->leftjoin('debtors', 'cashbills.account_code', '=', 'debtors.accountcode')
            ->selectRaw("
            cashbilldts.doc_no, cashbills.account_code, cashbilldts.item_code, cashbilldts.subject,
            totalqty AS Tquantity, cashbilldts.uprice AS price,
            cashbills.date AS doc_date, cashbills.name AS customer,'A' AS seq
            ")
            ->whereRaw("
            cashbilldts.deleted_at IS NULL AND cashbills.deleted_at IS NULL
            AND
            debtors.deleted_at IS NULL
            AND
            (debtors.`custom1` = 'License No' AND debtors.`customval1` IS NOT NULL)
            ")
            ->where(function ($query) use ( $dates_from,  $dates_to, $request) {
                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

                $query->where('item_code', '=', $request->Stockcode);
            });

    $second = Invoice::join('invoice_data', 'invoice_data.doc_no', '=', 'invoices.docno')
            ->leftjoin('debtors', 'invoices.account_code', '=', 'debtors.accountcode')
            ->selectRaw("
            invoice_data.doc_no, invoice_data.account_code, invoice_data.item_code,
            invoice_data.subject, totalqty AS Tquantity, invoice_data.unit_price AS price,
            invoices.date AS doc_date, invoices.name AS customer,'A' AS seq
            ")
            ->whereRaw("
            invoice_data.deleted_at IS NULL AND invoices.deleted_at IS NULL
            AND
            debtors.deleted_at IS NULL
            AND
            (debtors.`custom1` = 'License No' AND debtors.`customval1` IS NOT NULL)
            ")
            ->where(function ($query) use ( $dates_from,  $dates_to, $request) {
                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

                $query->where('item_code', '=', $request->Stockcode);
            })->unionAll($first)
            ->orderBy('seq')
                ->orderBy('doc_no')
                ->get();

        return $second;
    }

    public function generate_ret(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $purchaseData = kpdnhep_purchase::where(function ($query) use ($request, $dates_from,  $dates_to) {
            if ($dates_from == $dates_to) {
                $query->where('date', '=', $dates_from);
            } else {
                $query->whereBetween('date', [$dates_from, $dates_to]);
            }
            $query->where('item_code', '=', $request->Stockcode);

            $query->where('doc_type', '=', 'Purchased');
        })->orderBy('date')
            ->orderBy('doc_no')
            ->get();

        if ($purchaseData->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $first = Cashbill::join('cashbilldts', 'cashbilldts.doc_no', '=', 'cashbills.docno')
            ->leftjoin('debtors', 'cashbills.account_code', '=', 'debtors.accountcode')
            ->selectRaw("
            cashbilldts.doc_no, cashbilldts.account_code, cashbilldts.item_code, cashbilldts.subject,
            totalqty AS Tquantity, cashbilldts.uprice AS price,
            cashbills.date AS doc_date, cashbills.name AS customer
            ")
            ->whereRaw("
            cashbilldts.deleted_at IS NULL AND cashbills.deleted_at IS NULL
            AND
            debtors.deleted_at IS NULL
            AND
            (debtors.`customval1` IS NULL)
            ")
            ->where(function ($query) use ( $dates_from,  $dates_to, $request) {
                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

               $query->where('item_code', '=', $request->Stockcode);
            });

    $saleData = Invoice::join('invoice_data', 'invoice_data.doc_no', '=', 'invoices.docno')
            ->leftjoin('debtors', 'invoices.account_code', '=', 'debtors.accountcode')
            ->selectRaw("
            invoice_data.doc_no, invoice_data.account_code, invoice_data.item_code,
            invoice_data.subject, totalqty AS Tquantity, invoice_data.unit_price AS price,
            invoices.date AS doc_date, invoices.name AS customer
            ")
            ->whereRaw("
            invoice_data.deleted_at IS NULL AND invoices.deleted_at IS NULL
            AND
            debtors.deleted_at IS NULL
            AND
            (debtors.`customval1` IS NULL)
            ")
            ->where(function ($query) use ( $dates_from,  $dates_to, $request) {
                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

               $query->where('item_code', '=', $request->Stockcode);
            })
            ->unionAll($first)
            ->orderBy('doc_date')
            ->orderBy('doc_no')
            ->orderBy('item_code')
            ->orderBy('Tquantity', 'DESC')
            ->get();

        if ($saleData->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $supplier = Creditor::select('name','address1')
                    ->where('name','=','T.S TRADING & CO')->first();

        $supplierName = $supplier->name;
        $supplierAddress = preg_split('/\n|\r\n?/', $supplier->address1);
        $supplierDetail = [];

        for($i=0;$i<5;$i++){
            $supplierDetail[] = $i == 0 ? $supplierName : $supplierAddress[$i-1];
        }

        switch($request->Stockcode){
            case "LPG14":
                $item = "14KG";
                break;
            case "LPG12":
                $item = "12KG";
                break;
            case "LPG50":
                $item = "50KG";
                break;
            default:
                $item = "14KG (C)";
        }
        $arr = [];
        $no = 1;
        $countRow = 0;
        $currStock = 0;
        $checkRow = 5;
        foreach ($purchaseData as $data) {
            $json = [];
            $json['no'] = $no;
            $json['doc_date'] =  date("d/m/Y", strtotime($data->date));
            $json['doc_no'] =  $data->doc_no;
            $json['quantity'] =  $data->qty;
            $json['price'] =  number_format($request->price,2);
            $json['current_stock'] = $json['quantity'];
            if($no <= 5){
                $json['supplier'] = $supplierDetail[$no-1];
                $checkRow = $checkRow - 1;
            }else{
                $json['supplier'] = "";
            }

            $json['total_sale'] = "";
            $json['closing_stock'] = $currStock + $json['current_stock'];

            $currStock = $currStock + $json['current_stock'];
            $no = $no + 1;
            $arr[] = $json;
            for ($i = $countRow; $i < count($saleData); $i++) {
                if ($currStock - $saleData[$i]['Tquantity'] < 0) {
                    break;
                }
                $json = [];
                $json['no'] = $no;
                $json['doc_date'] =  date("d/m/Y", strtotime($saleData[$i]['doc_date']));
                $json['doc_no'] =  $saleData[$i]['doc_no'];
                $json['quantity'] =  "";
                $json['price'] = "";
                if($no <= 5){
                    $json['supplier'] = $supplierDetail[$no-1];
                    $checkRow = $checkRow - 1;
                }else{
                    $json['supplier'] = "";
                }

                $json['opening_stock'] = "";
                $json['current_stock'] = "";
                $json['total_sale'] =  $saleData[$i]['Tquantity'];
                $json['closing_stock'] =  $currStock - $json['total_sale'];

                $currStock = $currStock - $json['total_sale'];
                $no = $no + 1;
                $arr[] = $json;
                $countRow++;
            }
        }

        $data = [];
        $data["page_title"] =  config('config.company.name');
        $data["inv"] = collect($arr);
        $data["item"] =  $item;
        $data["checkRow"] =  $checkRow;
        $data["supplierDetail"] = $supplierDetail;
        $data["r_date"] = strtoupper(date("M",strtotime( $dates_to))) . "-" . date("y",strtotime( $dates_to));

        // return view('reportprinting.inventory.kpdnhep.retailer', $data);
        return Excel::download(new ExportRetailer( $data), 'Peruncit.xlsx');
    }

    public function process(Request $request)
    {
        switch ($request->input('Btn')) {
            case 'view':
                return $this->process_view($request);
                break;

            case 'generate':
                return $this->generate($request);
                break;
        }
    }

    public function generate(Request $request)
    {
        $runningNumber = DocumentSetup::findByName('Cash Sales');
        $initDocNo = $runningNumber->nextRunningNoString;
        $Dates = $request->dates;

        if($request->noQuota){
            $quota = 1000000;
        }else{
            $quota = $request->Quota;
        }


        PurchasedTable::getQuery()->delete();
        $item = $request['Stockcode_chk'];

        if ($item == null) {
            return Redirect::back()->with([
                'Error' => 'No Item Selected', 'Quota' => $quota,
                'Dates' => $Dates, 'Items' => $item
            ]);
        }

        $Sales = $this->getSaleProcessQuery($request, $item);
        if ($Sales->isEmpty()) {
            return Redirect::back()->with([
                'Error' => 'No Data Found', 'Quota' => $quota,
                'Dates' => $Dates, 'DocNo' => $initDocNo, 'Items' => $item
            ]);
        }

        $arr = [];
        $sum = [];
        $countIt = 1;
        foreach ($item as $data) {
            $purchase = [];
            $purchase['item_' .  $countIt]  = 0;
            $sum[$countIt] = 0;
            $purchase['item_' .  $countIt . '_code'] = $data;
            $arr[] = $purchase;
            $countIt++;
        }

        $totalBal = array_sum($sum);

        $cbID = 0;
        foreach ($Sales as $cb) {

            if ($totalBal > 0) {
                $sums = [];
                for ($j = 1; $j <= count($arr); $j++) {
                    if ($cb->item_code == $arr[$j - 1]['item_' . $j . '_code']) {

                        $arr[$j - 1]['item_' . $j] = $arr[$j - 1]['item_' . $j] - $cb->Tquantity;
                    }
                    $sums[$j] = $arr[$j - 1]['item_' . $j];
                }
                $totalBal = array_sum($sums);
            } else {
                $arr = $this->addFunction($cbID, $Sales, $quota, $arr, $cb, $initDocNo);

                $sums = [];
                for ($j = 1; $j <= count($arr); $j++) {
                    if ($cb->item_code == $arr[$j - 1]['item_' . $j . '_code']) {

                        $arr[$j - 1]['item_' . $j] = $arr[$j - 1]['item_' . $j] - $cb->Tquantity;
                    }
                    $sums[$j] = $arr[$j - 1]['item_' . $j];
                }
                $totalBal = array_sum($sums);
            }
            $cbID++;
        }

        return redirect()->route('reportprinting.KPDNHEP_Process')
            ->with([
                'Success' => 'Purchased sucessfully Generated', 'Quota' => $quota,
                'Dates' => $Dates, 'DocNo' => $initDocNo, 'Items' => $item
            ]);
    }

    public function addFunction($cbID, $Sales, $quota, $arr, $cb, $initDocNo)
    {
        $totPurch = 0;
        for ($i = $cbID; $i < count($Sales); $i++) {

            if ($totPurch + $Sales[$i]->Tquantity > $quota) {
                $this->insertToPurchase($cb, $arr, $initDocNo, $quota);
                break;
            } else {

                $totPurch += $Sales[$i]->Tquantity;

                $j = 1;
                for ($j = 1; $j <= count($arr); $j++) {
                    if ($Sales[$i]->item_code == $arr[$j - 1]['item_' . $j . '_code']) {
                        $arr[$j - 1]['item_' . $j] += $Sales[$i]->Tquantity;
                    }
                }
            }

            if ($i == count($Sales) - 1) {
                $this->insertToPurchase($cb, $arr, $initDocNo, $quota);
            }
        }

        return $arr;
    }

    public function insertToPurchase($cb, $arr, $initDocNo, $quota)
    {
        $j = 1;
        foreach ($arr as $item) {
            $getPreviousData = PurchasedTable::orderby('doc_no', 'desc')->first();
            if ($getPreviousData != null) {
                $alphabet = preg_replace("/[0-9]+/", "", $getPreviousData->doc_no);
                $digit = preg_replace("/[^0-9]+/", "", $getPreviousData->doc_no);
                $lengDig = strlen($digit);
                $nextDig = (int) $digit + 1;
                $nextDigStr = str_pad($nextDig, $lengDig, '0', STR_PAD_LEFT);
                $docno = $alphabet . $nextDigStr;
            } else {
                $docno = $initDocNo;
            }
            if ($item['item_' . $j] != 0) {

                PurchasedTable::insert(
                    [
                        'doc_no' => $docno, 'stock_code' => $item['item_' . $j . '_code'],  'date' => $cb->doc_date,
                        'qty' => $item['item_' . $j], 'price' => 0, 'amount' => 0, 'max_quota' =>  $quota,
                        'created_at' => date('Y-m-d H:i:s'), 'created_by' => Auth::user()->id
                    ]
                );
                Cashsalesdt::insert([
                    'doc_no' => $docno, 'sequence_no' => '0001', 'account_code' =>"CT001-00",
                    'item_code' => $item['item_' . $j . '_code'], 'subject' => $cb->subject, 'qty' => $item['item_' . $j],
                    'uprice' => $cb->price, 'amount' => $cb->price * $item['item_' . $j] ,
                    'created_at' => date('Y-m-d H:i:s'), 'created_by' => Auth::user()->id
                ]);

                Cashsales::insert([
                    'docno' => $docno, 'date' => $cb->doc_date,  'amount' => $cb->price * $item['item_' . $j],
                    'account_code' =>"CT001-00", 'name' => config('config.company.name'),
                    'created_at' => date('Y-m-d H:i:s'), 'created_by' => Auth::user()->id
                    ]);

                 $CashbsalesDoc = DocumentSetup::findByName('Cash Sales');
                 $CashbsalesDoc->update(['D_LAST_NO' => $CashbsalesDoc->D_LAST_NO + 1]);

            }
            $j++;
        }
    }

    public function getSaleProcessQuery(Request $request, $item)
    {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $first = Cashbill::join('cashbilldts', 'cashbilldts.doc_no', '=', 'cashbills.docno')
                        ->leftjoin('debtors', 'cashbills.account_code', '=', 'debtors.accountcode')
                        ->selectRaw("
                        cashbilldts.doc_no, cashbilldts.account_code, cashbilldts.item_code, cashbilldts.subject,
                        totalqty AS Tquantity, cashbilldts.uprice AS price,
                        cashbills.date AS doc_date, cashbills.name AS customer
                        ")
                        ->whereRaw("
                        cashbilldts.deleted_at IS NULL AND cashbills.deleted_at IS NULL
                        AND
                        debtors.deleted_at IS NULL
                        AND
                        (debtors.`customval1` IS NULL)
                        ")
                        ->where(function ($query) use ( $dates_from,  $dates_to, $item) {
                            if ($dates_from == $dates_to) {
                                $query->where('date', '=', $dates_from);
                            } else {
                                $query->whereBetween('date', [$dates_from, $dates_to]);
                            }

                            $query->whereIn('item_code', $item);
                        });

        $second = Invoice::join('invoice_data', 'invoice_data.doc_no', '=', 'invoices.docno')
                        ->leftjoin('debtors', 'invoices.account_code', '=', 'debtors.accountcode')
                        ->selectRaw("
                        invoice_data.doc_no, invoice_data.account_code, invoice_data.item_code,
                        invoice_data.subject, totalqty AS Tquantity, invoice_data.unit_price AS price,
                        invoices.date AS doc_date, invoices.name AS customer
                        ")
                        ->whereRaw("
                        invoice_data.deleted_at IS NULL AND invoices.deleted_at IS NULL
                        AND
                        debtors.deleted_at IS NULL
                        AND
		                (debtors.`customval1` IS NULL)
                        ")
                        ->where(function ($query) use ( $dates_from,  $dates_to, $item) {
                            if ($dates_from == $dates_to) {
                                $query->where('date', '=', $dates_from);
                            } else {
                                $query->whereBetween('date', [$dates_from, $dates_to]);
                            }

                            $query->whereIn('item_code', $item);
                        })
                        ->unionAll($first)
                        ->orderBy('doc_date')
                        ->orderBy('doc_no')
                        ->orderBy('item_code')
                        ->orderBy('Tquantity', 'DESC')
                        ->get();
        return $second;
    }

    public function process_view(Request $request)
    {
        $initDocNo = $request->doc_no;
        $quota = $request->Quota;
        $Dates = $request->dates;

        $item = $request['Stockcode_chk'];

        if ($item == null) {
            return Redirect::back()->with([
                'Error' => 'No Item Selected', 'Quota' => $quota,
                'Dates' => $Dates, 'DocNo' => $initDocNo, 'Items' => $item
            ]);
        }

        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $purchase = kpdnhep_purchase::where(function ($query) use ($request, $item, $dates_from,  $dates_to) {

            if ($dates_from == $dates_to) {
                $query->where('date', '=', $dates_from);
            } else {
                $query->whereBetween('date', [$dates_from, $dates_to]);
            }
            $query->whereIn('item_code', $item);
            $query->where('doc_type', '=', 'Purchased');
        })->orderBy('doc_no')->get();

        if ($purchase->isEmpty()) {
            return $this->viewNoPurchased($request, $item, $dates_from, $dates_to, $Dates, $quota, $initDocNo);
            // return Redirect::back()->with([
            //     'Error' => 'No Item Found', 'Quota' => $quota,
            //     'Dates' => $Dates,'DocNo' => $initDocNo, 'Items' => $item
            // ]);
        }

        $first = Cashbill::join('cashbilldts', 'cashbilldts.doc_no', '=', 'cashbills.docno')
            ->leftjoin('debtors', 'cashbills.account_code', '=', 'debtors.accountcode')
            ->selectRaw("
            cashbilldts.doc_no, cashbilldts.account_code, cashbilldts.item_code, cashbilldts.subject,
            totalqty AS Tquantity, cashbilldts.uprice AS price,
            cashbills.date AS doc_date, cashbills.name AS customer
            ")
            ->whereRaw("
            cashbilldts.deleted_at IS NULL AND cashbills.deleted_at IS NULL
            AND
            debtors.deleted_at IS NULL
            AND
            (debtors.`customval1` IS NULL)
            ")
            ->where(function ($query) use ( $dates_from,  $dates_to, $item) {
                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

                $query->whereIn('item_code', $item);
            });

        $sale = Invoice::join('invoice_data', 'invoice_data.doc_no', '=', 'invoices.docno')
            ->leftjoin('debtors', 'invoices.account_code', '=', 'debtors.accountcode')
            ->selectRaw("
            invoice_data.doc_no, invoice_data.account_code, invoice_data.item_code,
            invoice_data.subject, totalqty AS Tquantity, invoice_data.unit_price AS price,
            invoices.date AS doc_date, invoices.name AS customer
            ")
            ->whereRaw("
            invoice_data.deleted_at IS NULL AND invoices.deleted_at IS NULL
            AND
            debtors.deleted_at IS NULL
            AND
            (debtors.`customval1` IS NULL)
            ")
            ->where(function ($query) use ( $dates_from,  $dates_to, $item) {
                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }

                $query->whereIn('item_code', $item);
            })
            ->unionAll($first)
            ->orderBy('doc_date')
            ->orderBy('doc_no')
            ->orderBy('item_code')
            ->orderBy('Tquantity', 'DESC')
            ->get();


        if ($sale->isEmpty()) {
            return Redirect::back()->with([
                'Error' => 'No Item Found', 'Quota' => $quota,
                'Dates' => $Dates, 'DocNo' => $initDocNo, 'Items' => $item
            ]);
        }

        $purch_quota = PurchasedTable::select('max_quota')
            ->where(function ($query) use ($request, $item, $dates_from,  $dates_to) {
                if ($dates_from == $dates_to) {
                    $query->where('date', '=', $dates_from);
                } else {
                    $query->whereBetween('date', [$dates_from, $dates_to]);
                }
                $query->whereIn('stock_code', $item);
            })->first();
        $max_quota = $purch_quota->max_quota;

        $no = 1;
        $countRow = 0;
        $currStock = 0;
        foreach ($purchase as $data) {
            if ($currStock +  $data->qty > $max_quota) {
                for ($i = $countRow; $i < count($sale); $i++) {
                    if ($currStock - $sale[$i]['Tquantity'] < 0) {
                        $countRow = $i;
                        break;
                    }
                    $items = [];
                    $items['no'] = $no;
                    $items['doc_type'] = "Sale";
                    $items['p_qty'] = "";
                    $items['doc_no'] = $sale[$i]['doc_no'];
                    $items['date'] = date("d/m/Y", strtotime($sale[$i]['doc_date']));
                    $items['stock_code'] = $sale[$i]['item_code'];
                    $items['qty'] = $sale[$i]['Tquantity'];
                    $currStock = $currStock - $sale[$i]['Tquantity'];
                    $no = $no + 1;
                    $arr[] = $items;
                }
            }
            $items = [];
            $items['no'] = $no;
            $items['doc_type'] = $data->doc_type;
            $items['p_qty'] =  $data->qty;
            $items['doc_no'] = $data->doc_no;
            $items['date'] = date("d/m/Y", strtotime($data->date));
            $items['stock_code'] = $data->item_code;
            $items['qty'] = "";
            $currStock = $currStock + $items['p_qty'];
            $no = $no + 1;
            $arr[] = $items;
        }

        if ($countRow < count($sale) && $currStock > 0) {

            for ($i = $countRow; $i < count($sale); $i++) {
                if ($currStock - $sale[$i]['Tquantity'] < 0) {
                    $countRow = $i;
                    break;
                }
                $items = [];
                $items['no'] = $no;
                $items['doc_type'] = "Sale";
                $items['p_qty'] = "";
                $items['doc_no'] = $sale[$i]['doc_no'];
                $items['date'] = date("d/m/Y", strtotime($sale[$i]['doc_date']));
                $items['stock_code'] = $sale[$i]['item_code'];
                $items['qty'] = $sale[$i]['Tquantity'];
                $currStock = $currStock - $sale[$i]['Tquantity'];
                $no = $no + 1;
                $arr[] = $items;
            }
        }

        return redirect()->route('reportprinting.KPDNHEP_Process')
            ->with([
                'Quota' => $quota, 'Dates' => $Dates, 'DocNo' => $initDocNo,
                'Items' => $item, 'view' => $arr
            ]);
    }

    public function viewNoPurchased($request, $item, $dates_from,  $dates_to, $Dates, $quota, $initDocNo)
    {
        // $sale = kpdnhep_2::where(function ($query) use ($request, $item, $dates_from,  $dates_to) {

        //     if ($dates_from == $dates_to) {
        //         $query->where('doc_date', '=', $dates_from);
        //     } else {
        //         $query->whereBetween('doc_date', [$dates_from, $dates_to]);
        //     }
        //     $query->whereIn('item_code', $item);
        // })->orderBy('doc_date')
        //     ->orderBy('doc_no')
        //     ->orderBy('item_code')
        //     ->orderBy('Tquantity', 'DESC')
        //     ->get();

    $first = Cashbill::join('cashbilldts', 'cashbilldts.doc_no', '=', 'cashbills.docno')
        ->leftjoin('debtors', 'cashbills.account_code', '=', 'debtors.accountcode')
        ->selectRaw("
        cashbilldts.doc_no, cashbilldts.account_code, cashbilldts.item_code, cashbilldts.subject,
        totalqty AS Tquantity, cashbilldts.uprice AS price,
        cashbills.date AS doc_date, cashbills.name AS customer
        ")
        ->whereRaw("
        cashbilldts.deleted_at IS NULL AND cashbills.deleted_at IS NULL
        AND
        debtors.deleted_at IS NULL
        AND
        (debtors.`customval1` IS NULL)
        ")
        ->where(function ($query) use ( $dates_from,  $dates_to, $item) {
            if ($dates_from == $dates_to) {
                $query->where('date', '=', $dates_from);
            } else {
                $query->whereBetween('date', [$dates_from, $dates_to]);
            }

            $query->whereIn('item_code', $item);
        });

    $sale = Invoice::join('invoice_data', 'invoice_data.doc_no', '=', 'invoices.docno')
        ->leftjoin('debtors', 'invoices.account_code', '=', 'debtors.accountcode')
        ->selectRaw("
        invoice_data.doc_no, invoice_data.account_code, invoice_data.item_code,
        invoice_data.subject, totalqty AS Tquantity, invoice_data.unit_price AS price,
        invoices.date AS doc_date, invoices.name AS customer
        ")
        ->whereRaw("
        invoice_data.deleted_at IS NULL AND invoices.deleted_at IS NULL
        AND
        debtors.deleted_at IS NULL
        AND
        (debtors.`customval1` IS NULL)
        ")
        ->where(function ($query) use ( $dates_from,  $dates_to, $item) {
            if ($dates_from == $dates_to) {
                $query->where('date', '=', $dates_from);
            } else {
                $query->whereBetween('date', [$dates_from, $dates_to]);
            }

            $query->whereIn('item_code', $item);
        })
        ->unionAll($first)
        ->orderBy('doc_date')
        ->orderBy('doc_no')
        ->orderBy('item_code')
        ->orderBy('Tquantity', 'DESC')
        ->get();


        $no = 1;
        foreach ($sale as $data) {
            $items = [];
            $items['no'] = $no;
            $items['doc_type'] = "Sale";
            $items['p_qty'] =  "";
            $items['doc_no'] = $data->doc_no;
            $items['date'] = date("d/m/Y", strtotime($data->date));
            $items['stock_code'] = $data->item_code;
            $items['qty'] = $data->Tquantity;
            $no = $no + 1;
            $arr[] = $items;
        }

        if ($sale->isEmpty()) {
            return Redirect::back()->with([
                'Error' => 'No Item Found', 'Quota' => $quota,
                'Dates' => $Dates, 'DocNo' => $initDocNo, 'Items' => $item
            ]);
        }

        return redirect()->route('reportprinting.KPDNHEP_Process')
            ->with([
                'Quota' => $quota, 'Dates' => $Dates, 'DocNo' => $initDocNo,
                'Items' => $item, 'view' => $arr
            ]);
    }
}
