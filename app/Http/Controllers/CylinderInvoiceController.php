<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CylinderInvoice;
use App\Model\CylinderInvoicedt;
use App\Model\DocumentSetup;
use App\Model\Cylinder;
use Carbon\Carbon;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Debtor;
use App\Model\Location;
use App\Model\Cylindercategory;
use App\Model\SystemSetup;
use App\Model\Stockcode;
use App\Model\CustomObject\Transaction;
use App\Model\DeliveryNote;
use App\Model\Salesman;
use App\Model\View\CylinderInvProcessing;
use App\Model\Gasrack;
use PDF;
use Storage;
use App\Model\View\GasrackInvProcessing;
use App\Model\Area;

class CylinderInvoiceController extends Controller
{
    public function index()
    {
        $data["invoices"] = CylinderInvoice::orderBy('docno', 'desc')->get();
        $data["customers"] = CylinderInvoice::pluck('name', 'account_code');
        $data["dates"] = CylinderInvoice::pluck('date', 'date');
        $data["docno_select"] = CylinderInvoice::orderBy('docno')->pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::where('custom2', '=', 'Auto Invoice Generation')
            ->where('customval2', '=', 'on')->get()->pluck('Detail', 'accountcode');
        $data["page_title"] = "Cylinder Invoices Listing";
        $data["bclvl1"] = "Cylinder Invoices Listing";
        $data["bclvl1_url"] = route('cylinderinvoices.index');
        return view('dailypro.cylinderinvoices.index', $data);
    }

    public function query(Request $request)
    {
        $datefrmto = explode("-", $request->datefrmto);
        $datefrm = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $datefrmto[0]))->format('Y-m-d');
        $dateto = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $datefrmto[1]))->format('Y-m-d');

        $data["invoices"] = CylinderInvoice::select('*')
            ->where(function ($query) use ($request, $datefrm, $dateto) {
                if (!empty($request->custfrm)) {
                    $query->whereBetween('account_code', [$request->custfrm, $request->custto]);
                }

                if (!empty($request->datefrmto)) {
                    if ($datefrm == $dateto) {
                        $query->where('date', '=', $datefrm);
                    } else {
                        $query->whereBetween('date', [$datefrm, $dateto]);
                    }
                }
            })
            ->get();

        $data["customers"] = CylinderInvoice::pluck('name', 'account_code');
        $data["dates"] = CylinderInvoice::pluck('date', 'date');
        $data["docno_select"] = CylinderInvoice::orderBy('docno')->pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');
        $data["page_title"] = "Cylinder Invoices Listing";
        $data["bclvl1"] = "Cylinder Invoices Listing";
        $data["bclvl1_url"] = route('cylinderinvoices.index');
        $data["bclvl1"] = "Cylinder Invoices Received Listing";
        $data["bclvl1_url"] = route('cylinderinvoices.index');
        return view('dailypro.cylinderinvoices.index', $data);
    }

    public function getDetailsBySerial($serial)
    {
        $product = Cylinder::where('serial', $serial)->first();
        return response()->json($product);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $runningNumber = DocumentSetup::findByName('Cylinder Invoice');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $systemsetup = SystemSetup::first();
        // $product = Cylinder::whereNotNull('serial')->get(['barcode','serial','cat_id', 'prod_id', 'capacity', 'testpressure']);
        $product = Cylinder::whereNotNull('barcode')->pluck('serial');

        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['masterCodes'] = $masterCodes;
        $data['runningNumber'] = $runningNumber;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;

        // return response()->json($data);
        $data["page_title"] = "Cylinder Invoices Listing";
        $data["bclvl1"] = "Cylinder Invoices Listing";
        $data["bclvl1_url"] = route('cylinderinvoices.index');
        $data["bclvl2"] = "Add Cylinder Invoices";
        $data["bclvl2_url"] = route('cylinderinvoices.create');
        return view('dailypro.cylinderinvoices.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $invoice = CylinderInvoice::create([
            'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
            'refinvdate'  => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
            'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'currency' => !empty($request['currency']) ? $request['currency'] : '',
            'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
            'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
            'created_by' => Auth::user()->id
        ]);

        // dd(count($request['subject']));
        if (count($request['subject']) > 1) {
            for ($i = 1; $i < count($request['subject']); $i++) {
                $dt = CylinderInvoicedt::create([
                    'doc_no' => $invoice->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'product' => $request['product'][$i],
                    'type' => $request['type'][$i],
                    'quantity' => $request['quantity'][$i],
                    'uom' => $request['uom'][$i],
                    'unit_price' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        $invoiceDoc = DocumentSetup::findByName('Cylinder Invoice');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        return redirect()->route('cylinderinvoices.index')->with('Success', 'Invoice added successfully');
    }

    public function edit($id)
    {
        $invoice = CylinderInvoice::find($id);
        $contents = CylinderInvoicedt::where('doc_no', $invoice->docno)->get();

        // return response()->json($invoice->doc_no);
        $selectedDebitor = Debtor::findByAcode($invoice->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();;
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $systemsetup = SystemSetup::first();
        $productval = Cylinder::pluck('serial', 'serial');
        $product = Cylinder::pluck('serial');
        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['item'] = $invoice;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;
        $data['productval'] = $productval;
        $data["page_title"] = "Invoice Sales Listing";
        $data["bclvl1"] = "Invoice Sales Listing";
        $data["bclvl1_url"] = route('cylinderinvoices.index');
        // $data["bclvl2"] = "Edit Invoices";
        // $data["bclvl2_url"] = route('invoices.edit');

        // return response()->json($data);

        return view('dailypro.cylinderinvoices.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // update acc_invmt row
        $invoice = CylinderInvoice::find($id);
        if (isset($invoice)) {

            // update invoice
            $invoice->update([
                'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                'refinvdate'  => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
                'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'created_by' => Auth::user()->id
            ]);

            if (count($request['subject']) > 1) {
                for ($i = 1; $i < count($request['subject']); $i++) {

                    $dt = CylinderInvoicedt::find($request['item_id'][$i]);

                    if (isset($dt)) {

                        $dt->update([
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['unit_measure'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        $dt = CylinderInvoicedt::create([
                            'doc_no' => $invoice->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['unit_measure'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }

        return redirect()->route('cylinderinvoices.edit', ['id' => $id])->with('Success', 'Invoice updated successfully');
    }

    public function destroy($id)
    {
        $invoice = CylinderInvoice::find($id);
        $invoice->update([
            'deleted_by' => Auth::user()->id
        ]);
        $invoice->delete();

        $invoicedt = CylinderInvoicedt::where('doc_no', $invoice->docno);
        $invoicedt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $invoicedt->delete();

        return redirect()->route('cylinderinvoices.index')->with('Success', 'Invoice record deleted successfully');
    }

    public function destroyData($id)
    {
        $data = CylinderInvoicedt::find($id);

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function jasper($id)
    {
        $Resources = CylinderInvoice::where('id', $id)->get();
        $invoice = CylinderInvoice::select(
            'docno',
            'date',
            'account_code',
            'name',
            'do_no',
            'addr1',
            'addr2',
            'addr3',
            'addr4',
            'tel_no',
            'fax_no',
            'amount',
            'taxed_amount',
            'header',
            'footer',
            'summary'
        )->where('id', $id)->first();

        $CTERM = Debtor::Select('cterm')
            ->where('accountcode', '=',  $invoice->ACC_CODE)->first();

        $data = [];
        $data['invoice'] = $invoice;
        $data['invdata'] = CylinderInvoicedt::where("doc_no", "=", $invoice->docno)
            ->get();
        $data['cterm'] = $CTERM;
        $systemSetup = SystemSetup::first(['bank_detail', 'bank_account_no']);

        $pdf = PDF::loadView('dailypro.cylinderinvoices.invoices-pdf', ['inv' => $data, 'systemSetup' => $systemSetup])->setPaper('A4');

        // $datass["inv"] = $data;
        // $datass["systemSetup"] = $systemSetup;
        // return view('dailypro.cylinderinvoices.invoices-pdf', $datass);
        return $pdf->inline();
    }

    public function print(Request $request)
    {
        $data = [];
        $amount = 0;
        if ($request->Invoice_rcv == "listing") {
            $Invoice = $this->getInvoiceQuery($request);

            if ($Invoice->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }
            foreach ($Invoice as $iv) {

                $amount = $amount + $iv->amount;
            }

            $data['invoiceJSON'] = $Invoice;
            $data['totalamount'] = $amount;
            $pdf = PDF::loadView('dailypro.cylinderinvoices.listing',  $data)->setPaper('A4');

            return $pdf->inline();
        } else if ($request->Invoice_rcv == "summary") {
            $Invoice = $this->getInvoiceQuery($request);
            if ($Invoice->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }

            foreach ($Invoice as $iv) {
                $amount = $amount + $iv->amount;
            }

            $data['invoiceJSON'] = $Invoice;
            $data['totalamount'] = $amount;
            $pdf = PDF::loadView('dailypro.cylinderinvoices.summary',  $data)->setPaper('A4');

            return $pdf->inline();
        } else {
            $Invoice = $this->getInvoiceQueryByBatch($request);
            if ($Invoice->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }
            return $this->printByBatch($Invoice);
        }
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/CylinderInvoice/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/CylinderInvoice/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/CylinderInvoice/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/CylinderInvoice/' . $JasperFileName . '.pdf';
        return $file;
    }
    public function getInvoiceQuery(Request $request)
    {
        return CylinderInvoice::join('cylinder_invoicedts', 'cylinder_invoices.docno', '=', 'cylinder_invoicedts.doc_no')
            ->selectRaw("`cylinder_invoices`.`docno`          AS `docno`,
        `cylinder_invoices`.`date`           AS `date`,
        `cylinder_invoices`.`do_no`          AS `do_no`,
        `cylinder_invoices`.`account_code`   AS `account_code`,
        `cylinder_invoices`.`account_code`   AS `debtor`,
        `cylinder_invoices`.`name`           AS `name`,
        `cylinder_invoices`.`footer`           AS `footer`,
        CONCAT(CONCAT(`cylinder_invoices`.`docno`),', ',DATE_FORMAT(`cylinder_invoices`.`date`, '%d-%m-%Y'),', ',`cylinder_invoices`.`account_code`,', ',`cylinder_invoices`.`name`) AS `details`,
        `cylinder_invoicedts`.`subject`      AS `description`,
        `cylinder_invoicedts`.`id`           AS `id`,
        `cylinder_invoicedts`.`doc_no`       AS `doc_no`,
        `cylinder_invoicedts`.`quantity`     AS `quantity`,
        `cylinder_invoicedts`.`uom`          AS `uom`,
        `cylinder_invoicedts`.`unit_price`   AS `unit_price`,
        `cylinder_invoicedts`.`amount`       AS `amount`,
        `cylinder_invoicedts`.`taxed_amount` AS `tax_amount`,
        `cylinder_invoicedts`.`subject`      AS `subject`,
        `cylinder_invoicedts`.`details`      AS `DETAIL`,
        cylinder_invoices.amount AS totalamt")
            ->where(function ($query) use ($request) {
                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->DO_Chkbx_1 == "on") {
                    if ($request->DO_to_1 == null) {
                        $query->where('do_no', "=", $request->DO_frm_1);
                    } else {
                        $query->whereBetween('do_no', [$request->DO_frm_1, $request->DO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

    public function getInvoiceQueryByBatch(Request $request)
    {
        return CylinderInvoice::select(
            'docno',
            'date',
            'do_no',
            'account_code',
            'name',
            'addr1',
            'addr2',
            'addr3',
            'addr4',
            'tel_no',
            'fax_no',
            'credit_term',
            'amount',
            'header',
            'summary',
            'footer'
        )
            ->where(function ($query) use ($request) {

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->DO_Chkbx_1 == "on") {
                    if ($request->DO_to_1 == null) {
                        $query->where('do_no', "=", $request->DO_frm_1);
                    } else {
                        $query->whereBetween('do_no', [$request->DO_frm_1, $request->DO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

    public function getInvoiceListingJSON($Invoices)
    {
        $dataArray = array();
        foreach ($Invoices as $invoice) {

            $invoiceTrans = CylinderInvoicedt::select(
                'serial',
                'barcode',
                'subject',
                'barcode',
                'serial',
                'product',
                'unit_price',
                'details',
                'amount',
                'taxed_amount'
            )
                ->where("doc_no", "=", $invoice->docno)
                ->get();

            $lastElement = count($invoiceTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($invoiceTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($invoice->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $invoice->docno . ", " . $date . ", " .
                    $invoice->account_code . ", " . $invoice->name;
                $objectJSON['description'] = $trans->subject;

                $objectJSON['barcode'] = $trans->barcode;
                $objectJSON['serial'] = $trans->serial;
                $objectJSON['product'] = $trans->product;
                $objectJSON['unit_price'] = $trans->unit_price;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $objectJSON['amount'];
                $t_qty =  $t_qty + $trans->quantity;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getInvoiceSummaryJSON($Invoices)
    {
        $dataArray = array();
        $s_n = 1;
        foreach ($Invoices as $invoice) {
            $invoiceTrans = CylinderInvoicedt::select('amount', 'updated_at')
                ->where("doc_no", "=", $invoice->docno)
                ->get();

            foreach ($invoiceTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $invoice->docno;
                $objectJSON['date'] = $date;
                if ($invoice->do_no != "") {
                    $objectJSON['do_no'] = $invoice->do_no;
                } else {
                    $objectJSON['do_no'] = "   -";
                }
                $objectJSON['debtor'] = $invoice->account_code;
                $objectJSON['name'] = $invoice->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function processing(Request $request)
    {
        $cylinderGenerated  = [];
        $gasrackGenerated =  [];
        $dates = explode("-", $request->datefrmto);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        //check whether invoice of the selected debtor and date range already generated
        $debtors = Debtor::where(function ($query) use ($request){
            if ($request->AC_Code_Chkbx == "on") {
                if ($request->AC_Code_to == $request->AC_Code_frm) {
                    $query->where('accountcode', "=", $request->AC_Code_frm);
                } else {
                    $query->whereBetween('accountcode', [$request->AC_Code_frm, $request->AC_Code_to]);
                }
            }
        })->pluck('accountcode')->toArray();

        $cylInvoice = CylinderInvoice::where(function ($query) use ($request, $dates_from, $dates_to, $debtors) {
            if ($request->AC_Code_Chkbx == "on") {
                if ($request->AC_Code_to == $request->AC_Code_frm) {
                    $query->where('account_code', "=", $request->AC_Code_frm);
                } else {
                    $query->whereBetween('account_code', [$request->AC_Code_frm, $request->AC_Code_to]);
                }
            } else {
                $query->whereIn('account_code', $debtors);
            }

            $query->where('date', '>=', $dates_from)
                ->where('date', '<=', $dates_to)
                ->get();
        })->groupBy('name')->pluck('name');

        //return if selected debtor and date already being processed
        if (!$cylInvoice->isEmpty()) {

            $debtorList = implode(', ', $cylInvoice->toArray());
            return redirect()->back()->with('Error', "Selected debtor ($debtorList) already generated");
        }

        //get free period cylinder rental
        $systemSetup = SystemSetup::select('free_cy')->first();
        $freePeriod = isset($systemSetup->free_cy) ? $systemSetup->free_cy : 0;

        //find all debtor in cylinder delivery prepare list
        $debtorDeliveryNote = DeliveryNote::join('debtors', 'delivery_notes.account_code', '=', 'debtors.accountcode')
            ->where(function ($query) use ($request, $dates_to) {
                if ($request->AC_Code_Chkbx == "on") {
                    if ($request->AC_Code_to == $request->AC_Code_frm) {
                        $query->where('account_code', "=", $request->AC_Code_frm);
                    } else {
                        $query->whereBetween('account_code', [$request->AC_Code_frm, $request->AC_Code_to]);
                    }
                } else {
                    $query->where('debtors.custom2', '=', 'Auto Invoice Generation');
                    $query->where('debtors.customval2', '=', 'on');
                }

                $query->where('date', "<=", $dates_to);
            })->groupBy('account_code')
            ->get([
                'account_code',
                'delivery_notes.name',
                'delivery_notes.addr1',
                'delivery_notes.addr2',
                'delivery_notes.addr3',
                'delivery_notes.addr4',
                'delivery_notes.tel_no',
                'delivery_notes.fax_no',
                'debtors.customval3',
                'debtors.cterm'
            ]);

        foreach ($debtorDeliveryNote as $DN) {

            //check seperate or not
            if ($DN->customval3 == "on") {
                $deliveryNote = DeliveryNote::where('account_code', "=", $DN->account_code)->get('dn_no');

                foreach ($deliveryNote as $delNotes) {
                    // CYLINDER RENTALL PROCESS START \\
                    $runningNumber = DocumentSetup::findByName('Cylinder Rental Invoice');
                    $cylInvDoc = $runningNumber->nextRunningNoString;

                    $CylinderDebtor = CylinderInvProcessing::where('dn_no', "=", $delNotes->dn_no)
                        ->where('type', '=', 'cy')
                        ->where(function ($query) use ($request,  $dates_from, $dates_to) {
                            $query->whereRaw("return_note IS NULL OR (return_note_date <= '$dates_to' AND return_note_date >= '$dates_from')");
                        })->get();
                    if (!$CylinderDebtor->isEmpty()) {
                        $cylinderGenerated[] = $this->processingInvoice($DN, $CylinderDebtor,  $dates_from, $dates_to, $cylInvDoc, $freePeriod);
                    }
                    // CYLINDER RENTALL PROCESS FINISH \\

                    // GASRACK RENTAL PROCESS START \\
                    $runningNumber = DocumentSetup::findByName('Rack Rental Invoice');
                    //find each gasrack before date_to or return_note empty and
                    $RackDebtor = GasrackInvProcessing::where('dn_no', "=", $delNotes->dn_no)
                        ->where('type', '=', 'gr')
                        ->where(function ($query) use ($request,  $dates_from, $dates_to) {
                            $query->whereRaw("return_note IS NULL OR (return_note_date <= '$dates_to' AND return_note_date >= '$dates_from')");
                        })->get();
                    //create invoice each gasrack
                    if (!$RackDebtor->isEmpty()) {
                        $gasrackGenerated[] = $this->processingGasrackInvoice($DN, $RackDebtor,  $dates_from, $dates_to, $runningNumber, $freePeriod);
                    }
                    // GASRACK RENTAL PROCESS FINISH \\
                }
            } else {
                // CYLINDER RENTALL PROCESS START \\
                $runningNumber = DocumentSetup::findByName('Cylinder Rental Invoice');
                $cylInvDoc = $runningNumber->nextRunningNoString;

                //find each cylinder before date_to or return_note empty and
                $CylinderDebtor = CylinderInvProcessing::where('debtor', "=", $DN->account_code)
                    ->where('type', '=', 'cy')
                    ->where(function ($query) use ($request,  $dates_from, $dates_to) {
                        $query->whereRaw("return_note IS NULL OR (return_note_date <= '$dates_to' AND return_note_date >= '$dates_from')");
                    })->get();
                //create invoice cylinder
                if (!$CylinderDebtor->isEmpty()) {

                    $cylinderGenerated[] = $this->processingInvoice($DN, $CylinderDebtor,  $dates_from, $dates_to, $cylInvDoc, $freePeriod);
                }
                // CYLINDER RENTALL PROCESS FINISH \\


                // GASRACK RENTAL PROCESS START \\
                $runningNumber = DocumentSetup::findByName('Rack Rental Invoice');
                //find each gasrack before date_to or return_note empty and
                $RackDebtor = GasrackInvProcessing::where('debtor', "=", $DN->account_code)
                    ->where('type', '=', 'gr')
                    ->where(function ($query) use ($request,  $dates_from, $dates_to) {
                        $query->whereRaw("return_note IS NULL OR (return_note_date <= '$dates_to' AND return_note_date >= '$dates_from')");
                    })->get();
                //create invoice each gasrack
                if (!$RackDebtor->isEmpty()) {
                    $gasrackGenerated[] = $this->processingGasrackInvoice($DN, $RackDebtor,  $dates_from, $dates_to,  $runningNumber, $freePeriod);
                }
                // GASRACK RENTAL PROCESS FINISH \\
            }
        }

        if (in_array(true,  $cylinderGenerated) || in_array(true,  $gasrackGenerated)) {
            return redirect()->route('cylinderinvoices.index')->with('Success', 'Invoice added successfully');
        } else {
            return redirect()->route('cylinderinvoices.index')->with('Error', 'No Invoice Added');
        }
    }


    public function processingInvoice($DN, $Cylinders, $dates_from, $dates_to, $DocNo, $freePeriod)
    {
        $amount = 0;
        $sequence = 1;

        foreach ($Cylinders as $cyl) {
            $isBilled = false;
            $startDate = $dates_from > $cyl->bill_date ? $dates_from : $cyl->bill_date;
            $endDate =  $cyl->return_note_date != null ? $cyl->return_note_date : $dates_to;

            // checked free period, also checked if already finish free period
            if ($freePeriod > 0) {
                $endFreeDate = date('Y-m-d', strtotime($cyl->bill_date . " + $freePeriod days"));

                if ($endFreeDate <= $endDate) {
                    $isBilled = true;
                    $startCharging = $endFreeDate;
                    $startDate = $startDate > $startCharging ? $startDate : $startCharging;
                }
            } else {
                $isBilled = true;
            }

            if ($isBilled) {

                $datediff = strtotime($endDate) - strtotime($startDate);
                $TotalDays = round($datediff / (60 * 60 * 24));

                //get how many days in the month of start date
                $year = date("Y",  strtotime($startDate));
                $month = date("m",  strtotime($startDate));
                $endMonthDay = cal_days_in_month(CAL_GREGORIAN, $month,  $year);
                $monthDifference = strtotime("$year-$month-$endMonthDay") - strtotime("$year-$month-1");

                //check whether use monthly or daily rate
                $rateType = $TotalDays < round($monthDifference / (60 * 60 * 24)) ? "daily" : "monthly";

                switch ($rateType) {
                    case "daily":
                        $totalRate = $cyl->daily_price * $TotalDays;
                        break;
                    case "monthly":
                        $totalRate = $cyl->monthly_price;
                        break;
                }

                $totalRate = $totalRate > $cyl->monthly_price ? $cyl->monthly_price : $totalRate;

                $dt = CylinderInvoicedt::create([
                    'doc_no' => $DocNo,
                    'sequence_no' => $sequence,
                    'account_code' =>  $DN->account_code,
                    'serial' =>  $cyl->serial,
                    'barcode' => $cyl->barcode,
                    'subject' => $cyl->descr,
                    'details' => "Refer D.N. " . $cyl->dn_no,
                    'product' => $cyl->product,
                    'type' => "cy",
                    'quantity' => $rateType == "monthly" ? 1 : $TotalDays,
                    'uom' => $rateType == "monthly" ? "MONTH" : "DAYS",
                    'unit_price' =>  $rateType == "monthly" ? $cyl->monthly_price : $cyl->daily_price,
                    // 'discount' => $request['discount'][$i],
                    'amount' =>  $totalRate,
                    // 'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'updated_by' => Auth::user()->id
                ]);

                $amount = $amount + $totalRate;
                $sequence++;
            }
        }


        if ($amount > 0) {
            $invoice = CylinderInvoice::create([
                'docno' => $DocNo,
                'date' => $dates_to,
                // 'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                // 'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                // 'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' =>  $amount,
                // 'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' =>  $amount,
                'account_code' => $DN->account_code,
                'name' => $DN->name,
                'addr1' => $DN->addr1,
                'addr2' => $DN->addr2,
                'addr3' => $DN->addr3,
                'addr4' => $DN->addr4,
                'tel_no' => $DN->tel_no,
                'fax_no' => $DN->fax_no,
                'credit_term' => $DN->cterm,
                // 'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => "Late payment charges at 1.5% P.M after due date",
                // 'summary' => !empty($request['summary']) ? $request['summary'] : '',
                // 'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' =>  0.00,
                'rounding' =>  0.00,
                'created_by' => Auth::user()->id
            ]);

            $invoiceDoc = DocumentSetup::findByName('Cylinder Rental Invoice');
            $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);  # code...
        }

        return $amount > 0;
    }

    public function processingGasrackInvoice($DN, $Gasrack, $dates_from, $dates_to,  $runningNumber, $freePeriod)
    {
        $isBilled = false;
        $sequence = 1;
        $dataArray = array();
        $is15th = false;
        $is30th = false;
        $invYear = date("Y",  strtotime($dates_from));
        $invMonth = date("m",  strtotime($dates_from));

        foreach ($Gasrack as $gr) {
            $startDate = date('Y-m-d', strtotime($gr->bill_date . " + 30 days"));
            $endDate =  $gr->return_note_date != null ? $gr->return_note_date : $dates_to;
            $isRentEnded = $gr->return_note_date != null ? true : false;

            if ($startDate < $endDate) {
                $year = date("Y",  strtotime($endDate));
                $month = date("m",  strtotime($endDate));

                if (!$isRentEnded) {
                    $monthStartInv = Carbon::parse($endDate)->startOfMonth();
                    $monthStartDN = Carbon::parse($startDate)->startOfMonth();

                    while ($monthStartDN !=  $monthStartInv) {
                        $startDate = date('Y-m-d', strtotime($startDate  . " + 30 days"));
                        $monthStartDN = Carbon::parse($startDate)->startOfMonth();
                    }

                    if ($startDate <= date("$year-$month-15")) {
                        //15TH BILLING
                        $is15th = true;
                        $isBilled = true;
                        $objectJSON = [];
                        $objectJSON['sequence_no'] = $sequence;
                        $objectJSON['serial'] = $gr->serial;
                        $objectJSON['barcode'] = $gr->barcode;
                        $objectJSON['subject'] = $gr->descr;
                        $objectJSON['details'] = "Refer D.N. " . $gr->dn_no . " <br> " . date('d/m/Y', strtotime($startDate  . " - 30 days")) . " to " .  date("d/m/Y", strtotime($startDate));
                        $objectJSON['quantity'] = "1";
                        $objectJSON['uom'] = "MONTH";
                        $objectJSON['unit_price'] =  $gr->monthly_price;
                        $objectJSON['amount'] =  $gr->monthly_price;
                        $objectJSON['billing'] =  "15th";
                        $dataArray[] = collect($objectJSON);
                    } else {
                        //3OTH BILLING
                        $is30th = true;
                        $isBilled = true;
                        $objectJSON = [];
                        $objectJSON['sequence_no'] = $sequence;
                        $objectJSON['serial'] = $gr->serial;
                        $objectJSON['barcode'] = $gr->barcode;
                        $objectJSON['subject'] = $gr->descr;
                        $objectJSON['details'] = "Refer D.N. " . $gr->dn_no . " <br> " . date('d/m/Y', strtotime($startDate  . " - 30 days")) . " to " .  date("d/m/Y", strtotime($startDate));
                        $objectJSON['quantity'] = "1";
                        $objectJSON['uom'] = "MONTH";
                        $objectJSON['unit_price'] =  $gr->monthly_price;
                        $objectJSON['amount'] =  $gr->monthly_price;
                        $objectJSON['billing'] =  "30th";
                        $dataArray[] = collect($objectJSON);
                    }
                } else {
                    $monthStartInv = Carbon::parse($endDate)->startOfMonth();
                    $monthStartDN = Carbon::parse($startDate)->startOfMonth();

                    while ($monthStartDN !=  $monthStartInv) {
                        $startDate = date('Y-m-d', strtotime($startDate  . " + 30 days"));
                        $monthStartDN = Carbon::parse($startDate)->startOfMonth();
                    }


                    if ($startDate <= date("$year-$month-15")) {
                        //15TH BILLING
                        $is15th = true;
                        $isBilled = true;
                        $objectJSON = [];
                        $objectJSON['sequence_no'] = $sequence;
                        $objectJSON['serial'] = $gr->serial;
                        $objectJSON['barcode'] = $gr->barcode;
                        $objectJSON['subject'] = $gr->descr;
                        $objectJSON['details'] = "Refer D.N. " . $gr->dn_no . " <br> " . date('d/m/Y', strtotime($startDate  . " - 30 days")) . " to " .  date("d/m/Y", strtotime($startDate));
                        $objectJSON['quantity'] = "1";
                        $objectJSON['uom'] = "MONTH";
                        $objectJSON['unit_price'] =  $gr->monthly_price;
                        $objectJSON['amount'] =  $gr->monthly_price;
                        $objectJSON['billing'] =  "15th";
                        $dataArray[] = collect($objectJSON);
                    } else {
                        //3OTH BILLING
                        $is30th = true;
                        $isBilled = true;
                        $objectJSON = [];
                        $objectJSON['sequence_no'] = $sequence;
                        $objectJSON['serial'] = $gr->serial;
                        $objectJSON['barcode'] = $gr->barcode;
                        $objectJSON['subject'] = $gr->descr;
                        $objectJSON['details'] = "Refer D.N. " . $gr->dn_no . " <br> " . date('d/m/Y', strtotime($startDate  . " - 30 days")) . " to " .  date("d/m/Y", strtotime($startDate));
                        $objectJSON['quantity'] = "1";
                        $objectJSON['uom'] = "MONTH";
                        $objectJSON['unit_price'] =  $gr->monthly_price;
                        $objectJSON['amount'] =  $gr->monthly_price;
                        $objectJSON['billing'] =  "30th";
                        $dataArray[] = collect($objectJSON);
                    }

                    // balance rental day that need to be charged using daily rate
                    $startDate =  date('Y-m-d', strtotime($startDate  . " + 1 days"));

                    $datediff = strtotime($endDate) - strtotime($startDate);
                    $TotalDays = round($datediff / (60 * 60 * 24));

                    if ($endDate <= date("$year-$month-15")) {
                        //15TH BILLING
                        $is15th = true;
                        $isBilled = true;
                        $objectJSON = [];
                        $objectJSON['sequence_no'] = $sequence;
                        $objectJSON['serial'] = $gr->serial;
                        $objectJSON['barcode'] = $gr->barcode;
                        $objectJSON['subject'] = $gr->descr;
                        $objectJSON['details'] = "Refer D.N. " . $gr->dn_no . " <br> " . date("d/m/Y", strtotime($startDate)) . " to " .  date("d/m/Y", strtotime($endDate));
                        $objectJSON['quantity'] =  $TotalDays;
                        $objectJSON['uom'] = "DAYS";
                        $objectJSON['unit_price'] =  $gr->daily_price;
                        $objectJSON['amount'] =   $TotalDays * $gr->daily_price;
                        $objectJSON['billing'] =  "15th";
                        $dataArray[] = collect($objectJSON);
                    } else {
                        //3OTH BILLING
                        $is30th = true;
                        $isBilled = true;
                        $objectJSON = [];
                        $objectJSON['sequence_no'] = $sequence;
                        $objectJSON['serial'] = $gr->serial;
                        $objectJSON['barcode'] = $gr->barcode;
                        $objectJSON['subject'] = $gr->descr;
                        $objectJSON['details'] = "Refer D.N. " . $gr->dn_no . " <br> " . date("d/m/Y", strtotime($startDate)) . " to " .  date("d/m/Y", strtotime($endDate));
                        $objectJSON['quantity'] =  $TotalDays;
                        $objectJSON['uom'] = "DAYS";
                        $objectJSON['unit_price'] =  $gr->daily_price;
                        $objectJSON['amount'] =  $TotalDays * $gr->daily_price;
                        $objectJSON['billing'] =  "30th";
                        $dataArray[] = collect($objectJSON);
                    }
                }
            }
        }

        $isTwoBilling = $is15th == true && $is30th == true ? true : false;
        if ($isBilled) {
            if ($isTwoBilling) {

                //insert 15th billing first
                $InvDoc = $runningNumber->nextRunningNoString;
                $totAmount = 0;
                $sequence = 1;
                foreach ($dataArray as $rack) {
                    if ($rack['billing'] == "15th") {
                        $dt = CylinderInvoicedt::create([
                            'doc_no' => $InvDoc,
                            'sequence_no' => $sequence,
                            'account_code' =>  $DN->account_code,
                            'serial' =>  $rack['serial'],
                            'barcode' => $rack['barcode'],
                            'subject' => "Rack No. " . $rack['serial'],
                            'details' => $rack['details'],
                            'type' => "gr",
                            'quantity' =>  $rack['quantity'],
                            'uom' =>  $rack['uom'],
                            'unit_price' =>   $rack['unit_price'],
                            // // 'discount' => $request['discount'][$i],
                            'amount' =>  $rack['amount'],
                            // 'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                        $sequence++;
                        $totAmount = $totAmount + $rack['amount'];
                    }
                }
                $invoice = CylinderInvoice::create([
                    'docno' => $InvDoc,
                    'date' =>   date("$invYear-$invMonth-15"),
                    // 'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                    // 'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                    // 'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                    'amount' =>  $totAmount,
                    // 'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                    'taxed_amount' => $totAmount,
                    'account_code' => $DN->account_code,
                    'name' => $DN->name,
                    'addr1' => $DN->addr1,
                    'addr2' => $DN->addr2,
                    'addr3' => $DN->addr3,
                    'addr4' => $DN->addr4,
                    'tel_no' => $DN->tel_no,
                    'fax_no' => $DN->fax_no,
                    'credit_term' => $DN->cterm,
                    // 'header' => !empty($request['header']) ? $request['header'] : '',
                    'footer' => "Late payment charges at 1.5% P.M after due date",
                    // 'summary' => !empty($request['summary']) ? $request['summary'] : '',
                    // 'currency' => !empty($request['currency']) ? $request['currency'] : '',
                    'exchange_rate' =>  0.00,
                    'rounding' =>  0.00,
                    'created_by' => Auth::user()->id
                ]);
                $invoiceDoc = DocumentSetup::findByName('Rack Rental Invoice');
                $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);  # code...

                //insert 30th billing
                $alphabet = preg_replace("/[0-9]+/", "", $runningNumber->nextRunningNoString);
                $digit = preg_replace("/[^0-9]+/", "", $runningNumber->nextRunningNoString);
                $lengDig = strlen($digit);
                $nextDig = (int) $digit + 1;
                $nextDigStr = str_pad($nextDig, $lengDig, '0', STR_PAD_LEFT);
                $docno = $alphabet . $nextDigStr;
                $InvDoc = $docno;
                $totAmount = 0;
                $sequence = 1;
                foreach ($dataArray as $rack) {
                    if ($rack['billing'] == "30th") {
                        $dt = CylinderInvoicedt::create([
                            'doc_no' => $InvDoc,
                            'sequence_no' => $sequence,
                            'account_code' =>  $DN->account_code,
                            'serial' =>  $rack['serial'],
                            'barcode' => $rack['barcode'],
                            'subject' => "Rack No. " . $rack['serial'],
                            'details' => $rack['details'],
                            'type' => "gr",
                            'quantity' =>  $rack['quantity'],
                            'uom' =>  $rack['uom'],
                            'unit_price' =>   $rack['unit_price'],
                            // // 'discount' => $request['discount'][$i],
                            'amount' =>  $rack['amount'],
                            // 'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                        $sequence++;
                        $totAmount = $totAmount + $rack['amount'];
                    }
                }
                $invoice = CylinderInvoice::create([
                    'docno' => $InvDoc,
                    'date' =>   date("$invYear-$invMonth-30"),
                    // 'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                    // 'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                    // 'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                    'amount' =>  $totAmount,
                    // 'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                    'taxed_amount' => $totAmount,
                    'account_code' => $DN->account_code,
                    'name' => $DN->name,
                    'addr1' => $DN->addr1,
                    'addr2' => $DN->addr2,
                    'addr3' => $DN->addr3,
                    'addr4' => $DN->addr4,
                    'tel_no' => $DN->tel_no,
                    'fax_no' => $DN->fax_no,
                    'credit_term' => $DN->cterm,
                    // 'header' => !empty($request['header']) ? $request['header'] : '',
                    'footer' => "Late payment charges at 1.5% P.M after due date",
                    // 'summary' => !empty($request['summary']) ? $request['summary'] : '',
                    // 'currency' => !empty($request['currency']) ? $request['currency'] : '',
                    'exchange_rate' =>  0.00,
                    'rounding' =>  0.00,
                    'created_by' => Auth::user()->id
                ]);
                $invoiceDoc = DocumentSetup::findByName('Rack Rental Invoice');
                $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);  # code...


            } else {
                $InvDoc = $runningNumber->nextRunningNoString;
                $billingIn = $is15th  == true ? "15th" : "30th";
                $totAmount = 0;
                $sequence = 1;
                foreach ($dataArray as $rack) {
                    $dt = CylinderInvoicedt::create([
                        'doc_no' => $InvDoc,
                        'sequence_no' => $sequence,
                        'account_code' =>  $DN->account_code,
                        'serial' =>  $rack['serial'],
                        'barcode' => $rack['barcode'],
                        'subject' => "Rack No. " . $rack['serial'],
                        'details' => $rack['details'],
                        'type' => "gr",
                        'quantity' =>  $rack['quantity'],
                        'uom' =>  $rack['uom'],
                        'unit_price' =>   $rack['unit_price'],
                        // // 'discount' => $request['discount'][$i],
                        'amount' =>  $rack['amount'],
                        // 'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                        'updated_by' => Auth::user()->id
                    ]);
                    $sequence++;
                    $totAmount = $totAmount + $rack['amount'];
                }
                $invoice = CylinderInvoice::create([
                    'docno' => $InvDoc,
                    'date' =>  $billingIn == "15th" ? date("$invYear-$invMonth-15") : date("$invYear-$invMonth-30"),
                    // 'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                    // 'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                    // 'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                    'amount' =>  $totAmount,
                    // 'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                    'taxed_amount' => $totAmount,
                    'account_code' => $DN->account_code,
                    'name' => $DN->name,
                    'addr1' => $DN->addr1,
                    'addr2' => $DN->addr2,
                    'addr3' => $DN->addr3,
                    'addr4' => $DN->addr4,
                    'tel_no' => $DN->tel_no,
                    'fax_no' => $DN->fax_no,
                    'credit_term' => $DN->cterm,
                    // 'header' => !empty($request['header']) ? $request['header'] : '',
                    'footer' => "Late payment charges at 1.5% P.M after due date",
                    // 'summary' => !empty($request['summary']) ? $request['summary'] : '',
                    // 'currency' => !empty($request['currency']) ? $request['currency'] : '',
                    'exchange_rate' =>  0.00,
                    'rounding' =>  0.00,
                    'created_by' => Auth::user()->id
                ]);
                $invoiceDoc = DocumentSetup::findByName('Rack Rental Invoice');
                $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);  # code...

            }
        }

        return $isBilled;
    }

    public function printByBatch($Invoice)
    {
        $systemSetup = SystemSetup::first(['bank_detail', 'bank_account_no']);

        $dataArray = array();
        foreach ($Invoice as $inv) {
            $objectJSON = [];
            $objectJSON['invoice'] = $inv;
            $objectJSON['invdata'] = CylinderInvoicedt::where("doc_no", "=", $inv->docno)
                ->get();


            $dataArray[] = collect($objectJSON);
        }
        $pdf = PDF::loadView('dailypro.cylinderinvoices.invoices-pdf-batch', ['Invoice' => $dataArray, 'systemSetup' => $systemSetup])->setPaper('A4');

        // $data["Invoice"] = $dataArray;
        // $data["systemSetup"] = $systemSetup;
        // return view('dailypro.cylinderinvoices.invoices-pdf-batch', $data);
        return $pdf->inline();
    }
}
