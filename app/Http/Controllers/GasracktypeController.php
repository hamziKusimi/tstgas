<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Gasracktype;
use Auth;

class GasracktypeController extends Controller
{
    public function index(){

        $data["gasracktypes"] = Gasracktype::get();
        $data["page_title"] = "Gas Rack Type Item Listings";
        $data["bclvl1"] = "Gas Rack Type Item Listings";
        $data["bclvl1_url"] = route('gasracktypes.index');

        return view('cylindermaster.gasracktypes.index', $data);
    }

    public function create(){
        
        $data["page_title"] = "Add Gas Rack Type";
        $data["bclvl1"] = "Gas Rack Type Item Listings";
        $data["bclvl1_url"] = route('gasracktypes.index');
        $data["bclvl2"] = "Add Gas Rack Type";
        $data["bclvl2_url"] = route('gasracktypes.create');

        return view('cylindermaster.gasracktypes.create', $data);
    }

    public function store(Request $request)
    {
        $gasracktype = Gasracktype::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('gasracktypes.index')->with('Success', 'Gas Rack Type created successfully.');

    }

    public function edit($id){

        $data["gasracktype"] = Gasracktype::find($id);
        $data["page_title"] = "Edit Gas Rack Type";
        $data["bclvl1"] = "Gas Rack Type Item Listings";
        $data["bclvl1_url"] = route('gasracktypes.index');
        $data["bclvl2"] = "Edit Gas Rack Type";
        $data["bclvl2_url"] = route('gasracktypes.edit', ['id'=>$id]);

        return view('cylindermaster.gasracktypes.edit', $data);
    }

    public function update(Request $request, $id)
    {
        
        $gasracktype = Gasracktype::find($id);
        if (isset($gasracktype)) {
            $gasracktype->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('gasracktypes.index')->with('Success', 'Gas Rack Type updated successfully.');
    }

    public function destroy(gasracktype $gasracktype)
    {
        $gasracktype->update([
            'deleted_by' => Auth::user()->id
        ]);
        $gasracktype->delete();
        return redirect()->route('gasracktypes.index')->with('Success', 'Gas Rack Type deleted successfully.');
    }
}
