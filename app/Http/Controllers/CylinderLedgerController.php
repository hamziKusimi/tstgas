<?php

namespace App\Http\Controllers;

use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\View\CylinderMovement;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;
use App\Model\DeliveryNotepr;

class CylinderLedgerController extends Controller
{
    public function print(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        //get balance
        // $result = DB::select(DB::raw("
        // SELECT SUM(balance) as balance FROM
        // (
        //     SELECT a.serial, b.`dn_no`, b.`datetime`,return_note,
        //         CASE
        //             WHEN return_note IS NULL AND  b.`dn_no` IS NULL
        //             THEN 0
        //             WHEN return_note IS NULL AND  b.`dn_no` IS NOT NULL
        //             THEN 1
        //             WHEN return_note IS NOT NULL
        //             THEN 0
        //         END AS balance
        //     FROM cylinders a
        //     LEFT JOIN delivery_noteprs b ON  b.`datetime`= (
        //         SELECT b1.`datetime` FROM delivery_noteprs b1
        //         WHERE b1.`datetime` < '$dates_from'  AND b1.`serial` = b.`serial` AND b.`serial` = a.`serial`
        //         ORDER BY b1.`datetime` DESC
        //         LIMIT 1
        //         ) AND b.`deleted_at` IS NULL
        //     LEFT JOIN delivery_notes c ON b.dn_no = c.dn_no
        //     WHERE a.deleted_at IS NULL
        //     AND b.type = 'cy'
        //     $condition_1
        //     $condition_2
        // ) a
        // "), []);

        $result = DeliveryNotepr::join('delivery_notes', 'delivery_noteprs.dn_no', '=', 'delivery_notes.dn_no')
                        ->selectRaw('count(delivery_noteprs.dn_no) AS count')
                        ->whereNull('return_note')
                        ->where('type','=','cy')
                        ->where('datetime','<',$dates_from)
                        ->where(function ($query) use ($request) {
                            if($request->debCode_Chkbx == "on"){
                                if ($request->debCode_frm == $request->debCode_to) {
                                    $query->Where('delivery_notes.account_code','=',$request->debCode_frm);
                                } else {
                                    $query->whereBetween('delivery_notes.account_code', [$request->debCode_frm, $request->debCode_to]);
                                }
                            }
                        })
                        ->first();

        $balance = $result->count;

        $CylLedger = $this->getCylLedgerQuery($request, $dates_from, $dates_to);

        if (empty($CylLedger)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        $cylJSON = $this->getCylLedgerJSON($CylLedger, $dates_from, $balance);

        // dd($cylJSON);
        $JsonFileName = "CylinderLedger" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/CylinderLedger/' . $JsonFileName . '.json'), $cylJSON);
        $JasperFileName = "CylinderLedger" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to)
    {
        $input = base_path() . '/resources/reporting/CylinderLedger/CylinderLedger.jrxml';
        $output = base_path() . '/resources/reporting/CylinderLedger/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/CylinderLedger/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date_from" => date('d/m/Y', strtotime($dates_from)),
                "date_to" => date('d/m/Y', strtotime($dates_to))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/CylinderLedger/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getCylLedgerQuery($request, $dates_from, $dates_to)
    {

        if($request->debCode_Chkbx == "on"){
            $condition_2 = "AND debtor BETWEEN '$request->debCode_frm' AND '$request->debCode_to' ";
        }else{
            $condition_2 = "";
        }
        return DB::select(DB::raw("
        SELECT cdate, doc_type, doc_no, s_no, category,
        CASE
            WHEN doc_type = 'DN'
            THEN 1
            WHEN doc_type = 'RN'
            THEN 0
        END AS `in`,
        CASE
            WHEN doc_type = 'DN'
            THEN 0
            WHEN doc_type = 'RN'
            THEN 1
        END AS `out`
        FROM view_cylinder_movement
        WHERE doc_type IN('DN','RN') AND cdate >='$dates_from' AND cdate <= '$dates_to'
        $condition_2
        ORDER BY cdate, doc_no
        "), []);
    }

    public function getCylLedgerJSON($CylLedger,  $dates_from, $balance)
    {
        $balance =  $balance == null ? 0 : $balance;
        $dataArray = [];
        $s_n = 1;

        $objectJSON =   [];
        $objectJSON['s_n'] = $s_n;
        $objectJSON['date'] = date('d/m/Y', strtotime( $dates_from));
        $objectJSON['doc_type'] = null;
        $objectJSON['doc_no'] = "Balance B/F";
        $objectJSON['cylinder_sn'] = null;
        $objectJSON['cyl_cat'] = null;
        $objectJSON['in'] = null;
        $objectJSON['out'] = null;
        $objectJSON['balance'] = $balance;
        $objectJSON['remark'] = null;

        $s_n =  $s_n + 1;
        $dataArray[] = collect($objectJSON);
        foreach ($CylLedger as $cyl) {
            $objectJSON =   [];

            $objectJSON['s_n'] = $s_n;
            $objectJSON['date'] = date('d/m/Y', strtotime( $cyl->cdate));
            $objectJSON['doc_type'] = $cyl->doc_type;
            $objectJSON['doc_no'] =  $cyl->doc_no;
            $objectJSON['cylinder_sn'] = $cyl->s_no;
            $objectJSON['cyl_cat'] = $cyl->category;
            $objectJSON['in'] =  $cyl->in;
            $objectJSON['out'] =$cyl->out;
            $balance = $cyl->in == 1 ? $balance + 1 : $balance - 1;
            $objectJSON['balance'] = $balance;
            $objectJSON['remark'] = "";

            $s_n =  $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }

}