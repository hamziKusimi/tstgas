<?php

namespace App\Http\Controllers;
use App\Model\Porder;
use App\Model\Invoice;
use App\Model\Cashbill;
use App\Model\Deliveryorder;
use App\Model\Salesreturn;
use DateTime;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $content)
    {
        $data = [];

        $totalpurchase = 0;
        $totalinvoice = 0;
        $totalcashbill = 0;
        $totaldo = 0;
        // $totalsalesreturn = 0;
        $totalsalesreturn1 = 0;
        $totalsalesreturn2 = 0;
        $totalsalesreturn3 = 0;
        $totalsalesreturn4 = 0;
        $totalsalesbyweek = 0;

        $porders = Porder::pluck('taxed_amount');
        foreach($porders as $porder){
            $totalpurchase = $totalpurchase + $porder;
        }
        
        $invoices = Invoice::pluck('taxed_amount');
        foreach($invoices as $invoice){
            $totalinvoice = $totalinvoice + $invoice;
        }

        $cashbills = Cashbill::pluck('taxed_amount');
        foreach($cashbills as $cashbill){
            $totalcashbill = $totalcashbill + $cashbill;
        }

        $dos = Deliveryorder::pluck('taxed_amount');
        foreach($dos as $do){
            $totaldo = $totaldo + $do;
        }

        $salesreturns1 = Salesreturn::whereBetween('date', [today()->addDays(-7), today()])->where('deleted_at', null)->pluck('taxed_amount');
        foreach($salesreturns1 as $salesreturn1){
            $totalsalesreturn1 = $totalsalesreturn1 + $salesreturn1;
        }

        $salesreturns2 = Salesreturn::whereBetween('date', [today()->addDays(-15), today()->addDays(-8)])->where('deleted_at', null)->pluck('taxed_amount');
        foreach($salesreturns2 as $salesreturn2){
            $totalsalesreturn2 = $totalsalesreturn2 + $salesreturn2;
        }
        
        $salesreturns3 = Salesreturn::whereBetween('date', [today()->addDays(-22), today()->addDays(-16)])->where('deleted_at', null)->pluck('taxed_amount');
        foreach($salesreturns3 as $salesreturn3){
            $totalsalesreturn3 = $totalsalesreturn3 + $salesreturn3;
        }

        $salesreturns4 = Salesreturn::whereBetween('date', [today()->addDays(-29), today()->addDays(-23)])->where('deleted_at', null)->pluck('taxed_amount');
        foreach($salesreturns4 as $salesreturn4){
            $totalsalesreturn4 = $totalsalesreturn4 + $salesreturn4;
        }

        $totalsalesreturn = [];
        // $totalsalesreturn = json_encode(array_unique(array_merge($totalsalesreturn1, $totalsalesreturn2, $totalsalesreturn3, $totalsalesreturn4)));
        array_push($totalsalesreturn, $totalsalesreturn1, $totalsalesreturn2, $totalsalesreturn3, $totalsalesreturn4);
        $data["page_title"] = "Dashboard";
        $data["totalpurchase"] = $totalpurchase;
        $data["totalinvoice"] = $totalinvoice;
        $data["totalcashbill"] = $totalcashbill;
        $data["totaldo"] = $totaldo;
        $data["salesreturns"] = $totalsalesreturn1;
        // dd($today);
        
        
        // return $content
        // ->header('Chartjs')
        // ->body(new Box('Bar chart', view('home')));
        // return response()-json($today);

        $invoices1 = Invoice::where('date', today()->addDays(-2))->where('deleted_at', null)->pluck('taxed_amount');
        // dd($invoices1);

        return view('home', $data);

        
    }

    public function getWeeklySales()
    {
        $totalinvoice1 = 0; $totalinvoice2 = 0;$totalinvoice3 = 0;$totalinvoice4 = 0;
        $totalinvoice5 = 0;$totalinvoice6 = 0;$totalinvoice7 = 0;

        $totalcashbill1 = 0; $totalcashbill2 = 0;$totalcashbill3 = 0;$totalcashbill4 = 0;
        $totalcashbill5 = 0;$totalcashbill6 = 0;$totalcashbill7 = 0;

        $totalsalesreturn1 = 0; $totalsalesreturn2 = 0;$totalsalesreturn3 = 0;$totalsalesreturn4 = 0;
        $totalsalesreturn5 = 0;$totalsalesreturn6 = 0;$totalsalesreturn7 = 0;

        //Sales Receive

        //invoice

            $invoices1 = Invoice::where('date', today()->addDays(-1))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices1 as $invoice1){
                $totalinvoice1 = $totalinvoice1 + $invoice1;
            }

            $invoices2 = Invoice::where('date', today()->addDays(-2))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices2 as $invoice2){
                $totalinvoice2 = $totalinvoice2 + $invoice2;
            }

            $invoices3 = Invoice::where('date', today()->addDays(-3))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices3 as $invoice3){
                $totalinvoice3 = $totalinvoice3 + $invoice3;
            }

            $invoices4 = Invoice::where('date', today()->addDays(-3))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices4 as $invoice4){
                $totalinvoice4 = $totalinvoice4 + $invoice4;
            }

            $invoices5 = Invoice::where('date', today()->addDays(-5))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices5 as $invoice5){
                $totalinvoice5 = $totalinvoice5 + $invoice5;
            }

            $invoices6 = Invoice::where('date', today()->addDays(-6))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices6 as $invoice6){
                $totalinvoice6 = $totalinvoice6 + $invoice6;
            }

            $invoices7 = Invoice::where('date', today()->addDays(-7))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices7 as $invoice7){
                $totalinvoice7 = $totalinvoice7 + $invoice7;
            }

        //cashbill
          
            $cashbills1 = Cashbill::where('date', today()->addDays(-1))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills1 as $cashbill1){
                $totalcashbill1 = $totalcashbill1 + $cashbill1;
            }

            $cashbills2 = Cashbill::where('date', today()->addDays(-2))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills2 as $cashbill2){
                $totalcashbill2 = $totalcashbill2 + $cashbill2;
            }

            $cashbills3 = Cashbill::where('date', today()->addDays(-3))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills3 as $cashbill3){
                $totalcashbill3 = $totalcashbill3 + $cashbill3;
            }

            $cashbills4 = Cashbill::where('date', today()->addDays(-3))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills4 as $cashbill4){
                $totalcashbill4 = $totalcashbill4 + $cashbill4;
            }

            $cashbills5 = Cashbill::where('date', today()->addDays(-5))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills5 as $cashbill5){
                $totalcashbill5 = $totalcashbill5 + $cashbill5;
            }

            $cashbills6 = Cashbill::where('date', today()->addDays(-6))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills6 as $cashbill6){
                $totalcashbill6 = $totalcashbill6 + $cashbill6;
            }

            $cashbills7 = Cashbill::where('date', today()->addDays(-7))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills7 as $cashbill7){
                $totalcashbill7 = $totalcashbill7 + $cashbill7;
            }

        //Sales Return
            $salesreturns1 = Salesreturn::where('date', today()->addDays(-1))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns1 as $salesreturn1){
                $totalsalesreturn1 = $totalsalesreturn1 + $salesreturn1;
            }

            $salesreturns2 = Salesreturn::where('date', today()->addDays(-2))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns2 as $salesreturn2){
                $totalsalesreturn2 = $totalsalesreturn2 + $salesreturn2;
            }

            $salesreturns3 = Salesreturn::where('date', today()->addDays(-3))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns3 as $salesreturn3){
                $totalsalesreturn3 = $totalsalesreturn3 + $salesreturn3;
            }

            $salesreturns4 = Salesreturn::where('date', today()->addDays(-3))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns4 as $salesreturn4){
                $totalsalesreturn4 = $totalsalesreturn4 + $salesreturn4;
            }

            $salesreturns5 = Salesreturn::where('date', today()->addDays(-5))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns5 as $salesreturn5){
                $totalsalesreturn5 = $totalsalesreturn5 + $salesreturn5;
            }

            $salesreturns6 = Salesreturn::where('date', today()->addDays(-6))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns6 as $salesreturn6){
                $totalsalesreturn6 = $totalsalesreturn6 + $salesreturn6;
            }

            $salesreturns7 = Salesreturn::where('date', today()->addDays(-7))->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns7 as $salesreturn7){
                $totalsalesreturn7 = $totalsalesreturn7 + $salesreturn7;
            }

            $totalsales1 = $totalinvoice1 + $totalcashbill1 - $totalsalesreturn1;
            $totalsales2 = $totalinvoice2 + $totalcashbill2 - $totalsalesreturn2;
            $totalsales3 = $totalinvoice3 + $totalcashbill3 - $totalsalesreturn3;
            $totalsales4 = $totalinvoice4 + $totalcashbill4 - $totalsalesreturn4;  
            $totalsales5 = $totalinvoice5 + $totalcashbill5 - $totalsalesreturn5;
            $totalsales6 = $totalinvoice6 + $totalcashbill6 - $totalsalesreturn6;
            $totalsales7 = $totalinvoice7 + $totalcashbill7 - $totalsalesreturn7;

            // dd($totalsales1);
            $totalsalesweekly = [];
            array_push($totalsalesweekly, $totalsales1, $totalsales2, $totalsales3, $totalsales4, $totalsales5, $totalsales6, $totalsales7);
            return $totalsalesweekly;
    }

    public function getMonthlySales()
    {
        $totalinvoice1 = 0;$totalinvoice2 = 0;$totalinvoice3 = 0;$totalinvoice4 = 0;$totalinvoice5 = 0;$totalinvoice6 = 0;
        $totalinvoice7 = 0;$totalinvoice8 = 0;$totalinvoice9 = 0;$totalinvoice10 = 0;$totalinvoice11 = 0;$totalinvoice12 = 0;
        
        $totalcashbill1 = 0;$totalcashbill2 = 0;$totalcashbill3 = 0;$totalcashbill4 = 0;$totalcashbill5 = 0;$totalcashbill6 = 0;
        $totalcashbill7 = 0;$totalcashbill8 = 0;$totalcashbill9 = 0;$totalcashbill10 = 0;$totalcashbill11 = 0;$totalcashbill12 = 0;

        $totalsalesreturn1 = 0;$totalsalesreturn2 = 0;$totalsalesreturn3 = 0;$totalsalesreturn4 = 0;$totalsalesreturn5 = 0;$totalsalesreturn6 = 0;
        $totalsalesreturn7 = 0;$totalsalesreturn8 = 0;$totalsalesreturn9 = 0;$totalsalesreturn10 = 0;$totalsalesreturn11 = 0;$totalsalesreturn12 = 0;

        //Sales Receive

        //invoice

            $invoices1 = Invoice::whereBetween('date', ["2019-01-01", "2019-01-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices1 as $invoice1){
                $totalinvoice1 = $totalinvoice1 + $invoice1;
            }

            $invoices2 = Invoice::whereBetween('date',  ["2019-02-01", "2019-02-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices2 as $invoice2){
                $totalinvoice2 = $totalinvoice2 + $invoice2;
            }

            $invoices3 = Invoice::whereBetween('date', ["2019-03-01", "2019-03-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices3 as $invoice3){
                $totalinvoice3 = $totalinvoice3 + $invoice3;
            }

            $invoices4 = Invoice::whereBetween('date', ["2019-04-01", "2019-04-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices4 as $invoice4){
                $totalinvoice4 = $totalinvoice4 + $invoice4;
            }

            $invoices5 = Invoice::whereBetween('date', ["2019-05-01", "2019-05-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices5 as $invoice5){
                $totalinvoice5 = $totalinvoice5 + $invoice5;
            }

            $invoices6 = Invoice::whereBetween('date', ["2019-06-01", "2019-06-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices6 as $invoice6){
                $totalinvoice6 = $totalinvoice6 + $invoice6;
            }

            $invoices7 = Invoice::whereBetween('date', ["2019-07-01", "2019-07-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices7 as $invoice7){
                $totalinvoice7 = $totalinvoice7 + $invoice7;
            }

            $invoices8 = Invoice::whereBetween('date', ["2019-08-01", "2019-08-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices8 as $invoice8){
                $totalinvoice8 = $totalinvoice8 + $invoice8;
            }

            $invoices9 = Invoice::whereBetween('date', ["2019-09-01", "2019-09-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices9 as $invoice9){
                $totalinvoice9 = $totalinvoice9 + $invoice9;
            }
            
            $invoices10 = Invoice::whereBetween('date', ["2019-10-01", "2019-10-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices10 as $invoice10){
                $totalinvoice10 = $totalinvoice10 + $invoice10;
            }

            $invoices11 = Invoice::whereBetween('date', ["2019-11-01", "2019-11-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices11 as $invoice11){
                $totalinvoice11 = $totalinvoice11 + $invoice11;
            }
            
            $invoices12 = Invoice::whereBetween('date', ["2019-12-01", "2019-12-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($invoices12 as $invoice12){
                $totalinvoice12 = $totalinvoice12 + $invoice12;
            }

        //cashbill

            $cashbills1 = Invoice::whereBetween('date', ["2019-01-01", "2019-01-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills1 as $cashbill1){
                $totalcashbill1 = $totalcashbill1 + $cashbill1;
            }

            $cashbills2 = Invoice::whereBetween('date',  ["2019-02-01", "2019-02-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills2 as $cashbill2){
                $totalcashbill2 = $totalcashbill2 + $cashbill2;
            }

            $cashbills3 = Invoice::whereBetween('date', ["2019-03-01", "2019-03-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills3 as $cashbill3){
                $totalcashbill3 = $totalcashbill3 + $cashbill3;
            }

            $cashbills4 = Invoice::whereBetween('date', ["2019-04-01", "2019-04-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills4 as $cashbill4){
                $totalcashbill4 = $totalcashbill4 + $cashbill4;
            }

            $cashbills5 = Invoice::whereBetween('date', ["2019-05-01", "2019-05-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills5 as $cashbill5){
                $totalcashbill5 = $totalcashbill5 + $cashbill5;
            }

            $cashbills6 = Invoice::whereBetween('date', ["2019-06-01", "2019-06-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills6 as $cashbill6){
                $totalcashbill6 = $totalcashbill6 + $cashbill6;
            }

            $cashbills7 = Invoice::whereBetween('date', ["2019-07-01", "2019-07-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills7 as $cashbill7){
                $totalcashbill7 = $totalcashbill7 + $cashbill7;
            }

            $cashbills8 = Invoice::whereBetween('date', ["2019-08-01", "2019-08-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills8 as $cashbill8){
                $totalcashbill8 = $totalcashbill8 + $cashbill8;
            }

            $cashbills9 = Invoice::whereBetween('date', ["2019-09-01", "2019-09-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills9 as $cashbill9){
                $totalcashbill9 = $totalcashbill9 + $cashbill9;
            }
            
            $cashbills10 = Invoice::whereBetween('date', ["2019-10-01", "2019-10-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills10 as $cashbill10){
                $totalcashbill10 = $totalcashbill10 + $cashbill10;
            }

            $cashbills11 = Invoice::whereBetween('date', ["2019-11-01", "2019-11-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills11 as $cashbill11){
                $totalcashbill11 = $totalcashbill11 + $cashbill11;
            }
            
            $cashbills12 = Invoice::whereBetween('date', ["2019-12-01", "2019-12-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($cashbills12 as $cashbill12){
                $totalcashbill12 = $totalcashbill12 + $cashbill12;
            }

        //Sales Return 
        
            $salesreturns1 = Invoice::whereBetween('date', ["2019-01-01", "2019-01-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns1 as $salesreturn1){
                $totalsalesreturn1 = $totalsalesreturn1 + $salesreturn1;
            }

            $salesreturns2 = Invoice::whereBetween('date',  ["2019-02-01", "2019-02-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns2 as $salesreturn2){
                $totalsalesreturn2 = $totalsalesreturn2 + $salesreturn2;
            }

            $salesreturns3 = Invoice::whereBetween('date', ["2019-03-01", "2019-03-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns3 as $salesreturn3){
                $totalsalesreturn3 = $totalsalesreturn3 + $salesreturn3;
            }

            $salesreturns4 = Invoice::whereBetween('date', ["2019-04-01", "2019-04-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns4 as $salesreturn4){
                $totalsalesreturn4 = $totalsalesreturn4 + $salesreturn4;
            }

            $salesreturns5 = Invoice::whereBetween('date', ["2019-05-01", "2019-05-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns5 as $salesreturn5){
                $totalsalesreturn5 = $totalsalesreturn5 + $salesreturn5;
            }

            $salesreturns6 = Invoice::whereBetween('date', ["2019-06-01", "2019-06-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns6 as $salesreturn6){
                $totalsalesreturn6 = $totalsalesreturn6 + $salesreturn6;
            }

            $salesreturns7 = Invoice::whereBetween('date', ["2019-07-01", "2019-07-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns7 as $salesreturn7){
                $totalsalesreturn7 = $totalsalesreturn7 + $salesreturn7;
            }

            $salesreturns8 = Invoice::whereBetween('date', ["2019-08-01", "2019-08-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns8 as $salesreturn8){
                $totalsalesreturn8 = $totalsalesreturn8 + $salesreturn8;
            }

            $salesreturns9 = Invoice::whereBetween('date', ["2019-09-01", "2019-09-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns9 as $salesreturn9){
                $totalsalesreturn9 = $totalsalesreturn9 + $salesreturn9;
            }
            
            $salesreturns10 = Invoice::whereBetween('date', ["2019-10-01", "2019-10-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns10 as $salesreturn10){
                $totalsalesreturn10 = $totalsalesreturn10 + $salesreturn10;
            }

            $salesreturns11 = Invoice::whereBetween('date', ["2019-11-01", "2019-11-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns11 as $salesreturn11){
                $totalsalesreturn11 = $totalsalesreturn11 + $salesreturn11;
            }
            
            $salesreturns12 = Invoice::whereBetween('date', ["2019-12-01", "2019-12-31"])->where('deleted_at', null)->pluck('taxed_amount');
            foreach($salesreturns12 as $salesreturn12){
                $totalsalesreturn12 = $totalsalesreturn12 + $salesreturn12;
            }


        //totalsales

            $totalsales1 = $totalinvoice1 + $totalcashbill1 - $totalsalesreturn1;
            $totalsales2 = $totalinvoice2 + $totalcashbill2 - $totalsalesreturn2;
            $totalsales3 = $totalinvoice3 + $totalcashbill3 - $totalsalesreturn3;
            $totalsales4 = $totalinvoice4 + $totalcashbill4 - $totalsalesreturn4;
            $totalsales5 = $totalinvoice5 + $totalcashbill5 - $totalsalesreturn5;
            $totalsales6 = $totalinvoice6 + $totalcashbill6 - $totalsalesreturn6;
            $totalsales7 = $totalinvoice7 + $totalcashbill7 - $totalsalesreturn7;
            $totalsales8 = $totalinvoice8 + $totalcashbill8 - $totalsalesreturn8;
            $totalsales9 = $totalinvoice9 + $totalcashbill9 - $totalsalesreturn9;
            $totalsales10 = $totalinvoice10 + $totalcashbill10 - $totalsalesreturn10;
            $totalsales11 = $totalinvoice11 + $totalcashbill11 - $totalsalesreturn11;
            $totalsales12 = $totalinvoice12 + $totalcashbill12 - $totalsalesreturn12;

            $totalfinalsales = [];
            array_push($totalfinalsales, $totalsales1, $totalsales2, $totalsales3, $totalsales4, $totalsales5, $totalsales6, $totalsales7, $totalsales7, $totalsales8, $totalsales9, $totalsales10, $totalsales11, $totalsales12);
            return $totalfinalsales;
    }
}
