<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Model\SupplierEnquiry;
// use App\Model\Creditor;
// use App\Model\SystemSetup;
// use App\Model\Stockcode;
// use App\Model\Category;
// use App\Model\Product;
// use App\Model\Brand;
// use App\Model\Location;
// use App\Model\Uom;
// use App\Model\View\CustomerView;
// use Illuminate\Support\Facades\Storage;
use DB;
// use DataTables;
// use PDF;
// use Illuminate\Filesystem\Filesystem;
// use Auth;
use DateTime;
use App\Model\CustomObject\DatabaseConnection;
// use Carbon\Carbon;
use App\Traits\AccountPosting;

class AccountPostingController extends Controller
{
    use AccountPosting;

    public function index(Request $request)
    {
        $data["page_title"] = "Account Posting";
        $data["bclvl1"] = "Account Posting";
        $connection = DatabaseConnection::setConnection();
        if($connection->getDatabaseName())
        {
            $data["DBConnection"] = "true";
        }else{
            $data["DBConnection"] = "false";
        }

        return view('settings.accountposting.search', $data);
    }

    public function query(Request $request)
    {
        if(request()->ajax())
        {
            $box = $request->all();
            $myValue=  array();
            parse_str($box['formdata'], $myValue);
            $isGR = ' AND FALSE';
            $isPR = ' AND FALSE';
            $isCB = ' AND FALSE';
            $isINV = ' AND FALSE';
            $isSR = ' AND FALSE';

            if(isset($myValue['itemType'])){
                switch($myValue['itemType']){
                    case "gr" :
                        $isGR = ' AND TRUE';
                        break;
                    case "pr" :
                        $isPR = ' AND TRUE';
                        break;
                    case "cb" :
                        $isCB = ' AND TRUE';
                        break;
                    case "inv" :
                        $isINV = ' AND TRUE';
                        break;
                    case "sr" :
                        $isSR = ' AND TRUE';
                        break;
                }
            }else{
                $isGR = ' AND TRUE';
                $isPR = ' AND TRUE';
                $isCB = ' AND TRUE';
                $isINV = ' AND TRUE';
                $isSR = ' AND TRUE';
            }

            if(isset($myValue['docno'])){
                $docFrom = $myValue['docnofrm'];
                $docTo = $myValue['docnoto'];
                $docNo = " AND ( docno BETWEEN '$docFrom' AND '$docTo')  " ;
            }else{
                $docNo = " ";
            }

            if(isset($myValue['debtor'])){
                $debtorFrom = $myValue['debtorfrm'];
                $debtorTo = $myValue['debtorto'];
                $debtor = " AND (account_code BETWEEN '$debtorFrom' AND '$debtorTo')  ";
            }else{
                $debtor = " ";
            }

            if(isset($myValue['creditor'])){
                $creditorFrom = $myValue['creditorfrm'];
                $creditorTo = $myValue['creditorto'];
                $creditor = " AND (account_code BETWEEN '$creditorFrom' AND '$creditorTo')  ";
            }else{
                $creditor = " ";
            }

            if(isset($myValue['dates'])){
                $dates = explode("-", $myValue['dates']);
                $datesFrom = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                $datesTo = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                $dates = " AND (`date` BETWEEN '$datesFrom' AND '$datesTo') ";
            }else{
                $dates = " ";
            }

            if(isset($myValue['showPosted'])){
                $isPosted = " ";
            }else{
                $isPosted = " AND posted ='N' ";
            }

            $data = DB::select(DB::raw("
            (SELECT id, 'GR' AS docType, docno AS docNo, DATE_FORMAT(`date`,'%d/%m/%Y') AS docDate,
                ref AS refNo, account_code AS accNo, `name`, IFNULL(amount, 0) as amount,
                 IFNULL(tax_amount, 0) AS taxAmt, 0 AS rdAdj,  IFNULL(taxed_amount, 0) AS eAmt, posted
            FROM goods
            WHERE deleted_at IS NULL $docNo $creditor $dates $isPosted $isGR
            LIMIT 5000)
            UNION ALL
            (SELECT id, 'CB' AS docType, docno AS docNo, DATE_FORMAT(`date`,'%d/%m/%Y') AS docDate,
                ref1 AS refNo, account_code AS accNo, `name`, IFNULL(amount, 0) as amount,
                 IFNULL(tax_amount, 0) AS taxAmt, 0 AS rdAdj,  IFNULL(taxed_amount, 0) AS eAmt, posted
            FROM cashbills
            WHERE deleted_at IS NULL $docNo $debtor $dates $isPosted $isCB
            LIMIT 5000)
            UNION ALL
            (SELECT id, 'INV' AS docType, docno AS docNo, DATE_FORMAT(`date`,'%d/%m/%Y') AS docDate,
                refno AS refNo, account_code AS accNo, `name`, IFNULL(amount, 0) as amount,
                 IFNULL(tax_amount, 0) AS taxAmt, 0 AS rdAdj,  IFNULL(taxed_amount, 0) AS eAmt, posted
            FROM invoices
            WHERE deleted_at IS NULL $docNo $debtor $dates $isPosted $isINV
            LIMIT 5000)
            "), []);

            $dataArray = "";
            $i = 1;
            foreach($data as $dt){

                if($i != count($data)){
                    $dataArray .= '[
                        "'.$dt->id.'",
                        "'.$dt->docType.'",
                        "'.$dt->docNo.'",
                        "'.$dt->docDate.'",
                        "'.$dt->refNo.'",
                        "'.$dt->accNo.'",
                        "'.$dt->name.'",
                        "'.number_format((float)$dt->amount, 2, '.', '').'",
                        "'.number_format((float)$dt->taxAmt, 2, '.', '').'",
                        "'.$dt->rdAdj.'",
                        "'.number_format((float)$dt->eAmt, 2, '.', '').'",
                        "'.$dt->posted.'"
                     ],';
                }else{
                    $dataArray .= '[
                        "'.$dt->id.'",
                        "'.$dt->docType.'",
                        "'.$dt->docNo.'",
                        "'.$dt->docDate.'",
                        "'.$dt->refNo.'",
                        "'.$dt->accNo.'",
                        "'.$dt->name.'",
                        "'.number_format((float)$dt->amount, 2, '.', '').'",
                        "'.number_format((float)$dt->taxAmt, 2, '.', '').'",
                        "'.$dt->rdAdj.'",
                        "'.number_format((float)$dt->eAmt, 2, '.', '').'",
                        "'.$dt->posted.'"
                     ]';
                }

                 $i++;
            };

            return '{
                "data": [ '. $dataArray .']
                }';
        }
    }

    public function getLeftText1($myValue)
    {
        $lefttextval1 = $myValue['lefttextval1'];
        $lefttextname1 = $myValue['lefttextname1'];
        if(!empty($lefttextval1)){
            switch($lefttextname1){
                case "stkCode" :
                    $leftText1 = " AND a.`code` LIKE '$lefttextval1%' ";
                    break;
                case "descr" :
                    $leftText1 = " AND a.`descr` LIKE '$lefttextval1%' ";
                    break;
                case "ref1" :
                    $leftText1 = " AND a.`ref1` LIKE '$lefttextval1%' ";
                    break;
                case "ref2" :
                    $leftText1 = " AND a.`ref2` LIKE '$lefttextval1%' ";
                    break;
                case "model" :
                    $leftText1 = " AND a.`model` LIKE '$lefttextval1%' ";
                    break;
                case "cat" :
                    $leftText1 = " AND e.`code` LIKE '$lefttextval1%' ";
                    break;
                case "prod" :
                    $leftText1 = " AND b.`code` LIKE '$lefttextval1%' ";
                    break;
                case "brand" :
                    $leftText1 = " AND c.`code` LIKE '$lefttextval1%' ";
                    break;
                case "loc" :
                    $leftText1 = " AND f.`code` LIKE '$lefttextval1%' ";
                    break;
                case "uom" :
                    $leftText1 = " AND d.`code` LIKE '$lefttextval1%' ";
                    break;
                case "type" :
                    $leftText1 = " AND a.`type` LIKE '$lefttextval1%' ";
                    break;
                case "weight" :
                    $leftText1 = " AND a.`weight` LIKE '$lefttextval1%' ";
                    break;
                }
                return $leftText1;
            }
            return "";
    }

    public function getctxtext1($myValue)
    {
        $ctxtextval1 = $myValue['ctxtextval1'];
        $ctxtextname1 = $myValue['ctxtextname1'];
        if(!empty($ctxtextval1)){
            switch($ctxtextname1){
                case "doNo" :
                    $ctxtext1 = " AND a.`dn_no` LIKE '%$ctxtextval1%' ";
                    break;
                case "stkCode" :
                    $ctxtext1 = " AND a.`code` LIKE '%$ctxtextval1%' ";
                    break;
                case "descr" :
                    $ctxtext1 = " AND a.`descr` LIKE '%$ctxtextval1%' ";
                    break;
                case "ref1" :
                    $ctxtext1 = " AND a.`ref1` LIKE '%$ctxtextval1%' ";
                    break;
                case "ref2" :
                    $ctxtext1 = " AND a.`ref2` LIKE '%$ctxtextval1%' ";
                    break;
                case "model" :
                    $ctxtext1 = " AND a.`model` LIKE '%$ctxtextval1%' ";
                    break;
                case "cat" :
                    $ctxtext1 = " AND e.`code` LIKE '%$ctxtextval1%' ";
                    break;
                case "prod" :
                    $ctxtext1 = " AND b.`code` LIKE '%$ctxtextval1%' ";
                    break;
                case "brand" :
                    $ctxtext1 = " AND c.`code` LIKE '%$ctxtextval1%' ";
                    break;
                case "loc" :
                    $ctxtext1 = " AND f.`code` LIKE '%$ctxtextval1%' ";
                    break;
                case "uom" :
                    $ctxtext1 = " AND d.`code` LIKE '%$ctxtextval1%' ";
                    break;
                case "type" :
                    $ctxtext1 = " AND a.`type` LIKE '%$ctxtextval1%' ";
                    break;
                case "weight" :
                    $ctxtext1 = " AND a.`weight` LIKE '%$ctxtextval1%' ";
                    break;
                }
                return $ctxtext1;
            }
            return "";
    }

}
