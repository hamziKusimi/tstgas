<?php

namespace App\Http\Controllers;

use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Cylindervalvetype;
use App\Model\Cylindercontainertypes;
use App\Model\Cylinderhandwheeltype;
use App\Model\Manufacturer;
use App\Model\Ownership;
use App\Model\CylinderActivity;
use App\Model\View\CylinderMovement;
use App\Model\View\CylinderCustmerActivity;
use App\Model\Debtor;
use App\Model\View\CylinderHolding;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;

class CylinderActivityController extends Controller
{
    public function print(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $CylActivity = $this->getCylActivityQuery($request, $dates_from, $dates_to);

        if ($CylActivity->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $cylJSON = $this->getCylActivityJSON($CylActivity, $request);

        $JsonFileName = "CylinderCustomerActivity" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/CylinderCustomerActivity/' . $JsonFileName . '.json'), $cylJSON);
        $JasperFileName = "CylinderCustomerActivity" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to)
    {
        $input = base_path() . '/resources/reporting/CylinderCustomerActivity/CylinderCustomerActivity-group.jrxml';
        $output = base_path() . '/resources/reporting/CylinderCustomerActivity/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/CylinderCustomerActivity/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date_from" => date('d/m/Y', strtotime($dates_from)),
                "date_to" => date('d/m/Y', strtotime($dates_to))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/CylinderCustomerActivity/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getCylActivityQuery($request, $dates_from, $dates_to)
    {
        return CylinderCustmerActivity::where(function ($query) use ($request, $dates_from, $dates_to) {
            if ($dates_from == $dates_to) {
                $query->whereDate('cdate', '=', $dates_from);
            } else {
                $query->whereBetween('cdate', [$dates_from, $dates_to]);
            }

            if ($request->Product_Chkbx == "on") {
                $query->whereBetween('product', [$request->Product_frm, $request->Product_to])
                    ->get();
            }

            if ($request->Debtor_Chkbx == "on") {
                $query->whereBetween('debtor', [$request->Debtor_frm, $request->Debtor_to])
                    ->get();
            }

            if ($request->DeliveryNote_Chkbx == "on") {
                $query->whereBetween('doc_no', [$request->DeliveryNote_frm, $request->DeliveryNote_to])
                    ->get();
            }

            if ($request->Area_Chkbx == "on") {
                $query->whereBetween('area', [$request->Area_frm, $request->Area_to])
                    ->get();
            }

            if ($request->include_Chkbx != "on") {
                $query->where('remark','=','');
            }

            $query->whereIn('doc_type', $request->sales);

        })->orderBy('cdate')->get();
    }


    public function getCylActivityJSON($CylActivity, $request)
    {
        $dataArray = [];
        foreach ($CylActivity as $cyl) {
            $objectJSON =   [];

            $objectJSON['cylinder_sn'] = $cyl->s_no;
            $objectJSON['prod_code'] = $cyl->product;
            $objectJSON['prod_descr'] = $cyl->category;
            $objectJSON['doc_type'] = $cyl->doc_type;
            $objectJSON['doc_no'] = $cyl->doc_no;
            $objectJSON['date'] = date('d/m/Y', strtotime($cyl->cdate));
            $objectJSON['remark'] = $cyl->remark;
            $objectJSON['debtor'] = $cyl->debtor;
            $objectJSON['name'] = $cyl->name;
            $objectJSON['grp1'] = $request->group_1 != "none" ? $request->group_1 : null ;
            $objectJSON['grp2'] = $request->group_1 != "none" ? $request->group_2 : null ;
            $objectJSON['grp3'] = $request->group_1 != "none" ? $request->group_3 : null ;

            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
