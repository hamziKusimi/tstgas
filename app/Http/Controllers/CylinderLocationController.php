<?php

namespace App\Http\Controllers;

use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Cylindervalvetype;
use App\Model\Cylindercontainertypes;
use App\Model\Cylinderhandwheeltype;
use App\Model\Manufacturer;
use App\Model\Ownership;
use App\Model\CylinderActivity;
use App\Model\View\CylinderMovement;
use App\Model\Debtor;
use App\Model\View\CylinderRental;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;

class CylinderLocationController extends Controller
{
    public function print(Request $request)
    {
        $CylRental = $this->getCylRentalQuery($request);
        if ($CylRental->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $header = $this->getHeader();
        $JSONData = $this->getJSONData($CylRental, $request);

        $jsonDecoded = json_decode($JSONData, true);

        $fileName = 'Cylinder Location' . date("Ymdhisa") . Auth::user()->id . '.csv';

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        $fp = fopen('php://output', 'w');
        fputcsv($fp,  $header);
        foreach ($jsonDecoded as $row) {
            fputcsv($fp, $row);
        }

        fclose($fp);
    }

    public function getCylRentalQuery($request)
    {
        return CylinderRental::selectRaw('SUM(qty) AS Total, debtor')
            ->whereDate('cdate', '<=', date('Y-m-d', strtotime($request->date)))
            ->where(function ($query) use ($request) {
                $query->whereBetween('debtor', [$request->Debtor_frm, $request->Debtor_to])
                    ->get();
            })->groupBy('debtor')
            ->orderBy('debtor')
            ->get();
    }

    public function getJSONData($CylRental, $request)
    {
        $dataArray = [];
        $no = 1;
        foreach ($CylRental as $debtorRent) {
            $objectJSON =   [];

            $objectJSON['no'] = $no;
            $debtor = Debtor::select('name')
                ->where('accountcode', '=', $debtorRent->debtor)->first();
            $objectJSON['client'] = $debtor != null ? $debtor->name : "-";

            $product = Cylinderproduct::orderBy('code')->get();

            $cylProd = CylinderRental::selectRaw('SUM(qty) AS Total, product')
                ->whereDate('cdate', '<=', date('Y-m-d', strtotime($request->date)))
                ->where('debtor', '=', $debtorRent->debtor)->groupBy('product')
                ->orderBy('product')
                ->get();

            foreach ($product as $prod) {
                foreach ($cylProd as $t_prod) {
                    if ($prod->code == $t_prod->product) {
                        $objectJSON[$prod->code] =  $t_prod->Total;
                    }
                }
                if (!isset($objectJSON[$prod->code])) {
                    $objectJSON[$prod->code] = "-";
                }
            }
            $objectJSON['Total'] = $debtorRent->Total;

            $no =  $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        return json_encode($dataArray);
    }

    public function getHeader()
    {
        $product = Cylinderproduct::orderBy('code')->get();
        $header = array('No.', 'Client');

        foreach ($product as $prod) {
            array_push($header, $prod->code);
        }

        array_push($header, 'Total');

        return $header;
    }
}
