<?php

namespace App\Http\Controllers;

use App\Model\Gasrack;
use App\Model\Gasracktype;
use App\Model\Manufacturer;
use App\Model\Gasrackcylinder;
use App\Model\SystemSetup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Auth;
use DB;
use DataTables;

class GasrackController extends Controller
{
    public function index()
    {
        $data["gasracks"] = Gasrack::get();
        $data["page_title"] = "Gas Rack Item Listing";
        $data["bclvl1"] = "Gas Rack Item Listing";
        $data["bclvl1_url"] = route('gasracks.index');

        return view('cylindermaster.gasracks.index', $data);
    }

    public function gasrackList()
    {
        $query = Gasrack::query()
            ->selectRaw('serial, details')->groupBy('serial');

        return DataTables::of($query)->make(true);
    }

    public function create()
    {
        $data["gasracks"] = Gasrack::get();
        $data["gasracktypes"] = Gasracktype::get();
        $data["manufacturers"] = Manufacturer::get();
        $data["systemsetup"] = SystemSetup::first();
        $data["page_title"] = "Add Gas Rack";
        $data["bclvl1"] = "Gas Rack Item Listing";
        $data["bclvl1_url"] = route('gasracks.index');
        $data["bclvl2"] = "Add Gas Rack";
        $data["bclvl2_url"] = route('gasracks.create');
        // return response()->json($data);

        return view('cylindermaster.gasracks.create', $data);
    }

    public function store(Request $request)
    {

        $attachment = $request->file('attachment');

        $gasrack = Gasrack::create([
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'description' => $request['description'],
            'mfr' => $request['mfr'],
            'mfgterm' => $request['mfgterm'],
            'owner' => $request['owner'],
            'type' => $request['type'],
            'maxgmass' => $request['maxgmass'],
            'taremass' => $request['taremass'],
            'payload' => $request['payload'],
            'certno' => $request['certno'],
            'testdate' => $request['testdate'] ? date('Y-m-d', strtotime($request['testdate'])) : null,
            'sg_barcode' => $request['sgbarcode'],
            'sg_serial' => $request['sgserial'],
            'created_by' => Auth::user()->id,
            'status' => $request['Status'],
            'reasons' => $request['reasons']
        ]);

        if(!empty($request['cyserial'])){
            Gasrackcylinder::create([
                'gr_id' => $gasrack->id,
                'cy_barcode' => $request['cybarcode'],
                'cy_serial' => $request['cyserial'],
                'cy_product' => $request['cyprod'],
                'cy_category' => $request['cycat'],
                'cy_descr' => $request['cydescr'],
                'cy_capacity' => $request['cycapacity'],
                'cy_pressure' => $request['cypressure'],
                'created_by' => Auth::user()->id,
            ]);
        }

        if ($request->file('attachment') !== null) {
            $att_name = rand() . '.' . $attachment->getClientOriginalExtension();
            $attachment->move(public_path('attachment'), $att_name);

            $gasrack->update([
                'attachment' => $att_name
            ]);
        }

        return redirect()->route('gasracks.edit', ['id' => $gasrack])->with('Success', 'Gas Rack created sucessfully');
    }

    public function edit($id)
    {
        $data = [];
        $data["gasracks"] = Gasrack::get();
        $data["gasrack"] = Gasrack::find($id);
        $data["gasracktypes"] = Gasracktype::get();
        $data["manufacturers"] = Manufacturer::get();
        $data["systemsetup"] = SystemSetup::first();
        $data["gasrackcylinders"] = Gasrackcylinder::where('gr_id', $id)->get();
        $data["page_title"] = "Edit Gas Rack";
        $data["bclvl1"] = "Gas Rack Item Listing";
        $data["bclvl1_url"] = route('gasracks.index');
        $data["bclvl2"] = "Edit Gas Rack";
        $data["bclvl2_url"] = route('gasracks.edit', ['id' => $id]);

        return view('cylindermaster.gasracks.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $gasrack = Gasrack::find($id);
        $attachment = $request->file('attachment');

        $gasrack->update([
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'description' => $request['description'],
            'mfr' => $request['mfr'],
            'mfgterm' => $request['mfgterm'],
            'owner' => $request['owner'],
            'type' => $request['type'],
            'maxgmass' => $request['maxgmass'],
            'taremass' => $request['taremass'],
            'payload' => $request['payload'],
            'certno' => $request['certno'],
            'testdate' => $request['testdate'] ? date('Y-m-d', strtotime($request['testdate'])) : null,
            'sg_barcode' => $request['sgbarcode'],
            'sg_serial' => $request['sgserial'],
            'updated_by' => Auth::user()->id,
            'status' => $request['Status'],
            'reasons' => $request['reasons']

        ]);

        if(!empty($request['cyserial'])){
            Gasrackcylinder::create([
                'gr_id' => $gasrack->id,
                'cy_barcode' => $request['cybarcode'],
                'cy_serial' => $request['cyserial'],
                'cy_product' => $request['cyprod'],
                'cy_category' => $request['cycat'],
                'cy_descr' => $request['cydescr'],
                'cy_capacity' => $request['cycapacity'],
                'cy_pressure' => $request['cypressure'],
                'created_by' => Auth::user()->id,
            ]);
        }

        if ($request->file('attachment') !== null) {
            $att_name = rand() . '.' . $attachment->getClientOriginalExtension();
            $attachment->move(public_path('attachment'), $att_name);

            $gasrack->update([
                'attachment' => $att_name
            ]);
        }


        return redirect()->route('gasracks.edit', ['id'=>$gasrack->id])->with('Success', 'Gas Rack created sucessfully');
        // return redirect()->route('gasracks.index')->with('Success', 'Gas Rack updated sucessfully');
    }

    public function destroy(gasrack $gasrack)
    {
        $gasrack->update([
            'deleted_by' => Auth::user()->id
        ]);
        $gasrack->delete();

        return redirect()->route('gasracks.index')->with('Success', 'Gas Rack deleted sucessfully');
    }

    public function destroyAttach(Request $request)
    {
        $gasrack = Gasrack::find($request['id']);
        $gasrack->update([
            'attachment' => null
        ]);
        File::delete(public_path('attachment/' . $request['attachment']));

        return "success";
    }

    public function storeCylinder(Request $request)
    {
        # code...
    }

    public function filter_records(Request $request)
    {

        $result = Gasrack::where(function ($query) use ($request) {

            if ($request['isBarcode'] == 'true') {
                $query->Where('barcode', '=', $request['item']);
            } else {
                $query->Where('serial', '=', $request['item']);
            }
            $query->whereNotNull('serial');
        })
            ->get(['barcode', 'serial', 'description']);

        return $result;
    }


    public function rn_filter_records(Request $request, $accountcode)
    {

        if ($request['isBarcode'] == 'true') {
            $result = DB::select(DB::raw("

                SELECT
                gasracks.serial AS SERIAL,
                gasracks.barcode AS barcode,
                gasracks.description AS descr,
                delivery_notes.dn_no AS dnno,
                delivery_notes.date AS dnno_date,
                delivery_notes.account_code AS account_code,
                delivery_noteprs.dn_no AS pr_dnno,
                delivery_noteprs.barcode AS pr_barcode,
                delivery_noteprs.serial AS pr_serial
                FROM gasracks
                INNER JOIN delivery_noteprs ON delivery_noteprs.serial = gasracks.serial
                INNER JOIN delivery_notes ON delivery_notes.dn_no = delivery_noteprs.dn_no

                WHERE delivery_noteprs.return_note IS NULL AND delivery_notes.account_code = :accountcode AND gasracks.barcode = :item
            "), [
                'accountcode' => $accountcode,
                'item' => $request['item']
            ]);
        }else{
            $result = DB::select(DB::raw("

                SELECT
                gasracks.serial AS SERIAL,
                gasracks.barcode AS barcode,
                gasracks.description AS descr,
                delivery_notes.dn_no AS dnno,
                delivery_notes.date AS dnno_date,
                delivery_notes.account_code AS account_code,
                delivery_noteprs.dn_no AS pr_dnno,
                delivery_noteprs.barcode AS pr_barcode,
                delivery_noteprs.serial AS pr_serial
                FROM gasracks
                INNER JOIN delivery_noteprs ON delivery_noteprs.serial = gasracks.serial
                INNER JOIN delivery_notes ON delivery_notes.dn_no = delivery_noteprs.dn_no

                WHERE delivery_noteprs.return_note IS NULL AND delivery_notes.account_code = :accountcode AND gasracks.serial = :item
            "), [
                'accountcode' => $accountcode,
                'item' => $request['item']
            ]);

        }

            // return response()->json($result);
        return $result;
    }


    public function check_slings(Request $request)
    {

        $result = Gasrack::where(function ($query) use ($request) {

            if ($request['isBarcode'] == 'true') {
                $query->Where('sg_barcode', '=', $request['item']);
            } else {
                $query->Where('sg_serial', '=', $request['item']);
            }
            $query->whereNotNull('sg_barcode');
        })
            ->get(['sg_barcode', 'sg_serial']);

        return $result;
    }

    public function destroycylinder($cylinder)
    {

        $gasrackcy = Gasrackcylinder::find($cylinder);
        $gasrackcy->update([
            'deleted_by' => Auth::user()->id
        ]);
        $gasrackcy->delete();

        $id = Gasrack::where('id', $gasrackcy->gr_id)->value('id');
        // dd($id);
        return redirect()->route('gasracks.edit', ['id'=>$id])->with('Success', 'Gas Rack deleted sucessfully');
    }

    public function destroysling($sling)
    {

        $gasrackcy = Gasrack::where('sg_serial', $sling)->first();

        $gasrackcy->update([

            'sg_serial' => null,
            'sg_barcode' => null,
            'deleted_by' => Auth::user()->id
        ]);

        $id = Gasrack::where('id', $gasrackcy->id)->value('id');
        // dd($id);
        return redirect()->route('gasracks.edit', ['id'=>$id])->with('Success', 'Gas Rack deleted sucessfully');
    }

    public function rackList()
    {
        $query = Gasrack::query()
        ->selectRaw('serial, description')->orderBy('serial');

        return DataTables::of($query)->make(true);
    }

}
