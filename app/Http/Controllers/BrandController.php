<?php

namespace App\Http\Controllers;

use App\Model\Brand;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class BrandController extends Controller
{
    public function index()
    {
        $data["brands"] = Brand::get();
        $data["page_title"] = "Brand Item Listing";
        $data["bclvl1"] = "Brand Item Listing";
        $data["bclvl1_url"] = route('brands.index');

        return view('stockmaster.brands.index', $data);
    }

    public function create()
    {
       $data["page_title"] = "Add Brand";
       $data["bclvl1"] = "Brand Item Listing";
       $data["bclvl1_url"] = route('brands.index');
       $data["bclvl2"] = "Add Brand";
       $data["bclvl2_url"] = route('brands.create');

       return view('stockmaster.brands.create', $data);
    }

    public function store(Request $request)
    {
        $brand = Brand::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('brands.index')->with('Success', 'Brand created successfully.');
    }

    public function edit($id)
    {
        $data["brand"] = Brand::find($id);
        $data["page_title"] = "Edit Brand";
        $data["bclvl1"] = "Brand Item Listing";
        $data["bclvl1_url"] = route('brands.index');
        $data["bclvl2"] = "Edit Brand";
        $data["bclvl2_url"] = route('brands.edit', ['id'=>$id]);

        return view('stockmaster.brands.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);
        if (isset($brand)) {
            $brand->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('brands.index')->with('Success', 'Brand updated successfully.');
    }

    public function destroy(brand $brand)
    {
        $brand->update([
            'deleted_by' => Auth::user()->id
        ]);
        $brand->delete();
        return redirect()->route('brands.index')->with('Success', 'Brand deleted successfully.');
    }

    public function print()
    {
        $Brands = Brand::all();

        $dataArray = array();
        $no = 1;
        foreach ($Brands as $Brand) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Brand->code;
            $objectJSON["description"] = $Brand->descr;
            $objectJSON["status"] = $Brand->active ? "Active":"Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $BrandsJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintBrand" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $BrandsJSON);
        $JasperFileName = "ReportPrintBrand" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Brand Listing Report (" .  date('d/m/Y') .")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }
    
}
