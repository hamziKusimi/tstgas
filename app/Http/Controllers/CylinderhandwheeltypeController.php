<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Cylinderhandwheeltype;
use Auth;

class CylinderhandwheeltypeController extends Controller
{
    public function index()
    {
        $data["cylinderhandwheeltypes"] = Cylinderhandwheeltype::get();
        $data["page_title"] = "Cylinder Handwheel Item Listing";
        $data["bclvl1"] = "Cylinder Handwheel Item Listing";
        $data["bclvl1_url"] = route('cylinderhandwheeltypes.index');

        return view('cylindermaster.cylinderhandwheeltypes.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Cylinder Handwheel";
        $data["bclvl1"] = "Cylinder Handwheel Item Listing";
        $data["bclvl1_url"] = route('cylinderhandwheeltypes.index');
        $data["bclvl2"] = "Add Cylinder Handwheel";
        $data["bclvl2_url"] = route('cylinderhandwheeltypes.create');

        return view('cylindermaster.cylinderhandwheeltypes.create', $data);
    }

    public function store(Request $request)
    {
        $cylinderhandwheeltype = Cylinderhandwheeltype::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('cylinderhandwheeltypes.index')->with('Success', 'Cylinder Handwheel created successfully');
    }

    public function edit($id)
    {
        $data["cylinderhandwheeltype"] = Cylinderhandwheeltype::find($id);
        $data["page_title"] = "Edit Cylinder Handwheel";
        $data["bclvl1"] = "Cylinder Handwheel Item Listing";
        $data["bclvl1_url"] = route('cylinderhandwheeltypes.index');
        $data["bclvl2"] = "Edit Cylinder Handwheel";
        $data["bclvl2_url"] = route('cylinderhandwheeltypes.edit', ['id'=>$id]);

        return view('cylindermaster.cylinderhandwheeltypes.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cylinderhandwheeltype = Cylinderhandwheeltype::find($id);
        if (isset($cylinderhandwheeltype)) {
            $cylinderhandwheeltype->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('cylinderhandwheeltypes.index')->with('Success', 'Cylinder Handwheel updated successfully');
    }

    public function destroy(cylinderhandwheeltype $cylinderhandwheeltype)
    {
        $cylinderhandwheeltype->update([
            'deleted_by' => Auth::user()->id
        ]);
        return redirect()->route('cylinderhandwheeltypes.index')->with('Success', 'Cylinder Handwheel deleted successfully');
    }
}
