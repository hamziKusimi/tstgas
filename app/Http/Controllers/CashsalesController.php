<?php

namespace App\Http\Controllers;

use App\Model\Cashsales;
use App\Model\Cashsalesdt;
use Carbon\Carbon;
use App\Model\Area;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Cashbill;
use App\Model\Cashbilldt;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\DocumentSetup;
use App\Model\Deliveryorder;
use App\Model\Invoice;
use App\Model\Deliveryorderdt;
use App\Model\SystemSetup;
use App\Model\PrintedIndexView;
use App\Model\Uom;
use App\Model\Salesman;
use App\Model\ViewTransaction;
use App\Model\Location;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use App\Model\CustomObject\Transaction;
use DB;
use PDF;

class CashsalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data["cashsales"] = Cashsales::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Cashsales::pluck('docno', 'docno')->toArray();
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode')->toArray();
        // $data["print"] = PrintedIndexView::where('index', 'Cash Bill')->pluck('printed_by');
        $data["page_title"] = "Cash Sales  Listing";
        $data["bclvl1"] = "Cash Sales  Listing";
        $data["bclvl1_url"] = route('cashsales.index');
        // dd("test tes");
        return view('dailypro/cashsales.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Cash Sales');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Cash Sales";
        $data["bclvl1"] = "Cash Sales Received Listing";
        $data["bclvl1_url"] = route('cashsales.index');
        $data["bclvl2"] = "Add Cash Sales";
        $data["bclvl2_url"] = route('cashsales.create');

        return view('dailypro.cashsales.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $cashsalesid = Cashsales::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'lpono' => $request['lpono'],
            'ref1' => $request['ref1'],
            'dono' => $request['dono'],
            'quotno' => $request['quotno'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'currency' => !empty($request['currency']) ? $request['currency'] : '',
            'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
            'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
            'created_by' => Auth::user()->id
        ]);

        $CashsalesDoc = DocumentSetup::findByName('Cash Sales');
        $CashsalesDoc->update(['D_LAST_NO' => $CashsalesDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Cashsalesdt::create([
                    'doc_no' => $cashsalesid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'item_code' => $request['item_code'][$i],
                    'reference_no' => $request['reference_no'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'uprice' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id
                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal - $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }

        return redirect()->route('cashsales.edit', ['id' => $cashsalesid])->with('Success', 'Cash Sales added sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cashsales  $cashsales
     * @return \Illuminate\Http\Response
     */
    public function show(Cashsales $cashsales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cashsales  $cashsales
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Cashsales = Cashsales::find($id);
        $contents = Cashsalesdt::where('doc_no', $Cashsales->docno)->get();

        // return response()->json($cashSales->docno);
        $selectedDebitor = Debtor::findByAcode($Cashsales->account_code);
        $masterCodes = AccountMastercode::get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $Cashsales;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedDebitor'] = $selectedDebitor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Cash Sales";
        $data["bclvl1"] = "Cash Sales Received Listing";
        $data["bclvl1_url"] = route('cashsales.index');
        $data["bclvl2"] = "Edit Cash Sales";
        $data["bclvl2_url"] = route('cashsales.edit', ['id' => $id]);

        return view('dailypro/cashsales.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cashsales  $cashsales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $cashsales = Cashsales::find($id);
        if (isset($cashsales)) {
            $cashsales->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'lpono' => $request['lpono'],
                'ref1' => $request['ref1'],
                'dono' => $request['dono'],
                'quotno' => $request['quotno'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $cashsalesdt_id = Cashsalesdt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if ($cashsalesdt_id != null) {
                        $stkBalance = $stock->stkbal + $cashsalesdt_id->totalqty;
                        $cashsalesdt_id->update([
                            'doc_no' => $cashsales->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stkBalance - $cashsalesdt_id->totalqty;
                    } else {
                        $dt = Cashsalesdt::create([
                            'doc_no' => $cashsales->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'item_code' => $request['item_code'][$i],
                            'reference_no' => $request['reference_no'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'uprice' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal - $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
            // return response()->json($request['cashsalesdts_id']);

        }

        return redirect()->route('cashsales.edit', ['id' => $id])->with('Success', 'Cash Sales updated sucessfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cashsales  $cashsales
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Cashsale = Cashsales::find($id);
        $Cashsale->update([
            'deleted_by' => Auth::user()->id
        ]);
        $Cashsale->delete();
        $docno = $Cashsale->docno;

        $Cashsalesdt = Cashsalesdt::where('doc_no', $docno);
        $Cashsalesdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $Cashsalesdt->delete();
        return redirect()->route('cashsales.index')->with('Success', 'Cash Sales deleted successfully');
    }

    public function searchindex(Request $request)
    {
        $data["cashsales"] = Cashsales::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('lpono', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('dono', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('quotno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->paginate(15);

        $data["docno_select"] = Cashsales::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        // $data["print"] = PrintedIndexView::where('index', 'Cash Sales')->pluck('printed_by');
        $data["page_title"] = "Cash Sales Listing";
        $data["bclvl1"] = "Cash Sales Listing";
        $data["bclvl1_url"] = route('cashsales.index');
        return view('dailypro.cashsales.index', $data);
    }

    public function jasper($id)
    {
        $Cashsales = Cashsales::find($id);
        $getprinted = Cashsales::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($Cashsales)) {
            $Cashsales->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $Resources = Cashsales::where('id', $id)->get();
        $ResourcesJsonList = $this->getCashsalesListingJSON($Resources);

        $formattedResource = Cashsales::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'dono as REF_NO',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'Cashsales';
        $jrxml = 'general-bill-two.jrxml';
        $reportTitle = 'Cash Sales';
        $jasperData = Transaction::generateBillDebtor(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function getCashsalesListingJSON($Cashsales)
    {
        $dataArray = array();

        foreach ($Cashsales as $CS) {

            $CSTrans = Cashsalesdt::select(

                'item_code',

                'subject',

                'qty',

                'uom',

                'uprice',

                'amount',

                'details',

                'taxed_amount'

            )

                ->where("doc_no", "=", $CS->docno)

                ->get();



            $lastElement = count($CSTrans);

            $s_n = 1;

            $t_amount = 0;

            $t_qty = 0;



            foreach ($CSTrans as $trans) {

                $objectJSON = [];

                $date = date("d/m/Y", strtotime($CS->date));

                $objectJSON['s_n'] = $s_n;

                $objectJSON['details'] = $CS->docno . ", " . $date . ", " .

                    $CS->account_code . ", " . $CS->name;

                $objectJSON['description'] = $trans->subject;

                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();

                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();

                $objectJSON['location'] = $loc->code;

                $objectJSON['item_code'] = $trans->item_code;

                $objectJSON['quantity'] = $trans->qty;

                $objectJSON['uom'] = $trans->uom;

                $objectJSON['unit_price'] = $trans->uprice;

                $objectJSON['amount'] = $trans->amount;

                $objectJSON['tax_amount'] = $trans->taxed_amount;

                $objectJSON['subject'] = $trans->subject;

                $objectJSON['_DETAIL'] = $trans->details;

                $t_amount = $t_amount + $trans->amount;

                $t_qty =  $t_qty + $trans->qty;



                if ($s_n == $lastElement) {

                    $objectJSON['t_amount'] = $t_amount;

                    $objectJSON['t_qty'] = $t_qty;
                }

                $s_n = $s_n + 1;

                $dataArray[] = collect($objectJSON);
            }
        }

        return  '{"data" :' . json_encode($dataArray) . '}';

        // return $dataArray;

    }

    public function print(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Cash Sales')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Cash Sales')->update([
                'index' => "Cash Sales",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Cash Sales",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;
        
        $dates = $request->dates;

        $Cashsales = $this->getCashsalesQuery($request);
        if (empty($Cashsales)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->cashsales_rcv == "listing") {
            $reportType = "CashsalesListing";

            foreach ($Cashsales as $cs) {

                $amount = $amount + $cs->amount;
            }

            $pdf = PDF::loadView('dailypro/cashsales.listing', [
                'CashsalesJSON' =>  $Cashsales,
                'date' =>  $dates,
                'totalamount' =>  $amount
            ])->setPaper('A4', 'landscape');
            return $pdf->inline();
        } else {

            foreach ($Cashsales as $cs) {

                $amount = $amount + $cs->amount;
            }

            $reportType = "CashsalesSummary";

            $pdf = PDF::loadView('dailypro/cashsales.summary', [
                'reportType' => $reportType,
                'CashsalesJSON' =>  $Cashsales,
                'date' =>  $dates,
                'totalamount' =>  $amount
            ])->setPaper('A4');
            return $pdf->inline();
        }
    }

    public function getCashsalesQuery(Request $request)
    {
        if ($request->dates_Chkbx == "on") {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

            if ($dates_from == $dates_to) {
                $date = "AND cashsales.date = '" . $dates_from . "'";
            } else {
                $date = "AND cashsales.date >= '" . $dates_from . "' AND cashsales.date <= '" . $dates_to . "'";
            }

            if ($request->docno_Chkbx == "on") {
                $docno = "AND (cashsales.docno >= '" . $request->docno_frm . "' AND cashsales.docno <= '" . $request->docno_to . "')";

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cashsales.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cashsales.lpono >= '" . $request->LPO_frm_1 . "' AND cashsales.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                }
            } else {
                $docno = '';

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cashsales.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cashsales.lpono >= '" . $request->LPO_frm_1 . "' AND cashsales.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                }
            }
        } else {
            $date = '';
            if ($request->docno_Chkbx == "on") {
                $docno = "AND cashsales.docno >= '" . $request->docno_frm . "' AND cashsales.docno <= '" . $request->docno_to . "'";

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND (cashsales.lpono = '" . $request->LPO_frm_1 . "')";
                    } else {
                        $lpono = "AND (cashsales.lpono >= '" . $request->LPO_frm_1 . "' AND cashsales.lpono <= '" . $request->LPO_to_1 . "')";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                }
            } else {
                $docno = '';

                if ($request->LPO_Chkbx_1 == "on") {
                    if ($request->LPO_to_1 == null) {
                        $lpono = "AND cashsales.lpono = '" . $request->LPO_frm_1 . "'";
                    } else {
                        $lpono = "AND cashsales.lpono >= '" . $request->LPO_frm_1 . "' AND cashsales.lpono <= '" . $request->LPO_to_1 . "'";
                    }

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND (cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "')";
                    } else {
                        $debtor = '';
                    }
                } else {
                    $lpono = '';

                    if ($request->debCode_Chkbx == "on") {
                        $debtor = "AND cashsales.account_code >= '" . $request->debCode_frm . "' AND cashsales.account_code <= '" . $request->debCode_to . "'";
                    } else {
                        $debtor = '';
                    }
                }
            }
        }

        $CashSales = DB::select(DB::raw("

                SELECT
                `cashsales`.`docno`          AS `docno`,
                `cashsales`.`date`           AS `date`,
                `cashsales`.`lpono`          AS `lpono`,
                `cashsales`.`account_code`   AS `account_code`,
                `cashsales`.`account_code`   AS `debtor`,
                `cashsales`.`name`           AS `name`,
                CONCAT(CONCAT(`cashsales`.`docno`),', ',DATE_FORMAT(`cashsales`.`date`,'%d-%m-%Y'),', ',`cashsales`.`account_code`,', ',`cashsales`.`name`) AS `details`,
                `cashsalesdts`.`subject`      AS `description`,
                `cashsalesdts`.`id`           AS `id`,
                `cashsalesdts`.`doc_no`       AS `doc_no`,
                `cashsalesdts`.`item_code`    AS `item_code`,
                `cashsalesdts`.`qty`          AS `quantity`,
                `cashsalesdts`.`uom`          AS `uom`,
                `cashsalesdts`.`uprice`       AS `unit_price`,
                `cashsalesdts`.`amount`       AS `amount`,
                `cashsalesdts`.`taxed_amount` AS `tax_amount`,
                `cashsalesdts`.`subject`      AS `subject`,
                `cashsalesdts`.`details`      AS `DETAIL`,
                `stockcodes`.`loc_id`        AS `loc_id`,
                cashsales.amount AS totalamt,
                (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location
                FROM ((`cashsales`
                JOIN `cashsalesdts`
                    ON ((`cashsales`.`docno` = `cashsalesdts`.`doc_no`)))
                JOIN `stockcodes`
                ON ((`cashsalesdts`.`item_code` = `stockcodes`.`code`)))
                WHERE `cashsalesdts`.`deleted_at` IS NULL AND `cashsales`.`deleted_at` IS NULL
                $date $docno $lpono $debtor
                order by docno

        "), []);

        return $CashSales;
    }

    public function getRouteByDocno(Request $request)
    {
        $Cashsales = Cashsales::where('docno', $request->docno)->first('id');

        if (!$Cashsales) {

            // Session::flash('message', 'This is a message!');
            $url = action('CashsalesController@create');
        } else {

            $url = action('CashsalesController@edit', ['id' => $Cashsales->id]);
        }

        return $url;
    }
}
