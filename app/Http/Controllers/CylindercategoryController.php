<?php

namespace App\Http\Controllers;

use App\Model\Cylindercategory;
use Illuminate\Http\Request;
use Auth;

class CylindercategoryController extends Controller
{
    public function Index()
    {
        $data["cylindercategory"] = Cylindercategory::get();
        $data["page_title"] = "Category Item Listing";
        $data["bclvl1"] = "Category Item Listing";
        $data["bclvl1_url"] = route('cylindercategory.index');

        return view('cylindermaster.cylindercategory.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Category";
        $data["bclvl1"] = "Category Item Listing";
        $data["bclvl1_url"] = route('cylindercategory.index');
        $data["bclvl2"] = "Add Category";
        $data["bclvl2_url"] = route('cylindercategory.create');
        return view('cylindermaster.cylindercategory.create', $data);
    }

    public function store(Request $request)
    {
       
        $cylindercategory = Cylindercategory::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('cylindercategory.index')->with('Success', 'Category created successfully.');
    }

    public function edit($id)
    {
        $data["cylindercategory"] = Cylindercategory::find($id);
        $data["page_title"] = "Edit Category";
        $data["bclvl1"] = "Category Item Listing";
        $data["bclvl1_url"] = route('cylindercategory.index');
        $data["bclvl2"] = "Edit Category";
        $data["bclvl2_url"] = route('cylindercategory.edit', ['id'=>$id]);
        return view('cylindermaster.cylindercategory.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cylindercategory = Cylindercategory::find($id);
        if (isset($cylindercategory)) {
            $cylindercategory->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('cylindercategory.index')->with('Success', 'Category updated successfully.');
    }

    public function destroy(cylindercategory $cylindercategory)
    {        
        $cylindercategory->update([
            'deleted_by' => Auth::user()->id
        ]);

        $cylindercategory->delete();
        return redirect()->route('cylindercategory.index')->with('Success', 'Category deleted successfully.');
    }

    public function getCode($code)
    {
        $user = Cylindercategory::where('code', $code)->where('deleted_at', NULL)->get();
        return $user;
    }
}
