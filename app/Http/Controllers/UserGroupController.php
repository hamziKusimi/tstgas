<?php

namespace App\Http\Controllers;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;  

use Illuminate\Http\Request;

use Spatie\Activitylog\Models\Activity;

class UserGroupController extends Controller
{
    public function index()
    {
        $data["usergroups"] = Role::all();
        $data["permissions"]  = Permission::all();
        $data["page_title"] = "User Group Item Listing";
        $data["bclvl1"] = "User Group Item Listing";
        $data["bclvl1_url"] = route('usergroups.index');

        return view('settings.usergroups.index', $data);
    }

    public function create()
    {
        $data["usergroups"] = Role::all();
        $data["user"] = Role::pluck('name');
        $data["permissions"]  = Permission::all();
        $data["page_title"] = "Add User Group";
        $data["bclvl1"] = "User Group Item Listing";
        $data["bclvl1_url"] = route('usergroups.index');
        $data["bclvl2"] = "Add User Group";
        $data["bclvl2_url"] = route('usergroups.create');
        return view('settings.usergroups.create', $data);
    }

    public function store(Request $request)
    {
        $role = Role::create(['name' => $request->input('name')]);

        foreach($request['ugRights'] as $right){
            $role->givePermissionTo($right);
        }

        return redirect()->route('usergroups.index')->with('Success', 'User Group created successfully');
    }

    public function edit($id)
    {
        // $user = User::find(5);
        // $activity = Activity::where('subject_type', 'App\Model\Cylinder')->where('causer_id', $user->id)->get();

        // foreach ($activies as $activity) {
        //     $newChanges = $activity->changes()['attributes']['lldate'];
        // }

        // return response()->json($activity->changes());
        $role = Role::find($id); 
        $permissions = $role->permissions()->get();
        // return response($permissions);
        
        $data["page_title"] = "Edit User Group";
        $data["bclvl1"] = "User Group Item Listing";
        $data["bclvl1_url"] = route('usergroups.index');
        $data["bclvl2"] = "Edit User Group";
        $data["bclvl2_url"] = route('usergroups.create');
        $data["role"] = $role;
        $data["permissions"] = $permissions;
        $data["user"] = Role::pluck('name');
        return view('settings.usergroups.edit', $data);
    }

    public function update(Request $request, $id)
    {        
        $role = Role::find($id);
        $role->update(['name' => $request->input('name')]);

        $role->syncPermissions($request['ugRights']);

        return redirect()->route('usergroups.edit', $id)->with('Success', 'User Group updated successfully');
    }

    public function destroy(usergroup $usergroup)
    {
        $usergroup->update([
            'deleted_by' => Auth::user()->id
        ]);
        
        $usergroup->delete();
        return redirect()->route('usergroups.index')->with('Success', 'User Group deleted successfully.');
    }
}
