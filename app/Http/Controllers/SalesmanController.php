<?php

namespace App\Http\Controllers;

use App\Model\Salesman;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class SalesmanController extends Controller
{
    public function Index()
    {
        $data["salesman"] = Salesman::get();
        $data["page_title"] = "Salesman Item Listing";
        $data["bclvl1"] = "Salesman Item Listing";
        $data["bclvl1_url"] = route('salesman.index');

        return view('stockmaster.salesman.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Salesman";
        $data["bclvl1"] = "Salesman Item Listing";
        $data["bclvl1_url"] = route('salesman.index');
        $data["bclvl2"] = "Add Salesman";
        $data["bclvl2_url"] = route('salesman.create');
        return view('stockmaster.salesman.create', $data);
    }

    public function store(Request $request)
    {
        $salesman = Salesman::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'password' => bcrypt($request['passwordcoy']),
            'password_h' => md5($request['passwordcoy']),
            'active' => $request['active'],
            'created_by'=> Auth::user()->id,
            'vehicle_no' => $request['vehicle_no']
        ]);
        return redirect()->route('salesman.index')->with('Success', 'Salesman created successfully.');
    }

    public function edit($id)
    {
        $data["salesman"] = Salesman::find($id);
        $data["page_title"] = "Edit Salesman";
        $data["bclvl1"] = "Salesman Item Listing";
        $data["bclvl1_url"] = route('salesman.index');
        $data["bclvl2"] = "Edit Salesman";
        $data["bclvl2_url"] = route('salesman.edit', ['id'=>$id]);

        return view('stockmaster.salesman.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $salesman = Salesman::find($id);
        if (isset($salesman)) {
            $salesman->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'password' => bcrypt($request['passwordcoy']),
                'password_h' => md5($request['passwordcoy']),
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id,
                'vehicle_no' => $request['vehicle_no']
            ]);
        }
        return redirect()->route('salesman.index')->with('Success', 'Salesman updated successfully.');
    }

    public function destroy(salesman $salesman)
    {
        $salesman->update([
            'deleted_by' => Auth::user()->id
        ]);
        $salesman->delete();
        return redirect()->route('salesman.index')->with('Success', 'Salesman deleted successfully.');
    }

    public function print()
    {
        $Salesmans = Salesman::all();

        if ($Salesmans->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($Salesmans as $Salesman) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Salesman->code;
            $objectJSON["description"] = $Salesman->descr;
            $objectJSON["status"] = $Salesman->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $SalesmansJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintSalesman" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $SalesmansJSON);
        $JasperFileName = "ReportPrintSalesman" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Salesman Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function download()
    {
        return "hi";
    }
}
