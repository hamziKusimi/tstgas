<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Driver;
use Auth;
use App\User;  
class DriverController extends Controller
{
    public function index(){

        $data["drivers"] = Driver::get();
        $data["page_title"] = "Driver Type Item Listings";
        $data["bclvl1"] = "Driver Type Item Listings";
        $data["bclvl1_url"] = route('drivers.index');

        return view('cylindermaster.drivers.index', $data);
    }

    public function create(){
        $data["usercode"] = User::orderBy('usercode')->pluck('usercode', 'usercode');
        $data["page_title"] = "Add Driver";
        $data["bclvl1"] = "Driver Type Item Listings";
        $data["bclvl1_url"] = route('drivers.index');
        $data["bclvl2"] = "Add Driver Type";
        $data["bclvl2_url"] = route('drivers.create');

        return view('cylindermaster.drivers.create', $data);
    }

    public function store(Request $request)
    {
        $driver = Driver::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('drivers.index')->with('Success', 'Driver Type created successfully.');

    }

    public function edit($id){
        $data["usercode"] = User::orderBy('usercode')->pluck('usercode', 'usercode');
        $data["driver"] = Driver::find($id);
        $data["page_title"] = "Edit Driver Type";
        $data["bclvl1"] = "Driver Type Item Listings";
        $data["bclvl1_url"] = route('drivers.index');
        $data["bclvl2"] = "Edit Driver Type";
        $data["bclvl2_url"] = route('drivers.edit', ['id'=>$id]);

        return view('cylindermaster.drivers.edit', $data);
    }

    public function update(Request $request, $id)
    {
        
        $driver = Driver::find($id);
        if (isset($driver)) {
            $driver->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('drivers.index')->with('Success', 'Driver Type updated successfully.');
    }

    public function destroy(driver $driver)
    {
        $driver->update([
            'deleted_by' => Auth::user()->id
        ]);
        $driver->delete();
        return redirect()->route('drivers.index')->with('Success', 'Driver Type deleted successfully.');
    }
}
