<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Proformainvoices;
use App\Model\Proformainvoicedts;
use App\Model\Debtor;
use App\Model\PrintedIndexView;use App\Model\DocumentSetup;
use App\Model\Cylinder;
use Carbon\Carbon;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Location;
use App\Model\Cylindercategory;
use App\Model\SystemSetup;
use App\Model\Stockcode;
use App\Model\CustomObject\Transaction;
use App\Model\DeliveryNote;
use App\Model\View\CylinderInvProcessing;
use App\Model\Gasrack;
use App\Model\Salesman;
use PDF;
use Storage;
use App\Model\View\GasrackInvProcessing;
use App\Model\TaxCode;
use App\Model\Uom;
use App\Model\Area;

class ProformaInvoiceController extends Controller
{
    public function index()
    {
        $data["proformas"] = Proformainvoices::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Proformainvoices::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Proforma Sales')->pluck('printed_by');
        $data["page_title"] = "Proforma Listing";
        $data["bclvl1"] = "Proforma Listing";
        $data["bclvl1_url"] = route('proformainvoices.index');
        return view('dailypro.proforma.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["proformasearch"] = Proformainvoices::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('credit_term', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('do_no', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Proformainvoices::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Proforma Sales')->pluck('printed_by');
        $data["page_title"] = "Proforma Listing";
        $data["bclvl1"] = "Proforma Listing";
        $data["bclvl1_url"] = route('proformainvoices.index');
        return view('dailypro.proforma.index', $data);
    }

    public function api_store(Request $request)
    {
        $data = Proformainvoices::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('do_no', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('refno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $runningNumber = DocumentSetup::findByName('Proforma Invoice');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();
        $product = Cylinder::whereNotNull('barcode')->pluck('serial');

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['taxCodes'] = $taxCodes;
        $data['runningNumber'] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Proforma Invoice";
        $data["bclvl1"] = "Proforma Invoice Listing";
        $data["bclvl1_url"] = route('proformainvoices.index');
        $data["bclvl2"] = "Add Proforma Invoice";
        $data["bclvl2_url"] = route('proformainvoices.create');
        // return response()->json($stockcode);
        return view('dailypro.proforma.create', $data);
    }

    public function editRoute($id)
    {
        $resources = Cylinder::where('id', $id)->get();

        return response()->json($resources);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $invoice = Proformainvoices::create([
            'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
            'refinvdate' => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
            'reference_no' => !empty($request['reference_no']) ? $request['reference_no'] : '',
            'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
            'credit_limit' => !empty($request['credit_limit']) ? $request['credit_limit'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'currency' => !empty($request['currency']) ? $request['currency'] : '',
            'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
            'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
            'created_by' => Auth::user()->id
        ]);

        // dd(count($request['subject']));
        if (count($request['subject']) > 1) {
            for ($i = 1; $i < count($request['subject']); $i++) {
                $dt = Proformainvoicedts::create([
                    'doc_no' => $invoice->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'serial' => $request['serial'][$i],
                    'barcode' => $request['barcode'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'product' => $request['product'][$i],
                    'type' => $request['type'][$i],
                    'quantity' => $request['quantity'][$i],
                    'uom' => $request['uom'][$i],
                    'unit_price' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        $invoiceDoc = DocumentSetup::findByName('Proforma Invoice');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        return redirect()->route('proformainvoices.edit', ['id' => $invoice])->with('Success', 'Proforma added successfully');
    }

    public function edit($id)
    {
        $invoice = Proformainvoices::find($id);
        $contents = Proformainvoicedts::where('doc_no', $invoice->docno)->get();
        $selectedDebitor = Debtor::findByAcode($invoice->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();;
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $systemsetup = SystemSetup::first();
        $productval = Cylinder::pluck('serial', 'serial');
        $product = Cylinder::pluck('serial');
        $runningNumber = DocumentSetup::findByName('Proforma Invoice');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();
        $product = Cylinder::whereNotNull('barcode')->pluck('serial');


        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['item'] = $invoice;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;
        $data['productval'] = $productval;
        $data["page_title"] = "Proforma Invoice Listing";
        $data["bclvl1"] = "Proforma Invoice Listing";
        $data["bclvl1_url"] = route('proformainvoices.index');
        // $data["bclvl2"] = "Edit Invoices";
        // $data["bclvl2_url"] = route('invoices.edit');

        // return response()->json($data);

        return view('dailypro.proforma.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // update acc_invmt row
        $invoice = Proformainvoices::find($id);
        if (isset($invoice)) {

            // update invoice
            $invoice->update([
                'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                'refinvdate' => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
                'reference_no' => !empty($request['reference_no']) ? $request['reference_no'] : '',
                'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
                'credit_limit' => !empty($request['credit_limit']) ? $request['credit_limit'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'created_by' => Auth::user()->id
            ]);

            if (count($request['serial']) > 1) {
                for ($i = 1; $i < count($request['serial']); $i++) {

                    $dt = Proformainvoicedts::find($request['item_id'][$i]);

                    if (isset($dt)) {

                        $dt->update([
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        $dt = Proformainvoicedts::create([
                            'doc_no' => $invoice->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }


        return redirect()->route('proformainvoices.edit', ['id' => $id])->with('Success', 'Invoice updated successfully');
    }


    public function destroy($id)
    {
        $proformainvoices = Proformainvoices::find($id);
        $proformainvoices->update([
            'deleted_by' => Auth::user()->id
        ]);
        $proformainvoices->delete();
        $docno = $proformainvoices->docno;

        $Proformainvoicedts = Proformainvoicedts::where('doc_no', $docno);
        $Proformainvoicedts->update([
            'deleted_by' => Auth::user()->id
        ]);
        $Proformainvoicedts->delete();
        return redirect()->route('proformainvoices.index')->with('Success', 'Proforma deleted successfully');
    }

    public function destroyData($id)
    {
        $data = Proformainvoicedts::find($id);

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function print($id)
    {
        $data = [];


        $invoice = Proformainvoices::find($id);
        $contents = Proformainvoicedts::where('doc_no', $invoice->docno)->get();
        $sytemsetups = Systemsetup::first();
        $data['items'] = $invoice;
        $data['contents'] = $contents;
        $data['sytemsetups'] = $sytemsetups;

        // return view('dailypro.proforma.print', $data);
        $pdf = PDF::loadView('dailypro.proforma.print',  $data)->setPaper('A4');
        return $pdf->inline();
    }

    public function printIndex(Request $request)
    {
        $data = [];
        $amount = 0;
        if ($request->Invoice_rcv == "listing") {
            $Invoice = $this->getInvoiceQuery($request);

            if ($Invoice->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }
            foreach ($Invoice as $iv) {

                $amount = $amount + $iv->amount;
            }

            $data['invoiceJSON'] = $Invoice;
            $data['totalamount'] = $amount;
            $pdf = PDF::loadView('dailypro.proforma.listing',  $data)->setPaper('A4');

            return $pdf->inline();
        } else if ($request->Invoice_rcv == "summary") {
            $Invoice = $this->getInvoiceQuery($request);
            if ($Invoice->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }

            foreach ($Invoice as $iv) {
                $amount = $amount + $iv->amount;
            }

            $data['invoiceJSON'] = $Invoice;
            $data['totalamount'] = $amount;
            $pdf = PDF::loadView('dailypro.proforma.summary',  $data)->setPaper('A4');

            return $pdf->inline();
        } else {
            $Invoice = $this->getInvoiceQueryByBatch($request);
            if ($Invoice->isEmpty()) {
                return "<script>alert('No Record Found');window.close();</script>";
            }
            return $this->printByBatch($Invoice);
        }
    }


    public function getInvoiceQuery(Request $request)
    {
        return Proformainvoices::join('proformainvoicedts', 'proformainvoices.docno', '=', 'proformainvoicedts.doc_no')
            ->selectRaw("`proformainvoices`.`docno`          AS `docno`,
        `proformainvoices`.`date`           AS `date`,
        `proformainvoices`.`do_no`          AS `do_no`,
        `proformainvoices`.`account_code`   AS `account_code`,
        `proformainvoices`.`account_code`   AS `debtor`,
        `proformainvoices`.`name`           AS `name`,
        `proformainvoices`.`footer`           AS `footer`,
        CONCAT(CONCAT(`proformainvoices`.`docno`),', ',DATE_FORMAT(`proformainvoices`.`date`, '%d-%m-%Y'),', ',`proformainvoices`.`account_code`,', ',`proformainvoices`.`name`) AS `details`,
        `proformainvoicedts`.`subject`      AS `description`,
        `proformainvoicedts`.`id`           AS `id`,
        `proformainvoicedts`.`doc_no`       AS `doc_no`,
        `proformainvoicedts`.`quantity`     AS `quantity`,
        `proformainvoicedts`.`uom`          AS `uom`,
        `proformainvoicedts`.`unit_price`   AS `unit_price`,
        `proformainvoicedts`.`amount`       AS `amount`,
        `proformainvoicedts`.`taxed_amount` AS `tax_amount`,
        `proformainvoicedts`.`subject`      AS `subject`,
        `proformainvoicedts`.`details`      AS `DETAIL`,
        proformainvoices.amount AS totalamt")
            ->where(function ($query) use ($request) {
                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->DO_Chkbx_1 == "on") {
                    if ($request->DO_to_1 == null) {
                        $query->where('do_no', "=", $request->DO_frm_1);
                    } else {
                        $query->whereBetween('do_no', [$request->DO_frm_1, $request->DO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

}
