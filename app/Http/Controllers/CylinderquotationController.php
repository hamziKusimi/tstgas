<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CylinderQuotation;
use App\Model\CylinderQuotationdt;
use App\Model\DocumentSetup;
use App\Model\Cylinder;
use Carbon\Carbon;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Debtor;
use App\Model\Location;
use App\Model\Cylindercategory;
use App\Model\SystemSetup;
use App\Model\UserSetup;
use App\Model\Stockcode;
use App\Model\CustomObject\Transaction;
use App\Model\DeliveryNote;
use App\Model\View\CylinderInvProcessing;
use App\Model\Gasrack;
use App\Model\Salesman;
use App\Model\PrintedIndexView;
use PDF;
use Storage;
use App\Model\View\GasrackInvProcessing;
use App\Model\Area;

class CylinderquotationController extends Controller
{
    public function index()
    {
        $data["cylinderquotations"] = CylinderQuotation::orderBy('docno', 'desc')->paginate(15);
        $data["customers"] = CylinderQuotation::pluck('name', 'account_code');
        $data["dates"] = CylinderQuotation::pluck('date', 'date');
        $data["docno_select"] = CylinderQuotation::orderBy('docno')->pluck('docno', 'docno');
        $data["print"] = PrintedIndexView::where('index', 'Quotation')->pluck('printed_by');
        $data["debtor_select"] = Debtor::where('custom2', '=', 'Auto Invoice Generation')
            ->where('customval2', '=', 'on')->get()->pluck('Detail', 'accountcode');
        $data["page_title"] = "Quotation Listing";
        $data["bclvl1"] = "Quotation Listing";
        $data["bclvl1_url"] = route('cylinderquotations.index');
        return view('dailypro.cylinderquotations.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["quotationsearch"] = CylinderQuotation::select('*')
        ->where(function ($query) use ($request) {
            $query->orWhere('docno', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('account_code', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('name', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('attention', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('contact', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('cbinvdono', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('quot', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('taxed_amount', 'LIKE', '%'.$request['search'].'%')
                  ->orWhere('date', 'LIKE', '%'.$request['search'].'%');
        })
        ->orderBy('date')
        ->get();

        $data["docno_select"] = CylinderQuotation::pluck('docno', 'docno');
        $data["debtor_select"] = Debtor::pluck('accountcode', 'accountcode');
        $data["page_title"] = "Quotation Listing";
        $data["bclvl1"] = "Quotation Listing";
        $data["bclvl1_url"] = route('cylinderquotations.index');
        return view('dailypro.cylinderquotations.index', $data);

    }

    public function getDetailsBySerial($serial)
    {
        $product = Cylinder::where('serial', $serial)->first();
        return response()->json($product);
    }

    public function api_store(Request $request)
    {
        $data = CylinderQuotation::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('do_no', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('refno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isDebtor()->get();
        $runningNumber = DocumentSetup::findByName('Cylinder Quotation');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountcode');
        $systemsetup = SystemSetup::first();
        // $product = Cylinder::whereNotNull('serial')->get(['barcode','serial','cat_id', 'prod_id', 'capacity', 'testpressure']);
        $product = Cylinder::whereNotNull('barcode')->pluck('serial');

        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['masterCodes'] = $masterCodes;
        $data['runningNumber'] = $runningNumber;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;

        // return response()->json($data);
        $data["page_title"] = "Quotation Listing";
        $data["bclvl1"] = "Quotation Listing";
        $data["bclvl1_url"] = route('cylinderquotations.index');
        $data["bclvl2"] = "Add Quotation";
        $data["bclvl2_url"] = route('cylinderquotations.create');
        return view('dailypro.cylinderquotations.create', $data);
    }

    public function editRoute($id)
    {
        $resources = Cylinder::where('id', $id)->get();

        return response()->json($resources);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $invoice = CylinderQuotation::create([
            'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
            'refinvdate' => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
            'refno' => !empty($request['reference_no']) ? $request['reference_no'] : '',
            'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
            'credit_limit' => !empty($request['credit_limit']) ? $request['credit_limit'] : '',
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'currency' => !empty($request['currency']) ? $request['currency'] : '',
            'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
            'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
            'created_by' => Auth::user()->id,
            'custom1' => $request['custom1'],
            'custom2' => $request['custom2'],
            'custom3' => $request['custom3'],
            'custom4' => $request['custom4'],
            'custom5' => $request['custom5'],
            'custom6' => $request['custom6'],
            'custom7' => $request['custom7'],
            'custom8' => $request['custom8'],
            'custom9' => $request['custom9'],
            'custom10' => $request['custom10'],
            'custom11' => $request['custom11'],
            'custom12' => $request['custom12'],
            'custom13' => $request['custom13'],
            'custom14' => $request['custom14'],
            'custom15' => $request['custom15'],
            'custom16' => $request['custom16'],
            'custom17' => $request['custom17'],
            'custom18' => $request['custom18'],
            'custom19' => $request['custom19'],
            'custom20' => $request['custom20'],
            'custom21' => $request['custom21'],
            'custom22' => $request['custom22'],
            'custom23' => $request['custom23'],
            'custom24' => $request['custom24'],
            'custom25' => $request['custom25'],
            'customval1' => isset($request['customval1'])?$request['customval1']:null,
            'customval2' => isset($request['customval2'])?$request['customval2']:null,
            'customval3' => isset($request['customval3'])?$request['customval3']:null,
            'customval4' => isset($request['customval4'])?$request['customval4']:null,
            'customval5' => isset($request['customval5'])?$request['customval5']:null,
            'customval6' => isset($request['customval6'])?$request['customval6']:null,
            'customval7' => isset($request['customval7'])?$request['customval7']:null,
            'customval8' => isset($request['customval8'])?$request['customval8']:null,
            'customval9' => isset($request['customval9'])?$request['customval9']:null,
            'customval10' => isset($request['customval10'])?$request['customval10']:null,
            'customval11' => isset($request['customval11'])?$request['customval11']:null,
            'customval12' => isset($request['customval12'])?$request['customval12']:null,
            'customval13' => isset($request['customval13'])?$request['customval13']:null,
            'customval14' => isset($request['customval14'])?$request['customval14']:null,
            'customval15' => isset($request['customval15'])?$request['customval15']:null,
            'customval16' => isset($request['customval16'])?$request['customval16']:null,
            'customval17' => isset($request['customval17'])?$request['customval17']:null,
            'customval18' => isset($request['customval18'])?$request['customval18']:null,
            'customval19' => isset($request['customval19'])?$request['customval19']:null,
            'customval20' => isset($request['customval20'])?$request['customval20']:null,
            'customval21' => isset($request['customval21'])?$request['customval21']:null,
            'customval22' => isset($request['customval22'])?$request['customval22']:null,
            'customval23' => isset($request['customval23'])?$request['customval23']:null,
            'customval24' => isset($request['customval24'])?$request['customval24']:null,
            'customval25' => isset($request['customval25'])?$request['customval25']:null
        ]);

        // dd(count($request['subject']));
        if (count($request['subject']) > 1) {
            for ($i = 1; $i < count($request['subject']); $i++) {
                $dt = CylinderQuotationdt::create([
                    'doc_no' => $invoice->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'serial' => $request['serial'][$i],
                    'barcode' => $request['barcode'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'product' => $request['product'][$i],
                    'type' => $request['type'][$i],
                    'quantity' => $request['quantity'][$i],
                    'uom' => $request['uom'][$i],
                    'unit_price' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        $invoiceDoc = DocumentSetup::findByName('Cylinder Quotation');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        return redirect()->route('cylinderquotations.edit', ['id' => $invoice])->with('Success', 'Quotation added successfully');
    }

    public function edit($id)
    {
        $invoice = CylinderQuotation::find($id);
        $contents = CylinderQuotationdt::where('doc_no', $invoice->docno)->get();

        // return response()->json($invoice->doc_no);
        $selectedDebitor = Debtor::findByAcode($invoice->account_code);
        $masterCodes = AccountMastercode::isDebtor()->get();;
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Debtor::all()->pluck('accountCode');
        $systemsetup = SystemSetup::first();
        $productval = Cylinder::pluck('serial', 'serial');
        $product = Cylinder::pluck('serial');
        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['item'] = $invoice;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;
        $data['productval'] = $productval;
        $data["page_title"] = "Edit Quotation Listing";
        $data["bclvl1"] = "Quotation Listing";
        $data["bclvl1_url"] = route('cylinderquotations.index');
        // $data["bclvl2"] = "Edit Invoices";
        // $data["bclvl2_url"] = route('invoices.edit');

        // return response()->json($data);

        return view('dailypro.cylinderquotations.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // update acc_invmt row
        $invoice = CylinderQuotation::find($id);
        if (isset($invoice)) {

            // update invoice
            $invoice->update([
                'docno' => !empty($request['doc_no']) ? $request['doc_no'] : '',
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                'refinvdate' => Carbon::createFromFormat('d/m/Y', $request['reference_date']),
                'refno' => !empty($request['reference_no']) ? $request['reference_no'] : '',
                'do_no' => !empty($request['do_no']) ? $request['do_no'] : '',
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
                'credit_limit' => !empty($request['credit_limit']) ? $request['credit_limit'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'created_by' => Auth::user()->id,
                'custom1' => $request['custom1'],
                'custom2' => $request['custom2'],
                'custom3' => $request['custom3'],
                'custom4' => $request['custom4'],
                'custom5' => $request['custom5'],
                'custom6' => $request['custom6'],
                'custom7' => $request['custom7'],
                'custom8' => $request['custom8'],
                'custom9' => $request['custom9'],
                'custom10' => $request['custom10'],
                'custom11' => $request['custom11'],
                'custom12' => $request['custom12'],
                'custom13' => $request['custom13'],
                'custom14' => $request['custom14'],
                'custom15' => $request['custom15'],
                'custom16' => $request['custom16'],
                'custom17' => $request['custom17'],
                'custom18' => $request['custom18'],
                'custom19' => $request['custom19'],
                'custom20' => $request['custom20'],
                'custom21' => $request['custom21'],
                'custom22' => $request['custom22'],
                'custom23' => $request['custom23'],
                'custom24' => $request['custom24'],
                'custom25' => $request['custom25'],
                'customval1' => isset($request['customval1'])?$request['customval1']:null,
                'customval2' => isset($request['customval2'])?$request['customval2']:null,
                'customval3' => isset($request['customval3'])?$request['customval3']:null,
                'customval4' => isset($request['customval4'])?$request['customval4']:null,
                'customval5' => isset($request['customval5'])?$request['customval5']:null,
                'customval6' => isset($request['customval6'])?$request['customval6']:null,
                'customval7' => isset($request['customval7'])?$request['customval7']:null,
                'customval8' => isset($request['customval8'])?$request['customval8']:null,
                'customval9' => isset($request['customval9'])?$request['customval9']:null,
                'customval10' => isset($request['customval10'])?$request['customval10']:null,
                'customval11' => isset($request['customval11'])?$request['customval11']:null,
                'customval12' => isset($request['customval12'])?$request['customval12']:null,
                'customval13' => isset($request['customval13'])?$request['customval13']:null,
                'customval14' => isset($request['customval14'])?$request['customval14']:null,
                'customval15' => isset($request['customval15'])?$request['customval15']:null,
                'customval16' => isset($request['customval16'])?$request['customval16']:null,
                'customval17' => isset($request['customval17'])?$request['customval17']:null,
                'customval18' => isset($request['customval18'])?$request['customval18']:null,
                'customval19' => isset($request['customval19'])?$request['customval19']:null,
                'customval20' => isset($request['customval20'])?$request['customval20']:null,
                'customval21' => isset($request['customval21'])?$request['customval21']:null,
                'customval22' => isset($request['customval22'])?$request['customval22']:null,
                'customval23' => isset($request['customval23'])?$request['customval23']:null,
                'customval24' => isset($request['customval24'])?$request['customval24']:null,
                'customval25' => isset($request['customval25'])?$request['customval25']:null
            ]);

            if (count($request['serial']) > 1) {
                for ($i = 1; $i < count($request['serial']); $i++) {

                    $dt = CylinderQuotationdt::find($request['item_id'][$i]);

                    if (isset($dt)) {

                        $dt->update([
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        $dt = CylinderQuotationdt::create([
                            'doc_no' => $invoice->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'quantity' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }


        return redirect()->route('cylinderquotations.edit', ['id' => $id])->with('Success', 'Invoice updated successfully');
    }

    public function print($id)
    {
        $data = [];

        $invoice = CylinderQuotation::find($id);
        $contents = CylinderQuotationdt::where('doc_no', $invoice->docno)->get();
        $sytemsetups = Systemsetup::first();
        $data['items'] = $invoice;
        $data['contents'] = $contents;
        $data['sytemsetups'] = $sytemsetups;

        $pdf = PDF::loadView('dailypro.cylinderquotations.print',  $data)->setPaper('A4');
        return $pdf->inline();
    }

    public function printSummaryListing(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Cylinder Quotation')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Cylinder Quotation')->update([
                'index' => "Cylinder Quotation",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Cylinder Quotation",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $PurchaseOrder = $this->getQTQuery($request);

        if ($PurchaseOrder->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->Quotation_rcv == "listing") {
            foreach($PurchaseOrder as $PO){

                $amount = $amount + $PO->amount;
            }

            $data['QuotationJSON'] = $PurchaseOrder;
            $data['totalamount'] = $amount;
            $pdf = PDF::loadView('dailypro.cylinderquotations.listing',  $data)->setPaper('A4');
            return $pdf->inline();
            // return view('dailypro/cylinderquotations.listing', $data);


        } else {

            $data['QuotationJSON'] = $PurchaseOrder;

            $pdf = PDF::loadView('dailypro.cylinderquotations.summary',  $data)->setPaper('A4');
            return $pdf->inline();
            // return view('dailypro/cylinderquotations.summary', $data);
        }
    }


    public function getQTQuery(Request $request)
    {
        return CylinderQuotation::join('cylinder_quotationdts', 'cylinder_quotations.docno', '=', 'cylinder_quotationdts.doc_no')
        ->join('cylinders', 'cylinder_quotationdts.barcode', '=', 'cylinders.barcode')
        ->selectRaw("
        `cylinder_quotations`.`docno`          AS `docno`,
        `cylinder_quotations`.`date`           AS `date`,
        `cylinder_quotations`.`refinvdate`          AS `cbinvdono`,
        `cylinder_quotations`.`account_code`   AS `account_code`,
        `cylinder_quotations`.`account_code`   AS `debtor`,
        `cylinder_quotations`.`name`           AS `name`,
        CONCAT(CONCAT(`cylinder_quotations`.`docno`),', ',DATE_FORMAT(`cylinder_quotations`.`date`,'%d-%m-%Y'),', ',`cylinder_quotations`.`account_code`,', ',`cylinder_quotations`.`name`) AS `details`,
        `cylinder_quotationdts`.`subject`      AS `description`,
        `cylinder_quotationdts`.`id`           AS `id`,
        `cylinder_quotationdts`.`doc_no`       AS `doc_no`,
        `cylinder_quotationdts`.`barcode`    AS `item_code`,
        `cylinder_quotationdts`.`quantity`          AS `quantity`,
        `cylinder_quotationdts`.`uom`          AS `uom`,
        `cylinder_quotationdts`.`unit_price`       AS `uprice`,
        `cylinder_quotationdts`.`amount`       AS `amount`,
        `cylinder_quotationdts`.`taxed_amount` AS `tax_amount`,
        `cylinder_quotationdts`.`subject`      AS `subject`,
        `cylinder_quotationdts`.`details`      AS `DETAIL`,
        cylinder_quotations.amount AS totalamt")
            ->where(function ($query) use ($request) {

                $query->whereNull('cylinder_quotations.deleted_at');
                $query->whereNull('cylinder_quotationdts.deleted_at');

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->CB_INV_DO_Chkbx_1 == "on") {
                    if ($request->CB_INV_DO_to_1 == null) {
                        $query->where('cbinvdono', "=", $request->CB_INV_DO_frm_1);
                    } else {
                        $query->whereBetween('CB_INV_DOno', [$request->CB_INV_DO_frm_1, $request->CB_INV_DO_to_1]);
                    }
                }

                if ($request->debCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->debCode_frm, $request->debCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

    public function destroy($id)
    {
        $cylinderQuotation = CylinderQuotation::find($id);

        $cylinderQuotation->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylinderQuotation->delete();
        $docno = $cylinderQuotation->docno;

        $cylinderQuotationdt = CylinderQuotationdt::where('doc_no', $docno);
        $cylinderQuotationdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylinderQuotationdt->delete();
        return redirect()->route('cylinderquotations.index')->with('Success', 'Proforma deleted successfully');
    }

    public function destroyData($id)
    {
        $data = CylinderQuotationdt::find($id);

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

}
