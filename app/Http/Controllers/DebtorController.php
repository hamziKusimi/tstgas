<?php

namespace App\Http\Controllers;

use App\Model\Debtor;
use Illuminate\Http\Request;
use App\Http\Requests\DebtorRequest;
use App\Model\PrintedIndexView;
use App\Model\SystemSetup;
use App\Model\Salesman;
use App\Model\Area;
use PHPJasper\PHPJasper;
use Auth;
use Carbon\Carbon;
use DB;
use Redirect;

class DebtorController extends Controller
{
    public function api_get()
    {
        $resources = Debtor::get();

        return response()->json($resources);
    }

    public function index()
    {
        $data["debtors"] = Debtor::get();
        $data["page_title"] = "Debtor";
        $data["bclvl1"] = "Debtor Item Listing";
        $data["bclvl1_url"] = route('debtors.index');
        return view('stockmaster.debtors.index', $data);
    }

    public function create()
    {
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = [];//Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        // dd($data["salesmans"]);
        $data["page_title"] = "Add Debtor";
        $data["bclvl1"] = "Debtor Item Listing";
        $data["bclvl1_url"] = route('debtors.index');
        $data["bclvl2"] = "Add Debtor";
        $data["bclvll2_url"] = route('debtors.create');
        // return response()->json($data);
        return view('stockmaster.debtors.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['D_ADDRESS']);
        $request['D_ADDR1'] = isset($address[0]) ? $address[0] : '';
        $request['D_ADDR2'] = isset($address[1]) ? $address[1] : '';
        $request['D_ADDR3'] = isset($address[2]) ? $address[2] : '';
        $request['D_ADDR4'] = isset($address[3]) ? $address[3] : '';

        // return response()->json($request);
        Debtor::create([
            'accountcode' => $request['D_ACODE'],
            'D_DC' => 'D',
            'name' => $request["D_NAME"],
            'def_price' => $request["def_price"],
            'salesman' => $request["salesman"],
            'area' => $request["area"],
            'address1' => $request['D_ADDR1'],
            'address2' => $request['D_ADDR2'],
            'address3' => $request['D_ADDR3'],
            'address4' => $request['D_ADDR4'],
            'tel' => $request["D_PHONE"],
            'fax' => $request["D_FAX"],
            'hp' => $request["D_HP"],
            'email' => $request["D_EMAIL"],
            'cterm' => $request["D_CTERM"],
            'climit' => $request["D_CLIMIT"],
            'gstno' => $request["d_gstno"],
            'brnno' => $request["d_brn"],
            'memo' => $request["D_MEMO"],
            'custom1' => $request["custom1"],
            'custom2' => $request["custom2"],
            'custom3' => $request["custom3"],
            'custom4' => $request["custom4"],
            'custom5' => $request["custom5"],
            'customval1' => $request["customval1"],
            'customval2' => $request["customval2"],
            'customval3' => $request["customval3"],
            'customval4' => $request["customval4"],
            'customval5' => $request["customval5"],
            'active' => $request['active'],
            'created_by' => Auth::user()->id
        ]);
        return redirect()->route('debtors.index')->with('Success', 'Debtor created successfully.');
    }

    public function api_store(Request $request)
    {
        $data = [];
        foreach ($request->toArray() as $key => $req)
            $data[$key] = is_null($req) ? '' : $req;

        $climit = !empty($request['D_CLIMIT']) ? $request['D_CLIMIT'] : 0.00;
        if (strpos($climit, ',') !== false)
            $climit = (float) str_replace(',', '', $climit);

        $debtor = Debtor::create([
            'C_DC' => 'D',
            'accountcode' => $request['D_ACODE'],
            'def_price' => $request["def_price"],
            'salesman' => $request["salesman"],
            'area' => $request["area"],
            'name' => $request["D_NAME"],
            'address1' => $request['D_ADDR1'],
            'address2' => $request['D_ADDR2'],
            'address3' => $request['D_ADDR3'],
            'address4' => $request['D_ADDR4'],
            'tel' => $request["D_TEL"],
            'fax' => $request["D_FAX"],
            'email' => $request["D_EMAIL"],
            'cterm' => $request["C_CTERM"],
            'climit' => $climit,
            'gstno' => $request["d_gstno"],
            'brnno' => $request["d_brn"],
            'memo' => $request["D_MEMO"],
            'active' => $request['active'],
            'created_by' => Auth::user()->id

        ]);

        return $debtor;
    }

    public function edit($id)
    {
        $data["debtor"] = Debtor::find($id);
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Debtor";
        $data["bclvl1"] = "Debtor Item Listing";
        $data["bclvl1_url"] = route('debtors.index');
        $data["bclvl2"] = "Edit Debtor";
        $data["bclvl2_url"] = route('debtors.edit', ['id' => $id]);

        return view('stockmaster.debtors.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['D_ADDRESS']);
        $request['D_ADDR1'] = isset($address[0]) ? $address[0] : '';
        $request['D_ADDR2'] = isset($address[1]) ? $address[1] : '';
        $request['D_ADDR3'] = isset($address[2]) ? $address[2] : '';
        $request['D_ADDR4'] = isset($address[3]) ? $address[3] : '';

        $debtor = Debtor::find($id);
        $debtor->update([
            'accountcode' => $request['D_ACODE'],
            'D_DC' => 'D',
            'name' => $request["D_NAME"],
            'def_price' => $request["def_price"],
            'salesman' => $request["salesman"],
            'area' => $request["area"],
            'address1' => $request['D_ADDR1'],
            'address2' => $request['D_ADDR2'],
            'address3' => $request['D_ADDR3'],
            'address4' => $request['D_ADDR4'],
            'tel' => $request["D_PHONE"],
            'fax' => $request["D_FAX"],
            'hp' => $request["D_HP"],
            'email' => $request["D_EMAIL"],
            'cterm' => $request["D_CTERM"],
            'climit' => $request["D_CLIMIT"],
            'gstno' => $request["d_gstno"],
            'brnno' => $request["d_brn"],
            'memo' => $request["D_MEMO"],
            'custom1' => $request["custom1"],
            'custom2' => $request["custom2"],
            'custom3' => $request["custom3"],
            'custom4' => $request["custom4"],
            'custom5' => $request["custom5"],
            'customval1' => $request["customval1"],
            'customval2' => $request["customval2"],
            'customval3' => $request["customval3"],
            'customval4' => $request["customval4"],
            'customval5' => $request["customval5"],
            'active' => $request['active'],
            'updated_by' => Auth::user()->id

        ]);

        return redirect()->route('debtors.index')->with('Success', 'Debtor updated successfully.');
    }

    public function destroy(debtor $debtor)
    {
        $debtor->update([
            'deleted_by' => Auth::user()->id
        ]);
        $debtor->delete();
        return redirect()->route('debtors.index')->with('Success', 'Debtor deleted successfully.');
    }

    public function print(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Debtor Master Listing')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Debtor Master Listing')->update([
                'index' => "Debtor Master Listing",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Debtor Master Listing",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        if ($request->AC_Code_Chkbx == "on") {
            $debtors = Debtor::whereBetween('accountcode', [$request->AC_Code_frm, $request->AC_Code_to])
                ->orderBy('accountcode')
                ->get();
        } else {
            $debtors = Debtor::orderBy('accountcode')->get();
        }

        if ($debtors->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        foreach ($debtors as $debtor) {
            $debtor->details = $debtor->name;
            if ($request->Address_Checkbox == "on") {
                $debtor->details = $debtor->details . PHP_EOL . $debtor->address1;
            }
            if ($request->Tel_Fax == "on") {
                $debtor->details = $debtor->details . PHP_EOL . "Tel:" . $debtor->tel;
                $debtor->details = $debtor->details . "  Fax:" . $debtor->fax;
            }
        }

        $debtorsJSON = '{"data" :' . json_encode($debtors) . '}';

        $JsonFileName = "DebtorMasterListing" . date("Ymdhisa") . Auth::user()->id;

        file_put_contents(base_path('resources/reporting/Debtor/' . $JsonFileName . '.json'), $debtorsJSON);
        $JasperFileName = "DebtorMasterListing" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        $content = file_get_contents($file);
        header('Content-type: application/pdf');
        header('Content-Length: ' . strlen($content));
        header('Content-Disposition: inline; filename="' . $JasperFileName . '.pdf"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');
        ini_set('zlib.output_compression', '0');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/Debtor/DebtorMasterListing.jrxml';
        $output = base_path() . '/resources/reporting/Debtor/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/Debtor/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();


        unlink($data_file);
        $file = base_path() . '/resources/reporting/Debtor/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function codereplacement(Request $request)
    {
        $Debtor = Debtor::where('accountcode', '=', $request->old_code)->get();

        if ($Debtor->isEmpty()) {
            return Redirect::back()->with([
                'Error' => 'No Debtor Code Found', 'old_code' => $request->old_code,
                'new_code' => $request->new_code
            ]);
        }

        DB::table('debtors')->where('accountcode', $request->old_code)->update(['accountcode' => $request->new_code]);
        DB::table('cashsalesdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('cashbilldts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('invoice_data')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('deliveryorderdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('salesorderdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('quotationdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('salesreturndts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('delivery_notedts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('delivery_note_grdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('return_notedts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('return_note_grdts')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);

        DB::table('cashsales')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('cashbills')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('invoices')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('deliveryorders')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('salesorders')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('quotations')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('salesreturns')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('delivery_notes')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('return_notes')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);
        DB::table('cylinder_invoices')->where('account_code', $request->old_code)->update(['account_code' => $request->new_code]);

        return redirect()->route('codereplacement.debtorcode')
            ->with([
                'Success' => 'Debtor Code Sucessfully Updated', 'old_code' => $request->old_code,
                'new_code' => $request->new_code
            ]);
    }

    public function getSalesman($id)
    {
        $result = Debtor::where('accountcode', $id)->pluck('salesman');

        return $result;
    }
}
