<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;
use DateTime;
use App\Model\Repair;
use App\Model\DocumentSetup;
use Carbon\Carbon;
use App\Model\CustomObject\Transaction;

class RepairController extends Controller
{
    public function index()
    {
        $data["repairs"] = Repair::orderBy('rp_no', 'desc')->get();
        $data["RP_select"] = Repair::pluck('rp_no', 'rp_no');
        $data["page_title"] = "Repair Item Listings";
        $data["bclvl1"] = "Repair Item Listings";
        $data["bclvl1_url"] = route('repairs.index');
        return view('dailypro.repairs.index', $data);
    }

    public function create()
    {
        $data["page_title"] = "Add Repair";
        $runningNumber = DocumentSetup::findByName('Repair');
        $data['runningNumber'] = $runningNumber;
        $data["bclvl1"] = "Repair Item Listings";
        $data["bclvl1_url"] = route('repairs.index');
        $data["bclvl2"] = "Add Repair";
        $data["bclvl2_url"] = route('repairs.create');

        return view('dailypro.repairs.create', $data);
    }

    public function store(Request $request)
    {
        $repair = Repair::create([
            'rp_no' => $request['rp_no'],
            'tech' => $request['tech'],
            // 'logindate'=>$request['logindate'] ? date('Y-m-d', strtotime($request['logindate'])):null,
            'logindate'=>$request['logindate'] ? date('Y-m-d', strtotime($request['logindate'])):null,
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'completedate' => $request['completedate']?date('Y-m-d', strtotime($request['completedate'])):null,
            'charge_in' => $request['charge_in'],
            'charge_out' => $request['charge_out'],
            'fault' => $request['fault'],
            'created_by'=> Auth::user()->id
        ]);

        $RepairDoc = DocumentSetup::findByName('Repair');
        $RepairDoc->update(['D_LAST_NO' => $RepairDoc->D_LAST_NO + 1]);

        return redirect()->route('repairs.index')->with('Success', 'Repair created successfully.');
    }
    
    public function edit($id)
    {
        $data["repair"] = Repair::find($id);
        $data["page_title"] = "Edit Repair";
        $data["bclvl1"] = "Repair Item Listings";
        $data["bclvl1_url"] = route('repairs.index');
        $data["bclvl2"] = "Edit Repair";
        $data["bclvl2_url"] = route('repairs.edit', ['id'=>$id]);

        return view('dailypro.repairs.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $repair = Repair::find($id);
        $repair->update([
            'rp_no' => $request['rp_no'],
            'tech' => $request['tech'],
            'logindate'=>$request['logindate']?date('Y-m-d', strtotime($request['logindate'])):null,
            'barcode' => $request['barcode'],
            'serial' => $request['serial'],
            'completedate' => $request['completedate']?date('Y-m-d', strtotime($request['completedate'])):null,
            'charge_in' => $request['charge_in'],
            'charge_out' => $request['charge_out'],
            'fault' => $request['fault'],
            'updated_by'=> Auth::user()->id
        ]);

        return redirect()->route('repairs.index')->with('Success', 'Repair updated successfully.');
    }

    public function destroy(repair $repair)
    {
        $repair->update([
            'deleted_by'=>Auth::user()->id
        ]);
        $repair->delete();
        return redirect()->route('repairs.index')->with('Success', 'Repair deleted successfully.');
    }

    public function jasper($id)
    {
        $formattedResource = Repair::select(
            'rp_no as DOC_NO',
            'logindate as DOC_DATE',
            'completedate',
            'tech',
            'barcode',
            'serial',
            'charge_in',
            'charge_out',
            'fault'
        )->where('id', $id)->first();
        $source = 'Repair';
        $jrxml = 'general-bill-Repair.jrxml';
        $reportTitle = 'Repair Report';
        Transaction::generateReportRepair(
            $formattedResource,
            $source,
            $jrxml,
            $reportTitle
        );
    }

    public function print(Request $request)
    {
        $Repair = $this->getRepairQuery($request);
        if ($Repair->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $RepairJSON = $this->getRepairJSON($Repair);
        
        $JsonFileName = "Repairs" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/DailyProcess/Repair/' . $JsonFileName . '.json'), $RepairJSON);
        $JasperFileName = "Repairs" . date("Ymdhisa") . Auth::user()->id;

        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/Repair/RepairReport.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/Repair/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/Repair/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/Repair/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getRepairQuery(Request $request)
    {
        return Repair::select(
            'rp_no',
            'logindate',
            'completedate',
            'tech',
            'barcode',
            'serial',
            'charge_in',
            'charge_out',
            'fault'
        )
            ->where(function ($query) use ($request) {

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('logindate', '=', $dates_from);
                    } else {
                        $query->whereBetween('logindate', [$dates_from, $dates_to]);
                    }
                }

                if ($request->RP_Chkbx == "on") {
                    $query->whereBetween('rp_no', [$request->RP_frm, $request->RP_to]);
                }
            })
            ->orderBy('rp_no')
            ->get();
    }

    public function getRepairJSON($Repairs)
    {
        $dataArray = array();
        $s_n = 1;
        foreach ($Repairs as $Repair) {
            $objectJSON = [];
            $l_date = date("d/m/Y", strtotime($Repair->logindate));
            $c_date = date("d/m/Y", strtotime($Repair->completedate));

            $objectJSON['s_n'] = $s_n;
            $objectJSON['login_date'] = $l_date;
            $objectJSON['complete_date'] = $c_date;
            $objectJSON['technician'] = $Repair->tech;
            $objectJSON['barcode'] = $Repair->barcode;
            $objectJSON['serial_no'] = $Repair->serial;
            $objectJSON['details'] = "<b>Charge in:</b><br>" . $Repair->charge_in .
                "<br><b>Charge out:</b><br>" . $Repair->charge_out;
            $objectJSON['fault'] = $Repair->fault;

            $s_n = $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
