<?php

namespace App\Http\Controllers;

use App\Model\Shackle;
use App\Model\Shackletype;
use App\Model\Manufacturer;

use Illuminate\Http\Request;

use Auth;

class ShackleController extends Controller
{
    public function index()
    {
        $data["shackles"] = Shackle::get();
        $data["page_title"] = "Shackle Item Listing";
        $data["bclvl1"] = "Shackle Item Listing";
        $data['bclvl1_url'] = route('shackles.index');

        return view('cylindermaster.shackles.index', $data);
    }

    public function create()
    {
        $data["shackles"] = Shackle::get();
        $data["shackletypes"] = Shackletype::get();
        $data["manufacturers"] = Manufacturer::get();
        $data["page_title"] = "Add Shackle";
        $data["bclvl1"] = "Shackle Item Listing";
        $data['bclvl1_url'] = route('shackles.index');
        $data["bclvl2"] = "Add Shackle";
        $data['bclvl2_url'] = route('shackles.create');

        return view('cylindermaster.shackles.create', $data);
    }

    public function store(Request $request)
    {
        Shackle::create([
            'barcode'=>$request['barcode'],
            'mfr'=>$request['mfr'],
            'serial'=>$request['serial'],
            'owner'=>$request['owner'],
            'loadlimit'=>$request['loadlimit'],
            'type'=>$request['type'],
            'size'=>$request['size'],
            'descr'=>$request['descr'],
            'coc'=>$request['coc'],
            'testdate'=>$request['testdate']?date('Y-m-d', strtotime($request['testdate'])):null,
            'created_by'=> Auth::user()->id
        ]);

        return redirect()->route('shackles.index')->with('Success','Shackle created succesfully');
    }

    public function edit($id)
    {
        $data["shackles"] = Shackle::get();
        $data["shackle"] = Shackle::find($id);
        $data["shackletypes"] = Shackletype::get();
        $data["manufacturers"] = Manufacturer::get();
        $data["page_title"] = "Edit Shackle";
        $data["bclvl1"] = "Shackle Item Listing";
        $data['bclvl1_url'] = route('shackles.index');
        $data["bclvl2"] = "Edit Shackle";
        $data['bclvl2_url'] = route('shackles.edit', ['id'=>$id]);

        return view('cylindermaster.shackles.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $shackle = Shackle::find($id);
        $shackle->update([
            'barcode'=>$request['barcode'],
            'mfr'=>$request['mfr'],
            'serial'=>$request['serial'],
            'owner'=>$request['owner'],
            'loadlimit'=>$request['loadlimit'],
            'type'=>$request['type'],
            'size'=>$request['size'],
            'descr'=>$request['descr'],
            'coc'=>$request['coc'],
            'testdate'=>$request['testdate']?date('Y-m-d', strtotime($request['testdate'])):null,
            'updated_by'=> Auth::user()->id
        ]);

        return redirect()->route('shackles.index')->with('Success','Shackle updated succesfully');
    }

    public function destroy(shackle $shackle)
    {
        $shackle->update([
            'deleted_by' => Auth::user()->id
        ]);
        $shackle->delete();
        return redirect()->route('shackles.index')->with('Success','Shackle deleted succesfully');
    }
}
