<?php

namespace App\Http\Controllers;

use App\Model\Area;
use App\Model\PrintedIndexView;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Auth;

class AreaController extends Controller
{
    public function index(){

        $data["areas"] = Area::get();
        $data["print"] = PrintedIndexView::where('index', 'Areas')->pluck('printed_by');
        $data["page_title"] = "Area Item Listings";
        $data["bclvl1"] = "Area Item Listings";
        $data["bclvl1_url"] = route('areas.index');

        return view('stockmaster.areas.index', $data);
    }

    public function create(){
        
        $data["page_title"] = "Add Area";
        $data["bclvl1"] = "Area Item Listings";
        $data["bclvl1_url"] = route('areas.index');
        $data["bclvl2"] = "Add Area";
        $data["bclvl2_url"] = route('areas.create');

        return view('stockmaster.areas.create', $data);
    }

    public function store(Request $request){

        $area = Area::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('areas.index')->with('Success', 'Area created successfully.');

    }

    public function edit($id){

        $data["area"] = Area::find($id);
        $data["page_title"] = "Edit Area";
        $data["bclvl1"] = "Area Item Listings";
        $data["bclvl1_url"] = route('areas.index');
        $data["bclvl2"] = "Edit Area";
        $data["bclvl2_url"] = route('areas.edit', ['id'=>$id]);

        return view('stockmaster.areas.edit', $data);
    }

    public function update(Request $request, $id){
        
        $area = Area::find($id);
        if (isset($area)) {
            $area->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('areas.index')->with('Success', 'Area updated successfully.');
    }

    public function destroy(area $area)
    {
        $area->update([
            'deleted_by' => Auth::user()->id
        ]);
        $area->delete();
        return redirect()->route('areas.index')->with('Success', 'Area deleted successfully.');
    }

    public function print()
    {
        $getprinted = PrintedIndexView::where('index', 'Areas')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Areas')->update([
                'index' => "Areas",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);                    
        }else{
            PrintedIndexView::create([
            'index' => "Areas",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);  

        }   
        
        $Areas = Area::all();

        if ($Areas->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $dataArray = array();
        $no = 1;
        foreach ($Areas as $Area) {
            $objectJSON = [];
            $objectJSON["no"] = $no;
            $objectJSON["code"] = $Area->code;
            $objectJSON["description"] = $Area->descr;
            $objectJSON["status"] = $Area->active ? "Active" : "Inactive";

            $no = $no + 1;
            $dataArray[] = collect($objectJSON);
        }

        $AreasJSON = '{"data" :' . json_encode($dataArray) . '}';

        $JsonFileName = "ReportPrintArea" . date("Ymdhisa");

        file_put_contents(base_path('resources/reporting/StockModule/' . $JsonFileName . '.json'), $AreasJSON);
        $JasperFileName = "ReportPrintArea" . date("Ymdhisa");;
        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockModule/ReportPrint.jrxml';
        $output = base_path() . '/resources/reporting/StockModule/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockModule/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "title" => "Area Listing Report (" .  date('d/m/Y') . ")"
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockModule/' . $JasperFileName . '.pdf';
        return $file;
    }


    public function download()
    {
        return "hi";
    }

}
