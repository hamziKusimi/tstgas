<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\CylinderPorder;
use App\Model\CylinderPorderdt;
use App\Model\Creditor;
use App\Model\Debtor;
use App\Model\TaxCode;
use App\Model\Cylinder;
use App\Model\Uom;
use App\Model\Location;
use App\Model\PrintedIndexView;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Salesman;
use Auth;
use DateTime;
use PHPJasper\PHPJasper;
use App\Model\CustomObject\Transaction;
use DB;
use PDF;
use Storage;
use App\Model\Area;
use Illuminate\Http\Request;

class CylinderporderController extends Controller
{
    public function index()
    {
        $data["cylinderporders"] = CylinderPorder::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = CylinderPorder::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["docno_select"] = CylinderPorder::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Cylinder Purchase Order')->pluck('printed_by');
        $data["page_title"] = "Purchase Orders Listing";
        $data["bclvl1"] = "Purchase Orders Listing";
        $data["bclvl1_url"] = route('cylinderporders.index');
        return view('dailypro.cylinderporders.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["cylinderpordersearch"] = CylinderPorder::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppdo', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppinv', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ptype', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = CylinderPorder::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["page_title"] = "Purchase Orders Listing";
        $data["bclvl1"] = "Purchase Orders Listing";
        $data["bclvl1_url"] = route('cylinderporders.index');
        return view('dailypro.cylinderporders.index', $data);
    }

    public function api_store(Request $request)
    {
        $data = CylinderPorder::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppdo', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('suppinv', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ptype', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function editRoute($id)
    {
        $resources = Cylinder::where('id', $id)->get();

        return response()->json($resources);
    }

    public function editAllRoute($id)
    {
        $responses = Cylinder::where('id', $id)->get();

        return response()->json($responses);
    }

    public function create()
    {
        $masterCodes = AccountMastercode::isCreditor()->get();
        $runningNumber = DocumentSetup::findByName('Cylinder Purchase Order');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountcode');
        $systemsetup = SystemSetup::first();
        // $product = Cylinder::whereNotNull('serial')->get(['barcode','serial','cat_id', 'prod_id', 'capacity', 'testpressure']);
        $product = Cylinder::whereNotNull('barcode')->pluck('serial');

        // return response()->json($uoms);

        $data = [];
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data['masterCodes'] = $masterCodes;
        $data['runningNumber'] = $runningNumber;
        $data['systemsetup'] = $systemsetup;
        $data['product'] = $product;

        // return response()->json($data);
        $data["page_title"] = "Purchase Order Listing";
        $data["bclvl1"] = "Purchase Order Listing";
        $data["bclvl1_url"] = route('cylinderporders.index');
        $data["bclvl2"] = "Add Purchase Order";
        $data["bclvl2_url"] = route('cylinderporders.create');
        return view('dailypro.cylinderporders.create', $data);

    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $CylinderPorderid = CylinderPorder::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'suppdo' => $request['suppdo'],
            'suppinv' => $request['suppinv'],
            'suppinvdate' => Carbon::createFromFormat('d/m/Y', $request['suppinvdate']),
            'ref' => $request['ref'],
            'ptype' => $request['ptype'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'currency' => $request['currency'],
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $CylinderPorderDoc = DocumentSetup::findByName('Cylinder Purchase Order');
        $CylinderPorderDoc->update(['D_LAST_NO' => $CylinderPorderDoc->D_LAST_NO + 1]);

        if (count($request['subject']) > 1) {
            for ($i = 1; $i < count($request['subject']); $i++) {
                $dt = CylinderPorderdt::create([
                    'doc_no' => $CylinderPorderid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'serial' => $request['serial'][$i],
                    'barcode' => $request['barcode'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'product' => $request['product'][$i],
                    'type' => $request['type'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' => $request['uom'][$i],
                    'ucost' => $request['unit_price'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'updated_by' => Auth::user()->id
                ]);
            }
        }

        return redirect()->route('cylinderporders.edit', ['id' => $CylinderPorderid])->with('Success', 'Purchase orders added sucessfully');
    }

    public function show($id)
    { }

    public function edit($id)
    {
        $cylinderporder = CylinderPorder::find($id);
        $contents = CylinderPorderdt::where('doc_no', $cylinderporder->docno)->get();

        $selectedCreditor = Creditor::findByAcode($cylinderporder->account_code);
        $masterCodes = AccountMastercode::isCreditor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Cylinder::whereNotNull('barcode')->pluck('serial');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $cylinderporder;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedCreditor'] = $selectedCreditor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Purchase Orders";
        $data["bclvl1"] = "Purchase Orders Listing";
        $data["bclvl1_url"] = route('cylinderporders.index');
        $data["bclvl2"] = "Edit Purchase Orders";
        $data["bclvl2_url"] = route('cylinderporders.edit', ['id' => $id]);

        return view('dailypro.cylinderporders.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $cylinderporder = CylinderPorder::find($id);
        if (isset($cylinderporder)) {
            $cylinderporder->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'suppdo' => $request['suppdo'],
                'suppinv' => $request['suppinv'],
                'suppinvdate' => Carbon::createFromFormat('d/m/Y', $request['suppinvdate']),
                'ref' => $request['ref'],
                'ptype' => $request['ptype'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'currency' => $request['currency'],
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['serial']) > 1) {
                for ($i = 1; $i < count($request['serial']); $i++) {

                    $dt = CylinderPorderdt::find($request['item_id'][$i]);

                    if (isset($dt)) {

                        $dt->update([
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'ucost' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        $dt = CylinderPorderdt::create([
                            'doc_no' => $cylinderporder->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'serial' => $request['serial'][$i],
                            'barcode' => $request['barcode'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'product' => $request['product'][$i],
                            'type' => $request['type'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['uom'][$i],
                            'ucost' => $request['unit_price'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }
            }
        }
        return redirect()->route('cylinderporders.edit', ['id' => $id])->with('Success', 'Purchase orders updated sucessfully');
    }

    public function destroy($id)
    {
        $cylinderporder = CylinderPorder::find($id);
        $cylinderporder->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylinderporder->delete();
        $docno = $cylinderporder->docno;

        $cylinderporderdt = CylinderPorderdt::where('doc_no', $docno);
        $cylinderporderdt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylinderporderdt->delete();
        return redirect()->route('cylinderporders.index')->with('Success', 'Purchase orders Received deleted successfully');
    }

    public function destroyData($id)
    {
        $data = CylinderPorderdt::find($id);

        if (isset($data)) {
            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function print($id)
    {
        $data = [];

        $invoice = CylinderPorder::find($id);
        $contents = CylinderPorderdt::where('doc_no', $invoice->docno)->get();
        $creditor = Creditor::where('accountcode', $invoice->account_code)->first();
        $sytemsetups = Systemsetup::first();
        $data['items'] = $invoice;
        $data['contents'] = $contents;
        $data['sytemsetups'] = $sytemsetups;
        $data['creditor'] = $creditor;

        // dd($data['items']);

        $pdf = PDF::loadView('dailypro.cylinderporders.print',  $data)->setPaper('A4');
        return $pdf->inline();
    }

    public function printSummaryListing(Request $request)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Cylinder Purchase Order')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Cylinder Purchase Order')->update([
                'index' => "Cylinder Purchase Order",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);
        }else{
            PrintedIndexView::create([
            'index' => "Cylinder Purchase Order",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);

        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $PurchaseOrder = $this->getPOQuery($request);

        if ($PurchaseOrder->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->porders_rcv == "listing") {
            foreach($PurchaseOrder as $PO){

                $amount = $amount + $PO->amount;
            }

            $data['PurchaseOrderJSON'] = $PurchaseOrder;
            $data['totalamount'] = $amount;
            // dd($data['PurchaseOrderJSON']);
            $pdf = PDF::loadView('dailypro.cylinderporders.listing',  $data)->setPaper('A4');
            return $pdf->inline();
            // return view('dailypro/cylinderporders.listing', $data);


        } else {

            $data['PurchaseOrderJSON'] = $PurchaseOrder;
            // dd($data['PurchaseOrderJSON']);
            $pdf = PDF::loadView('dailypro.cylinderporders.summary',  $data)->setPaper('A4');
            return $pdf->inline();

            // return view('dailypro/cylinderporders.summary', $data);
        }
    }


    public function getPOQuery(Request $request)
    {
        return CylinderPorder::join('cylinder_porderdts', 'cylinder_porders.docno', '=', 'cylinder_porderdts.doc_no')
        ->join('cylinders', 'cylinder_porderdts.serial', '=', 'cylinders.serial')
        ->selectRaw("
        `cylinder_porders`.`docno`          AS `docno`,
        `cylinder_porders`.`date`           AS `date`,
        `cylinder_porders`.`suppdo`          AS `suppdo`,
        `cylinder_porders`.`suppinv`          AS `suppinv`,
        `cylinder_porders`.`ref`          AS `ref`,
        `cylinder_porders`.`account_code`   AS `account_code`,
        `cylinder_porders`.`account_code`   AS `debtor`,
        `cylinder_porders`.`name`           AS `name`,
        CONCAT(CONCAT(`cylinder_porders`.`docno`),', ',DATE_FORMAT(`cylinder_porders`.`date`,'%d-%m-%Y'),', ',`cylinder_porders`.`account_code`,', ',`cylinder_porders`.`name`) AS `details`,
        `cylinder_porderdts`.`subject`      AS `description`,
        `cylinder_porderdts`.`id`           AS `id`,
        `cylinder_porderdts`.`doc_no`       AS `doc_no`,
        `cylinder_porderdts`.`serial`    AS `serial`,
        `cylinder_porderdts`.`qty`          AS `quantity`,
        `cylinder_porderdts`.`uom`          AS `uom`,
        `cylinder_porderdts`.`ucost`       AS `ucost`,
        `cylinder_porderdts`.`amount`       AS `amount`,
        `cylinder_porderdts`.`taxed_amount` AS `tax_amount`,
        `cylinder_porderdts`.`subject`      AS `subject`,
        `cylinder_porderdts`.`details`      AS `DETAIL`,
        cylinder_porders.amount AS totalamt")
        ->where(function ($query) use ($request) {

            $query->whereNull('cylinder_porders.deleted_at');
            $query->whereNull('cylinder_porderdts.deleted_at');

                if ($request->dates_Chkbx == "on") {
                    $dates = explode("-", $request->dates);
                    $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
                    $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

                    if ($dates_from == $dates_to) {
                        $query->where('date', '=', $dates_from);
                    } else {
                        $query->whereBetween('date', [$dates_from, $dates_to]);
                    }
                }

                if ($request->docno_Chkbx == "on") {
                    $query->whereBetween('docno', [$request->docno_frm, $request->docno_to]);
                }

                if ($request->suppDo_Chkbx == "on") {
                    if ($request->suppDo_to == null) {
                        $query->where('suppdo', "=", $request->suppDo_frm);
                    } else {
                        $query->whereBetween('suppdo', [$request->suppDo_frm, $request->suppDo_to]);
                    }
                }

                if ($request->suppInv_Chkbx == "on") {
                    if ($request->suppInv_to == null) {
                        $query->where('suppinv', "=", $request->suppInv_frm);
                    } else {
                        $query->whereBetween('suppinv', [$request->suppInv_frm, $request->suppInv_to]);
                    }
                }

                if ($request->refNo_Chkbx == "on") {
                    if ($request->refNo_to == null) {
                        $query->where('ref', "=", $request->refNo_frm);
                    } else {
                        $query->whereBetween('ref', [$request->refNo_frm, $request->refNo_to]);
                    }
                }

                if ($request->credCode_Chkbx == "on") {
                    $query->whereBetween('account_code', [$request->credCode_frm, $request->credCode_to]);
                }
            })
            ->orderBy('docno')
            ->get();
    }

}
