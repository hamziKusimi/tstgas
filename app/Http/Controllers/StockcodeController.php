<?php

namespace App\Http\Controllers;

use App\Model\Stockcode;
use App\Model\Category;
use App\Model\Product;
use App\Model\Brand;
use App\Model\Location;
use App\Model\Uom;
use App\Model\SystemSetup;
use App\Model\PrintedIndexView;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;
use Storage;
use Auth;
use Carbon\Carbon;
use Redirect;
use DB;

class StockcodeController extends Controller
{
    public function api_get()
    {
        $resources = Stockcode::get();

        return response()->json($resources);
    }

    public function Index()
    {
        $data["stockcodes"] = Stockcode::get();
        $data["page_title"] = "Stock Item Listing";
        $data["bclvl1"] = "Stock Item Listing";
        $data["bclvl1_url"] = route('stockcodes.index');
        // return response()->json($data);

        return view('stockmaster.stockcodes.index', $data);
    }

    public function create()
    {
        $data["categories"] = Category::get()->pluck('ItemMastercode', 'id');
        $data["products"] = Product::get()->pluck('ItemMastercode', 'id');
        $data["brands"] = Brand::get()->pluck('ItemMastercode', 'id');
        $data["locations"] = Location::get();
        $data["uoms"] = Uom::get();
        $data["systemsetup"] = SystemSetup::first();
        $data["page_title"] = "Add Stock";
        $data["bclvl1"] = "Stock Item Listing";
        $data["bclvl1_url"] = route('stockcodes.index');
        $data["bclvl2"] = "Add Stock";
        $data["bclvl2_url"] = route('stockcodes.create');

        return view('stockmaster.stockcodes.create',  $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);

        // if ($request->has('image')) {
        //     // Get image file
        //     $image = $request->file('image');
        //     // Make a image name based on user name and current timestamp
        //     $name = str_slug($request->input('name')).'_'.time();
        //     // Define folder path
        //     $folder = '/uploads/images/';
        //     // Make a file path where image will be stored [ folder path + file name + file extension]
        //     $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
        //     // Upload image
        //     $this->uploadOne($image, $folder, 'public', $name);
        //     // Set user profile image path in database to filePath
        //     $request->image = $filePath;
        // }

        if ($request->hasFile('image')) {
            // dd($filesystem);
            // Get filename with extension
            $stockcode = $request['code'];
            // $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = 'public/stock/' . $stockcode . '_' . time() . '.' . $extension;
            $filenameInDB = 'storage/stock/' . $stockcode . '_' . time() . '.' . $extension;

            // Upload Image
            Storage::put($fileNameToStore, file_get_contents($request['image']));
        } else {
            $filenameInDB = 'noimage.jpg';
        }


        Stockcode::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'edit' => $request['edit'] == 1 ? 1 : 0,
            'inactive' => $request['inactive'] == 1 ? 1 : 0,
            'ref1' => $request['ref1'],
            'ref2' => $request['ref2'],
            'model' => $request['model'],
            'cat_id' => $request['category'],
            'type' => $request['type'],
            'prod_id' => $request['product'],
            'weight' => $request['weight'],
            'brand_id' => $request['brand'],
            'loc_id' => $request['location'],
            'minstkqty' => $request['minstkqty'],
            'maxstkqty' => $request['maxstkqty'],
            'uom_id1' => $request['uom1'],
            'uom_id2' => $request['uom2'],
            'uom_id3' => $request['uom3'],
            'stkbal' => $request['stkbal'],
            'laststkch' => $request['laststkch'],
            'currency' => $request['currency'],
            'volume' => $request['volume'],
            'uomrate2' => $request['uomrate2'],
            'uomrate3' => $request['uomrate3'],
            'unitcost' => $request['unitcost'],
            'unitcost1' => $request['unitcost1'],
            'unitcost2' => $request['unitcost2'],
            'unitcost3' => $request['unitcost3'],
            'unitcost4' => $request['unitcost4'],
            'unitcost5' => $request['unitcost5'],
            'unitcost6' => $request['unitcost6'],
            'unitcost7' => $request['unitcost7'],
            'prevcost' => $request['prevcost'],
            'avecost' => $request['avecost'],
            'curcost' => $request['curcost'],
            'minprice' => (($request['minprice']) != '0.00') ? $request['minprice'] : (($request['unitcost']) * ($request['unitcost1'])) / 100,
            'price1' => (($request['price1']) != '0.00') ? $request['price1'] : (($request['unitcost']) * ($request['unitcost2'])) / 100,
            'price2' => (($request['price2']) != '0.00') ? $request['price2'] : (($request['unitcost']) * ($request['unitcost3'])) / 100,
            'price3' => (($request['price3']) != '0.00') ? $request['price3'] : (($request['unitcost']) * ($request['unitcost4'])) / 100,
            'price4' => (($request['price4']) != '0.00') ? $request['price4'] : (($request['unitcost']) * ($request['unitcost5'])) / 100,
            'price5' => (($request['price5']) != '0.00') ? $request['price5'] : (($request['unitcost']) * ($request['unitcost6'])) / 100,
            'price6' => (($request['price6']) != '0.00') ? $request['price6'] : (($request['unitcost']) * ($request['unitcost7'])) / 100,
            'remark' => $request['remark'],
            'memo' => $request['memo'],
            'image' => $filenameInDB,
            'created_by' => Auth::user()->id
        ]);

        return redirect()->route('stockcodes.index')->with('Success', 'Stock created successfully');
    }

    public function edit($id)
    {
        $data["stockcode"] = Stockcode::find($id);
        $data["categories"] = Category::get()->pluck('ItemMastercode', 'id');
        $data["products"] = Product::get()->pluck('ItemMastercode', 'id');
        $data["brands"] = Brand::get()->pluck('ItemMastercode', 'id');
        $data["locations"] = Location::get();
        $data["uoms"] = Uom::get();
        $data["systemsetup"] = SystemSetup::first();
        $data["page_title"] = "Edit Stock";
        $data["bclvl1"] = "Stock Item Listing";
        $data["bclvl1_url"] = route('stockcodes.index');
        $data["bclvl2"] = "Edit Stock";
        $data["bclvl2_url"] = route('stockcodes.edit', ['id' => $id]);

        return view('stockmaster.stockcodes.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // return response()->json($request);
        if ($request->hasFile('image')) {
            // dd($filesystem);
            // Get filename with extension
            $stockcode = $request['code'];
            // $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = 'public/stock/' . $stockcode . '_' . time() . '.' . $extension;
            $filenameInDB = 'storage/stock/' . $stockcode . '_' . time() . '.' . $extension;

            // Upload Image
            Storage::put($fileNameToStore, file_get_contents($request['image']));
        } else {
            $filenameInDB = 'noimage.jpg';
        }

        $stockcode = Stockcode::find($id);

        $stockcode->update([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'edit' => $request['edit'] == 1 ? 1 : 0,
            'inactive' => $request['inactive'] == 1 ? 1 : 0,
            'ref1' => $request['ref1'],
            'ref2' => $request['ref2'],
            'model' => $request['model'],
            'cat_id' => $request['category'],
            'type' => $request['type'],
            'prod_id' => $request['product'],
            'weight' => $request['weight'],
            'brand_id' => $request['brand'],
            'loc_id' => $request['location'],
            'minstkqty' => $request['minstkqty'],
            'maxstkqty' => $request['maxstkqty'],
            'uom_id1' => $request['uom1'],
            'uom_id2' => $request['uom2'],
            'uom_id3' => $request['uom3'],
            'volume' => $request['volume'],
            'uomrate2' => $request['uomrate2'],
            'uomrate3' => $request['uomrate3'],
            'unitcost' => $request['unitcost'],
            'unitcost1' => $request['unitcost1'],
            'unitcost2' => $request['unitcost2'],
            'unitcost3' => $request['unitcost3'],
            'unitcost4' => $request['unitcost4'],
            'unitcost5' => $request['unitcost5'],
            'unitcost6' => $request['unitcost6'],
            'unitcost7' => $request['unitcost7'],
            'prevcost' => $request['prevcost'],
            'avecost' => $request['avecost'],
            'curcost' => $request['curcost'],
            'minprice' => (($request['minprice']) != '0.00') ? $request['minprice'] : (($request['unitcost']) * ($request['unitcost1'])) / 100,
            'price1' => (($request['price1']) != '0.00') ? $request['price1'] : (($request['unitcost']) * ($request['unitcost2'])) / 100,
            'price2' => (($request['price2']) != '0.00') ? $request['price2'] : (($request['unitcost']) * ($request['unitcost3'])) / 100,
            'price3' => (($request['price3']) != '0.00') ? $request['price3'] : (($request['unitcost']) * ($request['unitcost4'])) / 100,
            'price4' => (($request['price4']) != '0.00') ? $request['price4'] : (($request['unitcost']) * ($request['unitcost5'])) / 100,
            'price5' => (($request['price5']) != '0.00') ? $request['price5'] : (($request['unitcost']) * ($request['unitcost6'])) / 100,
            'price6' => (($request['price6']) != '0.00') ? $request['price6'] : (($request['unitcost']) * ($request['unitcost7'])) / 100,
            'remark' => $request['remark'],
            'memo' => $request['memo'],
            'image' => $filenameInDB,
            'updated_by' => Auth::user()->id
        ]);
        // return response()->json($request);
        // $stockcode->update($request->all());
        return redirect()->route('stockcodes.index')->with('Success', 'Stock updated successfully.');
    }

    public function destroy(stockcode $stockcode)
    {
        $stockcode->update([
            'deleted_by' => Auth::user()->id
        ]);

        $stockcode->delete();
        return redirect()->route('stockcodes.index')->with('Success', 'Stock deleted successfully.');
    }

    public function print(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Stock Master Listing')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Stock Master Listing')->update([
                'index' => "Stock Master Listing",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Stock Master Listing",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $stocks = $this->getQuery($request);

        if ($stocks->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $stocksJSON = $this->getStockJSON($stocks, $request);

        $JsonFileName = "StockMasterListing" . date("Ymdhisa") . Auth::user()->id;

        file_put_contents(base_path('resources/reporting/StockMasterListing/' . $JsonFileName . '.json'), $stocksJSON);
        $JasperFileName = "StockMasterListing" . date("Ymdhisa") . Auth::user()->id;

        $file = $this->printReport($JsonFileName, $JasperFileName);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName)
    {
        $input = base_path() . '/resources/reporting/StockMasterListing/StockMasterListing.jrxml';
        $output = base_path() . '/resources/reporting/StockMasterListing/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockMasterListing/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockMasterListing/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getQuery($request)
    {
        return Stockcode::where(function ($query) use ($request) {

            if ($request->Active_Chkbx == "on") {
                $query->where('inactive', '=', 0);
            }
            if ($request->Inactive_Chkbx == "on") {
                $query->where('inactive', '=', 1);
            }
            if ($request->STK_Code_Chkbx == "on") {
                $query->whereBetween('code', [$request->STK_Code_frm, $request->STK_Code_to]);
            }
            if ($request->Category_Chkbx == "on") {
                $categoryidfrm = Category::select('id')->where('code', $request->Category_frm)->first();
                $categoryidto = Category::select('id')->where('code', $request->Category_to)->first();
                if ($categoryidfrm != null && $categoryidto != null) {
                    $cat_id_frm = $categoryidfrm->id;
                    $cat_id_to = $categoryidto->id;
                } else {
                    $cat_id_frm = null;
                    $cat_id_to = null;
                }
                $query->whereBetween('stockcodes.cat_id', [$cat_id_frm, $cat_id_to]);
            }
            if ($request->Product_Chkbx == "on") {
                $productidfrm = Product::select('id')->where('code', $request->Product_frm)->first();
                $productidto = Product::select('id')->where('code', $request->Product_to)->first();
                if ($productidfrm  != null && $productidto != null) {
                    $prod_id_frm = $productidfrm->id;
                    $prod_id_to = $productidto->id;
                } else {
                    $prod_id_frm = null;
                    $prod_id_to = null;
                }
                $query->whereBetween('stockcodes.prod_id', [$prod_id_frm, $prod_id_to]);
            }
            if ($request->Brand_Chkbx == "on") {
                $brandidfrm = Brand::select('id')->where('code', $request->Brand_frm)->first();
                $brandidto = Brand::select('id')->where('code', $request->Brand_to)->first();
                if ($brandidfrm  != null && $brandidto != null) {
                    $brand_id_frm = $brandidfrm->id;
                    $brand_id_to = $brandidto->id;
                } else {
                    $brand_id_frm = null;
                    $brand_id_to = null;
                }
                $query->whereBetween('stockcodes.brand_id', [$brand_id_frm, $brand_id_to]);
            }
            if ($request->Location_Chkbx == "on") {
                $locationidfrm = Location::select('id')->where('code', $request->Location_frm)->first();
                $locationidto = Location::select('id')->where('code', $request->Location_to)->first();
                if ($locationidfrm != null && $locationidto != null) {
                    $location_id_frm = $locationidfrm->id;
                    $location_id_to = $locationidto->id;
                } else {
                    $location_id_frm = null;
                    $location_id_to = null;
                }
                $query->whereBetween('stockcodes.loc_id', [$location_id_frm, $location_id_to]);
            }
        })->get();
    }

    public function getStockJSON($stocks, $request)
    {

        $dataArray = [];

        foreach ($stocks as $stock) {
            $objectJSON =   new \stdClass();

            //Get Code Column
            $objectJSON->code =  $stock->code;

            //Get Description Column
            $objectJSON->desc = $stock->descr;

            //Get Location Column
            $locationQuery = Location::select('code')->where('id', $stock->loc_id)->first();
            $objectJSON->location = $locationQuery->code;

            //Get Category Column
            $categoryQuery = Category::select('code')->where('id', $stock->cat_id)->first();
            $objectJSON->category = $categoryQuery->code;

            //Get Product Column
            $productQuery = Product::select('code')->where('id', $stock->prod_id)->first();
            $objectJSON->product = $productQuery->code;

            //Get Brand Column
            $brand = Brand::select('code')->where('id', $stock->brand_id)->first();
            $objectJSON->brand = $brand->code;

            //Get UOM Column
            $uomQuery = Uom::select('code')->where('id', $stock->uom_id1)->first();
            $objectJSON->uom = $uomQuery->code;

            // Get Price Column
            if ($request->select_price == "minprice") {
                $objectJSON->price = $stock->minprice;
                $objectJSON->price_header = "Min Price";
            } elseif ($request->select_price == "price2") {
                $objectJSON->price = $stock->price2;
                $objectJSON->price_header = "Unit Price";
            } elseif ($request->select_price == "price1") {
                $objectJSON->price = $stock->price1;
                $objectJSON->price_header = "Ws Price";
            } elseif ($request->select_price == "retailprice") {
                $objectJSON->price = $stock->price3;
                $objectJSON->price_header = "Retail Price";
            } elseif ($request->select_price == "price1") {
                $objectJSON->price = $stock->price1;
                $objectJSON->price_header = "Price 1";
            } elseif ($request->select_price == "price2") {
                $objectJSON->price = $stock->price2;
                $objectJSON->price_header = "Price 2";
            } elseif ($request->select_price == "price3") {
                $objectJSON->price = $stock->price3;
                $objectJSON->price_header = "Price 3";
            }

            //Get Cost Column
            if ($request->select_cost == "unitcost") {
                $objectJSON->cost = $stock->minprice;
                $objectJSON->cost_header = "Unit Cost";
            } elseif ($request->select_cost == "avgcost") {
                $objectJSON->cost = $stock->price2;
                $objectJSON->cost_header = "Avg Cost";
            }

            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function codereplacement(Request $request)
    {
        $Stock = Stockcode::where('code', '=', $request->old_code)->get();

        if ($Stock->isEmpty()) {
            return Redirect::back()->with([
                'Error' => 'No Stock Code Found', 'old_code' => $request->old_code,
                'new_code' => $request->new_code
            ]);
        }

        DB::table('stockcodes')->where('code', $request->old_code)->update(['code' => $request->new_code]);
        DB::table('cashsalesdts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('cashbilldts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('invoice_data')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('gooddts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('preturndts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('deliveryorderdts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('salesorderdts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('quotationdts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('adjustmentidts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('adjustmentodts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('salesreturndts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);
        DB::table('porderdts')->where('item_code', $request->old_code)->update(['item_code' => $request->new_code]);

        return redirect()->route('codereplacement.stockcode')
            ->with([
                'Success' => 'Stock Code Sucessfully Updated', 'old_code' => $request->old_code,
                'new_code' => $request->new_code
            ]);
    }
}
