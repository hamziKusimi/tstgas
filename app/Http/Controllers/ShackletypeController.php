<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Shackletype;
use Auth;

class ShackletypeController extends Controller
{
    public function index(){

        $data["shackletypes"] = Shackletype::get();
        $data["page_title"] = "Shackle Type Item Listings";
        $data["bclvl1"] = "Shackle Type Item Listings";
        $data["bclvl1_url"] = route('shackletypes.index');

        return view('cylindermaster.shackletypes.index', $data);
    }

    public function create(){
        
        $data["page_title"] = "Add Shackle Type";
        $data["bclvl1"] = "Shackle Type Item Listings";
        $data["bclvl1_url"] = route('shackletypes.index');
        $data["bclvl2"] = "Add Shackle Type";
        $data["bclvl2_url"] = route('shackletypes.create');

        return view('cylindermaster.shackletypes.create', $data);
    }

    public function store(Request $request){

        $shackletype = Shackletype::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('shackletypes.index')->with('Success', 'Shackle Type created successfully.');

    }

    public function edit($id){

        $data["shackletype"] = Shackletype::find($id);
        $data["page_title"] = "Edit Shackle Type";
        $data["bclvl1"] = "Shackle Type Item Listings";
        $data["bclvl1_url"] = route('shackletypes.index');
        $data["bclvl2"] = "Edit Shackle Type";
        $data["bclvl2_url"] = route('shackletypes.edit', ['id'=>$id]);

        return view('cylindermaster.shackletypes.edit', $data);
    }

    public function update(Request $request, $id){
        
        $shackletype = Shackletype::find($id);
        if (isset($shackletype)) {
            $shackletype->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('shackletypes.index')->with('Success', 'Shackle Type updated successfully.');
    }

    public function destroy(shackletype $shackletype)
    {
        $shackletype->update([
            'deleted_by' => Auth::user()->id
        ]);
        $shackletype->delete();
        return redirect()->route('shackletypes.index')->with('Success', 'Shackle Type deleted successfully.');
    }
}
