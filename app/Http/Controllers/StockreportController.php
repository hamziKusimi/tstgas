<?php

namespace App\Http\Controllers;

use App\Model\Stockcode;
use App\Model\Category;
use App\Model\Product;
use App\Model\Brand;
use App\Model\Location;
use App\Model\Uom;
use App\Model\View\StockBalance;
use App\Model\PrintedIndexView;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;
use Carbon\Carbon;

class StockreportController extends Controller
{
    public function printStkBal(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Stock Balance Listing')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Stock Balance Listing')->update([
                'index' => "Stock Balance Listing",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Stock Balance Listing",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $stockBal = $this->getStkBalQuery($request);

        if ($stockBal->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $stocksJSON = $this->getStockBalJSON($stockBal, $request);

        if ($request->stock_report == "stock_bal_1") {
            $JsonFileName = "StockBalance" . date("Ymdhisa") . Auth::user()->id;
            file_put_contents(base_path('resources/reporting/StockBalance/' . $JsonFileName . '.json'), $stocksJSON);
            $JasperFileName = "StockBalance" . date("Ymdhisa") . Auth::user()->id;
            $jrxmlName = "StockBalance";
            $file = $this->printReport($request, $JsonFileName, $JasperFileName, $jrxmlName);
        } elseif ($request->stock_report == "stock_bal_2") {
            $JsonFileName = "StockBalance_2" . date("Ymdhisa") . Auth::user()->id;
            file_put_contents(base_path('resources/reporting/StockBalance/' . $JsonFileName . '.json'), $stocksJSON);
            $JasperFileName = "StockBalance_2" . date("Ymdhisa") . Auth::user()->id;
            $jrxmlName = "StockBalance_2";
            $file = $this->printReport($request, $JsonFileName, $JasperFileName, $jrxmlName);
        } elseif ($request->stock_report == "stock_tl_1") {
            $JsonFileName = "StockTakeList_1" . date("Ymdhisa") . Auth::user()->id;
            file_put_contents(base_path('resources/reporting/StockBalance/' . $JsonFileName . '.json'), $stocksJSON);
            $JasperFileName = "StockTakeList_1" . date("Ymdhisa") . Auth::user()->id;
            $jrxmlName = "StockTakeList_1";
            $file = $this->printReport($request, $JsonFileName, $JasperFileName, $jrxmlName);
        } elseif ($request->stock_report == "stock_tl_2") {
            $JsonFileName = "StockTakeList_2" . date("Ymdhisa") . Auth::user()->id;
            file_put_contents(base_path('resources/reporting/StockBalance/' . $JsonFileName . '.json'), $stocksJSON);
            $JasperFileName = "StockTakeList_2" . date("Ymdhisa") . Auth::user()->id;
            $jrxmlName = "StockTakeList_2";
            $file = $this->printReport($request, $JsonFileName, $JasperFileName, $jrxmlName);
        } elseif ($request->stock_report == "stock_tl_3") {
            $JsonFileName = "StockTakeList_3" . date("Ymdhisa") . Auth::user()->id;
            file_put_contents(base_path('resources/reporting/StockBalance/' . $JsonFileName . '.json'), $stocksJSON);
            $JasperFileName = "StockTakeList_3" . date("Ymdhisa") . Auth::user()->id;
            $jrxmlName = "StockTakeList_3";
            $file = $this->printReport($request, $JsonFileName, $JasperFileName, $jrxmlName);
        }

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JsonFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($request, $JsonFileName, $JasperFileName, $jrxmlName)
    {
        $input = base_path() . '/resources/reporting/StockBalance/' . $jrxmlName . '.jrxml';
        $output = base_path() . '/resources/reporting/StockBalance/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockBalance/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date" => date('d/m/Y', strtotime($request->date))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockBalance/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getStkBalQuery($request)
    {
        return StockBalance::join('stockcodes', 'view_stock_balance.item_code', '=', 'stockcodes.code')
            ->select(DB::raw('item_code, SUM(totalqty) as stock_bal'))
            ->whereDate('t_date', '<=', date('Y-m-d', strtotime($request->date)))
            ->where(function ($query) use ($request) {

                if ($request->Active_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 0);
                }
                if ($request->Inactive_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 1);
                }
                if ($request->STK_Code_Chkbx == "on") {
                    $query->whereBetween('item_code', [$request->STK_Code_frm, $request->STK_Code_to]);
                }
                if ($request->Category_Chkbx == "on") {
                    $categoryidfrm = Category::select('id')->where('code', $request->Category_frm)->first();
                    $categoryidto = Category::select('id')->where('code', $request->Category_to)->first();
                    if ($categoryidfrm != null && $categoryidto != null) {
                        $cat_id_frm = $categoryidfrm->id;
                        $cat_id_to = $categoryidto->id;
                    } else {
                        $cat_id_frm = null;
                        $cat_id_to = null;
                    }
                    $query->whereBetween('stockcodes.cat_id', [$cat_id_frm, $cat_id_to]);
                }
                if ($request->Product_Chkbx == "on") {
                    $productidfrm = Product::select('id')->where('code', $request->Product_frm)->first();
                    $productidto = Product::select('id')->where('code', $request->Product_to)->first();
                    if ($productidfrm  != null && $productidto != null) {
                        $prod_id_frm = $productidfrm->id;
                        $prod_id_to = $productidto->id;
                    } else {
                        $prod_id_frm = null;
                        $prod_id_to = null;
                    }
                    $query->whereBetween('stockcodes.prod_id', [$prod_id_frm, $prod_id_to]);
                }
                if ($request->Brand_Chkbx == "on") {
                    $brandidfrm = Brand::select('id')->where('code', $request->Brand_frm)->first();
                    $brandidto = Brand::select('id')->where('code', $request->Brand_to)->first();
                    if ($brandidfrm  != null && $brandidto != null) {
                        $brand_id_frm = $brandidfrm->id;
                        $brand_id_to = $brandidto->id;
                    } else {
                        $brand_id_frm = null;
                        $brand_id_to = null;
                    }
                    $query->whereBetween('stockcodes.brand_id', [$brand_id_frm, $brand_id_to]);
                }
                if ($request->Location_Chkbx == "on") {
                    $locationidfrm = Location::select('id')->where('code', $request->Location_frm)->first();
                    $locationidto = Location::select('id')->where('code', $request->Location_to)->first();
                    if ($locationidfrm != null && $locationidto != null) {
                        $location_id_frm = $locationidfrm->id;
                        $location_id_to = $locationidto->id;
                    } else {
                        $location_id_frm = null;
                        $location_id_to = null;
                    }
                    $query->whereBetween('stockcodes.loc_id', [$location_id_frm, $location_id_to]);
                }
            })
            ->groupBy('item_code')
            ->get();
    }

    public function getStockBalJSON($stockBalance, $request)
    {

        $dataArray = array();
        $s_n = 1;
        foreach ($stockBalance as $stockBal) {
            $objectJSON =   new \stdClass();

            //Get S/N Column
            $objectJSON->s_n = $s_n;

            //Get Code Column
            $objectJSON->stock_code =  $stockBal->item_code;

            $stock = Stockcode::select('descr', 'price2', 'loc_id', 'uom_id1')
                ->where('code', $stockBal->item_code)
                ->first();

            //Get Description Column
            $objectJSON->description = $stock->descr;

            //Get Location Column
            $locationQuery = Location::select('code')->where('id', $stock->loc_id)->first();
            $objectJSON->location = $locationQuery->code;

            //Get unit price Column
            $objectJSON->unit_price =  $stock->price2 == null ? 0 : $stock->price2;

            //Get uom Column
            $uomQuery = Uom::select('code')->where('id', $stock->uom_id1)->first();
            $objectJSON->uom = $uomQuery->code;

            //Get Stock Balance Column
            $objectJSON->stock_balance = $stockBal->stock_bal;

            //Get Stock Balance + uom Column
            $objectJSON->stock_balance_uom = strval($stockBal->stock_bal) . " " . $uomQuery->code;

            $s_n = $s_n + 1;
            $dataArray[] = $objectJSON;
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
