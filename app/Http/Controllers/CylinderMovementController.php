<?php

namespace App\Http\Controllers;

use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Debtor;
use App\Model\View\CylinderMovement;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;

class CylinderMovementController extends Controller
{
    public function print(Request $request)
    {
        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $CylMovement = $this->getCylMovementQuery($request, $dates_from, $dates_to);

        if (empty($CylMovement)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }
        $cylJSON = $this->getCylMovementJSON($CylMovement);

        $JsonFileName = "CylinderMovement" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/CylinderMovement/' . $JsonFileName . '.json'), $cylJSON);
        $JasperFileName = "CylinderMovement" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to)
    {
        $input = base_path() . '/resources/reporting/CylinderMovement/CylinderMovement.jrxml';
        $output = base_path() . '/resources/reporting/CylinderMovement/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/CylinderMovement/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date_from" => date('d/m/Y', strtotime($dates_from)),
                "date_to" => date('d/m/Y', strtotime($dates_to))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/CylinderMovement/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getCylMovementQuery($request, $dates_from, $dates_to)
    {
        // return CylinderMovement::where(function ($query) use ($request, $dates_from, $dates_to) {

        //     if ($dates_from == $dates_to) {
        //         $query->whereDate('cdate', '=', $dates_from);
        //     } else {
        //         $query->whereBetween('cdate', [$dates_from, $dates_to]);
        //     }

        //     if ($request->serial_Chkbx == "on") {
        //         // $serial_frm = Cylindertype::select('id')->where('code', $request->serial_frm)->first();
        //         // $serial_to = Cylindertype::select('id')->where('code', $request->serial_to)->first();
        //         // if ($serial_frm != null && $serial_to != null) {
        //         //     $type_id_frm = $serial_frm->id;
        //         //     $type_id_to = $serial_to->id;
        //         // } else {
        //         //     $type_id_frm = null;
        //         //     $type_id_to = null;
        //         // }
        //         // $cylFrom = Cylinder::select('serial')->whereBetween('type_id', [$type_id_frm, $type_id_to])
        //         //     ->orderBy('serial', 'asc')
        //         //     ->first();
        //         // $cylTo = Cylinder::select('serial')->whereBetween('type_id', [$type_id_frm, $type_id_to])
        //         //     ->orderBy('serial', 'desc')
        //         //     ->first();
        //         $query->whereBetween('s_no', [$request->serial_frm, $request->serial_to]);
        //     }

        //     if ($request->Type_Chkbx == "on") {
        //         $typefrm = Cylindertype::select('id')->where('code', $request->Type_frm)->first();
        //         $typeto = Cylindertype::select('id')->where('code', $request->Type_to)->first();
        //         if ($typefrm != null && $typeto != null) {
        //             $type_id_frm = $typefrm->id;
        //             $type_id_to = $typeto->id;
        //         } else {
        //             $type_id_frm = null;
        //             $type_id_to = null;
        //         }
        //         $cylFrom = Cylinder::select('serial')->whereBetween('type_id', [$type_id_frm, $type_id_to])
        //             ->orderBy('serial', 'asc')
        //             ->first();
        //         $cylTo = Cylinder::select('serial')->whereBetween('type_id', [$type_id_frm, $type_id_to])
        //             ->orderBy('serial', 'desc')
        //             ->first();
        //         $query->whereBetween('s_no', [$cylFrom->serial, $cylTo->serial]);
        //     }

        //     if ($request->Category_Chkbx == "on") {
        //         $categoryfrm = Cylindercategory::select('id')->where('code', $request->Category_frm)->first();
        //         $categoryto = Cylindercategory::select('id')->where('code', $request->Category_to)->first();
        //         if ($categoryfrm != null && $categoryto != null) {
        //             $category_id_frm = $categoryfrm->id;
        //             $category_id_to = $categoryto->id;
        //         } else {
        //             $category_id_frm = null;
        //             $category_id_to = null;
        //         }
        //         $cylFrom = Cylinder::select('serial')->whereBetween('cat_id', [$category_id_frm, $category_id_to])
        //             ->orderBy('serial', 'asc')
        //             ->first();
        //         $cylTo = Cylinder::select('serial')->whereBetween('cat_id', [$category_id_frm, $category_id_to])
        //             ->orderBy('serial', 'desc')
        //             ->first();
        //         $query->whereBetween('s_no', [$cylFrom->serial, $cylTo->serial]);
        //     }

        //     if ($request->Product_Chkbx == "on") {
        //         $productfrm = Cylinderproduct::select('id')->where('code', $request->Product_frm)->first();
        //         $productto = Cylinderproduct::select('id')->where('code', $request->Product_to)->first();
        //         if ($productfrm != null && $productto != null) {
        //             $Product_id_frm = $productfrm->id;
        //             $Product_id_to = $productto->id;
        //         } else {
        //             $Product_id_frm = null;
        //             $Product_id_to = null;
        //         }
        //         $cylFrom = Cylinder::select('serial')->whereBetween('cat_id', [$Product_id_frm, $Product_id_to])
        //             ->orderBy('serial', 'asc')
        //             ->first();
        //         $cylTo = Cylinder::select('serial')->whereBetween('cat_id', [$Product_id_frm, $Product_id_to])
        //             ->orderBy('serial', 'desc')
        //             ->first();
        //         $query->whereBetween('s_no', [$cylFrom->serial, $cylTo->serial]);
        //     }

        //     if ($request->Group_Chkbx == "on") {
        //         $grouptfrm = Cylindergroup::select('id')->where('code', $request->Group_frm)->first();
        //         $grouptto = Cylindergroup::select('id')->where('code', $request->Group_to)->first();
        //         if ($grouptfrm != null && $grouptto != null) {
        //             $Group_id_frm = $grouptfrm->id;
        //             $Group_id_to = $grouptto->id;
        //         } else {
        //             $Group_id_frm = null;
        //             $Group_id_to = null;
        //         }
        //         $cylFrom = Cylinder::select('serial')->whereBetween('cat_id', [$Group_id_frm, $Group_id_to])
        //             ->orderBy('serial', 'asc')
        //             ->first();
        //         $cylTo = Cylinder::select('serial')->whereBetween('cat_id', [$Group_id_frm, $Group_id_to])
        //             ->orderBy('serial', 'desc')
        //             ->first();
        //         $query->whereBetween('s_no', [$cylFrom->serial, $cylTo->serial]);
        //     }
        // })->orderBy('doc_no')
        //     ->get();


            if ($dates_from == $dates_to) {
                $dates = "AND cdate = '".$dates_from."'";
            } else {
                $dates = "AND cdate >= '" . $dates_from . "' AND cdate <= '" . $dates_to . "'";
            }

            if ($request->serial_Chkbx == "on") {
                $serial = "AND (s_no >= '" . $request->serial_frm . "' AND s_no <= '" . $request->serial_to . "') ";
            }else{
                $serial = "";
            }

            if ($request->Type_Chkbx == "on") {
                $type = "AND (type >= '" . $request->Type_frm . "' AND type <= '" . $request->Type_to. "')";
            }else{
                $type = "";
            }

            if ($request->Category_Chkbx == "on") {
                $category = "AND (category >= '" . $request->Category_frm . "' AND category <= '" . $request->Category_to . "')";
            }else{
                $category = "";
            }

            if ($request->Product_Chkbx == "on") {
                $product = "AND (product >= '" .$request->Product_frm. "' AND product <= '" .$request->Product_to. "')";
            }else{
                $product = "";
            }

            if ($request->Group_Chkbx == "on") {
                $group = "AND (groups >= '" . $request->Group_frm . "' AND groups <= '" . $request->Group_to . "')";
            }else{
                $group = "";
            }

            if ($request->debCode_Chkbx == "on") {
                $debtor = "AND (debtor >= '" . $request->debCode_frm . "' AND debtor <= '" . $request->debCode_to . "')";
            }else{
                $debtor = "";
            }

            if($request->printtype == "all"){
                $doctype = "";
            }elseif($request->printtype == "do"){

                $doctype = "AND doc_type = 'DN'";

            }elseif($request->printtype == "iv"){

                $doctype = "AND doc_type = 'IV'";

            }elseif($request->printtype == "rn"){

                $doctype = "AND doc_type = 'RN'";

            }else{
                $doctype = "";

            }

            if($request->orderby == "docno"){
                $orderby = "ORDER BY doc_no";
            }elseif($request->orderby == "date"){

                $orderby = "ORDER BY cdate";

            }elseif($request->orderby == "gastype"){

                $orderby = "ORDER BY TYPE";

            }else{
                $orderby = "";

            }

            // if ($request->location_Chkbx == "on") {
            //     $debtor = "AND (debtor >= '" . $request->location_frm . "' AND debtor <= '" . $request->location_to . "')";
            // }else{
            //     $debtor = "";
            // }

            $cylmovement = DB::select(DB::raw("

                   SELECT * from view_cylinder_movement where 1=1
                   $dates $serial $type $category $product $group $debtor $doctype
                   $orderby


            "), []);

            return $cylmovement;


    }

    public function getCylMovementJSON($CylMovement)
    {

        $dataArray = [];
        $s_n = 1;
        foreach ($CylMovement as $cyl) {
            $objectJSON =   [];

            $objectJSON['s_n'] = $s_n;
            $objectJSON['cylinder_sn'] = $cyl->s_no;
            $objectJSON['doc_type'] = $cyl->doc_type;
            $objectJSON['doc_no'] = $cyl->doc_no;
            $objectJSON['date'] = date('d/m/Y', strtotime($cyl->cdate));
            $objectJSON['debtor'] = $cyl->debtor;
            $objectJSON['gastype'] = $cyl->category;
            $objectJSON['remark'] =  $cyl->remark;
            if($cyl->doc_type == 'IV'){
                $objectJSON['type'] = 'SALES';
            }else{
                $objectJSON['type'] = '';
            }

            $debtor = Debtor::select('name')->where('accountcode', $cyl->debtor)->first();
            $objectJSON['name'] = $debtor['name'];

            $s_n =  $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
