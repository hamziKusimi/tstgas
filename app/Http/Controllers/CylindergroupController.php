<?php

namespace App\Http\Controllers;

use App\Model\Cylindergroup;
use Illuminate\Http\Request;
use Auth;

class CylindergroupController extends Controller
{
    public function index(){

        $data["cylindergroups"] = Cylindergroup::get();
        $data["page_title"] = "Group Item Listings";
        $data["bclvl1"] = "Group Item Listings";
        $data["bclvl1_url"] = route('cylindergroups.index');

        return view('cylindermaster.cylindergroups.index', $data);
    }

    public function create(){
        
        $data["page_title"] = "Add Group";
        $data["bclvl1"] = "Group Item Listings";
        $data["bclvl1_url"] = route('cylindergroups.index');
        $data["bclvl2"] = "Add Group";
        $data["bclvl2_url"] = route('cylindergroups.create');

        return view('cylindermaster.cylindergroups.create', $data);
    }

    public function store(Request $request)
    {
        $cylindergroup = Cylindergroup::create([
            'code' => $request['code'],
            'descr' => $request['descr'],
            'active' => $request['active'],
            'created_by'=> Auth::user()->id
        ]);
        return redirect()->route('cylindergroups.index')->with('Success', 'Cylindergroup created successfully.');

    }

    public function edit($id){

        $data["cylindergroup"] = Cylindergroup::find($id);
        $data["page_title"] = "Edit Group";
        $data["bclvl1"] = "Group Item Listings";
        $data["bclvl1_url"] = route('cylindergroups.index');
        $data["bclvl2"] = "Edit Group";
        $data["bclvl2_url"] = route('cylindergroups.edit', ['id'=>$id]);

        return view('cylindermaster.cylindergroups.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cylindergroup = Cylindergroup::find($id);
        if (isset($cylindergroup)) {
            $cylindergroup->update([
                'code' => $request['code'],
                'descr' => $request['descr'],
                'active' => $request['active'],
                'updated_by'=> Auth::user()->id
            ]);
        }
        return redirect()->route('cylindergroups.index')->with('Success', 'Cylindergroup updated successfully.');
    }

    public function destroy(cylindergroup $cylindergroup)
    {
        $cylindergroup->update([
            'deleted_by' => Auth::user()->id
        ]);
        $cylindergroup->delete();
        return redirect()->route('cylindergroups.index')->with('Success', 'Cylindergroup deleted successfully.');
    }
}
