<?php

namespace App\Http\Controllers;

use App\Model\Stockcode;
use App\Model\Category;
use App\Model\Product;
use App\Model\Brand;
use App\Model\Location;
use App\Model\Uom;
use App\Model\PrintedIndexView;
use App\Model\View\StockLedger;
use DateTime;
use Illuminate\Http\Request;
use DB;
use PHPJasper\PHPJasper;
use Auth;
use Carbon\Carbon;

class StockLedgerController extends Controller
{

    public function printStkLedger(Request $request)
    {           
        $getprinted = PrintedIndexView::where('index', 'Stock Ledger Listing')->pluck('printed');
        if(!$getprinted->isEmpty()){
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Stock Ledger Listing')->update([
                'index' => "Stock Ledger Listing",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
                ]);                    
        }else{
            PrintedIndexView::create([
            'index' => "Stock Ledger Listing",
            'printed' => 1,
            'printed_at' => Carbon::now(),
            'printed_by' => Auth::user()->name
            ]);  

        }  

        $dates = explode("-", $request->dates);
        $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
        $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');

        $stockLedger = $this->getStkLedgerQuery($request, $dates_from, $dates_to);
        if ($stockLedger->isEmpty()) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        $stocksJSON = $this->getStockLedgerJSON($stockLedger, $request, $dates_from, $dates_to);

        $JsonFileName = "StockLedger" . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/StockLedger/' . $JsonFileName . '.json'), $stocksJSON);
        $JasperFileName = "StockLedger" . date("Ymdhisa") . Auth::user()->id;
        $file = $this->printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to);

        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $JasperFileName . 'pdf');
        readfile($file);
        unlink($file);
        flush();
        exit;
    }

    public function printReport($JsonFileName, $JasperFileName, $dates_from, $dates_to)
    {
        $input = base_path() . '/resources/reporting/StockLedger/StockLedger.jrxml';
        $output = base_path() . '/resources/reporting/StockLedger/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/StockLedger/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no'),
                "date_from" => date('d/m/Y', strtotime($dates_from)),
                "date_to" => date('d/m/Y', strtotime($dates_to))
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/StockLedger/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getStkLedgerQuery($request, $dates_from, $dates_to)
    {
        return StockLedger::join('stockcodes', 'view_stock_balance.item_code', '=', 'stockcodes.code')
            ->select(DB::raw('item_code, uom, count(view_stock_balance.t_type) as countt_type, SUM(totalqty) as stock_bal'))
            ->where(function ($query) use ($request, $dates_from, $dates_to) {
                if ($dates_from == $dates_to) {
                    $query->where('t_date', '=', $dates_from);
                } else {
                    $query->whereBetween('t_date', [$dates_from, $dates_to]);
                }

                if ($request->Active_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 0);
                }
                if ($request->Inactive_Chkbx == "on") {
                    $query->where('stockcodes.inactive', '=', 1);
                }
                if ($request->STK_Code_Chkbx == "on") {
                    $query->whereBetween('item_code', [$request->STK_Code_frm, $request->STK_Code_to]);
                }
                if ($request->Category_Chkbx == "on") {
                    $categoryidfrm = Category::select('id')->where('code', $request->Category_frm)->first();
                    $categoryidto = Category::select('id')->where('code', $request->Category_to)->first();
                    if ($categoryidfrm != null && $categoryidto != null) {
                        $cat_id_frm = $categoryidfrm->id;
                        $cat_id_to = $categoryidto->id;
                    } else {
                        $cat_id_frm = null;
                        $cat_id_to = null;
                    }
                    $query->whereBetween('stockcodes.cat_id', [$cat_id_frm, $cat_id_to]);
                }
                if ($request->Product_Chkbx == "on") {
                    $productidfrm = Product::select('id')->where('code', $request->Product_frm)->first();
                    $productidto = Product::select('id')->where('code', $request->Product_to)->first();
                    if ($productidfrm  != null && $productidto != null) {
                        $prod_id_frm = $productidfrm->id;
                        $prod_id_to = $productidto->id;
                    } else {
                        $prod_id_frm = null;
                        $prod_id_to = null;
                    }
                    $query->whereBetween('stockcodes.prod_id', [$prod_id_frm, $prod_id_to]);
                }
                if ($request->Brand_Chkbx == "on") {
                    $brandidfrm = Brand::select('id')->where('code', $request->Brand_frm)->first();
                    $brandidto = Brand::select('id')->where('code', $request->Brand_to)->first();
                    if ($brandidfrm  != null && $brandidto != null) {
                        $brand_id_frm = $brandidfrm->id;
                        $brand_id_to = $brandidto->id;
                    } else {
                        $brand_id_frm = null;
                        $brand_id_to = null;
                    }
                    $query->whereBetween('stockcodes.brand_id', [$brand_id_frm, $brand_id_to]);
                }
                if ($request->Location_Chkbx == "on") {
                    $locationidfrm = Location::select('id')->where('code', $request->Location_frm)->first();
                    $locationidto = Location::select('id')->where('code', $request->Location_to)->first();
                    if ($locationidfrm != null && $locationidto != null) {
                        $location_id_frm = $locationidfrm->id;
                        $location_id_to = $locationidto->id;
                    } else {
                        $location_id_frm = null;
                        $location_id_to = null;
                    }
                    $query->whereBetween('stockcodes.loc_id', [$location_id_frm, $location_id_to]);
                }
            })
            ->groupBy('item_code')
            ->get();
    }

    public function getStockLedgerJSON($stockLedger, $request, $dates_from, $dates_to)
    {
        $dataArray = array();

        foreach ($stockLedger as $stockLed) {
            $objectJSON = [];

            //Get Code Column
            $objectJSON['stock_code'] =  $stockLed->item_code;

            $stockTransaction = StockLedger::selectRaw('item_code, t_type, doc_no, SUM(totalqty) as totalquantity, t_date')
                ->where(function ($query) use ($stockLed, $dates_from, $dates_to) {
                    if ($dates_from == $dates_to) {
                        $query->where('t_date', '=', $dates_from);
                    } else {
                        $query->whereBetween('t_date', [$dates_from, $dates_to]);
                    }
                    $query->where('item_code',   $stockLed->item_code);
                })
                ->groupBy('item_code', 't_type', 'doc_no', 'type_seq', 't_date')
                ->orderBy('type_seq')
                ->orderBy('doc_no')
                ->get();

            $stockBF = StockLedger::selectRaw('SUM(totalqty) as b_f')
                ->whereDate('t_date', '<', $dates_from)
                ->where('item_code',   $stockLed->item_code)
                ->groupBy('item_code')
                ->first();

            if ($stockBF == null) {
                $objectJSON['balance_f'] = 0;
            } else {
                $objectJSON['balance_f'] = $stockBF->b_f;
            }

            $stockCodes = Stockcode::select('descr')->where('code', $stockLed->item_code)->first();

            $s_n = 1;
            $tot_in = 0;
            $tot_out = 0;
            foreach ($stockTransaction as $stockTrans) {
                $objectJSON['description'] = $stockCodes->descr;
                $objectJSON['uom'] = $stockLed->uom;
                $objectJSON['doc_1'] = $stockTrans->doc_no;
                $objectJSON['date_1'] = date('d/m/Y', strtotime($stockTrans->t_date));
                $objectJSON['type_1'] = $stockTrans->t_type;

                if ($stockTrans->t_type == "GR" || $stockTrans->t_type == "SR" || $stockTrans->t_type == "AIN") {
                    $objectJSON['in_qty_1'] = $stockTrans->totalquantity;
                    $objectJSON['out_qty_1'] = 0;
                    $tot_in =  $tot_in + $stockTrans->totalquantity;
                    if ($s_n == 1) {
                        $objectJSON['bal_1'] = $objectJSON['balance_f'] + abs($stockTrans->totalquantity);
                    } else {
                        $objectJSON['bal_1'] = $objectJSON['bal_1'] + abs($stockTrans->totalquantity);
                    }
                } else {
                    $objectJSON['in_qty_1'] = 0;
                    $objectJSON['out_qty_1'] = abs($stockTrans->totalquantity);
                    $tot_out =  $tot_out + abs($stockTrans->totalquantity);
                    if ($s_n == 1) {
                        $objectJSON['bal_1'] = $objectJSON['balance_f'] - abs($stockTrans->totalquantity);
                    } else {
                        $objectJSON['bal_1'] = $objectJSON['bal_1'] - abs($stockTrans->totalquantity);
                    }
                   
                }
                $objectJSON['tot_in'] = $tot_in;
                $objectJSON['tot_out'] = $tot_out;
                $s_n += 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
