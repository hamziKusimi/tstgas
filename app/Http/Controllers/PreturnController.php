<?php

namespace App\Http\Controllers;

use App\Model\View\NewMasterCode as AccountMastercode;
use App\Model\View\MasterCode;
use App\Model\Preturn;
use App\Model\Preturndt;
use App\Model\Creditor;
use App\Model\TaxCode;
use App\Model\Stockcode;
use App\Model\Uom;
use App\Model\Reason;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;
use App\Model\Location;
use App\Model\Salesman;
use PHPJasper\PHPJasper;
use App\Model\PrintedIndexView;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Model\CustomObject\Transaction;
use DB;
use DataTables;
use App\Model\Area;
use PDF;

class PreturnController extends Controller
{
    public function index()
    {
        $data["preturns"] = Preturn::orderBy('docno', 'desc')->paginate(15);
        $data["docno_select"] = Preturn::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["print"] = PrintedIndexView::where('index', 'Purchase Returns')->pluck('printed_by');
        $data["page_title"] = "Purchase Returns Listing";
        $data["bclvl1"] = "Purchase Returns Listing";
        $data["bclvl1_url"] = route('preturns.index');
        return view('dailypro/preturns.index', $data);
    }

    public function searchindex(Request $request)
    {
        $data["preturnsearch"] = Preturn::select('*')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref1', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref2', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        $data["docno_select"] = Preturn::pluck('docno', 'docno');
        $data["creditor_select"] = Creditor::pluck('accountcode', 'accountcode');
        $data["page_title"] = "Preturns Listing";
        $data["bclvl1"] = "Preturns Listing";
        $data["bclvl1_url"] = route('preturns.index');
        return view('dailypro.preturns.index', $data);
    }

    public function preturnDatatableList(Request $request)
    {
        $columns = array(
                            0 => 'id',
                            1 => 'docno',
                            2 => 'date',
                            3 => 'account_code',
                            4 => 'name',
                            5 => 'ref1',
                            6 => 'ref2',
                            7 => 'taxed_amount',
                        );

        $totalData = Preturn::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $preturns = Preturn::offset($start)
                         ->limit($limit)
                         ->orderBy('date','DESC')
                         ->orderBy('docno','DESC')
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $preturns =  Preturn::Where('docno', 'LIKE',"%{$search}%")
                            ->orWhere('account_code', 'LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('ref1', 'LIKE',"%{$search}%")
                            ->orWhere('ref2', 'LIKE',"%{$search}%")
                            ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                            ->orWhere('date', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('date','DESC')
                            ->orderBy('docno','DESC')
                            ->get();

            $totalFiltered = Preturn::Where('docno', 'LIKE',"%{$search}%")
                             ->orWhere('account_code', 'LIKE',"%{$search}%")
                             ->orWhere('name', 'LIKE',"%{$search}%")
                             ->orWhere('ref1', 'LIKE',"%{$search}%")
                             ->orWhere('ref2', 'LIKE',"%{$search}%")
                             ->orWhere('taxed_amount', 'LIKE',"%{$search}%")
                             ->orWhere('date', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($preturns))
        {
            foreach ($preturns as $key => $preturn)
            {
                $delete =  route('preturns.destroy', $preturn->id);
                $edit =  route('preturns.edit', $preturn->id);

                $nestedData['id'] = $key + 1;
                if(Auth::user()->hasPermissionTo('PURCHASE_RT_UP')){
                    $nestedData['docno'] = '<a href="'.$edit.'">'.$preturn->docno.'</a>';
                }else{
                    $nestedData['docno'] = $preturn->docno;
                }
                $nestedData['date'] = date('d-m-Y',strtotime($preturn->date));
                $nestedData['account_code'] = $preturn->account_code;
                $nestedData['name'] = $preturn->name;
                $nestedData['ref1'] = $preturn->ref1;
                $nestedData['ref2'] = $preturn->ref2;
                $nestedData['taxed_amount'] = $preturn->taxed_amount;
                if(Auth::user()->hasPermissionTo('PURCHASE_RT_DL')){
                    $nestedData['more'] = '&emsp;<a href="'.$delete.'" title="Delete" data-method="delete" data-confirm="Confirm delete this account?" ><span class="fa fa-trash"></span></a>';
                }else{
                    $nestedData['more'] = '<p>  </p>';
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    'draw'            => intval($request->input('draw')),
                    'recordsTotal'    => intval($totalData),
                    'recordsFiltered' => intval($totalFiltered),
                    'data'            => $data
                    );
        echo json_encode($json_data);
    }

    public function api_store(Request $request)
    {
        $data = Preturn::select('id', 'docno', 'account_code', 'name', 'updated_at')
            ->where(function ($query) use ($request) {
                $query->orWhere('docno', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('account_code', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref1', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('ref2', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('taxed_amount', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('date', 'LIKE', '%' . $request['search'] . '%');
            })
            ->orderBy('date')
            ->get();

        return response()->json($data);
    }

    public function docnoList()
    {
        $query = Preturn::query()
            ->selectRaw('docno, date, name');

        return DataTables::of($query)->make(true);
    }

    public function debtorList()
    {
        $query = Creditor::query()
            ->selectRaw('accountcode, name')
            ->where('active', '<>', '0');

        return DataTables::of($query)->make(true);
    }



    public function create()
    {
        $masterCodes = AccountMastercode::isCreditor()->get();
        // $masterCodes = AccountMastercode::get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        // $templates = TemplateMaster::all();
        $runningNumber = DocumentSetup::findByName('Purchase Returns');
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountcode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        // $data['templates'] = $templates;
        $data['taxCodes'] = $taxCodes;
        $data["runningNumber"] = $runningNumber;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Add Purchase Returns";
        $data["bclvl1"] = "Purchase Returns Received Listing";
        $data["bclvl1_url"] = route('preturns.index');
        $data["bclvl2"] = "Add Purchase Returns";
        $data["bclvl2_url"] = route('preturns.create');
        // return response()->json($data);

        return view('dailypro/preturns.create', $data);
    }

    public function store(Request $request)
    {
        // return response()->json($request);

        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        $Preturnid = Preturn::create([
            'docno' => $request['doc_no'],
            'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
            'ref1' => $request['ref1'],
            'ref2' => $request['ref2'],
            'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
            'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
            'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
            'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
            'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
            'addr1' => $request['addr1'],
            'addr2' => $request['addr2'],
            'addr3' => $request['addr3'],
            'addr4' => $request['addr4'],
            'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
            'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
            'balance' => $request['balance'],
            'header' => !empty($request['header']) ? $request['header'] : '',
            'footer' => !empty($request['footer']) ? $request['footer'] : '',
            'summary' => !empty($request['summary']) ? $request['summary'] : '',
            'created_by' => Auth::user()->id
        ]);
        $invoiceDoc = DocumentSetup::findByName('Purchase Returns');
        $invoiceDoc->update(['D_LAST_NO' => $invoiceDoc->D_LAST_NO + 1]);

        if (count($request['item_code']) > 1) {
            for ($i = 1; $i < count($request['item_code']); $i++) {
                $dt = Preturndt::create([
                    'doc_no' => $Preturnid->docno,
                    'sequence_no' => $request['sequence_no'][$i],
                    'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                    'sn' => $request['sequence_no'][$i],
                    'item_code' => $request['item_code'][$i],
                    'subject' => $request['subject'][$i],
                    'details' => $request['details'][$i],
                    'qty' => $request['quantity'][$i],
                    'uom' =>  $request['unit_measuredt'][$i],
                    'rate' => $request['rate'][$i],
                    'ucost' => $request['unit_cost'][$i],
                    'reason' => $request['reason'][$i],
                    'gr' => $request['gr'][$i],
                    'discount' => $request['discount'][$i],
                    'amount' => $request['amount_dt'][$i],
                    'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                    'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                    'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                    'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                    'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                    'created_by' => Auth::user()->id

                ]);

                // Update stockabalance
                $stock = Stockcode::where('code', $dt->item_code)->first();
                $stkBalance = $stock->stkbal - $dt->totalqty;
                $stock->update(['stkbal' => $stkBalance]);
            }
        }

        return redirect()->route('preturns.edit', ['id' => $Preturnid])->with('Success', 'Purchase Returns added sucessfully');
    }

    public function edit($id)
    {
        $preturn = Preturn::find($id);
        $contents = Preturndt::where('doc_no', $preturn->docno)->get();
        $selectedCreditor = Creditor::findByAcode($preturn->account_code);
        $masterCodes = AccountMastercode::isCreditor()->get();
        $taxCodes = TaxCode::all();
        $stockcode = Stockcode::get();
        $generalLedgers = AccountMastercode::glmt()->pluck('m_detail', 'm_id');
        $existingAcodes = Creditor::all()->pluck('accountCode');
        $uom = Uom::get();
        $systemsetup = SystemSetup::first();

        $data = [];
        $data['item'] = $preturn;
        $data['contents'] = $contents;
        $data['masterCodes'] = $masterCodes;
        $data['stockcode'] = $stockcode;
        $data['selectedCreditor'] = $selectedCreditor;
        $data['taxCodes'] = $taxCodes;
        $data['generalLedgers'] = $generalLedgers;
        $data['existingAcodes'] = $existingAcodes;
        $data["uom"] = $uom;
        $data['systemsetup'] = $systemsetup;
        $data["custom1"] = SystemSetup::pluck('custom1')->first();
        $data["custom2"] = SystemSetup::pluck('custom2')->first();
        $data["custom3"] = SystemSetup::pluck('custom3')->first();
        $data["custom4"] = SystemSetup::pluck('custom4')->first();
        $data["custom5"] = SystemSetup::pluck('custom5')->first();
        $data["custom1_type"] = SystemSetup::pluck('custom1_type')->first();
        $data["custom2_type"] = SystemSetup::pluck('custom2_type')->first();
        $data["custom3_type"] = SystemSetup::pluck('custom3_type')->first();
        $data["custom4_type"] = SystemSetup::pluck('custom4_type')->first();
        $data["custom5_type"] = SystemSetup::pluck('custom5_type')->first();
        $data["price1"] = SystemSetup::pluck('price1')->first();
        $data["price2"] = SystemSetup::pluck('price2')->first();
        $data["price3"] = SystemSetup::pluck('price3')->first();
        $data["price4"] = SystemSetup::pluck('price4')->first();
        $data["price5"] = SystemSetup::pluck('price5')->first();
        $data["price6"] = SystemSetup::pluck('price6')->first();
        $data["salesmans"] = Salesman::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["areas"] = Area::where('active', '<>', '0')->pluck('code', 'code')->all();
        $data["page_title"] = "Edit Purchase Returns";
        $data["bclvl1"] = "Purchase Returns Received Listing";
        $data["bclvl1_url"] = route('preturns.index');
        $data["bclvl2"] = "Edit Purchase Returns";
        $data["bclvl2_url"] = route('preturns.edit', ['id' => $id]);

        // return response()->json( count($data["preturndts"]));
        // return response()->json($data);
        return view('dailypro/preturns.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // return response()->json($request);
        $preturn = Preturn::find($id);
        if (isset($preturn)) {
            $preturn->update([
                'docno' => $request['doc_no'],
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'ref1' => $request['ref1'],
                'ref2' => $request['ref2'],
                'discount' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'balance' => $request['balance'],
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'updated_by' => Auth::user()->id
            ]);

            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {

                    $preturndt_id = Preturndt::find($request['item_id'][$i]);
                    $stock = Stockcode::where('code', $request['item_code'][$i])->first();
                    $stkBalance = 0;

                    if ($preturndt_id != null) {
                        $stkBalance = $stock->stkbal + $preturndt_id->totalqty;
                        $preturndt_id->update([
                            'doc_no' => $preturn->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'sn' => $request['sequence_no'][$i],
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' => $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'reason' => $request['reason'][$i],
                            'gr' => $request['gr'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => $request['amount_dt'][$i],
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'updated_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stkBalance - $preturndt_id->totalqty;
                    } else {
                        $dt = Preturndt::create([
                            'doc_no' => $preturn->docno,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                            'sn' => $request['sequence_no'][$i],
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'qty' => $request['quantity'][$i],
                            'uom' =>  $request['unit_measuredt'][$i],
                            'rate' => $request['rate'][$i],
                            'ucost' => $request['unit_cost'][$i],
                            'reason' => $request['reason'][$i],
                            'gr' => $request['gr'][$i],
                            'discount' => $request['discount'][$i],
                            'amount' => $request['amount_dt'][$i],
                            'totalqty' => (($request['rate'][$i]) * ($request['quantity'][$i])),
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'created_by' => Auth::user()->id
                        ]);

                        $stkBalance = $stock->stkbal - $dt->totalqty;
                    }

                    // Update stockabalance
                    if (isset($stock))
                        $stock->update(['stkbal' => $stkBalance]);
                }
            }
        }
        // dd($preturndt);
        return redirect()->route('preturns.edit', ['id' => $id])->with('Success', 'Purchase Returns updated sucessfully');
    }

    public function destroy($id)
    {

        $preturn = Preturn::find($id);
        $preturn->update([
            'deleted_by' => Auth::user()->id
        ]);
        $preturn->delete();
        $docno = $preturn->docno;
        // return redirect()->route('preturns.index')->with('Success', 'Purchase Returns deleted successfully');

        $preturndt = Preturndt::where('doc_no', $docno);
        $preturndt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $preturndt->delete();
        return redirect()->route('preturns.index')->with('Success', 'Purchase Returns deleted successfully');
    }

    public function destroyData($id)
    {
        $data = Preturndt::find($id);

        if (isset($data)) {
            $Preturn = Preturn::where('docno','=',$data->doc_no)->first();
            $Preturn->update([
                'amount' => $Preturn->amount - $data->amount,
                'taxed_amount' => $Preturn->taxed_amount - $data->amount,
                'updated_by' => Auth::user()->id
            ]);

            $data->update([
                'deleted_by' => Auth::user()->id
            ]);
            $data->delete();
            return response()->json(['response' => 'deleted']);
        }
        return response()->json(['response' => 'failed']);
    }

    public function destroyPreturndt($id)
    {
        $preturndt = Preturndt::find($id);
        $preturndt->update([
            'deleted_by' => Auth::user()->id
        ]);
        $preturndt->delete();
        return redirect()->back()->with('Success', 'Deleted successfully');
    }

    public function jasper($id)
    {
        //update printed details
        $preturn = Preturn::find($id);
        $getprinted = Preturn::where('id', $id)->pluck('printed');
        $print = $getprinted[0];
        if (isset($preturn)) {
            $preturn->update([
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $originalResource = Preturn::find($id);
        $Resources = Preturn::where('id', $id)->get();
        $ResourcesJsonList = $this->getPreturnListingJSON($Resources);

        $formattedResource = Preturn::select(
            'docno as DOC_NO',
            'date as DOC_DATE',
            'account_code as ACC_CODE',
            'name as ACC_HOLDER',
            'addr1 as ADDR1',
            'addr2 as ADDR2',
            'addr3 as ADDR3',
            'addr4 as ADDR4',
            'tel_no as TEL',
            'fax_no as FAX',
            'amount as SUBTOTAL',
            'taxed_amount as GRANDTOTAL',
            'header as HEADER',
            'footer as FOOTER',
            'summary as SUMMARY'
        )->where('id', $id)->first();
        $source = 'PurchasedReturn';
        $jrxml = 'general-bill-one.jrxml';
        $reportTitle = 'Purchased Return';
        $jasperData = Transaction::generateBill(
            $formattedResource,
            $originalResource,
            $source,
            $jrxml,
            $reportTitle,
            json_decode($ResourcesJsonList)
        );
    }

    public function print(Request $request)
    {
        $getprinted = PrintedIndexView::where('index', 'Purchase Returns')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Purchase Returns')->update([
                'index' => "Purchase Returns",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Purchase Returns",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $data = [];
        $amount = 0;
        $qty = 0;

        $systemsetup = SystemSetup::pluck('qty_decimal')->first();

         $dates = $request->dates;
        $Preturns = $this->getPreturnsQuery($request);

        if (empty($Preturns)) {
            return "<script>alert('No Record Found');window.close();</script>";
        }

        if ($request->preturn_rcv == "listing") {
            foreach ($Preturns as $PR) {

                $amount = $amount + $PR->amount;
            }

            $pdf = PDF::loadView('dailypro/preturns.listing', [
                'PreturnsJSON' =>  $Preturns,
                'finaltotalamount' =>  $amount,
                'date' =>  $dates,
                'systemsetup' =>  $systemsetup
            ])->setPaper('A4', 'landscape');
            return $pdf->inline();
        } else {

            foreach ($Preturns as $PR) {

                $amount = $amount + $PR->totalamount;
            }

            $pdf = PDF::loadView('dailypro/preturns.summary', [
                'PreturnsJSON' =>  $Preturns,
                'finaltotalamount' =>  $amount,
                'date' =>  $dates,
                'systemsetup' =>  $systemsetup
            ])->setPaper('A4');
            return $pdf->inline();
        }
    }

    public function printReport($JsonFileName, $JasperFileName, $reportType)
    {
        $input = base_path() . '/resources/reporting/DailyProcess/PurchaseReturn/' . $reportType . '.jrxml';
        $output = base_path() . '/resources/reporting/DailyProcess/PurchaseReturn/' . $JasperFileName;

        $data_file = base_path() . '/resources/reporting/DailyProcess/PurchaseReturn/' . $JsonFileName . '.json';

        $options = [
            'format' => ['pdf'],
            'params' => [
                "company_name" => config('config.company.name'),
                "company_no" => config('config.company.company_no')
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $jasper = new PHPJasper;

        $jasper->process(
            $input,
            $output,
            $options
        )->execute();
        unlink($data_file);
        $file = base_path() . '/resources/reporting/DailyProcess/PurchaseReturn/' . $JasperFileName . '.pdf';
        return $file;
    }

    public function getPreturnsQuery(Request $request)
    {
        $systemsetupstock = SystemSetup::pluck('stock_item')->first();

        if($request->preturn_rcv == "summary"){
            $type = "GROUP BY docno";
            if($systemsetupstock == '1'){

                // $stock = "AND stockcodes.type = 'Stock Item'";
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM preturndts INNER JOIN stockcodes on ((preturndts.item_code = stockcodes.code) and (stockcodes.type = 'Stock Item'))
                WHERE preturndts.item_code = item_code and preturndts.doc_no = docno and preturndts.deleted_at IS NULL) AS totalqty,";
            }else{
                $stock = "";
                $totalqty = "(SELECT sum(qty) FROM preturndts WHERE preturndts.item_code = item_code and preturndts.doc_no = docno and preturndts.deleted_at IS NULL) AS totalqty,";

            }
        }else{
            $type = "GROUP BY docno, item_code";
            $stock = "";
            $totalqty = "";
        }

        if ($request->dates_Chkbx == "on") {
            $dates = explode("-", $request->dates);
            $dates_from = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[0]))->format('Y-m-d');
            $dates_to = DateTime::createFromFormat('d/m/Y', str_replace(' ', '', $dates[1]))->format('Y-m-d');


            if ($dates_from == $dates_to) {
                $date = "AND preturns.date = '" . $dates_from . "'";
            } else {
                $date = "AND preturns.date >= '" . $dates_from . "' AND preturns.date <= '" . $dates_to . "'";
            }

            // if ($dates_from == $dates_to) {
            //     $query->where('preturns.date', '=', $dates_from);
            // } else {
            //     $query->whereBetween('preturns.date', [$dates_from, $dates_to]);
            // }
        }else{
            $date = "";
        }

        if ($request->docno_Chkbx == "on") {

            $docno = "AND (preturns.docno >= '" . $request->docno_frm . "' AND preturns.docno <= '" . $request->docno_to . "')";

            // $query->whereBetween('preturns.docno', [$request->docno_frm, $request->docno_to]);
        }else{
            $docno = "";
        }

        if ($request->refNo_Chkbx_1 == "on") {

            if ($request->refNo_to_1 == null) {
                $ref1 = "AND (preturns.ref1 = '" . $request->refNo_frm_1 . "')";
            } else {
                $ref1 = "AND (preturns.ref1 >= '" . $request->refNo_frm_1 . "' AND preturns.ref1 <= '" . $request->refNo_to_1 . "')";
            }

            // if ($request->refNo_to == null) {
            //     $query->where('preturns.ref', "=", $request->refNo_frm);
            // } else {
            //     $query->whereBetween('preturns.ref', [$request->refNo_frm, $request->refNo_to]);
            // }
        }else{
            $ref1 = "";
        }

        if ($request->refNo_Chkbx_2 == "on") {

            if ($request->refNo_to_2 == null) {
                $ref2 = "AND (preturns.ref2 = '" . $request->refNo_frm_2 . "')";
            } else {
                $ref2 = "AND (preturns.ref2 >= '" . $request->refNo_frm_2 . "' AND preturns.ref2 <= '" . $request->refNo_to_2 . "')";
            }

            // if ($request->refNo_to == null) {
            //     $query->where('preturns.ref', "=", $request->refNo_frm);
            // } else {
            //     $query->whereBetween('preturns.ref', [$request->refNo_frm, $request->refNo_to]);
            // }
        }else{
            $ref2 = "";
        }

        if ($request->credCode_Chkbx == "on") {

            $creditor = "AND preturns.account_code >= '" . $request->credCode_frm . "' AND preturns.account_code <= '" . $request->credCode_to . "'";

            // $query->whereBetween('preturns.account_code', [$request->credCode_frm, $request->credCode_to]);
        }else{
            $creditor = "";
        }


        $Preturn = DB::select(DB::raw("
            SELECT
            `preturns`.`docno`          AS `docno`,
            `preturns`.`date`           AS `date`,
            `preturns`.`ref1`          AS `ref1`,
            `preturns`.`ref2`          AS `ref2`,
            `preturns`.`account_code`   AS `account_code`,
            `preturns`.`account_code`   AS `debtor`,
            `preturns`.`name`           AS `name`,
            `preturns`.`amount`           AS `totalamount`,
            CONCAT(CONCAT(`preturns`.`docno`),', ',DATE_FORMAT(`preturns`.`date`,'%d-%m-%Y'),', ',`preturns`.`account_code`,', ',`preturns`.`name`) AS `details`,
            `preturndts`.`subject`      AS `description`,
            `preturndts`.`id`           AS `id`,
            `preturndts`.`doc_no`       AS `doc_no`,
            `preturndts`.`item_code`    AS `item_code`,
            `preturndts`.`qty`          AS `quantity`,
            `preturndts`.`uom`          AS `uom`,
            `preturndts`.`ucost`       AS `ucost`,
            `preturndts`.`amount`       AS `amount`,
            `preturndts`.`taxed_amount` AS `tax_amount`,
            `preturndts`.`subject`      AS `subject`,
            `preturndts`.`details`      AS `DETAIL`,
            `stockcodes`.`loc_id`        AS `loc_id`,
            preturns.amount AS totalamt,
            $totalqty
            (SELECT CODE FROM locations WHERE id = stockcodes.loc_id) AS location
            FROM ((`preturns`
            JOIN `preturndts`
                ON ((`preturns`.`docno` = `preturndts`.`doc_no`)))
            JOIN `stockcodes`
            ON ((`preturndts`.`item_code` = `stockcodes`.`code`)))
            WHERE `preturndts`.`deleted_at` IS NULL AND `preturns`.`deleted_at` IS NULL
            $stock $date $docno $ref1 $ref2 $creditor
            $type
            order by docno

        "), []);

        return $Preturn;

    }

    public function getPreturnListingJSON($PurchaseReturn)
    {

        $dataArray = array();
        foreach ($PurchaseReturn as $preturn) {

            $preturnTrans = Preturndt::select(
                'item_code',
                'subject',
                'qty',
                'uom',
                'ucost',
                'amount',
                'taxed_amount',
                'details'
            )
                ->where("doc_no", "=", $preturn->docno)
                ->get();

            $lastElement = count($preturnTrans);
            $s_n = 1;
            $t_amount = 0;
            $t_qty = 0;
            foreach ($preturnTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($preturn->date));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['details'] = $preturn->docno . ", " . $date . ", " .
                    $preturn->account_code . ", " . $preturn->name;
                $objectJSON['description'] = $trans->subject;

                $Stockcode = Stockcode::select('loc_id')->where('code', '=', $trans->item_code)->first();
                $loc = Location::select('code')->where('id', '=', $Stockcode->loc_id)->first();
                $objectJSON['location'] = $loc->code;
                $objectJSON['item_code'] = $trans->item_code;
                $objectJSON['quantity'] = $trans->qty;
                $objectJSON['uom'] = $trans->uom;
                $objectJSON['unit_cost'] = $trans->ucost;
                $objectJSON['amount'] = $trans->amount;
                $objectJSON['tax_amount'] = $trans->taxed_amount;
                $objectJSON['subject'] = $trans->subject;
                $objectJSON['_DETAIL'] = $trans->details;
                $t_amount = $t_amount + $trans->amount;
                $t_qty =  $t_qty + $trans->qty;

                if ($s_n == $lastElement) {
                    $objectJSON['t_amount'] = $t_amount;
                    $objectJSON['t_qty'] = $t_qty;
                }
                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }

    public function getPreturnSummaryJSON($PurchaseReturn)
    {
        //update printed details
        $getprinted = PrintedIndexView::where('index', 'Purchase Returns')->pluck('printed');
        if (!$getprinted->isEmpty()) {
            $print = $getprinted[0];
            PrintedIndexView::where('index', 'Purchase Returns')->update([
                'index' => "Purchase Returns",
                'printed' => $print + 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        } else {
            PrintedIndexView::create([
                'index' => "Purchase Returns",
                'printed' => 1,
                'printed_at' => Carbon::now(),
                'printed_by' => Auth::user()->name
            ]);
        }

        $dataArray = array();
        $s_n = 1;
        foreach ($PurchaseReturn as $preturn) {
            $preturnTrans = Preturndt::select('amount', 'updated_at')
                ->where("doc_no", "=", $preturn->docno)
                ->get();

            foreach ($preturnTrans as $trans) {
                $objectJSON = [];
                $date = date("d/m/Y", strtotime($trans->updated_at));

                $objectJSON['s_n'] = $s_n;
                $objectJSON['doc_no'] = $preturn->docno;
                $objectJSON['date'] = $date;
                $objectJSON['ref1'] = $preturn->ref1;
                $objectJSON['ref2'] = $preturn->ref2;
                $objectJSON['creditor'] = $preturn->account_code;
                $objectJSON['name'] = $preturn->name;
                $objectJSON['t_amount'] = $trans->amount;

                $s_n = $s_n + 1;
                $dataArray[] = collect($objectJSON);
            }
        }
        return  '{"data" :' . json_encode($dataArray) . '}';
    }
}
