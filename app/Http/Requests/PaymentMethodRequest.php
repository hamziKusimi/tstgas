<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
            case 'PUT':
            case 'PATCH':
                return [
                    'P_TYPE' => 'required',
                    'P_DESC' => 'required',
                    'P_ACCOUNT_GL_ID' => 'required',
                ];
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'P_TYPE.required' => 'Type of payment is required',
            'P_DESC.required' => 'Description is required',
            'P_ACCOUNT_GL_ID.required' => 'Please select one of the accounts specified',
        ];
    }
}
