<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralLedgerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
            case 'PUT':
            case 'PATCH':
                return [
                    'M_COA_SUB' => 'required',
                    'M_ACODE' => 'required',
                    'M_DESCR' => 'required',
                    'M_CURRCODE' => 'required',
                ];
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'M_COA_SUB.required' => 'Chart of Account is required',
            'M_ACODE.required' => 'Account Code is required',
            'M_DESCR.required' => 'Description is required',
            'M_CURRCODE.required' => 'Currency Code is required',
        ];
    }
}
