<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\FileMaintenance\ChartOfAccount;

class ChartOfAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Several fields are omitted here because they'll be prefilled from the start
     * for the C_HD case, its a checkbox so it can be allowed to be empty,
     * which will be converted to 'N'; where otherwise, if checked, will be 'Y'
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'C_DESCR' => 'required',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'C_DESCR' => 'required',
                ];
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'C_DESCR.required' => 'Description is required',
        ];
    }
}
