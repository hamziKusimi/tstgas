<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\FileMaintenance\ItemMaster;

class ItemMasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
                case 'POST':
                    return [
                        'I_CODE' => 'required|unique:items',
                        'I_DESC' => 'required',
                        'I_DETAIL' => 'required',
                        'I_UOM' => 'required',
                    ];
                case 'PUT':
                case 'PATCH':
                    $item = ItemMaster::find($this->route('item_master'));
                    return [
                        'I_CODE' => 'required|unique:items,I_CODE,'.$item->ID,
                        'I_DESC' => 'required',
                        'I_DETAIL' => 'required',
                        'I_UOM' => 'required',
                    ];
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'I_CODE.required' => 'Code is required',
            'I_CODE.unique' => 'Code must be unique',
            'I_DESC.required' => 'Description is required',
            'I_DETAIL.required' => 'Detail is required',
            'I_UOM.required' => 'Unit of Measurement (UOM) is required',
        ];
    }
}
