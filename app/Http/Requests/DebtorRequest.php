<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\FileMaintenance\Debtor;

class DebtorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {

            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
            case 'PUT':
            case 'PATCH':
                return [
                    'D_DC' => 'required',
                    'name' => 'required',
                ];
            default:
                break;
        }
    }

    /**
     * Creates a custom message after request has been passed successfully or not
     * This is required for every requests due to our database attribute format gets automatically
     * converted from `D_ACODE` into `d a c o d e`
     * */
    public function messages()
    {
    	// Session::flash('selected-tab', true);
    	return [
            'D_DC.required' => 'Account Type is required',
            'D_name.required' => 'Name is required',
        ];
    }
}
