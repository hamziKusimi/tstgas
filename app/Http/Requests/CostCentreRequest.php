<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\FileMaintenance\CostCentre;

class CostCentreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'C_CODE' => 'required|unique:ccentre',
                    'C_DESCR' => 'required',
                ];
            case 'PUT':
            case 'PATCH':
                $costCentre = CostCentre::find($this->route('cost_centre'));
                return [
                    'C_CODE' => 'required|unique:ccentre,C_CODE,'.$costCentre->ID,
                    'C_DESCR' => 'required',
                ];
            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'C_CODE.required' => 'Code is required',
            'C_CODE.unique' => 'Code must be unique',
            'C_DESCR.required' => 'Description is required',
        ];
    }
}
