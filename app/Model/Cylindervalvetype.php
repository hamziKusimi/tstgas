<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cylindervalvetype extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code',
        'descr',
        'active',
        'updated_by',
        'created_by',
        'deleted_by'
    ];

    public function cylinders()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Cylinder', 'valvetype_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
