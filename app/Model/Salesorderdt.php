<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salesorderdt extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'doc_no',
        'sequence_no',
        'account_code',
        'item_code',
        'subject',
        'details',
        'qty',
        'uom',
        'rate',
        'uprice', 
        'discount',
        'amount',
        'totalqty',
        'tax_code',
        'tax_rate',
        'tax_amount',
        'taxed_amount',
        'created_by',
        'updated_by'
    ];
    
    public function stockcode()
    {
        return $this->belongsTo('App\Model\Stockcode', 'stockcode_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}

