<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Uom;
use App\Model\Category;

use Spatie\Activitylog\Traits\LogsActivity;

class Stockcode extends Model
{
    use SoftDeletes, LogsActivity;
    protected $fillable = [
            'code',
            'descr',
            'edit',
            'inactive',
            'ref1',
            'ref2',
            'model',
            'cat_id',
            'type',
            'prod_id',
            'weight',
            'brand_id',
            'loc_id',
            'minstkqty',
            'maxstkqty',
            'uom_id1',
            'uom_id2',
            'uom_id3',
            'stkbal',
            'laststkch',
            'currency',
            'volume',
            'uomrate2',
            'uomrate3',
            'unitcost',
            'prevcost',
            'avecost',
            'curcost',
            'minprice',
            'price1',
            'price2',
            'price3',
            'price4',
            'price5',
            'price6',
            'unitcost1',
            'unitcost2',
            'unitcost3',
            'unitcost4',
            'unitcost5',
            'unitcost6',
            'unitcost7',
            'remark',
            'memo',
            'image',
            'created_by',
            'updated_by',
            'deleted_by'
    ];

    protected static $logAttributes = [
        'code',
        'descr',
    ];

    protected $appends = [
        'uom',
        'uoms',
        'itemMasterCode',
        // 'categorycode',
    ];

    
    public function getItemMastercodeAttribute()
    {
        return $this->code . ' ' . $this->descr;
    }

    public function getUomAttribute()
    {
        return Uom::find($this->uom_id1);
    }

    public function getUomsAttribute()
    {
        $uom1 = Uom::find($this->uom_id1);
        $uom2 = Uom::find($this->uom_id2);
        $uom3 = Uom::find($this->uom_id3);

        $uoms = [];
        // ['1' => 'box']!in_array($value, $liste, true)
        if (isset($uom1))
            array_push($uoms, $uom1);

        if (isset($uom2))
            array_push($uoms, $uom2);

        if (isset($uom3))
            array_push($uoms, $uom3);
        
        return array_unique($uoms);
    }
       

    public function category()
    {
        return $this->belongsTo('App\Model\Category','cat_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product','prod_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Model\Brand','brand_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Model\Location','loc_id');
    }

    public function uom_id1()
    {
        return $this->belongsTo('App\Model\Uom','uom_id1');
    }

    public function uom_id2()
    {
        return $this->belongsTo('App\Model\Uom','uom_id2');
    }

    public function uom_id3()
    {
        return $this->belongsTo('App\Model\Uom','uom_id3');
    }



    public function gooddts()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Gooddt','stockcode_id');
    }

    public function preturndts()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Preturndt','stockcode_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
