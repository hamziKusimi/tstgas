<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\View\DeliverynotegrMaster;

class DeliveryNoteGrdt extends Model
{  
    use SoftDeletes;
    protected $fillable = [
        'dn_no', 
        'sequence_no',
        'account_code',
        'item_code',
        'type',
        'qty',
        'daily_price',
        'monthly_price',
        'bill_date',
        'updated_by',
        'created_by', 
        'deleted_by'     
    ];

    protected $append = [
        'gasracks'
    ];

    public function getGasracksAttribute()
    {
        return DeliverynotegrMaster::where('dn_no', $this->dn_no)
            ->where('dt_sqn', $this->sequence_no)
            ->get();


    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

}
