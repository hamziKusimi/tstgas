<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cylindercontainertypes extends Model
{
    protected $fillable = [
        'code',
        'descr',
        'active',
        'updated_by',
        'created_by',
    ];

    public function cylinders()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Cylinder', 'conttype_id');
    }
}
