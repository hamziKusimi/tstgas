<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Repair extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'rp_no',
        'tech',
        'logindate',
        'barcode',
        'serial',
        'completedate',
        'charge_in',
        'charge_out',
        'fault',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
