<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code',
        'descr',
        'active',
        'created_by',
        'updated_by',
        'deleted_by',
        'dr_cashpurchase_acc',
        'dr_creditpurcase_acc',
        'cr_purchasereturn_acc',
        'cr_cashsales_acc',
        'dr_cashsales_return_acc',
        'cr_invoicesales_acc',
        'dr_creditsales_return_acc'
    ];
    protected $appends = [
        'itemMasterCode'
    ];

    public function getItemMastercodeAttribute()
    {
        return $this->code . ' ' . $this->descr;
    }

    public function stockcodes()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Stockcode', 'cat_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    public function isUsedInStockcode()
    {
        $stockcode = Stockcode::where('cat_id', $this->id)->count();

        if ($stockcode > 0) {
            return true;
        }else{
            return false;
        }
    }
}
