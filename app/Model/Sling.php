<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sling extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'barcode',
        'serial',
        'mfr',
        'owner',
        'type',
        'loadlimit',
        'dimension',
        'legged',
        'certno',
        'testdate',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
