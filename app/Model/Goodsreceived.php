<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Goodsreceived extends Model
{
    protected $fillable = [
        'doc_no',
        'date',
        'm_duedate',
        'batch_no',
        'm_lotno',
        'rent_sold',
        'm_ti',
        'amount',
        'm_ttax',
        'tax_amount',
        'taxed_amount',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'fax_no',
        'credit_term',
        'header',
        'footer',
        'summary',
        'reference1',
        'reference2',
        'reference3',
        'reference4',
        'reference5',
        'reference6',
        'reference7',
        'reference8',
        'reference9',
        'reference10',
        'm_refcap1',
        'm_upricelbl',
        'm_post',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'currency',
        'exchange_rate',
        'rounding',
        'm_dontpost',
        'm_print',
    ];

    protected $appends = [
        'editRoute',
        'debtorAccountCode'
    ];

    public function getEditRouteAttribute()
    {
        return route('invoices.edit', isset($this->id) ? $this->id : 'id');
    }

    // since there's no primary keys, connect by using attribute rather than using relations
    public function getDataAttribute()
    {
        return InvoiceData::where('doc_no', $this->doc_no)
            ->orderBy('sequence_no')
            ->get();
    }

    public function getDebtorAccountCodeAttribute()
    {
        return $this->account_code;
    }

    public function getInvoiceDateAttribute() // : date
    {
        return Carbon::parse($this->date);
    }

    public function getDueDateAttribute() // : date
    {
        return Carbon::parse($this->date)->addDays($this->termsInDays);
    }

    public function getTermsInDaysAttribute() // : int
    {
        if ($this->credit_term) {
            return (int) str_replace('DAYS', '', $this->credit_term);
        }

        return 0;
    }

    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }

    public function getSubTotalAttribute()
    {
        $sum = 0;
        foreach ($this->data as $data)
            $sum += $data->taxed_amount;

        return $sum;
    }

    public function getTaxAmountAttribute()
    {
        $tax = 0;
        foreach ($this->data as $data)
            $tax += $data->tax_amount;

        return $tax;
    }

    public function getGrandTotalAttribute()
    {
        return $this->taxed_amount;
    }

    public static function createRecord($request)
    {
        return response()->json($request);
        
        return response()->json($request['doc_no']);
       
        return $invoice;
    }

    public static function updateRecord($id, $request)
    {
        $address = preg_split('/\r\n|[\r\n]/', $request['detail']);
        $request['addr1'] = isset($address[0]) ? $address[0] : '';
        $request['addr2'] = isset($address[1]) ? $address[1] : '';
        $request['addr3'] = isset($address[2]) ? $address[2] : '';
        $request['addr4'] = isset($address[3]) ? $address[3] : '';

        // update acc_invmt row
        $invoice = Invoice::find($id);
        if (isset($invoice)) {
            // updating to opening balance
            // $debtor = Debtor::findByAcode($invoice->account_code);
            // $debtor->update([ 'D_YOPEN' => $debtor->D_YOPEN - $invoice->taxed_amount ]);

            // update invoice
            $invoice->update([
                'doc_no' => !empty($request['doc_no']) ? $request['doc_no'] : '',
                'date' => Carbon::createFromFormat('d/m/Y', $request['date']),
                'm_duedate' => Carbon::createFromFormat('d/m/Y', $request['due_date']),
                'batch_no' => !empty($request['batch_no']) ? $request['batch_no'] : '',
                'm_lotno' => !empty($request['m_lotno']) ? $request['m_lotno'] : 0,
                'rent_sold' => isset($request['rent_sold']) ? $request['rent_sold'] : 0,
                'm_ti' => isset($request['discount_mt']) ? $request['discount_mt'] : '',
                'amount' => !empty($request['subtotal']) ? $request['subtotal'] : 0.00,
                'm_ttax' => 0.00,
                'tax_amount' => !empty($request['tax']) ? $request['tax'] : 0.00,
                'taxed_amount' => !empty($request['grand_total']) ? $request['grand_total'] : 0.00,
                'account_code' => !empty($request['account_code_mt']) ? $request['account_code_mt'] : '',
                'name' => !empty($request['debtor_name']) ? $request['debtor_name'] : '',
                'addr1' => $request['addr1'],
                'addr2' => $request['addr2'],
                'addr3' => $request['addr3'],
                'addr4' => $request['addr4'],
                'tel_no' => !empty($request['tel_no']) ? $request['tel_no'] : '',
                'fax_no' => !empty($request['fax_no']) ? $request['fax_no'] : '',
                'credit_term' => !empty($request['credit_term']) ? $request['credit_term'] : '',
                'header' => !empty($request['header']) ? $request['header'] : '',
                'footer' => !empty($request['footer']) ? $request['footer'] : '',
                'summary' => !empty($request['summary']) ? $request['summary'] : '',
                'currency' => !empty($request['currency']) ? $request['currency'] : '',
                'exchange_rate' => !empty($request['exchange_rate']) ? $request['exchange_rate'] : 0.00,
                'rounding' => !empty($request['rounding']) ? $request['rounding'] : 0.00,
                'gl_description' => !empty($request['gl_description']) ? $request['gl_description'] : '',
            ]);

            // after update, then re-add the new value
            // $debtor->update([ 'D_YOPEN' => $debtor->D_YOPEN + $invoice->taxed_amount ]);

            // populate data for acc_invdt, first row is a template, so we start with the second row
            if (count($request['item_code']) > 1) {
                for ($i = 1; $i < count($request['item_code']); $i++) {
                    $dt = InvoiceData::find($request['item_id'][$i]);
                    if (isset($dt)) {
                        // undo the posting temporarily
                        // $gl = GeneralLedger::findByAcode($dt->account_code);
                        // if (isset($gl)) {
                        //     $opening = $gl->M_YROPEN;
                        //     $gl->update(['M_YROPEN' => $opening - $dt->taxed_amount]);
                        // }

                        $dt->update([
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'quantity' => $request['quantity'][$i],
                            'unit_measure' => $request['unit_measure'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            't_idisctype' => ($request['t_idisctype'][$i] == 0) ? 0 : 1,
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            't_fullamt' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'exchange_rate' => $invoice->exchange_rate,
                        ]);

                        // update the posting data with updated value
                        // if (isset($gl)) {
                        //     $opening = $gl->M_YROPEN;
                        //     $gl->update(['M_YROPEN' => $opening + $dt->taxed_amount]);
                        // }

                    } else {
                        $dt = InvoiceData::create([
                            'doc_no' => $invoice->doc_no,
                            'sequence_no' => $request['sequence_no'][$i],
                            'account_code' => !empty($request['account_code'][$i]) ? $request['account_code'][$i] : '',
                            'item_code' => $request['item_code'][$i],
                            'subject' => $request['subject'][$i],
                            'details' => $request['details'][$i],
                            'quantity' => $request['quantity'][$i],
                            'unit_measure' => $request['unit_measure'][$i],
                            'unit_price' => $request['unit_price'][$i],
                            't_idisctype' => ($request['t_idisctype'][$i] == 0) ? 0 : 1,
                            'discount' => $request['discount'][$i],
                            'amount' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            't_fullamt' => !empty($request['amount_dt'][$i]) ? $request['amount_dt'][$i] : 0,
                            'tax_code' => !empty($request['tax_code'][$i]) ? $request['tax_code'][$i] : '',
                            'tax_rate' => !empty($request['tax_rate_dt'][$i]) ? $request['tax_rate_dt'][$i] : 0,
                            'tax_amount' => !empty($request['tax_amount_dt'][$i]) ? $request['tax_amount_dt'][$i] : 0,
                            'taxed_amount' => !empty($request['taxed_amount_dt'][$i]) ? $request['taxed_amount_dt'][$i] : 0,
                            'exchange_rate' => $invoice->exchange_rate,
                        ]);

                        // posting to account
                            // $gl = GeneralLedger::findByAcode($dt->account_code);
                            // if (isset($gl)) {
                            //     $opening = $gl->M_YROPEN;
                            //     $gl->update(['M_YROPEN' => $opening + $dt->taxed_amount]);
                            // }
                    }
                }
            }
        }

        return $invoice;
    }

    public static function deleteRecord($id)
    {
        $resource = Invoice::find($id);

        if (isset($resource)) {
            // delete dt table items
            if (isset($resource->data)) {
                foreach ($resource->data as $dt) {
                    $gl = GeneralLedger::findByAcode($dt->account_code);
                    if (isset($gl)) {
                        $opening = $gl->M_YROPEN;
                        $gl->update(['M_YROPEN' => $opening - $dt->taxed_amount]);
                    }
                    $dt->delete();
                }
            }

            // update the year open
            $debtor = Debtor::findByAcode($resource->account_code);
            $debtor->update([ 'D_YOPEN' => $debtor->D_YOPEN - $resource->taxed_amount ]);

            $resource->delete();
        }
    }

}
