<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TaxCode extends Model
{
    protected $fillable = [
        't_code',
        't_descr1',
        't_descr2',
        't_type',
        't_rate',
        't_ttype',
        't_acode',
        't_active',
        't_keyuser',
        't_modiuser',
        't_keydt',
        't_modidt',
        't_traffic',
    ];
    
    public function getTaxInfoAttribute()
    {
        return $this->t_code . ' ' . $this->t_descr1 . ' ' . $this->t_rate;
    }
}
