<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cylinderhandwheeltype extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code',
        'descr',
        'active',
        'updated_by',
        'created_by',
        'deleted_by'
    ];

    public function cylinders()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Cylinder', 'hwtype_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
