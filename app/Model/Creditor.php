<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Creditor extends Model
{
   use SoftDeletes;
   protected $fillable =[
        'accountcode',
        'C_DC',
        'name',
        'type',
        'C_CCODE',
        'address1',
        'address2',
        'address3',
        'address4',
        'type',
        'hp',
        'tel',
        'fax',
        'cperson',
        'email',
        'cptel',
        'cterm',
        'climit',
        'ccurrency',
        'gstno',
        'brnno',
        'yopen',
        'memo',
        'created_by',
        'updated_by',
        'deleted_by',
        'active'
   ];

   public function goods()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Good', 'creditor_id');
    }

    public function preturns()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Preturn', 'creditor_id');
    }

    public static function findByAcode($acode)
    {
        return Creditor::where('accountcode', $acode)->first();
    }

    public function getAddressAttribute()
    {
        return "$this->address1 \n$this->address2 \n$this->address3 \n$this->address4";
    }

    public static function generateAcode($name)
    {
        // getting the initial to fill the second character in the account code
        $initial = !empty($name) ? $name[0] : 'A';

        // get the latest creditor and add 1 to the count (to get the next number)
        $latestCreditor = Creditor::orderBy('id', 'desc')->first();
        $sprintfFormat = "%03d";
        $runningNumber = sprintf($sprintfFormat, isset($latestCreditor) ? $latestCreditor->id + 1 : 1);

        // concatenate whole string; the cost centre code will be applied separately (into D_CCODE)
        return 'C'.$initial.$runningNumber;
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    public function getDetailAttribute()
    {
        return $this->accountcode . ' ' . $this->name;
    }
}

