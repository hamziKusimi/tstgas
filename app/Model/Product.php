<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'code','descr','active'
    ];
    protected $appends = [
        'itemMasterCode'
    ];
    
    public function getItemMastercodeAttribute()
    {
        return $this->code . ' ' . $this->descr;
    }
    
    public function stockcodes()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Stockcode', 'prod_id');
    }

    public function isUsedInStockcode()
    {
        $stockcode = Stockcode::where('prod_id', $this->id)->count();

        if ($stockcode > 0) {
            return true;
        }else{
            return false;
        }
    }
}
