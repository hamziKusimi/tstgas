<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

use App\Model\Cylindercategory;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Gasracktype;

class Cylinder extends Model
{
    use SoftDeletes, LogsActivity;
    protected $fillable = [
        'barcode',
        'serial',
        'cat_id',
        'group_id',
        'type_id',
        'prod_id',
        'descr',
        'owner',
        'mass',
        'approval',
        'approved',
        'capacity',
        'conttype',
        'hwtype',
        'testdate',
        'mfgdate',
        'mfr',
        'material',
        'neckcode',
        'oriowner',
        'oritareweight',
        'ramplot',
        'solvent',
        'valveguard',
        'valveoutlet',
        'testpressure',
        'valvetype',
        'valvemfr',
        'created_by',
        'updated_by',
        'deleted_by',
        'status',
        'reasons',
        'workingpressure'
    ];

    protected $appends = [
        'category',
        'product',
        'cytype',
    ];

    protected static $logAttributes = ['barcode', 'serial'];

    public function getCategoryAttribute()
    {
        return Cylindercategory::find($this->cat_id);
    }

    public function getProductAttribute()
    {
        return Cylinderproduct::find($this->prod_id);
    }
    
    public function getCytypeAttribute()
    {
        return Cylindertype::find($this->type_id);
    }

    public function cylindercategory()
    {
        return $this->belongsTo('App\Model\Cylindercategory', 'cat_id');
    }

    public function cylindergroup()
    {
        return $this->belongsTo('App\Model\Cylindergroup', 'group_id');
    }

    public function cylinderproduct()
    {
        return $this->belongsTo('App\Model\Cylinderproduct', 'prod_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

}
