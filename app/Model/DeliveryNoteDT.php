<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\View\DeliverynoteMaster;

class DeliveryNotedt extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'dn_no',
        'sequence_no',
        'account_code',
        'item_code',
        'subject',
        'category',
        'product',
        'capacity',
        'pressure',
        'qty',
        'daily_price',
        'monthly_price',
        'gas_price',
        'remarks',
        'bill_date',
        'updated_by',
        'created_by',
        'deleted_by'
    ];

    public function getCylindersAttribute()
    {
        return DeliverynoteMaster::where('dn_no', $this->dn_no)
            ->where('dt_sqn', $this->sequence_no)
            ->get();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
