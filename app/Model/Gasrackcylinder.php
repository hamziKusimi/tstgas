<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gasrackcylinder extends Model
{   
    use SoftDeletes;
    protected $fillable = [
        'gr_id',
        'cy_barcode',
        'cy_serial',
        'cy_category',
        'cy_product',
        'cy_descr',
        'cy_capacity',
        'cy_pressure',
        'updated_by',
        'created_by',
        'deleted_by' 
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
