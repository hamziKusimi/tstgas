<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReturnnoteMaster extends Model
{
    protected $table = 'view_new_returnnote_master';

    public function details()
    {
        $detail  = !empty($this->addr1) ? $this->addr1 . "\r\n" : '';
        $detail .= !empty($this->addr2) ? $this->addr2 . "\r\n" : '';
        $detail .= !empty($this->addr3) ? $this->addr3 . "\r\n" : '';
        $detail .= !empty($this->addr4) ? $this->addr4 . "\r\n" : '';
        $detail .= !empty($this->tel_no) ? 'Tel: ' . $this->tel_no . "\t" : '';
        $detail .= !empty($this->tel_no) && empty($this->fax_no) ? "\n" : '';
        $detail .= !empty($this->fax_no) ? 'Fax: ' . $this->fax_no . "\r\n" : '';
        $detail .= !empty($this->credit_term) ? 'Credit Term: ' . $this->credit_term . "\t" : '';
        $detail .= $this->credit_limit !== 0.00 ? 'Credit Limit: ' . $this->credit_limit : '';

        return $detail;
    }

    public static function findByAcode($acode)
    {
        return ReturnnoteMaster::where('account_code', $acode)->first();
    }

    public function scopeForAcode($query, $acode)
    {
        return $query->where('account_code', $acode);
    }

    public function scopeIsDebtor($query)
    {
        return $query->where('type', 'D');
    }

    public function scopeIsCreditor($query)
    {
        return $query->where('type', 'C');
    }

    public function scopeIsGL($query)
    {
        return $query->where('type', 'M');
    }

    /**
     * Queries to generate report
     * * */
    public static function selectDebtors()
    {
        $query = ReturnnoteMaster::isDebtor()->select('*')->orderBy('id')->get();
        return $query;
    }

    /**
     * Static functions to simulate previously App\Model\FileMaintenance\AccountMasterCode
     * */
    public static function debtor()
    {
        return ReturnnoteMaster::isDebtor()->orderBy('d_id')->get();
    }

    public static function creditor()
    {
        return ReturnnoteMaster::isCreditor()->orderBy('d_id')->get();
    }

    public static function glmt()
    {
        return ReturnnoteMaster::isGL()->get();
    }



}
