<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class CylinderTrackingView extends Model
{
    protected $table = 'cylinder_tracking_view';
}
