<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class PurchaseStock extends Model
{
    protected $table = 'view_purchase_report_stock';
}
