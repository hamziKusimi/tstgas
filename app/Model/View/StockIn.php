<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;

class StockIn extends Model
{
    //
    protected $table = 'view_stock_in';
}
