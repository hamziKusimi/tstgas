<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class SalesBybill extends Model
{
    protected $table = 'view_sales_report_bybill';
}
