<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;

class StockLedger extends Model
{
    //
    protected $table = 'view_stock_balance';
}
