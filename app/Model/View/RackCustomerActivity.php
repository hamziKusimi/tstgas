<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class RackCustomerActivity extends Model
{
    //
    protected $table = 'view_rack_customer_activity';
}
