<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class MasterCode extends Model
{
    protected $table = 'view_master_codes';

    public static function findByAcode($acode)
    {
        return MasterCode::where('accountcode', $acode)->first();
    }

    public function scopeIsDebtor($query)
    {
        return $query->where('type', 'D');
    }

    public function scopeIsCreditor($query)
    {
        return $query->where('type', 'C');
    }

    public function scopeForAcode($query, $acode)
    {
        return $query->where('accountcode', $acode);
    }
}
