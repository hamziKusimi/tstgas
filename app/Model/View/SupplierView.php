<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;

class SupplierView extends Model
{
    protected $table = 'supplier_view';
}
