<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class GasrackInvProcessing extends Model
{
    protected $table = 'view_gasrack_process_inv';
}
