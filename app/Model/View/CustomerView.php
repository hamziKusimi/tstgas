<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class CustomerView extends Model
{
    protected $table = 'customer_view';
}
