<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class DeliveryNotePrintRackView extends Model
{
    //
    protected $table = 'view_delivery_note_print_rack';
}
