<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReturnNotePrintCylinderView extends Model
{
    //
    protected $table = 'view_return_note_print_cylinder';
}
