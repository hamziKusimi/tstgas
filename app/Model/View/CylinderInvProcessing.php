<?php

namespace App\Model\View;

use Illuminate\Database\Eloquent\Model;
use DB;

class CylinderInvProcessing extends Model
{
    protected $table = 'view_cylinder_process_inv';
}
