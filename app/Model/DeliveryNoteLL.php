<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryNotell extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'dn_no',
        'sequence_no',
        'sqn_no',
        'datetime',
        'barcode',
        'serial',
        'driver',
        'type',
        'updated_by',
        'created_by',
        'deleted_by'   
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
