<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Refill extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'wo_no',
        'tech',
        'datetime',
        'barcode',
        'serial',
        'charge_in',
        'charge_out',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
