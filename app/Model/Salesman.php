<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salesman extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code',
        'descr',
        'active',
        'password',
        'password_h',
        'updated_by',
        'created_by',
        'deleted_by',
        'vehicle_no'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    public function getDetailAttribute()
    {
        return $this->code . ' ' . $this->descr;
    }
}
