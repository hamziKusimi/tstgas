<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchasedTable extends Model
{
    
    protected $fillable = [
        'doc_no',
        'date',
        'stock_code',
        'qty',
        'price',
        'amount',
    ];
    
    // public function user()
    // {
    //     return $this->belongsTo('App\User', 'userid');
    // }
}
