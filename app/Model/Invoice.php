<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

use App\Model\Debtor;
use App\Model\InvoiceData;
use App\Model\DocumentSetup;

class Invoice extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'docno',
        'date',
        'm_duedate',
        'refinvdate',
        'refno',
        'do_no',
        'salesman',
        'discount',
        'amount',
        'tax_amount',
        'taxed_amount',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'fax_no',
        'credit_term',
        'credit_limit',
        'header',
        'footer',
        'summary',
        'created_by',
        'updated_by',
        'currency',
        'exchange_rate',
        'rounding',
        'delete_by',
        'printed', 
        'printed_by', 
        'printed_at', 
    ];

    protected $appends = [
        'editRoute',
        'debtorAccountCode'
    ];

    public function getEditRouteAttribute()
    {
        return route('invoices.edit', isset($this->id) ? $this->id : 'id');
    }

    // since there's no primary keys, connect by using attribute rather than using relations
    public function getDataAttribute()
    {
        return InvoiceData::where('doc_no', $this->doc_no)
            ->orderBy('sequence_no')
            ->get();
    }

    public function getDebtorAccountCodeAttribute()
    {
        return $this->account_code;
    }

    public function getInvoiceDateAttribute() // : date
    {
        return Carbon::parse($this->date);
    }

    public function getDueDateAttribute() // : date
    {
        return Carbon::parse($this->date)->addDays($this->termsInDays);
    }

    public function getTermsInDaysAttribute() // : int
    {
        if ($this->credit_term) {
            return (int) str_replace('DAYS', '', $this->credit_term);
        }

        return 0;
    }

    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }

    public function getSubTotalAttribute()
    {
        $sum = 0;
        foreach ($this->data as $data)
            $sum += $data->taxed_amount;

        return $sum;
    }

    // public function getTaxAmountAttribute()
    // {
    //     $tax = 0;
    //     foreach ($this->data as $data)
    //         $tax += $data->tax_amount;

    //     return $tax;
    // }

    public function getGrandTotalAttribute()
    {
        return $this->taxed_amount;
    }

    public static function createRecord($request)
    {
        return response()->json($request);
        
        return response()->json($request['doc_no']);
       
        return $invoice;
    }

    public static function updateRecord($id, $request)
    {
     
        // return $invoice;
    }

    public static function deleteRecord($id)
    {
        $resource = Invoice::find($id);

        if (isset($resource)) {
            // delete dt table items
            if (isset($resource->data)) {
                foreach ($resource->data as $dt) {
                    $gl = GeneralLedger::findByAcode($dt->account_code);
                    if (isset($gl)) {
                        $opening = $gl->M_YROPEN;
                        $gl->update(['M_YROPEN' => $opening - $dt->taxed_amount]);
                    }
                    $dt->delete();
                }
            }

            // update the year open
            $debtor = Debtor::findByAcode($resource->account_code);
            $debtor->update([ 'D_YOPEN' => $debtor->D_YOPEN - $resource->taxed_amount ]);

            $resource->delete();
        }
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

}
