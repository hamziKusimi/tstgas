<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CylinderInvoicedt extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'doc_no',
        'sequence_no',
        'account_code',
        'serial',
        'barcode',
        'product',
        'type',
        'subject',
        'details',
        'quantity',
        'unit_price',
        'discount',
        'amount',
        'taxed_amount',
        'created_by',
        'updated_by',
        'exchange_rate',
        'deleted_by',
        'uom'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
