<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryNoteRack extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'dn_no',
        'rack_no',
        'sling_no',
        'gwl',
        'next_inspect_date',
        're_test_date',
        'type',
        'return_note',
        'return_note_date',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
