<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DNRNSetup extends Model
{
    // use AuditTrailTraits;

    const CREATED_AT = 'D_KEYDATE';
    const UPDATED_AT = 'D_MODIDATE';

    protected $table = 'dnrn_setups';
    protected $primaryKey = "ID";
    protected $fillable = [
        'D_NAME',
        'D_PREFIX',
        'D_SEPARATOR',
        'D_LAST_NO',
        'D_ZEROES',
        'D_ACTIVE',
        'D_KEYUSER',
        'D_MODIUSER',
        'D_KEYDATE',
        'D_MODIDATE',
    ];

    public static function scopeFindByName($query, $name)
    {
        return $query->where('D_NAME', $name)->first();
    }

    // $dnrn->lastNumberString
    public function getLastNumberStringAttribute()
    {
        $sprintfFormat = "%0{$this->D_ZEROES}d";
        return sprintf($sprintfFormat, $this->D_LAST_NO);
    }

    // $dnrn->nextNumberString
    public function getNextNumberStringAttribute()
    {
        $sprintfFormat = "%0{$this->D_ZEROES}d";
        return sprintf($sprintfFormat, $this->D_LAST_NO+1);
    }

    // $dnrn->lastRunningNoString
    public function getLastRunningNoStringAttribute()
    {
        return $this->D_PREFIX . $this->D_SEPARATOR . $this->lastNumberString;
    }

    // $dnrn->nextRunningNoString
    public function getNextRunningNoStringAttribute()
    {
        return $this->D_PREFIX . $this->D_SEPARATOR . $this->nextNumberString;
    }
}
