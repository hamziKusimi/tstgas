<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrintedIndexView extends Model
{
    protected $fillable = [
        'index',
        'printed',
        'printed_by',
        'printed_at', 
    ];
}
