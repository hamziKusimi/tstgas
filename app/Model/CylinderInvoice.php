<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class CylinderInvoice extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'docno',
        'date',
        'refinvdate',
        'refno',
        'm_duedate',
        'do_no',
        'discount',
        'amount',
        'tax_amount',
        'taxed_amount',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'fax_no',
        'credit_term',
        'credit_limit',
        'header',
        'footer',
        'summary',
        'created_by',
        'updated_by',
        'currency',
        'exchange_rate',
        'rounding',
        'delete_by'
    ];

    
    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }
}
