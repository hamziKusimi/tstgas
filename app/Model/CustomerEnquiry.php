<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerEnquiry extends Model
{
    protected $fillable = [
        'name'
    ];
}
