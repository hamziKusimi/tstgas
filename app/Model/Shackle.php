<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shackle extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'barcode',
        'mfr',
        'serial',
        'owner',
        'loadlimit',
        'type',
        'size',
        'descr',
        'coc',
        'testdate',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
