<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Model\Creditor;
use App\Model\Porderdt;
use App\Model\Gooddt;
use App\Model\DocumentSetup;
use App\Model\SystemSetup;

class CylinderPorder extends Model
{
    use SoftDeletes;
    protected $fillable = [
        
        'docno',
        'date',
        'suppdo',
        'suppinv',
        'suppinvdate',
        'ref',
        'ptype',
        'discount',
        'amount',
        'tax_amount',
        'taxed_amount',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'fax_no',
        'currency',
        'header', 
        'footer', 
        'summary', 
        'created_by',
        'updated_by',
        'deleted_by',
        'printed',
        'printed_by',
        'printed_at',

    ];
    protected $appends = [
        'editRoute',
        'debtorAccountCode',
        'dt'
    ];


    public function creditor()
    {
        return $this->belongsTo('App\Model\creditor', 'creditor_id');
    }

    public function getDtAttribute()
    {
        $goodsReceivedFromPorderDt = Gooddt::where('reference_no', $this->docno)->pluck('item_code');
        $remainingPorderDt = Porderdt::where('doc_no', $this->docno)
            ->whereNotIn('item_code', $goodsReceivedFromPorderDt)
            ->get();

        return $remainingPorderDt;
    }

    public function getEditRouteAttribute()
    {
        return route('porders.edit', isset($this->id) ? $this->id : 'id');
    }

    // since there's no primary keys, connect by using attribute rather than using relations
    public function getDataAttribute()
    {
        return Porderdt::where('id', $this->id)
            ->orderBy('sequence_no')
            ->get();
    }

    public function getDebtorAccountCodeAttribute()
    {
        return $this->account_code;
    }

    public function getGoodDateAttribute() // : date
    {
        return Carbon::parse($this->date);
    }

    public function getDueDateAttribute() // : date
    {
        return Carbon::parse($this->date)->addDays($this->termsInDays);
    }

    public function getTermsInDaysAttribute() // : int
    {
        if ($this->credit_term) {
            return (int) str_replace('DAYS', '', $this->credit_term);
        }

        return 0;
    }

    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }

    public function getSubTotalAttribute()
    {
        $sum = 0;
        foreach ($this->data as $data)
            $sum += $data->taxed_amount;

        return $sum;
    }

    // public function getTaxAmountAttribute()
    // {
    //     $tax = 0;
    //     foreach ($this->data as $data)
    //         $tax += $data->tax_amount;

    //     return $tax;
    // }

    public function getGrandTotalAttribute()
    {
        return $this->taxed_amount;
    }

    public static function createRecord($request)
    {
        return response()->json($request);
        
        return response()->json($request['doc_no']);
       
        return $porder;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
