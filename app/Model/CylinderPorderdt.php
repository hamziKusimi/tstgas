<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CylinderPorderdt extends Model
{  
    use SoftDeletes;
    protected $fillable = [
        'doc_no',
        'sequence_no',
        'account_code',
        'serial',
        'barcode',
        'product',
        'type',
        'subject',
        'details',
        'qty',
        'ucost',
        'discount',
        'amount',
        'taxed_amount',
        'created_by',
        'updated_by',
        'exchange_rate',
        'deleted_by',
        'uom'
    ];
    protected $appends = [
        'editDescr',
    ];
    
    public function getEditDescrAttribute()
    {
        return Stockcode::where('code', $this->item_code)->pluck('edit');
    }
        
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
