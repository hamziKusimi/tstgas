<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReturnNote extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'dn_no',
        'date',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'phone',
        'fax_no',
        'lpono',
        'driver',
        'charge_type',
        'start_from',
        'header', 
        'footer',
        'summary',
        'vehicle_no',
        'updated_by',
        'created_by',
        'deleted_by'
    ];
    protected $appends = [
        'editRoute',
        'debtorAccountCode'
    ];

    public function debtor()
    {
        return $this->belongsTo('App\Model\Debtor', 'debtor_id');
    }

    public function getEditRouteAttribute()
    {
        return route('returnnotes.edit', isset($this->id) ? $this->id : 'id');
    }

    // since there's no primary keys, connect by using attribute rather than using relations
    public function getDataAttribute()
    {
        return Gooddt::where('id', $this->id)
            ->orderBy('sequence_no')
            ->get();
    }

    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }
    
    public function getDebtorAccountCodeAttribute()
    {
        return $this->accountcode;
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    
}
