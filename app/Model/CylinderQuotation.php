<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CylinderQuotation extends Model
{  
    use SoftDeletes;
    protected $fillable = [
        'docno',
        'date',
        'refinvdate',
        'refno',
        'm_duedate',
        'do_no',
        'discount',
        'amount',
        'tax_amount',
        'taxed_amount',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'fax_no',
        'credit_term',
        'credit_limit',
        'header',
        'footer',
        'summary',
        'created_by',
        'updated_by',
        'currency',
        'exchange_rate',
        'rounding',
        'delete_by',
        'custom1',
        'custom2',
        'custom3',
        'custom4',
        'custom5',
        'custom6',
        'custom7',
        'custom8',
        'custom9',
        'custom10',
        'custom11',
        'custom12',
        'custom13',
        'custom14',
        'custom15',
        'custom16',
        'custom17',
        'custom18',
        'custom19',
        'custom20',
        'custom21',
        'custom22',
        'custom23',
        'custom24',
        'custom25',
        'customval1',
        'customval2',
        'customval3',
        'customval4',
        'customval5',
        'customval6',
        'customval7',
        'customval8',
        'customval9',
        'customval10',
        'customval11',
        'customval12',
        'customval13',
        'customval14',
        'customval15',
        'customval16',
        'customval17',
        'customval18',
        'customval19',
        'customval20',
        'customval21',
        'customval22',
        'customval23',
        'customval24',
        'customval25'

    ];

    
    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }
}
