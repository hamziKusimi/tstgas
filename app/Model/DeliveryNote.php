<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\DeliveryNotedt;
use App\Model\DeliveryNotepr;
use App\Model\CylinderCashbilldt;
use DB;
use Carbon\Carbon;

class DeliveryNote extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'dn_no',
        'date',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'phone',
        'fax_no',
        'lpono',
        'driver',
        'charge_type',
        'start_from',
        'header',
        'footer',
        'summary',
        'driver_remark',
        'gas_invoice_date',
        'vehicle_no',
        'updated_by',
        'created_by',
        'deleted_by'
    ];
    protected $appends = [
        'editRoute',
        'debtorAccountCode',
        'dt',
        'dt2',
        'dt3',
    ];

    
    public function getDtAttribute()
    {
        $cashbillsFromDnDt = CylinderCashbilldt::where('reference_no', $this->dn_no)->pluck('serial');
        $remainingDnDt = DeliveryNotedt::where('dn_no', $this->dn_no)
            ->whereNotIn('dn_no', $cashbillsFromDnDt)
            ->get();

        return $remainingDnDt;
    }

    public function getDt2Attribute()
    {
        $cashbillsFromDnDt = CylinderCashbilldt::where('reference_no', $this->dn_no)->pluck('serial');
        $remainingDnDt = DeliveryNotedt::where('dn_no', $this->dn_no)
            ->whereNotIn('dn_no', $cashbillsFromDnDt)
            ->get();

        return $remainingDnDt;
    }

    public function getDt3Attribute()
    {
        $cashbillsFromDnDt = CylinderCashbilldt::where('reference_no', $this->dn_no)->pluck('serial');
        
        $remainingDnDt = DB::table('delivery_noteprs')
            ->join('delivery_notedts', 'delivery_notedts.sequence_no', '=', 'delivery_noteprs.sequence_no')
            ->where('delivery_noteprs.dn_no', $this->dn_no)
            ->where('delivery_notedts.dn_no', $this->dn_no)
            ->whereNotIn('delivery_noteprs.serial', $cashbillsFromDnDt)
            ->get(['delivery_noteprs.barcode','delivery_noteprs.serial', 'delivery_noteprs.dn_no', 'delivery_noteprs.sequence_no', 'delivery_noteprs.type',
                    'delivery_notedts.sequence_no', 'delivery_notedts.dn_no', 'delivery_notedts.daily_price']);

        return $remainingDnDt;
    }

    public function debtor()
    {
        return $this->belongsTo('App\Model\Debtor', 'debtor_id');
    }

    public function getEditRouteAttribute()
    {
        return route('deliverynotes.edit', isset($this->id) ? $this->id : 'id');
    }

    // since there's no primary keys, connect by using attribute rather than using relations
    public function getDataAttribute()
    {
        return Gooddt::where('id', $this->id)
            ->orderBy('sequence_no')
            ->get();
    }

    public function getDebtorAccountCodeAttribute()
    {
        return $this->accountcode;
    }

    public function getGoodDateAttribute() // : date
    {
        return Carbon::parse($this->date);
    }

    public function getDueDateAttribute() // : date
    {
        return Carbon::parse($this->date)->addDays($this->termsInDays);
    }

    public function getTermsInDaysAttribute() // : int
    {
        if ($this->credit_term) {
            return (int) str_replace('DAYS', '', $this->credit_term);
        }

        return 0;
    }

    public function getAddressAttribute()
    {
        return $this->addr1 . "\n" . $this->addr2 . "\n" . $this->addr3 . "\n" . $this->addr4;
    }

    public function getSubTotalAttribute()
    {
        $sum = 0;
        foreach ($this->data as $data)
            $sum += $data->taxed_amount;

        return $sum;
    }

    public function getTaxAmountAttribute()
    {
        $tax = 0;
        foreach ($this->data as $data)
            $tax += $data->tax_amount;

        return $tax;
    }

    public function getGrandTotalAttribute()
    {
        return $this->taxed_amount;
    }

    public static function createRecord($request)
    {
        return response()->json($request);

        return response()->json($request['doc_no']);

        return $deliveryorder;
    }

    public static function deleteRecord($id)
    {
        $resource = Cashbill::find($id);

        if (isset($resource)) {
            // delete dt table items
            if (isset($resource->data)) {
                foreach ($resource->data as $dt) {
                    $gl = GeneralLedger::findByAcode($dt->account_code);
                    if (isset($gl)) {
                        $opening = $gl->M_YROPEN;
                        $gl->update(['M_YROPEN' => $opening - $dt->taxed_amount]);
                    }
                    $dt->delete();
                }
            }

            // update the year open
            $debtor = Debtor::findByAcode($resource->account_code);
            $debtor->update(['D_YOPEN' => $debtor->D_YOPEN - $resource->taxed_amount]);

            $resource->delete();
        }
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
