<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cashbilldt extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'doc_no',
        'sequence_no',
        'account_code',
        'item_code',
        'subject',
        'details',
        'reference_no',
        'qty',
        'uom',
        'rate',
        'uprice', 
        'discount',
        'totalqty',
        'amount',
        'tax_code',
        'tax_rate',
        'tax_amount',
        'taxed_amount',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $appends = [
        'editDescr',
    ];
    
    public function getEditDescrAttribute()
    {
        return Stockcode::where('code', $this->item_code)->pluck('edit');
    }
    
    public function stockcode()
    {
        return $this->belongsTo('App\Model\Stockcode', 'stockcode_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
