<?php
namespace App\Model\CustomObject;
use Config;
use DB;
use App\Model\SystemSetup;

class DatabaseConnection
{
    public static function setConnection()
    {
        $dbSetup = SystemSetup::select('db_server','db_name')->first();
        config(['database.connections.mysql2' => [
            'driver' => 'mysql',
            'host' => $dbSetup->db_server,
            'database' => $dbSetup->db_name,
            'username' => 'root',
            'password' =>234586,
            'port'  => 3306
        ]]);

        return DB::connection('mysql2');
    }
}