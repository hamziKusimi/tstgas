<?php

namespace App\Model\CustomObject;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;
use App\Model\SystemSetup;

class Company extends Model
{
    public static function getAddress()
    {
        $Company = SystemSetup::select('address1', 'address2', 'address3', 'address4')
            ->first();

        $address = $Company['address1'];
        $address .= str_replace(' ', '', $Company['address2']) == null ? "" : "<br>" . $Company['address2'];
        $address .= str_replace(' ', '', $Company['address3']) == null ? "" : "<br>" . $Company['address3'];
        $address .= str_replace(' ', '', $Company['address4']) == null ? "" : "<br>" . $Company['address4'];
        return $address;
    }

    public static function getTel()
    {
        $Company = SystemSetup::select('tel')
            ->first();
        return $Company->tel;
    }

    public static function getFax()
    {
        $Company = SystemSetup::select('fax')
            ->first();
        return $Company->fax;
    }

    public static function getEmail()
    {
        $Company = SystemSetup::select('email')
            ->first();
        return $Company->email;
    }
}
