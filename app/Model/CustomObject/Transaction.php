<?php

namespace App\Model\CustomObject;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;
use PHPJasper\PHPJasper;
use Auth;
use App\Model\CustomObject\Company;

class Transaction extends Model
{
    protected $fillable = [];
    public static $tensNames = [
        "",
        " TEN",
        " TWENTY",
        " THIRTY",
        " FORTY",
        " FIFTY",
        " SIXTY",
        " SEVENTY",
        " EIGHTY",
        " NINETY"
    ];
    public static $numNames = [
        "",
        " ONE",
        " TWO",
        " THREE",
        " FOUR",
        " FIVE",
        " SIX",
        " SEVEN",
        " EIGHT",
        " NINE",
        " TEN",
        " ELEVEN",
        " TWELVE",
        " THIRTEEN",
        " FOURTEEN",
        " FIFTEEN",
        " SIXTEEN",
        " SEVENTEEN",
        " EIGHTEEN",
        " NINETEEN"
    ];

    /**
     * This helper function help to process the inputs, create the datasource and display the pdf in new tab
     *
     * @param collection    $formattedResource  - Model::select()->get()
     * @param collection    $originalResource   - Model::find();
     * @param string        $source             - datasource; i.e.: "invoice".json (the .json) - later to concatenate with user id and time, to make file unique
     * @param string        $jrxml              - i.e.: general-bill.jrxml
     * **/
    public static function generateBill($formattedResource, $originalResource, $source, $jrxml, $reportTitle, $ResourcesJsonList)
    {
        $dataArray = [];
        $merge = $formattedResource->toArray();

        $GRANDTOTAL = ($merge['GRANDTOTAL'] == 0 || $merge['GRANDTOTAL'] == NULL ? $merge['SUBTOTAL'] : $merge['GRANDTOTAL']);

        // get total amount in words
        $merge['TOTAL_IN_WORDS'] = ltrim(Transaction::convertRinggit($GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= Transaction::convertCents(sprintf("%.2f", $GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= ' ONLY';
        $merge['TOTAL_IN_WORDS'] = preg_replace('/\s+/', ' ', $merge['TOTAL_IN_WORDS']);

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $mt_ADDR =  $merge['ADDR1'];
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR2']) == 'null' ? "" : $merge['ADDR2']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR3']) == 'null' ? "" : $merge['ADDR3']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR4']) == 'null' ? "" : $merge['ADDR4']);
        $mt_ACC_DETAIL = $mt_ADDR == null ? null : $mt_ADDR . "<br>";
        $mt_ACC_DETAIL .= $merge['TEL'] == null ? null : "Tel : " . $merge['TEL'] . ",<ensp>";
        $mt_ACC_DETAIL .= $merge['FAX'] == null ? null : "Fax : " . $merge['FAX'] . "<ensp>";

        $footer = preg_replace('/\s+/', ' ', preg_split("/[\r]+/", $merge['FOOTER']));
        $mt_FOOTER_LINE = "";
        foreach ($footer as $foot) {
            $mt_FOOTER_LINE .= $foot . "<br>";
        }

        // create json file and place it into the datasource
        foreach ($ResourcesJsonList->data as $item) {
            $objectJSON = [];
            $objectJSON['item_sn'] = $item->s_n;
            $objectJSON['item_code'] = $item->item_code;
            $objectJSON['subject'] = $item->description;
            $objectJSON['details'] = $item->_DETAIL;
            $objectJSON['unit_measure'] = $item->uom;
            $objectJSON['mt__HEADER'] = $merge['HEADER'];
            $objectJSON['mt__SUMMARY'] = $merge['SUMMARY'];
            $objectJSON['mt__FOOTER_LINE1'] = $mt_FOOTER_LINE;
            $objectJSON['unit_cost'] = floatval($item->unit_cost);
            $objectJSON['quantity'] = floatval($item->quantity);
            $objectJSON['taxed_amount'] = ($item->tax_amount == null || $item->tax_amount == 0 ?
                floatval($item->amount) : floatval($item->tax_amount));
            $dataArray[] = collect($objectJSON);
        }

        if (empty($dataArray)) {
            $data =  '{"data" :[{}]}';
        } else {
            $data =  '{"data" :' . json_encode($dataArray) . '}';
        }

        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);
        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'CURRENCY' => 'RINGGIT MALAYSIA',
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                'mt_ACC_HOLDER' =>  $merge['ACC_HOLDER'],
                'mt_ACC_DETAIL' =>  $mt_ACC_DETAIL,
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'mt_ACC_CODE' => $merge['ACC_CODE'],
                'mt_TOTAL_IN_WORDS' => $merge['TOTAL_IN_WORDS'],
                'mt_GRANDTOTAL' => $GRANDTOTAL
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();

        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        unlink($data_file);
        flush();
        exit;
    }

    public static function generateBillDebtor($formattedResource, $source, $jrxml, $reportTitle, $ResourcesJsonList)
    {
        $dataArray = [];
        $merge = $formattedResource->toArray();

        switch ($source) {
            case 'CashBill':
                $CTERM = "Cash";
                break;
            case 'Invoice':
                $CTERM = $merge['TERMS'];
                break;
            case 'SalesReturn':
                $CTERM = null;
                break;
            case 'DeliveryOrder':
                $CTERM = null;
                break;
            case 'Quotation':
                $CTERM = null;
                break;
            case 'Cashsales':
                $CTERM = null;
                break;
        }

        $mt_INV_DUE_DATE = $source == 'Invoice' ? date("d/m/Y", strtotime($merge['INV_Due_Date'])) : null;

        $GRANDTOTAL = ($merge['GRANDTOTAL'] == 0 || $merge['GRANDTOTAL'] == NULL ? $merge['SUBTOTAL'] : $merge['GRANDTOTAL']);

        // get total amount in words
        $merge['TOTAL_IN_WORDS'] = ltrim(Transaction::convertRinggit($GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= Transaction::convertCents(sprintf("%.2f", $GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= ' ONLY';
        $merge['TOTAL_IN_WORDS'] = preg_replace('/\s+/', ' ', $merge['TOTAL_IN_WORDS']);

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $mt_ADDR =  $merge['ADDR1'];
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR2']) == 'null' ? "" : $merge['ADDR2']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR3']) == 'null' ? "" : $merge['ADDR3']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR4']) == 'null' ? "" : $merge['ADDR4']);
        $mt_ACC_DETAIL = $mt_ADDR == null ? null : $mt_ADDR . "<br>";
        $mt_ACC_DETAIL .= $merge['TEL'] == null ? null : "Tel : " . $merge['TEL'] . ",<ensp>";
        $mt_ACC_DETAIL .= $merge['FAX'] == null ? null : "Fax : " . $merge['FAX'] . "<ensp>";

        $footer = preg_replace('/\s+/', ' ', preg_split("/[\r]+/", $merge['FOOTER']));
        $mt_FOOTER_LINE = "";
        foreach ($footer as $foot) {
            $mt_FOOTER_LINE .= $foot . "<br>";
        }

        // create json file and place it into the datasource
        foreach ($ResourcesJsonList->data as $item) {
            $objectJSON = [];
            $objectJSON['item_sn'] = $item->s_n;
            $objectJSON['item_code'] = $item->item_code;
            $objectJSON['subject'] = $item->description;
            $objectJSON['details'] = $item->_DETAIL;
            $objectJSON['unit_measure'] = $item->uom;
            $objectJSON['unit_price'] = floatval($item->unit_price);
            $objectJSON['quantity'] = floatval($item->quantity);
            $objectJSON['taxed_amount'] = ($item->tax_amount == null || $item->tax_amount == 0 ?
                floatval($item->amount) : floatval($item->tax_amount));
            $objectJSON['mt__HEADER'] = $merge['HEADER'];
            $objectJSON['mt__SUMMARY'] = $merge['SUMMARY'];
            $objectJSON['mt__FOOTER_LINE1'] = $mt_FOOTER_LINE;
            $dataArray[] = collect($objectJSON);
        }

        if (empty($dataArray)) {
            $data =  '{"data" :[{}]}';
        } else {
            $data =  '{"data" :' . json_encode($dataArray) . '}';
        }


        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);
        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'CURRENCY' => 'RINGGIT MALAYSIA',
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                // 'mt_COYBANKACC' => config('config.company.company_bank_acc'),
                // 'mt_COYBANK' => config('config.company.company_bank'),
                // 'mt_COYSWCODE' => config('config.company.company_swift_code'),
                'mt_ACC_HOLDER' =>  $merge['ACC_HOLDER'],
                'mt_ACC_DETAIL' =>  $mt_ACC_DETAIL,
                'REF_NO' =>  $merge['REF_NO'],
                'DO_NO' =>  isset($merge['DO_NO']) ? $merge['DO_NO'] : "" ,
                'mt_CTERM' =>  $CTERM,
                // 'mt_INV_DUE_DATE' => $mt_INV_DUE_DATE,
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'mt_ACC_CODE' => $merge['ACC_CODE'],
                'mt_TOTAL_IN_WORDS' => $merge['TOTAL_IN_WORDS'],
                'mt_GRANDTOTAL' => $GRANDTOTAL
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();
        //dd($PHPJasper);
        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        unlink($data_file);
        flush();
        exit;
    }

    public static function generateBillAdjustment($formattedResource, $source, $jrxml, $reportTitle, $ResourcesJsonList)
    {
        $dataArray = [];
        $merge = $formattedResource->toArray();

        $GRANDTOTAL = ($merge['GRANDTOTAL'] == 0 || $merge['GRANDTOTAL'] == NULL ? $merge['SUBTOTAL'] : $merge['GRANDTOTAL']);

        // get total amount in words
        $merge['TOTAL_IN_WORDS'] = ltrim(Transaction::convertRinggit($GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= Transaction::convertCents(sprintf("%.2f", $GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= ' ONLY';
        $merge['TOTAL_IN_WORDS'] = preg_replace('/\s+/', ' ', $merge['TOTAL_IN_WORDS']);

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $footer = preg_replace('/\s+/', ' ', preg_split("/[\r]+/", $merge['FOOTER']));
        $mt_FOOTER_LINE = "";
        foreach ($footer as $foot) {
            $mt_FOOTER_LINE .= $foot . "<br>";
        }

        // create json file and place it into the datasource
        foreach ($ResourcesJsonList->data as $item) {
            $objectJSON = [];
            $objectJSON['item_sn'] = $item->s_n;
            $objectJSON['item_code'] = $item->item_code;
            $objectJSON['subject'] = $item->description;
            $objectJSON['details'] = $item->_DETAIL;
            $objectJSON['unit_measure'] = $item->uom;
            $objectJSON['unit_cost'] = floatval($item->unit_cost);
            $objectJSON['quantity'] = floatval($item->quantity);
            $objectJSON['taxed_amount'] = ($item->tax_amount == null || $item->tax_amount == 0 ?
                floatval($item->amount) : floatval($item->tax_amount));
            $objectJSON['mt__HEADER'] = $merge['HEADER'];
            $objectJSON['mt__SUMMARY'] = $merge['SUMMARY'];
            $objectJSON['mt__FOOTER_LINE1'] = $mt_FOOTER_LINE;
            $dataArray[] = collect($objectJSON);
        }

        if (empty($dataArray)) {
            $data =  '{"data" :[{}]}';
        } else {
            $data =  '{"data" :' . json_encode($dataArray) . '}';
        }

        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);
        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'CURRENCY' => 'RINGGIT MALAYSIA',
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_TOTAL_IN_WORDS' => $merge['TOTAL_IN_WORDS'],
                'mt_GRANDTOTAL' => $GRANDTOTAL
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();

        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        unlink($data_file);
        flush();
        exit;
    }

    public static function generateBillNote($formattedResource, $CTERM, $source, $jrxml, $reportTitle, $ResourcesJsonList, $type)
    {
        $dataArray = [];
        $dataArray2 = [];
        $merge = $formattedResource->toArray();

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $mt_ADDR =  $merge['ADDR1'];
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR2']) == 'null' ? "" : $merge['ADDR2']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR3']) == 'null' ? "" : $merge['ADDR3']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR4']) == 'null' ? "" : $merge['ADDR4']);
        $mt_ACC_DETAIL = $mt_ADDR == null ? null : $mt_ADDR . "<br>";
        $mt_ACC_DETAIL .= $merge['TEL'] == null ? null : "Tel : " . $merge['TEL'] . ",<ensp>";
        $mt_ACC_DETAIL .= $merge['FAX'] == null ? null : "Fax : " . $merge['FAX'] . "<ensp>";

        $countSling = 0;
        // create json file and place it into the datasource (Cylinder)
        foreach ($ResourcesJsonList->data as $item) {
            $objectJSON = [];
            $objectJSON['item_code'] = $item->s_n;
            if($type == "cyl"){
                $objectJSON['subject'] = $item->description;
                $objectJSON['details'] = $item->details;
                $objectJSON['remark'] = $item->remark;
            }else{
                $objectJSON['serial_no'] = $item->serial_no;
                $objectJSON['rack_type'] = $item->rack_type;
                $objectJSON['sling_no'] = $item->sling_no;
                $objectJSON['gwl'] = $item->gwl;
                $objectJSON['inspDate'] = $item->inspDate;
                $objectJSON['testDate'] = $item->testDate;
                $countSling = $objectJSON['sling_no'] != null || $objectJSON['sling_no'] != '' ? $countSling + 1 : $countSling + 0;
                $objectJSON['totalSling'] = $countSling;
            }

            $objectJSON['quantity'] = floatval($item->quantity);
            $dataArray[] = collect($objectJSON);
        }

        $dataArray_merge = $dataArray; //array_merge($dataArray, $dataArray2);
        if (empty($dataArray_merge)) {
            $data =  '{"data" :[{}]}';
        } else {
            $data =  '{"data" :' . json_encode($dataArray_merge) . '}';
        }

        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);
        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                'mt_ACC_HOLDER' =>  $merge['ACC_HOLDER'],
                'LPO_NO' =>  $merge['LPO_NO'],
                'START_FROM' =>  date("d/m/Y", strtotime($merge['START_FROM'])),
                'DRIVER' =>  $merge['DRIVER'],
                'mt_ACC_DETAIL' =>  $mt_ACC_DETAIL,
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'mt_ACC_CODE' => $merge['ACC_CODE'],
                'charge_type' => $merge['CH_TYPE'],
                'mt_HEADER'=> $merge['HEADER'],
                'mt_SUMMARY'=> $merge['SUMMARY'],
                'mt_FOOTER_LINE1' => $merge['FOOTER']
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();

        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        unlink($data_file);
        flush();
        exit;
    }

    public static function generateBillReturnNote($formattedResource, $CTERM, $source, $jrxml, $reportTitle, $ResourcesJsonList, $type)
    {
        $dataArray = [];
        $merge = $formattedResource->toArray();

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $mt_ADDR =  $merge['ADDR1'];
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR2']) == 'null' ? "" : $merge['ADDR2']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR3']) == 'null' ? "" : $merge['ADDR3']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR4']) == 'null' ? "" : $merge['ADDR4']);
        $mt_ACC_DETAIL = $mt_ADDR == null ? null : $mt_ADDR . "<br>";
        $mt_ACC_DETAIL .= $merge['TEL'] == null ? null : "Tel : " . $merge['TEL'] . ",<ensp>";
        $mt_ACC_DETAIL .= $merge['FAX'] == null ? null : "Fax : " . $merge['FAX'] . "<ensp>";

        // create json file and place it into the datasource (Cylinder)
        foreach ($ResourcesJsonList->data as $item) {
            $objectJSON = [];
            $objectJSON['item_code'] = $item->s_n;
                $objectJSON['subject'] = $item->subject;
                $objectJSON['details'] = $item->details;
                $objectJSON['type'] = $item->type;
                $objectJSON['total'] = $item->quantity;

            $objectJSON['quantity'] = floatval($item->quantity);
            $dataArray[] = collect($objectJSON);
        }

        $dataArray_merge = $dataArray;
        if (empty($dataArray_merge)) {
            $data =  '{"data" :[{}]}';
        } else {
            $data =  '{"data" :' . json_encode($dataArray_merge) . '}';
        }

        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);
        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                'mt_ACC_HOLDER' =>  $merge['ACC_HOLDER'],
                'LPO_NO' =>  $merge['LPO_NO'],
                'START_FROM' =>  date("d/m/Y", strtotime($merge['START_FROM'])),
                'DRIVER' =>  $merge['DRIVER'],
                'mt_ACC_DETAIL' =>  $mt_ACC_DETAIL,
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'mt_ACC_CODE' => $merge['ACC_CODE'],
                'charge_type' => $merge['CH_TYPE'],
                'mt_HEADER'=> $merge['HEADER'],
                'mt_SUMMARY'=> $merge['SUMMARY'],
                'mt_FOOTER_LINE1' => $merge['FOOTER']
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();

        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        unlink($data_file);
        flush();
        exit;
    }

    public static function generateReportRefill($formattedResource, $source, $jrxml, $reportTitle)
    {
        $merge = $formattedResource->toArray();

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                'Technician' => $merge['tech'],
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'Barcode' => $merge['barcode'],
                'Serial_No' => $merge['serial'],
                'CH_IN' => $merge['charge_in'],
                'CH_OUT' => $merge['charge_out']
            ],
            'locale' => 'en'
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();

        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        flush();
        exit;
    }

    public static function generateReportRepair($formattedResource, $source, $jrxml, $reportTitle)
    {
        $dataArray = [];

        $merge = $formattedResource->toArray();

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $objectJSON['FAULT'] = $merge['fault'] == null ? "-" : $merge['fault'];
        $dataArray[] = collect($objectJSON);

        $data =  '{"data" :' . json_encode($dataArray) . '}';
        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);

        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;
        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                'Technician' => $merge['tech'],
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'Barcode' => $merge['barcode'],
                'Serial_No' => $merge['serial'],
                'COMPLETE_DATE' =>  date("d/m/Y", strtotime($merge['completedate'])),
                'CH_IN' => $merge['charge_in'],
                'CH_OUT' => $merge['charge_out']
            ],
            'locale' => 'en',

            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();

        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        unlink($data_file);
        flush();
        exit;
    }

    public static function generateBillCylinderInvoice($formattedResource, $CTERM, $source, $jrxml, $reportTitle, $ResourcesJsonList)
    {
        $dataArray = [];
        $merge = $formattedResource->toArray();

        $GRANDTOTAL = ($merge['GRANDTOTAL'] == 0 || $merge['GRANDTOTAL'] == NULL ? $merge['SUBTOTAL'] : $merge['GRANDTOTAL']);

        // get total amount in words
        $merge['TOTAL_IN_WORDS'] = ltrim(Transaction::convertRinggit($GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= Transaction::convertCents(sprintf("%.2f", $GRANDTOTAL));
        $merge['TOTAL_IN_WORDS'] .= ' ONLY';
        $merge['TOTAL_IN_WORDS'] = preg_replace('/\s+/', ' ', $merge['TOTAL_IN_WORDS']);

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $mt_ADDR =  $merge['ADDR1'];
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR2']) == 'null' ? "" : $merge['ADDR2']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR3']) == 'null' ? "" : $merge['ADDR3']);
        $mt_ADDR .= (str_replace(' ', '', $merge['ADDR4']) == 'null' ? "" : $merge['ADDR4']);
        $mt_ACC_DETAIL = $mt_ADDR == null ? null : $mt_ADDR . "<br>";
        $mt_ACC_DETAIL .= $merge['TEL'] == null ? null : "Tel : " . $merge['TEL'] . ",<ensp>";
        $mt_ACC_DETAIL .= $merge['FAX'] == null ? null : "Fax : " . $merge['FAX'] . "<ensp>";

        // create json file and place it into the datasource
        foreach ($ResourcesJsonList->data as $item) {
            $objectJSON = [];
            $objectJSON['item_code'] = $item->s_n;
            $objectJSON['subject'] = $item->description;
            $objectJSON['details'] = $item->_DETAIL;
            $objectJSON['product'] = $item->product;
            $objectJSON['unit_price'] = floatval($item->unit_price);
            $objectJSON['barcode'] = $item->barcode;
            $objectJSON['serial'] = $item->serial;
            $objectJSON['taxed_amount'] = ($item->tax_amount == null || $item->tax_amount == 0 ?
                floatval($item->amount) : floatval($item->tax_amount));
            $objectJSON['mt__HEADER'] = $merge['HEADER'];
            $objectJSON['mt__SUMMARY'] = $merge['SUMMARY'];
            $dataArray[] = collect($objectJSON);
        }

        if (empty($dataArray)) {
            $data =  '{"data" :[{}]}';
        } else {
            $data =  '{"data" :' . json_encode($dataArray) . '}';
        }
        $footer = preg_replace('/\s+/', ' ', preg_split("/[\r]+/", $merge['FOOTER']));
        $mt_FOOTER_LINE = "";
        foreach ($footer as $foot) {
            $mt_FOOTER_LINE .= $foot . "<br>";
        }

        dd("test");

        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);
        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'CURRENCY' => 'RINGGIT MALAYSIA',
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
                // 'mt_COYADDR1' => $mt_COYADDR,
                // 'mt_COYTEL' =>  $mt_TEL,
                // 'mt_COYFAX' =>  $mt_FAX,
                // 'mt_COYEMAIL' =>  $mt_EMAIL,
                'mt_COYNO' => config('config.company.company_no'),
                'mt_ACC_HOLDER' =>  $merge['ACC_HOLDER'],
                'REF_NO' =>  $merge['REF_NO'],
                'mt_ACC_DETAIL' =>  $mt_ACC_DETAIL,
                'mt_CTERM' =>  $CTERM,
                // 'mt_ADDR1' => $mt_ADDR,
                // 'mt_TEL' => $merge['TEL'],
                // 'mt_FAX' => $merge['FAX'],
                'mt_DOC_NO' => $merge['DOC_NO'],
                'mt_DOC_DATE' => date("d/m/Y", strtotime($merge['DOC_DATE'])),
                'mt_ACC_CODE' => $merge['ACC_CODE'],
                'mt_TOTAL_IN_WORDS' => $merge['TOTAL_IN_WORDS'],
                'mt_GRANDTOTAL' => $GRANDTOTAL,
                'mt_FOOTER_LINE1' =>  $mt_FOOTER_LINE
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        // $PHPJasper = new PHPJasper();
        // $PHPJasper->process($input, $output, $options)->execute();

        // // open in web browser
        // header('Content-Description: application/pdf');
        // header('Content-Type: application/pdf');
        // header('Content-Disposition:; filename=' . $output . 'pdf');
        // readfile($output . '.pdf');
        // unlink($output . '.pdf');
        // unlink($data_file);
        // flush();
        // exit;
    }

    public static function generateBillReprint($cashbills, $source,  $jrxml)
    {
        $dataArray = array();
        $s_n = 1;

        switch ($source) {
            case 'CashBill':
                $reportTitle = "Cash Bill";
                break;
            case 'Invoice':
                $reportTitle = "Invoice";
                break;
        }

        foreach ($cashbills as $trans) {
            $objectJSON = [];

            $mt_ADDR =  $trans['ADDR1'];
            $mt_ADDR .= (str_replace(' ', '', $trans['ADDR2']) == 'null' ? "" : $trans['ADDR2']);
            $mt_ADDR .= (str_replace(' ', '', $trans['ADDR3']) == 'null' ? "" : $trans['ADDR3']);
            $mt_ADDR .= (str_replace(' ', '', $trans['ADDR4']) == 'null' ? "" : $trans['ADDR4']);
            $mt_ACC_DETAIL = $mt_ADDR == null ? null : $mt_ADDR . "<br>";
            $mt_ACC_DETAIL .= $trans['TEL'] == null ? null : "Tel : " . $trans['TEL'] . ",<ensp>";
            $mt_ACC_DETAIL .= $trans['FAX'] == null ? null : "Fax : " . $trans['FAX'] . "<ensp>";

            $objectJSON['item_sn'] = $s_n;
            $objectJSON['mt__DOC_NO'] = $trans->DOC_NO;
            $objectJSON['mt__ACC_HOLDER'] = $trans->ACC_HOLDER;
            $objectJSON['mt__ACC_DETAIL'] = $mt_ACC_DETAIL;
            $objectJSON['mt__ACC_CODE'] = $trans->ACC_CODE;
            $objectJSON['mt__DOC_DATE'] = date("d/m/Y", strtotime($trans->DOC_DATE));

            //added due date, c_term and c_limit for inv here
            // $objectJSON['mt__INV_DUE_DATE'] = $source == 'Invoice' ? date("d/m/Y", strtotime($trans->INV_Due_Date)) : null;
            $objectJSON['mt_CTERM'] = $source == 'Invoice' ? $trans->TERMS : 'Cash';
            $objectJSON['mt_CLIMIT'] = $source == 'Invoice' ? $trans->LIMITS : null;

            $objectJSON['REF_NO'] = $trans->REF_NO;
            $objectJSON['DO_NO'] = $trans->DO_NO;
            $objectJSON['item_code'] = $trans->item_code;
            $objectJSON['subject'] = $trans->subject;
            $objectJSON['details'] = $trans->details;
            $objectJSON['unit_measure'] = $trans->uom;
            $objectJSON['unit_price'] = floatval($trans->uprice);
            $objectJSON['quantity'] = floatval($trans->qty);
            $objectJSON['taxed_amount'] = ($trans->taxed_amounts == null || $trans->taxed_amounts == 0 ?
                floatval($trans->amounts) : floatval($trans->taxed_amounts));



            $objectJSON['mt_GRANDTOTAL']  =  ($trans->GRANDTOTAL == 0 || $trans->GRANDTOTAL == NULL ?
                $trans->STOTAL : $trans->GRANDTOTAL);



            // get total amount in words
            $objectJSON['mt__TOTAL_IN_WORDS'] = ltrim(Transaction::convertRinggit($objectJSON['mt_GRANDTOTAL']));
            $objectJSON['mt__TOTAL_IN_WORDS'] .= Transaction::convertCents(sprintf("%.2f", $objectJSON['mt_GRANDTOTAL']));
            $objectJSON['mt__TOTAL_IN_WORDS'] .= ' ONLY';
            $objectJSON['mt__TOTAL_IN_WORDS'] = preg_replace('/\s+/', ' ', $objectJSON['mt__TOTAL_IN_WORDS']);

            $objectJSON['mt__HEADER'] = $trans['HEADER'];
            $objectJSON['mt__SUMMARY'] = $trans['SUMMARY'];
            $objectJSON['mt__FOOTER_LINE1'] = $trans['FOOTER'];

            $s_n = $s_n + 1;
            $dataArray[] = collect($objectJSON);
        }

        $data = '{"data" :' . json_encode($dataArray) . '}';
        // dd($data);

        $mt_COYADDR = Company::getAddress();
        $mt_TEL = Company::getTel();
        $mt_FAX = Company::getFax();
        $mt_EMAIL = Company::getEmail();
        $mt_COYDETAIL = config('config.company.company_no');
        $mt_COYDETAIL .= $mt_COYADDR == null ? null : "<br>" . $mt_COYADDR . "<br>";
        $mt_COYDETAIL .= $mt_EMAIL == null ? null : "E-Mail : " . $mt_EMAIL . "<br>";
        $mt_COYDETAIL .= $mt_TEL == null ? null : "Tel : " . $mt_TEL . ",   ";
        $mt_COYDETAIL .= $mt_FAX == null ? null : "Fax : " . $mt_FAX . "  ";

        $JsonFileName = $source . date("Ymdhisa") . Auth::user()->id;
        file_put_contents(base_path('resources/reporting/GeneralBill/' . $JsonFileName . '.json'), $data);
        $JasperFileName = $source . date("Ymdhisa") . Auth::user()->id;

        $input = base_path() . '/resources/reporting/GeneralBill/' . $jrxml;
        $output = base_path() . '/resources/reporting/GeneralBill/' . $JasperFileName;
        $data_file = base_path() . '/resources/reporting/GeneralBill/' . $JsonFileName . '.json';
        $options = [
            'format' => ['pdf'],
            'params' => [
                'REPORT_TITLE' => $reportTitle,
                'CURRENCY' => 'RINGGIT MALAYSIA',
                'mt_COYNAME' => config('config.company.name'),
                'mt_COYDETAIL' => $mt_COYDETAIL,
            ],
            'locale' => 'en',
            'db_connection' => [
                'driver' => 'json',
                'data_file' => $data_file,
                'json_query' => 'data'
            ]
        ];

        $PHPJasper = new PHPJasper();
        $PHPJasper->process($input, $output, $options)->execute();
        // dd($PHPJasper);
        // open in web browser
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename=' . $output . 'pdf');
        readfile($output . '.pdf');
        unlink($output . '.pdf');
        unlink($data_file);
        flush();
        exit;
    }

    public static function convertHelper($amount)
    {
        $TOTAL_IN_WORDS = ltrim(Transaction::convertRinggit($amount));
        $TOTAL_IN_WORDS .= Transaction::convertCents(sprintf("%.2f", $amount));
        $TOTAL_IN_WORDS .= ' ONLY';
        $TOTAL_IN_WORDS = preg_replace('/\s+/', ' ', $TOTAL_IN_WORDS);

        return $TOTAL_IN_WORDS;
    }

    public static function convertRinggit($number)
    {
        // 0 to 999 999 999 999
        $snumber = (string)$number;
        // pad with "0"
        // String mask = "000000000000";
        $sprintfFormat = "%012d";
        $snumber = sprintf($sprintfFormat, $number);
        if ($snumber == '0') {
            return "";
        }
        // XXXnnnnnnnnn
        $billions = (int)substr($snumber, 0, 3);
        // nnnXXXnnnnnn
        $millions  = (int)substr($snumber, 3, 3);
        // nnnnnnXXXnnn
        $hundredThousands = (int)substr($snumber, 6, 3);
        // nnnnnnnnnXXX
        $thousands = (int)substr($snumber, 9, 3);
        $tradBillions = '';
        switch ($billions) {
            case 0:
                $tradBillions = "";
                break;
            default:
                $tradBillions = Transaction::convertLessThanOneThousand($billions) . " BILLION ";
        }
        $result =  $tradBillions;
        $tradMillions = '';
        switch ($millions) {
            case 0:
                $tradMillions = "";
                break;
            default:
                $tradMillions = Transaction::convertLessThanOneThousand($millions) . " MILLION ";
        }
        $result .=  $tradMillions;
        $tradHundredThousands = '';
        switch ($hundredThousands) {
            case 0:
                $tradHundredThousands = "";
                break;
                // case 1 : // originally because 1000, 1 is not detected somehow
                //   $tradHundredThousands = Transaction::convertLessThanOneThousand($hundredThousands) . " ONE THOUSAND ";
                //   break;
            default:
                $tradHundredThousands = Transaction::convertLessThanOneThousand($hundredThousands) . " THOUSAND ";
        }
        $result .=  $tradHundredThousands;
        $tradThousand = '';
        $tradThousand = Transaction::convertLessThanOneThousand($thousands);
        $result .= $tradThousand;
        // remove extra spaces!
        return $result;
    }

    public static function convertLessThanOneThousand($number, $quantifier = ' HUNDRED')
    {
        $soFar = '';
        $original_number = $number;
        if ($number % 100 < 20) {
            $soFar = Transaction::$numNames[$number % 100];
            $number /= 100;
        } else {
            $soFar = Transaction::$numNames[$number % 10];
            $number /= 10;
            $soFar = Transaction::$tensNames[$number % 10] . $soFar;
            $number /= 10;
        }
        if ($number == 0) return $soFar;
        if (strlen($original_number) < 3)
            return Transaction::$numNames[$number] . $soFar;
        return Transaction::$numNames[$number] . $quantifier . $soFar;
    }

    public static function convertCents($number)
    {
        list($whole, $decimal) = explode('.', $number);
        $snumber = $whole;
        $cents = (int)substr($decimal, 0);
        if ($cents === 0) return;
        if ((int)$snumber != 0)
            return " AND " . Transaction::convertLessThanOneThousand($cents, '') . " CENTS";
        else
            return Transaction::convertLessThanOneThousand($cents, '') . " CENTS";
    }
}
