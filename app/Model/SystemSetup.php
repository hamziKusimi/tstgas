<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemSetup extends Model
{
    protected $fillable = [
        'code',
        'name',
        'company_no',
        'address1',
        'address2',
        'address3',
        'address4',
        'tel',
        'fax',
        'email',
        'name1',
        'name2',
        'servtaxno',
        'licenseno',
        'salestaxno',
        'use_tax',
        'uom1',
        'uom2',
        'uom3',
        'updoc_gasrack',
        'free_cy',
        'created_by',
        'updated_by',
        'custom1',
        'custom2',
        'custom3',
        'custom4',
        'custom5',
        'custom1_type',
        'custom2_type',
        'custom3_type',
        'custom4_type',
        'custom5_type',
        'price1',
        'price2',
        'price3',
        'price4',
        'price5',
        'price5',
        'warning_min',
        'req_pass',
        'password_min',
        'task_scheduler',
        'bank_detail',
        'bank_account_no',
        'qty_decimal',
        'qty_decimal_place',
        'rate_decimal',
        'rate_decimal_place',
        'stock_item',
        'db_server',
        'db_name',
        'dr_cashpurchase_acc',
        'dr_creditpurchase_acc',
        'cr_purchasereturn_acc',
        'dr_cashsales_acc',
        'cr_cashsales_acc',
        'dr_cashsales_return_acc',
        'cr_invoicesales_acc',
        'dr_creditsales_return_acc',
        'set_category1',
        'set_category2',
        'set_category3',
        'set_category4',
        'set_category5',
        'set_category6',
        'set_category7',
        'set_category8',
        'direct_posting'
    ];
    protected $appends = [
        // 'name',
    ];

    public function getAddressAttribute()
    {
        return $this->address1 . "\n" . $this->address2 . "\n" . $this->address3 . "\n" . $this->address4;
    }

    // public function getNameAttribute()
    // {
    //     return $this->name;
    // }
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
