<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proformainvoicedts extends Model
{
    use SoftDeletes;
    protected $fillable = [

        'doc_no',
        'sequence_no',
        'account_code',
        'serial',
        'barcode',
        'product',
        'type',
        'subject',
        'details',
        'quantity',
        'uom',
        'rate',
        'reference_no',
        'unit_price',
        'discount',
        'amount',
        'totalqty',
        'tax_code',
        'tax_rate',
        'tax_amount',
        'taxed_amount',
        'created_by',
        'updated_by',
        'exchange_rate',
        'deleted_by'
        
    ];
    
    public function stockcode()
    {
        return $this->belongsTo('App\Model\Stockcode', 'stockcode_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
