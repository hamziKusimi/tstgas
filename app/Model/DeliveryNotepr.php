<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryNotepr extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'dn_no',
        'sequence_no',
        'sqn_no',
        'datetime',
        'barcode',
        'serial',
        'driver',
        'type',
        'return_note',
        'return_note_date',
        'updated_by',
        'created_by',
        'deleted_by'   
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
