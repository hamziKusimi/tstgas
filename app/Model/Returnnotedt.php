<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\View\ReturnnoteMaster;

class ReturnNotedt extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'dn_no', 
        'sequence_no',
        'account_code',
        'item_code',
        'subject',
        'category',
        'product',
        'capacity',
        'pressure',
        'qty',
        'price',
        'updated_by',
        'created_by',
        'deleted_by' 
    ];
    
    public function getCylindersAttribute()
    {
        return ReturnnoteMaster::where('dn_no', $this->dn_no)
            ->where('dt_sqn', $this->sequence_no)
            ->get();
    }

    public function stockcode()
    {
        return $this->belongsTo('App\Model\Stockcode', 'stockcode_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
