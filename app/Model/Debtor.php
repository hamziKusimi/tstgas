<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Debtor extends Model
{
   use SoftDeletes;
   protected $fillable =[
        'accountcode',
        'D_DC',
        'name',
        'type',
        'D_CCODE',
        'def_price',
        'salesman',
        'address1',
        'address2',
        'address3',
        'address4',
        'ptype',
        'hp',
        'tel',
        'fax',
        'cperson',
        'email',
        'cptel',
        'cterm',
        'climit',
        'ccurrency',
        'gstno',
        'brnno',
        'yopen',
        'memo',
        'custom1', 
        'custom2', 
        'custom3', 
        'custom4', 
        'custom5', 
        'customval1', 
        'customval2', 
        'customval3', 
        'customval4', 
        'customval5', 
        'created_by',
        'updated_by',
        'deleted_by',
        'active'
   ];

   protected $appends = [
       'address',
       'detail'
   ];

   public function cashbills()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Cashbill', 'debtor_id');
    }

    public static function findByAcode($acode)
    {
        return Debtor::where('accountcode', $acode)->first();
    }
    
    public function getAddressAttribute()
    {
        return "$this->address1 \n$this->address2 \n$this->address3 \n$this->address4";
    }

    public static function generateAcode($name)
    {
        // getting the initial to fill the second character in the account code
        $initial = !empty($name) ? $name[0] : 'A';

        // get the latest debtor and add 1 to the count (to get the next number)
        $latestDebtor = Debtor::orderBy('id', 'desc')->first();
        $sprintfFormat = "%03d";
        $runningNumber = sprintf($sprintfFormat, isset($latestDebtor) ? $latestDebtor->id + 1 : 1);

        // concatenate whole string; the cost centre code will be applied separately (into D_CCODE)
        return 'D'.$initial.$runningNumber;
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    public function getDetailAttribute()
    {
        return $this->accountcode.' '.$this->name;
    }
}
