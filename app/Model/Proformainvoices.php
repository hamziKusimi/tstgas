<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proformainvoices extends Model
{
    use SoftDeletes;
    protected $fillable = [
        
        'docno',
        'date',
        'reference_no',
        'm_duedate',
        'refinvdate',
        'refno',
        'do_no',
        'discount',
        'amount',
        'tax_amount',
        'taxed_amount',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'fax_no',
        'credit_term',
        'credit_limit',
        'header',
        'footer',
        'summary',
        'created_by',
        'updated_by',
        'currency',
        'exchange_rate',
        'rounding',
        'deleted_by',
        'printed', 
        'printed_by', 
        'printed_at'
    ];
       
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
    
    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }
}
