<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ViewTransaction extends Model
{
    protected $table = 'view_transactions';
}
