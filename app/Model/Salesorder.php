<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Deliveryorderdt;

class Salesorder extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'docno',
        'date',
        'lpono',
        'ref1',
        'cbinvno',
        'quotno',
        'discount',
        'amount',
        'tax_amount',
        'taxed_amount',
        'account_code',
        'name',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'tel_no',
        'fax_no',
        'header',
        'footer',
        'summary',
        'created_by',
        'updated_by',
        'deleted_by',
        'printed',
        'printed_by',
        'printed_at',
    ];
    protected $appends = [
        'editRoute',
        'debtorAccountCode',
    ];

    public function debtor()
    {
        return $this->belongsTo('App\Model\Debtor', 'debtor_id');
    }

    public function getEditRouteAttribute()
    {
        return route('salesorders.edit', isset($this->id) ? $this->id : 'id');
    }

    // since there's no primary keys, connect by using attribute rather than using relations
    public function getDataAttribute()
    {
        return Gooddt::where('id', $this->id)
            ->orderBy('sequence_no')
            ->get();
    }

    public function getDebtorAccountCodeAttribute()
    {
        return $this->accountcode;
    }

    public function getGoodDateAttribute() // : date
    {
        return Carbon::parse($this->date);
    }

    public function getDueDateAttribute() // : date
    {
        return Carbon::parse($this->date)->addDays($this->termsInDays);
    }

    public function getTermsInDaysAttribute() // : int
    {
        if ($this->credit_term) {
            return (int) str_replace('DAYS', '', $this->credit_term);
        }

        return 0;
    }

    public function getAddressAttribute()
    {
        return $this->addr1."\n".$this->addr2."\n".$this->addr3."\n".$this->addr4;
    }

    public function getSubTotalAttribute()
    {
        $sum = 0;
        foreach ($this->data as $data)
            $sum += $data->taxed_amount;

        return $sum;
    }

    // public function getTaxAmountAttribute()
    // {
    //     $tax = 0;
    //     foreach ($this->data as $data)
    //         $tax += $data->tax_amount;

    //     return $tax;
    // }

    public function getGrandTotalAttribute()
    {
        return $this->taxed_amount;
    }

    public static function createRecord($request)
    {
        return response()->json($request);

        return response()->json($request['doc_no']);

        return $deliveryorder;
    }

    public static function deleteRecord($id)
    {
        $resource = Cashbill::find($id);

        if (isset($resource)) {
            // delete dt table items
            if (isset($resource->data)) {
                foreach ($resource->data as $dt) {
                    $gl = GeneralLedger::findByAcode($dt->account_code);
                    if (isset($gl)) {
                        $opening = $gl->M_YROPEN;
                        $gl->update(['M_YROPEN' => $opening - $dt->taxed_amount]);
                    }
                    $dt->delete();
                }
            }

            // update the year open
            $debtor = Debtor::findByAcode($resource->account_code);
            $debtor->update([ 'D_YOPEN' => $debtor->D_YOPEN - $resource->taxed_amount ]);

            $resource->delete();
        }
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

}
