<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code',
        'descr',
        'active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $appends = [
        'itemMasterCode'
    ];
    
    public function getItemMastercodeAttribute()
    {
        return $this->code . ' ' . $this->descr;
    }
    
    public function stockcodes()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Stockcode', 'loc_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    public function isUsedInStockcode()
    {
        $stockcode = Stockcode::where('loc_id', $this->id)->count();

        if ($stockcode > 0) {
            return true;
        }else{
            return false;
        }
    }
}
