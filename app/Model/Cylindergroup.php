<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cylindergroup extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'code',
        'descr',
        'active',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function cylinders()
    {
        //userid refer to foreign key
        return $this->hasMany('App\Model\Cylinder', 'group_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }
}
