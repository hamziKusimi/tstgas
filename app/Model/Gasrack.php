<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Model\Gasracktype;
use App\Model\Gasrackcylinder;
use DB;

class Gasrack extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'barcode',
        'serial',
        'mfr',
        'mfgterm',
        'owner',
        'type',
        'maxgmass',
        'taremass',
        'payload',
        'certno',
        'testdate',
        'sg_barcode',
        'sg_serial',
        'created_by',
        'updated_by',
        'deleted_by',
        'status',
        'reasons',
        'attachment',
        'description'
    ];
    
    protected $appends = [
        'gstype',
        'gscylinder'
        // 'dnno'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'userid');
    }

    public function getGstypeAttribute()
    {
        return Gasracktype::find($this->type);
    }  
    
    public function getGscylinderAttribute()
    {
        return Gasrackcylinder::where('gr_id', $this->id)
        ->whereNull('deleted_at')
        ->get();
    }

    // public function getDnnoAttribute()
    // { 
    //     return DB::select(DB::raw("
                
    //     SELECT 
    //         gasracks.serial AS serial,
    //         gasracks.barcode AS barcode,
    //         gasracks.cat_id AS cat_id,
    //         gasracks.prod_id AS prod_id,
    //         gasracks.capacity AS capacity,
    //         gasracks.descr AS descr,
    //         gasracks.testpressure AS testpressure,
    //         delivery_notes.dn_no AS dnno,
    //         delivery_notes.date AS dnno_date,
    //         delivery_notes.account_code AS account_code,
    //         delivery_noteprs.dn_no AS pr_dnno,
    //         delivery_noteprs.barcode AS pr_barcode,
    //         delivery_noteprs.serial AS pr_serial
    //         FROM gasracks 
    //         INNER JOIN delivery_noteprs ON delivery_noteprs.serial = gasracks.serial
    //         INNER JOIN delivery_notes ON delivery_notes.dn_no = delivery_noteprs.dn_no

    //         where delivery_noteprs.return_note is null and delivery_notes.account_code = :accountcode and gasracks.barcode = :item
    //     "), [
    //             'accountcode' => $request['acc_code'],
    //             'item' => $this->barcode
    //     ]);

    // }
}
