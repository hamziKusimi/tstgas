<?php
namespace App\ExportsSupplier;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportSupplier implements FromView, WithEvents
{
    public $date;
    public $title;
    public $item;
    public $data;

    public function __construct($data,$date)
    {
        $this->date = $date;
        $this->title = $data['page_title'];
        $this->item = $data['item'];
        $this->data = $data['inv'];
    }

    public function view(): View
    {
        return view('reportprinting.inventory.kpdnhep.supplier2',
        [ 'title'=>$this->title,
        'item'=>$this->item,
        'data'=>$this->data,
        'date'=>$this->date]);
    }

    public function registerEvents(): array
    {


        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '#000000'],
                        ],
                    ],
                ];

                $event->sheet->getDelegate()->mergeCells('A2:E2');
                $event->sheet->getDelegate()->mergeCells('A5:G5');
                $event->sheet->getStyle('A2:E2')->ApplyFromArray($styleArray);
                $event->sheet->getStyle('A5:G5')->ApplyFromArray($styleArray);
                $event->sheet->formatColumn('D','0.00');
            },
        ];
    }

}