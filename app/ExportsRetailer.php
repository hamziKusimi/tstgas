<?php
namespace App\ExportsRetailer;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportRetailer implements FromView, WithEvents
{
    public $date;
    public $title;
    public $item;
    public $data;
    public $checkRow;
    public $supplierDetail;

    public function __construct($data)
    {

        $this->date =  $data['r_date'];
        $this->title = $data['page_title'];
        $this->item = $data['item'];
        $this->data = $data['inv'];
        $this->checkRow = $data['checkRow'];
        $this->supplierDetail = $data['supplierDetail'];
    }

    public function view(): View
    {
        return view('reportprinting.inventory.kpdnhep.retailer2',
        [ 'title'=>$this->title,
        'item'=>$this->item,
        'data'=>$this->data,
        'checkRow'=>$this->checkRow,
        'supplierDetail'=>$this->supplierDetail,
        'date'=>$this->date]);
    }

    public function registerEvents(): array
    {


        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '#000000'],
                        ],
                    ],
                ];

                $event->sheet->getDelegate()->mergeCells('B3:G3');
                $event->sheet->getStyle('B3:G3')->ApplyFromArray($styleArray);
                $event->sheet->formatColumn('D','0.00');
            },
        ];
    }

}