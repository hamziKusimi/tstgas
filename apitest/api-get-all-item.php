<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   $lastsyncdate = file_get_contents("php://input");
    if (isset($lastsyncdate)) {
        $date = json_decode($lastsyncdate);
    } else {
        exit;
    }

   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo = new PDO($dsn, $un, $pwd, $opt);
   
   $itemDB = array();

   // Attempt to query database table and retrieve data
   try {
      $stmt = $pdo->query("SELECT id,
                                 code as itmcode, 
                                 descr as itmdescr, 
                                 price1 as itmprice1,
                                 price2 as itmprice2,
                                 price3 as itmprice3,
                                 price4 as itmprice4,
                                 price5 as itmprice5,
                                 price6 as itmprice6,
                                 updated_at,
                                 created_at
                                 FROM stockcodes 
                                 WHERE inactive = 0 and
                                        type = 'Stock Item'
                                 ORDER BY descr ASC" );
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
        $row->itmprice = number_format($row->itmprice3, 2);

        $row->itmtotprice = number_format($row->itmtotprice, 2);

        $row->itmqty = 0;

        if(empty($date)){
            $itemDB['add'][] = $row;
        }
        else if(!empty($date)){
          if($date < $row->created_at){
            $itemDB['add'][] = $row;
          }
          else if($date < $row->updated_at){
            $itemDB['update'][] = $row;
          }
        } 
      }

      $stmt = $pdo->query("SELECT code as itmcode, 
                                 descr as itmdescr, 
                                 price1 as itmprice1,
                                 price2 as itmprice2,
                                 price3 as itmprice3,
                                 price4 as itmprice4,
                                 price5 as itmprice5,
                                 price6 as itmprice6,
                                 updated_at,
                                 created_at
                                 FROM stockcodes 
                                 WHERE inactive = 0 and
                                        type = 'Service' and 
                                       LOCATE('TRANSPORT', descr)
                                 ORDER BY descr ASC" );
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
        $row->itmprice = number_format($row->itmprice3, 2);

        $row->itmtotprice = number_format($row->itmtotprice, 2);

        $row->itmqty = 0;

        if(empty($date)){
            $itemDB['add'][] = $row;
        }
        else if(!empty($date)){
          if($date < $row->created_at){
            $itemDB['add'][] = $row;
          }
          else if($date < $row->updated_at){
            $itemDB['update'][] = $row;
          }
        } 
      }

      $stmt = $pdo->query("SELECT code as itmcode, 
                                 descr as itmdescr, 
                                 price1 as itmprice1,
                                 price2 as itmprice2,
                                 price3 as itmprice3,
                                 price4 as itmprice4,
                                 price5 as itmprice5,
                                 price6 as itmprice6,
                                 updated_at,
                                 created_at
                                 FROM stockcodes 
                                 WHERE inactive = 0 and
                                        type = 'Service' and 
                                       LOCATE('DEPOSIT', descr)
                                 ORDER BY descr ASC" );
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
        $row->itmprice = number_format($row->itmprice3, 2);

        $row->itmtotprice = number_format($row->itmtotprice, 2);

        $row->itmqty = 0;

        if(empty($date)){
            $itemDB['add'][] = $row;
        }
        else if(!empty($date)){
          if($date < $row->created_at){
            $itemDB['add'][] = $row;
          }
          else if($date < $row->updated_at){
            $itemDB['update'][] = $row;
          }
        } 
      }

      // Return data as JSON
      echo json_encode($itemDB);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>