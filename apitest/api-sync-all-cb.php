<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   $cbget = file_get_contents("php://input");
    if (isset($cbget)) {
        $cashbill = json_decode($cbget);
    } else {
        exit;
    }

    $totallines       = count($cashbill);

    $syncedlines      = array();
    $duplicatedlines  = array();

   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo         = new PDO($dsn, $un, $pwd, $opt);

   // Attempt to query database table and retrieve data
   try {
    //delete first then just insert
    for($i = 0; $i < $totallines; $i++){
      //check duplication
      $custDetails = array();

      //replace any quotes
      $cashbill[$i]->custSel->name = str_replace("'", "\'", $cashbill[$i]->custSel->name);

      if($cashbill[$i]->custSel->name == 'CASH SALES'){
        $custDetails[] = $cashbill[$i]->custSel;
      }
      else{
        //get customer details
        $stmt = $pdo->query("SELECT *
                            from debtors where name = '".$cashbill[$i]->custSel->name."'");
        while($row  = $stmt->fetch(PDO::FETCH_OBJ))
        {
           // Assign each row of data to associative array
           $custDetails[] = $row;
        }
        //replace any quotes
        $custDetails[0]->name = str_replace("'", "\'", $custDetails[0]->name);
      }
      
      $stmt = $pdo->query("SELECT *
                        FROM cashbills
                        WHERE docno = '".$cashbill[$i]->docno."' and
                              date  = '".$cashbill[$i]->date."' and
                              name  = '".$custDetails[0]->name."'");

      $duplicated  = $stmt->fetch(PDO::FETCH_OBJ);

      if($duplicated){
        $sqldel = "delete from cashbills 
                          where docno = '".$cashbill[$i]->docno."' and
                                date  = '".$cashbill[$i]->date."' and
                                name  = '".$custDetails[0]->name."'";
        
        $stmtdel = $pdo->query($sqldel);
      }
    }

    for($i = 0; $i < $totallines; $i++){

      $custDetails = array();

      //get customer details
      if($cashbill[$i]->custSel->name == 'CASH SALES'){
        $custDetails[] = $cashbill[$i]->custSel;
      }
      else{
        $stmt = $pdo->query("SELECT *
                            from debtors where name = '".$cashbill[$i]->custSel->name."'");
        while($row  = $stmt->fetch(PDO::FETCH_OBJ))
        {
           // Assign each row of data to associative array
           $custDetails[] = $row;
        }
      }

      $docno          = $cashbill[$i]->docno;
      $date           = $cashbill[$i]->date;

      //replace any quotes
      $cashbill[$i]->rmk = str_replace("'", "\'", $cashbill[$i]->rmk);

      $ref1           = $cashbill[$i]->rmk;
      $amount         = $cashbill[$i]->amount;
      $account_code   = $custDetails[0]->accountcode;

      //replace any quotes
      $custDetails[0]->name = str_replace("'", "\'", $custDetails[0]->name);
      
      $name           = $custDetails[0]->name;

      //replace any quotes
      $custDetails[0]->address1 = str_replace("'", "\'", $custDetails[0]->address1);

      $addr1          = $custDetails[0]->address1;

      //replace any quotes
      $custDetails[0]->address2 = str_replace("'", "\'", $custDetails[0]->address2);

      $addr2          = $custDetails[0]->address2;

      //replace any quotes
      $custDetails[0]->address3 = str_replace("'", "\'", $custDetails[0]->address3);

      $addr3          = $custDetails[0]->address3;

      //replace any quotes
      $custDetails[0]->address4 = str_replace("'", "\'", $custDetails[0]->address4);

      $addr4          = $custDetails[0]->address4;
      
      $tel_no         = $custDetails[0]->tel;
      $fax_no         = $custDetails[0]->fax;
      //$credit_term    = $custDetails[0]->cterm;
      //$rounding       = $cashbill[$i]->rounding;
      $created_by     = $cashbill[$i]->created_by;
      $created_at     = $cashbill[$i]->created_at; 
      $updated_by     = $cashbill[$i]->updated_by;
      $updated_at     = $cashbill[$i]->updated_at;
      
      $stmtgetsalesmancode = $pdo->query("SELECT code FROM salesmen WHERE descr = '".$created_by."'");

      $salesmancode   = $stmtgetsalesmancode->fetch(PDO::FETCH_OBJ);

      $stmt = $pdo->query("INSERT INTO cashbills 
                          (docno, date, ref1, amount, account_code, name, addr1, addr2, addr3, addr4,
                          tel_no,fax_no,created_by,created_at,
                          updated_by,updated_at, salesman) 
                          VALUES ('".$docno."',
                                  '".$date."',
                                  '".$ref1."',
                                  '".$amount."',
                                  '".$account_code."',
                                  '".$name."',
                                  '".$addr1."',
                                  '".$addr2."',
                                  '".$addr3."',
                                  '".$addr4."',
                                  '".$tel_no."',
                                  '".$fax_no."',
                                  '".$created_by."',
                                  '".$created_at."',
                                  '".$updated_by."',
                                  '".$updated_at."',
                                  '".$salesmancode->code."')");

      array_push($syncedlines, $docno);

      $totalitem = count($cashbill[$i]->items);

      for($j = 0; $j < $totalitem; $j++){
        $itemDetails = array();

        //get item details
        $stmt = $pdo->query("SELECT *
                            from stockcodes where code = '".$cashbill[$i]->items[$j]->itmcode."'");
        while($row  = $stmt->fetch(PDO::FETCH_OBJ))
        {
           // Assign each row of data to associative array
           $itemDetails[] = $row;
        }
        
        $stmt = $pdo->query("INSERT INTO cashbilldts
                          (doc_no, sequence_no, account_code, item_code,
                            subject, qty, uom, rate, uprice, amount, totalqty, created_by, updated_by,  created_at,
                            updated_at) 
                          VALUES ('".$docno."',
                                  '".($j + 1)."',
                                  '".$custDetails[0]->accountcode."',
                                  '".$cashbill[$i]->items[$j]->itmcode."',
                                  '".$cashbill[$i]->items[$j]->itmdescr."',
                                  '".$cashbill[$i]->items[$j]->itmqty."',
                                  '".$itemDetails[0]->uom_id1."',
                                  '1',
                                  '".$cashbill[$i]->items[$j]->itmprice."',
                                  '".$cashbill[$i]->items[$j]->itmtotprice."',
                                  '".($cashbill[$i]->items[$j]->itmqty * 1)."',
                                  '".$created_by."',
                                  '".$updated_by."',
                                  '".$created_at."',
                                  '".$updated_at."')");
      }
    }
    echo json_encode($syncedlines);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>