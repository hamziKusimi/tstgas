<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   // Set up the PDO parameters
   $accountcode = $_REQUEST['accountcode'];

   $name  = $_REQUEST['name'];

   $dsn   = "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt   = array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo = new PDO($dsn, $un, $pwd, $opt);
   $data = array();

   // Attempt to query database table and retrieve data
   try {
    if($accountcode){
      $stmt = $pdo->query("SELECT id, 
                                  accountcode,
                                  name,
                                  def_price,
                                  address1,
                                  address2,
                                  address3,
                                  address4,
                                  salesman,
                                  updated_at
                          from debtors 
                          where accountcode like '%".$accountcode."%' and
                                active = '1'
                          ORDER BY accountcode ASC");
    }
    else if($name){
      $stmt = $pdo->query("SELECT id, 
                                  accountcode,
                                  name,
                                  def_price,
                                  address1,
                                  address2,
                                  address3,
                                  address4,
                                  salesman,
                                  updated_at
                          from debtors 
                          where name like '%".$name."%' and
                                active = '1'
                          ORDER BY name ASC");
    }
    else{
      $stmt = $pdo->query("SELECT id, 
                                  accountcode,
                                  name,
                                  def_price,
                                  address1,
                                  address2,
                                  address3,
                                  address4,
                                  salesman,
                                  updated_at
                          from debtors 
                          where active = '1'
                          ORDER BY accountcode ASC");
    }
      
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
        if(!$row->address1){
          $row->address1 = "";
        }

        if(!$row->address2){
          $row->address2 = "";
        }

        if(!$row->address3){
          $row->address3 = "";
        }

        if(!$row->address4){
          $row->address4 = "";
        }

         $data[] = $row;
      }

      // Return data as JSON
      echo json_encode($data);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>