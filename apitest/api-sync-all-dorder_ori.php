<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   $dorderget = file_get_contents("php://input");
    if (isset($dorderget)) {
        $dorder = json_decode($dorderget);
    } else {
        exit;
    }

    $totallines       = count($dorder);

    $syncedlines      = array();
    $duplicatedlines  = array();

   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo         = new PDO($dsn, $un, $pwd, $opt);

   // Attempt to query database table and retrieve data
   try {
    //delete first then just insert
    for($i = 0; $i < $totallines; $i++){
      //check duplication

      $custDetails = array();

      //get customer details
      $stmt = $pdo->query("SELECT *
                          from debtors where name = '".$dorder[$i]->custSel->name."'");
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $custDetails[] = $row;
      }

      if(empty($custDetails)){
        $custDetails[] = $dorder[$i]->custSel;
      }

      $stmt = $pdo->query("SELECT *
                        FROM deliveryorders
                        WHERE docno = '".$dorder[$i]->docno."' and
                              date  = '".$dorder[$i]->date."' and
                              name  = '".$custDetails[0]->name."'");

      $duplicated  = $stmt->fetch(PDO::FETCH_OBJ);

      if($duplicated){
        $sqldel = "delete from deliveryorders 
                          where docno = '".$dorder[$i]->docno."' and
                                date  = '".$dorder[$i]->date."' and
                                name  = '".$custDetails[0]->name."'";
        
        $stmtdel = $pdo->query($sqldel);
      }
    }

    for($i = 0; $i < $totallines; $i++){

      $custDetails = array();

      //get customer details
      $stmt = $pdo->query("SELECT *
                          from debtors where name = '".$dorder[$i]->custSel->name."'");
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $custDetails[] = $row;
      }

      if(empty($custDetails)){
        $custDetails[] = $dorder[$i]->custSel;
      }

      $docno          = $dorder[$i]->docno;
      $date           = $dorder[$i]->date;
      $amount         = $dorder[$i]->amount;
      $account_code   = $custDetails[0]->accountcode;
      $name           = $custDetails[0]->name;
      $addr1          = $custDetails[0]->address1;
      $addr2          = $custDetails[0]->address2;
      $addr3          = $custDetails[0]->address3;
      $addr4          = $custDetails[0]->address4;
      $tel_no         = $custDetails[0]->tel;
      $fax_no         = $custDetails[0]->fax;
      //$credit_term    = $custDetails[0]->cterm;
      //$rounding       = $dorder[$i]->rounding;
      $created_by     = $dorder[$i]->created_by;
      $created_at     = $dorder[$i]->created_at; 
      $updated_by     = $dorder[$i]->updated_by;
      $updated_at     = $dorder[$i]->updated_at;
    
    
      $stmt = $pdo->query("INSERT INTO deliveryorders 
                          (docno, date, amount, account_code, name, addr1, addr2, addr3, addr4,
                          tel_no,fax_no,created_by,created_at,
                          updated_by,updated_at) 
                          VALUES ('".$docno."',
                                  '".$date."',
                                  '".$amount."',
                                  '".$account_code."',
                                  '".$name."',
                                  '".$addr1."',
                                  '".$addr2."',
                                  '".$addr3."',
                                  '".$addr4."',
                                  '".$tel_no."',
                                  '".$fax_no."',
                                  '".$created_by."',
                                  '".$created_at."',
                                  '".$updated_by."',
                                  '".$updated_at."')");

      array_push($syncedlines, $docno);

      $totalitem = count($dorder[$i]->items);

      for($j = 0; $j < $totalitem; $j++){
        $itemDetails = array();

        //get item details
        $stmt = $pdo->query("SELECT *
                            from stockcodes where code = '".$dorder[$i]->items[$j]->itmcode."'");
        while($row  = $stmt->fetch(PDO::FETCH_OBJ))
        {
           // Assign each row of data to associative array
           $itemDetails[] = $row;
        }

        $stmt = $pdo->query("INSERT INTO deliveryorderdts
                          (doc_no, sequence_no, account_code, item_code,
                            subject, qty, uom, rate, uprice, amount, totalqty, created_at,
                            updated_at) 
                          VALUES ('".$docno."',
                                  '".($j + 1)."',
                                  '".$custDetails[0]->accountcode."',
                                  '".$dorder[$i]->items[$j]->itmcode."',
                                  '".$dorder[$i]->items[$j]->itmdescr."',
                                  '".$dorder[$i]->items[$j]->itmqty."',
                                  '".$itemDetails[0]->uom_id1."',
                                  '1',
                                  '".$dorder[$i]->items[$j]->itmprice."',
                                  '".$dorder[$i]->items[$j]->itmtotprice."',
                                  '".($dorder[$i]->items[$j]->itmqty * 1)."',
                                  '".$created_at."',
                                  '".$updated_at."')");
      }
    }
    echo json_encode($syncedlines);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>