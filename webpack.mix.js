const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

    
mix.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.min.js');
mix.copy('node_modules/moment/min/locales.min.js', 'public/js/locales.min.js')
mix.copy('node_modules/moment/min/moment.min.js', 'public/js/moment.min.js')
mix.copy('node_modules/jquery-ui-sortable-npm/jquery-ui-sortable.min.js', 'public/js/jquery-ui-sortable.min.js');
mix.copy('node_modules/jquery-validation/dist/jquery.validate.min.js', 'public/js/jquery-validation.min.js');

// adminlte
mix.copy('node_modules/admin-lte/dist/css/AdminLTE.css', 'public/css/adminlte.css')
mix.copy('node_modules/admin-lte/dist/css/skins/_all-skins.css', 'public/css/adminlte-allskins.css')
mix.copy('node_modules/admin-lte/dist/css/skins/skin-blue.min.css', 'public/css/adminlte-skin-blue.min.css')
mix.copy('node_modules/admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.css', 'public/css')
mix.copy('node_modules/admin-lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', 'public/css')
mix.copy('node_modules/select2/dist/css/select2.min.css', 'public/css/select2.min.css');
mix.copy('node_modules/admin-lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', 'public/js')
mix.copy('node_modules/admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.js', 'public/js')



// inputmask
mix.copy('node_modules/inputmask/dist/jquery.inputmask.bundle.js', 'public/js/jquery-inputmask.js');
mix.copy('node_modules/inputmask/dist/inputmask/bindings/inputmask.binding.js', 'public/js/jquery-inputmask-bindings.js');
mix.copy('node_modules/inputmask/dist/inputmask/inputmask.date.extensions.js', 'public/js/jquery-inputmask-date-ext.js');

// datatables
mix.copy('node_modules/datatables.net/js/jquery.dataTables.js','public/js/datatables.js');
mix.copy('node_modules/datatables.net-bs/css/dataTables.bootstrap.css','public/css/datatables.css');
mix.copy('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css', 'public/css/datatables.bootstrap4.min.css');
mix.copy('node_modules/datatables.net-responsive-dt/css/responsive.dataTables.min.css', 'public/css/datatables.responsive.min.css');

// select2
mix.copy('node_modules/select2/dist/css/select2.min.css', 'public/css/select2.min.css');
mix.copy('node_modules/select2/dist/js/select2.min.js', 'public/js/select2.min.js');
mix.copy('node_modules/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css', 'public/css/select2-bootstrap4.min.css')

// eonasdan date library for bootstrap4 - tempusdominus
mix.copy('node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css', 'public/css/tempusdominus-bootstrap-4.min.css')
mix.copy('node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js', 'public/js/tempusdominus-bootstrap-4.min.js')

// chartjs
mix.copy('node_modules/chart.js/dist/Chart.min.js', 'public/js/chart.min.js')
mix.copy('node_modules/chart.js/dist/Chart.bundle.min.js', 'public/js/chart.bundle.min.js')

// bootstrap custom file input
mix.copy('node_modules/bs-custom-file-input/dist/bs-custom-file-input.min.js', 'public/js/bs-custom-file-input.min.js')

// summernote
mix.copy('node_modules/bs4-summernote/dist/summernote-bs4.js', 'public/js/summernote-bs4.js')
mix.copy('node_modules/bs4-summernote/dist/summernote.min.js', 'public/js/summernote.min.js')
mix.copy('node_modules/bs4-summernote/dist/summernote-bs4.min.js', 'public/js/summernote-bs4.min.js')
mix.copy('node_modules/bs4-summernote/dist/summernote.css', 'public/css/summernote.css')
mix.copy('node_modules/bs4-summernote/dist/summernote-bs4.css', 'public/css/summernote-bs4.css')
mix.copy('node_modules/bs4-summernote/dist/font/summernote.eot', 'public/css/font/summernote.eot')
mix.copy('node_modules/bs4-summernote/dist/font/summernote.ttf', 'public/css/font/summernote.ttf')
mix.copy('node_modules/bs4-summernote/dist/font/summernote.woff', 'public/css/font/summernote.woff')

mix.copy('node_modules/bootstrap4-toggle/css/bootstrap4-toggle.min.css', 'public/css/bootstrap4-toggle.min.css')
mix.copy('node_modules/bootstrap4-toggle/js/bootstrap4-toggle.min.js', 'public/js/bootstrap4-toggle.min.js')

mix.js('resources/js/app.js', 'public/js').sass('resources/sass/app.scss', 'public/css');
// mix.sass('resources/sass/mail.scss', 'public/css');
