<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>KSM - Health Tracking System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Bird Nest Control System" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">

        <!-- App favicon -->
        <!-- <link rel="shortcut icon" href="../assets/images/favicon.ico"> -->
        <link rel="shortcut icon" href="favicon.ico">
        <!-- Plugins css -->
        <!-- <link href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" /> -->
        <!-- <link rel="stylesheet" href="../assets/plugins/switchery/switchery.min.css"> -->
        <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <!-- <link href="../assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
        <link href="../assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"> -->

        <!-- DataTables -->
        <link href="assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <!-- App favicon -->
        <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->
        <link rel="shortcut icon" href="favicon.ico">
        <!-- Summernote css -->
        <link href="assets/plugins/summernote/summernote-bs4.css" rel="stylesheet" />

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/plugins/morris/morris.css">

        <script src="assets/js/modernizr.min.js"></script>

    </head>

    <style>
        #myBtnUp {
        /*display: none;*/
        position: fixed;
        bottom: 60px;
        right: 30px;
        z-index: 99;
        font-size: 15px;
        border: none;
        outline: none;
        background-color: white;
        cursor: pointer;
        padding: 10px 15px;
        border-radius: 4px;
        }

        #myBtnDown {
        /*display: none;*/
        position: fixed;
        bottom: 10px;
        right: 30px;
        z-index: 99;
        font-size: 15px;
        border: none;
        outline: none;
        background-color: white;
        cursor: pointer;
        padding: 10px 15px;
        border-radius: 4px;
        }

        #myBtnUp:hover {
        background-color: #D5D8DC;
        }

        #myBtnDown:hover {
        background-color: #D5D8DC;
        }
    </style>
    
    <body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <div class="topbar">

            <!-- LOGO -->
            <div class="topbar-left">
                <a href="index.php" class="logo">
                    <span>
                        <img src="logo.png" alt="">
                    </span>
                    <i>
                        <img src="assets/images/logo_sm.png" alt="">
                    </i>
                </a>
            </div>
            <nav class="navbar-custom">
                <ul class="list-unstyled topbar-right-menu float-right mb-0">
                    <li class="dropdown notification-list">
                        <br>
                        <a href="login.php" onclick="return confirm('Confirm to logout?')" class="dropdown-item notify-item">
                            <i class="ti-power-off"></i> <span>Logout</span>
                        </a>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-light waves-effect">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                </ul>
            </nav>

        </div>
        <!-- Top Bar End -->


        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <div class="user-details">
                <div class="pull-left">
                    <img src="assets/images/users/person-placeholder.jpg" alt="" class="thumb-md rounded-circle">
                </div>
                <div class="user-info">
                    <a href="#"><?php echo $_SESSION["uname"]; ?></a>
                </div>
            </div>

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu" id="side-menu">
                    <li class="menu-title">Navigation</li>
                    <li><a href="index.php"><i class="ti-home"></i><span> Dashboard </span></a></li>
                    <li><a href="listing.php"><i class="ti-files"></i><span> Visitor Information</span></a></li>
                    <li><a href="myqrcode.php"><i class="ti-printer"></i><span> My QRCode</span></a></li>
                    <li><a href="myqrcode2.php"><i class="ti-printer"></i><span> My QRCode II</span></a></li>
                    <li><a href="userlist.php"><i class="ti-user"></i><span> System User</span></a></li>
                </ul>

            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">

                <button onclick="topFunction()" id="myBtnUp" title="Go to top"><i class="mdi ti-arrow-up"></i></button>
                <button onclick="bottomFunction()" id="myBtnDown" title="Go to top"><i class="mdi ti-arrow-down"></i></button>    
