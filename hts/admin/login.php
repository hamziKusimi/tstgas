<?php 
        include_once("config.php");
        $pdo = new PDO($dsn, $uid, $pass, $opt);
        $err = "";
        $alert = "";

        $_SESSION["uid"] = "";
        $_SESSION["uname"] = "";
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>KSM Health Tracking System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

    <Style>
    /* Set a background image by replacing the URL below */
    body {
        background: url('21820.jpg') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
    }
    </style>

</head>
<body>
<?php
        if (isset($_POST['submit'])){
            $username = $_POST['username'];
            $password = $_POST['password'];

            try {
                $stmt = $pdo->prepare('SELECT * FROM sysuser WHERE u_code =:username and u_pass =:pass');
                $stmt->execute(array(':username'=>$username,':pass'=>$password));
                $result = $stmt->fetchAll(PDO::FETCH_OBJ);

                if (count($result) <= 0){
                    $alert = "Incorrect User Name or Password.";
                }else{
                    foreach ($result as $item){
                        $_SESSION["uid"] = $item->u_id;
                        $_SESSION["ucode"] = $item->u_code;
                        $_SESSION["uname"] = $item->u_name;
                        $_SESSION["uperm"] = $item->u_perm;

                        echo "<script>self.location='index.php';</script>";
                    }
                }
           
            } catch(PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
            }
        }
?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-5 m-p-20">
                    <div class="wrapper-page">
                        <div class="m-t-10 card-box">
                            <div class="text-center">
                                <img src="logo.svg" class="img-fluid" alt="Responsive image">
                                <h4 class="m-t-0 m-b-10">
                                    KSM Health Tracking System
                                </h4>
                                <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                            </div>

<?php 
        // show error
        if (!empty($err)) { 
?>

                            <div class="row">
                                <div class="col-12">
                                    <div class="panel panel-color panel-danger">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Error</h3>
                                        </div>
                                        <div class="panel-body">
                                            <p><?php echo $err; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
        } 
        //show alert
        if (!empty($alert)) { 
    ?>
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <?php echo $alert; ?>
                                    </div>
                                </div>
                            </div>
                            <?php 
        }
    ?>
                            <div class="account-content">
                                <form class="form-horizontal" method="post" action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>>

                                    <div class="form-group m-b-5">
                                        <div class="col-xs-12">
                                            <label for="emailaddress">User Name</label>
                                            <input class="form-control" type="text" id="username" name="username"
                                                required placeholder="Enter your User Name" autofocus
                                                value=<?php echo $username; ?>>
                                        </div>
                                    </div>

                                    <div class="form-group m-b-5">
                                        <div class="col-xs-12">
                                            <label for="password">Password</label>
                                            <input class="form-control" type="password" id="password" name="password"
                                                placeholder="Enter your password" value=<?php echo $password; ?>>
                                        </div>
                                    </div>

                                    <div class="form-group account-btn text-center m-t-5">
                                        <div class="col-xs-12">
                                            <button class="btn btn-lg btn-primary btn-block" type="submit"
                                                name="submit">Login</button>
                                        </div>
                                    </div>

                                </form>

                                <div class="clearfix"></div>

                            </div>
                        </div>
                        <!-- end card-box-->

                        <div class="row m-t-20">
                            <div class="col-sm-12 text-center">
                                <p class="text-muted">2020 © KUSIMI E-SOLUTION SDN BHD</p>
                            </div>
                        </div>

                    </div>
                    <!-- end wrapper -->

                </div>
            </div>
        </div>
    </section>



    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

</body>

</html>