<?php
    include_once("config.php");
    if (!isset($_SESSION["uid"]) || empty($_SESSION["uid"]) ){
        header('Location: login.php');
	}
	
	$pdo = new PDO($dsn, $uid, $pass, $opt);
	$err = "";
	$alert = "";

	if (!isset($_POST["id"])){
		$id = $_GET["id"];
		$active = "on";
	}else{
		$id = $_POST["id"];
	}
	

	// retreive modules listing 
	try {
		$res = $pdo->query('SELECT * FROM modules order by m_level1,m_level2,m_level3')->fetchAll(PDO::FETCH_OBJ);
	} catch(PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}

	// insert or edit
	if (isset($_POST["save"])) {
		$date = new DateTime("NOW");
		$dtnow = $date->format('Y-m-d H:i:s');
		$code = $_POST["code"];
		$name = $_POST["name"];
		$pass1 = $_POST["pass1"];
		$pass2 = $_POST["pass2"];
		$active = $_POST["active"];
		$active2 = ($_POST["active"]=="on"?"1":"0");
		$id = $_POST["id"];

		$perm = "";
		foreach($res as $item) {
			$perm .= $_POST[$item->m_code].","; 
		}
		if (isset($_SESSION["uperm"])){
			$_SESSION["uperm"] = $perm;
		}

		if (!empty($id)) {		// update
			try {
				$stmt = $pdo->prepare('update sysuser set u_name=:name,u_pass=:pass,u_active=:active,u_perm=:perm,u_modiuser=:modiuser,u_modidt=:modidt WHERE u_id=:id');
				$stmt->execute(array(':name'=>$name,':pass'=>$pass1,':active'=>$active2,':perm'=>$perm,':modiuser'=>$_SESSION["uid"],':modidt'=>$dtnow,':id'=>$id));
	
				$affected_rows = $stmt->rowCount();
				$alert = $affected_rows . " record has been updated";
	
			} catch(PDOException $ex) {
				$err = $err . $ex->getMessage() . "<br/>";
			}

		} else {				// insert
			try {
				$stmt = $pdo->prepare('insert into sysuser (u_code,u_name,u_pass,u_active,u_modiuser,u_modidt,u_keyuser,u_keydt) values (:code,:name,:pass,:active,:modiuser,:modidt,:keyuser,:keydt)');
				$stmt->execute(array(':code'=>$code,':name'=>$name,':pass'=>$pass1,':active'=>$active2,':modiuser'=>$_SESSION["uid"],':modidt'=>$dtnow,':keyuser'=>$_SESSION["uid"],':keydt'=>$dtnow));
				$affected_rows = $stmt->rowCount();
				$alert = $affected_rows . " record has been inserted";
	
			} catch(PDOException $ex) {
				$err = $err . $ex->getMessage() . "<br/>";
			}
		}
	}

	// retrieve record
	if (!empty($id)){

		try {
			$stmt = $pdo->prepare('SELECT * FROM sysuser WHERE u_id = :id');
			$stmt->execute(array(':id' => $id));
			$rec = $stmt->fetch();

			$code = $rec["u_code"];
			$name = $rec["u_name"];
			$pass1 = $rec["u_pass"];
			$pass2 = $pass1;
			$active = ($rec["u_active"]=="1"?"on":"");
			$perm = $rec["u_perm"];

		} catch(PDOException $ex) {
			$err = $err . $ex->getMessage() . "<br/>";
		}
	}
	
	include_once("header.php");

	// show error
	if (!empty($err)) { 
?>
	<div class="row">
		<div class="col-12">
			<div class="panel panel-color panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title">Error</h3>
				</div>
				<div class="panel-body">
					<p>
						<?php echo $err; ?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<?php 
	} 
	//show alert
	if (!empty($alert)) { 
?>
	<div class="row">
		<div class="col-12">
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $alert; ?>
			</div>
		</div>
	</div>
<?php 
	}
?>
	<div class="row">
		<div class="col-12">
			<h4 class="header-title m-t-0 m-b-20">Add/Edit System User</h4>
		</div>
	</div>
	<div class="row p-t-5">
		<div class="col-12">
			<div class="m-b-20">
				<form role="form" class="form-validation" action=<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]); ?> method="post">
					<div class="form-group row">
						<label for="inputCode" class="col-sm-2 form-control-label">User Code
							<span class="text-danger">*</span>
						</label>
						<div class="col-sm-10">
							<input name="code" id="code" type="text" maxlength="10" required parsley-trigger="change" class="form-control" placeholder="Enter User Code"
							    value="<?php echo $code; ?>" <?php echo (!empty($id)? 'disabled': '') ?> >
						</div>
					</div>
					<div class="form-group row">
						<label for="inputName" class="col-sm-2 form-control-label">Name
							<span class="text-danger">*</span>
						</label>
						<div class="col-sm-10">
							<input name="name" id="descr" type="text" maxlength="100" required parsley-trigger="change" class="form-control" placeholder="Enter Name"
							    value="<?php echo $name; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPass" class="col-sm-2 form-control-label">Password
							<span class="text-danger">*</span>
						</label>
						<div class="col-sm-5">
							<input name="pass1" id="pass1" type="password" maxlength="20" required parsley-trigger="change" class="form-control" placeholder="Enter Password"
							    value="<?php echo $pass1; ?>">
						</div>
						<div class="col-sm-5">
							<input name="pass2" id="pass2" type="password" oninput="validatepass()" maxlength="20" required parsley-trigger="change" class="form-control" placeholder="Retype Password for confirmation"
							    value="<?php echo $pass2; ?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-10 offset-sm-2">
							<div class="checkbox">
								<input name="active" id="active" type="checkbox" <?php echo ($active=="on"?"checked":""); ?>>
								<label for="Active"> Active </label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-10 offset-sm-2">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Permission</h3>
								</div>
								<div class="panel-body">
									<div id="page-wrap">
										<ul class="treeview">
<?php 
	$lvl1 = 0;
	$lvl2 = 0;
	$lvl3 = 0;
	foreach($res as $item) {

		${$item->m_code} = (isset($perm) && strstr($perm,$item->m_code.",")?"Checked":"");
		
		if ($lvl1 !== $item->m_level1){

			if ($lvl3 > 0){
				echo '</ul>';
			}

			if ($lvl2 > 0){
				echo '</li>';
				echo '</ul>';
			}
		
			if ($lvl1 > 0){
				echo '</li>';
				echo '<hr>';
			}

			$lvl1 = $item->m_level1;
			$lvl2 = 0;
			$lvl3 = 0;

			echo '<li>';
			echo '<input type="checkbox" name="'.$item->m_code.'" id="'.$item->m_code.'" value="'.$item->m_code.'" '.${$item->m_code}.'>';
			echo '<label for="'.$item->m_code.'" class="custom-unchecked">'.$item->m_descr.'</label>';

			continue;
		}

		if ($lvl2 !== $item->m_level2){

			if ($lvl3 > 0){
				echo '</ul>';
			}

			if ($lvl2 > 0){
				echo '</li>';
			}

			$lvl2 = $item->m_level2;
			if ($lvl2 == 1){
				echo '<ul>';
			}

			echo '<li>';
			echo '<input type="checkbox" name="'.$item->m_code.'" id="'.$item->m_code.'" value="'.$item->m_code.'" '.${$item->m_code}.'>';
			echo '<label for="'.$item->m_code.'" class="custom-unchecked">'.$item->m_descr.'</label>';
			
			continue;
		}

		$lvl3 = $item->m_level3;
		if ($lvl3 == 1){
			echo '<ul>';
		}

		echo '<li>';
		echo '<input type="checkbox" name="'.$item->m_code.'" id="'.$item->m_code.'" value="'.$item->m_code.'" '.${$item->m_code}.'>';
		echo '<label for="'.$item->m_code.'" class="custom-unchecked">'.$item->m_descr.'</label>';
		echo '</li>';
	}

	if ($lvl3 > 0){
		echo '</ul>';
	}

	if ($lvl2 > 0){
		echo '</li>';
		echo '</ul>';
	}

	if ($lvl1 > 0){
		echo '</li>';
	}
?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>					
					<div class="form-group row">
						<div class="col-sm-10 offset-sm-2">
							<button type="button" class="btn btn-default waves-effect m-l-5" onclick="location.href='userlist.php';">
								Back
							</button>
							<input type="hidden" name="id" value="<?php echo $id;?>">
							<Button type="submit" name="save" class="btn btn-primary waves-effect waves-light" <?php echo ((isset($_SESSION["uperm"]) && strstr($_SESSION["uperm"],"USER_E,"))?'':'hidden') ?>>
								Save
							</Button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php

	$res = null;
	$pdo = null;
	include_once("footer.php");
?>

<script>
	function validatepass(){
		if (document.getElementById("pass1").value !== document.getElementById("pass2").value){
			document.getElementById("pass2").setCustomValidity("Confirmation password doesn't match with password.");
		}else{
			document.getElementById("pass2").setCustomValidity("");
		}
	}

	$(function () {

		$('input[type="checkbox"]').change(checkboxChanged);

		function checkboxChanged() {
			var $this = $(this),
				checked = $this.prop("checked"),
				container = $this.parent(),
				siblings = container.siblings();

			container.find('input[type="checkbox"]')
				.prop({
					indeterminate: false,
					checked: checked
				})
				.siblings('label')
				.removeClass('custom-checked custom-unchecked custom-indeterminate')
				.addClass(checked ? 'custom-checked' : 'custom-unchecked');

			checkSiblings(container, checked);
		}

		function checkSiblings($el, checked) {
			var parent = $el.parent().parent(),
				all = true,
				indeterminate = false;

			$el.siblings().each(function () {
				return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
			});

			if (all && checked) {
				parent.children('input[type="checkbox"]')
					.prop({
						indeterminate: false,
						checked: checked
					})
					.siblings('label')
					.removeClass('custom-checked custom-unchecked custom-indeterminate')
					.addClass(checked ? 'custom-checked' : 'custom-unchecked');

				checkSiblings(parent, checked);
			} else if (all && !checked) {
				indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

				parent.children('input[type="checkbox"]')
					.prop("checked", checked)
					.prop("indeterminate", indeterminate)
					.siblings('label')
					.removeClass('custom-checked custom-unchecked custom-indeterminate')
					.addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

				checkSiblings(parent, checked);
			} else {
				$el.parents("li").children('input[type="checkbox"]')
					.prop({
						indeterminate: true,
						checked: false
					})
					.siblings('label')
					.removeClass('custom-checked custom-unchecked custom-indeterminate')
					.addClass('custom-indeterminate');
			}
		}
	});
</script>