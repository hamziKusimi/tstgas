<?php
	include_once("config.php");
	if (!isset($_SESSION["uid"]) || empty($_SESSION["uid"]) ){
        header('Location: login.php');
	}

	$pdo = new PDO($dsn, $uid, $pass, $opt);
	$err = "";
	$alert = "";

	$date = new DateTime("NOW");
	if (!isset($_POST["date1"])) {
		$currdate1 = $date->format('d/m/Y');
	} else {
		$currdate1 = $_POST["date1"];
	}

	if (!isset($_POST["date2"])) {
		$currdate2 = $date->format('d/m/Y');
	} else {
		$currdate2 = $_POST["date2"];
	}

	$cond = "";
	If (isset($_POST["fever"])) {
		$cond = $cond. " and (d_fever='Y') ";
	}
	If (isset($_POST["cough"])) {
		$cond = $cond. " and (d_cough='Y') ";
	}
	If (isset($_POST["breath"])) {
		$cond = $cond. " and (d_breath='Y') ";
	}
	If (isset($_POST["throat"])) {
		$cond = $cond. " and (d_throat='Y') ";
	}
	If (isset($_POST["otr"])) {
		$cond = $cond. " and (d_others<>'') ";
	}
	If (isset($_POST["mask"])) {
		$cond = $cond. " and (d_mask='N') ";
	}
	$fever = isset($_POST["fever"])?"Y":"N";
	$cough = isset($_POST["cough"])?"Y":"N";
	$breath = isset($_POST["breath"])?"Y":"N";
	$throat = isset($_POST["throat"])?"Y":"N";
	$otr = isset($_POST["otr"])?"Y":"N";
	$mask = isset($_POST["mask"])?"Y":"N";

// retreive listing (MUST BBE IN BOTTOM)
	try {
		$date1 = date('Y-m-d', strtotime(str_replace('/', '-', $currdate1)));
		$date2 = date('Y-m-d', strtotime(str_replace('/',  '-', $currdate2)));

		$stmt = $pdo->prepare('SELECT * FROM decl where left(d_datetime,10) >=:date1 and left(d_datetime,10) <= :date2 '.$cond.' order by d_datetime desc');
		$stmt->execute(array(':date1' => $date1, ':date2' => $date2));
		$res = $stmt->fetchAll(PDO::FETCH_OBJ);

	} catch(PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}
	include_once("header.php");
?>

<?php 
	// show error
	if (!empty($err)) { 
?>
<div class="row">
	<div class="col-12">
		<div class="panel panel-color panel-danger">
			<div class="panel-heading">
				<h3 class="panel-title">Error</h3>
			</div>
			<div class="panel-body">
				<p><?php echo $err; ?></p>
			</div>
		</div>
	</div>
</div>
<?php 
	} 
	//show alert
	if (!empty($alert)) { 
?>
<div class="row">
	<div class="col-12">
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<?php echo $alert; ?>
		</div>
	</div>
</div>
<?php 
	}
?>
<div class="row">
    <div class="col-12">
        <h4 class="header-title m-t-0 m-b-20">Visitor Information Listing</h4>
    </div>
</div>
    <div class="row p-t-5">
        <div class="col-12">
            <div class="m-b-20">
                <form name="myform" id="myform" role="form" class="form-validation" action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                    <Button type="submit" id="load" name="load" value="" hidden></Button>
                    <div class="form-group row">
                        <div class="col-sm-1">
                            <label>Date</label>
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" type="text" name="date1" placeholder="dd/mm/yyyy" autocomplete="off" id="datepicker1" value="<?php echo $currdate1; ?>">
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" type="text" name="date2" placeholder="dd/mm/yyyy" autocomplete="off" id="datepicker2" value="<?php echo $currdate2; ?>">
                        </div>
                        <div class="col-sm-2">
                            <Button type="submit" name="Process" class="btn btn-primary waves-effect waves-light">
                                Preview
                            </Button>
                        </div>
                    </div>
					<div class="form-group row">
						<div class="col-sm-10 offset-1">
							<div class="checkbox form-check-inline">
								<input type="checkbox" id="inlineCheckbox1" name="mask" <?php echo $mask=='Y'?'Checked':'';?>>
								<label for="inlineCheckbox1"> No Mask </label>
							</div>						
							<div class="checkbox form-check-inline">
								<input type="checkbox" id="inlineCheckbox2" name="fever" <?php echo $fever=='Y'?'Checked':'';?>">
								<label for="inlineCheckbox2"> Fever </label>
							</div>
							<div class="checkbox checkbox-success form-check-inline">
								<input type="checkbox" id="inlineCheckbox3" name="cough" <?php echo $cough=='Y'?'Checked':'';?>>
								<label for="inlineCheckbox3"> Cough </label>
							</div>
							<div class="checkbox checkbox-pink form-check-inline">
								<input type="checkbox" id="inlineCheckbox4" name="throat" <?php echo $throat=='Y'?'Checked':'';?>>
								<label for="inlineCheckbox4"> Sore throat </label>
							</div>
							<div class="checkbox checkbox-pink form-check-inline">
								<input type="checkbox" id="inlineCheckbox5" name="breath" <?php echo $breath=='Y'?'Checked':'';?>>
								<label for="inlineCheckbox5"> Shortness of breath </label>
							</div> 
							<div class="checkbox checkbox-pink form-check-inline">
								<input type="checkbox" id="inlineCheckbox6" name="otr" <?php echo $otr=='Y'?'Checked':'';?>>
								<label for="inlineCheckbox6"> Others </label>
							</div>  
						</div>
					</div>					
                </form>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table id="mytable" class="table table-bordered" cellspacing="0">
                <thead>
                <tr>
                    <th>Date & Time</th>
                    <th>Name</th>
                    <th>NRIC</th>
					<th>Gender</th>
					<th>HP</th>
					<th>Area</th>
					<th>Type</th>
					<th>Company</th>
					<th>Purpose</th>
					<th>Temp.</th>
					<th>Mask</th>
					<th>Fever</th>
					<th>Cough</th>
					<th>Breath</th>
					<th>Sore Throats</th>
					<th>Others</th>
                </tr>
                </thead>

                <tbody>
<?php 
	foreach($res as $item) {
		echo '<tr>';
		echo '<td>'. $item->d_datetime.'</td>';		
		echo '<td>'. $item->d_name.'</td>';
		echo '<td>'. $item->d_nric.'</td>';
		echo '<td>'. $item->d_gender.'</td>';
		echo '<td>'. $item->d_hp.'</td>';
		echo '<td>'. $item->d_area.'</td>';
		echo '<td>'. $item->d_type.'</td>';
		echo '<td>'. $item->d_company.'</td>';
		echo '<td>'. $item->d_purpose.'</td>';
		echo '<td>'. $item->d_temp.'</td>';
		echo '<td>'. $item->d_mask.'</td>';
		echo '<td>'. $item->d_fever.'</td>';
		echo '<td>'. $item->d_cough.'</td>';
		echo '<td>'. $item->d_breath.'</td>';
		echo '<td>'. $item->d_throat.'</td>';
		echo '<td>'. $item->d_others.'</td>';
		echo '</tr>';
	}
?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- end row -->

<?php

	$res = null;
	$pdo = null;
	include_once("footer.php");
?>
<script>
    function submitform() {
        document.getElementById("myform").submit();
    }
	
    $(document).ready(function() {
        $('#datepicker1').datepicker({
            autoclose: true,
        });
        $('#datepicker2').datepicker({
            autoclose: true,
        });
        
        $('#mytable').DataTable( {
            "searching": false,
            "paging":   true,
			"ordering": false,
			"scrollX": true,
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
				{
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'A4' 
				}	
            ]
        } );
    });
</script>
