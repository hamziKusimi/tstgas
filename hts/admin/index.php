<?php
    include_once("config.php");
    if (!isset($_SESSION["uid"]) || empty($_SESSION["uid"])) {
        header('Location: login.php');
    }

    include_once("header.php");

    $pdo = new PDO($dsn, $uid, $pass, $opt);
    $err = "";
    $alert = "";

    $date = date('m/d/Y h:i:s a', time());

    $dateto = date("Y-m-d");
    $datefr = date("Y-m-d",strtotime("-7 days"));

    $date01 = addDayswithdate($date,-7);
    $date02 = addDayswithdate($date,-6);
    $date03 = addDayswithdate($date,-5);
    $date04 = addDayswithdate($date,-4);
    $date05 = addDayswithdate($date,-3);
    $date06  =addDayswithdate($date,-2);
    $date07 = addDayswithdate($date,-1);
    $date08 = $dateto;

    try {
        $query = "SELECT distinct
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date01') AS dat01,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date02') AS dat02,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date03') AS dat03,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date04') AS dat04,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date05') AS dat05,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date06') AS dat06,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date07') AS dat07,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date08') AS dat08
                FROM decl";
        $stmt = $pdo->query($query);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $ex) {
        throw $ex->getMessage(); 
    }
	
	try {
        $query = "SELECT distinct
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date01' and d_fever='Y') AS dat01,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date02' and d_fever='Y') AS dat02,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date03' and d_fever='Y') AS dat03,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date04' and d_fever='Y') AS dat04,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date05' and d_fever='Y') AS dat05,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date06' and d_fever='Y') AS dat06,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date07' and d_fever='Y') AS dat07,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date08' and d_fever='Y') AS dat08
                FROM decl";
        $stmt = $pdo->query($query);
        $FRows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $ex) {
        throw $ex->getMessage(); 
	}

	try {
        $query = "SELECT distinct
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date01' and d_cough='Y') AS dat01,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date02' and d_cough='Y') AS dat02,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date03' and d_cough='Y') AS dat03,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date04' and d_cough='Y') AS dat04,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date05' and d_cough='Y') AS dat05,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date06' and d_cough='Y') AS dat06,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date07' and d_cough='Y') AS dat07,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date08' and d_cough='Y') AS dat08
                FROM decl";
        $stmt = $pdo->query($query);
        $CRows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $ex) {
        throw $ex->getMessage(); 
	}

	try {
        $query = "SELECT distinct
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date01' and d_throat='Y') AS dat01,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date02' and d_throat='Y') AS dat02,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date03' and d_throat='Y') AS dat03,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date04' and d_throat='Y') AS dat04,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date05' and d_throat='Y') AS dat05,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date06' and d_throat='Y') AS dat06,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date07' and d_throat='Y') AS dat07,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date08' and d_throat='Y') AS dat08
                FROM decl";
        $stmt = $pdo->query($query);
        $TRows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $ex) {
        throw $ex->getMessage(); 
	}

	try {
        $query = "SELECT distinct
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date01' and d_breath='Y') AS dat01,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date02' and d_breath='Y') AS dat02,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date03' and d_breath='Y') AS dat03,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date04' and d_breath='Y') AS dat04,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date05' and d_breath='Y') AS dat05,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date06' and d_breath='Y') AS dat06,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date07' and d_breath='Y') AS dat07,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date08' and d_breath='Y') AS dat08
                FROM decl";
        $stmt = $pdo->query($query);
        $BRows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $ex) {
        throw $ex->getMessage(); 
	}

	try {
        $query = "SELECT distinct
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date01' and d_others='Y') AS dat01,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date02' and d_others='Y') AS dat02,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date03' and d_others='Y') AS dat03,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date04' and d_others='Y') AS dat04,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date05' and d_others='Y') AS dat05,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date06' and d_others='Y') AS dat06,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date07' and d_others='Y') AS dat07,
                    (SELECT COUNT(DISTINCT d_id) FROM decl WHERE LEFT(d_datetime,10) = '$date08' and d_others='Y') AS dat08
                FROM decl";
        $stmt = $pdo->query($query);
        $ORows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $ex) {
        throw $ex->getMessage(); 
	}

    echo '<div class="row">';
    echo '<div class="col-12">';
    echo '<table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">';
    echo '<caption style="caption-side:top; font-weight:bold">7 Days Visitors Summary</Caption>';
    echo '<thead>';
    echo '<tr>';
    echo '<td style="font-weight:bold;text-align:center"></td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date01,8,2)."/".substr($date01,5,2) . '</td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date02,8,2)."/".substr($date02,5,2) . '</td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date03,8,2)."/".substr($date03,5,2) . '</td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date04,8,2)."/".substr($date04,5,2) . '</td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date05,8,2)."/".substr($date05,5,2) . '</td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date06,8,2)."/".substr($date06,5,2) . '</td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date07,8,2)."/".substr($date07,5,2) . '</td>';
    echo '<td style="font-weight:bold;text-align:center">' . substr($date08,8,2)."/".substr($date08,5,2) . '</td>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    foreach ($rows as $arrx) {

        echo '<tr>';
        echo '<td style="text-align:center">Total</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat02"]) .'</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat02"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat03"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat04"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat05"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat06"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat07"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat08"]). '</td>';
        echo '</tr>';
	} 
    foreach ($FRows as $arrx) {

        echo '<tr>';
        echo '<td style="text-align:center">Fever</td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]) .'</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat03"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat04"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat05"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat06"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat07"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat08"]). '</span></td>';
        echo '</tr>';
	} 
    foreach ($TRows as $arrx) {

        echo '<tr>';
        echo '<td style="text-align:center">Sore Throat</td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]) .'</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat03"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat04"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat05"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat06"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat07"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat08"]). '</span></td>';
        echo '</tr>';
	}	
    foreach ($CRows as $arrx) {

        echo '<tr>';
        echo '<td style="text-align:center">Cough</td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]) .'</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat03"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat04"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat05"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat06"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat07"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat08"]). '</span></td>';
        echo '</tr>';
	}	
    foreach ($BRows as $arrx) {

        echo '<tr>';
        echo '<td style="text-align:center">Shortness of breath</td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]) .'</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat02"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat03"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat04"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat05"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat06"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat07"]). '</span></td>';
        echo '<td style="text-align:center"><span class="text-danger">' . number_format( $arrx["dat08"]). '</span></td>';
        echo '</tr>';
	}
    foreach ($ORows as $arrx) {

        echo '<tr>';
        echo '<td style="text-align:center">Others</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat02"]) . '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat02"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat03"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat04"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat05"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat06"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat07"]). '</td>';
        echo '<td style="text-align:center">' . number_format( $arrx["dat08"]). '</td>';
        echo '</tr>';
    }				   
    echo '</tbody>';
    echo '</table>';
    echo '</div>';
    echo '</div>';

    $res = null;
    $pdo = null;
    include_once("footer.php");
?>