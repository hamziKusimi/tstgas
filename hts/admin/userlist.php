<?php
	include_once("config.php");
	if (!isset($_SESSION["uid"]) || empty($_SESSION["uid"]) ){
        header('Location: login.php');
	}

	$pdo = new PDO($dsn, $uid, $pass, $opt);
	$err = "";
    $alert = "";
    
	//delete
	if (isset($_GET["action"]) && $_GET["action"]=="delete") {
		try {
			$id = $_GET["id"];
			$stmt = $pdo->prepare("DELETE FROM sysuser WHERE u_id = :id");
			$stmt->execute(array(':id' => $id));
			$affected_rows = $stmt->rowCount();
			$alert = $affected_rows . " record has been removed";
		} catch(PDOException $ex) {
			$err = $err . $ex->getMessage() . "<br/>";
		}
	}    

	// retreive listing (MUST BBE IN BOTTOM)
	try {
		$res = $pdo->query('SELECT * FROM sysuser order by u_id')->fetchAll(PDO::FETCH_OBJ);
	} catch(PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}
	include_once("header.php");
?>

<?php 
	// show error
	if (!empty($err)) { 
?>
<div class="row">
	<div class="col-12">
		<div class="panel panel-color panel-danger">
			<div class="panel-heading">
				<h3 class="panel-title">Error</h3>
			</div>
			<div class="panel-body">
				<p><?php echo $err; ?></p>
			</div>
		</div>
	</div>
</div>
<?php 
	} 
	//show alert
	if (!empty($alert)) { 
?>
<div class="row">
	<div class="col-12">
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<?php echo $alert; ?>
		</div>
	</div>
</div>
<?php 
	}
?>
<div class="row">
    <div class="col-12">
        <h4 class="header-title m-t-0 m-b-20">System User</h4>
    </div>
</div>
<div class="row p-t-5">
    <div class="col-12 m-1">
		<button type="button" class="btn btn-info waves-effect waves-light" onclick="location.href = 'user.php';" <?php echo ((isset($_SESSION["uperm"]) && strstr($_SESSION["uperm"],"USER_C,"))?'':'hidden') ?>>New</button>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
					<th>User Code</th>
					<th>Name</th>
                    <th>Active</th>
				<?php 
					if (isset($_SESSION["uperm"]) && strstr($_SESSION["uperm"],"USER_D,")){
				?>
                    <th>Actions</th>
				<?php
					}
				?>
                </tr>
                </thead>

                <tbody>
<?php 
	foreach($res as $item) {
		echo '<tr>';
		echo '<td><a href="user.php?id='.$item->u_id.'">'.$item->u_code.'</a></td>';
		echo '<td>'.$item->u_name.'</td>';
		echo '<td>'. ($item->u_active == 1? "Y":"N").'</td>';
		
		if (isset($_SESSION["uperm"]) && strstr($_SESSION["uperm"],"USER_D,")){
			echo '<td>';
			echo '<a href="'.htmlspecialchars($_SERVER["PHP_SELF"]).'?id='.$item->u_id.'&action=delete" onclick="return confirm(\'Confirm to Delete?\')"><i class="mdi mdi-delete"></i></a>';
			echo '</td>';
		}
	}
?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- end row -->

<?php

	$res = null;
	$pdo = null;
	include_once("footer.php");
?>
