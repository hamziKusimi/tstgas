<?php
    include_once("config.php");
    if (!isset($_SESSION["uid"]) || empty($_SESSION["uid"]) ){
        header('Location: login.php');
	}
	
	$pdo = new PDO($dsn, $uid, $pass, $opt);
	$err = "";
	$alert = "";

	try {
		$res = $pdo->query('SELECT * FROM syspec')->fetch();

		$smtp = $res['smtp'];
		$username = $res['smtpuser'];
		$password = $res['smtppass'];

		$coyname = $res['coyname'];
		$coyno = $res['coyno'];
		$coyaddr1 = $res['coyaddr1'];
		$coyaddr2 = $res['coyaddr2'];
		$coyaddr3 = $res['coyaddr3'];
		$coyaddr4 = $res['coyaddr4'];
		$coytel = $res['coytel'];
		$coyfax = $res['coyfax'];
		$coyemail = $res['coyemail'];
		$invrmk = $res['invrmk'];
		$prodcost = $res['prodcost'];
		$cleancost = $res['cleancost'];

	} catch(PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}

	if (isset($_POST["save"])) {
		$smtp = $_POST["smtp"];
		$username = $_POST["username"];
		$password = $_POST["password"];

		$coyname = $_POST["coyname"];
		$coyno = $_POST["coyno"];
		$coyaddr1 = $_POST["coyaddr1"];
		$coyaddr2 = $_POST["coyaddr2"];
		$coyaddr3 = $_POST["coyaddr3"];
		$coyaddr4 = $_POST["coyaddr4"];
		$coytel = $_POST["coytel"];
		$coyfax = $_POST["coyfax"];
		$coyemail = $_POST["coyemail"];
		$invrmk = $_POST["invrmk"];
		$prodcost = $_POST["prodcost"];
		$cleancost = $_POST["cleancost"];

		try {
			$stmt = $pdo->prepare('update syspec set smtp=:smtp,smtpuser=:username,smtppass=:password,coyname=:coyname,coyno=:coyno,coyaddr1=:coyaddr1,coyaddr2=:coyaddr2,coyaddr3=:coyaddr3,coyaddr4=:coyaddr4,coytel=:coytel,coyfax=:coyfax,coyemail=:coyemail,invrmk=:invrmk,prodcost=:prodcost,cleancost=:cleancost');
			$stmt->execute(array(':smtp'=>$smtp,':username'=>$username,':password'=>$password,':coyname'=>$coyname,':coyno'=>$coyno,':coyaddr1'=>$coyaddr1,':coyaddr2'=>$coyaddr2,':coyaddr3'=>$coyaddr3,':coyaddr4'=>$coyaddr4,':coytel'=>$coytel,':coyfax'=>$coyfax,':coyemail'=>$coyemail,':invrmk'=>$invrmk,':prodcost'=>$prodcost,':cleancost'=>$cleancost));

			$affected_rows = $stmt->rowCount();
			$alert = $affected_rows . " record has been updated";

		} catch(PDOException $ex) {
			$err = $err . $ex->getMessage() . "<br/>";
		}
	}
	
	include_once("header.php");

	// show error
	if (!empty($err)) { 
?>
	<div class="row">
		<div class="col-12">
			<div class="panel panel-color panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title">Error</h3>
				</div>
				<div class="panel-body">
					<p>
						<?php echo $err; ?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<?php 
	} 
	//show alert
	if (!empty($alert)) { 
?>
	<div class="row">
		<div class="col-12">
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $alert; ?>
			</div>
		</div>
	</div>
<?php 
	}
?>
	<div class="row">
		<div class="col-12">
			<h4 class="header-title m-t-0 m-b-20">System Setup</h4>
		</div>
	</div>
	<div class="row p-t-5">
		<div class="col-12">
			<div class="m-b-20">
				<form role="form" class="form-validation" action=<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]); ?> method="post">
					<div class="form-group row">
						<label for="inputCoyname" class="col-sm-2 form-control-label">Company Name
						</label>
						<div class="col-sm-10">
							<input name="coyname" id="coyname" type="text" maxlength="100" class="form-control" placeholder="Enter Company Name" value="<?php echo $coyname; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoyno" class="col-sm-2 form-control-label">Company No.
						</label>
						<div class="col-sm-10">
							<input name="coyno" id="coyno" type="text" maxlength="50" class="form-control" placeholder="Enter Company No." value="<?php echo $coyno; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoyaddr1" class="col-sm-2 form-control-label">Address 1
						</label>
						<div class="col-sm-10">
							<input name="coyaddr1" id="coyaddr1" type="text" maxlength="100" class="form-control" placeholder="Enter Address" value="<?php echo $coyaddr1; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoyaddr2" class="col-sm-2 form-control-label">Address 2
						</label>
						<div class="col-sm-10">
							<input name="coyaddr2" id="coyaddr2" type="text" maxlength="100" class="form-control" placeholder="Enter Address" value="<?php echo $coyaddr2; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoyaddr3" class="col-sm-2 form-control-label">Address 3
						</label>
						<div class="col-sm-10">
							<input name="coyaddr3" id="coyaddr3" type="text" maxlength="100" class="form-control" placeholder="Enter Address" value="<?php echo $coyaddr3; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoyaddr4" class="col-sm-2 form-control-label">Address 4
						</label>
						<div class="col-sm-10">
							<input name="coyaddr4" id="coyaddr4" type="text" maxlength="100" class="form-control" placeholder="Enter Address" value="<?php echo $coyaddr4; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoytel" class="col-sm-2 form-control-label">Telephone
						</label>
						<div class="col-sm-10">
							<input name="coytel" id="coytel" type="text" maxlength="50" class="form-control" placeholder="Enter Telephone" value="<?php echo $coytel; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoyfax" class="col-sm-2 form-control-label">Fax
						</label>
						<div class="col-sm-10">
							<input name="coyfax" id="coyfax" type="text" maxlength="50" class="form-control" placeholder="Enter Fax" value="<?php echo $coyfax; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCoyemail" class="col-sm-2 form-control-label">Email
						</label>
						<div class="col-sm-10">
							<input name="coyemail" id="coyemail" type="text" maxlength="50" class="form-control" placeholder="Enter Email" value="<?php echo $coyemail; ?>">
						</div>
					</div>
					<hr>
					<div class="form-group row">
						<label for="inputProdcost" class="col-sm-2 form-control-label">Production Cost
						</label>
						<div class="col-sm-5">
							<input name="prodcost" id="prodcost" type="number" min="0.00" step="0.01" class="form-control" value="<?php echo $prodcost; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputCleancost" class="col-sm-2 form-control-label">Cleaning Process Cost
						</label>
						<div class="col-sm-5">
							<input name="cleancost" id="cleancost" type="number" min="0.00" step="0.01" class="form-control" value="<?php echo $cleancost; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputInvRmk" class="col-sm-2 form-control-label">Invoice Print Out Footer Remark
						</label>
						<div class="col-sm-10">
							<textarea name="invrmk" id="invrmk" class="form-control" style="height: 150px"><?php echo $invrmk; ?></textarea>
						</div>
					</div>	
					<hr>			
					<div class="form-group row">
						<label for="inputSmtp" class="col-sm-2 form-control-label">SMTP Server
						</label>
						<div class="col-sm-5">
							<input name="smtp" id="smtp" type="text" maxlength="30" class="form-control" placeholder="Enter SMTP Server" value="<?php echo $smtp; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputUsername" class="col-sm-2 form-control-label">User Name
						</label>
						<div class="col-sm-5">
							<input name="username" id="username" type="text" maxlength="30" class="form-control" placeholder="Enter User Name" value="<?php echo $username; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword" class="col-sm-2 form-control-label">Password
						</label>
						<div class="col-sm-5">
							<input name="password" id="password" type="password" maxlength="30" class="form-control" placeholder="Enter Password" value="<?php echo $password; ?>">
						</div>
					</div>
								
					<div class="form-group row">
						<div class="col-sm-10 offset-sm-2">
							<Button type="submit" name="save" class="btn btn-primary waves-effect waves-light"  <?php echo ((isset($_SESSION["uperm"]) && strstr($_SESSION["uperm"],"ST_SYS_E,"))?'':'hidden') ?>>
								Save
							</Button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php

	$res = null;
	$pdo = null;
	include_once("footer.php");
?>