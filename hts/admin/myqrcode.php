<?php
    include_once("config.php");
    if (!isset($_SESSION["uid"]) || empty($_SESSION["uid"]) ){
        header('Location: login.php');
    }
    
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF']; 
    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
    $pathInfo = pathinfo($currentPath); 
    $dirInfo =  substr($pathInfo['dirname'],0,strlen($pathInfo['dirname'])-6);
    // output: localhost
    $hostName = $_SERVER['HTTP_HOST']; 
    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
    // return: http://localhost/myproject/
    $path = $protocol.$hostName.$dirInfo."/visitor.php";
	
	include_once("header.php");
    echo '<div class="row">';
    echo '<div class="col-12">';
    echo '<h1 class="m-t-20 m-b-20 text-center">KSM Health Tracking System</h2>';
    echo '</div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col-12 p-200">';
    echo '<img src="https://api.qrserver.com/v1/create-qr-code/?data='.$path.'&size=500x500" class="rounded mx-auto my-auto d-block" alt="..." />';
    echo '</div>';
    echo '</div>';

	include_once("footer.php");
?>
