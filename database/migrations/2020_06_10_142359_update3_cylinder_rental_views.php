<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update3CylinderRentalViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_cylinder_rental");
        DB::statement("
        CREATE
        VIEW view_cylinder_rental
        AS
        SELECT
        qty, delivery_notedts.dn_no AS doc_no,
        'DN' AS doc_type, delivery_notedts.account_code AS debtor,
        delivery_notes.date AS cdate, product
        FROM delivery_notedts
        INNER JOIN delivery_notes ON delivery_notes.dn_no = delivery_notedts.dn_no
        WHERE delivery_notedts.deleted_at IS NULL AND delivery_notedts.deleted_at IS NULL
        AND delivery_notes.dn_type <> 'Sales'
        UNION ALL
        SELECT
        -(COUNT(return_notells.dn_no)) AS qty, return_notells.dn_no AS doc_no, 'RN' AS doc_type,
        return_notes.account_code AS debtor, return_notes.date AS cdate, cylinderproducts.`code` AS product
        FROM return_notells
        INNER JOIN return_notes ON return_notes.dn_no = return_notells.dn_no
        LEFT JOIN cylinders ON return_notells.serial = cylinders.`serial`
        INNER JOIN cylinderproducts ON cylinders.prod_id = cylinderproducts.id
        WHERE return_notells.`type` = 'cy' AND return_notells.deleted_at IS NULL AND return_notes.`deleted_at` IS NULL
        GROUP BY doc_no, cylinderproducts.`code`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
