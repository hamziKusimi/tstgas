<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnnoteMasterView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_new_returnnote_master
            AS
            SELECT
                return_notedts.dn_no AS dn_no,
                new_return_view.dt_sqn AS dt_sqn,
                new_return_view.list_sqn AS list_sqn,
                new_return_view.loading_date,
                new_return_view.unloading_date,
                new_return_view.barcode AS barcode,
                new_return_view.serial AS serial,
                new_return_view.driver AS driver,
                new_return_view.rt_dnno AS rt_dnno,
                new_return_view.descr AS descr,
                new_return_view.rt_dndate AS rt_dndate
            FROM
                return_notedts
                
            JOIN new_return_view ON return_notedts.dn_no = new_return_view.dn_no
            GROUP BY dn_no, dt_sqn, list_sqn
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_new_returnnote_master");
    }
}
