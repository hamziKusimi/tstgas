<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBarcodeserialToCylinderpordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_porderdts', function (Blueprint $table) {
            //
                $table->string('barcode')->nullable();
                $table->string('serial')->nullable();
                $table->string('type')->nullable();
                $table->string('product')->nullable();
                $table->dropColumn('item_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_porderdts', function (Blueprint $table) {
            //
        });
    }
}
