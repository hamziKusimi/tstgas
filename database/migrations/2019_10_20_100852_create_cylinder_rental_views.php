<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylinderRentalViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //DB::statement("DROP VIEW view_cylinder_rental");
        DB::statement("
        CREATE
            /*[ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
            [DEFINER = { user | CURRENT_USER }]
            [SQL SECURITY { DEFINER | INVOKER }]*/
            VIEW view_cylinder_rental
            AS
            SELECT
                qty, delivery_notedts.dn_no AS doc_no,
                'DN' AS doc_type, delivery_notedts.account_code AS debtor,
                delivery_notes.date AS cdate, product
            FROM delivery_notedts
                INNER JOIN delivery_notes ON delivery_notes.dn_no = delivery_notedts.dn_no
            WHERE delivery_notedts.deleted_at IS NULL
            UNION ALL
            SELECT
                -qty, return_notedts.dn_no AS doc_no, 'RN' AS doc_type,
                return_notedts.account_code AS debtor, return_notes.date AS cdate, product
            FROM return_notedts
                INNER JOIN return_notes ON return_notes.dn_no = return_notedts.dn_no
            WHERE return_notedts.deleted_at IS NULL; ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement("DROP VIEW view_cylinder_rental");
    }
}
