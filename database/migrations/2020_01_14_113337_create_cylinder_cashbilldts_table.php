<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylinderCashbilldtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cylinder_cashbilldts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_no')->nullable();
            $table->string('sequence_no')->nullable();
            $table->string('account_code')->nullable();
            $table->string('serial')->nullable();
            $table->string('barcode')->nullable();
            $table->string('subject')->nullable();
            $table->text('details')->nullable();
            $table->string('product')->nullable();
            $table->string('type')->nullable();
            $table->string('reference_no')->nullable();
            $table->string('quantity')->nullable();
            $table->string('unit_price')->nullable();
            $table->string('discount')->nullable();
            $table->string('amount')->nullable();
            $table->string('uom')->nullable();
            $table->string('taxed_amount')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cylinder_cashbilldts');
    }
}
