<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverynoteStatusViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_deliverynote_status
            AS
            SELECT 
                delivery_notes.dn_no AS dn_no,
                delivery_noteprs.datetime AS prepare,
                delivery_notells.datetime AS loading,
                delivery_noteuls.datetime AS unloading
            
            FROM delivery_notes
                INNER JOIN delivery_notedts ON (delivery_notes.dn_no = delivery_notedts.dn_no)
                LEFT JOIN delivery_noteprs ON (delivery_notes.dn_no = delivery_noteprs.dn_no)
                LEFT JOIN delivery_notells ON (delivery_notes.dn_no = delivery_notells.dn_no)
                LEFT JOIN delivery_noteuls ON (delivery_notes.dn_no = delivery_noteuls.dn_no)
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_deliverynote_status");
    }
}
