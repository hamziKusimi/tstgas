<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCyToGasracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gasracks', function (Blueprint $table) {
            //
            $table->string('cy_category')->nullable();
            $table->string('cy_product')->nullable();
            $table->string('cy_capacity')->nullable();
            $table->string('cy_pressure')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gasracks', function (Blueprint $table) {
            //
        });
    }
}
