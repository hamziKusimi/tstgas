<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('porders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('docno');
            $table->date('date')->nullable();
            $table->string('suppdo')->nullable();
            $table->string('suppinv')->nullable();
            $table->date('suppinvdate')->nullable();
            $table->string('ref')->nullable();
            $table->string('ptype')->nullable();   
            $table->string('discount')->nullable();
            $table->string('amount')->nullable();
            $table->string('tax_amount')->nullable();
            $table->string('taxed_amount')->nullable();
            $table->string('account_code')->nullable();
            $table->string('name')->nullable();
            $table->string('addr1')->nullable();
            $table->string('addr2')->nullable();
            $table->string('addr3')->nullable();
            $table->string('addr4')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('currency')->nullable();
            $table->text('header')->nullable(); 
            $table->text('footer')->nullable(); 
            $table->text('summary')->nullable(); 
            $table->string('created_by')->nullable(); 
            $table->string('updated_by')->nullable(); 
            $table->string('deleted_by')->nullable(); 
            $table->string('printed')->nullable(); 
            $table->string('printed_by')->nullable(); 
            $table->date('printed_at')->nullable(); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('porders');
    }
}
