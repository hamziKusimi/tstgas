<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReasonsToCylindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinders', function (Blueprint $table) {
            //
            $table->date('reasons')->nullable();
        });

        Schema::table('gasracks', function (Blueprint $table) {
            //
            $table->date('reasons')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinders', function (Blueprint $table) {
            //
        });
    }
}
