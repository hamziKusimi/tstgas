<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewcolsToSystemSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_setups', function (Blueprint $table) {
            //
            $table->Integer('rate_decimal_place')->nullable();
            $table->string('rate_decimal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_setup', function (Blueprint $table) {
            //
        });
    }
}
