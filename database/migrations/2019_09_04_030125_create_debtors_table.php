<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debtors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('accountcode')->nullable();
            $table->string('D_DC')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('D_CCODE')->nullable();
            $table->string('def_price')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('address4')->nullable();
            $table->string('ptype')->nullable();
            $table->string('hp')->nullable();
            $table->string('tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('cperson')->nullable();
            $table->string('email')->nullable();
            $table->string('cptel')->nullable();
            $table->string('cterm')->nullable();
            $table->string('climit')->nullable();
            $table->string('ccurrency')->nullable();
            $table->string('gstno')->nullable();
            $table->string('brnno')->nullable();
            $table->string('yopen')->nullable();
            $table->text('memo')->nullable();
            $table->string('custom1')->nullable();
            $table->string('custom2')->nullable();
            $table->string('custom3')->nullable();
            $table->string('custom4')->nullable();
            $table->string('custom5')->nullable();
            $table->string('customval1')->nullable();
            $table->string('customval2')->nullable();
            $table->string('customval3')->nullable();
            $table->string('customval4')->nullable();
            $table->string('customval5')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debtors');
    }
}
