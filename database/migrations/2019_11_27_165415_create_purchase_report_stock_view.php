<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReportStockView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE
        VIEW view_purchase_report_stock
        AS
        SELECT item_code, `subject`,uom, SUM(a.qty) AS quantity, SUM(a.amount) AS amount,
            c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
            g.date AS `date`, SUM(a.`taxed_amount`) AS t_amount
        FROM gooddts a
        INNER JOIN stockcodes b ON a.`item_code` = b.`code`
        INNER JOIN categories c ON b.`cat_id` = c.`id`
        INNER JOIN products d ON b.`prod_id` = d.`id`
        INNER JOIN brands e ON b.`brand_id` = e.`id`
        INNER JOIN locations f ON b.`loc_id` = f.`id`
        INNER JOIN goods g ON a.doc_no = g.docno
        WHERE a.`deleted_at` IS NULL
        GROUP BY item_code, `subject`, uom, c.code, d.code, e.code, f.code, g.date;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_purchase_report_stock");
    }
}
