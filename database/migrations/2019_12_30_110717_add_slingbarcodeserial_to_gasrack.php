<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlingbarcodeserialToGasrack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('gasracks', function (Blueprint $table) {
            //
            $table->string('sg_barcode')->nullable();
            $table->string('sg_serial')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gasracks', function (Blueprint $table) {
            //
        });
    }
}
