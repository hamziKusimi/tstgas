<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverynoteMasterView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_new_deliverynote_master
            AS
            SELECT
                delivery_notedts.dn_no AS dn_no,
                new_view.dt_sqn AS dt_sqn,
                new_view.list_sqn AS list_sqn,
                new_view.prepare_date,
                new_view.loading_date,
                new_view.unloading_date,
                new_view.barcode AS barcode,
                new_view.serial AS serial,
                new_view.driver AS driver
            FROM
                delivery_notedts
                
            JOIN new_view ON delivery_notedts.dn_no = new_view.dn_no
            GROUP BY dn_no, dt_sqn, list_sqn
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_new_deliverynote_master");
    }
}
