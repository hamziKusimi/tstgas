<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditors', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('accountcode')->nullable();
            $table->string('C_DC')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('C_CCODE')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('address4')->nullable();
            $table->string('ptype')->nullable();
            $table->string('hp')->nullable();
            $table->string('tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('cperson')->nullable();
            $table->string('email')->nullable();
            $table->string('cptel')->nullable();
            $table->string('cterm')->nullable();
            $table->string('climit')->nullable();
            $table->string('ccurrency')->nullable();
            $table->string('gstno')->nullable();
            $table->string('brnno')->nullable();
            $table->string('yopen')->nullable();
            $table->text('memo')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditors');
    }
}
