<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReturnNotePrintRackView3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        DB::statement("DROP VIEW view_return_note_print_rack");
        DB::statement("
        CREATE
        VIEW view_return_note_print_rack
        AS
        SELECT return_notells.dn_no AS rn, delivery_noteprs.`dn_no`, delivery_noteprs.`serial` AS 'serial', gasracktypes.code AS 'type', gasracks.`testdate` AS 'testdate'
        FROM return_notells
            INNER JOIN delivery_noteprs ON (return_notells.dn_no = delivery_noteprs.return_note AND return_notells.serial = delivery_noteprs.serial)
            INNER JOIN gasracks ON delivery_noteprs.serial = gasracks.serial
            INNER JOIN gasracktypes ON gasracktypes.`id` = gasracks.`type`
        WHERE delivery_noteprs.`deleted_at` IS NULL AND gasracks.`deleted_at`IS NULL
        AND gasracktypes.`deleted_at`IS NULL; ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
