<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //DB::statement("DROP VIEW customer_view");
        DB::statement("
            CREATE VIEW `customer_view` AS
            SELECT
                `gooddts`.`id` AS `id`,
                `gooddts`.`doc_no` AS `doc_no`,
                `gooddts`.`updated_at` AS `updated_at`,
                `gooddts`.`account_code` AS `account_code`,
                `gooddts`.`item_code` AS `item_code`,
                `gooddts`.`subject` AS `subject`,
                `gooddts`.`qty` AS `qty`,
                `gooddts`.`uom` AS `uom`,
                `gooddts`.`rate` AS `rate`,
                `gooddts`.`ucost` AS `ucost`,
                `gooddts`.`discount` AS `discount`,
                `gooddts`.`amount` AS `amount`,
                `gooddts`.`updated_at` AS `date`,
                `goods`.`name` AS `name`,
                `stockcodes`.`descr` AS `descr`,
                `stockcodes`.`ref1` AS `ref1`,
                `stockcodes`.`ref2` AS `ref2`,
                `stockcodes`.`model` AS `model`,
                `stockcodes`.`weight` AS `weight`,
                `stockcodes`.`type` AS `type`,
                `categories`.`code` AS `category`,
                `products`.`code` AS `product`,
                `brands`.`code` AS `brand`,
                `locations`.`code` AS `location`
            FROM gooddts
            INNER JOIN `stockcodes`
                ON `gooddts`.`item_code` = `stockcodes`.`code`
            INNER JOIN `categories`
                ON `stockcodes`.`cat_id` = `categories`.`id`
            INNER JOIN `products`
                ON `stockcodes`.`prod_id` = `products`.`id`
            INNER JOIN `brands`
                ON `stockcodes`.`brand_id` = `brands`.`id`
            INNER JOIN `locations`
                ON `stockcodes`.`loc_id` = `locations`.`id`
            INNER JOIN `goods`
                ON `gooddts`.`doc_no` = `goods`.`docno`
            UNION ALL
            SELECT
                `preturndts`.`id` AS `id`,
                `preturndts`.`doc_no` AS `doc_no`,
                `preturndts`.`updated_at` AS `updated_at`,
                `preturndts`.`account_code` AS `account_code`,
                `preturndts`.`item_code` AS `item_code`,
                `preturndts`.`subject` AS `subject`,
                `preturndts`.`qty` AS `qty`,
                `preturndts`.`uom` AS `uom`,
                `preturndts`.`rate` AS `rate`,
                `preturndts`.`ucost` AS `ucost`,
                `preturndts`.`discount` AS `discount`,
                `preturndts`.`amount` AS `amount`,
                `preturndts`.`updated_at` AS `date`,
                `preturns`.`name` AS `name`,
                `stockcodes`.`descr` AS `descr`,
                `stockcodes`.`ref1` AS `ref1`,
                `stockcodes`.`ref2` AS `ref2`,
                `stockcodes`.`model` AS `model`,
                `stockcodes`.`weight` AS `weight`,
                `stockcodes`.`type` AS `type`,
                `categories`.`code` AS `category`,
                `products`.`code` AS `product`,
                `brands`.`code` AS `brand`,
                `locations`.`code` AS `location`
            FROM preturndts
            INNER JOIN `stockcodes`
                ON `preturndts`.`item_code` = `stockcodes`.`code`
            INNER JOIN `categories`
                ON `stockcodes`.`cat_id` = `categories`.`id`
            INNER JOIN `products`
                ON `stockcodes`.`prod_id` = `products`.`id`
            INNER JOIN `brands`
                ON `stockcodes`.`brand_id` = `brands`.`id`
            INNER JOIN `locations`
                ON `stockcodes`.`loc_id` = `locations`.`id`
            INNER JOIN `preturns`
                ON `preturndts`.`doc_no` = `preturns`.`docno`
            ORDER BY `date`;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement("DROP VIEW customer_view");
    }
}
