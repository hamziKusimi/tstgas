<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReturnNotePrintCylinderView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_return_note_print_cylinder");
        DB::statement("
        CREATE
        VIEW view_return_note_print_cylinder
        AS
        SELECT IFNULL(cylinders.descr, 'Undefined') AS description, return_notells.dn_no AS dn_no, `type`, return_notells.id AS id,
            IFNULL(return_notells.barcode, 'Undefined') AS barcode, IFNULL(cylinders.serial, 'Undefined') AS serial
        FROM return_notells
        LEFT JOIN cylinders ON return_notells.barcode = cylinders.barcode OR cylinders.serial = return_notells.serial
        WHERE return_notells.type = 'cy' AND return_notells.`deleted_at` IS NULL; ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
