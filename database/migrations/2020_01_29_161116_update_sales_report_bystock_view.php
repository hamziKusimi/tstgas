<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSalesReportBystockView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW view_sales_report_bystock");
        DB::statement("
        CREATE
            VIEW view_sales_report_bystock
            AS
            SELECT item_code, `subject`,uom, SUM(a.quantity) AS quantity, SUM(a.amount) AS amount,
            c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
            g.date AS `date`, a.`taxed_amount` AS t_amount, 'INV' AS s_type,
            SUM(a.unit_price * a.quantity) AS totalprice,
            SUM(a.discount) AS discount,
            SUM(a.tax_amount) AS taxamount,
            SUM(a.amount + a.tax_amount) AS taxed_amount
            FROM invoice_data a
            INNER JOIN stockcodes b ON a.`item_code` = b.`code`
            INNER JOIN categories c ON b.`cat_id` = c.`id`
            INNER JOIN products d ON b.`prod_id` = d.`id`
            INNER JOIN brands e ON b.`brand_id` = e.`id`
            INNER JOIN locations f ON b.`loc_id` = f.`id`
            INNER JOIN invoices g ON a.doc_no = g.docno
            WHERE a.`deleted_at` IS NULL AND g.`deleted_at` IS NULL
            GROUP BY item_code

            UNION ALL
            SELECT item_code, `subject`,uom, SUM(a.qty) AS quantity, SUM(a.amount) AS amount,
                c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
                CAST(g.date as date) AS `date`, a.`taxed_amount` AS t_amount, 'CB' AS s_type,
                SUM(a.uprice * a.qty) AS totalprice,
                SUM(a.discount) AS discount,
                SUM(a.tax_amount) AS taxamount,
                SUM(a.amount + a.tax_amount) AS taxed_amount
            FROM cashbilldts a
            INNER JOIN stockcodes b ON a.`item_code` = b.`code`
            INNER JOIN categories c ON b.`cat_id` = c.`id`
            INNER JOIN products d ON b.`prod_id` = d.`id`
            INNER JOIN brands e ON b.`brand_id` = e.`id`
            INNER JOIN locations f ON b.`loc_id` = f.`id`
            INNER JOIN cashbills g ON a.doc_no = g.docno
            WHERE a.`deleted_at` IS NULL AND g.`deleted_at` IS NULL
            GROUP BY item_code

            UNION ALL
            SELECT item_code, `subject`,uom, SUM(a.qty) AS quantity, SUM(a.amount) AS amount,
                c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
                g.date AS `date`, a.`taxed_amount` AS t_amount, 'DO' AS s_type,
                SUM(a.uprice * a.qty) AS totalprice,
                SUM(a.discount) AS discount,
                SUM(a.tax_amount) AS taxamount,
                SUM(a.amount + a.tax_amount) AS taxed_amount
            FROM deliveryorderdts a
            INNER JOIN stockcodes b ON a.`item_code` = b.`code`
            INNER JOIN categories c ON b.`cat_id` = c.`id`
            INNER JOIN products d ON b.`prod_id` = d.`id`
            INNER JOIN brands e ON b.`brand_id` = e.`id`
            INNER JOIN locations f ON b.`loc_id` = f.`id`
            INNER JOIN deliveryorders g ON a.doc_no = g.docno
            WHERE a.`deleted_at` IS NULL AND g.`deleted_at` IS NULL
            GROUP BY item_code

            UNION ALL
            SELECT item_code, `subject`,uom, SUM(a.qty) AS quantity, SUM(a.amount) AS amount,
                c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
                g.date AS `date`, a.`taxed_amount` AS t_amount, 'SR' AS s_type,
                SUM(a.uprice * a.qty) AS totalprice,
                SUM(a.discount) AS discount,
                SUM(a.tax_amount) AS taxamount,
                SUM(a.amount + a.tax_amount) AS taxed_amount
            FROM salesreturndts a
            INNER JOIN stockcodes b ON a.`item_code` = b.`code`
            INNER JOIN categories c ON b.`cat_id` = c.`id`
            INNER JOIN products d ON b.`prod_id` = d.`id`
            INNER JOIN brands e ON b.`brand_id` = e.`id`
            INNER JOIN locations f ON b.`loc_id` = f.`id`
            INNER JOIN salesreturns g ON a.doc_no = g.docno
            WHERE a.`deleted_at` IS NULL AND g.`deleted_at` IS NULL
            GROUP BY item_code;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
