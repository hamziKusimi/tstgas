<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rp_no')->nullable();
            $table->string('tech')->nullable();
            $table->date('logindate')->nullable();
            $table->string('barcode')->nullable();
            $table->string('serial')->nullable();
            $table->date('completedate')->nullable();
            $table->string('charge_in')->nullable();
            $table->string('charge_out')->nullable();
            $table->text('fault')->nullable();
            $table->string('created_by')->nullable(); 
            $table->string('updated_by')->nullable(); 
            $table->string('deleted_by')->nullable(); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
}
