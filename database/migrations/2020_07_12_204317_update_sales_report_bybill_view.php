<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSalesReportBybillView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(config('config.cylinder.cylinder') == "true"){
            DB::statement("DROP VIEW view_sales_report_bybill");
            DB::statement("
                CREATE
                VIEW view_sales_report_bybill
                AS
                SELECT docno, DATE, account_code, account_code AS CODE, NAME, amount,
                    taxed_amount, CONCAT(MONTHNAME(`date`), ' ' , YEAR(`date`)) AS MONTH,
                    'INV' AS s_type, discount
                FROM cylinder_invoices
                WHERE cylinder_invoices.`deleted_at` IS NULL
                UNION
                SELECT docno, DATE, account_code, account_code AS CODE, NAME, amount,
                    taxed_amount, CONCAT(MONTHNAME(`date`), ' ' , YEAR(`date`)) AS MONTH,
                    'CB' AS s_type, discount
                FROM cylinder_cashbills
                WHERE cylinder_cashbills.`deleted_at` IS NULL
                UNION
                SELECT docno, DATE, account_code, account_code AS CODE, NAME, amount,
                    taxed_amount, CONCAT(MONTHNAME(`date`), ' ' , YEAR(`date`)) AS MONTH,
                    'SR' AS s_type, '' AS discount
                FROM salesreturns
                WHERE salesreturns.`deleted_at` IS NULL;
            ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
