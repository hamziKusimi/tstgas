<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostedToInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goods', function (Blueprint $table) {
            $table->char('posted')->nullable()->default('N');;
        });
        Schema::table('preturns', function (Blueprint $table) {
            $table->char('posted')->nullable()->default('N');;
        });
        Schema::table('cashsales', function (Blueprint $table) {
            $table->char('posted')->nullable()->default('N');;
        });
        Schema::table('cashbills', function (Blueprint $table) {
            $table->char('posted')->nullable()->default('N');;
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->char('posted')->nullable()->default('N');;
        });
        Schema::table('salesreturns', function (Blueprint $table) {
            $table->char('posted')->nullable()->default('N');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
