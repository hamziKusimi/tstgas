<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSetCategoryToSystemSetups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_setups', function (Blueprint $table) {
            //
            $table->string('set_category1')->nullable();
            $table->string('set_category2')->nullable();
            $table->string('set_category3')->nullable();
            $table->string('set_category4')->nullable();
            $table->string('set_category5')->nullable();
            $table->string('set_category6')->nullable();
            $table->string('set_category7')->nullable();
            $table->string('set_category8')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_setups', function (Blueprint $table) {
            //
        });
    }
}
