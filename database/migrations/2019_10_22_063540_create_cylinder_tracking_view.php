<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylinderTrackingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        DB::statement("
            CREATE VIEW `cylinder_tracking_view` AS
            SELECT 
                'Delivery Note' AS model,
                delivery_notells.barcode AS barcode,
                delivery_notells.serial AS serial,
                'Loading' AS action,
                delivery_notells.driver AS description,
                delivery_notells.datetime AS datetime,
                delivery_notells.created_at AS created_at,
                delivery_notells.updated_at AS updated_at
            FROM delivery_notells WHERE delivery_notells.datetime IS NOT NULL
            UNION ALL
            SELECT
                'Delivery Note' AS model,
                delivery_noteuls.barcode AS barcode,
                delivery_noteuls.serial AS serial,
                'UnLoading' AS action,
                delivery_noteuls.driver AS description,
                delivery_noteuls.datetime AS datetime,
                delivery_noteuls.created_at AS created_at,
                delivery_noteuls.updated_at AS updated_at	
            FROM delivery_noteuls WHERE delivery_noteuls.datetime IS NOT NULL 
            UNION ALL
            SELECT 
                'Return Note' AS model,
                return_notells.barcode AS barcode,
                return_notells.serial AS serial,
                'Loading' AS action,
                return_notells.driver AS description,
                return_notells.datetime AS datetime,
                return_notells.created_at AS created_at,
                return_notells.updated_at AS updated_at
            FROM return_notells WHERE return_notells.datetime IS NOT NULL
            UNION ALL
            SELECT
                'Return Note' AS model,
                return_noteuls.barcode AS barcode,
                return_noteuls.serial AS serial,
                'UnLoading' AS action,
                return_noteuls.driver AS description,
                return_noteuls.datetime AS datetime,
                return_noteuls.created_at AS created_at,
                return_noteuls.updated_at AS updated_at	
            FROM return_noteuls WHERE return_noteuls.datetime IS NOT NULL 
            UNION ALL
            SELECT
                'Refill' AS model,
                refills.barcode AS barcode,
                refills.serial AS serial,
                'Refill Cylinder' AS action,
                refills.tech AS description,
                refills.datetime AS datetime,
                refills.created_at AS created_at,
                refills.updated_at AS updated_at	
            FROM refills WHERE refills.datetime IS NOT NULL 
            UNION ALL
            SELECT
                'Repair' AS model,
                repairs.barcode AS barcode,
                repairs.serial AS serial,
                'Repair Start' AS action,
                repairs.tech AS description,
                repairs.logindate AS datetime,
                repairs.created_at AS created_at,
                repairs.updated_at AS updated_at	
            FROM repairs WHERE repairs.logindate IS NOT NULL 
            
            UNION ALL
            SELECT
                'Repair' AS model,
                repairs.barcode AS barcode,
                repairs.serial AS serial,
                'Repair End' AS action,
                repairs.tech AS description,
                repairs.completedate AS datetime,
                repairs.created_at AS created_at,
                repairs.updated_at AS updated_at	
            FROM repairs WHERE repairs.completedate IS NOT NULL 
            
            ORDER BY datetime
            
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW cylinder_tracking_view");
    }
}
