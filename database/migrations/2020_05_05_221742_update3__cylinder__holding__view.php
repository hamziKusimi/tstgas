<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update3CylinderHoldingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //DB::statement("DROP VIEW view_cylinder_holding");
        DB::statement("
        CREATE
        VIEW view_cylinder_holding
        AS
        SELECT
        delivery_notedts.dn_no AS doc_no,'DN' AS doc_type,
        delivery_notes.account_code AS debtor, IFNULL(delivery_noteprs.serial , '-') AS s_no,
        delivery_notes.date AS cdate, delivery_noteprs.return_note AS RNdoc,
        delivery_noteprs.return_note_date AS RNdate, product, delivery_notes.lpono,
        delivery_notes.project_name,
        CASE
        WHEN return_notes.`account_code` != delivery_notes.`account_code` THEN 'Invalid CRR-Debtor'
        END AS `remark`,
        debtors.`name` AS `name`,
        debtors.area AS `area`,
        CONCAT(IFNULL(cylindergroups.`code`,''), ' ', IFNULL(cylindercategories.`code`,'')) AS `type`
    FROM delivery_notes
        INNER JOIN delivery_notedts ON  delivery_notedts.dn_no =  delivery_notes.dn_no
        INNER JOIN delivery_noteprs ON delivery_noteprs.dn_no = delivery_notedts.dn_no
        AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no
        LEFT JOIN return_notes ON return_notes.`dn_no` = delivery_noteprs.return_note
        LEFT JOIN debtors ON delivery_notes.`account_code` = debtors.`accountcode`
        INNER JOIN cylinders ON delivery_noteprs.serial =  cylinders.`serial`
        LEFT JOIN cylindercategories ON cylinders.`cat_id` = cylindercategories.`id`
        LEFT JOIN cylindergroups ON cylinders.`group_id` = cylindergroups.`id`
    WHERE
        delivery_notedts.deleted_at IS NULL
        AND delivery_noteprs.deleted_at IS NULL AND delivery_notes.deleted_at IS NULL
        AND delivery_noteprs.type = 'cy' AND return_notes.deleted_at IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
