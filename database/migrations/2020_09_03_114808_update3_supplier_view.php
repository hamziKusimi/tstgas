<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update3SupplierView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(config('config.cylinder.cylinder') == "true"){
            DB::statement("DROP VIEW supplier_view");
            DB::statement("
                CREATE VIEW `supplier_view` AS
                SELECT
                    `cylinder_cashbilldts`.`id` AS `id`,
                    `cylinder_cashbilldts`.`doc_no` AS `doc_no`,
                    `cylinder_cashbilldts`.`updated_at` AS `updated_at`,
                    `cylinder_cashbilldts`.`account_code` AS `account_code`,
                    '-' AS `item_code`,
                    `cylinder_cashbilldts`.`subject` AS `subject`,
                    `cylinder_cashbilldts`.`quantity` AS `qty`,
                    `cylinder_cashbilldts`.`uom` AS `uom`,
                    '-' AS `rate`,
                    `cylinder_cashbilldts`.`unit_price` AS `uprice`,
                    0 AS `discount`,
                    `cylinder_cashbilldts`.`amount` AS `amount`,
                    `cylinder_cashbills`.`date` AS `date`,
                    `cylinder_cashbills`.`name` AS `name`,
                    `cyl`.`descr` AS `descr`,
                    '-' AS `ref1`,
                    '-' AS `ref2`,
                    '-' AS `model`,
                    NULL AS `weight`,
                    'cylinder' AS `type`,
                    0 AS `inactive`,
                    `cylindercategories`.`code` AS `category`,
                    `cylinderproducts`.`code` AS `product`,
                    '-' AS `brand`,
                    '-' AS `location`,
                    `cylindergroups`.`code` AS `group`
                FROM cylinder_cashbilldts
                LEFT JOIN (
                        SELECT descr, cat_id, prod_id, group_id
                        FROM cylinders
                        WHERE deleted_at IS NULL
                        GROUP BY descr
                    ) cyl ON cyl.descr = TRIM(SUBSTRING_INDEX(cylinder_cashbilldts.subject, 'IN MANIFOLD', 1))
                LEFT JOIN `cylindercategories`
                            ON `cyl`.`cat_id` = `cylindercategories`.`id`
                            AND `cylindercategories`.`deleted_at` IS NULL
                LEFT JOIN `cylinderproducts`
                            ON `cyl`.`prod_id` = `cylinderproducts`.`id`
                            AND `cylinderproducts`.`deleted_at` IS NULL
                LEFT JOIN `cylindergroups`
                            ON `cyl`.`group_id` = `cylindergroups`.`id`
                            AND `cylindergroups`.`deleted_at` IS NULL
                INNER JOIN `cylinder_cashbills`
                            ON `cylinder_cashbilldts`.`doc_no` = `cylinder_cashbills`.`docno`
                            AND `cylinder_cashbills`.`deleted_at` IS NULL
                WHERE cylinder_cashbilldts.`deleted_at` IS NULL
                UNION ALL
                SELECT
                    `cylinder_invoicedts`.`id` AS `id`,
                    `cylinder_invoicedts`.`doc_no` AS `doc_no`,
                    `cylinder_invoicedts`.`updated_at` AS `updated_at`,
                    `cylinder_invoicedts`.`account_code` AS `account_code`,
                    '-' AS `item_code`,
                    `cylinder_invoicedts`.`subject` AS `subject`,
                    `cylinder_invoicedts`.`quantity` AS `qty`,
                    `cylinder_invoicedts`.`uom` AS `uom`,
                    '-' AS `rate`,
                    `cylinder_invoicedts`.`unit_price` AS `uprice`,
                    0 AS `discount`,
                    `cylinder_invoicedts`.`amount` AS `amount`,
                    `cylinder_invoices`.`date` AS `date`,
                    `cylinder_invoices`.`name` AS `name`,
                    `cyl`.`descr` AS `descr`,
                    '-' AS `ref1`,
                    '-' AS `ref2`,
                    '-' AS `model`,
                    NULL AS `weight`,
                    'cylinder' AS `type`,
                    0 AS `inactive`,
                    `cylindercategories`.`code` AS `category`,
                    `cylinderproducts`.`code` AS `product`,
                    '-' AS `brand`,
                    '-' AS `location`,
                    `cylindergroups`.`code` AS `group`
                FROM cylinder_invoicedts
                LEFT JOIN (
                        SELECT descr, cat_id, prod_id, group_id
                        FROM cylinders
                        WHERE deleted_at IS NULL
                        GROUP BY descr
                    ) cyl ON cyl.descr = TRIM(SUBSTRING_INDEX(cylinder_invoicedts.subject, 'IN MANIFOLD', 1))
                LEFT JOIN `cylindercategories`
                            ON `cyl`.`cat_id` = `cylindercategories`.`id`
                            AND `cylindercategories`.`deleted_at` IS NULL
                LEFT JOIN `cylinderproducts`
                            ON `cyl`.`prod_id` = `cylinderproducts`.`id`
                            AND `cylinderproducts`.`deleted_at` IS NULL
                LEFT JOIN `cylindergroups`
                            ON `cyl`.`group_id` = `cylindergroups`.`id`
                            AND `cylindergroups`.`deleted_at` IS NULL
                INNER JOIN `cylinder_invoices`
                            ON `cylinder_invoicedts`.`doc_no` = `cylinder_invoices`.`docno`
                            AND `cylinder_invoices`.`deleted_at` IS NULL
                WHERE cylinder_invoicedts.`deleted_at` IS NULL
                UNION ALL
                SELECT
                    `cylinder_cashbillspdts`.`id` AS `id`,
                    `cylinder_cashbillspdts`.`doc_no` AS `doc_no`,
                    `cylinder_cashbillspdts`.`updated_at` AS `updated_at`,
                    `cylinder_cashbillspdts`.`account_code` AS `account_code`,
                    `cylinder_cashbillspdts`.`item_code` AS `item_code`,
                    `cylinder_cashbillspdts`.`subject` AS `subject`,
                    `cylinder_cashbillspdts`.`quantity` AS `qty`,
                    `cylinder_cashbillspdts`.`uom` AS `uom`,
                    `cylinder_cashbillspdts`.`rate` AS `rate`,
                    `cylinder_cashbillspdts`.`unit_price` AS `uprice`,
                    `cylinder_cashbillspdts`.`discount` AS `discount`,
                    `cylinder_cashbillspdts`.`amount` AS `amount`,
                    `cylinder_cashbills`.`date` AS `date`,
                    `cylinder_cashbills`.`name` AS `name`,
                    `stockcodes`.`descr` AS `descr`,
                    `stockcodes`.`ref1` AS `ref1`,
                    `stockcodes`.`ref2` AS `ref2`,
                    `stockcodes`.`model` AS `model`,
                    `stockcodes`.`weight` AS `weight`,
                    `stockcodes`.`type` AS `type`,
                    `stockcodes`.`inactive` AS `inactive`,
                    `categories`.`code` AS `category`,
                    `products`.`code` AS `product`,
                    `brands`.`code` AS `brand`,
                    `locations`.`code` AS `location`,
                    '-' AS `group`
                FROM cylinder_cashbillspdts
                LEFT JOIN `stockcodes`
                    ON `cylinder_cashbillspdts`.`item_code` = `stockcodes`.`code`
                    AND `stockcodes`.`deleted_at` IS NULL
                LEFT JOIN `categories`
                    ON `stockcodes`.`cat_id` = `categories`.`id`
                    AND `categories`.`deleted_at` IS NULL
                LEFT JOIN `products`
                    ON `stockcodes`.`prod_id` = `products`.`id`
                    AND `products`.`deleted_at` IS NULL
                LEFT JOIN `brands`
                    ON `stockcodes`.`brand_id` = `brands`.`id`
                    AND `brands`.`deleted_at` IS NULL
                LEFT JOIN `locations`
                    ON `stockcodes`.`loc_id` = `locations`.`id`
                    AND `locations`.`deleted_at` IS NULL
                INNER JOIN `cylinder_cashbills`
                    ON `cylinder_cashbillspdts`.`doc_no` = `cylinder_cashbills`.`docno`
                    AND `cylinder_cashbills`.`deleted_at` IS NULL
                WHERE cylinder_cashbillspdts.deleted_at IS NULL
                UNION ALL
                SELECT
                    `cylinder_invoicespdts`.`id` AS `id`,
                    `cylinder_invoicespdts`.`doc_no` AS `doc_no`,
                    `cylinder_invoicespdts`.`updated_at` AS `updated_at`,
                    `cylinder_invoicespdts`.`account_code` AS `account_code`,
                    `cylinder_invoicespdts`.`item_code` AS `item_code`,
                    `cylinder_invoicespdts`.`subject` AS `subject`,
                    `cylinder_invoicespdts`.`quantity` AS `qty`,
                    `cylinder_invoicespdts`.`uom` AS `uom`,
                    `cylinder_invoicespdts`.`rate` AS `rate`,
                    `cylinder_invoicespdts`.`unit_price` AS `unit_price`,
                    `cylinder_invoicespdts`.`discount` AS `discount`,
                    `cylinder_invoicespdts`.`amount` AS `amount`,
                    `cylinder_invoices`.`date` AS `date`,
                    `cylinder_invoices`.`name` AS `name`,
                    `stockcodes`.`descr` AS `descr`,
                    `stockcodes`.`ref1` AS `ref1`,
                    `stockcodes`.`ref2` AS `ref2`,
                    `stockcodes`.`model` AS `model`,
                    `stockcodes`.`weight` AS `weight`,
                    `stockcodes`.`type` AS `type`,
                    `stockcodes`.`inactive` AS `inactive`,
                    `categories`.`code` AS `category`,
                    `products`.`code` AS `product`,
                    `brands`.`code` AS `brand`,
                    `locations`.`code` AS `location`,
                    '-' AS `group`
                FROM cylinder_invoicespdts
                INNER JOIN `stockcodes`
                    ON `cylinder_invoicespdts`.`item_code` = `stockcodes`.`code`
                    AND `stockcodes`.`deleted_at` IS NULL
                LEFT JOIN `categories`
                    ON `stockcodes`.`cat_id` = `categories`.`id`
                    AND `categories`.`deleted_at` IS NULL
                LEFT JOIN `products`
                    ON `stockcodes`.`prod_id` = `products`.`id`
                    AND `products`.`deleted_at` IS NULL
                LEFT JOIN `brands`
                    ON `stockcodes`.`brand_id` = `brands`.`id`
                    AND `brands`.`deleted_at` IS NULL
                LEFT JOIN `locations`
                    ON `stockcodes`.`loc_id` = `locations`.`id`
                    AND `locations`.`deleted_at` IS NULL
                INNER JOIN `cylinder_invoices`
                    ON `cylinder_invoicespdts`.`doc_no` = `cylinder_invoices`.`docno`
                    AND `cylinder_invoices`.`deleted_at` IS NULL
                WHERE cylinder_invoicespdts.deleted_at IS NULL
                UNION ALL
                SELECT
                    `delivery_notespdts`.`id` AS `id`,
                    `delivery_notespdts`.`dn_no` AS `dn_no`,
                    `delivery_notespdts`.`updated_at` AS `updated_at`,
                    `delivery_notespdts`.`account_code` AS `account_code`,
                    `delivery_notespdts`.`item_code` AS `item_code`,
                    `delivery_notespdts`.`subject` AS `subject`,
                    `delivery_notespdts`.`qty` AS `qty`,
                    `delivery_notespdts`.`uom` AS `uom`,
                    `delivery_notespdts`.`rate` AS `rate`,
                    `delivery_notespdts`.`uprice` AS `uprice`,
                    `delivery_notespdts`.`discount` AS `discount`,
                    `delivery_notespdts`.`amount` AS `amount`,
                    `delivery_notes`.`date` AS `date`,
                    `delivery_notes`.`name` AS `name`,
                    `stockcodes`.`descr` AS `descr`,
                    `stockcodes`.`ref1` AS `ref1`,
                    `stockcodes`.`ref2` AS `ref2`,
                    `stockcodes`.`model` AS `model`,
                    `stockcodes`.`weight` AS `weight`,
                    `stockcodes`.`type` AS `type`,
                    `stockcodes`.`inactive` AS `inactive`,
                    `categories`.`code` AS `category`,
                    `products`.`code` AS `product`,
                    `brands`.`code` AS `brand`,
                    `locations`.`code` AS `location`,
                    '-' AS `group`
                FROM delivery_notespdts
                LEFT JOIN `stockcodes`
                    ON `delivery_notespdts`.`item_code` = `stockcodes`.`code`
                    AND `stockcodes`.`deleted_at` IS NULL
                LEFT JOIN `categories`
                    ON `stockcodes`.`cat_id` = `categories`.`id`
                    AND `categories`.`deleted_at` IS NULL
                LEFT JOIN `products`
                    ON `stockcodes`.`prod_id` = `products`.`id`
                    AND `products`.`deleted_at` IS NULL
                LEFT JOIN `brands`
                    ON `stockcodes`.`brand_id` = `brands`.`id`
                    AND `brands`.`deleted_at` IS NULL
                LEFT JOIN `locations`
                    ON `stockcodes`.`loc_id` = `locations`.`id`
                    AND `locations`.`deleted_at` IS NULL
                INNER JOIN `delivery_notes`
                    ON `delivery_notespdts`.`dn_no` = `delivery_notes`.`dn_no`
                    AND `delivery_notes`.`deleted_at` IS NULL
                WHERE delivery_notespdts.deleted_at IS NULL
                UNION ALL
                SELECT
                    `salesreturndts`.`id` AS `id`,
                    `salesreturndts`.`doc_no` AS `doc_no`,
                    `salesreturndts`.`updated_at` AS `updated_at`,
                    `salesreturndts`.`account_code` AS `account_code`,
                    `salesreturndts`.`item_code` AS `item_code`,
                    `salesreturndts`.`subject` AS `subject`,
                    `salesreturndts`.`qty` AS `qty`,
                    `salesreturndts`.`uom` AS `uom`,
                    `salesreturndts`.`rate` AS `rate`,
                    `salesreturndts`.`uprice` AS `uprice`,
                    `salesreturndts`.`discount` AS `discount`,
                    `salesreturndts`.`amount` AS `amount`,
                    `salesreturns`.`date` AS `date`,
                    `salesreturns`.`name` AS `name`,
                    `stockcodes`.`descr` AS `descr`,
                    `stockcodes`.`ref1` AS `ref1`,
                    `stockcodes`.`ref2` AS `ref2`,
                    `stockcodes`.`model` AS `model`,
                    `stockcodes`.`weight` AS `weight`,
                    `stockcodes`.`type` AS `type`,
                    `stockcodes`.`inactive` AS `inactive`,
                    `categories`.`code` AS `category`,
                    `products`.`code` AS `product`,
                    `brands`.`code` AS `brand`,
                    `locations`.`code` AS `location`,
                    '-' AS `group`
                FROM salesreturndts
                INNER JOIN `stockcodes`
                    ON `salesreturndts`.`item_code` = `stockcodes`.`code`
                    AND `stockcodes`.`deleted_at` IS NULL
                LEFT JOIN `categories`
                    ON `stockcodes`.`cat_id` = `categories`.`id`
                    AND `categories`.`deleted_at` IS NULL
                LEFT JOIN `products`
                    ON `stockcodes`.`prod_id` = `products`.`id`
                    AND `products`.`deleted_at` IS NULL
                LEFT JOIN `brands`
                    ON `stockcodes`.`brand_id` = `brands`.`id`
                    AND `brands`.`deleted_at` IS NULL
                LEFT JOIN `locations`
                    ON `stockcodes`.`loc_id` = `locations`.`id`
                    AND `locations`.`deleted_at` IS NULL
                INNER JOIN `salesreturns`
                    ON `salesreturndts`.`doc_no` = `salesreturns`.`docno`
                    AND `salesreturns`.`deleted_at` IS NULL
                WHERE salesreturndts.deleted_at IS NULL
                UNION ALL
                SELECT
                    `quotationdts`.`id` AS `id`,
                    `quotationdts`.`doc_no` AS `doc_no`,
                    `quotationdts`.`updated_at` AS `updated_at`,
                    `quotationdts`.`account_code` AS `account_code`,
                    `quotationdts`.`item_code` AS `item_code`,
                    `quotationdts`.`subject` AS `subject`,
                    `quotationdts`.`qty` AS `qty`,
                    `quotationdts`.`uom` AS `uom`,
                    `quotationdts`.`rate` AS `rate`,
                    `quotationdts`.`uprice` AS `uprice`,
                    `quotationdts`.`discount` AS `discount`,
                    `quotationdts`.`amount` AS `amount`,
                    `cylinder_quotations`.`date` AS `date`,
                    `cylinder_quotations`.`name` AS `name`,
                    `stockcodes`.`descr` AS `descr`,
                    `stockcodes`.`ref1` AS `ref1`,
                    `stockcodes`.`ref2` AS `ref2`,
                    `stockcodes`.`model` AS `model`,
                    `stockcodes`.`weight` AS `weight`,
                    `stockcodes`.`type` AS `type`,
                    `stockcodes`.`inactive` AS `inactive`,
                    `categories`.`code` AS `category`,
                    `products`.`code` AS `product`,
                    `brands`.`code` AS `brand`,
                    `locations`.`code` AS `location`,
                    '-' AS `group`
                FROM quotationdts
                INNER JOIN `stockcodes`
                    ON `quotationdts`.`item_code` = `stockcodes`.`code`
                    AND `stockcodes`.`deleted_at` IS NULL
                LEFT JOIN `categories`
                    ON `stockcodes`.`cat_id` = `categories`.`id`
                    AND `categories`.`deleted_at` IS NULL
                LEFT JOIN `products`
                    ON `stockcodes`.`prod_id` = `products`.`id`
                    AND `products`.`deleted_at` IS NULL
                LEFT JOIN `brands`
                    ON `stockcodes`.`brand_id` = `brands`.`id`
                    AND `brands`.`deleted_at` IS NULL
                LEFT JOIN `locations`
                    ON `stockcodes`.`loc_id` = `locations`.`id`
                    AND `locations`.`deleted_at` IS NULL
                INNER JOIN `cylinder_quotations`
                    ON `quotationdts`.`doc_no` = `cylinder_quotations`.`docno`
                    AND `cylinder_quotations`.`deleted_at` IS NULL
                WHERE quotationdts.deleted_at IS NULL
                ORDER BY `date`
            ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
