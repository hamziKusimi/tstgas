<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasracks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barcode')->nullable();
            $table->string('serial')->nullable();
            $table->string('mfr')->nullable();
            $table->string('mfgterm')->nullable();
            $table->string('owner')->nullable();
            $table->string('type')->nullable();
            $table->string('maxgmass')->nullable();
            $table->string('taremass')->nullable();
            $table->string('payload')->nullable();
            $table->string('certno')->nullable();
            $table->date('testdate')->nullable();
            $table->string('status')->nullable();
            $table->string('attachment')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasracks');
    }
}
