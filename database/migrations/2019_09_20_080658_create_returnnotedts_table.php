<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnNotedtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_notedts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dn_no')->nullable();
            $table->string('sequence_no')->nullable();
            $table->string('account_code')->nullable();
            $table->string('item_code')->nullable();
            $table->string('subject')->nullable(); 
            $table->string('category')->nullable();
            $table->string('product')->nullable();
            $table->string('capacity')->nullable();
            $table->string('pressure')->nullable();
            $table->string('qty')->nullable();
            $table->string('price')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_notedts');
    }
}
