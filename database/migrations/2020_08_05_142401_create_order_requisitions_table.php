<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requisitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dn_no')->unique();
            $table->date('date')->nullable();
            $table->string('account_code')->nullable();
            $table->string('name')->nullable();
            $table->string('addr1')->nullable();
            $table->string('addr2')->nullable();
            $table->string('addr3')->nullable();
            $table->string('addr4')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('lpono')->nullable();
            $table->string('driver')->nullable();
            $table->string('charge_type')->nullable();
            $table->string('start_from')->nullable();
            $table->string('dn_type')->nullable();
            $table->text('header')->nullable();
            $table->text('footer')->nullable();
            $table->text('summary')->nullable();
            $table->string('vehicle_no')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->date('gas_invoice_date')->nullable();
            $table->text('driver_remark')->nullable();
            $table->string('project_name')->nullable();
            $table->string('dn_rack_remark')->nullable();
            $table->char('complete_do')->nullable()->default('N');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requisitions');
    }
}
