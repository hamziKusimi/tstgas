<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewReturnnotegrMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW view_new_returnnotegr_master");
        DB::statement("
            CREATE VIEW view_new_returnnotegr_master
            AS
            SELECT
                new_return_grview.dn_no AS dn_no,
                new_return_grview.dt_sqn AS dt_sqn,
                new_return_grview.list_sqn AS list_sqn,
                new_return_grview.loading_date,
                new_return_grview.unloading_date,
                new_return_grview.barcode AS barcode,
                new_return_grview.serial AS serial,
                new_return_grview.driver AS driver,
                new_return_grview.rt_dnno AS rt_dnno,
                new_return_grview.descr AS descr,
                new_return_grview.rt_dndate AS rt_dndate
            FROM
                new_return_grview
            GROUP BY dn_no, dt_sqn, list_sqn
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
