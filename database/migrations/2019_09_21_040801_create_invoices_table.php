<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('docno')->nullable();
            $table->string('date')->nullable();
            $table->date('m_duedate')->nullable();
            $table->date('refinvdate')->nullable();
            $table->string('refno')->nullable();
            $table->string('do_no')->nullable();
            $table->string('discount')->nullable();
            $table->string('amount')->nullable();
            $table->string('tax_amount')->nullable();
            $table->string('taxed_amount')->nullable();
            $table->string('account_code')->nullable();
            $table->string('name')->nullable();
            $table->string('addr1')->nullable();
            $table->string('addr2')->nullable();
            $table->string('addr3')->nullable();
            $table->string('addr4')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('credit_term')->nullable();
            $table->string('credit_limit')->nullable();
            $table->text('header')->nullable();
            $table->text('footer')->nullable();
            $table->text('summary')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('currency')->nullable();
            $table->string('exchange_rate')->nullable();
            $table->string('rounding')->nullable();
            $table->string('deleted_by')->nullable();
            $table->string('printed')->nullable(); 
            $table->string('printed_by')->nullable(); 
            $table->date('printed_at')->nullable(); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
