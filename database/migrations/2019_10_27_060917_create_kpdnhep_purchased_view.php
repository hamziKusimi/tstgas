<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpdnhepPurchasedView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE
        VIEW view_purchased_kpdnhep
            AS
            SELECT 
                'Cashbill' AS doc_type, doc_no, cashbills.date, item_code, 
                totalqty AS qty 
            FROM cashbilldts
                INNER JOIN cashbills ON cashbills.docno = cashbilldts.doc_no
            WHERE cashbilldts.deleted_at IS NULL
            UNION ALL
            SELECT 
                'Invoice' AS doc_type, a.doc_no, b.date, a.item_code,
                totalqty AS qty
            FROM invoice_data a
                INNER JOIN invoices b ON b.docno = a.doc_no
            WHERE a.deleted_at IS NULL
            UNION ALL
            SELECT 
                'Purchased' AS doc_type, doc_no, purchased_tables.date, 
                stock_code AS item_code, qty
            FROM purchased_tables
            ORDER BY DATE;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_purchased_kpdnhep");
    }
}
