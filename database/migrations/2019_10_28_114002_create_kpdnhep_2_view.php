<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpdnhep2View extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE
        VIEW view_kpdnhep_2
        AS
        SELECT
            a.doc_no, a.account_code, a.item_code, a.subject,
            totalqty AS Tquantity, a.uprice AS price,
            b.date AS doc_date, b.name AS customer
        FROM cashbilldts a
            INNER JOIN cashbills b ON b.docno = a.doc_no
            LEFT JOIN debtors c ON b.account_code = c.`accountcode`
        WHERE a.deleted_at IS NULL
		AND c.`deleted_at` IS NULL AND
		((c.`custom1` = 'License No' AND c.`customval1` IS NULL) OR c.`custom1` != 'License No')
	    UNION ALL
        SELECT
            a.doc_no, a.account_code, a.item_code, a.subject,
            totalqty AS Tquantity, a.unit_price AS price,
            b.date AS doc_date, b.name AS customer
        FROM invoice_data a
            INNER JOIN invoices b ON b.docno = a.doc_no
            LEFT JOIN debtors c ON b.account_code = c.`accountcode`
        WHERE a.deleted_at IS NULL
		AND c.`deleted_at` IS NULL AND
		((c.`custom1` = 'License No' AND c.`customval1` IS NULL) OR c.`custom1` != 'License No');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_kpdnhep_2");
    }
}
