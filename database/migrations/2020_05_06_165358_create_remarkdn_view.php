<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemarkdnView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_notedts_rmk', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_no');
            $table->string('seq');
            $table->string('cat');
            $table->string('prod');
            $table->string('rmk');
            $table->string('cyl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_notedts_rmk');
    }
}
