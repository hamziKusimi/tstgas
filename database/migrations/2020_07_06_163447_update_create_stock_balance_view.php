<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCreateStockBalanceView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // DB::statement("DROP VIEW view_stock_balance");
        // DB::statement("
        // CREATE
        //         VIEW view_stock_balance
        //             AS
        //             SELECT
        //             item_code, totalqty,
        //             goods.date AS 't_date', uom, 'GR' AS t_type,
        //             gooddts.doc_no, 'a' AS type_seq
        //     FROM gooddts
        //     LEFT JOIN goods ON goods.docno = gooddts.doc_no AND goods.deleted_at IS NULL
        //     WHERE gooddts.deleted_at IS NULL
        //     UNION ALL
        //     SELECT
        //             item_code,-1*totalqty, preturns.date AS 't_date', uom, 'PR' AS t_type,
        //             preturndts.doc_no, 'd' AS type_seq
        //     FROM preturndts
        //     LEFT JOIN preturns ON preturns.docno = preturndts.doc_no AND preturns.deleted_at IS NULL
        //     WHERE preturndts.deleted_at IS NULL
        //     UNION ALL
        //     SELECT
        //             item_code, -1*totalqty, cashbills.date AS 't_date',
        //             uom, 'CB' AS t_type, cashbilldts.doc_no, 'e' AS type_seq
        //     FROM cashbilldts
        //     LEFT JOIN cashbills ON cashbills.docno = cashbilldts.doc_no AND cashbills.deleted_at IS NULL
        //     WHERE cashbilldts.deleted_at IS NULL AND (cashbills.dono = '' OR cashbills.dono IS NULL)
        //     UNION ALL
        //     SELECT
        //             item_code, -1*totalqty, CONVERT(invoices.date, DATE) AS 't_date', uom,
        //             'INV' AS t_type, invoice_data.doc_no, 'f' AS type_seq
        //     FROM invoice_data
        //     LEFT JOIN invoices ON invoice_data.doc_no = invoices.docno  AND invoices.deleted_at IS NULL
        //     WHERE invoice_data.deleted_at IS NULL
        //             AND (invoices.do_no = '' OR invoices.do_no IS NULL)
        //     UNION ALL
        // SELECT
        //         item_code, -1*totalqty, delivery_notes.date AS 't_date',
        //         uom, 'DO' AS t_type, delivery_notespdts.dn_no, 'g' AS type_seq
        //     FROM delivery_notespdts
        //     LEFT JOIN delivery_notes ON delivery_notespdts.dn_no = delivery_notes.dn_no AND delivery_notes.deleted_at IS NULL
        //     WHERE
        //         delivery_notespdts.deleted_at IS NULL
        //     UNION ALL
        //     SELECT
        //         item_code, totalqty, salesreturns.date AS 't_date',
        //         uom, 'SR' AS t_type, salesreturndts.doc_no, 'b' AS type_seq
        //     FROM salesreturndts
        //     LEFT JOIN salesreturns ON salesreturndts.doc_no = salesreturns.docno AND salesreturns.deleted_at IS NULL
        //     WHERE salesreturndts.deleted_at IS NULL
        //     UNION ALL
        //     SELECT
        //         item_code, totalqty, adjustmentis.date AS 't_date', uom,
        //         'AIN' AS t_type, adjustmentidts.doc_no, 'c' AS type_seq
        //     FROM adjustmentidts
        //     LEFT JOIN adjustmentis ON adjustmentidts.doc_no = adjustmentis.docno AND adjustmentis.deleted_at IS NULL
        //     WHERE adjustmentidts.deleted_at IS NULL AND adjustmentis.deleted_at IS NULL
        //     UNION ALL
        //     SELECT
        //         item_code, -1*totalqty, adjustmentos.date AS 't_date',
        //         uom, 'AOUT' AS t_type, adjustmentodts.doc_no, 'h' AS type_seq
        //     FROM adjustmentodts
        //         LEFT JOIN adjustmentos ON adjustmentodts.doc_no = adjustmentos.docno AND adjustmentos.deleted_at IS NULL
        //     WHERE adjustmentodts.deleted_at IS NULL AND adjustmentos.deleted_at IS NULL;
        // ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
