<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_setups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('company_no')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('address4')->nullable();
            $table->string('tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('name1')->nullable();
            $table->string('name2')->nullable();
            $table->string('gstno')->nullable();
            $table->string('salestaxno')->nullable();
            $table->string('servtaxno')->nullable();
            $table->tinyInteger('use_tax')->nullable();
            $table->tinyInteger('uom1')->nullable();
            $table->tinyInteger('uom2')->nullable();
            $table->tinyInteger('uom3')->nullable();
            $table->tinyInteger('updoc_gasrack')->nullable();
            $table->string('free_cy')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('custom1')->nullable();
            $table->string('custom2')->nullable();
            $table->string('custom3')->nullable();
            $table->string('custom4')->nullable();
            $table->string('custom5')->nullable();
            $table->string('price1')->nullable();
            $table->string('price2')->nullable();
            $table->string('price3')->nullable();
            $table->string('price4')->nullable();
            $table->string('price5')->nullable();
            $table->string('price6')->nullable();
            $table->tinyInteger('warning_min')->nullable();
            $table->tinyInteger('req_pass')->nullable();
            $table->string('password_min')->nullable();
            $table->timestamps();
            $table->Integer('task_scheduler')->nullable();
            $table->string('bank_detail')->nullable();
            $table->string('bank_account_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_setups');
    }
}
