<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReturnNotePrintRackView2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_return_note_print_rack");
        DB::statement("
        CREATE
        VIEW view_return_note_print_rack
        AS
        SELECT IFNULL(gasracks.description, 'Undefined') AS description, return_notells.dn_no AS dn_no, return_notells.`type`, return_notells.id AS id,
            IFNULL(return_notells.barcode, 'Undefined') AS barcode, IFNULL(gasracks.serial, 'Undefined') AS SERIAL,
            IFNULL(gasracks.type, 'Undefined') AS rack_type
        FROM return_notells
        LEFT JOIN gasracks ON return_notells.barcode = gasracks.barcode OR gasracks.serial = return_notells.serial
        WHERE return_notells.type = 'gr' AND return_notells.`deleted_at` IS NULL; ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
