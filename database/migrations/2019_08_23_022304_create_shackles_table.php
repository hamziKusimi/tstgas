<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShacklesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shackles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barcode')->nullable();
            $table->string('mfr')->nullable();
            $table->string('serial')->nullable();
            $table->string('owner')->nullable();
            $table->string('loadlimit')->nullable();
            $table->string('type')->nullable();
            $table->string('size')->nullable();
            $table->string('descr')->nullable();
            $table->string('coc')->nullable();
            $table->date('testdate')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shackles');
    }
}
