<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoiceProcessingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_cylinder_process_inv");
        DB::statement("
       CREATE VIEW view_cylinder_process_inv AS
        SELECT a.*, b.`account_code` AS debtor, b.`gas_price`, b.`qty`, b.`product` AS product, b.`category` AS category,
        b.`daily_price`, b.`monthly_price` , b.`bill_date`, d.`descr`
        FROM delivery_noteprs a
        INNER JOIN delivery_notedts b ON a.`dn_no` =  b.`dn_no`
            AND a.`sequence_no` = b.`sequence_no`
        INNER JOIN delivery_notes c ON b.`dn_no` =  c.`dn_no`
        LEFT JOIN cylinders d ON a.`barcode` =  d.`barcode`
        WHERE a.deleted_at IS NULL AND b.deleted_at IS NULL AND c.deleted_at IS NULL ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
