<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalesmansToCbDoIvSrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cashbills', function (Blueprint $table) {
            
            $table->string('salesman')->nullable();
        });

        Schema::table('invoices', function (Blueprint $table) {
            
            $table->string('salesman')->nullable();
        });

        Schema::table('deliveryorders', function (Blueprint $table) {
            
            $table->string('salesman')->nullable();
        });

        Schema::table('salesreturns', function (Blueprint $table) {
            
            $table->string('salesman')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cashbills', function (Blueprint $table) {
            //
        });
    }
}
