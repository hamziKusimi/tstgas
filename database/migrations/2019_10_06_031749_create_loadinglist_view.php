<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadinglistView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //DB::statement("DROP VIEW view_loadinglist");
        DB::statement("
            CREATE VIEW view_loadinglist
            AS
            SELECT
                delivery_notells.id,
                delivery_notells.dn_no,
                delivery_notells.sequence_no AS dt_sqn,
                delivery_notells.sqn_no AS list_sqn,
                delivery_notells.barcode,
                delivery_notells.serial,
                delivery_notells.datetime AS loading_date,
                delivery_noteuls.datetime AS unloading_date

            FROM delivery_notells
            LEFT JOIN delivery_noteuls ON delivery_notells.barcode = delivery_noteuls.barcode
            GROUP BY dn_no, dt_sqn, list_sqn
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement("DROP VIEW view_loadinglist");
    }
}
