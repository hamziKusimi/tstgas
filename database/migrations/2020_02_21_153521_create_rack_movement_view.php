<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRackMovementView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW `view_rack_movement` AS
        SELECT
            delivery_notes.dn_no AS doc_no,'DN' AS doc_type, delivery_notes.account_code AS debtor,
            IFNULL(delivery_noteprs.serial , '-') AS s_no, delivery_notes.date AS cdate
            FROM delivery_notes
            INNER JOIN delivery_note_grdts ON  delivery_note_grdts.dn_no =  delivery_notes.dn_no
            INNER JOIN delivery_noteprs ON  (delivery_noteprs.dn_no =  delivery_notes.dn_no
            AND delivery_noteprs.sequence_no = delivery_note_grdts.sequence_no)
            LEFT JOIN gasracks ON delivery_noteprs.`serial` = gasracks.`serial`
        WHERE delivery_note_grdts.deleted_at IS NULL AND delivery_noteprs.deleted_at IS NULL
            AND delivery_notes.deleted_at IS NULL AND delivery_noteprs.type = 'gr'
        UNION ALL
        SELECT
            return_notes.dn_no AS doc_no, 'RN' AS doc_type, return_notes.account_code AS debtor,
            IFNULL(return_notells.serial , '-') AS s_no,
            return_notes.date AS cdate
        FROM return_notes
            INNER JOIN return_note_grdts ON  return_note_grdts.dn_no =  return_notes.dn_no
            INNER JOIN return_notells ON return_notells.dn_no = return_note_grdts.dn_no AND return_notells.sequence_no = return_note_grdts.sequence_no
            LEFT JOIN gasracks ON return_notells.`serial` = gasracks.`serial`
        WHERE
            return_note_grdts.deleted_at IS NULL
            AND return_notells.deleted_at IS NULL AND return_notes.deleted_at IS NULL
            AND return_notells.type = 'gr';
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_rack_movement");
    }
}
