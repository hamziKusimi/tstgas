<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGasracksProcessingView2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
                //
                DB::statement("DROP VIEW view_gasrack_process_inv");
                DB::statement("
                CREATE VIEW view_gasrack_process_inv AS
                SELECT a.*, b.`account_code` AS debtor,  b.`qty`,
                b.`daily_price`, b.`monthly_price` , b.`bill_date`, d.`description`
                FROM delivery_noteprs a
                INNER JOIN delivery_note_grdts b ON a.`dn_no` =  b.`dn_no`
                    AND a.`sequence_no` = b.`sequence_no`
                LEFT JOIN gasracks d ON a.`barcode` =  d.`barcode`
                WHERE a.deleted_at IS NULL AND b.deleted_at IS NULL AND a.`type` = 'gr';
                ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
