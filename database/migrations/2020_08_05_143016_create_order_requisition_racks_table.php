<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequisitionRacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requisition_racks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dn_no');
            $table->string('rack_no');
            $table->string('sling_no')->nullable();
            $table->string('gwl')->nullable();
            $table->date('next_inspect_date')->nullable();
            $table->date('re_test_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requisition_racks');
    }
}
