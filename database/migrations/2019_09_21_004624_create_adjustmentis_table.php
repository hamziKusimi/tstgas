<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustmentisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjustmentis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('docno');
            $table->date('date')->nullable(); 
            $table->string('discount')->nullable(); 
            $table->string('amount')->nullable(); 
            $table->string('tax_amount')->nullable(); 
            $table->string('taxed_amount')->nullable(); 
            $table->text('header')->nullable();
            $table->text('footer')->nullable();
            $table->text('summary')->nullable();
            $table->string('created_by')->nullable(); 
            $table->string('updated_by')->nullable(); 
            $table->string('deleted_by')->nullable(); 
            $table->string('printed')->nullable(); 
            $table->string('printed_by')->nullable(); 
            $table->date('printed_at')->nullable(); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjustmentis');
    }
}
