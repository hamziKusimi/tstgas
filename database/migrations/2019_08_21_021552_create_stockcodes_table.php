<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockcodes', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('code');
            $table->string('descr')->nullable();
            $table->tinyInteger('edit')->nullable();
            $table->tinyInteger('inactive')->nullable();
            $table->string('ref1')->nullable();
            $table->string('ref2')->nullable();
            $table->string('model')->nullable();
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('categories');
            $table->string('type')->nullable();
            $table->integer('prod_id')->unsigned();
            $table->foreign('prod_id')->references('id')->on('products');
            $table->string('weight')->nullable();
            $table->integer('brand_id')->unsigned();
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->integer('loc_id')->unsigned();
            $table->foreign('loc_id')->references('id')->on('locations');
            $table->float('minstkqty')->nullable();
            $table->float('maxstkqty')->nullable();
            $table->integer('uom_id1')->unsigned();
            $table->foreign('uom_id1')->references('id')->on('uoms');
            $table->integer('uom_id2')->nullable();
            $table->integer('uom_id3')->nullable();
            $table->string('stkbal')->nullable();
            $table->string('laststkch')->nullable();
            $table->float('volume')->nullable();
            $table->float('currency')->nullable();
            $table->float('uomrate2')->nullable();
            $table->float('uomrate3')->nullable();
            $table->float('unitcost')->nullable();
            $table->float('prevcost')->nullable();
            $table->float('avecost')->nullable();
            $table->float('curcost')->nullable();
            $table->float('minprice')->nullable();
            $table->float('price1')->nullable();
            $table->float('price2')->nullable();
            $table->float('price3')->nullable();
            $table->float('price4')->nullable();
            $table->float('price5')->nullable();
            $table->float('price6')->nullable();
            $table->float('unitcost1')->nullable();
            $table->float('unitcost2')->nullable();
            $table->float('unitcost3')->nullable();
            $table->float('unitcost4')->nullable();
            $table->float('unitcost5')->nullable();
            $table->float('unitcost6')->nullable();
            $table->float('unitcost7')->nullable();
            $table->text('remark')->nullable();
            $table->text('memo')->nullable();
            $table->binary('image')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockcodes');
    }
}
