<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2ViewToCylindermaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW view_cylinder_master");
        DB::statement("
        CREATE
        VIEW view_cylinder_master
        AS
        SELECT
            a.id, a.barcode, a.serial, a.cat_id, a.group_id, a.type_id, a.prod_id, a.descr,
            b.code AS own, c.code AS htype,  d.code AS manu, material, oriowner ,
            oritareweight, valveguard,
            valveoutlet, testpressure, e.code AS vtype, valvemfr,
            testdate, mfgdate, f.code AS ctype,
            g.code AS category, h.code AS product, i.code AS cgroup, j.`cdate` AS last_activity,
            a.status AS status
        FROM cylinders a
        LEFT JOIN ownerships b ON a.`owner` = b.`id` AND b.deleted_at IS NULL
        LEFT JOIN cylinderhandwheeltypes c ON a.`hwtype` = c.`id` AND c.deleted_at IS NULL
        LEFT JOIN manufacturers d ON a.`mfr` = d.`id` AND d.deleted_at IS NULL
        LEFT JOIN cylindervalvetypes e ON a.`valvetype` = e.`id` AND e.deleted_at IS NULL
        LEFT JOIN cylindertypes f ON a.`type_id` = f.`id` AND f.deleted_at IS NULL
        LEFT JOIN cylindercategories g ON a.`cat_id` = g.`id` AND g.deleted_at IS NULL
        LEFT JOIN cylinderproducts h ON a.`prod_id` = h.`id` AND h.deleted_at IS NULL
        LEFT JOIN cylindergroups i ON a.`group_id` = i.`id` AND i.deleted_at IS NULL
        LEFT JOIN view_cylinder_customer_activity j ON a.`serial` = j.`s_no`
        WHERE a.deleted_at IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
