<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDeliveryNoteprintRackView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_delivery_note_print_rack");
        DB::statement("
            CREATE
            VIEW `view_delivery_note_print_rack`
            AS
            SELECT delivery_noteprs.`dn_no`, delivery_noteprs.`serial` AS 'serial', gasracktypes.code AS 'type', gasracks.`testdate` AS 'testdate'
            FROM delivery_noteprs
            LEFT JOIN gasracks ON delivery_noteprs.serial = gasracks.serial AND gasracks.`deleted_at` IS NULL
            LEFT JOIN gasracktypes ON gasracktypes.`id` = gasracks.`type` AND gasracktypes.`deleted_at` IS NULL
            WHERE delivery_noteprs.`deleted_at` IS NULL AND gasracks.`deleted_at`IS NULL
                AND gasracktypes.`deleted_at` IS NULL
                AND delivery_noteprs.`type` = 'gr';
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
