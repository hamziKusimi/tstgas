<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewGrview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW `new_grview` AS
        SELECT
            `delivery_noteprs`.`id` AS `id`,
            `delivery_noteprs`.`dn_no` AS `dn_no`,
            `delivery_noteprs`.`sequence_no` AS `dt_sqn`,
            `delivery_noteprs`.`sqn_no` AS `list_sqn`,
            `delivery_noteprs`.`barcode` AS `barcode`,
            `delivery_noteprs`.`serial` AS `serial`,
            `delivery_noteprs`.`driver` AS `driver`,
            `delivery_noteprs`.`type` AS `type`,
            `delivery_noteprs`.`datetime` AS `prepare_date`,
            `delivery_notells`.`datetime` AS `loading_date`,
            `delivery_noteuls`.`datetime` AS `unloading_date`
         FROM
        `delivery_noteprs`
            LEFT JOIN `delivery_notells` ON `delivery_noteprs`.`barcode` = `delivery_notells`.`barcode`
            AND `delivery_noteprs`.`dn_no` = `delivery_notells`.`dn_no`
            AND `delivery_noteprs`.`sqn_no` = `delivery_notells`.`sqn_no`
            AND `delivery_noteprs`.`sequence_no` = `delivery_notells`.`sequence_no`
            AND `delivery_notells`.`deleted_at` IS NULL
            LEFT JOIN `delivery_noteuls` ON `delivery_noteprs`.`barcode` = `delivery_noteuls`.`barcode`
            AND `delivery_noteprs`.`dn_no` = `delivery_noteuls`.`dn_no`
            AND `delivery_noteprs`.`sqn_no` = `delivery_noteuls`.`sqn_no`
            AND `delivery_noteprs`.`sequence_no` = `delivery_noteuls`.`sequence_no`
            AND `delivery_noteuls`.`deleted_at` IS NULL
            WHERE ISNULL(`delivery_noteprs`.`deleted_at`)
               AND `delivery_noteprs`.`type` = 'gr'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW new_grview");
    }
}
