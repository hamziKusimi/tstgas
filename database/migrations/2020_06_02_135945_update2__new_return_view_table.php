<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2NewReturnViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW new_return_view");
        DB::statement("
        CREATE VIEW `new_return_view` AS
            SELECT
            `return_notells`.`id` AS `id`,
            `return_notells`.`dn_no` AS `dn_no`,
            `return_notells`.`sequence_no` AS `dt_sqn`,
            `return_notells`.`sqn_no` AS `list_sqn`,
            `return_notells`.`barcode` AS `barcode`,
            `return_notells`.`serial` AS `serial`,
            `return_notells`.`driver` AS `driver`,
            `return_notells`.`descr` AS `descr`,
            `return_notells`.`datetime` AS `loading_date`,
            `return_noteuls`.`datetime` AS `unloading_date`,
            `delivery_noteprs`.`dn_no` AS `rt_dnno`,
            `delivery_notes`.`date` AS `rt_dndate`,
            `return_notells`.`updated_at` AS `updated_at`
            FROM
                `return_notells`
            LEFT JOIN `return_noteuls` ON ((`return_notells`.`barcode` = `return_noteuls`.`barcode`)
                AND (`return_noteuls`.`dn_no` = `return_notells`.`dn_no`)
                AND ISNULL(`return_noteuls`.`deleted_at`))
            LEFT JOIN `delivery_noteprs` ON ((`delivery_noteprs`.`return_note` = `return_notells`.`dn_no`)
                AND (`delivery_noteprs`.`barcode` = `return_notells`.`barcode`))
            LEFT JOIN `delivery_notes` ON (`delivery_notes`.`dn_no` = `delivery_noteprs`.`dn_no`)
            WHERE ISNULL(`delivery_noteprs`.`deleted_at`) AND ISNULL(`return_notells`.`deleted_at`)
                AND `return_notells`.`type` = 'cy';
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
