<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpdnhepPurchasedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchased_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_no')->nullable();
            $table->string('date')->nullable();
            $table->string('stock_code')->nullable();
            $table->string('qty')->nullable();
            $table->string('price')->nullable();
            $table->string('amount')->nullable(); 
            $table->string('max_quota')->nullable(); 
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchased_tables');
    }
}
