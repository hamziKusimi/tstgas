<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToCylindercategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylindercategories', function (Blueprint $table) {
            //
            $table->string('dr_cashpurchase_acc')->nullable();
            $table->string('dr_creditpurcase_acc')->nullable();
            $table->string('cr_purchasereturn_acc')->nullable();
            $table->string('cr_cashsales_acc')->nullable();
            $table->string('dr_cashsales_return_acc')->nullable();
            $table->string('cr_invoicesales_acc')->nullable();
            $table->string('dr_creditsales_return_acc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylindercategories', function (Blueprint $table) {
            //
        });
    }
}
