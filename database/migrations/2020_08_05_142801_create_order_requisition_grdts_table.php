<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequisitionGrdtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requisition_grdts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dn_no')->nullable();
            $table->string('sequence_no')->nullable();
            $table->string('account_code')->nullable();
            $table->string('item_code')->nullable();
            $table->string('type')->nullable();
            $table->string('qty')->nullable();
            $table->string('daily_price')->nullable();
            $table->string('monthly_price')->nullable();
            $table->date('bill_date')->nullable();
            $table->string('remarks')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requisition_grdts');
    }
}
