<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockInView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS view_stock_in');
        DB::statement("
        CREATE
        VIEW view_stock_in
        AS
        SELECT
            item_code, totalqty, ucost,  goods.date AS 't_date', uom,'GR' AS t_type,
            gooddts.doc_no, 'A' AS type_seq
        FROM gooddts
        INNER JOIN goods ON goods.docno = gooddts.doc_no
        WHERE gooddts.deleted_at IS NULL
        UNION ALL
        SELECT
            item_code, totalqty, ucost, salesreturns.date AS 't_date',
            uom,'SR' AS t_type,salesreturndts.doc_no, 'B' AS type_seq
        FROM salesreturndts
            INNER JOIN salesreturns ON salesreturndts.doc_no = salesreturns.docno
        WHERE salesreturndts.deleted_at IS NULL
        UNION ALL
        SELECT
            item_code, totalqty, ucost, adjustmentis.date AS 't_date', uom,'AIN' AS t_type,
            adjustmentidts.doc_no, 'C' AS type_seq
        FROM adjustmentidts
        INNER JOIN adjustmentis ON adjustmentidts.doc_no = adjustmentis.docno
        WHERE adjustmentidts.deleted_at IS NULL AND adjustmentis.deleted_at IS NULL;
        ");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement('DROP VIEW IF EXISTS view_stock_in');
    }
}
