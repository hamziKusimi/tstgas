<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnNoteManifoldViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::statement("DROP VIEW return_note_manifold_view");
        // DB::statement("
        //     CREATE
        //     VIEW `view_return_note_manifold`
        //     AS
        //     SELECT return_notells.dn_no, return_notells.barcode, gasrackcylinders.cy_serial AS cy_serial, gasracks.`serial` AS gr_serial, gasracks.description AS descr
        //     FROM return_notells
        //     INNER JOIN gasracks ON gasracks.`serial` = return_notells.`serial`
        //     LEFT JOIN  gasrackcylinders ON gasrackcylinders.`gr_id` = gasracks.`id`
        //     WHERE return_notells.`deleted_at` IS NULL AND gasrackcylinders.`deleted_at` IS NULL;
        // ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement("DROP VIEW return_note_manifold_view");
    }
}
