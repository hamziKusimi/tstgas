<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylinderHoldingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::statement("DROP VIEW view_cylinder_holding");
        // DB::statement("
        // CREATE
        //     /*[ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
        //     [DEFINER = { user | CURRENT_USER }]
        //     [SQL SECURITY { DEFINER | INVOKER }]*/
        //     VIEW view_cylinder_holding
        //     AS
        //     SELECT
        //         delivery_notedts.dn_no AS doc_no,'DN' AS doc_type,
        //         delivery_notes.account_code AS debtor, IFNULL(delivery_noteuls.serial , '-') AS s_no,
        //         delivery_notes.date AS cdate, product, lpono
        //     FROM delivery_notes
        //         INNER JOIN delivery_notedts ON  delivery_notedts.dn_no =  delivery_notes.dn_no
        //         INNER JOIN delivery_noteuls ON delivery_noteuls.dn_no = delivery_notedts.dn_no
        //         AND delivery_noteuls.sequence_no = delivery_notedts.sequence_no
        //     WHERE
        //         delivery_notedts.deleted_at IS NULL
        //         AND delivery_noteuls.deleted_at IS NULL AND delivery_notes.deleted_at IS NULL
        //         AND delivery_noteuls.type = 'cy'
        //     UNION
        //     SELECT
        //         return_notedts.dn_no AS doc_no, 'RN' AS doc_type,
        //         return_notes.account_code AS debtor, IFNULL(return_noteuls.serial , '-') AS s_no,
        //         return_notes.date AS cdate, product, lpono
        //     FROM return_notes
        //         INNER JOIN return_notedts ON  return_notedts.dn_no =  return_notes.dn_no
        //         INNER JOIN return_noteuls ON return_noteuls.dn_no = return_notedts.dn_no
        //         AND return_noteuls.sequence_no = return_notedts.sequence_no
        //     WHERE
        //         return_notedts.deleted_at IS NULL AND return_noteuls.deleted_at IS NULL
        //         AND return_notes.deleted_at IS NULL AND return_noteuls.type = 'cy';
        // ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
