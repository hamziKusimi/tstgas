<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostedToCylinderCashbills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_cashbills', function (Blueprint $table) {
            //posted
            $table->char('posted')->nullable()->default('N');
        });
        Schema::table('cylinder_invoices', function (Blueprint $table) {
            $table->char('posted')->nullable()->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_cashbills', function (Blueprint $table) {
            //
        });
    }
}
