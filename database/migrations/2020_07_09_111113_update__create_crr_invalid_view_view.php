<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCreateCrrInvalidViewView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_crr_invalid");
        DB::statement("
        CREATE VIEW `view_crr_invalid` AS
            SELECT
                    `return_notells`.`id` AS `id`,
                    `return_notells`.`dn_no` AS `rn_no`,
                    `return_notells`.`sequence_no` AS `dt_sqn`,
                    `return_notells`.`sqn_no` AS `list_sqn`,
                    `return_notells`.`barcode` AS `barcode`,
                    `return_notells`.`serial` AS `serial`,
                    `return_notells`.`driver` AS `driver`,
                    `return_notells`.`descr` AS `descr`,
                    `return_notells`.`datetime` AS `loading_date`,
                    `return_noteuls`.`datetime` AS `unloading_date`,
                    `delivery_noteprs`.`dn_no` AS `rt_dnno`,
                    `delivery_notes`.`date` AS `rt_dndate`,
                    return_notes.`date` AS crr_date,
                    return_notes.`account_code` AS rn_debtor,
                    return_notes.`name` AS `rn_name`,
                    delivery_notes.`account_code` AS dn_debtor,
                    delivery_notes.`name` AS `dn_name`,
                    CASE
                WHEN ISNULL(`delivery_noteprs`.`dn_no`) OR return_notes.`account_code` != delivery_notes.`account_code` THEN 'true'
                END AS `invalid`,
                CASE
                WHEN return_notes.`account_code` != delivery_notes.`account_code` THEN 'true'
                END AS `invalid_debtor`,
                CASE
                WHEN ISNULL(`cylinders`.`serial`) THEN 'true'
	            END AS `invalid_cylinder`
            FROM
                `return_notells`
            LEFT JOIN `return_noteuls` ON ((`return_notells`.`serial` = `return_noteuls`.`serial`)
                AND (`return_noteuls`.`dn_no` = `return_notells`.`dn_no`)
                AND ISNULL(`return_noteuls`.`deleted_at`))
            LEFT JOIN `delivery_noteprs` ON (`delivery_noteprs`.`return_note` = `return_notells`.`dn_no`
			    AND `delivery_noteprs`.`serial` = `return_notells`.`serial` AND ISNULL(`delivery_noteprs`.`deleted_at`) )
            LEFT JOIN `delivery_notes` ON (`delivery_notes`.`dn_no` = `delivery_noteprs`.`dn_no`)
            LEFT JOIN `cylinders` ON `cylinders`.`serial` = `return_notells`.`serial` OR cylinders.`barcode` = `return_notells`.`barcode`
            LEFT JOIN return_notes ON return_notells.`dn_no` = return_notes.`dn_no`
                        AND return_notes.deleted_at IS NULL
            WHERE ISNULL(`return_notells`.`deleted_at`) AND `return_notells`.`type` = 'cy';
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
