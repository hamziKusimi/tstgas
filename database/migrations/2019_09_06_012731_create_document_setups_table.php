<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_setups', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('D_NAME');
            $table->string('D_PREFIX')->nullable();
            $table->string('D_SEPARATOR')->nullable()->default('/');
            $table->integer('D_LAST_NO')->default(0);
            $table->integer('D_ZEROES')->default(4);
            $table->boolean('D_ACTIVE')->default(true);
            $table->string('D_KEYUSER')->nullable();
            $table->datetime('D_MODIUSER')->nullable();
            $table->string('D_KEYDATE')->nullable();
            $table->datetime('D_MODIDATE')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_setups');
    }
}
