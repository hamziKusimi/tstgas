<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCylinderHoldingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // DB::statement("DROP VIEW view_cylinder_holding");
    //     DB::statement("
    //     CREATE
    //     VIEW view_cylinder_holding
    //     AS
    //     SELECT
    //     delivery_notedts.dn_no AS doc_no,'DN' AS doc_type,
    //     delivery_notes.account_code AS debtor, IFNULL(delivery_noteprs.serial , '-') AS s_no,
    //     delivery_notes.date AS cdate, product, lpono
    // FROM delivery_notes
    //     INNER JOIN delivery_notedts ON  delivery_notedts.dn_no =  delivery_notes.dn_no
    //     INNER JOIN delivery_noteprs ON delivery_noteprs.dn_no = delivery_notedts.dn_no
    //     AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no
    // WHERE
    //     delivery_notedts.deleted_at IS NULL
    //     AND delivery_noteprs.deleted_at IS NULL AND delivery_notes.deleted_at IS NULL
    //     AND delivery_noteprs.type = 'cy'
    // UNION ALL
    // SELECT
    //     return_notedts.dn_no AS doc_no, 'RN' AS doc_type,
    //     return_notes.account_code AS debtor, IFNULL(return_notells.serial , '-') AS s_no,
    //     return_notes.date AS cdate, cylinderproducts.`code` AS product, lpono
    // FROM return_notes
    //     INNER JOIN return_notedts ON  return_notedts.dn_no =  return_notes.dn_no
    //     INNER JOIN return_notells ON return_notells.dn_no = return_notedts.dn_no
    //     AND return_notells.sequence_no = return_notedts.sequence_no
    //     LEFT JOIN cylinders ON return_notells.serial = cylinders.`serial`
    //     INNER JOIN cylinderproducts ON cylinders.`prod_id` = cylinderproducts.`id`
    // WHERE
    //     return_notedts.deleted_at IS NULL AND return_notells.deleted_at IS NULL
    //     AND return_notes.deleted_at IS NULL AND return_notells.type = 'cy';
    //     ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
