<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2CylinderMovementNewView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_cylinder_movement");
        DB::statement("

        CREATE
        VIEW view_cylinder_movement
        AS
        SELECT
            delivery_notes.dn_no AS doc_no,'DN' AS doc_type, delivery_notes.account_code AS debtor,
            IFNULL(delivery_noteprs.serial , '-') AS s_no, delivery_notes.date AS cdate, delivery_notedts.category AS category,
            delivery_notedts.product AS product, cylindertypes.code AS TYPE, cylindergroups.code AS groups,
            '-' AS remark
            FROM delivery_notes
            INNER JOIN delivery_notedts ON  delivery_notedts.dn_no =  delivery_notes.dn_no
            INNER JOIN delivery_noteprs ON  (delivery_noteprs.dn_no =  delivery_notes.dn_no
            AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no)
            LEFT JOIN cylinders ON delivery_noteprs.`serial` = cylinders.`serial`
            INNER JOIN cylindertypes ON cylinders.`type_id` = cylindertypes.`id`
            INNER JOIN cylindergroups ON cylinders.`group_id` = cylindergroups.`id`
        WHERE delivery_notedts.deleted_at IS NULL AND delivery_noteprs.deleted_at IS NULL
            AND delivery_notes.deleted_at IS NULL AND delivery_noteprs.type = 'cy'
            UNION ALL
        SELECT
            return_notes.dn_no AS doc_no, 'RN' AS doc_type, return_notes.account_code AS debtor,
            IFNULL(return_notells.serial , '-') AS s_no,
            return_notes.date AS cdate, cylindercategories.code AS category, cylinderproducts.code AS product,
            cylindertypes.code AS TYPE, cylindergroups.code AS groups,
            CASE
		WHEN ISNULL(`delivery_noteprs`.`dn_no`) AND `cylinders`.`serial` IS NOT NULL THEN 'No Matching DO'
		WHEN return_notes.`account_code` != delivery_notes.`account_code` THEN 'Invalid CRR-Debtor'
		WHEN ISNULL(`cylinders`.`serial`) THEN 'Invalid CRR-Cylinder No.'
		END AS `remark`
        FROM return_notes
            INNER JOIN return_notedts ON return_notedts.dn_no =  return_notes.dn_no
            INNER JOIN return_notells ON (return_notells.dn_no = return_notedts.dn_no
		AND return_notells.sequence_no = return_notedts.sequence_no)
            LEFT JOIN delivery_noteprs ON delivery_noteprs.`serial` = return_notells.`serial`
		AND delivery_noteprs.`return_note` = return_notells.`dn_no`
	    LEFT JOIN `delivery_notes` ON (`delivery_notes`.`dn_no` = `delivery_noteprs`.`dn_no`)
            LEFT JOIN cylinders ON return_notells.`serial` = cylinders.`serial`
            INNER JOIN cylinderproducts ON cylinders.`prod_id` = cylinderproducts.`id`
            INNER JOIN cylindercategories ON cylinders.`cat_id` = cylindercategories.`id`
            INNER JOIN cylindertypes ON cylinders.`type_id` = cylindertypes.`id`
            INNER JOIN cylindergroups ON cylinders.`group_id` = cylindergroups.`id`
        WHERE
            return_notedts.deleted_at IS NULL
            AND return_notedts.sequence_no <> '2'
            AND return_notells.deleted_at IS NULL AND return_notes.deleted_at IS NULL
            AND return_notells.type = 'cy'
        UNION ALL
        SELECT
            wo_no AS doc_no, 'Refill' AS doc_type , '-' AS debtor,
            refills.serial AS s_no, DATETIME AS cdate, cylindercategories.code AS category,
        cylinderproducts.code AS product, cylindertypes.code AS TYPE, cylindergroups.code AS groups,'-' AS remark
        FROM refills
            INNER JOIN cylinders ON cylinders.serial = refills.serial
            INNER JOIN cylinderproducts ON cylinders.prod_id = cylinderproducts.id
            INNER JOIN cylindercategories ON cylinders.`cat_id` = cylindercategories.`id`
            INNER JOIN cylindertypes ON cylinders.`type_id` = cylindertypes.`id`
            INNER JOIN cylindergroups ON cylinders.`group_id` = cylindergroups.`id`
        WHERE refills.deleted_at IS NULL
        UNION ALL
        SELECT
            rp_no AS doc_no, 'Repair' AS doc_type , '-' AS debtor,
            repairs.serial AS s_no, logindate AS cdate, cylindercategories.code AS category, cylinderproducts.code AS product,
            cylindertypes.code AS TYPE, cylindergroups.code AS groups,'-' AS remark
        FROM repairs
            INNER JOIN cylinders ON cylinders.serial = repairs.serial
            INNER JOIN cylinderproducts ON cylinders.prod_id = cylinderproducts.id
            INNER JOIN cylindercategories ON cylinders.`cat_id` = cylindercategories.`id`
            INNER JOIN cylindertypes ON cylinders.`type_id` = cylindertypes.`id`
            INNER JOIN cylindergroups ON cylinders.`group_id` = cylindergroups.`id`
        WHERE repairs.deleted_at IS NULL
        UNION ALL
        SELECT
            cylinder_invoices.docno AS doc_no, 'IV' AS doc_type, cylinder_invoices.account_code AS debtor,
            IFNULL(cylinder_invoicedts.serial , '-') AS s_no,
            cylinder_invoices.date AS cdate, cylindercategories.code AS category, cylinderproducts.code AS product,
            cylindertypes.code AS TYPE, cylindergroups.code AS groups,'-' AS remark
        FROM cylinder_invoices
            INNER JOIN cylinder_invoicedts ON  cylinder_invoicedts.doc_no =  cylinder_invoices.docno
            LEFT JOIN cylinders ON cylinder_invoicedts.`serial` = cylinders.`serial`
            INNER JOIN cylinderproducts ON cylinders.`prod_id` = cylinderproducts.`id`
            INNER JOIN cylindercategories ON cylinders.`cat_id` = cylindercategories.`id`
            INNER JOIN cylindertypes ON cylinders.`type_id` = cylindertypes.`id`
            INNER JOIN cylindergroups ON cylinders.`group_id` = cylindergroups.`id`
        WHERE
            cylinder_invoicedts.deleted_at IS NULL
            AND cylinder_invoices.deleted_at IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
