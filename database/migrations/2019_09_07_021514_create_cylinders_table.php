<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cylinders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barcode')->nullable();
            $table->string('serial')->nullable();
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('cylindercategories')->onDelete("cascade");
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('cylindergroups')->onDelete("cascade");
            $table->integer('type_id')->nullable();
            $table->integer('prod_id')->unsigned();
            $table->foreign('prod_id')->references('id')->on('cylinderproducts')->onDelete("cascade");
            $table->string('descr')->nullable();
            $table->string('owner')->nullable();
            $table->string('mass')->nullable();
            $table->string('approval')->nullable();
            $table->string('approved')->nullable();
            $table->string('capacity')->nullable();
            $table->string('conttype')->nullable();
            $table->string('hwtype')->nullable();
            $table->date('testdate')->nullable();
            $table->date('mfgdate')->nullable();
            $table->string('mfr')->nullable();
            $table->string('material')->nullable();
            $table->string('neckcode')->nullable();
            $table->string('oriowner')->nullable();
            $table->string('oritareweight')->nullable();
            $table->string('ramplot')->nullable();
            $table->string('solvent')->nullable();
            $table->string('valveguard')->nullable();
            $table->string('valveoutlet')->nullable();
            $table->string('testpressure')->nullable();
            $table->string('valvetype')->nullable();
            $table->string('valvemfr')->nullable();
            $table->string('status')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cylinders');
    }
}
