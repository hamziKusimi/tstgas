<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReportBybillView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE
        VIEW view_sales_report_bybill
        AS
        SELECT docno, DATE, account_code, account_code AS CODE, NAME, amount, 
            taxed_amount, CONCAT(MONTHNAME(`date`), ' ' , YEAR(`date`)) AS MONTH, 'INV' AS s_type
        FROM invoices
        WHERE invoices.`deleted_at` IS NULL
        UNION 
        SELECT docno, DATE, account_code, account_code AS CODE, NAME, amount, 
            taxed_amount, CONCAT(MONTHNAME(`date`), ' ' , YEAR(`date`)) AS MONTH, 'CB' AS s_type
        FROM cashbills
        WHERE cashbills.`deleted_at` IS NULL
        UNION 
        SELECT docno, DATE, account_code, account_code AS CODE, NAME, amount, 
            taxed_amount, CONCAT(MONTHNAME(`date`), ' ' , YEAR(`date`)) AS MONTH, 'DO' AS s_type
        FROM deliveryorders
        WHERE deliveryorders.`deleted_at` IS NULL
        UNION 
        SELECT docno, DATE, account_code, account_code AS CODE, NAME, amount, 
            taxed_amount, CONCAT(MONTHNAME(`date`), ' ' , YEAR(`date`)) AS MONTH, 'SR' AS s_type
        FROM salesreturns
        WHERE salesreturns.`deleted_at` IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_sales_report_bybill");
    }
}
