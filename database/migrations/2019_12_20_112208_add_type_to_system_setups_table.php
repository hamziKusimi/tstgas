<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToSystemSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_setups', function (Blueprint $table) {
            //
            $table->string('custom1_type')->nullable();
            $table->string('custom2_type')->nullable();
            $table->string('custom3_type')->nullable();
            $table->string('custom4_type')->nullable();
            $table->string('custom5_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('system_setups', function (Blueprint $table) {
        //     //
        // });
    }
}
