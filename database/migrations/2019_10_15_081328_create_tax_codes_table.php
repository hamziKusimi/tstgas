<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('t_code')->nullable();
            $table->text('t_descr1')->nullable();
            $table->text('t_descr2')->nullable();
            $table->string('t_type')->nullable();
            $table->string('t_rate')->nullable();
            $table->string('t_ttype')->nullable();
            $table->string('t_acode')->nullable();
            $table->string('t_active')->nullable();
            $table->string('t_keyuser')->nullable();
            $table->string('t_modiuser')->nullable();
            $table->string('t_keydt')->nullable();
            $table->string('t_modidt')->nullable();
            $table->string('t_traffic')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_codes');
    }
}
