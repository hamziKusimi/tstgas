<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2SalesReportBystockView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(config('config.cylinder.cylinder') == "true"){
            DB::statement("DROP VIEW view_sales_report_bystock");
            DB::statement("
            CREATE
                VIEW view_sales_report_bystock
                AS
                SELECT *, SUM(quantityd) AS quantity, SUM(amountd) AS amount, SUM(totalpriced) AS totalprice,
                    SUM(discountd) AS discount, SUM(taxamountd) AS taxamount, (SUM(amountd) + SUM(taxamountd)) AS taxed_amount
                FROM
                (
                    SELECT item_code, `subject`,uom, IF(a.quantity IS NULL,'0',a.quantity) AS quantityd, IF(a.amount IS NULL,'0',a.amount) AS amountd, a.tax_amount,
                        c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
                        g.date AS `date`, a.`taxed_amount` AS t_amount, 'INV' AS s_type,
                        (IF(a.unit_price IS NULL,'0',a.unit_price) * IF(a.quantity IS NULL,'0',a.quantity)) AS totalpriced,
                        IF(a.discount IS NULL,'0',a.discount) AS discountd,
                        IF(a.tax_amount IS NULL,'0',a.tax_amount) AS taxamountd
                    FROM cylinder_invoicespdts a
                    LEFT JOIN stockcodes b ON a.`item_code` = b.`code` AND b.`deleted_at` IS NULL
                    LEFT JOIN categories c ON b.`cat_id` = c.`id` AND c.`deleted_at` IS NULL
                    LEFT JOIN products d ON b.`prod_id` = d.`id` AND d.`deleted_at` IS NULL
                    LEFT JOIN brands e ON b.`brand_id` = e.`id` AND e.`deleted_at` IS NULL
                    LEFT JOIN locations f ON b.`loc_id` = f.`id` AND f.`deleted_at` IS NULL
                    LEFT JOIN cylinder_invoices g ON a.doc_no = g.docno AND g.`deleted_at` IS NULL
                    WHERE a.`deleted_at` IS NULL
                    UNION ALL
                    SELECT item_code, `subject`,uom, IF(a.quantity IS NULL,'0',a.quantity) AS quantityd, IF(a.amount IS NULL,'0',a.amount) AS amountd, a.tax_amount,
                    c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
                    g.date AS `date`, a.`taxed_amount` AS t_amount, 'CB' AS s_type,
                        (IF(a.unit_price IS NULL,'0',a.unit_price) * IF(a.quantity IS NULL,'0',a.quantity)) AS totalpriced,
                        IF(a.discount IS NULL,'0',a.discount) AS discountd,
                        IF(a.tax_amount IS NULL,'0',a.tax_amount) AS taxamountd
                    FROM cylinder_cashbillspdts a
                    LEFT JOIN stockcodes b ON a.`item_code` = b.`code` AND b.`deleted_at` IS NULL
                    LEFT JOIN categories c ON b.`cat_id` = c.`id` AND c.`deleted_at` IS NULL
                    LEFT JOIN products d ON b.`prod_id` = d.`id` AND d.`deleted_at` IS NULL
                    LEFT JOIN brands e ON b.`brand_id` = e.`id` AND e.`deleted_at` IS NULL
                    LEFT JOIN locations f ON b.`loc_id` = f.`id` AND f.`deleted_at` IS NULL
                    INNER JOIN cylinder_cashbills g ON a.doc_no = g.docno AND g.`deleted_at` IS NULL
                    WHERE a.`deleted_at` IS NULL
                    UNION ALL
                    SELECT item_code, `subject`,uom, IF(a.qty IS NULL,'0',a.qty) AS quantityd, IF(a.amount IS NULL,'0',a.amount) AS amountd, a.tax_amount,
                    c.code AS category, d.code AS product, e.code AS brand, f.code AS location,
                    g.date AS `date`, a.`taxed_amount` AS t_amount, 'SR' AS s_type,
                        (IF(a.uprice IS NULL,'0',a.uprice) * IF(a.qty IS NULL,'0',a.qty)) AS totalpriced,
                        IF(a.discount IS NULL,'0',a.discount) AS discountd,
                        IF(a.tax_amount IS NULL,'0',a.tax_amount) AS taxamountd
                    FROM salesreturndts a
                    LEFT JOIN stockcodes b ON a.`item_code` = b.`code` AND b.`deleted_at` IS NULL
                    LEFT JOIN categories c ON b.`cat_id` = c.`id` AND c.`deleted_at` IS NULL
                    LEFT JOIN products d ON b.`prod_id` = d.`id` AND d.`deleted_at` IS NULL
                    LEFT JOIN brands e ON b.`brand_id` = e.`id` AND e.`deleted_at` IS NULL
                    LEFT JOIN locations f ON b.`loc_id` = f.`id` AND f.`deleted_at` IS NULL
                    INNER JOIN salesreturns g ON a.doc_no = g.docno AND g.`deleted_at` IS NULL
                    WHERE a.`deleted_at` IS NULL
                ) AS stocks
                GROUP BY item_code, DATE, s_type
                ORDER BY DATE DESC;
            ");

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
