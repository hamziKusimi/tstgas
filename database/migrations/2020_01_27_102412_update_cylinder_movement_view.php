<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCylinderMovementView2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW view_cylinder_movement");
        DB::statement("
            CREATE
            VIEW view_cylinder_movement
            AS
            SELECT
                delivery_notes.dn_no AS doc_no,'DN' AS doc_type, delivery_notes.account_code AS debtor,
                IFNULL(delivery_noteprs.serial , '-') AS s_no, delivery_notes.date AS cdate, delivery_notedts.product
            FROM delivery_notes
            INNER JOIN delivery_notedts ON  delivery_notedts.dn_no =  delivery_notes.dn_no
                INNER JOIN delivery_noteprs ON  delivery_noteprs.dn_no =  delivery_notes.dn_no
                AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no
            WHERE delivery_notedts.deleted_at IS NULL AND delivery_noteprs.deleted_at IS NULL
                AND delivery_notes.deleted_at IS NULL AND delivery_noteprs.type = 'cy'
                UNION ALL
            SELECT
                return_notes.dn_no AS doc_no, 'RN' AS doc_type, return_notes.account_code AS debtor,
                IFNULL(return_notells.serial , '-') AS s_no,
                return_notes.date AS cdate, cylinderproducts.code
            FROM return_notes
                INNER JOIN return_notedts ON  return_notedts.dn_no =  return_notes.dn_no
                INNER JOIN return_notells ON return_notells.dn_no = return_notedts.dn_no AND return_notells.sequence_no = return_notedts.sequence_no
                LEFT JOIN cylinders ON return_notells.`serial` = cylinders.`serial`
                INNER JOIN cylinderproducts ON cylinders.`prod_id` = cylinderproducts.`id`
            WHERE
                return_notedts.deleted_at IS NULL
                AND return_notells.deleted_at IS NULL AND return_notes.deleted_at IS NULL
                AND return_notells.type = 'cy'
            UNION ALL
            SELECT
                wo_no AS doc_no, 'Refill' AS doc_type , '-' AS debtor,
                refills.serial AS s_no, DATETIME AS cdate,
            cylinderproducts.code AS product
            FROM refills
                INNER JOIN cylinders ON cylinders.serial = refills.serial
                INNER JOIN cylinderproducts ON cylinders.prod_id = cylinderproducts.id
            WHERE refills.deleted_at IS NULL
            UNION ALL
            SELECT
                rp_no AS doc_no, 'Repair' AS doc_type , '-' AS debtor,
                repairs.serial AS s_no, logindate AS cdate, cylinderproducts.code AS product
            FROM repairs
                INNER JOIN cylinders ON cylinders.serial = repairs.serial
                INNER JOIN cylinderproducts ON cylinders.prod_id = cylinderproducts.id
            WHERE repairs.deleted_at IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
