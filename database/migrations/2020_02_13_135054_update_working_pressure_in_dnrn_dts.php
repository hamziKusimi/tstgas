<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWorkingPressureInDnrnDts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_notedts', function (Blueprint $table) {

            $table->dropColumn('working_pressure')->nullable();
            
        });

        Schema::table('return_notedts', function (Blueprint $table) {

            $table->dropColumn('working_pressure')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
