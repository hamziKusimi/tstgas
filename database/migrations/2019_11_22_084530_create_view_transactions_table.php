<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        DB::statement("
        CREATE VIEW view_transactions
        AS
        SELECT
        `cashbills`.`docno`          AS `docno`,
        `cashbills`.`date`           AS `date`,
        `cashbills`.`lpono`          AS `lpono`,
        `cashbills`.`account_code`   AS `account_code`,
        `cashbills`.`account_code`   AS `debtor`,
        `cashbills`.`name`           AS `name`,
        CONCAT(CONCAT(`cashbills`.`docno`),', ',`cashbills`.`date`,', ',`cashbills`.`account_code`,', ',`cashbills`.`name`) AS `details`,
        `cashbilldts`.`subject`      AS `description`,
        `cashbilldts`.`doc_no`       AS `doc_no`,
        `cashbilldts`.`item_code`    AS `item_code`,
        `cashbilldts`.`qty`          AS `quantity`,
        `cashbilldts`.`uom`          AS `uom`,
        `cashbilldts`.`uprice`       AS `unit_price`,
        `cashbilldts`.`amount`       AS `amount`,
        `cashbilldts`.`taxed_amount` AS `tax_amount`,
        `cashbilldts`.`subject`      AS `subject`,
        `cashbilldts`.`details`      AS `DETAIL`,
        `stockcodes`.`loc_id`        AS `loc_id`
        FROM ((`cashbills`
            JOIN `cashbilldts`
            ON ((`cashbills`.`docno` = `cashbilldts`.`doc_no`)))
        JOIN `stockcodes`
            ON ((`cashbilldts`.`item_code` = `stockcodes`.`code`)))
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_transactions");
    }
}
