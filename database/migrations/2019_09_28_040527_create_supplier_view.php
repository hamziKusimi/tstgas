<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //DB::statement("DROP VIEW supplier_view");
        DB::statement("
            CREATE VIEW `supplier_view` AS
            SELECT
                `cashbilldts`.`id` AS `id`,
                `cashbilldts`.`doc_no` AS `doc_no`,
                `cashbilldts`.`updated_at` AS `updated_at`,
                `cashbilldts`.`account_code` AS `account_code`,
                `cashbilldts`.`item_code` AS `item_code`,
                `cashbilldts`.`subject` AS `subject`,
                `cashbilldts`.`qty` AS `qty`,
                `cashbilldts`.`uom` AS `uom`,
                `cashbilldts`.`rate` AS `rate`,
                `cashbilldts`.`uprice` AS `uprice`,
                `cashbilldts`.`discount` AS `discount`,
                `cashbilldts`.`amount` AS `amount`,
                `cashbilldts`.`updated_at` AS `date`,
                `cashbills`.`name` AS `name`,
                `stockcodes`.`descr` AS `descr`,
                `stockcodes`.`ref1` AS `ref1`,
                `stockcodes`.`ref2` AS `ref2`,
                `stockcodes`.`model` AS `model`,
                `stockcodes`.`weight` AS `weight`,
                `stockcodes`.`type` AS `type`,
                `stockcodes`.`inactive` AS `inactive`,
                `categories`.`code` AS `category`,
                `products`.`code` AS `product`,
                `brands`.`code` AS `brand`,
                `locations`.`code` AS `location`
            FROM cashbilldts
            INNER JOIN `stockcodes`
                ON `cashbilldts`.`item_code` = `stockcodes`.`code`
            INNER JOIN `categories`
                ON `stockcodes`.`cat_id` = `categories`.`id`
            INNER JOIN `products`
                ON `stockcodes`.`prod_id` = `products`.`id`
            INNER JOIN `brands`
                ON `stockcodes`.`brand_id` = `brands`.`id`
            INNER JOIN `locations`
                ON `stockcodes`.`loc_id` = `locations`.`id`
            INNER JOIN `cashbills`
                ON `cashbilldts`.`doc_no` = `cashbills`.`docno`
            UNION ALL
            SELECT
                `invoice_data`.`id` AS `id`,
                `invoice_data`.`doc_no` AS `doc_no`,
                `invoice_data`.`updated_at` AS `updated_at`,
                `invoice_data`.`account_code` AS `account_code`,
                `invoice_data`.`item_code` AS `item_code`,
                `invoice_data`.`subject` AS `subject`,
                `invoice_data`.`quantity` AS `qty`,
                `invoice_data`.`uom` AS `uom`,
                `invoice_data`.`rate` AS `rate`,
                `invoice_data`.`unit_price` AS `unit_price`,
                `invoice_data`.`discount` AS `discount`,
                `invoice_data`.`amount` AS `amount`,
                `invoice_data`.`updated_at` AS `date`,
                `invoices`.`name` AS `name`,
                `stockcodes`.`descr` AS `descr`,
                `stockcodes`.`ref1` AS `ref1`,
                `stockcodes`.`ref2` AS `ref2`,
                `stockcodes`.`model` AS `model`,
                `stockcodes`.`weight` AS `weight`,
                `stockcodes`.`type` AS `type`,
                `stockcodes`.`inactive` AS `inactive`,
                `categories`.`code` AS `category`,
                `products`.`code` AS `product`,
                `brands`.`code` AS `brand`,
                `locations`.`code` AS `location`
            FROM invoice_data
            INNER JOIN `stockcodes`
                ON `invoice_data`.`item_code` = `stockcodes`.`code`
            INNER JOIN `categories`
                ON `stockcodes`.`cat_id` = `categories`.`id`
            INNER JOIN `products`
                ON `stockcodes`.`prod_id` = `products`.`id`
            INNER JOIN `brands`
                ON `stockcodes`.`brand_id` = `brands`.`id`
            INNER JOIN `locations`
                ON `stockcodes`.`loc_id` = `locations`.`id`
            INNER JOIN `invoices`
                ON `invoice_data`.`doc_no` = `invoices`.`docno`
            UNION ALL
                SELECT
                `deliveryorderdts`.`id` AS `id`,
                `deliveryorderdts`.`doc_no` AS `doc_no`,
                `deliveryorderdts`.`updated_at` AS `updated_at`,
                `deliveryorderdts`.`account_code` AS `account_code`,
                `deliveryorderdts`.`item_code` AS `item_code`,
                `deliveryorderdts`.`subject` AS `subject`,
                `deliveryorderdts`.`qty` AS `qty`,
                `deliveryorderdts`.`uom` AS `uom`,
                `deliveryorderdts`.`rate` AS `rate`,
                `deliveryorderdts`.`uprice` AS `uprice`,
                `deliveryorderdts`.`discount` AS `discount`,
                `deliveryorderdts`.`amount` AS `amount`,
                `deliveryorderdts`.`updated_at` AS `date`,
                `deliveryorders`.`name` AS `name`,
                `stockcodes`.`descr` AS `descr`,
                `stockcodes`.`ref1` AS `ref1`,
                `stockcodes`.`ref2` AS `ref2`,
                `stockcodes`.`model` AS `model`,
                `stockcodes`.`weight` AS `weight`,
                `stockcodes`.`type` AS `type`,
                `stockcodes`.`inactive` AS `inactive`,
                `categories`.`code` AS `category`,
                `products`.`code` AS `product`,
                `brands`.`code` AS `brand`,
                `locations`.`code` AS `location`
            FROM deliveryorderdts
            INNER JOIN `stockcodes`
                ON `deliveryorderdts`.`item_code` = `stockcodes`.`code`
            INNER JOIN `categories`
                ON `stockcodes`.`cat_id` = `categories`.`id`
            INNER JOIN `products`
                ON `stockcodes`.`prod_id` = `products`.`id`
            INNER JOIN `brands`
                ON `stockcodes`.`brand_id` = `brands`.`id`
            INNER JOIN `locations`
                ON `stockcodes`.`loc_id` = `locations`.`id`
            INNER JOIN `deliveryorders`
                ON `deliveryorderdts`.`doc_no` = `deliveryorders`.`docno`
            UNION ALL
            SELECT
                `salesreturndts`.`id` AS `id`,
                `salesreturndts`.`doc_no` AS `doc_no`,
                `salesreturndts`.`updated_at` AS `updated_at`,
                `salesreturndts`.`account_code` AS `account_code`,
                `salesreturndts`.`item_code` AS `item_code`,
                `salesreturndts`.`subject` AS `subject`,
                `salesreturndts`.`qty` AS `qty`,
                `salesreturndts`.`uom` AS `uom`,
                `salesreturndts`.`rate` AS `rate`,
                `salesreturndts`.`uprice` AS `uprice`,
                `salesreturndts`.`discount` AS `discount`,
                `salesreturndts`.`amount` AS `amount`,
                `salesreturndts`.`updated_at` AS `date`,
                `salesreturns`.`name` AS `name`,
                `stockcodes`.`descr` AS `descr`,
                `stockcodes`.`ref1` AS `ref1`,
                `stockcodes`.`ref2` AS `ref2`,
                `stockcodes`.`model` AS `model`,
                `stockcodes`.`weight` AS `weight`,
                `stockcodes`.`type` AS `type`,
                `stockcodes`.`inactive` AS `inactive`,
                `categories`.`code` AS `category`,
                `products`.`code` AS `product`,
                `brands`.`code` AS `brand`,
                `locations`.`code` AS `location`
            FROM salesreturndts
            INNER JOIN `stockcodes`
                ON `salesreturndts`.`item_code` = `stockcodes`.`code`
            INNER JOIN `categories`
                ON `stockcodes`.`cat_id` = `categories`.`id`
            INNER JOIN `products`
                ON `stockcodes`.`prod_id` = `products`.`id`
            INNER JOIN `brands`
                ON `stockcodes`.`brand_id` = `brands`.`id`
            INNER JOIN `locations`
                ON `stockcodes`.`loc_id` = `locations`.`id`
            INNER JOIN `salesreturns`
                ON `salesreturndts`.`doc_no` = `salesreturns`.`docno`
            UNION ALL
            SELECT
                `quotationdts`.`id` AS `id`,
                `quotationdts`.`doc_no` AS `doc_no`,
                `quotationdts`.`updated_at` AS `updated_at`,
                `quotationdts`.`account_code` AS `account_code`,
                `quotationdts`.`item_code` AS `item_code`,
                `quotationdts`.`subject` AS `subject`,
                `quotationdts`.`qty` AS `qty`,
                `quotationdts`.`uom` AS `uom`,
                `quotationdts`.`rate` AS `rate`,
                `quotationdts`.`uprice` AS `uprice`,
                `quotationdts`.`discount` AS `discount`,
                `quotationdts`.`amount` AS `amount`,
                `quotationdts`.`updated_at` AS `date`,
                `quotations`.`name` AS `name`,
                `stockcodes`.`descr` AS `descr`,
                `stockcodes`.`ref1` AS `ref1`,
                `stockcodes`.`ref2` AS `ref2`,
                `stockcodes`.`model` AS `model`,
                `stockcodes`.`weight` AS `weight`,
                `stockcodes`.`type` AS `type`,
                `stockcodes`.`inactive` AS `inactive`,
                `categories`.`code` AS `category`,
                `products`.`code` AS `product`,
                `brands`.`code` AS `brand`,
                `locations`.`code` AS `location`
            FROM quotationdts
            INNER JOIN `stockcodes`
                ON `quotationdts`.`item_code` = `stockcodes`.`code`
            INNER JOIN `categories`
                ON `stockcodes`.`cat_id` = `categories`.`id`
            INNER JOIN `products`
                ON `stockcodes`.`prod_id` = `products`.`id`
            INNER JOIN `brands`
                ON `stockcodes`.`brand_id` = `brands`.`id`
            INNER JOIN `locations`
                ON `stockcodes`.`loc_id` = `locations`.`id`
            INNER JOIN `quotations`
                ON `quotationdts`.`doc_no` = `quotations`.`docno`
            ORDER BY `date`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement("DROP VIEW supplier_view");
    }
}
