<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNewMastercodeView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW view_new_mastercode");
        DB::statement("
            CREATE VIEW view_new_mastercode
            AS
            SELECT
                debtors.id as id,
                'D' as type,
                CONCAT(debtors.accountcode) as account_code,
                debtors.name AS name,
                CONCAT(CONCAT(debtors.accountcode), ' ', debtors.name) AS full_detail,
                debtors.tel AS tel_no,
                debtors.fax AS fax_no,
                debtors.cperson AS contact_person,
                debtors.email AS email,
                debtors.address1 AS addr1,
                debtors.address2 AS addr2,
                debtors.address3 AS addr3,
                debtors.address4 AS addr4,
                debtors.hp AS handphone,
                debtors.cptel AS contact_person_tel_no,
                debtors.cterm AS credit_term,
                debtors.climit AS credit_limit,
                debtors.ccurrency AS currency,
                debtors.gstno AS gst_no,
                debtors.brnno AS brn_no,
                debtors.yopen AS opening,
                debtors.active AS active,
                debtors.def_price AS def_price
            FROM debtors
            WHERE debtors.`deleted_at` IS NULL
            UNION ALL
            SELECT
                creditors.id as id,
                'C' as type,
                CONCAT(creditors.accountcode) as account_code,
                creditors.name AS name,
                CONCAT(CONCAT(creditors.accountcode), ' ', creditors.name) AS full_detail,
                creditors.tel AS tel_no,
                creditors.fax AS fax_no,
                creditors.cperson AS contact_person,
                creditors.email AS email,
                creditors.address1 AS addr1,
                creditors.address2 AS addr2,
                creditors.address3 AS addr3,
                creditors.address4 AS addr4,
                creditors.hp AS handphone,
                creditors.cptel AS contact_person_tel_no,
                creditors.cterm AS credit_term,
                creditors.climit AS credit_limit,
                creditors.ccurrency AS currency,
                creditors.gstno AS gst_no,
                creditors.brnno AS brn_no,
                creditors.yopen AS opening,
                creditors.active AS active,
                NULL AS def_price
            FROM creditors
            WHERE creditors.`deleted_at` IS NULL
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
