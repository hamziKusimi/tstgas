<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylinderQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cylinder_quotations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('docno')->nullable();
            $table->string('date')->nullable();
            $table->date('m_duedate')->nullable();
            $table->date('refinvdate')->nullable();
            $table->date('refno')->nullable();
            $table->string('do_no')->nullable();
            $table->string('discount')->nullable();
            $table->string('amount')->nullable();
            $table->string('tax_amount')->nullable();
            $table->string('taxed_amount')->nullable();
            $table->string('account_code')->nullable();
            $table->string('name')->nullable();
            $table->string('addr1')->nullable();
            $table->string('addr2')->nullable();
            $table->string('addr3')->nullable();
            $table->string('addr4')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('credit_term')->nullable();
            $table->string('credit_limit')->nullable();
            $table->text('header')->nullable();
            $table->text('footer')->nullable();
            $table->text('summary')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('currency')->nullable();
            $table->string('exchange_rate')->nullable();
            $table->string('rounding')->nullable();
            $table->string('deleted_by')->nullable();
            $table->text('custom1')->nullable();
            $table->text('custom2')->nullable();
            $table->text('custom3')->nullable();
            $table->text('custom4')->nullable();
            $table->text('custom5')->nullable();
            $table->text('custom6')->nullable();
            $table->text('custom7')->nullable();
            $table->text('custom8')->nullable();
            $table->text('custom9')->nullable();
            $table->text('custom10')->nullable();
            $table->text('custom11')->nullable();
            $table->text('custom12')->nullable();
            $table->text('custom13')->nullable();
            $table->text('custom14')->nullable();
            $table->text('custom15')->nullable();
            $table->text('custom16')->nullable();
            $table->text('custom17')->nullable();
            $table->text('custom18')->nullable();
            $table->text('custom19')->nullable();
            $table->text('custom20')->nullable();
            $table->text('custom21')->nullable();
            $table->text('custom22')->nullable();
            $table->text('custom23')->nullable();
            $table->text('custom24')->nullable();
            $table->text('custom25')->nullable();
            $table->string('customval1')->nullable();
            $table->string('customval2')->nullable();
            $table->string('customval3')->nullable();
            $table->string('customval4')->nullable();
            $table->string('customval5')->nullable();
            $table->string('customval6')->nullable();
            $table->string('customval7')->nullable();
            $table->string('customval8')->nullable();
            $table->string('customval9')->nullable();
            $table->string('customval10')->nullable();
            $table->string('customval11')->nullable();
            $table->string('customval12')->nullable();
            $table->string('customval13')->nullable();
            $table->string('customval14')->nullable();
            $table->string('customval15')->nullable();
            $table->string('customval16')->nullable();
            $table->string('customval17')->nullable();
            $table->string('customval18')->nullable();
            $table->string('customval19')->nullable();
            $table->string('customval20')->nullable();
            $table->string('customval21')->nullable();
            $table->string('customval22')->nullable();
            $table->string('customval23')->nullable();
            $table->string('customval24')->nullable();
            $table->string('customval25')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cylinder_quotations');
    }
}
