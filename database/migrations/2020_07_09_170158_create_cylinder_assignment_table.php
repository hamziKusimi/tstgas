<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylinderAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cylinder_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_assign_no');
            $table->string('doc_assign_date');
            $table->string('doc_assign_user');
            $table->string('do_no')->nullable();
            $table->string('do_date')->nullable();
            $table->string('do_serial_no')->nullable();
            $table->string('assign_serial_no')->nullable();
            $table->string('rn_no')->nullable();
            $table->string('rn_date')->nullable();
            $table->string('is_sold')->default('N')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cylinder_assignments');
    }
}
