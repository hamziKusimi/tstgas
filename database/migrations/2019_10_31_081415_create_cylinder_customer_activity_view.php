<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCylinderCustomerActivityView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE
        VIEW view_cylinder_customer_activity
        AS
        SELECT
            delivery_notedts.dn_no AS doc_no,'DN' AS doc_type, delivery_notes.account_code AS debtor,
            IFNULL(delivery_noteuls.serial , '-') AS s_no,
            delivery_noteuls.datetime AS cdate, product
        FROM delivery_notes
            INNER JOIN delivery_notedts ON  delivery_notedts.dn_no =  delivery_notes.dn_no
            INNER JOIN delivery_noteuls ON delivery_noteuls.dn_no = delivery_notedts.dn_no
            AND delivery_noteuls.sequence_no = delivery_notedts.sequence_no AND delivery_noteuls.`type` ='cy'
        WHERE delivery_notedts.deleted_at IS NULL AND delivery_noteuls.deleted_at IS NULL
            AND delivery_notes.deleted_at IS NULL AND delivery_noteuls.type = 'cy'
            UNION ALL
        SELECT
            return_notedts.dn_no AS doc_no, 'RN' AS doc_type, return_notes.account_code AS debtor,
            IFNULL(return_noteuls.serial , '-') AS s_no,
            return_noteuls.datetime AS cdate, product
        FROM return_notes
            INNER JOIN return_notedts ON  return_notedts.dn_no =  return_notes.dn_no
            INNER JOIN return_noteuls ON return_noteuls.dn_no = return_notedts.dn_no
            AND return_noteuls.sequence_no = return_notedts.sequence_no AND return_noteuls.`type` ='cy'
        WHERE
            return_notedts.deleted_at IS NULL
            AND return_noteuls.deleted_at IS NULL AND return_notes.deleted_at IS NULL
            AND return_noteuls.type = 'cy'
        UNION ALL
        SELECT
            wo_no AS doc_no, 'Refill' AS doc_type , '-' AS debtor,
            refills.serial AS s_no, DATETIME AS cdate,
        cylinderproducts.code AS product
        FROM refills
            INNER JOIN cylinders ON cylinders.serial = refills.serial
            INNER JOIN cylinderproducts ON cylinders.prod_id = cylinderproducts.id
        WHERE refills.deleted_at IS NULL
        UNION ALL
        SELECT
            rp_no AS doc_no, 'Repair' AS doc_type , '-' AS debtor,
            repairs.serial AS s_no, logindate AS cdate, cylinderproducts.code AS product
        FROM repairs
            INNER JOIN cylinders ON cylinders.serial = repairs.serial
            INNER JOIN cylinderproducts ON cylinders.prod_id = cylinderproducts.id
        WHERE repairs.deleted_at IS NULL
        UNION ALL
        SELECT
            cylinder_invoices.docno AS doc_no, 'CINV' AS doc_type,
            cylinder_invoices.account_code AS debtor, IFNULL(cylinder_invoicedts.serial , '-') AS s_no,
            DATE(cylinder_invoices.date) AS cdate, cylinder_invoicedts.product
        FROM cylinder_invoices
        INNER JOIN cylinder_invoicedts ON  cylinder_invoicedts.doc_no =  cylinder_invoices.docno
        WHERE cylinder_invoicedts.deleted_at IS NULL AND cylinder_invoices.deleted_at IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_cylinder_customer_activity");
    }
}
