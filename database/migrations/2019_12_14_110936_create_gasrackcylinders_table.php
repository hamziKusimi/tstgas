<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasrackcylindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasrackcylinders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gr_id')->nullable();
            $table->string('cy_barcode')->nullable();
            $table->string('cy_serial')->nullable();
            $table->string('cy_category')->nullable();
            $table->string('cy_product')->nullable();
            $table->string('cy_descr')->nullable();
            $table->string('cy_capacity')->nullable();
            $table->string('cy_pressure')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasrackcylinders');
    }
}
