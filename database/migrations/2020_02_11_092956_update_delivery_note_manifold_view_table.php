<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDeliveryNoteManifoldViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW view_delivery_note_manifold");
        DB::statement("
        CREATE
        VIEW `view_delivery_note_manifold`
        AS
        SELECT dn_no, delivery_noteprs.barcode, cy_serial, gasracks.`serial` AS gr_serial, gasracks.description as descr
        FROM delivery_noteprs
        INNER JOIN gasracks ON gasracks.`serial` = delivery_noteprs.`serial`
        LEFT JOIN  gasrackcylinders ON gasrackcylinders.`gr_id` = gasracks.`id`
        WHERE delivery_noteprs.`deleted_at` IS NULL AND gasrackcylinders.`deleted_at` IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
