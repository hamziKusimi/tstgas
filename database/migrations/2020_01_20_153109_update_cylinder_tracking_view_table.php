<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCylinderTrackingViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW cylinder_tracking_view");
        DB::statement("
            CREATE VIEW `cylinder_tracking_view` AS
            SELECT 
                'Delivery Note' AS model,
                delivery_notells.barcode AS barcode,
                delivery_notells.serial AS SERIAL,
                'Loading' AS ACTION,
                delivery_notells.driver AS description,
                delivery_notells.datetime AS DATETIME,
                DATE(delivery_notells.created_at) AS created_at,
                DATE(delivery_notells.updated_at) AS updated_at,
                delivery_notells.dn_no AS dn_no,
                delivery_notedts.category AS TYPE
            FROM delivery_notells 
            INNER JOIN delivery_notedts ON (delivery_notells.dn_no = delivery_notedts.dn_no AND delivery_notells.sequence_no = delivery_notedts.sequence_no)
            WHERE delivery_notells.datetime IS NOT NULL AND delivery_notells.deleted_at IS NULL AND delivery_notells.type = 'cy'
            UNION ALL
            SELECT 
                'Delivery Note' AS model,
                delivery_notells.barcode AS barcode,
                delivery_notells.serial AS SERIAL,
                'Loading' AS ACTION,
                delivery_notells.driver AS description,
                delivery_notells.datetime AS DATETIME,
                DATE(delivery_notells.created_at) AS created_at,
                DATE(delivery_notells.updated_at) AS updated_at,
                delivery_notells.dn_no AS dn_no,
                delivery_note_grdts.type AS TYPE
            FROM delivery_notells 
            INNER JOIN delivery_note_grdts ON (delivery_notells.dn_no = delivery_note_grdts.dn_no AND delivery_notells.sequence_no = delivery_note_grdts.sequence_no)
            WHERE delivery_notells.datetime IS NOT NULL AND delivery_notells.deleted_at IS NULL AND delivery_notells.type = 'gr'
            UNION ALL
            SELECT
                'Delivery Note' AS model,
                delivery_noteuls.barcode AS barcode,
                delivery_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                delivery_noteuls.driver AS description,
                delivery_noteuls.datetime AS DATETIME,
                DATE(delivery_noteuls.created_at) AS created_at,
                DATE(delivery_noteuls.updated_at) AS updated_at,	
                delivery_noteuls.dn_no AS dn_no,
                delivery_notedts.category AS TYPE
            FROM delivery_noteuls 
            INNER JOIN delivery_notedts ON (delivery_noteuls.dn_no = delivery_notedts.dn_no AND delivery_noteuls.sequence_no = delivery_notedts.sequence_no)
            WHERE delivery_noteuls.datetime IS NOT NULL AND delivery_noteuls.deleted_at IS NULL AND delivery_noteuls.type = 'cy'
            UNION ALL
            SELECT
                'Delivery Note' AS model,
                delivery_noteuls.barcode AS barcode,
                delivery_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                delivery_noteuls.driver AS description,
                delivery_noteuls.datetime AS DATETIME,
                DATE(delivery_noteuls.created_at) AS created_at,
                DATE(delivery_noteuls.updated_at) AS updated_at,	
                delivery_noteuls.dn_no AS dn_no,
                delivery_note_grdts.type AS TYPE
            FROM delivery_noteuls 
            INNER JOIN delivery_note_grdts ON (delivery_noteuls.dn_no = delivery_note_grdts.dn_no AND delivery_noteuls.sequence_no = delivery_note_grdts.sequence_no)
            WHERE delivery_noteuls.datetime IS NOT NULL AND delivery_noteuls.deleted_at IS NULL AND delivery_noteuls.type = 'gr'
            UNION ALL
            SELECT 
                'Return Note' AS model,
                return_notells.barcode AS barcode,
                return_notells.serial AS SERIAL,
                'Loading' AS ACTION,
                return_notells.driver AS description,
                return_notells.datetime AS DATETIME,
                DATE(return_notells.created_at) AS created_at,
                DATE(return_notells.updated_at) AS updated_at,
                return_notells.dn_no AS dn_no,
                delivery_notedts.category AS TYPE
            FROM return_notells 
            INNER JOIN delivery_notells ON (return_notells.barcode = delivery_notells.barcode)
            INNER JOIN delivery_notedts ON (delivery_notells.dn_no = delivery_notedts.dn_no AND delivery_notells.sequence_no = delivery_notedts.sequence_no)
            WHERE return_notells.datetime IS NOT NULL AND return_notells.deleted_at IS NULL AND return_notells.type = 'cy'
            UNION ALL
            SELECT 
                'Return Note' AS model,
                return_notells.barcode AS barcode,
                return_notells.serial AS SERIAL,
                'Loading' AS ACTION,
                return_notells.driver AS description,
                return_notells.datetime AS DATETIME,
                DATE(return_notells.created_at) AS created_at,
                DATE(return_notells.updated_at) AS updated_at,
                return_notells.dn_no AS dn_no,
                delivery_note_grdts.type AS TYPE
            FROM return_notells 
            INNER JOIN delivery_notells ON (return_notells.barcode = delivery_notells.barcode)
            INNER JOIN delivery_note_grdts ON (delivery_notells.dn_no = delivery_note_grdts.dn_no AND delivery_notells.sequence_no = delivery_note_grdts.sequence_no)
            WHERE return_notells.datetime IS NOT NULL AND return_notells.deleted_at IS NULL AND return_notells.type = 'gr'
            UNION ALL
            SELECT
                'Return Note' AS model,
                return_noteuls.barcode AS barcode,
                return_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                return_noteuls.driver AS description,
                return_noteuls.datetime AS DATETIME,
                DATE(return_noteuls.created_at) AS created_at,
                DATE(return_noteuls.updated_at) AS updated_at,
                return_noteuls.dn_no AS dn_no,
                delivery_notedts.category AS TYPE
            FROM return_noteuls 
            INNER JOIN delivery_notells ON (return_noteuls.barcode = delivery_notells.barcode)
            INNER JOIN delivery_notedts ON (delivery_notells.dn_no = delivery_notedts.dn_no AND delivery_notells.sequence_no = delivery_notedts.sequence_no)
            WHERE return_noteuls.datetime IS NOT NULL AND return_noteuls.deleted_at IS NULL AND return_noteuls.type = 'cy'
            UNION ALL
            SELECT
                'Return Note' AS model,
                return_noteuls.barcode AS barcode,
                return_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                return_noteuls.driver AS description,
                return_noteuls.datetime AS DATETIME,
                DATE(return_noteuls.created_at) AS created_at,
                DATE(return_noteuls.updated_at) AS updated_at,
                return_noteuls.dn_no AS dn_no,
                delivery_note_grdts.type AS TYPE
            FROM return_noteuls 
            INNER JOIN delivery_notells ON (return_noteuls.barcode = delivery_notells.barcode)
            INNER JOIN delivery_note_grdts ON (delivery_notells.dn_no = delivery_note_grdts.dn_no AND delivery_notells.sequence_no = delivery_note_grdts.sequence_no)
            WHERE return_noteuls.datetime IS NOT NULL AND return_noteuls.deleted_at IS NULL AND return_noteuls.type = 'gr'
            ORDER BY DATETIME
            
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
