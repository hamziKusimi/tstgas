<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequisitionprsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requisitionprs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dn_no')->nullable();
            $table->string('sequence_no')->nullable();
            $table->string('sqn_no')->nullable();
            $table->date('datetime')->nullable();
            $table->string('barcode')->nullable();
            $table->string('serial')->nullable();
            $table->string('driver')->nullable();
            $table->string('type')->nullable();
            $table->string('return_note')->nullable();
            $table->date('return_note_date')->nullable();
            $table->string('return_note_serial')->nullable();
            $table->char('is_exported')->nullable()->default('N');
            $table->string('rack_remark')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requisitionprs');
    }
}
