<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockBalanceView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS view_stock_balance');
        DB::statement("
        CREATE
        VIEW view_stock_balance
            AS
            SELECT
                    item_code, totalqty,
                    goods.date AS 't_date', uom, 'GR' AS t_type,
                    gooddts.doc_no, 'a' AS type_seq
            FROM gooddts
               INNER JOIN goods ON goods.docno = gooddts.doc_no
            WHERE gooddts.deleted_at IS NULL
            UNION ALL
            SELECT
                    item_code,-1*totalqty, preturns.date AS 't_date', uom, 'PR' AS t_type,
                    preturndts.doc_no, 'd' AS type_seq
            FROM preturndts
               INNER JOIN preturns ON preturns.docno = preturndts.doc_no
            WHERE preturndts.deleted_at IS NULL
            UNION ALL
            SELECT
                    item_code, -1*totalqty, cashbills.date AS 't_date',
                    uom, 'CB' AS t_type, cashbilldts.doc_no, 'e' AS type_seq
            FROM cashbilldts
               INNER JOIN cashbills ON cashbills.docno = cashbilldts.doc_no
            WHERE cashbilldts.deleted_at IS NULL
            UNION ALL
            SELECT
                    item_code, -1*totalqty, CONVERT(invoices.date, DATE) AS 't_date', uom,
                    'INV' AS t_type, invoice_data.doc_no, 'f' AS type_seq
            FROM invoice_data
                    INNER JOIN invoices ON invoice_data.doc_no = invoices.docno
            WHERE invoice_data.deleted_at IS NULL
                       AND invoices.deleted_at IS NULL
                       AND invoices.do_no = ''
            UNION ALL
            SELECT
                item_code, -1*totalqty, deliveryorders.date AS 't_date',
                uom, 'DO' AS t_type, deliveryorderdts.doc_no, 'g' AS type_seq
            FROM deliveryorderdts
                INNER JOIN deliveryorders ON deliveryorderdts.doc_no = deliveryorders.docno
            WHERE
                deliveryorderdts.deleted_at IS NULL
            UNION ALL
            SELECT
                item_code, totalqty, salesreturns.date AS 't_date',
                uom, 'SR' AS t_type, salesreturndts.doc_no, 'b' AS type_seq
            FROM salesreturndts
                INNER JOIN salesreturns ON salesreturndts.doc_no = salesreturns.docno
            WHERE salesreturndts.deleted_at IS NULL
            UNION ALL
            SELECT
                item_code, totalqty, adjustmentis.date AS 't_date', uom,
                'AIN' AS t_type, adjustmentidts.doc_no, 'c' AS type_seq
            FROM adjustmentidts
                INNER JOIN adjustmentis ON adjustmentidts.doc_no = adjustmentis.docno
            WHERE adjustmentidts.deleted_at IS NULL AND adjustmentis.deleted_at IS NULL
            UNION ALL
            SELECT
                item_code, -1*totalqty, adjustmentos.date AS 't_date',
                uom, 'AOUT' AS t_type, adjustmentodts.doc_no, 'h' AS type_seq
            FROM adjustmentodts
                INNER JOIN adjustmentos ON adjustmentodts.doc_no = adjustmentos.docno
            WHERE adjustmentodts.deleted_at IS NULL AND adjustmentos.deleted_at IS NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement('DROP VIEW IF EXISTS view_stock_balance');
    }
}
