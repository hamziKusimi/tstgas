<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewUpdateCylinderTrackingTableView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW cylinder_tracking_view");
        DB::statement("
            CREATE VIEW `cylinder_tracking_view` AS
            SELECT
                'Delivery Note' AS model,
                delivery_noteprs.barcode AS barcode,
                delivery_noteprs.serial AS SERIAL,
                'Loading' AS ACTION,
                delivery_noteprs.driver AS description,
                delivery_noteprs.datetime AS DATETIME,
                DATE(delivery_noteprs.created_at) AS created_at,
                DATE(delivery_noteprs.updated_at) AS updated_at,
                delivery_noteprs.dn_no AS dn_no,
                delivery_notedts.category AS TYPE,
                delivery_notes.account_code AS accountcode,
                delivery_notes.name AS NAME
            FROM delivery_noteprs
            LEFT JOIN delivery_notedts ON (delivery_noteprs.dn_no = delivery_notedts.dn_no
                        AND delivery_noteprs.sequence_no = delivery_notedts.sequence_no
                        AND delivery_notedts.deleted_at IS NULL)
            LEFT JOIN delivery_notes ON (delivery_noteprs.dn_no = delivery_notes.dn_no
                        AND delivery_notes.deleted_at IS NULL)
            WHERE delivery_noteprs.datetime IS NOT NULL AND delivery_noteprs.deleted_at IS NULL AND delivery_noteprs.type = 'cy'
            UNION ALL
            SELECT
                'Delivery Note' AS model,
                delivery_noteprs.barcode AS barcode,
                delivery_noteprs.serial AS SERIAL,
                'Loading' AS ACTION,
                delivery_noteprs.driver AS description,
                delivery_noteprs.datetime AS DATETIME,
                DATE(delivery_noteprs.created_at) AS created_at,
                DATE(delivery_noteprs.updated_at) AS updated_at,
                delivery_noteprs.dn_no AS dn_no,
                delivery_note_grdts.type AS TYPE,
                delivery_notes.account_code AS accountcode,
                delivery_notes.name AS NAME
            FROM delivery_noteprs
            LEFT JOIN delivery_note_grdts ON (delivery_noteprs.dn_no = delivery_note_grdts.dn_no
                        AND delivery_noteprs.sequence_no = delivery_note_grdts.sequence_no
                        AND delivery_note_grdts.deleted_at IS NULL)
            LEFT JOIN delivery_notes ON (delivery_noteprs.dn_no = delivery_notes.dn_no
                        AND delivery_notes.deleted_at IS NULL)
            WHERE delivery_noteprs.datetime IS NOT NULL AND delivery_noteprs.deleted_at IS NULL
                        AND delivery_noteprs.type = 'gr'
            UNION ALL
            SELECT
                'Delivery Note' AS model,
                delivery_noteuls.barcode AS barcode,
                delivery_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                delivery_noteuls.driver AS description,
                delivery_noteuls.datetime AS DATETIME,
                DATE(delivery_noteuls.created_at) AS created_at,
                DATE(delivery_noteuls.updated_at) AS updated_at,
                delivery_noteuls.dn_no AS dn_no,
                delivery_notedts.category AS TYPE,
                delivery_notes.account_code AS accountcode,
                delivery_notes.name AS NAME
            FROM delivery_noteuls
            LEFT JOIN delivery_notedts ON (delivery_noteuls.dn_no = delivery_notedts.dn_no
                        AND delivery_noteuls.sequence_no = delivery_notedts.sequence_no
                        AND delivery_notedts.deleted_at IS NULL)
            LEFT JOIN delivery_notes ON (delivery_noteuls.dn_no = delivery_notes.dn_no
                        AND delivery_notes.deleted_at IS NULL)
            WHERE delivery_noteuls.datetime IS NOT NULL
                        AND delivery_noteuls.deleted_at IS NULL
                        AND delivery_noteuls.type = 'cy'
            UNION ALL
            SELECT
                'Delivery Note' AS model,
                delivery_noteuls.barcode AS barcode,
                delivery_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                delivery_noteuls.driver AS description,
                delivery_noteuls.datetime AS DATETIME,
                DATE(delivery_noteuls.created_at) AS created_at,
                DATE(delivery_noteuls.updated_at) AS updated_at,
                delivery_noteuls.dn_no AS dn_no,
                delivery_note_grdts.type AS TYPE,
                delivery_notes.account_code AS accountcode,
                delivery_notes.name AS NAME
            FROM delivery_noteuls
            LEFT JOIN delivery_note_grdts ON (delivery_noteuls.dn_no = delivery_note_grdts.dn_no
                        AND delivery_noteuls.sequence_no = delivery_note_grdts.sequence_no
                        AND delivery_note_grdts.deleted_at IS NULL)
            LEFT JOIN delivery_notes ON (delivery_noteuls.dn_no = delivery_notes.dn_no
                        AND delivery_notes.deleted_at IS NULL)
            WHERE delivery_noteuls.datetime IS NOT NULL
                        AND delivery_noteuls.deleted_at IS NULL
                        AND delivery_noteuls.type = 'gr'
            UNION ALL
            SELECT
                'Return Note' AS model,
                return_notells.barcode AS barcode,
                return_notells.serial AS SERIAL,
                'Loading' AS ACTION,
                return_notells.driver AS description,
                return_notells.datetime AS DATETIME,
                DATE(return_notells.created_at) AS created_at,
                DATE(return_notells.updated_at) AS updated_at,
                return_notells.dn_no AS dn_no,
                cylindercategories.code AS TYPE,
                return_notes.account_code AS accountcode,
                return_notes.name AS NAME
            FROM return_notells
            LEFT JOIN return_notes ON (return_notells.dn_no = return_notes.dn_no
                        AND return_notes.deleted_at IS NULL)
            LEFT JOIN cylinders ON (return_notells.serial = cylinders.serial
                        AND cylinders.deleted_at IS NULL)
            LEFT JOIN cylindercategories ON (cylinders.cat_id = cylindercategories.id
                        AND cylindercategories.deleted_at IS NULL)
            WHERE return_notells.datetime IS NOT NULL
                        AND return_notells.deleted_at IS NULL
                        AND return_notells.type = 'cy'
            UNION ALL
            SELECT
                'Return Note' AS model,
                return_notells.barcode AS barcode,
                return_notells.serial AS SERIAL,
                'Loading' AS ACTION,
                return_notells.driver AS description,
                return_notells.datetime AS DATETIME,
                DATE(return_notells.created_at) AS created_at,
                DATE(return_notells.updated_at) AS updated_at,
                return_notells.dn_no AS dn_no,
                gasracktypes.code AS TYPE,
                return_notes.account_code AS accountcode,
                return_notes.name AS NAME
            FROM return_notells
            LEFT JOIN return_notes ON (return_notells.dn_no = return_notes.dn_no
                        AND return_notes.deleted_at IS NULL)
            LEFT JOIN gasracks ON (return_notells.serial = gasracks.serial
                        AND gasracks.deleted_at IS NULL)
            LEFT JOIN gasracktypes ON (gasracks.type = gasracktypes.id
                        AND gasracktypes.deleted_at IS NULL)
            WHERE return_notells.datetime IS NOT NULL
                        AND return_notells.deleted_at IS NULL
                        AND return_notells.type = 'gr'
            UNION ALL
            SELECT
                'Return Note' AS model,
                return_noteuls.barcode AS barcode,
                return_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                return_noteuls.driver AS description,
                return_noteuls.datetime AS DATETIME,
                DATE(return_noteuls.created_at) AS created_at,
                DATE(return_noteuls.updated_at) AS updated_at,
                return_noteuls.dn_no AS dn_no,
                cylindercategories.code AS TYPE,
                return_notes.account_code AS accountcode,
                return_notes.name AS NAME
            FROM return_noteuls
            LEFT JOIN return_notes ON (return_noteuls.dn_no = return_notes.dn_no
                        AND return_notes.deleted_at IS NULL)
            LEFT JOIN cylinders ON (return_noteuls.serial = cylinders.serial
                        AND cylinders.deleted_at IS NULL)
            LEFT JOIN cylindercategories ON (cylinders.cat_id = cylindercategories.id
                        AND cylindercategories.deleted_at IS NULL)
            WHERE return_noteuls.datetime IS NOT NULL
                        AND return_noteuls.deleted_at IS NULL
                        AND return_noteuls.type = 'cy'
            UNION ALL
            SELECT
                'Return Note' AS model,
                return_noteuls.barcode AS barcode,
                return_noteuls.serial AS SERIAL,
                'UnLoading' AS ACTION,
                return_noteuls.driver AS description,
                return_noteuls.datetime AS DATETIME,
                DATE(return_noteuls.created_at) AS created_at,
                DATE(return_noteuls.updated_at) AS updated_at,
                return_noteuls.dn_no AS dn_no,
                gasracktypes.code AS TYPE,
                return_notes.account_code AS accountcode,
                return_notes.name AS NAME
            FROM return_noteuls
            LEFT JOIN return_notes ON (return_noteuls.dn_no = return_notes.dn_no
                        AND return_notes.deleted_at IS NULL)
            LEFT JOIN gasracks ON (return_noteuls.serial = gasracks.serial
                        AND gasracks.deleted_at IS NULL)
            LEFT JOIN gasracktypes ON (gasracks.type = gasracktypes.id
                        AND gasracktypes.deleted_at IS NULL)
            WHERE return_noteuls.datetime IS NOT NULL
                        AND return_noteuls.deleted_at IS NULL
                        AND return_noteuls.type = 'gr'
            ORDER BY DATETIME
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
