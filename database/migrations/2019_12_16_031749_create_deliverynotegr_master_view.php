<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverynotegrMasterView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW view_new_deliverynotegr_master
            AS
            SELECT
                delivery_note_grdts.dn_no AS dn_no,
                new_grview.dt_sqn AS dt_sqn,
                new_grview.list_sqn AS list_sqn,
                new_grview.prepare_date,
                new_grview.loading_date,
                new_grview.unloading_date,
                new_grview.barcode AS barcode,
                new_grview.serial AS serial,
                new_grview.driver AS driver
            FROM
                delivery_note_grdts
            JOIN new_grview ON delivery_note_grdts.dn_no = new_grview.dn_no
            GROUP BY dn_no, dt_sqn, list_sqn, serial
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_new_deliverynotegr_master");
    }
}
