<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterCodesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //DB::statement("DROP VIEW view_master_codes");
        DB::statement("
            CREATE VIEW view_master_codes
            AS
            SELECT
                debtors.id as d_id,
                'D' as type,
                CONCAT(debtors.accountcode) as accountcode,
                debtors.name AS name,
                debtors.tel AS d_tel,
                debtors.fax AS d_fax,
                debtors.cperson AS d_cperson,
                debtors.email AS d_email,
                debtors.address1 AS d_addr1,
                debtors.address2 AS d_addr2,
                debtors.address3 AS d_addr3,
                debtors.address4 AS d_addr4,
                debtors.ptype AS d_ptype,
                debtors.hp AS d_hp,
                debtors.cptel AS d_cptel,
                'DB' as d_dbcr,
                debtors.cterm AS d_cterm,
                debtors.climit AS d_climit,
                debtors.ccurrency AS d_currcode,
                debtors.gstno AS d_gstno,
                debtors.brnno AS d_brn,
                debtors.type AS d_type,
                CONCAT(CONCAT(debtors.accountcode), ' ', debtors.name) AS d_detail
            FROM debtors
            UNION ALL
            SELECT
                creditors.id as c_id,
                'C' as c_dc,
                CONCAT(creditors.accountcode) as accountcode,
                creditors.name AS c_name,
                creditors.tel AS c_tel,
                creditors.fax AS c_fax,
                creditors.cperson AS c_cperson,
                creditors.email AS c_email,
                creditors.address1 AS c_addr1,
                creditors.address2 AS c_addr2,
                creditors.address3 AS c_addr3,
                creditors.address4 AS c_addr4,
                creditors.ptype AS c_ptype,
                creditors.hp AS c_hp,
                creditors.cptel AS c_cptel,
                'CR' as c_dbcr,
                creditors.cterm AS c_cterm,
                creditors.climit AS c_climit,
                creditors.ccurrency AS c_currCode,
                creditors.gstno AS c_gstno,
                creditors.brnno AS c_brn,
                creditors.type AS c_type,
                CONCAT(CONCAT(creditors.accountcode), ' ', creditors.name) AS c_detail
            FROM creditors
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement("DROP VIEW view_master_codes");
    }
}
