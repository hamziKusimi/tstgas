<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProducttypeToProformainvoicedtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proformainvoicedts', function (Blueprint $table) {
            $table->string('type')->nullable();
            $table->string('product')->nullable();
            $table->dropColumn('item_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proformainvoicedts', function (Blueprint $table) {
            //
        });
    }
}
