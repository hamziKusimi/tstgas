<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_notes', function (Blueprint $table) {
            $table->bigIncrements('id');  
            $table->string('dn_no')->nullable();
            $table->date('date')->nullable();
            $table->string('account_code')->nullable(); 
            $table->string('name')->nullable();
            $table->string('addr1')->nullable();
            $table->string('addr2')->nullable();
            $table->string('addr3')->nullable();
            $table->string('addr4')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('lpono')->nullable();
            $table->string('driver')->nullable();
            $table->string('charge_type')->nullable();
            $table->string('start_from')->nullable();
            $table->text('header')->nullable(); 
            $table->text('footer')->nullable();
            $table->text('summary')->nullable();
            $table->string('vehicle_no')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_notes');
    }
}
