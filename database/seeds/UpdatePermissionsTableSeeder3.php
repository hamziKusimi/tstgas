<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UpdatePermissionsTableSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'VR',
            'CR',
            'UP',
            'DL',
            'PR',
            'RPT',
            'PRY'
        ];

        $groups = [
            'CY_INV',
            'CY_CB',
            'CY_PO',
            'CODE_REPLACEMENT',
            'DRIVER',
            'CY_RP_TRACKING',
            'CY_RP_MOVEMENT',
            'CY_RP_LEDGER',
            'CY_RP_RENTAL',
            'CY_RP_LOCATION',
            'CY_RP_HOLDING',
            'CY_RP_CACTIVITY',
            'CY_RP_INVRET',
            'RACK_RP_ACTIVITY',
            'RACK_RP_LEDGER',
            'PROJECT_LISTING'
        ];

        foreach ($groups as $group){
            foreach ($permissions as $permission){
                // Goods View / Goods Edit
                $name = $group.'_'.$permission;
                DB::table('permissions')->insert([
                    'name' => $name,
                    'guard_name' => 'web'
                ]);
            }
        }

        $getPermissions = Permission::all();
        $roleSA = Role::first();

        foreach($getPermissions as $getPermission) {

           $roleSA->givePermissionTo($getPermission);

        }
    }

}
