<?php

use Illuminate\Database\Seeder;

use App\Model\DocumentSetup;

class DocumentSetupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('document_setups')->insert([
            ['D_NAME' => 'Goods Received', 'D_PREFIX' => 'GR', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Purchase Order', 'D_PREFIX' => 'PR', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Purchase Returns', 'D_PREFIX' => 'PR', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Cash Bill', 'D_PREFIX' => 'CB', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Invoice Sales', 'D_PREFIX' => 'IS', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Delivery Order', 'D_PREFIX' => 'DO', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Sales Order', 'D_PREFIX' => 'SO', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Quotation', 'D_PREFIX' => 'QT', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Sales Return', 'D_PREFIX' => 'SR', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Adjustment In', 'D_PREFIX' => 'AI', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Adjustment Out', 'D_PREFIX' => 'AO', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ['D_NAME' => 'Cash Sales', 'D_PREFIX' => 'CS', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],           
            ]);

        if (config('config.cylinder.cylinder') == "true") {

            DB::table('document_setups')->insert([
                ['D_NAME' => 'Return Note', 'D_PREFIX' => 'RN', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Delivery Note', 'D_PREFIX' => 'DN', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Refill', 'D_PREFIX' => 'RF', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Repair', 'D_PREFIX' => 'RP', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Cylinder Invoice', 'D_PREFIX' => 'CYI', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Sales Invoice', 'D_PREFIX' => 'SI', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Cylinder Rental Invoice', 'D_PREFIX' => 'CRI', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Rack Rental Invoice', 'D_PREFIX' => 'RRI', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
            ]);
        }
    }
}
