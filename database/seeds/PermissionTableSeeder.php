<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'VR',
            'CR',
            'UP',
            'DL',
            'PR',
            'RPT',
            'PRY'
        ];
        
        $groups = [
            'GOOD_RC',
            'PURCHASE_OD',
            'PURCHASE_RT',
            'CASH_BILL',
            'CASH_SALES',
            'INVOICE',
            'DELIVERY_OD',
            'SALES_OD',
            'QUOTATION',
            'ADJUSTMENT_IN',
            'ADJUSTMENT_OUT',
            'SALES_RETURN',
            'DELIVERY_NT',
            'RETURN_NT',
            'REFILL',
            'REPAIR',
            'INVENTORY',
            'IV_MD_STOCK',
            'IV_MD_CATEGORY',
            'IV_MD_PRODUCT',
            'IV_MD_BRAND',
            'IV_MD_LOCATION',
            'IV_MD_UOM',
            'IV_MD_CREDITOR',
            'IV_MD_DEBTOR',
            'IV_MD_ADJUSTMENT_I',
            'IV_MD_ADJUSTMENT_O',
            'IV_MD_REASON',
            'IV_MD_AREA',
            'IV_MD_CURRENCY',
            'IV_MD_CUSTOMER',
            'IV_MD_SALESMAN',
            'CYLINDER',
            'CY_CATEGORY',
            'CY_GROUP',
            'CY_PRODUCT',
            'CY_TYPE',
            'CY_HANDWHEEL',
            'CY_VALVE',
            'CY_MANUFACTURER',
            'CY_OWNERSHIP',
            'GS_RACK',
            'GS_RACK_TYPE',
            'SLING',
            'SLING_TYPE',
            'SHACKLE',
            'SHACKLE_TYPE',
            'SYSTEM_SETUP',
            'DOC_SETUP',
            'USER_SETUP',
            'UG_SETUP',
            'DEBTOR_LISTING',
            'CREDITOR_LISTING',
            'STOCK_LISTING',
            'STOCK_BALANCE',
            'STOCK_LEDGER',
            'STOCK_VAL_FIFO',
            'STOCK_VAL_ASAT',
            'STOCK_MOVEMENT',
            'PEMBEKAL',
            'PERUNCIT',
            'PROCESSING',
            'STOCK_CUST_ENQ',
            'STOCK_SUPP_ENQ'
        ];

        $roles = [
            'SA'
        ];
        
        foreach ($groups as $group){
            foreach ($permissions as $permission){
                // Goods View / Goods Edit
                $name = $group.'_'.$permission;
                // DB::table('permissions')->insert($name);
                DB::table('permissions')->insert([
                    'name' => $name,
                    'guard_name' => 'web'
                ]);
            }
        }
        DB::table('permissions')->insert([
           ['name' => 'INVENTORY_AS', 'guard_name' => 'web',],
           ['name' => 'INVENTORY_MD_AS', 'guard_name' => 'web',],
           ['name' => 'INVENTORY_RPT_AS', 'guard_name' => 'web',],
           ['name' => 'CYLINDER_AS', 'guard_name' => 'web',],
           ['name' => 'CYLINDER_MD_AS', 'guard_name' => 'web',],
           ['name' => 'SETUP_AS', 'guard_name' => 'web',],
           ['name' => 'MOD_AFT_PR', 'guard_name' => 'web',],
           ['name' => 'ACC_MIN_PRICE', 'guard_name' => 'web',],
        ]);

        DB::table('roles')->insert([
            'name' => 'SA',
            'guard_name' => 'web'
        ]);
        
        $getPermissions = Permission::all();
        $roleSA = Role::first();

        foreach($getPermissions as $getPermission) { 

           $roleSA->givePermissionTo($getPermission);

        }

        $userSA = User::first();
        $userSA->assignRole('SA');
    }
}
