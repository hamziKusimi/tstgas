<?php

use Illuminate\Database\Seeder;

use App\Model\Uom;

class InsertTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('uoms')->insert([
            ['id' => '1', 'code' => 'UNIT', 'descr' => 'UNIT',],
            ['id' => '2', 'code' => 'CYLINDER', 'descr' => 'CYLINDER',],
            ['id' => '3', 'code' => 'LENGTH', 'descr' => 'LENGTH',],
        ]);

        // DB::table('debtors')->insert([
        //     ['id' => '1', 'code' => 'UNIT', 'descr' => 'UNIT',],
        //     ['id' => '2', 'code' => 'CYLINDER', 'descr' => 'CYLINDER',],
        //     ['id' => '3', 'code' => 'LENGTH', 'descr' => 'LENGTH',],
        // ]);
    }
}
