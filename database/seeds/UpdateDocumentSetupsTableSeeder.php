<?php

use Illuminate\Database\Seeder;

use App\Model\DocumentSetup;

class UpdateDocumentSetupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (config('config.cylinder.cylinder') == "true") {

            DB::table('document_setups')->insert([
                ['D_NAME' => 'Proforma Invoice', 'D_PREFIX' => 'PI', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],
                ['D_NAME' => 'Cylinder Quotation', 'D_PREFIX' => 'CQT', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],    
            ]);
        }
    }
}
