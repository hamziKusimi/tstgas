<?php

use Illuminate\Database\Seeder;
use App\Model\SystemSetup;

class SystemSetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('system_setups')->insert([
            [
                'id' => '1', 'code' => config('config.company.company_no'),
                'name' => config('config.company.name'),
                'company_no' => config('config.company.company_no'),
                'uom1' => '1',
                'custom1' => 'License No',
                'custom2' => 'Auto Invoice Generation',
                'custom3' => 'Generate Seperate Invoice per PO/DN',
                'custom1_type' => 'text',
                'custom2_type' => 'checkbox',
                'custom3_type' => 'checkbox',
                'bank_detail' => 'Public Islamic Bank Berhad - Labuan Branch',
                'bank_account_no' => '399-059-000-7'
            ]
        ]);
    }
}
