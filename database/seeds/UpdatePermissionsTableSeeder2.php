<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UpdatePermissionsTableSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'VR',
            'CR',
            'UP',
            'DL',
            'PR',
            'RPT',
            'PRY'
        ];

        $groups = [
            'AP_SETUP',
            'STOCK_CODE_ENQ',
            'CYL_ENQ'
        ];

        foreach ($groups as $group){
            foreach ($permissions as $permission){
                // Goods View / Goods Edit
                $name = $group.'_'.$permission;
                DB::table('permissions')->insert([
                    'name' => $name,
                    'guard_name' => 'web'
                ]);
            }
        }

        $getPermissions = Permission::all();
        $roleSA = Role::first();

        foreach($getPermissions as $getPermission) {

           $roleSA->givePermissionTo($getPermission);

        }
    }

}
