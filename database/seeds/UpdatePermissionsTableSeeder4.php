<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UpdatePermissionsTableSeeder4 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'VR',
            'CR',
            'UP',
            'DL',
            'PR',
            'RPT',
            'PRY'
        ];

        $groups = [
            'AUDIT_TRAIL_ENQ'
        ];

        foreach ($groups as $group){
            foreach ($permissions as $permission){
                $name = $group.'_'.$permission;
                DB::table('permissions')->insert([
                    'name' => $name,
                    'guard_name' => 'web'
                ]);
            }
        }

        $getPermissions = Permission::all();
        $roleSA = Role::first();

        foreach($getPermissions as $getPermission) {

           $roleSA->givePermissionTo($getPermission);

        }
    }

}
