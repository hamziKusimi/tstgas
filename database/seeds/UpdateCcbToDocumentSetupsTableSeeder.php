<?php

use Illuminate\Database\Seeder;

class UpdateCcbToDocumentSetupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('config.cylinder.cylinder') == "true") {

            DB::table('document_setups')->insert([
                ['D_NAME' => 'Cylinder Cashbill', 'D_PREFIX' => 'CCB', 'D_SEPARATOR' => '/', 'D_LAST_NO' => '0', 'D_ZEROES' => '8', 'D_ACTIVE' => '1',],   
            ]);
        }
    }
}
