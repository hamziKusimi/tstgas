<?php
include_once("config.php");

$pdo = new PDO($dsn, $uid, $pass, $opt);
$err = "";
$alert = "";

$strdocno = 'SR/00000004';
$stracode = 'true';

try {
    $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);
} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
    echo "error 15";
}

foreach ($syspec as $syspecitem) {
    $tx_roundac = $syspecitem->s_roundac;
    $tx_gstcc = $syspecitem->s_gstcc;
    $s_cndetail = $syspecitem->s_cndetail;
}

try {
    $gTaxCode = $pdo->query("SELECT * FROM taxcode ORDER BY t_code")->fetchAll(PDO::FETCH_OBJ);
} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
    echo "error 28";
}

$tt_round = 0.00;
$tt_sn = "0005";

// remove existing transaction
if (isset($strdocno)) {
    try {
        $stmt = $pdo->prepare('DELETE FROM TRN WHERE t_docno=:t_docno');
        $stmt->execute(array(':t_docno' => $strdocno));
        $affected_rows = $stmt->rowCount();
    } catch(PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 41";
    }
}

// To check if the Cr.A/C is Brought Forward or Open Item type
try {
    $stmt = $pdo->prepare("SELECT * FROM mastercode WHERE d_acode=:d_acode");
    $stmt->execute(array(':d_acode' => $stracode));
    $mastercodex = $stmt->fetchAll(PDO::FETCH_OBJ);
} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
    echo "error 52";
}

foreach($mastercodex as $mCodexItem){
    $d_dc = $mCodexItem->d_dc;
    switch($d_dc){
        case "D":
            $tx_dbcr = "DB";
            $tx_ptype = $mCodexItem->d_ptype;

            $tt_dType = "CN";

        break;

        case "C":
            $tx_dbcr = "CR";
            $tx_ptype = $mCodexItem->d_ptype;

            $tt_dType = "SC";
        break;

        default:
            $tx_dbcr = "DB";
            $tx_ptype = $mCodexItem->d_ptype;

            $tt_dType = "CN";
        break;
    }
}

// Debit GL A/C
$stmt = $pdo->prepare(
    "SELECT *, taxcode.t_type as t_taxtype
    FROM acc_cnmt
    INNER JOIN acc_cndt
    ON acc_cnmt.m_docno = acc_cndt.t_docno
    LEFT JOIN taxcode ON acc_cndt.t_taxcode = taxcode.t_code
    WHERE acc_cnmt.m_docno=:strdocno and (TRIM(acc_cndt.t_drac) <> '-' AND TRIM( acc_cndt.t_drac) <> '')
    ORDER BY t_sn ASC");

$stmt->execute(array(':strdocno' => $strdocno));
$affected_rows = $stmt->rowCount();
$appendx = $stmt->fetchAll(PDO::FETCH_OBJ);

foreach($appendx as $item){

    $tt_docno = $item->m_docno;
    $tt_date = $item->M_DATE;
    $tt_gdate = $item->m_gdate;
    $tt_batch = $item->M_BATCH;
    $tt_round = $item->m_round;

    $tt_sn = $item->T_SN;
    $tt_drac = $item->T_DRAC;
    $tt_subject = $item->T_SUBJECT;
    $tt_amt = $item->T_AMT;
    $tt_bill = $item->t_bill;
    $tt_paybill = $item->t_paybill;
    $tt_exrate = $item->T_EXRATE;
    $tt_taxcode = $item->t_taxcode;
    $tt_taxrate = $item->t_taxrate;
    $tt_taxamt = $item->t_taxamt;
    $tt_taxtype = $item->t_taxtype;
    $tt_type = prgdbcrgltype($tt_drac, $pdo);


    foreach ($gTaxCode as $gtaxcodeItem) {
        if ($gtaxcodeItem->t_code == $tt_taxcode) {
            $tt_iocode = $gtaxcodeItem->t_acode;
        break;
        }else{
            $tt_iocode = "";
        }
    }

    if ($tx_gstcc == 1) {
        $tx_cc = trim(substr($tt_drac, 5,0));
        $tx_gstoac = substr($tt_iocode, 0,5).$tx_cc;
        $tx_roundac = substr($tx_roundac, 0,5).$tx_cc;
    }

    // To check if the Dr.A/C is Brought Forward or Open Item type
    try {
        $mastercodex = $pdo->query("SELECT * FROM mastercode WHERE d_acode= '$tt_drac'")->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 141";
    }

    foreach($mastercodex as $mt){
        switch($mt->d_dc){
            case "D":
                $tt_dbcr = "DB";
                $tt_ptype = $mt->d_ptype;
            break;
            case "C":
                $tt_dbcr = "CR";
                $tt_ptype = $mt->d_ptype;
            break;
            default:
                $tt_dbcr = "GL";
                $tt_ptype = " ";
            break;
        }
    }

    try {
        $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
        $stmt->execute();
    } catch(PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 167";
    }

    switch($tt_dbcr){
        case "DB":
            if($tt_ptype == "B"){
                // If Debtor is Brought Forward type
			    $tt_bill = str_repeat(" ", 20);
            }elseif($tt_ptype == "O"){
                // If Debtor is Open Item type ***
                $tt_bill = $tt_bill;
            }
        break;
        case "CR":
            if($tt_ptype == "B"){
                // If Creditor is Brought Forward type
			$tt_bill = str_repeat(" ", 20);
            }elseif($tt_ptype == "O"){
                // If Creditor is Open Item type
                $tt_paybill = $tt_paybill;
            }
        break;
        default:
            $tt_bill = "";
            $tt_paybill = "";
        break;
    }

    if($tt_amt > 0.00){
        $tt_dramt  = round($tt_amt * $tt_exrate,2);
		$tt_cramt  = '0.00';
		$tt_drcurr = $tt_amt;
        $tt_crcurr = '0.00';

        $tx_gstDrAmt = round($tt_taxamt*$tt_exrate,2);
		$tx_gstDrCurr = $tt_taxamt;
		$tx_gstCrAmt = '0.00';
		$tx_gstCrCurr = '0.00';
    }else{
        $tt_dramt  = '0.00';
		$tt_cramt  = abs(round($tt_amt * $tt_exrate,2));
		$tt_drcurr = '0.00';
		$tt_crcurr = abs($tt_amt);

		$tx_gstDrAmt = '0.00';
		$tx_gstDrCurr = '0.00';
		$tx_gstCrAmt = abs(round($tt_taxamt*$tt_exrate,2));
		$tx_gstCrCurr = $tt_taxamt;
    }


    try {
        $ttt_drac   = prgdbcrgltype($tt_drac, $pdo);
        $ttdate     = date("m/Y", strtotime($tt_date));
        $tttaxamt	= abs($tt_taxamt);

        $stmt = $pdo->prepare("UPDATE trn
                            SET
                            t_mtype 	='$tt_dType',
                            t_batch 	='$tt_batch',
                            t_term 		='$ttdate',
                            t_type 		='$ttt_drac',
                            t_mc 		=13,
                            t_date 		='$tt_date',
                            t_gdate 	='$tt_date',
                            t_acode 	='$tt_drac',
                            t_descr 	=' $tt_subject',
                            t_bill		='$tt_bill',
                            t_paybill	='$tt_paybill',
                            t_drcurr	='$tt_drcurr',
                            t_crcurr	='$tt_crcurr',
                            t_dramt 	='$tt_dramt',
                            t_cramt 	='$tt_cramt',
                            t_exrate	='$tt_exrate',
                            t_taxcode 	='$tt_taxcode',
                            t_taxamt	='$tttaxamt',
                            t_taxrate 	='$tt_taxrate',
                            t_import 	='Y',
                            t_modiuser  ='PHPSYS',
                            t_modidate 	=NOW()

                            WHERE t_docno='$tt_docno'
							AND t_sn='$tt_sn'");
        $stmt->execute();
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 254";
    }

	// input/output tax
    if ( !empty($tt_taxamt) ) {
        $tx_sn = str_pad(trim(strval(intval($tt_sn)+1000)),4,"0");

        try {
            $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                        VALUES ('$tt_docno','$tx_sn','PHPSYS',NOW())");
            $stmt->execute();
        } catch(PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 268";
        }

        try {
            $ttdate     = date("m/Y", strtotime($tt_date));
            $tttaxamt	= abs($tt_taxamt);

            $stmt = $pdo->prepare("UPDATE trn
                                SET
                                t_mtype 	='$tt_dType',
                                t_batch 	='$tt_batch',
                                t_term 		='$ttdate',
                                t_type 		='$tt_type',
                                t_mc 		=13,
                                t_date 		='$tt_date',
                                t_gdate 	='$tt_gdate',
                                t_acode 	='$tt_iocode',
                                t_descr 	='$tt_subject',
                                t_dramt 	='$tx_gstDrAmt',
                                t_cramt 	='$tx_gstCrAmt',
                                t_drcurr	='$tx_gstDrCurr',
                                t_crcurr	='$tx_gstCrCurr',
                                t_exrate	='$tt_exrate',
                                t_import 	='Y',
                                t_modiuser  ='PHPSYS',
                                t_modidate 	=NOW()

                                WHERE t_docno='$tt_docno'
								AND t_sn='$tx_sn'");
            $stmt->execute();
        } catch (PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 299";
        }
    }
}

// for rounding adj
if($tt_round > 0.00){
    $tx_sn = str_pad(trim(strval(intval($tt_sn)+1500)),4,"0");

    try {
        $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                        VALUES ('$tt_docno','$tx_sn','PHPSYS',NOW())");
        $stmt->execute();
    } catch(PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 314";
    }

    try {
        $ttdate     = date("m/Y", strtotime($tt_date));
        $tdramt 	= ($tt_round>0.00)? $tt_round : '0.00';
        $tcramt 	= ($tt_round<0.00)? abs($tt_round) : '0.00';
        $tdrcurr 	= sprintf("%0.2f",round($tt_exrate * (($tt_round>0.00)? $tt_round : 0.00),2));
        $tcrcurr 	= sprintf("%0.2f",round($tt_exrate * (($tt_round<0.00)? abs($tt_round) : 0.00),2));

        $stmt = $pdo->prepare("UPDATE trn SET
                            t_mtype 	='$tt_dType',
                            t_batch 	='$tt_batch',
                            t_term 		='$ttdate',
                            t_type 		='$tt_type',
                            t_mc 		=13,
                            t_date 		='$tt_date',
                            t_gdate 	='$tt_gdate',
                            t_acode 	='$tx_roundac',
                            t_descr 	='$tt_subject',
                            t_dramt 	='$tdramt',
                            t_cramt 	='$tcramt',
                            t_drcurr	='$tdrcurr',
                            t_crcurr	='$tcrcurr',
                            t_exrate	='$tt_exrate',
                            t_import 	='Y',
                            t_modiuser  ='PHPSYS',
                            t_modidate 	=NOW()

                            WHERE t_docno='$tt_docno'
							AND t_sn='$tx_sn'");
        $stmt->execute();
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 348";
    }
}

// Credit debtor/creditor A/C
try {
    $acc_cnmtx = $pdo->query("SELECT * FROM acc_cnmt WHERE m_docno='$strdocno'")->fetchAll(PDO::FETCH_OBJ);
    foreach($acc_cnmtx as $item){
        $tt_billdebtor = $item->M_BILLDEBTOR;
    }

} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
    echo "error 359";
}

// try {
//     $acc_cndtx = $pdo->query("SELECT * FROM acc_cndt WHERE t_docno='$strdocno'")->fetchAll(PDO::FETCH_OBJ);
// } catch (PDOException $ex) {
//     $err = $err . $ex->getMessage() . "<br/>";
//     echo "error 365";
// }


switch($tx_dbcr){
    case "CR":
        if($tx_ptype == "B"){
            if($s_cndetail == 0){
                try {
                    // $stmt = $pdo->prepare("SELECT acc_cnmtx.m_docno, acc_cnmtx.m_date, acc_cnmtx.m_gdate, acc_cnmtx.m_batch,
                    //     acc_cnmtx.m_crac, acc_cnmtx.m_name, acc_cnmtx.m_ra, '2005' as t_sn, acc_cnmtx.m_gldescr as t_subject,
                    //     acc_cndtx.t_amtt, str_repeat('&nbsp;', 20) as t_bill, str_repeat('&nbsp;', 20) as t_paybill, acc_cndtx.t_exrate

                    //     FROM acc_cnmtx
                    //     INNER JOIN acc_cndtx ON acc_cnmtx.m_docno = acc_cndtx.t_docno

                    //     where acc_cnmtx.m_docno=:strdocno and TRIM('-' from acc_cndtx.t_drac or empty(acc_cndtx.t_drac)) and acc_cndtx.t_amt <> 0.00
                    //     INTO CURSOR appendx;
                    //     ORDER BY acc_cndtx.t_sn ASC");

                    $stmt = $pdo->prepare("
                    SELECT m_docno, m_date, m_gdate, m_batch, m_Crac, m_name,m_ra
                            '2005' as t_sn, t_subject, t_bill, SPACE(20) AS t_paybill, MAX(t_exrate) t_exrate,
                            SUM(t_amtt) AS t_amtt
                    FROM acc_cnmt INNER JOIN acc_cndt ON (m_docno = t_docno)
                    WHERE m_docno = '$strdocno' AND TRIM(t_drac = '-' OR t_drac = '') AND acc_cndtx.t_amt <> 0.00
                    GROUP BY m_docno, m_date, m_gdate, m_batch, m_crac, m_name,m_ra, t_sn, t_subject, t_bill, t_paybill
                    ");
                    $stmt->execute();
                    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 394";
                }
            }else{
                try {
                    $stmt = $pdo->prepare("SELECT *
                                            FROM acc_cnmt
                                            INNER JOIN acc_cndt ON m_docno = t_docno
                                            WHERE m_docno='$strdocno' AND trim(t_drac = '-' OR t_drac = '') and t_amt <> 0.00 ");
                    $stmt->execute();
                    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 409";
                }
            }

        }elseif($tx_ptype == "O"){
            // try {
            //     $stmt = $pdo->prepare("SELECT *
            //                             FROM acc_cnmtx
            //                             INNER JOIN acc_cndtx ON m_docno = t_docno
            //                             INTO CURSOR checkx;
            //                             WHERE t_amt <> 0.00 and (trim(t_drac) == '-' or trim(t_drac) == '') and !EMPTY(acc_cndtx.t_bill)");
            //     $stmt->execute(array());
            //     $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
            // } catch (PDOException $ex) {
            //     $err = $err . $ex->getMessage() . "<br/>";
            // }

            if($s_cndetail == 0){
                try {
                    $stmt = $pdo->prepare("
                    SELECT m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_gldescr AS t_subject,m_tamtt,m_ra,
                            m_docno AS t_bill,SPACE(20) AS t_paybill,t_exrate,SUM(t_amtt) t_amtt
                    FROM acc_cnmt INNER JOIN acc_cndt ON (m_docno = t_docno)
                    WHERE m_docno='$strdocno' AND (TRIM(t_drac) = '-' OR TRIM(t_drac) = '') AND t_amt <> 0.00
                    GROUP BY m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_gldescr,m_tamtt,m_ra,t_exrate
                    ");
                    $stmt->execute();
                    $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 439";
                }

                if($stmt->rowCount() > 1){
                    $appendx = $appendx2;
                }else{
                    foreach($appendx2 as $item){
                        $item->t_amtt = $item->m_tamtt;
                    }
                    $appendx = $appendx2;
                }

            }else{
                try {
                    $stmt = $pdo->prepare("SELECT *
                                            FROM acc_cnmt
                                            INNER JOIN acc_cndt ON m_docno = t_docno
                                            WHERE m_docno='$strdocno' AND (trim(t_drac) = '-' OR t_drac = '') AND t_amt <> 0.00
                                            ");
                    $stmt->execute();
                    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 462";
                }
            }

        }
        break;

    case "DB":
        if($tx_ptype == "B"){
            try {
                if($s_cndetail == 0){

                            $stmt = $pdo->prepare("SELECT m_docno,
                                                m_date,
                                                m_gdate,
                                                m_batch,
                                                m_crac,
                                                m_name,
                                                m_ra,
                                                '2005' AS t_sn,
                                                m_gldescr AS t_subject,
                                                t_amtt,
                                                SPACE(20) AS t_bill,
                                                SPACE(20) AS t_paybill,
                                                t_exrate, MAX(t_exrate) AS t_exrate, SUM(t_amtt) AS t_amtt
                                            FROM acc_cnmt
                                            INNER JOIN acc_cndt ON m_docno = t_docno
                                            WHERE m_docno='$strdocno' AND (TRIM(t_drac) = '-' OR t_drac = '') AND t_amt <> 0.00
                                            GROUP BY m_docno, m_date, m_gdate, m_batch, m_crac,
                                            m_name, m_ra, t_sn, t_subject, t_bill, t_paybill
                                                    ");
                            $stmt->execute();
                            $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);

                }else{

                            $stmt = $pdo->prepare("SELECT *
                                                    FROM acc_cnmt
                                                    INNER JOIN acc_cndt ON m_docno = t_docno
                                                    WHERE m_docno='$strdocno' AND (trim(t_drac) = '-' OR t_drac = '') AND t_amt <> 0.00
                                                    ");
                            $stmt->execute();
                            $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);

                }
                break;
            } catch (PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 510";
            }
        }elseif($tx_ptype == "O" && $tt_billdebtor = "Y"){
            if($s_cndetail == 0){
                try {
                    $stmt = $pdo->prepare("
                    SELECT m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_gldescr AS t_subject,m_tamtt,m_ra,
                            m_docno AS t_bill,SPACE(20) AS t_paybill,t_exrate,SUM(t_amtt) t_amtt
                    FROM acc_cnmt INNER JOIN acc_cndt ON (m_docno = t_docno)
                    WHERE m_docno='$strdocno' AND (TRIM(t_drac) = '-' OR TRIM(t_drac) = '') AND t_amt <> 0.00
                    GROUP BY m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_gldescr,m_tamtt,m_ra,t_exrate
                    ");
                    $stmt->execute();
                    $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 526";
                }

                if($stmt->rowCount() > 1){
                    $appendx = $appendx2;
                }else{
                    $appendx = [];
                    foreach($appendx2 as $item){
                        $item->t_amtt = $item->m_tamtt;
                    }
                }

            }else{
                try {
                    $stmt = $pdo->prepare("SELECT *
                                            FROM acc_cnmt
                                            INNER JOIN acc_cndt ON m_docno = t_docno
                                            WHERE m_docno='$strdocno' AND (trim(t_drac) = '-' OR t_drac = '') AND t_amt <> 0.00
                                            ");
                    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 548";
                }
            }
        }elseif($tx_ptype == "O"){
            if ($s_cndetail == 0){

                    $stmt = $pdo->prepare("
                    SELECT m_docno, m_date, m_gdate, m_batch,
                                                m_crac, m_name, m_gldescr AS t_subject, m_tamtt,
                                                m_ra, t_bill, t_paybill,
                                                t_exrate, SUM(t_amtt) t_amtt
                                            FROM acc_cnmt
                                            INNER JOIN acc_cndt ON m_docno = t_docno
                                            WHERE m_docno='$strdocno' AND (TRIM(t_drac) = '-' OR t_drac = '') AND t_amt <> 0.00
                                                AND ((TRIM(t_paybill) <> '' OR t_paybill IS NOT NULL) OR (TRIM(t_bill) <> '' OR t_bill IS NOT NULL))
                                            GROUP BY m_docno,m_date,m_gdate,m_batch,m_crac, m_name, m_gldescr, m_tamtt, m_ra,
                                             t_bill, t_paybill, t_exrate
                    ");
                    $stmt->execute();
                    $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);


                    if($stmt->rowCount() > 1){
                        $appendx = $appendx2;
                    }else{
                        foreach($appendx2 as $item){
                            $item->t_amtt = $item->m_tamtt;
                        }
                        $appendx = $appendx2;
                    }
            }else{
                try {
                    $stmt = $pdo->prepare("
                    SELECT m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_ra,
                        t_subject,t_bill,t_paybill,t_exrate,SUM(t_amtt) t_amtt
                    FROM acc_cnmt INNER JOIN acc_cndt ON (m_docno = t_docno)
                    WHERE m_docno='$strdocno' AND (TRIM(t_drac) = '-' OR t_drac = '') AND t_amt <> 0.00 AND
                    ( (TRIM(t_paybill) <> '' OR t_paybill IS NOT NULL) OR (TRIM(t_bill) <> '' OR t_bill IS NOT NULL))
                    GROUP BY m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_round,m_ra,t_subject,t_bill,t_paybill,t_exrate
                    ");
                    $stmt->execute();
                    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 591";
                }
            }
        }
    break;
}






$tx_sn = '2000';


foreach ($appendx as $appndx){
    $tt_docno = $appndx->m_docno;
	$tt_date = $appndx->M_DATE;
	$tt_gdate = $appndx->m_gdate;
	$tt_batch = $appndx->M_BATCH;
	$tt_crac = $appndx->M_CRAC;
	$tt_name = $appndx->M_NAME;
	$tx_sn = str_pad( $tx_sn + 5, 4, "0", STR_PAD_LEFT );
	$tt_subject = !empty($appndx->t_subject) ? $appndx->t_subject : $tt_docno;
	$tt_amt = $appndx->t_amtt;
	$tt_bill = $appndx->t_bill;
	$tt_paybill = $appndx->t_paybill;
	$tt_exrate = $appndx->T_EXRATE;
    $tt_ra = $appndx->m_ra;

    $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
    VALUES ('$tt_docno','$tx_sn','PHPSYS',NOW())");
    $stmt->execute();

    switch($tx_dbcr){
        case "CR":
            if($tx_ptype == "B"){
                //If Creditor is Brought Forward type
                $tt_bill = str_repeat(" ", 20);
            }elseif($tx_ptype = "O"){
                //If Creditor is Open Item type
                $tt_bill = $tt_bill;
            }
            break;
        case "DB":
            if($tx_ptype == "B"){
                //If Creditor is Brought Forward type
                str_repeat(" ", 20);
            }elseif($tx_ptype = "O"){
                if($tt_billdebtor == "Y"){
                    //If Creditor is Open Item type
                    $tt_bill = $tt_bill;
                }else{
                    //If Creditor is Open Item type
                    $tt_paybill = $tt_paybill;
                }
            }
            break;
        default:
            $tt_paybill = "";
            $tt_bill = "";
            break;
    }

    if($tt_amt > 0.00){
        $tt_dramt  = 0.00;
        $tt_cramt  = sprintf("%0.2f", round($tt_amt * $tt_exrate,2));
        $tt_drcurr = 0.00;
        $tt_crcurr = $tt_amt;
    }else{
        $tt_dramt  = sprintf("%0.2f", abs(round($tt_amt * $tt_exrate,2)));
        $tt_cramt  = 0.00;
        $tt_drcurr = abs($tt_amt);
        $tt_crcurr = 0.00;
    }

    if($tt_ra == 1 && count($appendx2) == 1){
        $tt_dramt  = round($tt_dramt);
        $tt_cramt  = round($tt_cramt);
    }

    $ttt_crac = prgdbcrgltype($tt_crac, $pdo);

    try {
        $ttdate = date("m/Y", strtotime($tt_date));
        $tmc = 13;
        $timport = 'Y';
        $stmt = $pdo->prepare("UPDATE trn
                        SET
                        t_mtype 	='$tt_dType',
                        t_batch 	='$tt_batch',
                        t_term 		='$ttdate',
                        t_type 		='$ttt_crac',
                        t_mc 		=13,
                        t_date 		='$tt_date',
                        t_gdate 	='$tt_gdate',
                        t_acode 	='$tt_crac',
                        t_descr 	='$tt_subject',
                        t_bill		='$tt_bill',
                        t_paybill	='$tt_paybill',
                        t_drcurr	='$tt_drcurr',
                        t_crcurr	='$tt_crcurr',
                        t_dramt 	='$tt_dramt',
                        t_cramt 	='$tt_cramt',
                        t_exrate	='$tt_exrate',
                        t_import 	='Y',
                        t_modiuser  ='PHPSYS',
                        t_modidate 	=NOW()

                        WHERE t_docno='$tt_docno'
						AND t_sn='$tx_sn'");
        $stmt->execute();
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 706";
    }
}


// mark as post
try {
    $stmt = $pdo->prepare('UPDATE acc_cnmt SET m_post =:m_post WHERE m_docno=:m_docno');
    $stmt->execute(array(':m_post' => 'Y', ':m_docno' => $strdocno ));
} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
    echo "error 713";
}


