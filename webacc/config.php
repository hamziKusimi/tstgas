<?php

    date_default_timezone_set('Asia/Kuala_Lumpur');

    $host = "tsagency.dyndns.biz:3306";
    $db = "tr_ts2020";
    $uid = "root";
    $pass = 234586;
    $charset = "utf8mb4";

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_LAZY,
        PDO::ATTR_EMULATE_PREPARES   => false,
    );

	//date conversion from dd/mm/yyyy to yyyy/mm/dd
	function YMD2DMY($target) {
		$arr = explode("/",$target);
		return $arr[2]."/".$arr[1]."/".$arr[0];
    }

	//date conversion from yyyy/mm/dd to dd/mm/yyyy
	function DMY2YMD($target) {
	    $arr = explode("/",$target);
		return $arr[2]."/".$arr[1]."/".$arr[0];
    }

    function addDayswithdate($date,$days){
        $date = strtotime($days." days", strtotime($date));
        return  date("Y-m-d", $date);
    }

    function isAllow($mod) {
        return isset($_SESSION["u_perm"]) && strstr($_SESSION["u_perm"],$mod);
        //return true;
    }

    function hr2str($hrs,$type) {

        switch($type) {
            case 1:
                return intval($hrs);
                break;
            case 2:
                return round(($hrs - intval($hrs))*60,0);
                break;
            default:
                if (intval($hrs) <> 0) {
                    $part1 = intval($hrs) . " Hrs ";
                } else {
                    $part1 = "";
                }
                if ($hrs - intval($hrs) <> 0) {
                    $part2 = round(($hrs - intval($hrs)) * 60,0) . " mins ";
                } else {
                    $part2 = "";
                }

                return $part1 . $part2;
        }
    }

    function prgdbcrgltype($stracode, $pdo){
        $stmt = $pdo->prepare(
            "SELECT * FROM debtor WHERE d_acode = :stracode
            ");
        $stmt->execute(array(':stracode' => $stracode));

        if($stmt->rowCount() > 0){
            return "2";
        }

        $stmt2 = $pdo->prepare(
            "SELECT * FROM Creditor WHERE c_acode =  :stracode
            ");
        $stmt2->execute(array(':stracode' => $stracode));

        if($stmt2->rowCount() > 0){
            return "2";
        }

        $stmt3 = $pdo->prepare(
            "SELECT * FROM Glmt WHERE m_acode = :stracode
            ");
        $stmt3->execute(array(':stracode' => $stracode));

        if($stmt3->rowCount() > 0){
            return "1";
        }

        return "1";
    }

    function getPType($stracode, $pdo){

        $stmt = $pdo->prepare(
            "SELECT d_dc,d_acode,d_ptype FROM debtor where d_acode = '$stracode'
             UNION ALL
             SELECT c_dc as d_dc,c_acode AS d_acode,C_PTYPE AS d_ptype FROM creditor where c_acode = '$stracode'
             UNION ALL
             SELECT 'M' AS d_dc,M_ACODE AS d_ACODE,'' AS d_ptype FROM glmt where m_acode =  '$stracode'
            ");
        $stmt->execute();
        $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);

        if($stmt->rowCount() > 0){
            foreach($appendx as $item){
                if($item->d_dc == "D" || $item->d_dc == "C"){
                    return $item->d_ptype;
                }
            }
            return "B";
        }else{
            return "B";
        }
    }


?>