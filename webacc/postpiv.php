<?php
include_once("config.php");
$pdo = new PDO($dsn, $uid, $pass, $opt);
$err = "";
$alert = "";

$post_Data = json_decode($_POST['data']);

foreach($post_Data as $postDt){
    $strdocno = $postDt->strdocno;
    $stracode = $postDt->stracode;

    try {
        $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }

    foreach ($syspec as $syspecitem) {
        $tx_roundac = $syspecitem->s_roundac;
        $tt_sidocno = $syspecitem->s_sidocno;
        $tx_gstcc = $syspecitem->s_gstcc;
        $s_invdetail = $syspecitem->S_INVDETAIL;
    }

    try {
        $gTaxCode = $pdo->query("SELECT * FROM taxcode ORDER BY t_code")->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }

    $tt_round = 0.00;
    $tt_sn = "0000";

    // remove from transaction if not found in invoice
    if (isset($strdocno)) {
        try {
            $stmt = $pdo->prepare('DELETE FROM trn WHERE t_docno=:t_docno');
            $stmt->execute(array(':t_docno' => $strdocno));
        } catch(PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 38";
        }
    }

    // debit GL A/C
    try {
        $stmt = $pdo->prepare(
            "SELECT acc_pinvmt.*,acc_pinvdt.*,taxcode.t_type t_taxtype
            FROM acc_pinvmt
            INNER JOIN acc_pinvdt ON (m_docno = t_docno)
            LEFT JOIN taxcode ON (t_taxcode = t_code)
            WHERE m_docno=:m_docno AND (TRIM(t_drac) <> '-' and trim(t_drac) <> '')
            ORDER BY t_sn asc");
        $stmt->execute(array(':m_docno' => $strdocno));
        $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 54";
    }

    foreach($appendx as $item){
        $tt_docno = $item->m_docno;
        $tt_date = $item->m_date;
        $tt_pdate = $item->m_spinvdate;
        $tt_gdate = $item->m_gdate;
        $tt_batch = $item->m_batch;
        $tt_round = $item->m_round;
        $tt_drac = $item->t_drac;
        $tt_subject = $item->t_subject;
        $tt_amt = $item->t_amt;
        $tt_taxcode = $item->t_taxcode;
        $tt_taxrate = $item->t_taxrate;
        $tt_taxamt = $item->t_taxamt;
        $tt_rate = $item->m_rate;
        $tt_spinvno = $item->m_spinvno;
        $tt_taxtype = $item->t_taxtype ?: '';
        $tt_type = prgdbcrgltype($tt_drac, $pdo);

        // check taxcode acc code
        try {
            foreach ($gTaxCode as $gtaxcodeItem) {
                if ($gtaxcodeItem->t_code == $tt_taxcode) {
                    $tt_iocode = $gtaxcodeItem->t_acode;
                    break;
                }else{
                    $tt_iocode = "";
                }
            }
        } catch (PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 88";
        }

        if ($tx_gstcc == 1) {
            $tx_cc = trim(substr($tt_drac, 5,0));
            $tx_gstoac = substr($tt_iocode, 0,5).$tx_cc;
            $tx_roundac = substr($tx_roundac, 0,5).$tx_cc;
        }

        if($tt_amt > 0){
            $tt_dramt = $tt_amt;
            $tt_cramt = 0.00;
            $tt_tdramt = $tt_taxamt;
            $tt_tcramt = 0.00;
        }else{
            $tt_dramt = 0.00;
            $tt_cramt = abs($tt_amt);
            $tt_tdramt = 0.00;
            $tt_tcramt = abs($tt_taxamt);
        }

        $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );

        try {
            $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                                VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
            $stmt->execute();
        } catch(PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 117";
        }

        try {
            $ttdate     = date("m/Y", strtotime($tt_date));
            $tdrampt    = round($tt_dramt*$tt_rate,12);
            $tcrampt    = round($tt_cramt*$tt_rate,12);
            $tttaxamt	= abs($tt_taxamt);

            $stmt = $pdo->prepare("UPDATE trn
                                SET
                                t_mtype 	='SI',
                                t_batch 	='$tt_batch',
                                t_term 		='$ttdate',
                                t_type 		='$tt_type',
                                t_mc 		= 13,
                                t_date 		='$tt_date',
                                t_gdate 	='$tt_date',
                                t_acode 	='$tt_drac',
                                t_ref 	    ='$tt_spinvno',
                                t_pdate 	='$tt_pdate',
                                t_descr 	='$tt_subject',
                                t_dramt 	='$tdrampt',
                                t_cramt 	='$tcrampt',
                                t_drcurr	='$tt_dramt',
                                t_crcurr 	='$tt_cramt',
                                t_exrate 	='$tt_rate',
                                t_taxcode 	='$tt_taxcode',
                                t_taxamt	='$tttaxamt',
                                t_taxrate 	='$tt_taxrate',
                                t_import 	='Y',
                                t_modiuser  ='PHPSYS',
                                t_modidate 	=NOW()

                                WHERE t_docno='$tt_docno'
                                AND t_sn='$tt_sn'");
            $stmt->execute();
        } catch (PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 156";
        }

        // input tax/expenses
        if(!EMPTY($tt_taxamt) && $tt_taxamt <> 0.00){
            $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );
            $tt_ac = $tt_iocode;

            try {
                $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                                VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
                $stmt->execute();
            } catch(PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 170";
            }


            try {
                $ttdate     = date("m/Y", strtotime($tt_date));
                $tdrampt    = round($tt_dramt*$tt_rate,12);
                $tcrampt    = round($tt_cramt*$tt_rate,12);
                $tttaxamt	= abs($tt_taxamt);
                $tdate      = date("Y-m-d", strtotime($tt_date));
                $tgdate     = date("Y-m-d", strtotime($tt_gdate));

                $stmt = $pdo->prepare("UPDATE trn
                                    SET
                                    t_mtype 	='SI',
                                    t_batch 	='$tt_batch',
                                    t_term 		='$ttdate',
                                    t_type 		='$tt_type',
                                    t_mc 		= 13,
                                    t_date 		='$tt_date',
                                    t_gdate 	='$tt_date',
                                    t_acode 	='$tt_ac',
                                    t_ref 	    ='$tt_spinvno',
                                    t_pdate 	='$tt_pdate',
                                    t_descr 	='$tt_subject',
                                    t_dramt 	='$tdrampt',
                                    t_cramt 	='$tcrampt',
                                    t_drcurr	='$tt_dramt',
                                    t_crcurr 	='$tt_cramt',
                                    t_exrate 	='$tt_rate',
                                    t_import 	='Y',
                                    t_modiuser  ='PHPSYS',
                                    t_modidate 	=NOW()

                                    WHERE t_docno='$tt_docno'
                                    AND t_sn='$tt_sn'");
                $stmt->execute();
            } catch (PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 209";
            }
        }
    }

    // for rounding adj
    if( $tt_round <> 0.00){
        $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );

        try {
            $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                    VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
            $stmt->execute();
        } catch(PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
        }

        try {
            $ttdate     = date("m/Y", strtotime($tt_date));
            $tdrampt    = sprintf("%0.2f", round($tt_rate*($tt_round>0.00? $tt_round:0.00)),2);
            $tcrampt    = sprintf("%0.2f", round($tt_rate*($tt_round<0.00? abs($tt_round):0.00)),2);
            $tdrcurr 	= ($tt_round>0.00)? abs($tt_round) : 0.00;
            $tcrcurr 	= ($tt_round<0.00)? abs($tt_round) : 0.00;

            $stmt = $pdo->prepare("UPDATE trn SET
                                t_mtype 	='SI',
                                t_batch 	='$tt_batch',
                                t_term 		='$ttdate',
                                t_type 		='$tt_type',
                                t_mc 		= 13,
                                t_date 		='$tt_date',
                                t_gdate 	='$tt_date',
                                t_acode 	='$tx_roundac',
                                t_ref   	='$tt_spinvno',
                                t_pdate   	='$tt_pdate',
                                t_descr 	='$tt_subject',
                                t_dramt 	='$tdrampt',
                                t_cramt 	='$tcrampt',
                                t_drcurr	='$tdrcurr',
                                t_crcurr	='$tcrcurr',
                                t_exrate	='$tt_rate',
                                t_import 	='Y',
                                t_modiuser  ='PHPSYS',
                                t_modidate 	=NOW()

                                WHERE t_docno='$tt_docno'
                                AND t_sn='$tt_sn'");
            $stmt->execute();
        } catch (PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 260";
        }
    }

    // To check if the Cr.A/C is Brought Forward or Open Item type
    $tt_ptype = getPType($stracode, $pdo);

    // credit debtor/creditor A/C
    switch($tt_ptype){
        case "B":
            if ($s_invdetail == 0){
                try{
                    $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_ref2,m_rate,m_spinvno,m_spinvdate,m_cash,m_ra,
                                    '1001' as t_sn,m_gldescr as t_subject,m_tamtt,'' as t_bill,'' as t_paybill
                                    FROM acc_pinvmt
                                    INNER JOIN acc_pinvdt ON (m_docno = t_docno)
                                    WHERE m_docno = '$strdocno' AND (TRIM(t_drac) = '-' OR trim(t_drac)='') and t_amt <> 0.00
                                    ORDER BY t_docno limit 1");
                    $stmt->execute();
                    $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 281";
                }
            }else{
                try{
                    $stmt = $pdo->prepare("SELECT *, '' as t_bill,'' as t_paybill, t_amtt as t_amtx
                                            FROM acc_pinvmt
                                            INNER JOIN acc_pinvdt ON (m_docno = t_docno)
                                            WHERE m_docno = '$strdocno' AND (TRIM(t_drac) = '-' OR trim(t_drac)='') and t_amt <> 0.00
                                            ORDER BY t_docno limit 1");
                    $stmt->execute();
                    $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                    echo "error 294";
                }
            }
        break;
        case "O";
            // try{
            //     $stmt = $pdo->prepare("SELECT *
            //     				from acc_pinvmt INNER JOIN acc_pinvdt ON (m_docno = t_docno)
            //     				WHERE m_docno = '<<sqlchar(strdocno)>>' AND (TRIM(t_drac) = '-' or trim(t_drac)='') and t_amt <> 0.00
            //     				ORDER BY t_docno");
            //     $stmt->execute(array());
            //     $checkx = $stmt->fetchAll(PDO::FETCH_OBJ);
            // } catch (PDOException $ex) {
            //     $err = $err . $ex->getMessage() . "<br/>";
            // }

            // $tt_recc = RECCOUNT();

            try{
                $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_crac,m_name,m_ref2,m_rate,m_spinvno,m_spinvdate,m_cash,m_ra,
                                            '2005' as t_sn,m_gldescr as t_subject,m_tamtt,m_docno as t_bill,'' as t_paybill
                                        from acc_pinvmt INNER JOIN acc_pinvdt ON (m_docno = t_docno)
                                        WHERE m_docno = '$strdocno' AND (TRIM(t_drac) = '-' or trim(t_drac)='') and t_amt <> 0.00
                                        ORDER BY t_docno limit 1");
                $stmt->execute();
                $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 323";
            }
        break;
    }

    // do{
    foreach($appendx2 as $item){
        $tt_docno = $item->m_docno;
        $tt_date = $item->m_date;
        $tt_gdate = $item->m_gdate;
        $tt_batch = $item->m_batch;
        $tt_crac = $item->m_crac;
        $tt_name = $item->m_name;
        $tt_subject = (!empty($item->t_subject)? trim($item->t_subject) : "PURCHASE - REFER $item->m_docno");
        $tt_amt = $item->m_tamtt;
        $tt_bill = $item->t_bill;
        $tt_paybill = $item->t_paybill;
        $tt_rate = $item->m_rate;
        $tt_spinvno = $item->m_spinvno;
        $tt_spinvdate = $item->m_spinvdate;
        $tt_cash = $item->m_cash;
        $tt_ra = $item->m_ra;
        $tt_type = prgdbcrgltype($tt_crac, $pdo);
        $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );

        if($tt_amt > 0){
            $tt_dramt = 0.00;
            $tt_cramt = $tt_amt;
        }else{
            $tt_dramt = abs($tt_amt);
            $tt_cramt = 0.00;
        }

        // if cash purchase
        if (!empty($tt_cash) && trim($tt_cash) <> "-") {
            $tt_crac = $tt_cash;
            $tt_type = "1";
            $tx_bill = "";
        }else{
            switch($tt_ptype){
                case "B":
                    // If Debtor is Brought Forward type ***
                    $tx_bill = str_repeat(" ", 20);
                break;
                case "O":
                    // If Debtor is Open Item type ***
                    $tx_bill = ($tt_sidocno == 1)? $tt_docno : $tt_spinvno;
            }

            try {
                $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                                    VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
                $stmt->execute();
            } catch(PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 378";
            }

            try {
                $ttdate     = date("m/Y", strtotime($tt_date));
                $t_mc 		= 13;
                $tdrampt    = sprintf("%0.2f", round($tt_dramt*$tt_rate,2));
                $tcrampt    = sprintf("%0.2f", round($tt_cramt*$tt_rate,2));
                $tdrcurr 	= sprintf("%0.2f", round($tt_dramt,2));
                $tcrcurr 	= sprintf("%0.2f", round($tt_cramt,2));
                $tttaxamt	= abs($tt_taxamt);
                $tdate      = date("Y-m-d", strtotime($tt_date));
                $tgdate     = date("Y-m-d", strtotime($tt_gdate));

                $stmt = $pdo->prepare("UPDATE trn
                                SET
                                t_mtype 	='SI',
                                t_batch 	='$tt_batch',
                                t_term 		='$ttdate',
                                t_type 		='$tt_type',
                                t_mc 		=13,
                                t_date 		='$tt_date',
                                t_gdate 	='$tt_gdate',
                                t_acode 	='$tt_crac',
                                t_bill   	='$tx_bill',
                                t_ref   	='$tt_spinvno',
                                t_pdate   	='$tt_spinvdate',
                                t_descr 	='$tt_subject',
                                t_cramt 	='$tcrampt ',
                                t_dramt 	='$tdrampt',
                                t_crcurr	='$tcrcurr',
                                t_drcurr	='$tdrcurr',
                                t_exrate	='$tt_rate',
                                t_import 	='Y',
                                t_modiuser  ='PHPSYS',
                                t_modidate 	=NOW()

                                WHERE t_docno='$tt_docno'
                                AND t_sn='$tt_sn'");
                $stmt->execute();
            } catch (PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 420";
            }
        }
    }

    // mark as post
    try {
        $stmt = $pdo->prepare("UPDATE acc_pinvmt SET m_post ='Y' WHERE m_docno='$tt_docno'");
        $stmt->execute();
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 432";
    }
}

echo "true";