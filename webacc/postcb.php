<?php
include_once("config.php");

$pdo = new PDO($dsn, $uid, $pass, $opt);
$err = "";
$alert = "";

$post_Data = json_decode($_POST['data']);

foreach($post_Data as $postDt){
	$strdocno = $postDt->strdocno;
	$stracode = $postDt->stracode;

	try {
		$syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);
	} catch (PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}

	foreach ($syspec as $syspecitem) {
		$tx_roundac = $syspecitem->s_roundac;
		$tx_gstcc = $syspecitem->s_gstcc;
		$s_cbdetail1 = $syspecitem->S_CBDETAIL;
	}

	try {
		$gTaxCode = $pdo->query("SELECT * FROM taxcode")->fetchAll(PDO::FETCH_OBJ);
	} catch (PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}

	$tt_round = 0.00;
	$tt_sn = '0000';

	// remove from transaction if not found in invoice
	try {
		$stmt = $pdo->prepare('DELETE FROM TRN WHERE t_docno=:t_docno');
		$stmt->execute(array(':t_docno' => $strdocno));
		$affected_rows = $stmt->rowCount();
		$alert = $affected_rows . " record has been removed";
	} catch(PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}

	//Credit GL A/C
	try {
		// echo $strdocno." <br> ";
		$stmt = $pdo->prepare(
			"SELECT *
			FROM acc_cbmt a
			INNER JOIN acc_cbdt b ON a.m_docno = b.t_docno
			WHERE a.m_docno = :strdocno
				AND (TRIM(b.t_crac) <> '-' AND TRIM(b.t_crac) <> '')
			ORDER BY t_sn ASC
			");

		$stmt->execute(array(':strdocno' => $strdocno));
		$affected_rows = $stmt->rowCount();
		$appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
	} catch (PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
	}

	if ($affected_rows > 0) {
		foreach ($appendx as $item) {
			$tt_docno = $item->m_docno;
			$tt_date = $item->m_date;
			$tt_batch = $item->m_batch;
			$tt_round = $item->m_round;
			$tt_crac = $item->t_crac;
			$tt_subject = $item->t_subject;
			$tt_amt = $item->t_amt;
			$tt_exrate = $item->t_exrate;
			$tt_taxcode = $item->t_taxcode;
			$tt_taxrate = $item->t_taxrate;
			$tt_taxamt = $item->t_taxamt;

			// check taxcode acc code
			try {
				foreach ($gTaxCode as $t_item) {
					if ($t_item->t_code == $tt_taxcode) {
						$tt_iocode = $t_item->t_acode;
						break;
					}else{
						$tt_iocode = "";
					}
				}

			} catch (PDOException $ex) {
				$err = $err . $ex->getMessage() . "<br/>";
				echo "<br>error 88";
			}

			if ($tx_gstcc == 1) {
				$tx_cc = trim(substr($tt_crac, 5,0));
				$tx_gstoac = substr($tt_iocode, 0,5).$tx_cc;
				$tx_roundac = substr($tx_roundac, 0,5).$tx_cc;
			}

			$tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );
			$tt_type = prgdbcrgltype($tt_crac, $pdo);

			try {
				$stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
				VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
				$stmt->execute();

			} catch(PDOException $ex) {
				$err = $err . $ex->getMessage() . "<br/>";
			}

			try {
				$tmtype = 'CB';
				$ttdate = date("m/Y", strtotime($tt_date));
				$tmc = 13;
				$t_dramt = '0.00';
				$t_cramt = sprintf("%0.2f",round($tt_exrate*$tt_amt,2));
				$t_drcurr = '0.00';
				$t_import = 'Y';

				$tt_subject = addslashes($tt_subject);
				$tt_batch = addslashes($tt_batch);
				$stmt = $pdo->prepare("UPDATE trn
										SET
										t_mtype 	= '$tmtype',
										t_batch 	= '$tt_batch',
										t_term 		= '$ttdate',
										t_type 		= '$tt_type',
										t_mc 		= $tmc,
										t_date 		= '$tt_date',
										t_gdate 	= '$tt_date',
										t_acode 	= '$tt_crac',
										t_descr 	= '$tt_subject',
										t_dramt 	= '$t_dramt',
										t_cramt 	= '$t_cramt',
										t_drcurr 	= '$t_drcurr',
										t_crcurr 	= '$tt_amt',
										t_exrate	= '$tt_exrate',
										t_taxcode 	= '$tt_taxcode',
										t_taxamt	= '$tt_taxamt',
										t_taxrate 	= '$tt_taxrate',
										t_import 	= '$t_import',
										t_modiuser  = 'PHPSYS',
										t_modidate 	= NOW()

										WHERE t_docno='$tt_docno'
										AND t_sn='$tt_sn'");
				$stmt->execute();
			} catch (PDOException $ex) {
				$err = $err . $ex->getMessage() . "<br/>";
				echo $err;
			}

			// output tax
			if (!empty($tt_taxamt) && $tt_taxamt <> 0.00) {
				$tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );
				$tmtype = 'CB';
				$ttdate = date("m/Y", strtotime($tt_date));
				$tmc = 13;
				$t_dramt = '0.00';
				$t_cramt = sprintf("%0.2f",round($tt_exrate*$tt_taxamt,2));
				$t_drcurr = '0.00';
				$t_import = 'Y';

				try {
					$stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
					VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
					$stmt->execute();

				} catch(PDOException $ex) {
					$err = $err . $ex->getMessage() . "<br/>";
				}

				try {

						$tt_subject = addslashes($tt_subject);
					$stmt = $pdo->prepare("UPDATE trn
										SET
										t_mtype 	= 'CB',
										t_batch 	= 'Y',
										t_term 		= '$ttdate',
										t_type 		= '$tt_type',
										t_mc 		= $tmc,
										t_date 		= '$tt_date',
										t_gdate 	= '$tt_date',
										t_acode 	= '$tt_iocode',
										t_descr 	= '$tt_subject',
										t_dramt 	= '$t_dramt',
										t_cramt 	= '$t_cramt',
										t_drcurr 	= '$t_drcurr',
										t_crcurr 	= '$tt_taxamt',
										t_exrate	= '$tt_exrate',
										t_import 	= '$t_import',
										t_modiuser  = 'PHPSYS',
										t_modidate 	= NOW()

										WHERE t_docno='$tt_docno'
										AND t_sn='$tt_sn'");
					$stmt->execute();
				} catch (PDOException $ex) {
					$err = $err . $ex->getMessage() . "<br/>";
				}
			}
		}
	}

	//for rounding adj
	if($tt_round != 0.00){
		$tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );
		try {
			$stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
			VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
			$stmt->execute();
		} catch(PDOException $ex) {
			$err = $err . $ex->getMessage() . "<br/>";
			echo "<br>error 209";
		}

		try {
			$tmtype = 'CB';
			$ttdate = date("m/Y", strtotime($tt_date));
			$tmc = 13;
			$t_dramt = "0.00";
			$t_cramt = sprintf("%0.2f",round($tt_exrate*$tt_amt,2));
			$t_import = 'Y';
			$t_dramt1 	= sprintf("%0.2f",round($tt_exrate * (($tt_round<0.00)?  abs($tt_round) :  0.00), 2));
			$t_cramt1 	= sprintf("%0.2f",round($tt_exrate * (($tt_round>0.00)? $tt_round : 0.00),2));
			$t_drcurr1 	= sprintf("%0.2f",(($tt_round<0.00)? abs($tt_round) : 0.));
			$t_crcurr1 	= sprintf("%0.2f",(($tt_round>0.00)? $tt_round : 0.00));

			$tt_subject = addslashes($tt_subject);
			$tt_batch = addslashes($tt_batch);
			$stmt = $pdo->prepare("UPDATE trn
								SET
								t_mtype 	='CB',
								t_batch 	='$tt_batch',
								t_term 		='$ttdate',
								t_type 		='$tt_type',
								t_mc 		='$tmc',
								t_date 		='$tt_date',
								t_gdate 	='$tt_date',
								t_acode 	='$tx_roundac',
								t_descr 	='$tt_subject',
								t_dramt 	='$t_dramt1',
								t_cramt 	='$t_cramt1',
								t_drcurr 	='$t_drcurr1',
								t_crcurr 	='$t_crcurr1',
								t_exrate	='$tt_exrate',
								t_import 	='$t_import',
								t_modiuser  ='PHPSYS',
								t_modidate 	= NOW()

								WHERE t_docno='$tt_docno'
								AND t_sn='$tt_sn'");
			$stmt->execute();

		} catch (PDOException $ex) {
			$err = $err . $ex->getMessage() . "<br/>";
			echo "<br>error 248";
		}
	}

	// To check if the Dr.A/C is Brought Forward or Open Item type
	$tt_ptype = getPType($stracode, $pdo);

	// Debit debtor/creditor A/C
	switch($tt_ptype){
		case 'B':
			if($s_cbdetail1 == 0){
				$stmt = $pdo->prepare(
					"SELECT
					acc_cbmt.m_docno, acc_cbmt.m_date, acc_cbmt.m_batch, acc_cbmt.m_drac,
					acc_cbmt.m_name, acc_cbmt.m_ref2, acc_cbmt.m_rate, '1001' AS t_sn,
					acc_cbmt.m_gldescr AS t_subject, acc_cbmt.m_tamtt AS t_amt, '' AS t_bill, '' AS t_paybill
				FROM acc_cbmt
				INNER JOIN acc_cbdt ON acc_cbmt.m_docno = acc_cbdt.t_docno
				WHERE acc_cbmt.m_docno= '$strdocno' AND (TRIM(acc_cbdt.t_crac) = '-' OR TRIM(acc_cbdt.t_crac) = '')
					AND t_amt <> 0.00
				ORDER BY acc_cbdt.t_docno LIMIT 1");

				$stmt->execute();
				$affected_rows = $stmt->rowCount();
				$appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
			}else{
				$stmt = $pdo->prepare(
					"SELECT
						acc_cbmt.m_docno, acc_cbmt.m_date, acc_cbmt.m_batch, acc_cbmt.m_drac,
						acc_cbmt.m_name, acc_cbmt.m_ref2, acc_cbmt.m_rate, '1001' as t_sn,
						acc_cbdt.t_subject, acc_cbmt.m_tamtt as t_amt, '' as t_bill, '' as t_paybill
					FROM acc_cbmt
					INNER JOIN acc_cbdt
					ON acc_cbmt.m_docno = acc_cbdt.t_docno
					where acc_cbmt.m_docno= '$strdocno' AND (TRIM(acc_cbdt.t_crac) = '-' OR TRIM(acc_cbdt.t_crac) = '')
						AND t_amt <> 0.00
					ORDER BY acc_cbdt.t_docno");

				$stmt->execute();
				$affected_rows = $stmt->rowCount();
				$appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
			}
		break;

		case 'O':
			$stmt = $pdo->prepare(
				"SELECT
					acc_cbmt.*, acc_cbdt.*
				FROM acc_cbmt
				INNER JOIN acc_cbdt ON acc_cbmt.m_docno = acc_cbdt.t_docno
				where acc_cbmt.m_docno= '$strdocno' AND (TRIM(acc_cbdt.t_crac) = '-' OR TRIM(acc_cbdt.t_crac) = '')
					AND t_amt <> 0.00
				ORDER BY acc_cbdt.t_docno
				");

			$stmt->execute();
			$affected_rows = $stmt->rowCount();
			$checkx = $stmt->fetchAll(PDO::FETCH_OBJ);
			$tt_recc = $stmt->rowCount();

			if($s_cbdetail1 == 0 || $tt_recc > 1){

			$stmt = $pdo->prepare(
				"SELECT
					m_docno, m_date, m_batch, m_drac,
					m_name, m_ref2, m_rate, '2005' as t_sn,
					m_gldescr as t_subject, m_tamtt as t_amt, m_docno as t_bill, '' as t_paybill
				FROM acc_cbmt
				INNER JOIN acc_cbdt
				ON m_docno = t_docno
				where m_docno='$strdocno' AND (TRIM(acc_cbdt.t_crac) = '-' OR TRIM(acc_cbdt.t_crac) = '')
					AND t_amt <> 0.00
				ORDER BY t_docno limit 1");

			$stmt->execute();
			$affected_rows = $stmt->rowCount();
			$appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
			}else{
				$stmt = $pdo->prepare(
					"SELECT
						m_docno, m_date, m_batch, m_drac,
						m_name, m_ref2, m_rate, '2005' as t_sn,
						t_subject, m_tamtt as t_amt, t_docno as t_bill, '' as t_paybill
					FROM acc_cbmt
					INNER JOIN acc_cbdt
					ON m_docno = t_docno
					where m_docno='$strdocno' AND (TRIM(t_crac) = '-' OR TRIM(t_crac) = '')
						AND t_amt <> 0.00
					ORDER BY t_docno limit 1");

				$stmt->execute();
				$affected_rows = $stmt->rowCount();
				$appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
			}
		break;
	}

		foreach ($appendx as $item){
			$tt_docno = $item->m_docno;
			$tt_date = $item->m_date;
			$tt_batch = $item->m_batch;
			$tt_drac = $item->m_drac;
			$tt_name = $item->m_name;
			// echo json_encode($item);
			$tt_subject = "SALES"; //empty($item->m_ref2)? $tt_docno : "SALES LPO : ". trim($item->m_ref2);
			$tt_amt = $item->t_amt;
			$tt_exrate	= $item->m_rate;
			$tt_bill = $item->t_bill;
			$tt_paybill = $item->t_paybill;
			$tt_type = prgdbcrgltype($tt_drac, $pdo);
			$tt_sn = $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );

			switch($tt_ptype){
				case "B":
					// If Debtor is Brought Forward type
					$lcl_t_bill = str_repeat(" ", 20);
				break;
				case "O":
					// If Debtor is Open Item type
					$lcl_t_bill = $tt_bill;
				break;
				default:
					$lcl_t_bill = str_repeat(" ", 20);
				break;
			}

			try {
				$stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
									VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
				$stmt->execute();
			} catch(PDOException $ex) {
				$err = $err . $ex->getMessage() . "<br/>";
				echo "<br>error 380";
			}

			$tmtype = 'CB';
			$t_term = date("m/Y", strtotime($tt_date));
			$tmc = 13;
			$t_cramt = sprintf("%0.2f",round($tt_exrate*$tt_amt,2));
			$t_import = 'Y';
			$t_dramt 	= sprintf("%0.2f", round($tt_amt * $tt_exrate, 2)  );
			$t_cramt1 	= sprintf("%0.2f",round($tt_exrate * (($tt_round>0.00)? $tt_round : 0.00),2));
			$t_drcurr1 	= sprintf("%0.2f",(($tt_round<0.00)? abs($tt_round) : 0.));
			$t_crcurr 	= '0.00';

			try {
					$tt_subject = addslashes($tt_subject);
					$tt_batch = addslashes($tt_batch);
					$stmt = $pdo->prepare("UPDATE trn
									SET
									t_mtype 	='$tmtype',
									t_batch 	='$tt_batch',
									t_term 		='$t_term',
									t_type 		='$tt_type',
									t_mc 		='$tmc',
									t_date 		='$tt_date',
									t_gdate 	='$tt_date',
									t_acode 	='$tt_drac',
									t_bill		='$lcl_t_bill',
									t_descr 	='$tt_subject',
									t_dramt 	='$t_dramt',
									t_cramt 	='0.00',
									t_drcurr 	='$tt_amt',
									t_crcurr 	='0.00',
									t_exrate	='$tt_exrate',
									t_import 	='$t_import',
									t_modiuser  ='PHPSYS',
									t_modidate 	=NOW()

									WHERE t_docno='$tt_docno'
									AND t_sn='$tt_sn'");

					$stmt->execute();
			} catch (PDOException $ex) {
					$err = $err . $ex->getMessage() . "<br/>";
					echo "<br>error 421";
			}

		}


	//mark as post
	try {
		$stmt = $pdo->prepare("UPDATE acc_cbmt SET m_post ='Y' WHERE m_docno='$strdocno'");
		$stmt->execute();
	} catch (PDOException $ex) {
		$err = $err . $ex->getMessage() . "<br/>";
		echo "<br>error 431";
	}

}
echo 'true';



