<?php
include_once("config.php");

$pdo = new PDO($dsn, $uid, $pass, $opt);
$err = "";
$alert = "";

$post_Data = json_decode($_POST['data']);
// $post_Data = json_decode('[{"strdocno":"TS19-0005","stracode":"DT010-00"}]');

foreach($post_Data as $postDt){
    $strdocno =  $postDt->strdocno;
    $stracode =  $postDt->stracode;
    $strtype = '';

    try {
        $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }

    foreach ($syspec as $syspecitem) {
        $tx_roundac = $syspecitem->s_roundac;
        $tx_gstcc = $syspecitem->s_gstcc;
        $s_invdetail = $syspecitem->S_INVDETAIL;
    }

    try {
        $gTaxCode = $pdo->query("SELECT * FROM taxcode ORDER BY t_code")->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }

    $tt_round=0.00;
    $tt_sn = "0000";

    // remove from transaction if not found in invoice
    if (isset($strdocno)) {
        try {
            $stmt = $pdo->prepare('DELETE FROM TRN WHERE t_docno=:t_docno');
            $stmt->execute(array(':t_docno' => $strdocno));
        } catch(PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
        }
    }

    // Credit GL A/C
    try {
        $stmt = $pdo->prepare(
            "SELECT *
            FROM acc_invmt
            INNER JOIN acc_invdt
            ON m_docno = t_docno
            WHERE m_docno= '$strdocno' AND (TRIM(t_crac) <> '-' AND TRIM(t_crac) <> '')
            ORDER BY t_sn ASC");

        $stmt->execute();
        $affected_rows = $stmt->rowCount();
        $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach($appendx as $item){
                $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );
                $tt_docno = $item->m_docno;
                $tt_date = $item->m_date;
                $tt_batch = $item->m_batch;
                $tt_round = $item->m_round;
                $tt_crac = $item->t_crac;

                if(is_string($strtype) && $strtype == "INT"){
                    $tt_subject = trim($item->m_name);
                }else{
                    $tt_subject = $item->t_subject;
                }

                $tt_amt = $item->t_amt;
                $tt_taxcode = $item->t_taxcode;
                $tt_taxrate = $item->t_taxrate;
                $tt_taxamt = $item->t_taxamt;
                $tt_exrate = $item->m_rate;

                foreach ($gTaxCode as $gtaxcodeItem) {
                    if ($gtaxcodeItem->t_code == $tt_taxcode) {
                        $tt_iocode = $gtaxcodeItem->t_acode;
                        break;
                    }else{
                        $tt_iocode = "";
                    }
                }

                if ($tx_gstcc == 1) {
                    $tx_cc = trim(substr($tt_crac, 5,0));
                    $tx_gstoac = substr($tt_iocode, 0,5).$tx_cc;
                    $tx_roundac = substr($tx_roundac, 0,5).$tx_cc;
                }

                if($tt_amt > 0){
                    $tt_cramt = $tt_amt;
                    $tt_dramt = 0.00;
                    $tt_tcramt = $tt_taxamt;
                    $tt_tdramt = 0.00;
                }else{
                    $tt_cramt = 0.00;
                    $tt_dramt = abs($tt_amt);
                    $tt_tcramt = 0.00;
                    $tt_tdramt = abs($tt_taxamt);
                }

                $tt_type = prgdbcrgltype($tt_crac, $pdo);

                $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                                    VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
                $stmt->execute();

                $ttdate     = date("m/Y", strtotime($tt_date));
                $t_mc 		= 13;
                $tttaxamt	= abs($tt_taxamt);
                $tmtype     = 'IV';
                $tdrampt    = round($tt_dramt*$tt_exrate,2);
                $tcrampt    = round($tt_cramt*$tt_exrate,2);

				$tt_subject = addslashes($tt_subject);
				$tt_batch = addslashes($tt_batch);
                $stmt = $pdo->prepare("UPDATE trn
                                    SET
                                    t_mtype 	='$tmtype',
                                    t_batch 	='$tt_batch',
                                    t_term 		='$ttdate',
                                    t_type 		='$tt_type',
                                    t_mc 		='$t_mc',
                                    t_date 		='$tt_date',
                                    t_gdate 	='$tt_date',
                                    t_acode 	='$tt_crac',
                                    t_descr 	='$tt_subject',
                                    t_dramt 	='$tdrampt',
                                    t_cramt 	='$tcrampt',
                                    t_crcurr 	='$tt_cramt',
                                    t_drcurr	='$tt_dramt',
                                    t_exrate 	='$tt_exrate',
                                    t_taxcode 	='$tt_taxcode',
                                    t_taxamt	='$tttaxamt',
                                    t_taxrate 	='$tt_taxrate',
                                    t_import 	='Y',
                                    t_modiuser  ='PHPSYS',
                                    t_modidate 	=NOW()

                                    WHERE t_docno='$tt_docno'
                                    AND t_sn='$tt_sn'");
                $stmt->execute();

                // output tax
                if(!EMPTY($tt_taxamt) && $tt_taxamt <> 0.00){
                    $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );

                    try {
                        $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                                            VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
                        $stmt->execute();
                    } catch(PDOException $ex) {
                        $err = $err . $ex->getMessage() . "<br/>";
                    }

                    $ttdate     = date("m/Y", strtotime($tt_date));
                    $t_mc 		= 13;
                    $tttaxamt	= abs($tt_taxamt);
                    $tdrampt    = round($tt_dramt*$tt_exrate,2);
                    $tcrampt    = round($tt_cramt*$tt_exrate,2);

					$tt_subject = addslashes($tt_subject);
					$tt_subject = addslashes($tt_subject);
                    $stmt = $pdo->prepare("UPDATE trn
                                        SET
                                        t_mtype 	='IV',
                                        t_batch 	='$tt_batch',
                                        t_term 		='$ttdate',
                                        t_type 		='$tt_type',
                                        t_mc 		='$t_mc',
                                        t_date 		='$tt_date',
                                        t_gdate 	='$tt_date',
                                        t_acode 	='$tt_iocode',
                                        t_descr 	='$tt_subject',
                                        t_dramt 	='$tdrampt',
                                        t_drcurr	='$tt_dramt',
                                        t_cramt 	='$tcrampt',
                                        t_crcurr 	='$tt_cramt',
                                        t_exrate 	='$tt_exrate',
                                        t_import 	='Y',
                                        t_modiuser  ='PHPSYS',
                                        t_modidate 	=NOW()

                                        WHERE t_docno='$tt_docno'
                                        AND t_sn='$tt_sn'");
                    $stmt->execute();
                }
        }

    } catch (PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo $err;
            echo "<br> errorr 188";
    }

    // for rounding adj
    if( $tt_round <> 0.00){
        $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );

        try {
            $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                                    VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
            $stmt->execute();

            $ttdate     = date("m/Y", strtotime($tt_date));
            $tdramt 	= sprintf("%0.2f",round($tt_exrate*($tt_round<0.00?abs($tt_round):0.00)), 2);
            $tcramt 	= sprintf("%0.2f",round($tt_exrate*($tt_round>0.00?abs($tt_round):0.00)), 2);
            $tdrcurr 	= ($tt_round<0.00)?abs($tt_round):0.00;
            $tcrcurr 	= ($tt_round>0.00)?abs($tt_round):0.00;

			$tt_subject = addslashes($tt_subject);
			$tt_batch = addslashes($tt_batch);
            $stmt = $pdo->prepare("UPDATE trn SET
                                t_mtype 	='IV',
                                t_batch 	='$tt_batch',
                                t_term 		='$ttdate',
                                t_type 		='$tt_type',
                                t_mc 		=13,
                                t_date 		='$tt_date',
                                t_gdate 	='$tt_date',
                                t_acode 	='$tx_roundac',
                                t_descr 	='$tt_subject',
                                t_dramt 	='$tdramt',
                                t_cramt 	='$tcramt',
                                t_drcurr	='$tdrcurr',
                                t_crcurr	='$tcrcurr',
                                t_exrate	='$tt_exrate',
                                t_import 	='Y',
                                t_modiuser  ='PHPSYS',
                                t_modidate 	=NOW()

                                WHERE t_docno='$tt_docno'
                                AND t_sn='$tt_sn'");
            $stmt->execute();
        } catch (PDOException $ex) {
            $err = $err . $ex->getMessage() . "<br/>";
            echo "error 238";
        }
    }

    // To check if the Dr.A/C is Brought Forward or Open Item type
    $tt_ptype = getPType($stracode, $pdo);

    // Debit debtor/creditor A/C
    switch($tt_ptype){
        case "B":
            if ($s_invdetail == 0){
                try{
                    $stmt = $pdo->prepare("SELECT
                        m_docno,
                        m_date,
                        m_batch,
                        m_drac,
                        m_name,
                        m_ref1,
                        m_rate,
                        '1001' as t_sn,
                        m_gldescr as t_subject,
                        m_tamtt as t_amtx,
                        '' as t_bill,
                        '' as t_paybill
                        From acc_invmt
                        INNER JOIN acc_invdt ON m_docno = t_docno
                        WHERE m_docno = '$strdocno'
                        AND (trim(t_crac) = '-' OR trim(t_crac) = '')
                        and t_amt <> 0.00
                        ORDER BY t_docno limit 1");
                    $stmt->execute();
                    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                }
            }else{
                try{
                    $stmt = $pdo->prepare("SELECT m_docno,m_date,m_batch,m_drac,m_name,m_ref1,m_rate,
                    '1001' as t_sn, t_subject,t_amtt as t_amtx,'' as t_bill,'' as t_paybill
                    from acc_invmt INNER JOIN acc_invdt ON m_docno =  t_docno
                    WHERE m_docno = '$strdocno' AND (trim( t_crac) = '-' or trim( t_crac)='') and  t_amt <> 0.00
                    ORDER BY  t_docno");
                    $stmt->execute();
                    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
                } catch (PDOException $ex) {
                    $err = $err . $ex->getMessage() . "<br/>";
                }
            }
        break;
        case "O";
            try{
                $stmt = $pdo->prepare("
                SELECT *
                FROM acc_invmt
                INNER JOIN acc_invdt ON m_docno = t_docno
                WHERE m_docno = '$strdocno' AND (trim(t_crac) = '-' or trim(t_crac) ='') and t_amt <> 0.00
                ORDER BY t_docno");
                $stmt->execute();
                $checkx = $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 299";
            }

            $tt_recc = $stmt->rowCount();

            try{
                $stmt = $pdo->prepare("
                    SELECT m_docno, m_date,m_batch,
                        m_drac,m_name,m_ref1,m_rate,
                        '2005' as t_sn,m_gldescr as t_subject,m_tamtt as t_amtx,m_docno as t_bill,'' as t_paybill
                    FROM acc_invmt
                    INNER JOIN acc_invdt ON m_docno = acc_invdt.t_docno
                    WHERE m_docno = '$strdocno' and acc_invdt.t_amt <> 0.00  AND (trim(acc_invdt.t_crac) = '-' or trim(acc_invdt.t_crac) ='')
                    ORDER BY acc_invdt.t_docno limit 1");
                $stmt->execute();
                $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 317";
            }
        break;
    }
        foreach($appendx as $item){

            $tt_docno = $item->m_docno;
            $tt_date = $item->m_date;
            $tt_batch = $item->m_batch;
            $tt_drac = $item->m_drac;
            $tt_name = $item->m_name;
            $tt_sn = str_pad( $tt_sn + 5, 4, "0", STR_PAD_LEFT );
            $tt_subject =(!EMPTY($item->m_ref1)? "SALES LPO : ".$item->m_ref1:"SALES");
            $tt_amt = $item->t_amtx;
            $tt_bill = $item->t_bill;
            $tt_exrate = $item->m_rate;
            $tt_paybill = $item->t_paybill;
            $tt_type = prgdbcrgltype($tt_drac, $pdo);


            if ($tt_amt > 0){
                $tt_cramt = 0.00;
                $tt_dramt = $tt_amt;
            }else{
                $tt_cramt = abs($tt_amt);
                $tt_dramt = 0.00;
            }

            switch($tt_ptype){
                case "B";
                    //If Debtor is Brought Forward type ***
                    $lcl_t_bill = str_repeat(" ", 20);
                    break;
                case "O";
                    //If Debtor is Open Item type ***
                    $lcl_t_bill = $tt_bill;
                    break;
            }

            try {
                $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                                    VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
                $stmt->execute();
            } catch(PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 358";
            }

            try {
                $ttdate     = date("m/Y", strtotime($tt_date));
                $t_date     = date("Y-m-d", strtotime($tt_date));
                $t_mc 		= $strtype == 'INT'? 99:13;
                $tdramt 	= sprintf("%0.2f",ROUND($tt_amt*$tt_exrate,2));
                $tcramt 	= '0.00';
                $tcrcurr 	= '0.00';

				$tt_subject = addslashes($tt_subject);
				$tt_batch = addslashes($tt_batch);
                $stmt = $pdo->prepare("UPDATE trn
                                SET
                                t_mtype 	='IV',
                                t_batch 	='$tt_batch',
                                t_term 		='$ttdate',
                                t_type 		='$tt_type',
                                t_mc 		='$t_mc',
                                t_date 		='$tt_date',
                                t_gdate 	='$tt_date',
                                t_acode 	='$tt_drac',
                                t_bill		='$lcl_t_bill',
                                t_descr 	='$tt_subject',
                                t_dramt 	='$tdramt',
                                t_cramt 	='$tcramt',
                                t_drcurr 	='$tt_amt',
                                t_crcurr 	='$tcrcurr',
                                t_exrate	='$tt_exrate',
                                t_import 	='Y',
                                t_modiuser  ='PHPSYS',
                                t_modidate 	=NOW()

                                WHERE t_docno='$tt_docno'
                                AND t_sn='$tt_sn'");
                $stmt->execute();
            } catch (PDOException $ex) {
                $err = $err . $ex->getMessage() . "<br/>";
                echo "error 395";
            }

        }

    // mark as post
    try {
        $stmt = $pdo->prepare("UPDATE acc_invmt SET m_post ='Y' WHERE m_docno='$strdocno'");
        $stmt->execute();
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
        echo "error 406";
    }
}
return 'true';