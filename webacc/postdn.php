<?php
include_once("config.php");

$pdo = new PDO($dsn, $uid, $pass, $opt);
$err = "";
$alert = "";

$strdocno = 'PR/00000007';
$stracode = 'true';

// remove existing transaction
if (isset($strdocno)) {
    try {
        $stmt = $pdo->prepare('DELETE FROM TRN WHERE t_docno=:t_docno');
        $stmt->execute(array(':t_docno' => $strdocno));
    } catch(PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }
}

try {
    $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);
} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
}

foreach ($syspec as $syspecitem) {
    $tx_roundac = $syspecitem->s_roundac;
    $tx_gstcc = $syspecitem->s_gstcc;
    $s_dndetail = $syspecitem->s_dndetail;
}

try {
    $gTaxCode = $pdo->query("SELECT * FROM taxcode ORDER BY t_code")->fetchAll(PDO::FETCH_OBJ);
} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
}

$tt_round = 0.00;

// To check if the Dr.A/C is Brought Forward or Open Item type
try {
    $stmt = $pdo->prepare("SELECT * FROM mastercode WHERE d_acode=:d_acode");
    $stmt->execute(array(':d_acode' => $stracode));
    $mastercodex = $stmt->fetchAll(PDO::FETCH_OBJ);
} catch (PDOException $ex) {
    $err = $err . $ex->getMessage() . "<br/>";
    echo "error 49";
}

foreach($mastercodex as $mCodexItem){
    $d_dc = $mCodexItem->d_dc;
    switch($d_dc){
        case "D":
            $tx_dbcr = "DB";
            $tx_ptype = $mCodexItem->d_ptype;
            // tt_dType = "DN"	&& document type
            $tt_dType = "DN";
            break;

        case "C":
            $tx_dbcr = "CR";
            $tx_ptype = $mCodexItem->d_ptype;
            // $tt_dType = "SC" && document type;
            $tt_dType = "SD";
            break;

        default:
            $tx_dbcr = "DB";
            $tx_ptype = "B";
            // $tt_dType = "DN" && document type;
            $tt_dType = "DN";
            break;
    }
}



// Credit GL A/C
    $stmt = $pdo->prepare(
        "SELECT acc_dnmt.*, acc_dndt.*, taxcode.t_type AS t_taxtype
        FROM acc_dnmt
        INNER JOIN acc_dndt ON m_docno = t_docno
        LEFT JOIN taxcode ON t_taxcode = taxcode.t_code
        WHERE m_docno=:strdocno AND (TRIM(t_crac) <> '-' AND TRIM(t_crac) <> '')
        ORDER BY t_sn ASC");

    $stmt->execute(array(':strdocno' => $strdocno));
    $affected_rows = $stmt->rowCount();
    $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);


foreach($appendx as $appendxItem){
    $tt_docno = $appendxItem->m_docno;
    $tt_date = $appendxItem->M_DATE;
    $tt_gdate = $appendxItem->m_gdate;
    $tt_batch = $appendxItem->M_BATCH;
    $tt_round = $appendxItem->m_round;

    $tt_sn = $appendxItem->T_SN;
    $tt_crac = $appendxItem->T_CRAC;
    $tt_subject = $appendxItem->T_SUBJECT;
    $tt_amt = $appendxItem->T_AMT;
    $tt_bill = $appendxItem->t_bill;
    $tt_paybill = $appendxItem->t_paybill;
    $tt_exrate = $appendxItem->T_EXRATE;
    $tt_taxcode = $appendxItem->t_taxcode;
    $tt_taxrate = $appendxItem->t_taxrate;
    $tt_taxamt = $appendxItem->t_taxamt;
    $tt_taxtype = $appendxItem->t_taxtype ?: '';


    //check taxcode acc code
    foreach ($gTaxCode as $gtaxcodeItem) {
        if ($gtaxcodeItem->t_code == $tt_taxcode) {
            $tt_iocode = $gtaxcodeItem->t_acode;
        break;
        }else{
            $tt_iocode = "";
        }
    }

    if ($tx_gstcc == 1) {
		$tx_cc = trim(substr($tt_crac, 5,0));
		$tt_iocode= substr($tt_iocode, 0,5).$tx_cc;
		$tx_roundac =substr($tx_roundac, 0,5).$tx_cc;
    }


	// To check if the Cr.A/C is Brought Forward or Open Item type
    try {
        $mastercodex = $pdo->query("SELECT * FROM mastercode WHERE d_acode=:$tt_crac")->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }

    foreach($mastercodex as $mt){
        switch($mt->d_dc){
            case "D":
                $tt_dbcr = "DB";
                $tt_ptype = $mt->d_ptype;
                break;
            case "C":
                $tt_dbcr = "CR";
                $tt_ptype = $mt->d_ptype;
                break;
            default:
                $tt_dbcr = "GL";
                $tt_ptype = " ";
                break;
        }
    }

    try {
        $stmt = $pdo->prepare("INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE)
                VALUES ('$tt_docno','$tt_sn','PHPSYS',NOW())");
        $stmt->execute();
    } catch(PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }

    switch($tt_dbcr){
        case "DB":
            if($tt_ptype == "B"){
                // If Debtor is Brought Forward type
			    $tt_paybill = str_repeat(" ", 20);
            }else{
                // If Debtor is Open Item type ***
                $tt_paybill = $tt_paybill;
            }
            break;
        case "CR":
            if($tt_ptype == "B"){
                // If Creditor is Brought Forward type
			    $tt_bill = str_repeat(" ", 20);
            }else{
                // If Creditor is Open Item type
                $tt_bill = $tt_bill;
            }
            break;
    }

    if($tt_amt > 0.00){
        $tt_dramt  = 0.00;
		$tt_cramt  = round($tt_amt * $tt_exrate,2);
		$tt_drcurr = 0.00;
        $tt_crcurr = $tt_amt;

        $tx_gstDrAmt = 0.00;
		$tx_gstDrCurr = 0.00;
		$tx_gstCrAmt = round($tt_taxamt*$tt_exrate,2);
		$tx_gstCrCurr = $tt_taxamt;
    }else{
        $tt_dramt  = abs(round($tt_amt * $tt_exrate,2));
		$tt_cramt  = 0.00;
		$tt_drcurr = abs($tt_amt);
		$tt_crcurr = 0.00;

		$tx_gstDrAmt = abs(round($tt_taxamt*$tt_exrate,2));
		$tx_gstDrCurr = abs($tt_taxamt);
		$tx_gstCrAmt = 0.00;
		$tx_gstCrCurr = 0.00;
    }

    $tt_type = prgdbcrgltype($tt_crac, $pdo);

    try {
        $ttt_drac   = prgdbcrgltype($tt_drac);
        $ttdate     = date("m/Y", strtotime($tt_date));
        $t_date     = date("Y-m-d", strtotime($tt_date));
        $t_mc 		= 13;
        $t_import 	= 'Y';
        $tttaxamt	= abs($tt_taxamt);

        $stmt = $pdo->prepare("UPDATE trn
                            SET
                            t_mtype 	='$tt_dType',
                            t_batch 	='$tt_batch',
                            t_term 		=: t_term,
                            t_type 		=: t_type,
                            t_mc 		=: t_mc,
                            t_date 		=: t_date,
                            t_gdate 	=: t_gdate,
                            t_acode 	=: t_acode,
                            t_descr 	=: t_descr,
                            t_bill		=: t_bill,
                            t_paybill	=: t_paybill,
                            t_drcurr	=: t_drcurr,
                            t_crcurr	=: t_crcurr,
                            t_dramt 	=: t_dramt,
                            t_cramt 	=: t_cramt,
                            t_exrate 	=: t_exrate,
                            t_taxcode 	=: t_taxcode,
                            t_taxamt	=: t_taxamt,
                            t_taxrate 	=: t_taxrate,
                            t_import 	=: t_import,
                            t_modiuser  =: t_modiuser,
                            t_modidate 	=: t_modidate

                            WHERE t_docno=:t_docno
                            AND t_sn=:t_sn");
        $stmt->execute(array(
                            ':t_mtype'      => $tt_dType,
                            ':t_batch'      => $tt_batch,
                            ':t_term'       => $ttdate,
                            ':t_type'       => $ttt_drac,
                            ':t_mc'         => $t_mc,
                            ':t_date'       => $t_date,
                            ':t_gdate'      => $t_date,
                            ':t_acode'      => $tt_drac,
                            ':t_descr'      => $tt_subject,
                            ':t_bill'       => $tt_bill,
                            ':t_paybill'    => $tt_paybill,
                            ':t_drcurr'     => $tt_drcurr,
                            ':t_crcurr'     => $tt_crcurr,
                            ':t_dramt'      => $tt_dramt,
                            ':t_cramt'      => $tt_cramt,
                            ':t_exrate' 	=> $tt_exrate,
                            ':t_taxcode'   	=> $tt_taxcode,
                            ':t_taxamt'	    => $tttaxamt,
                            ':t_taxrate'   	=> $tt_taxrate,
                            ':t_import' 	=> $t_import,
                            ':t_modiuser'   => $pbUserCode,
                            ':t_modidate'  	=> $currdate,
                            ':t_docno'      => $tt_docno,
                            ':t_sn'         => $tt_sn
        ));
    } catch (PDOException $ex) {
        $err = $err . $ex->getMessage() . "<br/>";
    }

// 	// input/output tax
//     if ( !empty($tt_taxamt) ) {

//         $tx_sn = str_pad(trim(strval(intval($tt_sn)+1000)),4,"0");

//         try {
//             $stmt = $pdo->prepare('INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE) VALUES (:t_docno,:t_sn,:t_KEYUSER,:t_KEYDATE)');
//             $stmt->execute(array(':t_docno'=>$tt_docno,':t_sn'=>$tx_sn,':t_KEYUSER'=>$pbUserCode,':t_KEYDATE'=>$currdate));
//         } catch(PDOException $ex) {
//             $err = $err . $ex->getMessage() . "<br/>";
//         }

//         try {
//             $ttdate     = date("m/Y", strtotime($tt_date));
//             $t_date     = date("Y-m-d", strtotime($tt_date));
//             $t_mc 		= 13;
//             $timport 	= 'Y';
//             $tttaxamt	= abs($tt_taxamt);

//             $stmt = $pdo->prepare('UPDATE trn
//                                 SET
//                                 t_mtype 	=: t_mtype,
//                                 t_batch 	=: t_batch,
//                                 t_term 		=: t_term,
//                                 t_type 		=: t_type,
//                                 t_mc 		=: t_mc,
//                                 t_date 		=: t_date,
//                                 t_gdate 	=: t_gdate,
//                                 t_acode 	=: t_acode,
//                                 t_descr 	=: t_descr,
//                                 t_dramt 	=: t_dramt,
//                                 t_cramt 	=: t_cramt,
//                                 t_drcurr	=: t_drcurr,
//                                 t_crcurr	=: t_crcurr,
//                                 t_exrate	=: t_exrate,
//                                 t_import 	=: t_import,
//                                 t_modiuser  =: t_modiuser,
//                                 t_modidate 	=: t_modidate

//                                 WHERE t_docno=:t_docno
//                                 AND t_sn=:t_sn');
//             $stmt->execute(array(
//                                 ':t_mtype'      => $tt_dType,
//                                 ':t_batch'      => $tt_batch,
//                                 ':t_term'       => $ttdate,
//                                 ':t_type'       => $tt_type,
//                                 ':t_mc'         => $t_mc,
//                                 ':t_date'       => $t_date,
//                                 ':t_gdate'      => $t_date,
//                                 ':t_acode'      => $tt_iocode,
//                                 ':t_descr'      => $tt_subject,
//                                 ':t_dramt'      => $tx_gstDrAmt,
//                                 ':t_cramt'      => $tx_gstCrAmt,
//                                 ':t_drcurr'     => $tx_gstDrCurr,
//                                 ':t_crcurr'     => $tx_gstCrCurr,
//                                 ':t_exrate'     => $tt_exrate,
//                                 ':t_import'     => $timport,
//                                 ':t_modiuser'   => $pbUserCode,
//                                 ':t_modidate'   => $currdate,
//                                 ':t_docno'      => $tt_docno,
//                                 ':t_sn'         => $tx_sn

//             ));
//         } catch (PDOException $ex) {
//             $err = $err . $ex->getMessage() . "<br/>";
//         }
//     }
}







// // for rounding adj
// if($tt_round > 0.00){
//     $tx_sn = str_pad(trim(strval(intval($tt_sn)+1500)),4,"0");

//     try {
//         $stmt = $pdo->prepare('INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE) VALUES (:t_docno,:t_sn,:t_KEYUSER,:t_KEYDATE)');
//         $stmt->execute(array(':t_docno'=>$tt_docno,':t_sn'=>$tx_sn,':t_KEYUSER'=>$pbUserCode,':t_KEYDATE'=>$currdate));
//     } catch(PDOException $ex) {
//         $err = $err . $ex->getMessage() . "<br/>";
//     }

//     try {
//         $ttdate     = date("m/Y", strtotime($tt_date));
//         $t_date     = date("Y-m-d", strtotime($tt_date));
//         $t_mc 		= 13;
//         $timport 	= 'Y';

//         $tdramt 	= ($tt_round>0.00)? abs($tt_round) : 0.00;
//         $tcramt 	= ($tt_round<0.00)? $tt_round : 0.00;
//         $tdrcurr 	= sprintf("%0.2f",round($tt_exrate * (($tt_round>0.00)? abs($tt_round) : 0.00),2));
//         $tcrcurr 	= sprintf("%0.2f",round($tt_exrate * (($tt_round<0.00)? $tt_round : 0.00),2));

//         $stmt = $pdo->prepare('UPDATE trn SET
//                             t_mtype 	=: t_mtype,
//                             t_batch 	=: t_batch,
//                             t_term 		=: t_term,
//                             t_type 		=: t_type,
//                             t_mc 		=: t_mc,
//                             t_date 		=: t_date,
//                             t_gdate 	=: t_gdate,
//                             t_acode 	=: t_acode,
//                             t_descr 	=: t_descr,
//                             t_dramt 	=: t_dramt,
//                             t_cramt 	=: t_cramt,
//                             t_drcurr	=: t_drcurr,
//                             t_crcurr	=: t_crcurr,
//                             t_exrate	=: t_exrate,
//                             t_import 	=: t_import,
//                             t_modiuser  =: t_modiuser,
//                             t_modidate 	=: t_modidate

//                             WHERE t_docno=:t_docno
//                             AND t_sn=:t_sn');
//         $stmt->execute(array(
//                             ':t_mtype'      => $tt_dType,
//                             ':t_batch'      => $tt_batch,
//                             ':t_term'       => $ttdate,
//                             ':t_type'       => $tt_type,
//                             ':t_mc'         => $t_mc,
//                             ':t_date'       => $tt_date,
//                             ':t_gdate'      => $tt_gdate,
//                             ':t_acode'      => $tx_roundac,
//                             ':t_descr'      => $tt_subject,
//                             ':t_dramt'      => $tdramt,
//                             ':t_cramt'      => $tcramt,
//                             ':t_drcurr'     => $tdrcurr,
//                             ':t_crcurr'     => $tcrcurr,
//                             ':t_exrate'     => $tt_exrate,
//                             ':t_import'     => $timport,
//                             ':t_modiuser'   => $pbUserCode,
//                             ':t_modidate'   => $currdate,
//                             ':t_docno'      => $tt_docno,
//                             ':t_sn'         => $tx_sn
//         ));
//     } catch (PDOException $ex) {
//         $err = $err . $ex->getMessage() . "<br/>";
//     }
// }


// // Credit debtor/creditor A/C
// try {
//     $acc_dnmtx = $pdo->query("SELECT * FROM acc_dnmt WHERE m_docno=:$strdocno")->fetchAll(PDO::FETCH_OBJ);
//     $tt_billdebtor = $acc_cnmtx->m_billdebtor;
//     $tt_billcreditor = $acc_dnmtx->m_billcreditor;
// } catch (PDOException $ex) {
//     $err = $err . $ex->getMessage() . "<br/>";
// }



// try {
//     $acc_dndtx = $pdo->query("SELECT * FROM acc_dndt WHERE t_docno=:$strdocno")->fetchAll(PDO::FETCH_OBJ);
// } catch (PDOException $ex) {
//     $err = $err . $ex->getMessage() . "<br/>";
// }


// switch($tx_dbcr){
//     case "DB":
//         if($tx_ptype = "B"){
//             try {
//                 $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);

//                 foreach ($syspec as $syspc){
//                     if ($syspc->S_DNDETAIL == 0){
//                         try {
//                             $stmt = $pdo->prepare("SELECT acc_dnmtx.m_docno, acc_dnmtx.m_date, acc_dnmtx.m_gdate, acc_dnmtx.m_batch,
//                                 acc_dnmtx.m_drac, acc_dnmtx.m_name, acc_dnmtx.m_ra, '1001' as t_sn, acc_dnmtx.m_gldescr as t_subject,
//                                 acc_dndtx.t_amtt, str_repeat('&nbsp;', 20) as t_bill, str_repeat('&nbsp;', 20) as t_paybill, acc_dndtx.t_exrate
//                                 FROM acc_dnmtx
//                                 INNER JOIN acc_dndtx ON acc_dnmtx.m_docno = acc_dndtx.t_docno
//                                 INTO CURSOR appendx
//                                 where acc_dnmtx.m_docno=:$strdocno and (TRIM(acc_dndtx.t_crac) == '-' or empty(acc_dndtx.t_crac)) and acc_dndtx.t_amt <> 0.00")->fetchAll(PDO::FETCH_OBJ);
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }

//                         try {
//                             $stmt = $pdo->prepare("SELECT m_docno, m_date, m_gdate, m_batch, m_drac, m_name,m_ra,
//                                                     t_sn, t_subject, t_bill, t_paybill, MAX(t_exrate) as t_exrate, SUM(t_amtt) as t_amtt
//                                                     FROM appendx INTO CURSOR appendx readwrite;
//                                                     GROUP BY m_docno, m_date, m_gdate, m_batch, m_drac, m_name, m_ra, t_sn, t_subject, t_bill, t_paybill ");
//                             $stmt->execute(array());
//                             $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }

//                     }else{
//                         try {
//                             $stmt = $pdo->prepare("SELECT acc_dnmtx.*, acc_dndtx.*
//                                                     FROM acc_dnmtx
//                                                     INNER JOIN acc_dndtx ON acc_dnmtx.m_docno = acc_dndtx.t_docno
//                                                     WHERE trim(t_crac == '-' or empty(t_crac)) and acc_dndtx.t_amt <> 0.00 ");
//                             $stmt->execute(array());
//                             $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }
//                     }
//                 }
//             } catch (PDOException $ex) {
//                 $err = $err . $ex->getMessage() . "<br/>";
//             }
//         }
//         if ($tx_ptype = "O"){

//             try {
//                 $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);

//                 foreach ($syspec as $syspc){
//             if ($syspc->S_DNDETAIL == 0){
//                 try {
//                     $stmt = $pdo->prepare("SELECT acc_dnmtx.m_docno, acc_dnmtx.m_date, acc_dnmtx.m_gdate, acc_dnmtx.m_batch,
//                                                 acc_dnmtx.m_drac, acc_dnmtx.m_name, acc_dnmtx.m_gldescr as t_subject, acc_dnmtx.m_tamtt,
//                                                 acc_dnmtx.m_ra, acc_dnmtx.m_docno as t_bill, str_repeat('&nbsp;', 20) as t_paybill,
//                                                 acc_dndtx.t_exrate, SUM(acc_dndtx.t_amtt) t_amtt
//                                             FROM acc_dnmtx
//                                             INNER JOIN acc_dndtx ON acc_dnmtx.m_docno = acc_dndtx.t_docno
//                                             WHERE (trim(acc_dndtx.t_crac) == '-' or empty(acc_dndtx.t_crac)) and acc_dndtx.t_amt <> 0.00 and !empty(acc_dndtx.t_bill)
//                                             GROUP BY acc_dnmtx.m_docno,acc_dnmtx.m_date,acc_dnmtx.m_gdate,acc_dnmtx.m_batch,acc_dnmtx.m_drac,
//                                             acc_dnmtx.m_name,acc_dnmtx.m_gldescr,acc_dnmtx.m_tamtt,acc_dnmtx.m_ra,acc_dndtx.t_exrate");
//                     $stmt->execute(array());
//                     $appendx2_affected_rows = $stmt->rowCount();
//                     $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);

//                     if($appendx2_affected_rows > 1){
//                         try {
//                             $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_drac,m_name,t_subject,m_ra,t_bill,t_paybill,t_exrate,t_amtt
//                                                     FROM appendx2 INTO CURSOR appendx");
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }
//                     }else{
//                         try {
//                             $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_drac,m_name,t_subject,m_tamtt as t_amtt,m_ra,t_bill,t_paybill,t_exrate
//                                                     FROM appendx2 INTO CURSOR appendx");
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }
//                     }
//                 } catch (PDOException $ex) {
//                     $err = $err . $ex->getMessage() . "<br/>";
//                 }

//             }else{
//                 try {
//                     $stmt = $pdo->prepare("SELECT acc_dnmtx.*, acc_dndtx.*
//                                             FROM acc_dnmtx
//                                             INNER JOIN acc_dndtx ON (acc_dnmtx.m_docno = acc_dndtx.t_docno)
//                                             WHERE (trim(acc_dndtx.t_crac) == '-' or empty(acc_dndtx.t_crac)) and acc_dndtx.t_amt <> 0.00
//                                             INTO CURSOR appendx");
//                 } catch (PDOException $ex) {
//                     $err = $err . $ex->getMessage() . "<br/>";
//                 }
//             }
//         }
//     } catch (PDOException $ex) {
//         $err = $err . $ex->getMessage() . "<br/>";
//     }
//         }

//     case "CR":
//         if($tx_ptype = "B"){
//             try {
//                 $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);
//                 foreach ($syspec as $syspc){
//                     if ($syspc->S_DNDETAIL == 0){
//                         try {
//                             $stmt = $pdo->prepare("SELECT acc_dnmtx.m_docno,acc_dnmtx.m_date,acc_dnmtx.m_gdate,acc_dnmtx.m_batch,acc_dnmtx.m_drac,
//                                                         acc_dnmtx.m_name,acc_dnmtx.m_ra,'1001' as t_sn,acc_dnmtx.m_gldescr as t_subject,acc_dndtx.t_amtt,
//                                                         str_repeat('&nbsp;', 20) as t_bill,str_repeat('&nbsp;', 20) as t_paybill,acc_dndtx.t_exrate
//                                                     FROM acc_dnmtx
//                                                     INNER JOIN acc_dndtx ON (acc_dnmtx.m_docno = acc_dndtx.t_docno)
//                                                     INTO CURSOR appendx
//                                                     WHERE (trim(acc_dndtx.t_crac) == '-' or empty(acc_dndtx.t_crac)) and acc_dndtx.t_amt <> 0.00");
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }

//                         try {
//                             $stmt = $pdo->prepare("SELECT acc_dndtx.m_docno, acc_dndtx.m_date, acc_dndtx.m_gdate, acc_dndtx.m_batch,
//                                                         acc_dndtx.m_drac, acc_dndtx.m_name, acc_dndtx.m_ra, acc_cndtx.t_sn,
//                                                         acc_cndtx.t_subject, acc_cndtx.t_amtt, acc_cndtx.t_bill, acc_cndtx.t_paybill,
//                                                         MAX(acc_cndtx.t_exrate) as t_exrate, SUM(acc_cndtx.t_amtt) as t_amtt
//                                                     FROM appendx INTO CURSOR appendx readwrite
//                                                     GROUP BY acc_dndtx.m_docno, acc_dndtx.m_date, acc_dndtx.m_gdate, acc_dndtx.m_batch, acc_dndtx.m_crac,
//                                                     acc_dndtx.m_name, acc_dndtx.m_ra, acc_cndtx.t_sn, acc_cndtx.t_subject, acc_cndtx.t_bill, acc_cndtx.t_paybill");
//                             } catch (PDOException $ex) {
//                                 $err = $err . $ex->getMessage() . "<br/>";
//                             }
//                     }else{
//                         try{
//                         $stmt = $pdo->prepare("SELECT acc_dnmtx.m_docno,acc_dnmtx.m_date,acc_dnmtx.m_gdate,acc_dnmtx.m_batch,acc_dnmtx.m_drac,
//                                                         acc_dnmtx.m_name,acc_dnmtx.m_ra,sum(acc_dndtx.t_amtt) t_amtt,
//                                                         str_repeat('&nbsp;', 20) as t_bill,str_repeat('&nbsp;', 20) as t_paybill,acc_dndtx.t_exrate
//                                                     FROM appendx INTO CURSOR appendx readwrite
//                                                     GROUP BY acc_dnmtx.m_docno, acc_dnmtx.m_date, acc_dnmtx.m_gdate, acc_dnmtx.m_batch, acc_dnmtx.m_crac,
//                                                     acc_dnmtx.m_name, acc_dnmtx.m_ra, acc_dndtx.t_sn, acc_dndtx.t_subject, acc_dndtx.t_bill, acc_dndtx.t_paybill");
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }
//                     }
//                 break;
//                 }
//             } catch (PDOException $ex) {
//                 $err = $err . $ex->getMessage() . "<br/>";
//             }
//         }

//         if($tx_ptype = "O"){
//             try {
//                 $syspec = $pdo->query("SELECT * FROM syspec")->fetchAll(PDO::FETCH_OBJ);
//                 foreach ($syspec as $syspc){
//                     if ($syspc->S_DNDETAIL == 0){
//                         try {
//                             $stmt = $pdo->prepare("SELECT acc_dnmtx.m_docno, acc_dnmtx.m_date, acc_dnmtx.m_gdate, acc_dnmtx.m_batch,
//                                                         acc_dnmtx.m_drac, acc_dnmtx.m_name, acc_dnmtx.m_gldescr as t_subject, acc_dnmtx.m_tamtt,
//                                                         acc_dnmtx.m_ra, acc_dnmtx.t_bill, acc_dnmtx.t_paybill,
//                                                         acc_dndtx.t_exrate, SUM(acc_dndtx.t_amtt) t_amtt
//                                                     FROM acc_dnmtx
//                                                     INNER JOIN acc_dndtx ON acc_dnmtx.m_docno = acc_dndtx.t_docno
//                                                     WHERE (trim(acc_dndtx.t_crac) == '-' or empty(trim(acc_dndtx.t_crac))
//                                                         and acc_dndtx.t_amt <> 0.00
//                                                         and (!empty(trim(acc_dndtx.t_paybill)) or !empty(trim(acc_dndtx.t_bill)));
//                                                     GROUP BY acc_dnmtx.m_docno,acc_dnmtx.m_date,acc_dnmtx.m_gdate,acc_dnmtx.m_batch,acc_dnmtx.m_drac,acc_dnmtx.m_name,acc_dnmtx.m_gldescr,acc_dnmtx.m_tamtt,acc_dnmtx.m_ra,acc_dndtx.t_bill,acc_dndtx.t_paybill,acc_dndtx.t_exrate");
//                             $stmt->execute(array());
//                             $appendx2_affected_rows = $stmt->rowCount();
//                             $appendx2 = $stmt->fetchAll(PDO::FETCH_OBJ);


//                             if ($appendx2_affected_rows > 1){
//                                 try {
//                                     $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_drac,m_name,t_subject,m_ra,
//                                                             t_bill,t_paybill,t_exrate,t_amtt
//                                                             FROM appendx2 INTO CURSOR appendx");
//                                 } catch (PDOException $ex) {
//                                     $err = $err . $ex->getMessage() . "<br/>";
//                                 }
//                             }else{
//                                 try {
//                                     $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_drac,m_name,t_subject,m_tamtt as t_amtt,m_ra,
//                                                             t_bill,t_paybill,t_exrate
//                                                             FROM appendx2 INTO CURSOR appendx");
//                                 } catch (PDOException $ex) {
//                                     $err = $err . $ex->getMessage() . "<br/>";
//                                 }
//                             }

//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }
//                     }else{
//                         try {
//                             $stmt = $pdo->prepare("SELECT acc_dnmtx.m_docno,acc_dnmtx.m_date,acc_dnmtx.m_gdate,acc_dnmtx.m_batch,acc_dnmtx.m_drac,acc_dnmtx.m_name,acc_dnmtx.m_ra,
//                                                         acc_dndtx.t_subject,acc_dndtx.t_bill,acc_dndtx.t_paybill,acc_dndtx.t_exrate,SUM(acc_dndtx.t_amtt) as t_amtt
//                                                     FROM acc_dnmtx
//                                                     INNER JOIN acc_dndtx ON (acc_dnmtx.m_docno = acc_dndtx.t_docno) ;
//                                                     WHERE (trim(acc_dndtx.t_drac) == '-' or empty(acc_dndtx.t_drac))
//                                                     and acc_dndtx.t_amt <> 0.00
//                                                     AND (!empty(trim(acc_dndtx.t_paybill)) OR !empty(trim(acc_dndtx.t_bill))) ;
//                                                     INTO CURSOR appendx ;
//                                                     GROUP BY acc_dnmtx.m_docno,acc_dnmtx.m_date,acc_dnmtx.m_gdate,acc_dnmtx.m_batch,acc_dnmtx.m_drac,acc_dnmtx.m_name,
//                                                     acc_dnmtx.m_round,acc_dnmtx.m_ra,acc_dndtx.t_subject,acc_dndtx.t_bill,acc_dndtx.t_paybill,acc_dndtx.t_exrate");
//                             $stmt->execute(array());
//                             $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
//                         } catch (PDOException $ex) {
//                             $err = $err . $ex->getMessage() . "<br/>";
//                         }
//                     }

//                     try {
//                         $acc_dnmtx = $pdo->query("SELECT * FROM acc_dnmt WHERE m_docno=:$strdocno")->fetchAll(PDO::FETCH_OBJ);
//                         $tt_billdebtor = $acc_cnmtx->m_billdebtor;
//                         $tt_billcreditor = $acc_dnmtx->m_billcreditor;


//                         if($tt_billcreditor == "Y"){
//                             if ($syspc->S_DNDETAIL == 0){
//                                 try {
//                                     $stmt = $pdo->prepare("SELECT acc_dnmtx.m_docno, acc_dnmtx.m_date, acc_dnmtx.m_gdate, acc_dnmtx.m_batch,
//                                                                 acc_dnmtx.m_drac, acc_dnmtx.m_name, acc_dnmtx.m_gldescr as t_subject, acc_dnmtx.m_tamtt,
//                                                                 acc_dnmtx.m_ra, acc_dnmtx.m_docno as t_bill, str_repeat('&nbsp;', 20) as t_paybill,
//                                                                 acc_dndtx.t_exrate, SUM(acc_dndtx.t_amtt) t_amtt
//                                                             FROM acc_dnmtx
//                                                             INNER JOIN acc_dndtx ON acc_dnmtx.m_docno = acc_dndtx.t_docno
//                                                             INTO CURSOR appendx2
//                                                             WHERE (trim(acc_dndtx.t_crac) == '-' or empty(acc_dndtx.t_crac)) and acc_dndtx.t_amt <> 0.00 and !empty(acc_dndtx.t_bill)
//                                                             GROUP BY acc_dnmtx.m_docno,acc_dnmtx.m_date,acc_dnmtx.m_gdate,acc_dnmtx.m_batch,acc_dnmtx.m_drac,
//                                                             acc_dnmtx.m_name,acc_dnmtx.m_gldescr,acc_dnmtx.m_tamtt,acc_dnmtx.m_ra,acc_dndtx.t_exrate");
//                                     $stmt->execute(array());
//                                     $appendx = $stmt->fetchAll(PDO::FETCH_OBJ);
//                                 } catch (PDOException $ex) {
//                                     $err = $err . $ex->getMessage() . "<br/>";
//                                 }
//                                 if(RECCOUNT("appendx2") > 1){
//                                     try {
//                                         $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_drac,m_name,t_subject,m_ra,t_bill,t_paybill,t_exrate,t_amtt
//                                                                 FROM appendx2 INTO CURSOR appendx");
//                                     } catch (PDOException $ex) {
//                                         $err = $err . $ex->getMessage() . "<br/>";
//                                     }
//                                 }else{
//                                     try {
//                                         $stmt = $pdo->prepare("SELECT m_docno,m_date,m_gdate,m_batch,m_drac,m_name,t_subject,m_tamtt as t_amtt,m_ra,t_bill,t_paybill,t_exrate
//                                                                 FROM appendx2 INTO CURSOR appendx");
//                                     } catch (PDOException $ex) {
//                                         $err = $err . $ex->getMessage() . "<br/>";
//                                     }
//                                 }

//                             }else{
//                                 try {
//                                     $stmt = $pdo->prepare("SELECT acc_dnmtx.*, acc_dndtx.*
//                                                             FROM acc_dnmtx
//                                                             INNER JOIN acc_dndtx ON (acc_dnmtx.m_docno = acc_dndtx.t_docno)
//                                                             WHERE (trim(acc_dndtx.t_crac) == '-' or empty(acc_dndtx.t_crac)) and acc_dndtx.t_amt <> 0.00
//                                                             INTO CURSOR appendx");
//                                 } catch (PDOException $ex) {
//                                     $err = $err . $ex->getMessage() . "<br/>";
//                                 }
//                             }
//                         }
//                     } catch (PDOException $ex) {
//                         $err = $err . $ex->getMessage() . "<br/>";
//                     }


//                     break;
//                 }
//             } catch (PDOException $ex) {
//                 $err = $err . $ex->getMessage() . "<br/>";
//             }

//         }
// }



// $tx_sn = 2000;

// // do{
// foreach($appendx as $appndx){
//     $tt_docno = $appndx->m_docno;
// 	$tt_date = $appndx->m_date;
// 	$tt_gdate = $appndx->m_gdate;
// 	$tt_batch = $appndx->m_batch;
// 	$tt_drac = $appndx->m_drac;
// 	$tt_name = $appndx->m_name;
// 	$tx_sn = prgAddFive($tx_sn);
// 	$tt_subject = IIF(!EMPTY($appndx->t_subject),$appndx->t_subject,ALLTRIM($tt_docno));
// 	$tt_amt = $appndx->t_amtt;
// 	$tt_bill = $appndx->t_bill;
// 	$tt_paybill = $appndx->t_paybill;
// 	$tt_exrate = $appndx->t_exrate;
//     $tt_ra = $appndx->m_ra;

//     try {
//         $stmt = $pdo->prepare('INSERT INTO trn (t_docno,t_sn,t_KEYUSER,t_KEYDATE) VALUES (:t_docno,:t_sn,:t_KEYUSER,:t_KEYDATE)');
//         $stmt->execute(array(':t_docno'=>$tt_docno,':t_sn'=>$tx_sn,':t_KEYUSER'=>$pbUserCode,':t_KEYDATE'=>$currdate));
//     } catch(PDOException $ex) {
//         $err = $err . $ex->getMessage() . "<br/>";
//     }

//     switch($tx_dbcr){
//         case "DB":
//             if($tx_ptype == "B"){
//                 //If Debtor is Brought Forward type
//                 $tt_bill = "";
//             }elseif($tx_ptype = "O"){
//                 //If Debtor is Open Item type
//                 $tt_bill = $tt_bill;
//             }
//             break;
//         case "CR":
//             if($tx_ptype == "B"){
//                 //If Creditor is Brought Forward type
//                 $tt_paybill = "";
//             }elseif($tx_ptype = "O"){
//                 //If Creditor is Open Item type
//                 $tt_paybill = $tt_paybill;
//                 if($tt_billdebtor == "Y"){
//                     //If Creditor is Open Item type
//                     $tt_bill = $tt_bill;
//                 }
//             }
//             break;
//         default:
//             $tt_paybill = "";
//             $tt_bill = "";
//             break;
//     }

//     if($tt_amt > 0.00){
//         $tt_dramt  = sprintf("%0.2f", round($tt_amt * $tt_exrate,2));
//         $tt_cramt  = 0.00;
//         $tt_drcurr = abs($tt_amt);
//         $tt_crcurr = 0.00;
//     }else{
//         $tt_dramt  = 0.00;
//         $tt_cramt  = sprintf("%0.2f", abs(round($tt_amt * $tt_exrate,2)));
//         $tt_drcurr = 0.00;
//         $tt_crcurr = abs($tt_amt);
//     }

//     if($tt_ra == 1 && RECCOUNT("appendx") == 1){
//         $tt_dramt  = round($tt_dramt);
//         $tt_cramt  = round($tt_cramt);
//     }

//     $tt_type = prgdbcrgltype($tt_drac);

//     try {
//         $ttdate = date("m/Y", strtotime($tt_date));
//         $tmc = 13;
//         $timport = 'Y';
//         $stmt = $pdo->prepare('UPDATE trn
//                         SET
//                         t_mtype 	=: t_mtype,
//                         t_batch 	=: t_batch,
//                         t_term 		=: t_term,
//                         t_type 		=: t_type,
//                         t_mc 		=: t_mc,
//                         t_date 		=: t_date,
//                         t_gdate 	=: t_gdate,
//                         t_acode 	=: t_acode,
//                         t_descr 	=: t_descr,
//                         t_bill		=: t_bill,
//                         t_paybill	=: t_paybill,
//                         t_drcurr	=: t_drcurr,
//                         t_crcurr	=: t_crcurr,
//                         t_dramt 	=: t_dramt,
//                         t_cramt 	=: t_cramt,
//                         t_import 	=: t_import,
//                         t_exrate	=: t_exrate,
//                         t_modiuser  =: t_modiuser,
//                         t_modidate 	=: t_modidate
//                         WHERE t_docno=:t_docno
//                         AND t_sn=:t_sn');
//         $stmt->execute(array(
//                         ':t_mtype' 	=> $tt_dType,
//                         ':t_batch' 	=> $tt_batch,
//                         ':t_term' 	=> $ttdate,
//                         ':t_type' 	=> $tt_type,
//                         ':t_mc' 	=> $tmc,
//                         ':t_date' 	=> $tt_date,
//                         ':t_gdate' 	=> $tt_gdate,
//                         ':t_acode' 	=> $tt_drac,
//                         ':t_descr' 	=> $tt_subject,
//                         ':t_bill'	=> $tt_bill,
//                         ':t_paybill'=> $tt_paybill,
//                         ':t_drcurr'	=> $tt_drcurr,
//                         ':t_crcurr'	=> $tt_crcurr,
//                         ':t_dramt' 	=> $tt_dramt,
//                         ':t_cramt' 	=> $tt_cramt,
//                         ':t_import' => $timport,
//                         ':t_exrate'	=> $tt_exrate,
//                         ':t_modiuser'   => $pbUserCode,
//                         ':t_modidate' 	=> $currdate,
//                         ':t_docno' => $tt_docno,
//                         ':t_sn' => $tx_sn
//         ));
//     } catch (PDOException $ex) {
//         $err = $err . $ex->getMessage() . "<br/>";
//     }
// }
// // }while (!feof($appendx));


// try {
//     $stmt = $pdo->prepare('UPDATE acc_Dnmt SET m_post =: m_post WHERE m_docno=:m_docno');
//     $stmt->execute(array(':m_post' => 'Y', ':m_docno' => $strDocno ));
// } catch (PDOException $ex) {
//     $err = $err . $ex->getMessage() . "<br/>";
// }

// include_once("footer.php");
// ?>
