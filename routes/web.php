<?php

use App\Model\Stockcode;
use App\Model\Category;
use App\Model\Product;
use App\Model\Brand;
use App\Model\Location;
use App\Model\Uom;
use App\Model\Debtor;
use App\Model\Creditor;
use App\Model\Cylinder;
use App\Model\Cylindercategory;
use App\Model\Cylindergroup;
use App\Model\Cylinderproduct;
use App\Model\Cylindertype;
use App\Model\Cylindervalvetype;
use App\Model\Cylinderhandwheeltype;
use App\Model\Manufacturer;
use App\Model\DeliveryNote;
use App\Model\PurchasedTable;
use App\Model\Gasrack;
use App\Model\PrintedIndexView;
use App\Model\Cashbill;
use App\Model\Invoice;
use App\Model\SystemSetup;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

// Route::get('/',function() {
//    return view("app");
// });



Route::get('/', function () {
    return redirect('home');
});



Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('clear/test', function () {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');
        dd("success");
});

    Route::post('cashbills/auth/minprice', 'CashbillController@getAuthenticateAjax')->name('auth.minprice.post');

    Route::resource('stockmaster/stockcodes', 'StockcodeController');

    Route::resource('stockmaster/products', 'ProductController');

    Route::resource('stockmaster/category', 'CategoryController');

    Route::resource('stockmaster/brands', 'BrandController');

    Route::resource('stockmaster/locations', 'LocationController');

    Route::resource('stockmaster/uom', 'UOMController');

    Route::resource('stockmaster/adjustmentits', 'AdjustmentitController');

    Route::resource('stockmaster/adjustmentots', 'AdjustmentotController');

    Route::resource('stockmaster/reasons', 'ReasonController');

    Route::resource('stockmaster/areas', 'AreaController');

    Route::resource('stockmaster/currencies', 'CurrencyController');

    Route::resource('stockmaster/customertypes', 'CustomerController');

    Route::resource('stockmaster/salesman', 'SalesmanController');

    Route::resource('stockmaster/equipments', 'EquipmentController');

    Route::resource('stockmaster/equipments', 'EquipmentController');

    Route::resource('cylindermaster/gasracks', 'GasrackController');
    Route::get('reportprinting/gasracksactivity/print', 'GasrackActivityController@print')->name('gasracksactivity.print');
    Route::get('cylindermaster/gasracks/gasrackcylinder/{id}', 'GasrackController@storeCylinder')->name('gasracks.storeCylinder');
    Route::get('cylindermaster/gasracks/search/filter', 'Gasrackcontroller@filter_records')->name('gasracks.search.filter');
    Route::get('cylindermaster/gasracks/search/rnfilter/{accountcode}', 'Gasrackcontroller@rn_filter_records')->name('gasracks.search.rnfilter');
    Route::delete('cylindermaster/gasracks/destroycylinders/{cylinder}', 'Gasrackcontroller@destroycylinder')->name('gasracks.destroycylinder');
    Route::delete('cylindermaster/gasracks/destroyslings/{sling}', 'Gasrackcontroller@destroysling')->name('gasracks.destroysling');

    Route::resource('cylindermaster/slings', 'SlingController');

    Route::resource('cylindermaster/shackles', 'ShackleController');


    Route::get('cylindermaster/cylinders/cylinderactivity', 'CylinderController@cylinderactivity')->name('cylinders.cylinderactivity');

    Route::get('cylindermaster/cylinders/cylinderactivity/listing', 'CylinderController@cylinderActivityListing')->name('cylinders.cylinderactivity.listing');

    Route::resource('cylindermaster/cylinders', 'CylinderController');
    Route::get('reportprinting/cylindermaster/print', 'CylinderController@print')->name('cylinders.print');
    Route::get('reportprinting/cylindermovement/print', 'CylinderMovementController@print')->name('cylindersmovement.print');
    Route::get('reportprinting/cylinderledger/print', 'CylinderLedgerController@print')->name('cylinderledger.print');
    Route::get('reportprinting/cylinderRental/print', 'CylinderRentalController@print')->name('cylinderRental.print');
    Route::get('reportprinting/Location/print', 'CylinderLocationController@print')->name('cylinderlocation.print');
    Route::get('reportprinting/CylinderSummaryHolding/print', 'CylinderSummaryHoldingController@print')->name('CylinderSummaryHolding.print');
    Route::get('reportprinting/CylinderActivity/print', 'CylinderActivityController@print')->name('CylinderActivity.print');
    Route::get('reportprinting/CylinderInvalid/print', 'CylinderInvalidController@print')->name('CylinderInvalid.print');
    Route::get('cylindermaster/search/filter', 'CylinderController@filter_records')->name('cylinders.search.filter');
    Route::get('cylindermaster/search/rnfilter/{accountcode}', 'CylinderController@rn_filter_records')->name('cylinders.search.rnfilter');
    Route::get('cylindermaster/search/cylinderfilter', 'CylinderController@getCylinderDetail')->name('cylinders.search.cylinderfilter');

    Route::get('cylindermaster/search/checkfilter', 'CylinderController@check_cylinders')->name('cylinders.check.filter');

    Route::get('cylindermaster/gasracks/search/checkfilter', 'Gasrackcontroller@check_slings')->name('slings.check.filter');

    Route::get('cylindermaster/slings/search/filter', 'SlingController@filter_records')->name('slings.search.filter');

    Route::resource('cylindermaster/cylindercategory', 'CylindercategoryController');


    Route::resource('cylindermaster/cylindergroups', 'CylindergroupController');


    Route::resource('cylindermaster/cylinderproducts', 'CylinderproductController');


    Route::delete('dailypro/goods/edit/{id}', 'GoodController@destroyGooddt')->name('goods.destroyGooddt');

    Route::get('reportprinting/debtormasterlisting/print', 'DebtorController@print')->name('debtors.print');

    Route::get('reportprinting/creditormasterlisting/print', 'CreditorController@print')->name('creditors.print');

    Route::get('reportprinting/stockmasterlisting/print', 'StockcodeController@print')->name('stock.print');

    Route::get('reportprinting/stockbalance/print', 'StockreportController@printStkBal')->name('stockbal.print');

    Route::get('reportprinting/stockledger/print', 'StockLedgerController@printStkLedger')->name('stockled.print');

    Route::get('reportprinting/stockmovement/print', 'StockMovementController@printStkMovement')->name('stockmov.print');

    Route::get('reportprinting/stockvaluefifo/print', 'StockValueController@printFIFO')->name('stockvalfifo.print');

    Route::get('reportprinting/stockvalueasat/print', 'StockValueController@printASAT')->name('stockvalasat.print');

    Route::resource('stockmaster/debtors', 'DebtorController');

    Route::resource('stockmaster/creditors', 'CreditorController');

    Route::get('stockmaster/creditors/api/get', 'CreditorController@api_get')->name('creditors.api.get');

    Route::get('stockmaster/debtors/api/get', 'DebtorController@api_get')->name('debtors.api.get');

    Route::resource('cylindermaster/cylindertypes', 'CylindertypeController');

    Route::resource('cylindermaster/cylindercontainertypes', 'CylindercontainertypeController');

    Route::resource('cylindermaster/cylinderhandwheeltypes', 'CylinderhandwheeltypeController');

    Route::resource('cylindermaster/cylindervalvetypes', 'CylindervalvetypeController');

    Route::resource('cylindermaster/gasracktypes', 'GasracktypeController');

    Route::resource('cylindermaster/slingtypes', 'SlingtypeController');

    Route::resource('cylindermaster/shackletypes', 'ShackletypeController');

    Route::resource('cylindermaster/manufacturers', 'ManufacturerController');

    Route::resource('cylindermaster/ownerships', 'OwnershipController');

    Route::resource('dailypro/preturns', 'PreturnController');
    Route::get('dailypro/preturns/report/print', 'PreturnController@print')->name('preturns.print');
    Route::get('dailypro/preturns/report/jasper/{id}/purchasedreturn', 'PreturnController@jasper')->name('preturns.jasper');

    Route::delete('dailypro/preturns/edit/{id}', 'PreturnController@destroyPreturndt')->name('preturns.destroyPreturndt');

    Route::resource('dailypro/cashbills', 'CashbillController');

    Route::get('dailypro/cashbills/report/print', 'CashbillController@print')->name('cashbills.print');

    Route::get('dailypro/cashbills/report/jasper/data/cashbills', 'CashbillController@jasper')->name('cashbills.jasper');

    Route::delete('dailypro/cashbills/edit/{id}', 'CashbillController@destroyCashbilldt')->name('cashbills.destroyCashbilldt');

    // Route::resource('dailypro/invoices', 'InvoiceController');
    Route::resource('settings/documentsetups', 'DocumentSetupController');

    Route::resource('settings/dnrnsetups', 'DNRNSetupController');

    Route::resource('dailypro/quotations', 'QuotationController');
    Route::get('dailypro/quotations/report/print', 'QuotationController@print')->name('quotations.print');
    Route::get('dailypro/quotations/report/jasper/{id}/quotation', 'QuotationController@jasper')->name('quotations.jasper');

    Route::delete('dailypro/quotations/edit/{id}', 'QuotationController@destroyQuotationdt')->name('quotations.destroyQuotationdt');

    Route::resource('dailypro/deliveryorders', 'DeliveryorderController');
    Route::get('dailypro/deliveryorders/report/print', 'DeliveryorderController@print')->name('deliveryorders.print');
    Route::get('dailypro/deliveryorders/report/jasper/{id}/deliveryorder', 'DeliveryorderController@jasper')->name('deliveryorders.jasper');

    Route::delete('dailypro/deliveryorders/edit/{id}', 'DeliveryorderController@destroyDeliveryorderdt')->name('deliveryorders.destroyDeliveryorderdt');

    Route::get('salesorders/qtno/get/{debtor}', 'Salesordercontroller@qtno_get')->name('salesorders.qtno.get');
    Route::resource('dailypro/salesorders', 'SalesorderController');
    Route::POST('dailypro/salesorders/getroutebydocno', 'SalesorderController@getRouteByDocno')->name('salesorders.searchdocno');
    Route::get('dailypro/salesorders/report/print', 'SalesorderController@print')->name('salesorders.print');
    Route::get('dailypro/salesorders/report/jasper/{id}/salesorder', 'SalesorderController@jasper')->name('salesorders.jasper');

    Route::delete('dailypro/salesorders/edit/{id}', 'SalesorderController@destroysalesorderdt')->name('salesorders.destroysalesorderdt');


    Route::resource('dailypro/salesreturns', 'SalesreturnController');
    Route::get('dailypro/salesreturns/report/print', 'SalesreturnController@print')->name('salesreturns.print');
    Route::get('dailypro/salesreturns/report/jasper/{id}/salesreturn', 'SalesreturnController@jasper')->name('salesreturns.jasper');
    Route::POST('dailypro/salesreturns/getroutebydocno', 'SalesreturnController@getRouteByDocno')->name('salesreturns.searchdocno');

    Route::delete('dailypro/salesreturns/edit/{id}', 'SalesreturnController@destroySalesreturndt')->name('salesreturns.destroySalesreturndt');

    Route::resource('dailypro/returnnotes', 'ReturnnoteController');
    Route::get('dailypro/returnnotes/report/print', 'ReturnnoteController@print')->name('returnnotes.print');
    Route::get('dailypro/returnnotes/{id}/jasper/returnnotes', 'ReturnnoteController@jasper')->name('returnnotes.jasper');

    Route::delete('dailypro/returnnotes/edit/{id}', 'ReturnnoteController@destroyReturnnotedt')->name('returnnotes.destroyReturnnotedt');

    Route::resource('dailypro/adjustmentis', 'AdjustmentiController');
    Route::get('dailypro/adjustmentis/report/print', 'AdjustmentiController@print')->name('adjustmentis.print');
    Route::get('dailypro/adjustmentis/report/jasper/{id}/adjustmentin', 'AdjustmentiController@jasper')->name('adjustmentis.jasper');

    Route::delete('dailypro/adjustmentis/edit/{id}', 'AdjustmentiController@destroyAdjustmentidt')->name('adjustmentis.destroyAdjustmentidt');

    Route::resource('dailypro/adjustmentos', 'AdjustmentoController');
    Route::get('dailypro/adjustmentos/report/print', 'AdjustmentoController@print')->name('adjustmentos.print');
    Route::get('dailypro/adjustmentos/report/jasper/{id}/adjustmentout', 'AdjustmentoController@jasper')->name('adjustmentos.jasper');

    Route::delete('dailypro/adjustmentos/edit/{id}', 'AdjustmentoController@destroyAdjustmentodt')->name('adjustmentos.destroyAdjustmentodt');

    Route::resource('dailypro/invoices', 'InvoiceController');
    Route::get('dailypro/invoices/report/print', 'InvoiceController@print')->name('invoices.print');
    Route::get('dailypro/invoices/report/jasper/{id}/invoice', 'InvoiceController@jasper')->name('invoices.jasper');
    Route::get('invoices/api/get', 'InvoiceController@api_get')->name('invoices.api.get');
    Route::POST('invoices/api/post', 'InvoiceController@api_store')->name('invoices.api.post');
    Route::get('invoices/api/get/acode/{acode}', 'InvoiceController@getInvoicesByAcode')->name('invoices.api.get.by-acode');
    Route::get('invoices/api/get/items/{invoice}', 'InvoiceController@getInvoiceItems')->name('invoices.api.get.items');
    Route::get('invoices/test-email', 'InvoiceController@emailInvoice')->name('invoices.test-email');
    Route::get('invoices/test-report/{id}', 'InvoiceController@testReport')->name('invoices.test-report');
    // Route::get('invoices/invoice/{id}/jasper', 'InvoiceController@jasper')->name('invoices.jasper');
    Route::get('invoice-data', 'InvoiceController@invoiceDatatableList')->name('invoice.datatable.data');

    Route::resource('dailypro/goods', 'GoodController');
    Route::get('dailypro/goods/report/print', 'GoodController@print')->name('goods.print');
    Route::get('dailypro/goods/report/jasper/{id}/goodsreceived', 'GoodController@jasper')->name('goods.jasper');
    Route::get('good-data', 'GoodController@goodDatatableList')->name('good.datatable.data');

    Route::delete('invoices/data/{data}', 'InvoiceController@destroyData')->name('invoices.data.destroy');

    Route::delete('goods/data/{data}', 'GoodController@destroyData')->name('goods.data.destroy');

    Route::delete('cashbills/data/{data}', 'CashbillController@destroyData')->name('cashbills.data.destroy');

    Route::delete('deliveryorders/data/{data}', 'DeliveryorderController@destroyData')->name('deliveryorders.data.destroy');

    Route::delete('salesorders/data/{data}', 'SalesorderController@destroyData')->name('salesorders.data.destroy');

    Route::delete('quotations/data/{data}', 'QuotationController@destroyData')->name('quotations.data.destroy');

    Route::delete('salesreturns/data/{data}', 'SalesreturnController@destroyData')->name('salesreturns.data.destroy');

    Route::delete('adjustmentis/data/{data}', 'AdjustmentiController@destroyData')->name('adjustmentis.data.destroy');

    Route::delete('adjustmentos/data/{data}', 'AdjustmentoController@destroyData')->name('adjustmentos.data.destroy');

    Route::delete('preturns/data/{data}', 'PreturnController@destroyData')->name('preturns.data.destroy');

    Route::POST('goods/api/post', 'GoodController@api_store')->name('goods.api.post');

    Route::POST('cashbills/api/post', 'CashbillController@api_store')->name('cashbills.api.post');

    Route::POST('cashbills/docno/frm', 'CashbillController@searchDocno')->name('cashbills.docno.frm');

    Route::get('cashbills/api/get_print_details', 'CashbillController@get_print_details')->name('cashbills.api.get_print_details');

    Route::POST('adjustmentis/api/post', 'AdjustmentiController@api_store')->name('adjustmentis.api.post');

    Route::POST('adjustmentos/api/post', 'AdjustmentoController@api_store')->name('adjustmentos.api.post');

    Route::get('returnnotes/api/get', 'ReturnnoteController@api_get')->name('returnnotes.api.get');

    Route::POST('preturns/api/post', 'PreturnController@api_store')->name('preturns.api.post');

    Route::POST('porders/api/post', 'PorderController@api_store')->name('porders.api.post');

    Route::POST('quotations/api/post', 'QuotationController@api_store')->name('quotations.api.post');

    Route::POST('deliveryorders/api/post', 'DeliveryorderController@api_store')->name('deliveryorders.api.post');

    Route::POST('salesorders/api/post', 'SalesorderController@api_store')->name('salesorders.api.post');

    Route::POST('salesreturns/api/post', 'SalesreturnController@api_store')->name('salesreturns.api.post');

    // Route::get('cashbills/api/get', 'CashbillController@api_get')->name('cashbills.api.get');

    Route::post('stockmaster/debtors/api/store', 'DebtorController@api_store')->name('debtors.api.store');

    Route::post('stockmaster/creditors/api/store', 'CreditorController@api_store')->name('creditors.api.store');

    Route::get('reportprinting/transaction', function () {
        $docno_select = [];
        $docno_select1 = Cashbill::pluck('docno', 'docno')->toArray();
        $docno_select2 = Invoice::pluck('docno', 'docno')->toArray();
        $docno_select = array_merge($docno_select, $docno_select1, $docno_select2);
        $debtor_select = Debtor::pluck('accountcode', 'accountcode')->toArray();
        // $cashbills = Cashbill::paginate(15);
        // $Debtor = Debtor::pluck('accountcode', 'accountcode');
        // $print = PrintedIndexView::where('index', 'Debtor Master Listing')->pluck('printed_by');
        $page_title = "Transaction Listing Report";
        return view(
            'reportprinting.inventory.transactionprinting',
            compact('page_title', 'docno_select', 'docno_select2', 'debtor_select')
        );
    })->name('reportprinting.transaction');

    // Route::get('reportprinting/transactioninv', function () {
    //     // $docno_select = Cashbill::pluck('docno', 'docno')->toArray();
    //     $debtor_select = Debtor::pluck('accountcode', 'accountcode')->toArray();
    //     // $cashbills = Cashbill::paginate(15);
    //     // $Debtor = Debtor::pluck('accountcode', 'accountcode');
    //     // $print = PrintedIndexView::where('index', 'Debtor Master Listing')->pluck('printed_by');
    //     $page_title = "Invoice Listing Report";
    //     return view(
    //         'reportprinting.inventory.invoiceprinting',
    //         compact('page_title', 'Invoice', 'print', 'debtor_select')
    //     );
    // })->name('reportprinting.transactioninv');

    Route::get('reportprinting/debtormasterlisting', function () {
        $Debtor = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');
        $print = PrintedIndexView::where('index', 'Debtor Master Listing')->pluck('printed_by');
        $page_title = "Debtor Listing Report";
        return view(
            'reportprinting.inventory.debtormasterlisting',
            compact('page_title', 'Debtor', 'print')
        );
    })->name('reportprinting.debtormasterlisting');

    Route::get('reportprinting/creditormasterlisting', function () {
        $Creditor = Creditor::pluck('accountcode', 'accountcode');
        $print = PrintedIndexView::where('index', 'Creditor Master Listing')->pluck('printed_by');
        $page_title = "Creditor Listing Report";
        return view(
            'reportprinting.inventory.creditormasterlisting',
            compact('page_title', 'Creditor', 'print')
        );
    })->name('reportprinting.creditormasterlisting');

    Route::get('reportprinting/stockmasterlisting', function () {
        $Stockcode = Stockcode::orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'Stock Master Listing')->pluck('printed_by');
        $page_title = "Stock Listing Report";
        return view(
            'reportprinting.inventory.stockmasterlisting',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'print')
        );
    })->name('reportprinting.stockmasterlisting');

    Route::get('reportprinting/stockbalance', function () {
        $Stockcode = Stockcode::orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'Stock Balance Listing')->pluck('printed_by');
        $page_title = "Stock Balance / Stock Take List";
        return view(
            'reportprinting.inventory.stockbalance',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'print')
        );
    })->name('reportprinting.stockbalance');

    Route::get('reportprinting/stockledger', function () {
        $Stockcode = Stockcode::orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'Stock Ledger Listing')->pluck('printed_by');
        $page_title = "Stock Ledger Report";
        return view(
            'reportprinting.inventory.stockledger',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'print')
        );
    })->name('reportprinting.stockledger');

    Route::get('reportprinting/stockvalue_fifo', function () {
        $Stockcode = Stockcode::orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'Stock Value FIFO')->pluck('printed_by');
        $page_title = "Stock Value (FIFO) Report";
        return view(
            'reportprinting.inventory.stockvalue_fifo',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'print')
        );
    })->name('reportprinting.stockvalue_fifo');

    Route::get('reportprinting/stockvalue_asat', function () {
        $Stockcode = Stockcode::orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'Stock Value ASAT')->pluck('printed_by');
        $page_title = "Stock Value (As at) Report";
        return view(
            'reportprinting.inventory.stockvalue_asat',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'print')
        );
    })->name('reportprinting.stockvalue_asat');

    Route::get('reportprinting/stockmovement', function () {
        $Stockcode = Stockcode::orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'Stock Movement')->pluck('printed_by');
        $page_title = "Stock Movement Report";
        return view(
            'reportprinting.inventory.stockmovement',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'print')
        );
    })->name('reportprinting.stockmovement');

    Route::get('reportprinting/cylindermasterlisting', function () {
        $Category = Cylindercategory::orderBy('code')->pluck('code', 'code');
        $Group = Cylindergroup::orderBy('code')->pluck('code', 'code');
        $Product = Cylinderproduct::orderBy('code')->pluck('code', 'code');
        $Type = Cylindertype::orderBy('code')->pluck('code', 'code');
        $ValveType = Cylindervalvetype::orderBy('code')->pluck('code', 'code');
        $Cylinderhandwheeltype = Cylinderhandwheeltype::orderBy('code')->pluck('code', 'code');
        $Manufacturer = Manufacturer::orderBy('code')->pluck('code', 'code');

        $page_title = "Cylinder Listing Report";
        return view(
            'reportprinting.cylinder.cylindermasterlisting',
            compact('page_title', 'Group', 'Category', 'Product', 'Type', 'ValveType', 'Cylinderhandwheeltype', 'Manufacturer')
        );
    })->name('reportprinting.cylindermasterlisting');

    Route::get('reportprinting/cylindermovementreport', function () {
        $Category = Cylindercategory::orderBy('code')->pluck('code', 'code');
        $Group = Cylindergroup::orderBy('code')->pluck('code', 'code');
        $Product = Cylinderproduct::orderBy('code')->pluck('code', 'code');
        $Type = Cylindertype::orderBy('code')->pluck('code', 'code');
        $Debtor = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');

        $page_title = "Cylinder Movement Report";
        return view(
            'reportprinting.cylinder.cylindermovementreport',
            compact('page_title', 'Group', 'Category', 'Product', 'Type', 'Debtor')
        );
    })->name('reportprinting.cylindermovementreport');

    Route::get('reportprinting/cylinderledgerreport', function () {
        $Category = Cylindercategory::orderBy('code')->pluck('code', 'code');
        $Group = Cylindergroup::orderBy('code')->pluck('code', 'code');
        $Product = Cylinderproduct::orderBy('code')->pluck('code', 'code');
        $Type = Cylindertype::orderBy('code')->pluck('code', 'code');
        $Debtor = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');

        $page_title = "Cylinder Ledger Report";
        return view(
            'reportprinting.cylinder.cylinderledgerreport',
            compact('page_title', 'Group', 'Category', 'Product', 'Type', 'Debtor')
        );
    })->name('reportprinting.cylinderledgerreport');

    Route::get('reportprinting/cylinderRentalreport', function () {
        $Debtor = Debtor::orderBy('accountcode')->pluck('name', 'accountcode');

        $page_title = "Cylinder Rental Statement Report";
        return view(
            'reportprinting.cylinder.cylinderRentalreport',
            compact('page_title', 'Debtor')
        );
    })->name('reportprinting.cylinderRentalreport');

    Route::get('reportprinting/cylinderLocationreport', function () {
        $Debtor = Debtor::orderBy('accountcode')->get()->pluck('detail', 'accountcode');

        $page_title = "Cylinder Location Statement Report";
        return view(
            'reportprinting.cylinder.cylinderlocationreport',
            compact('page_title', 'Debtor')
        );
        // return response()->json($Debtor);
    })->name('reportprinting.cylinderLocationreport');

    Route::get('reportprinting/CylinderSummaryHolding', function () {
        $Debtor = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');
        $Product = Cylinderproduct::orderBy('code')->pluck('code', 'code');
        $DeliveryNote = DeliveryNote::orderBy('dn_no')->pluck('dn_no', 'dn_no');

        $page_title = "Cylinder Holding Summary Report";
        return view(
            'reportprinting.cylinder.CylinderSummaryHolding',
            compact('page_title', 'Debtor', 'Product', 'DeliveryNote')
        );
    })->name('reportprinting.CylinderSummaryHolding');

    Route::get('reportprinting/CylinderActivity', function () {
        $Debtor = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');
        $Product = Cylinderproduct::orderBy('code')->pluck('code', 'code');

        $page_title = "Customer Cylinder Activity Report";
        return view(
            'reportprinting.cylinder.CylinderActivity',
            compact('page_title', 'Debtor', 'Product')
        );
    })->name('reportprinting.CylinderActivity');

    Route::get('reportprinting/RackActivity', function () {
        $Debtor = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');
        $Gasrack = Gasrack::orderBy('serial')->pluck('serial', 'serial');

        $page_title = "Customer Rack Activity Report";
        return view(
            'reportprinting.cylinder.RackActivity',
            compact('page_title', 'Debtor', 'Gasrack')
        );
    })->name('reportprinting.RackActivity');

    Route::get('reportprinting/CylinderInvalid', function () {
        $Debtor = Debtor::orderBy('accountcode')->pluck('accountcode', 'accountcode');
        // $Product = Cylinderproduct::orderBy('code')->pluck('code', 'code');

        $page_title = "Invalid Cylinder Return Listing Report";
        return view(
            'reportprinting.cylinder.CylinderInvalid',
            compact('page_title', 'Debtor')
        );
    })->name('reportprinting.CylinderInvalid');

    Route::get('reportprinting/KPDNHEP_SUPPLIER', function () {
        $Stockcode = Stockcode::where('type','=','Stock Item')->orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'KPDNHEP Supplier')->pluck('printed_by');

        $page_title = "Supplier / Wholesaler Report";
        return view(
            'reportprinting.inventory.KPDNHEP_SUPPLIER',
            compact('page_title', 'Stockcode', 'print')
        );
    })->name('reportprinting.KPDNHEP_SUPPLIER');

    Route::get('reportprinting/KPDNHEP_RETAILER', function () {
        $Stockcode = Stockcode::where('type','=','Stock Item')->orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'KPDNHEP Retailer')->pluck('printed_by');

        $page_title = "Retailer Report";
        return view(
            'reportprinting.inventory.KPDNHEP_RETAILER',
            compact('page_title', 'Stockcode', 'print')
        );
    })->name('reportprinting.KPDNHEP_RETAILER');

    Route::get('reportprinting/KPDNHEP_Process', function () {
        $Stockcode = Stockcode::where('type','=','Stock Item')->orderBy('code')->pluck('code', 'code');
        $print = PrintedIndexView::where('index', 'KPDNHEP Process')->pluck('printed_by');
        if (!Session::has('Error') && !Session::has('Success') && !Session::has('view')) {
            // PurchasedTable::getQuery()->delete();
        }

        $page_title = "Processing Peruncit";
        return view(
            'reportprinting.inventory.KPDNHEP_Process',
            compact('page_title', 'Stockcode')
        );
    })->name('reportprinting.KPDNHEP_Process');

    Route::get('reportprinting/KPDNHEP_Belian', function () {
        $Stockcode = Stockcode::where('type','=','Stock Item')->orderBy('code')->pluck('code', 'code');

        $page_title = "Belian Reporting";
        return view(
            'reportprinting.inventory.KPDNHEP_BELIAN',
            compact('page_title', 'Stockcode')
        );
    })->name('reportprinting.KPDNHEP_BELIAN');

    Route::resource('dailypro/deliverynotes', 'DeliverynoteController');
    Route::get('dailypro/deliverynotes/report/print', 'DeliverynoteController@print')->name('deliverynotes.print');
    Route::get('dailypro/deliverynotes/{id}/jasper/deliverynote', 'DeliverynoteController@jasper')->name('deliverynotes.jasper');

    Route::delete('dailypro/deliverynotes/delete/{id}', 'DeliverynoteController@destroyDeliverynotedt')->name('deliverynotes.destroyDeliverynotedt');

    Route::get('deliverynotes/api/get', 'DeliverynoteController@api_get')->name('deliverynotes.api.get');

    Route::delete('deliverynotes/data/{data}', 'DeliverynoteController@destroyData')->name('deliverynotes.data.destroy');
    Route::delete('deliverynotes/gr/data/{data}', 'DeliverynoteController@destroyData2')->name('deliverynotes.data.destroygr');

    Route::post('deliverynotes/datalist/delete', 'DeliverynoteController@destroyDatalist')->name('deliverynotes.data.destroylist');

    // Route::post('deliverynotes/datalistgas/delete', 'DeliverynoteController@destroyDatalist')->name('deliverynotes.data.destroylistgas');

    Route::resource('dailypro/refills', 'RefillController');
    Route::get('dailypro/refills/report/print', 'RefillController@print')->name('refills.print');
    Route::get('dailypro/refills/report/jasper/{id}/refill', 'RefillController@jasper')->name('refills.jasper');

    Route::resource('dailypro/repairs', 'RepairController');
    Route::get('dailypro/repairs/report/print', 'RepairController@print')->name('repairs.print');
    Route::get('dailypro/repairs/report/jasper/{id}/repair', 'RepairController@jasper')->name('repairs.jasper');

    Route::delete('returnnotes/data/{data}', 'ReturnnoteController@destroyData')->name('returnnotes.data.destroy');

    Route::post('returnnotes/datalist', 'ReturnnoteController@destroyDatalist')->name('returnnotes.data.destroylist');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('settings/systemsetups', 'SystemSetupController');

    Route::resource('settings/usergroups', 'UserGroupController');

    Route::resource('settings/usersetups', 'UserSetupController');

    Route::POST('enquiry/customers/query', 'CustomerEnqController@query')->name('customers.query');

    Route::get('enquiry/customers/search', 'CustomerEnqController@search')->name('customers.search');

    Route::resource('enquiry/customers', 'CustomerEnqController');

    Route::POST('enquiry/suppliers/query', 'SupplierEnqController@query')->name('suppliers.query');

    Route::get('enquiry/suppliers/search', 'SupplierEnqController@search')->name('suppliers.search');

    Route::resource('enquiry/suppliers', 'SupplierEnqController');

    Route::resource('cylindermaster/drivers', 'DriverController');

    Route::resource('cylindermaster/manyfor', 'ManyforController');

    Route::resource('dailypro/cylinderinvoices', 'CylinderInvoiceController');
    Route::POST('dailypro/cylinderinvoices/query', 'CylinderInvoiceController@processing')->name('cylinderinvoices.query');
    Route::get('dailypro/cylinderinvoices/report/print', 'CylinderInvoiceController@print')->name('cylinderinvoices.print');
    Route::get('dailypro/cylinderinvoices/report/jasper/{id}/cylinderinvoice', 'CylinderInvoiceController@jasper')->name('cylinderinvoices.jasper');

    Route::get('reportprinting/Kpdnhep/supp', 'KpdnhepController@generate_supp')->name('Kpdnhep_supp.generate');

    Route::get('reportprinting/Kpdnhep/ret', 'KpdnhepController@generate_ret')->name('Kpdnhep_ret.generate');

    Route::get('reportprinting/Kpdnhep/process', 'KpdnhepController@process')->name('Kpdnhep_ret.process');

    Route::get('reportprinting/Kpdnhep/process/view', 'KpdnhepController@process_view')->name('Kpdnhep_ret.processview');

    Route::get('reportprinting/Kpdnhep/process/belian', 'KpdnhepController@process_belian')->name('Kpdnhep_ret.processbelian');

    Route::get('ajax/cylinder/get-by/serial/{serial}', 'CylinderInvoiceController@getDetailsBySerial')->name('ajax.cylinder.get.by-serial');

    Route::delete('dailypro/porders/edit/{id}', 'PorderController@destroyGooddt')->name('porders.destroyGooddt');

    Route::POST('dailypro/porders/getroutebydocno', 'PorderController@getRouteByDocno')->name('porders.searchdocno');

    Route::delete('porders/data/{data}', 'PorderController@destroyData')->name('porders.data.destroy');

    Route::get('porders/api/get', 'PorderController@api_get')->name('porders.api.get');

    Route::resource('dailypro/porders', 'PorderController');
    Route::get('dailypro/porders/report/print', 'PorderController@print')->name('porders.print');
    Route::get('dailypro/porders/report/jasper/{id}/purchasedorder', 'PorderController@jasper')->name('porders.jasper');

    Route::POST('dailypro/goods/searchindex', 'GoodController@searchindex')->name('goods.searchindex');

    Route::POST('dailypro/goods/getroutebydocno', 'GoodController@getRouteByDocno')->name('goods.searchdocno');

    Route::POST('dailypro/porders/searchindex', 'PorderController@searchindex')->name('porders.searchindex');

    Route::POST('dailypro/preturns/searchindex', 'PreturnController@searchindex')->name('preturns.searchindex');

    // Route::POST('dailypro/cashbills/index', 'CashbillController@searchindex')->name('cashbills.searchindex');

    Route::get('searchindex', 'CashbillController@searchindex')->name('cashbills.searchindex');

    Route::POST('dailypro/invoices/searchindex', 'InvoiceController@searchindex')->name('invoices.searchindex');

    Route::POST('dailypro/deliveryorders/searchindex', 'DeliveryorderController@searchindex')->name('deliveryorders.searchindex');

    Route::POST('dailypro/deliveryorders/getroutebydocno', 'DeliveryorderController@getRouteByDocno')->name('deliveryorders.searchdocno');

    Route::POST('dailypro/salesorders/searchindex', 'SalesorderController@searchindex')->name('salesorders.searchindex');

    Route::POST('dailypro/quotations/searchindex', 'QuotationController@searchindex')->name('quotations.searchindex');

    Route::POST('dailypro/quotations/getroutebydocno', 'QuotationController@getRouteByDocno')->name('quotations.searchdocno');

    Route::POST('dailypro/adjustmentis/searchindex', 'AdjustmentiController@searchindex')->name('adjustmentis.searchindex');

    Route::POST('dailypro/adjustmentis/getroutebydocno', 'AdjustmentiController@getRouteByDocno')->name('adjustmentis.searchdocno');

    Route::POST('dailypro/adjustmentos/searchindex', 'AdjustmentoController@searchindex')->name('adjustmentos.searchindex');

    Route::POST('dailypro/adjustmentos/getroutebydocno', 'AdjustmentoController@getRouteByDocno')->name('adjustmentos.searchdocno');

    Route::POST('dailypro/salesreturns/searchindex', 'SalesreturnController@searchindex')->name('salesreturns.searchindex');

    Route::POST('cylindermaster/cylinders/searchindex', 'CylinderController@searchindex')->name('cylinders.searchindex');

    Route::get('ajax/dailypro/index/{search}', 'GoodController@searchindex')->name('ajax.index.get.search');

    Route::get('goods/pono/get/{creditor}', 'GoodController@pono_get')->name('goods.pono.get');

    Route::post('goods/pono/porderdt', 'GoodController@getPOrderDt')->name('goods.porderdt.post');

    Route::get('cashbills/dono/get/{debtor}', 'CashbillController@dono_get')->name('cashbills.dono.get');

    Route::POST('dailypro/cashbills/getroutebydocno', 'CashbillController@getRouteByDocno')->name('cashbills.searchdocno');

    Route::post('cashbills/dono/dorderdt', 'CashbillController@getDOrderDt')->name('cashbills.dorderdt.post');

    Route::get('invoices/dono/get/{debtor}', 'InvoiceController@dono_get')->name('invoices.dono.get');

    Route::post('invoices/dono/dorderdt', 'InvoiceController@getDOrderDt')->name('invoices.dorderdt.post');

    Route::POST('dailypro/invoices/getroutebydocno', 'InvoiceController@getRouteByDocno')->name('invoices.searchdocno');

    Route::get('home/get/sales', 'HomeController@getWeeklySales')->name('home.get.sales');

    Route::get('home/get/sales/monthly', 'HomeController@getMonthlySales')->name('home.get.sales.monthly');
    // Route::get('ajax/dailypro/get-by/itemcode/{itemcode}', 'CylinderInvoiceController@create')->name('ajax.dailypro.get.by-uom');

    Route::get('goods/editRoute/{id}', 'GoodController@editRoute')->name('stock.editRoute');

    Route::get('cashbill/editRoute/{id}', 'CashbillController@editRoute')->name('cashbill.editRoute');

    Route::get('goods/editAllRoute/{id}', 'GoodController@editAllRoute')->name('stock.editAllRoute');

    Route::get('purchasereport/bybill', function () {
        $Creditor = Creditor::get()->pluck('Detail', 'accountcode');
        $grouping = ['date' => 'Date', 'month' => 'Month', 'code' => 'Creditor', 'name' => 'Name'];
        $page_title = "Daily and Monthly Purchase by Bill No.";
        return view(
            'reportprinting.purchase.purchasebybill',
            compact('page_title', 'Creditor', 'grouping')
        );
    })->name('reportprinting.purchasebybill');

    Route::get('purchasereport/bystockcode', function () {
        $Stockcode = Stockcode::orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::orderBy('code')->pluck('code', 'code');
        $grouping = [
            'category' => 'Category', 'product' => 'Product',
            'brand' => 'Brand', 'location' => 'Location', 'uom' => 'U.O.M'
        ];

        $page_title = "Daily and Monthly Purchase by Stock Code";
        return view(
            'reportprinting.purchase.purchasebystockcode',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'grouping')
        );
    })->name('reportprinting.purchasebystockcode');

    Route::get('purchasereport/bybill/search', 'PurchaseReportController@processbybill')->name('purchasereport.processbybill');
    Route::get('purchasereport/bystockcode/search', 'PurchaseReportController@processbystockcode')->name('purchasereport.processbystockcode');

    Route::get('salesreport/bybill', function () {
        $Debtor = Debtor::get()->pluck('Detail', 'accountcode');
        $grouping = ['DATE' => 'Date', 'month' => 'Month', 'CODE' => 'Debtor', 'NAME' => 'Name'];
        $page_title = "Daily and Monthly Sales by Bill No.";
        return view(
            'reportprinting.sales.salesbybill',
            compact('page_title', 'Debtor', 'grouping')
        );
    })->name('reportprinting.salesbybill');

    Route::get('salesreport/bystockcode', function () {
        $Stockcode = Stockcode::orderBy('type', 'DESC')->orderBy('code')->pluck('code', 'code');
        $Category = Category::orderBy('code')->pluck('code', 'code');
        $Product = Product::orderBy('code')->pluck('code', 'code');
        $Brand = Brand::orderBy('code')->pluck('code', 'code');
        $Location = Location::orderBy('code')->pluck('code', 'code');
        $Uom = Uom::orderBy('code')->pluck('code', 'code');
        $grouping = [
            'category' => 'Category', 'product' => 'Product',
            'brand' => 'Brand', 'location' => 'Location', 'uom' => 'U.O.M'
        ];

        $page_title = "Daily and Monthly Sales by Stock Code";
        return view(
            'reportprinting.sales.salesbystockcode',
            compact('page_title', 'Stockcode', 'Category', 'Product', 'Brand', 'Location', 'Uom', 'grouping')
        );
    })->name('reportprinting.salesbystockcode');

    Route::get('sales/bybill/search', 'SalesReportController@processbybill')->name('salesreport.processbybill');
    Route::get('sales/bystockcode/search', 'SalesReportController@processbystockcode')->name('salesreport.processbystockcode');

    Route::resource('dailypro/cashsales', 'CashsalesController');
    Route::POST('dailypro/cashsales/index', 'CashsalesController@searchindex')->name('cashsales.searchindex');
    Route::POST('dailypro/cashsales/getroutebydocno', 'CashsalesController@getRouteByDocno')->name('cashsales.searchdocno');
    Route::get('dailypro/cashsales/report/jasper/{id}/cashsales', 'CashsalesController@jasper')->name('cashsales.jasper');
    Route::get('dailypro/cashsales/report/print', 'CashsalesController@print')->name('cashsales.print');

    Route::get('codereplacement/stockcode', function () {
        $print = PrintedIndexView::where('index', 'Creditor Master Listing')->pluck('printed_by');
        $page_title = "Stock Code Replacement";
        return view(
            'reportprinting.inventory.stockcodereplacement',
            compact('page_title', 'print')
        );
    })->name('codereplacement.stockcode');

    Route::get('stockmaster/stockcode/api/get', 'StockcodeController@api_get')->name('stockcode.api.get');

    Route::get('codereplacment/stockcode/page', 'StockcodeController@codereplacement')->name('codereplacement.replace');

    Route::get('codereplacement/debtorcode', function () {
        $print = PrintedIndexView::where('index', 'Creditor Master Listing')->pluck('printed_by');
        $page_title = "Debtor Code Replacement";
        return view(
            'reportprinting.inventory.debtorcodereplacement',
            compact('page_title', 'print')
        );
    })->name('codereplacement.debtorcode');

    Route::get('codereplacment/debtorcode/page', 'DebtorController@codereplacement')->name('debtorcode.replace');

    Route::get('codereplacement/creditorcode', function () {
        $print = PrintedIndexView::where('index', 'Creditor Master Listing')->pluck('printed_by');
        $page_title = "Creditor Code Replacement";
        return view(
            'reportprinting.inventory.creditorcodereplacement',
            compact('page_title', 'print')
        );
    })->name('codereplacement.creditorcode');

    Route::get('codereplacment/creditorcode/page', 'CreditorController@codereplacement')->name('creditorcode.replace');

    Route::get('public/attachment/{attachment}', function ($attachment) {

        return response()->download(public_path('attachment/' . $attachment));
    });

    Route::post('gasrack/attachment/delete', 'GasrackController@destroyAttach')->name('gasrack.attachment.destroy');

    Route::get('cylinder-data', 'CylinderController@cylinderDatatableList')->name('cylinder.datatable.data');

    Route::resource('dailypro/proformainvoices', 'ProformaInvoiceController');

    Route::POST('dailypro/proformainvoices/searchindex', 'ProformaInvoiceController@searchindex')->name('proformainvoices.searchindex');

    Route::get('proformainvoices/editRoute/{id}', 'ProformaInvoiceController@editRoute')->name('proforma.editRoute');

    Route::get('proformainvoices/editAllRoute/{id}', 'ProformaInvoiceController@editAllRoute')->name('proforma.editAllRoute');

    Route::get('dailypro/proforma/report/print/{id}', 'ProformaInvoiceController@print')->name('proformainvoices.print');

    Route::get('dailypro/proformainvoices/report/printIndex', 'ProformaInvoiceController@printIndex')->name('proformainvoices.printIndex');

    Route::resource('dailypro/cylinderquotations', 'CylinderquotationController');

    Route::POST('dailypro/cylinderquotations/searchindex', 'CylinderquotationController@searchindex')->name('cylinderquotations.searchindex');

    Route::get('cylinderquotations/editRoute/{id}', 'CylinderquotationController@editRoute')->name('cylinderquotations.editRoute');

    Route::get('cylinderquotations/editAllRoute/{id}', 'CylinderquotationController@editAllRoute')->name('cylinderquotations.editAllRoute');

    Route::get('dailypro/cylinderquotations/report/print/{id}', 'CylinderquotationController@print')->name('cylinderquotations.print');

    Route::get('cashbills/get/latestprice/{latestprice}', 'CashbillController@getLatestPrice')->name('cashbills.get.latestprice');

    Route::resource('dailypro/cylinderporders', 'CylinderporderController');

    Route::POST('dailypro/cylinderporders/searchindex', 'CylinderporderController@searchindex')->name('cylinderporders.searchindex');

    Route::get('cylinderporders/editRoute/{id}', 'CylinderporderController@editRoute')->name('cylinderporders.editRoute');

    Route::get('cylinderporders/editAllRoute/{id}', 'CylinderporderController@editAllRoute')->name('cylinderporders.editAllRoute');

    Route::get('dailypro/cylinderporders/report/print/{id}', 'CylinderporderController@print')->name('cylinderporders.print');

    Route::get('dailypro/cylinderporders/listsummary/print', 'CylinderporderController@printSummaryListing')->name('cylinderporders.listsummary.print');

    Route::POST('cylinderporders/api/post', 'CylinderporderController@api_store')->name('cylinderporders.api.post');

    Route::POST('proformainvoices/api/post', 'ProformaInvoiceController@api_store')->name('proformainvoices.api.post');

    Route::POST('cylinderquotations/api/post', 'CylinderquotationController@api_store')->name('cylinderquotations.api.post');

    Route::delete('cylinderporders/data/{data}', 'CylinderporderController@destroyData')->name('cylinderporders.data.destroy');

    Route::delete('proformainvoices/data/{data}', 'ProformaInvoiceController@destroyData')->name('proforma.data.destroy');

    Route::delete('cylinderquotations/data/{data}', 'CylinderquotationController@destroyData')->name('cylinderquotations.data.destroy');

    Route::get('usersetups/get/email/{email}', 'UserSetupController@getEmail')->name('usersetups.get.email');

    Route::get('usersetups/get/usercode/{usercode}', 'UserSetupController@getUsercode')->name('usersetups.get.usercode');

    Route::get('cylindercategory/get/code/{code}', 'CylindercategoryController@getCode')->name('cylindercategory.get.code');

    Route::get('dailypro/cylinderquotations/listsummary/print', 'CylinderquotationController@printSummaryListing')->name('cylinderquotations.listsummary.print');

    Route::resource('dailypro/cylindercashbills', 'CylinderCashbillController');

    Route::get('cylindercashbills/editRoute/{id}', 'CylinderCashbillController@editRoute')->name('cylindercashbills.editRoute');

    Route::get('dailypro/cylindercashbills/report/jasper/{id}/cylindercashbills', 'CylinderCashbillController@jasper')->name('cylindercashbills.jasper');

    Route::post('cylindercashbills/dnno/dndt', 'CylinderCashbillController@getDnDt')->name('cylindercashbills.dndt.post');

    Route::get('cylindercashbills/dnno/get/{debtor}', 'CylinderCashbillController@dnno_get')->name('cylindercashbills.dnno.get');

    Route::POST('cylindercashbills/api/post', 'CylinderCashbillController@api_store')->name('cylindercashbills.api.post');

    Route::get('debtormaster/salesman/{id}', 'DebtorController@getSalesman')->name('debtors.get.salesman');

    Route::get('deliverynotes/get/cylinder/{serial}', 'DeliveryNoteController@getCyDt')->name('deliverynote.get.cylinder');

    Route::get('deliverynotes/get/gasrack/{serial}', 'DeliveryNoteController@getGrDt')->name('deliverynote.get.gasrack');

    Route::POST('deliverynotes/set/rackdetail', 'DeliveryNoteController@saveRackDetail')->name('deliverynote.set.rackdetail');

    Route::get('deliverynotes/get/rackdetail', 'DeliveryNoteController@getRackDetail')->name('deliverynote.get.rackdetail');

    Route::get('dailypro/cylindercashbills/report/printindex', 'CylinderCashbillController@printIndex')->name('cylindercashbill.printIndex');

    Route::delete('cylindercashbills/data/{data}', 'CylinderCashbillController@destroyData')->name('cylindercashbills.data.destroy');

    Route::delete('cylinderinvoices/data/{data}', 'CylinderInvoiceController@destroyData')->name('cylinderinvoices.data.destroy');

    Route::get('debtor-data', 'DebtorController@debtorDatatableList')->name('debtor.datatable.data');

    Route::POST('cylindertracking-data', 'CylinderController@cylindertrackingDatatableList')->name('cylindertracking.datatable.post');

    Route::get('cashbill-data', 'CashbillController@cashbillDatatableList')->name('cashbill.datatable.data');

    Route::get('porder-data', 'PorderController@porderDatatableList')->name('porder.datatable.data');

    Route::get('cashbilldocnolist-data', 'CashbillController@docnoList')->name('cashbill.docno.list.data');

    Route::get('cashbilldebtorlist-data', 'CashbillController@debtorList')->name('cashbill.debtor.list.data');

    Route::get('cashbillsalesmanlist-data', 'CashbillController@salesmanList')->name('cashbill.salesman.list.data');

    Route::get('invoicedocnolist-data', 'InvoiceController@docnoList')->name('invoice.docno.list.data');

    Route::get('invoicedebtorlist-data', 'InvoiceController@debtorList')->name('invoice.debtor.list.data');

    Route::get('invoicesalesmanlist-data', 'InvoiceController@salesmanList')->name('invoice.salesman.list.data');

    Route::get('porderdocnolist-data', 'PorderController@docnoList')->name('porder.docno.list.data');

    Route::get('porderdebtorlist-data', 'PorderController@debtorList')->name('porder.debtor.list.data');

    // Route::get('pordersalesmanlist-data', 'PorderController@salesmanList')->name('porder.salesman.list.data');

    Route::get('gooddocnolist-data', 'GoodController@docnoList')->name('good.docno.list.data');

    Route::get('gooddebtorlist-data', 'GoodController@debtorList')->name('good.debtor.list.data');

    Route::get('preturndocnolist-data', 'PreturnController@docnoList')->name('preturn.docno.list.data');

    Route::get('preturndebtorlist-data', 'PreturnController@debtorList')->name('preturn.debtor.list.data');

    // Route::get('goodsalesmanlist-data', 'GoodController@salesmanList')->name('good.salesman.list.data');
    // Route::get('cashbilldocnoto-data', 'CashbillController@docnoList')->name('cashbill.docnoto.list.data');

    Route::resource('settings/accountposting', 'AccountPostingController');
    Route::any('settings/accountposting/query/search/find', 'AccountPostingController@query')->name('accountposting.query');
    Route::any('settings/accountposting/query/post', 'AccountPostingController@postAccount')->name('accountposting.post');

    Route::get('cylinderdebtorlist-data', 'CylinderController@debtorList')->name('cylinder.debtor.list.data');
    Route::get('creditorlist-data', 'CreditorController@creditorList')->name('creditor.list.data');

    Route::get('preturn-data', 'PreturnController@preturnDatatableList')->name('preturn.datatable.data');

    Route::get('deliveryorder-data', 'DeliveryorderController@deliveryorderDatatableList')->name('deliveryorder.datatable.data');

    Route::get('salesorder-data', 'SalesorderController@salesorderDatatableList')->name('salesorder.datatable.data');

    Route::get('quotation-data', 'QuotationController@quotationDatatableList')->name('quotation.datatable.data');

    Route::get('adjustmenti-data', 'AdjustmentiController@adjustmentiDatatableList')->name('adjustmenti.datatable.data');

    Route::get('adjustmento-data', 'AdjustmentoController@adjustmentoDatatableList')->name('adjustmento.datatable.data');

    Route::get('salesreturn-data', 'SalesreturnController@salesreturnDatatableList')->name('salesreturn.datatable.data');

});
