<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   $lastsyncdate = file_get_contents("php://input");
    if (isset($lastsyncdate)) {
        $date = json_decode($lastsyncdate);
    } else {
        exit;
    }

   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo = new PDO($dsn, $un, $pwd, $opt);
   
   $custDB = array();

   // Attempt to query database table and retrieve data
   try {
      $stmt = $pdo->query("SELECT id,
                          accountcode,
                          name,
                          def_price,
                          address1,
                          address2,
                          address3,
                          address4,
                          salesman,
                          created_at,
                          updated_at 
                          from debtors
                          where active = '1'
                          ORDER BY accountcode ASC" );
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
        if(!$row->address1){
          $row->address1 = "";
        }

        if(!$row->address2){
          $row->address2 = "";
        }

        if(!$row->address3){
          $row->address3 = "";
        }

        if(!$row->address4){
          $row->address4 = "";
        }

        if(empty($date)){
            $custDB['add'][] = $row;
        }
        else if(!empty($date)){
          if($date < $row->created_at){
            $custDB['add'][] = $row;
          }
          else if($date < $row->updated_at){
            $custDB['update'][] = $row;
          }
        } 
      }

      // Return data as JSON
      echo json_encode($custDB);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>