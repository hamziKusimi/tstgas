<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   // Set up the PDO parameters
   $date = $_REQUEST['date'];
    if (empty($date) or is_null($date)){
        exit;
    }

    $user = $_REQUEST['user'];
    if (empty($user) or is_null($user)){
        exit;
    }

   $dsn   = "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt   = array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo = new PDO($dsn, $un, $pwd, $opt);
   $data = array();

   $totsales = 0;

   // Attempt to query database table and retrieve data
   try {
      $stmt = $pdo->query("SELECT item_code as itmcode, 
                              subject as itmdescr,
                              SUM(totalqty) AS totqty,
                              SUM(cashbilldts.amount) AS amt 
                              FROM cashbills JOIN cashbilldts ON docno = doc_no
                              JOIN stockcodes ON item_code = code
                              WHERE cashbills.created_by = '".$user."' and 
                                    date = '".$date."' and
                                    type = 'Stock Item'
                              GROUP BY item_code
                              ORDER BY subject ASC");
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $row->amt = number_format($row->amt, 2);
         $row->amt = str_replace(',', '', $row->amt);

         $totsales = $totsales + $row->amt;
         
         $data['details'][] = $row;

         $data['totsales'] = str_replace(',', '', number_format($totsales, 2));
      }

      $stmt = $pdo->query("SELECT item_code as itmcode, 
                              subject as itmdescr,
                              SUM(totalqty) AS totqty,
                              SUM(cashbilldts.amount) AS amt 
                              FROM cashbills JOIN cashbilldts ON docno = doc_no
                              JOIN stockcodes ON item_code = code
                              WHERE cashbills.created_by = '".$user."' and 
                                    date = '".$date."' and
                                    type = 'Service'
                              GROUP BY item_code
                              ORDER BY subject ASC");
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $row->amt = number_format($row->amt, 2);
         $row->amt = str_replace(',', '', $row->amt);

         $totsales = $totsales + $row->amt;
         
         $data['details'][] = $row;

         $data['totsales'] = str_replace(',', '', number_format($totsales, 2));
      }

      $totsalesbydoc = 0;

      $stmtbydocno = $pdo->query("SELECT doc_no, 
                                        SUM(totalqty) as totalqty, 
                                        SUM(t1.amount) as amount 
                                  FROM  cashbilldts as t1 JOIN stockcodes as t2 
                                  ON t1.item_code = t2.code JOIN cashbills AS t3 ON doc_no = docno
                                  WHERE (t2.type = 'Stock Item' OR t2.type = 'Service') and date = '".$date."' and t1.created_by = '".$user."'
                                  GROUP by doc_no");

      while($row  = $stmtbydocno->fetch(PDO::FETCH_OBJ))
      { 
        $stmttransportqty = $pdo->query("SELECT SUM(totalqty) as transportqty
                                  FROM  cashbilldts as t1 JOIN stockcodes as t2 
                                  ON t1.item_code = t2.code JOIN cashbills AS t3 ON doc_no = docno
                                  WHERE t2.type = 'Service' and 
                                         date = '".$date."' and 
                                         t1.created_by = '".$user."' and
                                         t1.doc_no = '".$row->doc_no."'");

         $transportqty = $stmttransportqty->fetch(PDO::FETCH_OBJ);

         if($transportqty){
          $row->totalqty = $row->totalqty - $transportqty->transportqty;
         }
         // Assign each row of data to associative array
         $row->amount = number_format($row->amount, 2);

         $row->amount = str_replace(',', '', $row->amount);

         $totsalesbydoc = $totsalesbydoc + $row->amount;

         //get doc cust name
         $stmtgetcustnm = $pdo->query("SELECT account_code,
                                              name 
                                        FROM cashbills 
                                        WHERE docno = '".$row->doc_no."'");
         
         $row->custdetails = $stmtgetcustnm->fetch(PDO::FETCH_OBJ);

         $data['bydocno'][] = $row;
         $data['totsalesbydoc'] = str_replace(',', '', number_format($totsalesbydoc, 2));
      }

      // Return data as JSON
      echo json_encode($data);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>