<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   $lastsyncdate = file_get_contents("php://input");
    if (isset($lastsyncdate)) {
        $date = json_decode($lastsyncdate);
    } else {
        exit;
    }

   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo = new PDO($dsn, $un, $pwd, $opt);
   
   $compDB = array();

   // Attempt to query database table and retrieve data
   try {
      $stmt = $pdo->query("SELECT code, 
                                 name, 
                                 company_no,
                                 address1,
                                 address2,
                                 address3,
                                 address4,
                                 tel,
                                 fax,
                                 name1,
                                 uom1,
                                 uom2,
                                 uom3,
                                 price1,
                                 price2,
                                 price3,
                                 price4,
                                 price5,
                                 price6,
                                 updated_at
                                 FROM system_setups" );

      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
        if(empty($date) || $date < $row->updated_at){
            $compDB[] = $row;
        }
      }

      // Return data as JSON
      echo json_encode($compDB);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>