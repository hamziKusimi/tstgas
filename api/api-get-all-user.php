<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   $lastsyncdate = file_get_contents("php://input");
    if (isset($lastsyncdate)) {
        $date = json_decode($lastsyncdate);
    } else {
        exit;
    }

   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo = new PDO($dsn, $un, $pwd, $opt);
   
   $userDB = array();

   // Attempt to query database table and retrieve data
   try {
      $stmt = $pdo->query("SELECT id,
                                  code as usercode, 
                                  descr as name, 
                                  password,
                                  updated_at, 
                                  created_at,
                                  password_h 
                            from salesmen" );
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
        if(empty($date)){
            $userDB['add'][] = $row;
        }
        else if(!empty($date)){
          if($date < $row->created_at){
            $userDB['add'][] = $row;
          }
          else if($date < $row->updated_at){
            $userDB['update'][] = $row;
          }
        } 
      }

      // Return data as JSON
      echo json_encode($userDB);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>