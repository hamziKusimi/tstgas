<?php
   header('Access-Control-Allow-Origin: *');
   include("./api-config.php");

   // Set up the PDO parameters

   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo = new PDO($dsn, $un, $pwd, $opt);
   $data = array();

   // Attempt to query database table and retrieve data
   try {
      $stmt = $pdo->query("SELECT code as itmcode, 
                                 descr as itmdescr, 
                                 retprice as itmprice 
                                 FROM stockcodes 
                                 WHERE inactive = 0 and
                                       type = 'Stock Item'
                                 ORDER BY descr ASC");
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $row->itmprice = number_format($row->itmprice, 2);

         $row->itmtotprice = number_format($row->itmtotprice, 2);

         $row->itmqty = 0;

         $data[] = $row;
      }

      $stmt = $pdo->query("SELECT code as itmcode, 
                                 descr as itmdescr, 
                                 retprice as itmprice 
                                 FROM stockcodes 
                                 WHERE inactive = 0 and
                                       type = 'Service'
                                 ORDER BY descr DESC");
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $row->itmprice = number_format($row->itmprice, 2);

         $row->itmtotprice = number_format($row->itmtotprice, 2);

         $row->itmqty = 0;

         $data[] = $row;
      }

      // Return data as JSON
      echo json_encode($data);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>